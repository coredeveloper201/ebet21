<?php

namespace App\Utility\DateTime;

use App\Contracts\DateContracts\DateContracts;


class EbetDateTime implements DateContracts
{
    private $date;

    public function __construct()
    {
        /*
          by default if we set it to today date;
        */
        $this->date = strtotime('today');
    }

    public function getFormattedDate()
    {
        return date('Y-m-d', $this->date);
    }

    public function setTimeMil($date)
    {
        // dd(gettype(strtotime($date)));
        if (strtotime($date) === 'integer') {
            $this->date = $date;
        } else {
            $this->date = strtotime($date);
        }
        return $this->getFormattedDate();
    }
}
