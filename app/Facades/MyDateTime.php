<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class MyDateTime extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'myTime';
    }
}
