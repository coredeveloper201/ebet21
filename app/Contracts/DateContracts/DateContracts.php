<?php

namespace App\Contracts\DateContracts;

interface DateContracts
{
    public function getFormattedDate();

    /* se the date in millisecond */
    public function setTimeMil($date);
}
