<?php

namespace App;

use App\Models\BettingDetail;
use App\Models\ParlayEventId;
use App\Models\HorseBettingDetail;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role', 'username', 'image', 'coins', 'available_balance', 'free_pay_balance', 'credit_limit', 'betting', 'last_login', 'registered',
        'lang', 'timezone', 'sex', 'bio', 'website', 'notify', 'sendmail', 'remind', 'site_news', 'game_digest', 'privacy_page', 'privacy_result', 'rate',
        'agent_total', 'referred_by', 'status', 'is_casino',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getAvailableAttribute()
    {
        return  $this->available_balance;
    }

    public function getFreePlayAttribute()
    {
        return $this->free_pay_balance;
    }

    public function getPendingAttribute()
    {
        $bettingAmount = BettingDetail::where('user_id', $this->id)->where('result', 0)->where('status', 2)->where('free_play', 0)->sum('risk_amount');
        $parleyAmount = ParlayEventId::where('user_id', $this->id)->where('result', 0)->sum('risk_amount');
        $horseAmount=HorseBettingDetail::where("user_id",$this->id)->where('result',0)->sum('risk_amount');
        return ($bettingAmount + $parleyAmount+$horseAmount);
    }

    public function getBalanceAttribute()
    {
        return $this->coins;
    }


    public function agentRate(){
        return $this->hasOne('App\Models\AgentRate', 'user_id', 'id')->orderBy('id', 'desc');
    }

    public function lastLogin(){
        return $this->hasOne('App\Models\LoginLog', 'user_id', 'id')->orderBy('id', 'desc');
    }

    public function assignUser(){
        return $this->hasOne('App\Models\AssignUser', 'user_id', 'id')
            ->select('assign_users.*','users.username as agent_username','users.name as agent_name')
            ->leftJoin('users', 'users.id','=','assign_users.agent_id');
    }

    public function allAssignUser(){
        return $this->hasMany('App\Models\AssignUser', 'agent_id', 'id');
    }
    public function creditLimits()
    {
       return $this->hasOne('App\Models\CreditLimit');
    }


}
