<?php

namespace App\Http\Controllers\UserController;

use stdClass;
use Illuminate\Http\Request;
use App\Ebet\Sports\Live\Basket;
use App\Ebet\Sports\Live\Hockey;
use App\Ebet\Sports\Live\Soccer;
use App\Ebet\Sports\Live\Tennis;
use Elasticsearch\ClientBuilder;
use App\Ebet\Sports\Live\Baseball;
use App\Ebet\Sports\Live\Football;
use App\Http\Controllers\Controller;
use App\Ebet\Sports\Live\LiveBetFormat;

class LiveBetsController extends Controller
{
    private $sports = [];

    public function __construct()
    {


        $this->sports['hockey'] = null;
        $this->sports['basket'] = null;
        $this->sports['baseball'] = null;
        $this->sports['soccer'] = null;
        $this->sports['tennis'] = null;
        $this->sports['football'] = null;
        $this->_getSportData();

    }
    public function _getSportData()
    {
        $client = ClientBuilder::create()->build();
        if(!empty($this->sports))
        {
            foreach($this->sports as $sport_key => $val)
            {
                 $get_index = [
                'index' => "live_".$sport_key,
                'id'    => 'live_'.$sport_key.'_id',
               ];
              $this->sports[$sport_key] =  ($client->getSource($get_index))[$sport_key.'_sports_data'];

            }
        }
    }
    public function getSportsData(){
        return $this->sports;
    }
    public function liveHockeyView(){
        return view('frontend.live_bets.live-hockey');
    }
    public function liveHockey()
    {
        return $this->sports['hockey'];
    }

    public function liveTennis()
    {
        return $this->sports['tennis'];
      /*   error_reporting(0);
        $url = 'https://api.betsapi.com/v1/bet365/inplay_filter?sport_id=13&token=26928-6HWCoyS7zhskxm';
        $sportsData = $this->getApiResponse($url);

        $event_details = array();
        foreach ($sportsData->results as $i => $item) {
            $fetchURLs = 'https://api.betsapi.com/v2/event/odds?token=26928-6HWCoyS7zhskxm&event_id=' . $item->our_event_id;
            $event_details[$i] = $this->getApiResponse($fetchURLs);
        }
        foreach ($event_details as $k => $ch) {
            foreach ($ch->results->odds as $keyy => $vall) {
                $itemm = $sportsData->results[$k];
                if ($keyy == '13_1') {
                    $winnerset_home =  $vall[0]->home_od;
                    if (!empty($winnerset_home)) {
                        $itemm->winner_set->home_odd = $this->toAmericanDecimal($winnerset_home);
                    } else {
                        $itemm->winner_set->home_odd = 0;
                    }
                    $winnerset_away =  $vall[0]->away_od;
                    if (!empty($winnerset_away)) {
                        $itemm->winner_set->away_odd = $this->toAmericanDecimal($winnerset_away);
                    } else {
                        $itemm->winner_set->away_odd = 0;
                    }
                }
                if ($keyy == '13_4') {
                    $currentset_home =  $vall[0]->home_od;
                    if (!empty($currentset_home)) {

                        $itemm->current_set->home_odd = $this->toAmericanDecimal($currentset_home);
                    } else {
                        $itemm->current_set->home_odd = 0;
                    }
                    $currentset_away =  $vall[0]->away_od;
                    if (!empty($currentset_away)) {

                        $itemm->current_set->away_odd = $this->toAmericanDecimal($currentset_away);
                    } else {
                        $itemm->current_set->away_odd = 0;
                    }
                }
            }
        }

        $sortedbyleague = [];
        foreach ($sportsData->results as $sdata) {
            $tag = false;
            foreach ($sortedbyleague as  $sbl) {
                if ($sbl->even_id == $sdata->league->id) {
                    $tag = true;
                    array_push($sbl->results, $sdata);
                    break;
                }
            }
            if (!$tag) {
                $obj = new stdClass();
                $obj->even_id   = $sdata->league->id;
                $obj->even_name = $sdata->league->name;
                $obj->results   =  array();
                array_push($obj->results, $sdata);
                array_push($sortedbyleague, $obj);
            }
        }
        return $sortedbyleague; */
    }
    public function liveBasketBall()
    {
        return $this->sports['basket'];

        error_reporting(0);

        $url = 'https://api.betsapi.com/v1/bet365/inplay_filter?sport_id=18&token=26928-6HWCoyS7zhskxm';
        $sportsData = $this->getApiResponse($url);

        $event_details = array();
        foreach ($sportsData->results as $i => $item) {
            $fetchURL = 'https://api.betsapi.com/v2/event/odds?token=26928-6HWCoyS7zhskxm&event_id=' . $item->our_event_id;
            $event_details[$i] = $this->getApiResponse($fetchURL);
        }
        foreach ($event_details as $k => $ch) {

            foreach ($ch->results->odds as $key => $val) {

                $item = $sportsData->results[$k];
                //money line
                if ($key == '18_1') {
                    $item->match->over->odd  =  $val[0]->home_od != 0 ? $this->toAmericanDecimal($val[0]->home_od) : 0;
                    $item->match->under->odd =  $val[0]->away_od != 0 ? $this->toAmericanDecimal($val[0]->away_od) : 0;
                }
                //Spread
                if ($key == '18_2') {
                    $item->odd->first      =  $val[0]->home_od != 0 ? $this->toAmericanDecimal($val[0]->home_od) : 0;
                    $item->odd->first_val  =  $val[0]->handicap;
                    $item->odd->second     =  $val[0]->away_od != 0 ? $this->toAmericanDecimal($val[0]->away_od) : 0;
                    $item->odd->second_val =  $val[0]->handicap;
                }
                //total
                if ($key == '18_3') {
                    $item->next->first          =  $val[0]->over_od != 0 ? $this->toAmericanDecimal($val[0]->over_od) : 0;
                    $item->next->first_lagline  = "over_od";
                    $item->next->first_val      =  $val[0]->handicap;
                    $item->next->second         =  $val[0]->under_od != 0 ? $this->toAmericanDecimal($val[0]->under_od) : 0;
                    $item->next->second_lagline = "under_od";
                    $item->next->second_val     =  $val[0]->handicap;
                }
            }
        }

        /*
            * For timer we call again another api for that
            *
            * */

        foreach ($sportsData->results as $i3 => $m) {
            $league_info = $m;
            $fetchURLs = 'https://api.betsapi.com/v1/event/view?token=26928-6HWCoyS7zhskxm&event_id=' . $m->our_event_id;
            $timer_response = $this->getApiResponse($fetchURLs);
            $league_info->eventimers =   $timer_response->results[0]->timer;
        }

        $sortedbyleague = [];
        foreach ($sportsData->results as $sdata) {
            $tag = false;

            foreach ($sortedbyleague as  $sbl) {
                if ($sbl->even_id == $sdata->league->id) {
                    $tag = true;
                    array_push($sbl->results, $sdata);
                    break;
                }
            }
            if (!$tag) {
                $obj = new stdClass();
                $obj->even_id   = $sdata->league->id;
                $obj->even_name = $sdata->league->name;
                $obj->results   =  array();
                array_push($obj->results, $sdata);
                array_push($sortedbyleague, $obj);
            }
        }


        return $sortedbyleague;
    }
    public function liveBaseBall()
    {
        return $this->sports['baseball'];
        error_reporting(0);
        $url = 'https://api.betsapi.com/v1/bet365/inplay_filter?sport_id=16&token=26928-6HWCoyS7zhskxm';
        $sportsData = $this->getApiResponse($url);


        $event_details = array();
        foreach ($sportsData->results as $i => $item) {

            $fetchURL = 'https://api.betsapi.com/v2/event/odds?token=26928-6HWCoyS7zhskxm&event_id=' . $item->our_event_id;
            $event_details[$i] = $this->getApiResponse($fetchURL);
        }


        foreach ($event_details as $k => $ch) {
            foreach ($ch->results->odds as $key => $val) {
                $item = $sportsData->results[$k];
                //money line
                if ($key == '16_1') {
                    $item->match->over->odd =   $val[0]->home_od != 0 ? $this->toAmericanDecimal($val[0]->home_od) : 0;
                    $item->match->under->odd =  $val[0]->away_od != 0 ? $this->toAmericanDecimal($val[0]->away_od) : 0;
                }

                //Run line
                if ($key == '16_2') {
                    $item->odd->first      =  $val[0]->home_od != 0 ? $this->toAmericanDecimal($val[0]->home_od) : 0;
                    $item->odd->first_val  =  $val[0]->handicap;
                    $item->odd->second     =  $val[0]->away_od != 0 ? $this->toAmericanDecimal($val[0]->away_od) : 0;
                    $item->odd->second_val =  $val[0]->handicap;
                }


                //total
                if ($key == '16_3') {
                    $item->next->first          =  $val[0]->over_od != 0 ? $this->toAmericanDecimal($val[0]->over_od) : 0;
                    $item->next->first_pretype  = "over_od";
                    $item->next->first_val      =   $val[0]->handicap;
                    $item->next->second         =  $val[0]->under_od != 0 ? $this->toAmericanDecimal($val[0]->under_od) : 0;
                    $item->next->second_pretype = "under_od";
                    $item->next->second_val     =   $val[0]->handicap;
                }

                $item->no_of_innings = 0;
            }
        }
        /*
        * we are calling again the old api for preserving some of
        * the old value for view
        * */
        $multiCurl1 = array();
        $mhs = curl_multi_init();
        foreach ($sportsData->results as $ii => $m) {
            $league_info = $m;
            // URL from which data will be fetched
            $fetchURL = 'https://api.betsapi.com/v1/bet365/event?token=26928-6HWCoyS7zhskxm&FI=' . $m->id;
            //$fetchURL = 'https://api.betsapi.com/v2/event/odds?token=26928-6HWCoyS7zhskxm&event_id='.$item->our_event_id;
            $timer_response = $this->getApiResponse($fetchURL);
            $league_info->eventimers =   $timer_response->results[0]->timer;

        }
        $sortedbyleague = [];
        foreach ($sportsData->results as $sdata) {
            $tag = false;
            foreach ($sortedbyleague as  $sbl) {
                if ($sbl->even_id == $sdata->league->id) {
                    $tag = true;
                    array_push($sbl->results, $sdata);
                    break;
                }
            }
            if (!$tag) {
                $obj = new stdClass();
                $obj->even_id   = $sdata->league->id;
                $obj->even_name = $sdata->league->name;
                $obj->results   =  array();
                array_push($obj->results, $sdata);
                array_push($sortedbyleague, $obj);
            }
        }
        return $sortedbyleague;
    }
    public function liveFootBall()
    {
        return $this->sports['football'];
        error_reporting(0);
        $url = 'https://api.betsapi.com/v1/bet365/inplay_filter?sport_id=12&token=26928-6HWCoyS7zhskxm';
        $sportsData = $this->getApiResponse($url);

        $event_details = array();
        foreach ($sportsData->results as $i => $item) {

            $fetchURL = 'https://api.betsapi.com/v2/event/odds?token=26928-6HWCoyS7zhskxm&event_id=' . $item->our_event_id;
            $event_details[$i] = $this->getApiResponse($fetchURL);
        }

        foreach ($event_details as $k => $ch) {
            foreach ($ch->results->odds as $key => $val) {
                $item = $sportsData->results[$k];
                //money line
                if ($key == '12_1') {
                    $item->match->over->odd  =  $val[0]->home_od != 0 ? $this->toAmericanDecimal($val[0]->home_od) : 0;
                    $item->match->under->odd =  $val[0]->away_od != 0 ? $this->toAmericanDecimal($val[0]->away_od) : 0;
                }

                //Spread
                if ($key == '12_2') {
                    $item->odd->first      =  $val[0]->home_od != 0 ? $this->toAmericanDecimal($val[0]->home_od) : 0;
                    $item->odd->first_val  =  $val[0]->handicap;
                    $item->odd->second     =  $val[0]->away_od != 0 ? $this->toAmericanDecimal($val[0]->away_od) : 0;
                    $item->odd->second_val =  $val[0]->handicap;
                }


                //total
                if ($key == '12_3') {
                    $item->next->first          =  $val[0]->over_od != 0 ? $this->toAmericanDecimal($val[0]->over_od) : 0;
                    $item->next->first_pretype  = "over_od";
                    $item->next->first_val      =  $val[0]->handicap;
                    $item->next->second         =  $val[0]->under_od != 0 ? $this->toAmericanDecimal($val[0]->under_od) : 0;
                    $item->next->second_pretype = "under_od";
                    $item->next->second_val     =  $val[0]->handicap;
                }
            }
        }

        $sortedbyleague = [];
        foreach ($sportsData->results as $sdata) {
            $tag = false;

            foreach ($sortedbyleague as  $sbl) {
                if ($sbl->even_id == $sdata->league->id) {
                    $tag = true;
                    array_push($sbl->results, $sdata);
                    break;
                }
            }
            if (!$tag) {
                $obj = new stdClass();
                $obj->even_id   = $sdata->league->id;
                $obj->even_name = $sdata->league->name;
                $obj->results   =  array();
                array_push($obj->results, $sdata);
                array_push($sortedbyleague, $obj);
            }
        }


        return $sortedbyleague;
    }

    public function liveCycling()
    {
        error_reporting(0);
        $url = 'https://api.betsapi.com/v1/bwin/inplay?token=26928-6HWCoyS7zhskxm&sport_id=10';
        $sportsData = $this->getApiResponse($url);

        $event_details = array();
        $cycling_details;
        foreach ($sportsData->results as $i => $item) {

            $fetchURL = 'https://api.betsapi.com/v1/bwin/event?token=26928-6HWCoyS7zhskxm&event_id=' . $item->Id;
            $cycling_details = $this->getApiResponse($fetchURL);
            $item->gamesData = [];
            foreach ($cycling_details->results[0]->Markets[0]->Results as $kr => $vr) {
                $obj       =   new stdClass;
                $obj->id   =   $vr->Id;
                $obj->name =   $vr->Name;
                $obj->odd  =   $this->toAmericanDecimal($vr->Odds);
                array_push($item->gamesData, $obj);
            }
        }
        return $sportsData->results;
    }
    public function liveMotorSport()
    {
        error_reporting(0);
        $url = 'https://api.betsapi.com/v1/bwin/inplay?token=26928-6HWCoyS7zhskxm&sport_id=40';
        /*$ch = curl_init($url);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $data = curl_exec($ch);
        if ($data === false) {
            $info = curl_getinfo($ch);
            curl_close($ch);
            die('error occured during curl exec. Additioanl info: ' . var_export($info));
        }
        curl_close($ch);*/

        $arrContextOptions=array(
            "ssl"=>array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ),
        );
        $sportsData = json_decode(file_get_contents($url, false, stream_context_create($arrContextOptions)));
       // $sportsData = json_decode($data);

        $multiCurl = array();

        $mh = curl_multi_init();
        foreach ($sportsData->results as $i => $item) {

            $fetchURL = 'https://api.betsapi.com/v1/bwin/event?token=26928-6HWCoyS7zhskxm&event_id=' . $item->Id;
            $multiCurl[$i] = curl_init();
            curl_setopt($multiCurl[$i], CURLOPT_URL, $fetchURL);
            curl_setopt($multiCurl[$i], CURLOPT_HEADER, 0);
            curl_setopt($multiCurl[$i], CURLOPT_RETURNTRANSFER, 1);
            curl_multi_add_handle($mh, $multiCurl[$i]);
        }
        $index = null;
        do {
            curl_multi_exec($mh, $index);
        } while ($index > 0);

        foreach ($multiCurl as $k => $ch) {

            $event     = curl_multi_getcontent($ch);
            $eventData = json_decode($event);


            $item      = $sportsData->results[$k];
            $item->gamesData = [];
            foreach ($eventData->results[0]->Markets[0]->Results as $kr => $vr) {
                $obj       =   new stdClass;
                $obj->id   =   $vr->Id;
                $obj->name =   $vr->Name;
                $obj->odd  =   $this->toAmericanDecimal($vr->Odds);
                array_push($item->gamesData, $obj);
            }
            curl_multi_remove_handle($mh, $ch);
        }
        // close
        curl_multi_close($mh);

        return $sportsData->results;
    }

    public function liveVolleyBall()
    {
        error_reporting(0);
        $url = 'https://api.betsapi.com/v1/bet365/inplay_filter?sport_id=91&token=26928-6HWCoyS7zhskxm';
        $sportsData = $this->getApiResponse($url);

        $event_details = array();
        foreach ($sportsData->results as $i => $item) {

            // URL from which data will be fetched
            //    $fetchURL = 'https://api.betsapi.com/v1/bet365/event?token=26928-6HWCoyS7zhskxm&FI='.$item->id;
            $fetchURL = 'https://api.betsapi.com/v2/event/odds?token=26928-6HWCoyS7zhskxm&event_id=' . $item->our_event_id;
            $event_details[$i] = $this->getApiResponse($fetchURL);
        }

        foreach ($event_details as $k => $ch) {

            foreach ($ch->results->odds as $key => $val) {

                $item = $sportsData->results[$k];

                //money line
                if ($key == '91_1') {
                    $item->match->over->odd =   $val[0]->home_od != 0 ? $this->toAmericanDecimal($val[0]->home_od) : 0;
                    $item->match->under->odd =  $val[0]->away_od != 0 ? $this->toAmericanDecimal($val[0]->away_od) : 0;
                }

                //Run line
                if ($key == '91_2') {
                    $item->odd->first      =  $val[0]->home_od != 0 ? $this->toAmericanDecimal($val[0]->home_od) : 0;
                    $item->odd->first_val  =  $val[0]->handicap;
                    $item->odd->second     =  $val[0]->away_od != 0 ? $this->toAmericanDecimal($val[0]->away_od) : 0;
                    $item->odd->second_val =  $val[0]->handicap;
                }


                //total
                if ($key == '91_3') {
                    $item->next->first          =   $val[0]->over_od != 0 ? $this->toAmericanDecimal($val[0]->over_od) : 0;
                    $item->next->first_pretype  =   "over_od";
                    $item->next->first_val      =   $val[0]->handicap;
                    $item->next->second         =   $val[0]->under_od != 0 ? $this->toAmericanDecimal($val[0]->under_od) : 0;
                    $item->next->second_pretype =   "under_od";
                    $item->next->second_val     =   $val[0]->handicap;
                }

                $item->no_of_innings = 0;
            }
        }
        // close
        foreach ($sportsData->results as $ii => $item2) {
            $league_info = $item2;
            //    $fetchURL = 'https://api.betsapi.com/v1/bet365/event?token=26928-6HWCoyS7zhskxm&FI='.$item->id;
            $fetchURL = 'https://api.betsapi.com/v1/event/view?token=26928-6HWCoyS7zhskxm&event_id=' . $item2->our_event_id;
            $response_result  = $this->getApiResponse($fetchURL);
            $league_info->set_number = count((array) $response_result->results[0]->scores);

        }

        $sortedbyleague = [];
        foreach ($sportsData->results as $sdata) {
            $tag = false;

            foreach ($sortedbyleague as  $sbl) {
                if ($sbl->even_id == $sdata->league->id) {
                    $tag = true;
                    array_push($sbl->results, $sdata);
                    break;
                }
            }
            if (!$tag) {
                $obj = new stdClass();
                $obj->even_id   = $sdata->league->id;
                $obj->even_name = $sdata->league->name;
                $obj->results   =  array();
                array_push($obj->results, $sdata);
                array_push($sortedbyleague, $obj);
            }
        }
        return $sortedbyleague;
    }

    public function liveSoccerLeaguesLists()
    {
        error_reporting(0);
        $url = 'https://api.betsapi.com/v1/bet365/inplay_filter?sport_id=1&token=26928-6HWCoyS7zhskxm';
        $sportsData = $this->getApiResponse($url);

        $sortedbyleague = [];
        foreach ($sportsData->results as $sdata) {
            $sortedbyleague[$sdata->league->id]['id'] = $sdata->league->id;
            $sortedbyleague[$sdata->league->id]['name'] = $sdata->league->name;
            $sortedbyleague[$sdata->league->id]['event'][] = $sdata;
        }
        return $sortedbyleague;
    }

    public function liveSoccerView(Request $r)
    {
        $sportsData = null;

        if (isset($r->leagues)) {
            $sportsData = json_decode($r->leagues);
        }
        return view('frontend.live_sports')->with('leaguesids', $sportsData);
    }
    public function liveSoccer(Request $r)
    {
     return $this->sports['soccer'];
        error_reporting(0);

        $sportsData = null;

        if (isset($r->leagueids)) {
            $sportsData = json_decode($r->leagueids);
        }

        $multiCurl = array();

        $mh = curl_multi_init();
        foreach ($sportsData as $i => $item) {

            //    $multiCurl[$i] = curl_init();
            $multiCurl[$i] = new stdClass();

            $event_fetchURL = 'https://api.betsapi.com/v1/bet365/event?token=26928-6HWCoyS7zhskxm&FI=' . $item->id;
            $multiCurl[$i]->events = curl_init($event_fetchURL);
            curl_setopt($multiCurl[$i]->events, CURLOPT_RETURNTRANSFER, 1);
            curl_multi_add_handle($mh, $multiCurl[$i]->events);

            $event_odd_fetchURLs = 'https://api.betsapi.com/v2/event/odds?token=26928-6HWCoyS7zhskxm&event_id=' . $item->our_event_id;
            $multiCurl[$i]->event_odd = curl_init($event_odd_fetchURLs);
            curl_setopt($multiCurl[$i]->event_odd, CURLOPT_RETURNTRANSFER, 1);
            curl_multi_add_handle($mh, $multiCurl[$i]->event_odd);


            $event_view_fetchURLs = 'https://api.betsapi.com/v1/event/view?token=26928-6HWCoyS7zhskxm&event_id=' . $item->our_event_id;
            $multiCurl[$i]->event_view = curl_init($event_view_fetchURLs);
            curl_setopt($multiCurl[$i]->event_view, CURLOPT_RETURNTRANSFER, 1);
            curl_multi_add_handle($mh, $multiCurl[$i]->event_view);
        }


        $index = null;
        do {
            curl_multi_exec($mh, $index);
        } while ($index > 0);

        foreach ($multiCurl as $k => $ch) {
            $event = curl_multi_getcontent($ch->events);
            $eventData = json_decode($event);

            $goal = 0;
            foreach ($eventData->results[0] as $key => $val) {
                $item = $sportsData[$k];

                if ($val->type == 'MA' && $goal == 0 && strpos($val->NA, 'Goal')) {
                    $goal = $key;
                }
                if ($val->type == 'PA') {
                    if (strpos($val->NA, 'Goal') && substr($val->NA, 0, 2) == 'No' && !strpos($val->NA, 'o Goal')) {
                        $item->draw->goal->name = $val->NA;
                        $numVal = explode('/', $val->OD);
                        if (count($numVal) > 1) {
                            if ($numVal[1] != 0) {
                                $pointVal = intval($numVal[0]) / intval($numVal[1]);
                                $item->draw->goal->odd = $pointVal >= 1 ? round($pointVal * 100, 0) : round(-100 / $pointVal, 0);
                            } else {
                                $item->draw->goal->odd = 0;
                            }
                        }
                    }
                }
            }
            curl_multi_remove_handle($mh, $ch->events);

            $event_odd = curl_multi_getcontent($ch->event_odd);
            $event_odds = json_decode($event_odd);

            foreach ($event_odds->results->odds as $key => $vv) {

                $event_ods = $sportsData[$k];


                //Halftime Result
                if ($key == '1_8') {

                    $event_ods->next->first = $vv[0]->home_od != 0 ? $this->toAmericanDecimal($vv[0]->home_od) : 0;
                    $event_ods->next->second = $vv[0]->away_od != 0 ? $this->toAmericanDecimal($vv[0]->away_od) : 0;
                }

                //1X2
                if ($key == '1_1') {
                    $event_ods->odd->first = $vv[0]->home_od != 0 ? $this->toAmericanDecimal($vv[0]->home_od) : 0;
                    $event_ods->odd->second = $vv[0]->away_od != 0 ? $this->toAmericanDecimal($vv[0]->away_od) : 0;
                    $event_ods->draw->o_to_t = $vv[0]->draw_od != 0 ? $this->toAmericanDecimal($vv[0]->draw_od) : 0;
                }

                //match goals
                if ($key == '1_3') {
                    $event_ods->match->over->odd = $vv[0]->over_od != 0 ? $this->toAmericanDecimal($vv[0]->over_od) : 0;
                    $event_ods->match->over->val = $vv[0]->handicap;
                    $event_ods->match->under->odd = $vv[0]->under_od != 0 ? $this->toAmericanDecimal($vv[0]->under_od) : 0;
                    $event_ods->match->under->val = $vv[0]->handicap;
                }
            }
            curl_multi_remove_handle($mh, $ch->event_odd);


            $event_view = curl_multi_getcontent($ch->event_view);
            $event_views = json_decode($event_view);


            $item3 = $sportsData[$k];

            $item3->eventimers = $event_views->results[0]->timer;



            curl_multi_remove_handle($mh, $ch->event_view);
        }

        // close
        curl_multi_close($mh);
        $sortedbyleague = [];
        foreach ($sportsData as $sdata) {
            $tag = false;

            foreach ($sortedbyleague as  $sbl) {

                if ($sbl->even_id == $sdata->league->id) {
                    $tag = true;
                    array_push($sbl->results, $sdata);
                    break;
                }
            }
            if (!$tag) {
                $obj = new stdClass();
                $obj->even_id   = $sdata->league->id;
                $obj->even_name = $sdata->league->name;
                $obj->results   =  array();

                array_push($obj->results, $sdata);
                array_push($sortedbyleague, $obj);
            }
        }

        return $sortedbyleague;
        //return $sortedbyleague;
    }
    public function liveSportsMenus()
    {
        $available_menu = [];
        $available_menu['hockey'] = (!empty($this->sports['hockey'])) ? 1:0;
        $available_menu['basket'] = (!empty($this->sports['basket'])) ? 1:0;
        $available_menu['baseball'] = (!empty($this->sports['baseball'])) ? 1:0;
        $available_menu['soccer'] = (!empty($this->sports['soccer'])) ? 1:0;
        $available_menu['tennis'] = (!empty($this->sports['tennis'])) ? 1:0;
        $available_menu['football'] = (!empty($this->sports['football'])) ? 1:0;
        return $available_menu;
    }

    public function liveSports()
    {
        return view('frontend.live_sports');
    }

    public function checkUserpassword(Request $r)
    {
        return response()->json(['status' => $this->checkPassword($r->pass)]);
    }

}
