<?php

namespace App\Http\Controllers\UserController;

use stdClass;
use App\Ebet\BetResults;
use App\Models\WagerDisable;
use Illuminate\Http\Request;
use App\Models\BettingDetail;
use App\Models\ParlayEventId;
use App\Models\HorseBettingDetail;
use App\Models\ParlayBettingDetail;
use App\Http\Controllers\Controller;
use App\Ebet\Sports\Contributor\PropBet;
use App\Ebet\Db\Elastic\Contributor\ElasticDataFilter;
use App\Ebet\Db\Elastic\Contributor\ElasticDataExtract;
use App\Ebet\Sports\Contributor\StraightBet as EbetBets;

class StraightBet extends Controller
{

    use EbetBets,PropBet,ElasticDataFilter,ElasticDataExtract;


    public function __construct()
    {
         $this->getData();
         $this->resourceDistributionArray();
    }

    public function index()
    {
        $data = [
            "first_column"  => $this->first_column,
            "second_column" => $this->second_column,
            "third_column"  => $this->third_column,
            "images"        => $this->sport_images,
        ];
        $data = json_decode(json_encode($data), TRUE);
        return view('frontend.index')->with("data", $data);
    }

    public function straightOdds(Request $r)
    {
        $league_ids = [];
        if (!$r->has('league_ids')) {
            if ($r->session()->has('league_ids')) {
                $league_ids = $r->session()->get('league_ids');
            } else {
                $r->session()->flash('notice', 'Please, select a league');
                return redirect('/straight-bet');
            }
        } else {
            $league_ids = $r->input('league_ids');
            $r->session()->put('league_ids', $league_ids);
        }

        $this->getleagueData($league_ids);
        foreach ($league_ids as $k => $v) {
                foreach ($v as $league_k => $league_v) {
                    if ($k != 133 && $k != 6 && $k != 10) {
                        $league =   $this->_getLeagueData($league_k,$k);
                        $sobj = new stdClass();
                        $sobj->league_id = $league['league_data']['league_id'];
                        $sobj->sport_id = $k;
                        $sobj->league_data = $league;
                        $this->league_details[$sobj->league_id] = $sobj;
                    }
                    else{
                        $sobj = new stdClass();
                        $sobj->league_id = $league_k;
                        $sobj->sport_id = $k;
                        $getresponse = curl_multi_getcontent($this->odds_multicurl[$league_k]);
                        $obj = json_decode($getresponse);
                        $sobj->league_data = $obj;
                        $this->league_details[$league_k] = $sobj;
                    }
                }
          }
        foreach ($this->league_details as $key => $val) {

                if (array_key_exists("league_data",$val->league_data)) {
                    foreach ($val->league_data['league_data']['events'] as $k => &$v) {
                        $item = null;
                        $event_id = null;
                        if ($val->sport_id != 133 && $val->sport_id != 6 && $val->sport_id != 10) {
                                $item =(object)$v;
                           /*  $item = new stdClass; */
                            $v['event_detail'] = $item->event_odd;
                            $v['prop_bets'] = isset($item->prop_bets)?$item->prop_bets:'';
                            $event_info = $v['event_info'];
                            $v['time'] = $event_info['e_time'];
                            $v['date'] = $event_info['e_date'];
                            $v['id'] = $event_info['id'];
                            $v['home']['name'] = $event_info['home_name'];
                            $v['home']['id'] = $event_info['home_id'];
                            $v['away']['name'] = $event_info['away_name'];
                            $v['away']['id'] = $event_info['away_id'];
                            $v['league']['name'] = $val->league_data['league_data']['league_name'];
                            $v['sport_id'] = $val->sport_id;
                        }
                    }
                }
        }
        //dd($this->league_details);
        return view('frontend.straight_leagues')->with("data", $this->league_details);
    }
    public function propsBet(Request $r)
    {
        $event_info = null;
        if ($r->event_info != null) {
            $rqsData = json_decode($r->event_info);

            $propBetInfos = $rqsData->prop_bets;
            $event_info = $rqsData->event_info;
            $event_info->home = $rqsData->home;
            $event_info->away = $rqsData->away;
            $event_info->league = $rqsData->league;
            $event_info->sport_id = $r->sport_id;
        }

        $array_data = [
            "event_info" => $event_info,
            "prop_data" => $propBetInfos
        ];
        if($r->sport_id == 12) {
          return view('frontend.prop_bets._football')->with('prop_bet_details', $array_data);
        } elseif($r->sport_id == 17) {
          return view('frontend.prop_bets._hockey')->with('prop_bet_details', $array_data);
        } elseif($r->sport_id == 18) {
          return view('frontend.prop_bets._basketball')->with('prop_bet_details', $array_data);
        } elseif($r->sport_id == 14) {
          return view('frontend.prop_bets._volleyball')->with('prop_bet_details', $array_data);
        } elseif($r->sport_id == 1) {
          return view('frontend.prop_bets._soccer')->with('prop_bet_details', $array_data);
        } elseif($r->sport_id == 16) {
          return view('frontend.prop_bets._baseball')->with('prop_bet_details', $array_data);
        } elseif($r->sport_id == 13) {
          return view('frontend.prop_bets._tennis')->with('prop_bet_details', $array_data);
        }
    }

    //for admin
    public function straightLeagueApproval()
    {

        $data = [
            "first_column"  => $this->first_column,
            "second_column" => $this->second_column,
            "third_column"  => $this->third_column,
            "images"        => $this->sport_images,
        ];
       // dd($data);
        $data = json_decode(json_encode($data), TRUE);
        return view('admin.straight-league-approval.index', compact('data'));
    }
    public function WagerDisableStore(Request $request)
    {
        $data = $request->all();
        if (!empty($data['sport_id'])) {
            $sportIds = [];
            foreach ($data['sport_id'] as $k => $sportId) {
                $wagerData = WagerDisable::where('sport_league', $sportId)->first();
                if ($wagerData) {
                    $wagerData->delete();
                }

                $sportIds[] = [
                    'sport_league' => $sportId,
                    'league_name' => $data['sport_name'][$sportId],
                    'type' => 'active',
                ];

            }
            if (!empty($sportIds)) {
                WagerDisable::insert($sportIds);
            }
        }

        if (!empty($data['league_id'])) {
            $leagueIds = [];
            foreach ($data['league_id'] as $k => $leagueId) {
                $wagerDataLg = WagerDisable::where('sport_league', $leagueId)->first();
                if ($wagerDataLg) {
                    $wagerDataLg->delete();
                }
                $leagueIds[] = [
                    'sport_league' => $leagueId,
                    'league_name' => $data['league_name'][$leagueId],
                    'type' => 'active',
                ];
            }
            if (!empty($leagueIds)) {
                WagerDisable::insert($leagueIds);
            }
        }
        return redirect()->back()->with('success', ' Wager disable store successfully!');
    }
    public function WagerDisableDelete($sport_league)
    {
        $wagerDataLg = WagerDisable::where('sport_league', $sport_league)->first();
        if ($wagerDataLg) {
            $wagerDataLg->delete();
            return response()->json([
                'status' => 'success',
            ]);
        } else {
            return response()->json([
                'status' => 'failed',
            ]);
        }
    }


}
