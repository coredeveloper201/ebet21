<?php

namespace App\Http\Controllers\UserController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Elasticsearch\ClientBuilder;

class HorseBet extends Controller
{
    public function index(){
        $client = ClientBuilder::create()->build();
        $today_index=[
                                'index'=> 'horse_today',
                                'id'=> 'horse_today_id'
                           ];
        $tomorrow_index=[
                                'index'=> 'horse_tomorrow',
                                'id'=> 'horse_tomorrow_id'
                            ];
        $horsebettodaydata = simplexml_load_string(($client->getSource($today_index))['today_horse_data']);
        $horsebettomorrowdata=simplexml_load_string(($client->getSource($tomorrow_index))['tomorrow_horse_data']);;
        return view('frontend.horses',compact('horsebettodaydata','horsebettomorrowdata'));
    }
}
