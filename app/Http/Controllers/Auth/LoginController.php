<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\LoginLog;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Session;
use Validator;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request){

        //validate the form data
        $data = $request->all();
        $validator = Validator::make($data, [
            'email' => 'required',
            'password' => 'required|min:8'
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        // for admin
        if(Auth::guard('web')->attempt(['email' => $request->email, 'password' => $request->password, 'role' => 'admin'], $request->remember)){
            $this->loginLog();
            return redirect('admin/dashboard');
        } elseif (Auth::guard('web')->attempt(['username' => $request->email, 'password' => $request->password, 'role' => 'admin'], $request->remember)){
            $this->loginLog();
            return redirect('admin/dashboard');
        }

        // for agent
        elseif (Auth::guard('web')->attempt(['email' => $request->email, 'password' => $request->password, 'role' => 'agent'], $request->remember)){
            if (!empty(getMaintenanceStatus()) && getMaintenanceStatus()->status == 1){
                return redirect(route('maintenance'));
            }else{
                $this->loginLog();
                return redirect('admin/dashboard');
            }
        }elseif (Auth::guard('web')->attempt(['username' => $request->email, 'password' => $request->password, 'role' => 'agent'], $request->remember)){
            if (!empty(getMaintenanceStatus()) && getMaintenanceStatus()->status == 1){
                return redirect(route('maintenance'));
            }else{
                $this->loginLog();
                return redirect('admin/dashboard');
            }
        }

        //for subagent
        elseif (Auth::guard('web')->attempt(['email' => $request->email, 'password' => $request->password, 'role' => 'subagent'], $request->remember)){
            if (!empty(getMaintenanceStatus()) && getMaintenanceStatus()->status == 1){
                return redirect(route('maintenance'));
            }else{
                $this->loginLog();
                return redirect('admin/dashboard');
            }
        }elseif (Auth::guard('web')->attempt(['username' => $request->email, 'password' => $request->password, 'role' => 'subagent'], $request->remember)){
            if (!empty(getMaintenanceStatus()) && getMaintenanceStatus()->status == 1){
                return redirect(route('maintenance'));
            }else{
                $this->loginLog();
                return redirect('admin/dashboard');
            }
        }

        // for user
        elseif (Auth::guard('web')->attempt(['email' => $request->email, 'password' => $request->password, 'role' => 'user'], $request->remember)){
            if (!empty(getMaintenanceStatus()) && getMaintenanceStatus()->status == 1){
                return redirect(route('maintenance'));
            }else{
                $this->loginLog();
                return redirect('/straight-bet');
            }

        }elseif (Auth::guard('web')->attempt(['username' => $request->email, 'password' => $request->password, 'role' => 'user'], $request->remember)){

            if (!empty(getMaintenanceStatus()) && getMaintenanceStatus()->status == 1){
                return redirect(route('maintenance'));
            }else{
                $this->loginLog();
                return redirect('/straight-bet');
            }
        }
        //if unsuccessful, redirect back with the form data
        return redirect()->back()->with('Input', $request->only('email', 'remember'))->with('error', 'Invalid Email & Password.');
    }

    private function loginLog(){
        if (Auth::check()){
            if (!empty($_SERVER['HTTP_CLIENT_IP'])){
                $ip = $_SERVER['HTTP_CLIENT_IP'];
            }elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            }else{
                $ip = $_SERVER['REMOTE_ADDR'];
            }
            $loginLog = new LoginLog();
            $loginLog->user_id    = Auth::user()->id;
            $loginLog->ip_address = $ip;
            $loginLog->created_at = date("Y-m-d H:i:s");
            $loginLog->save();
            Session::flash('success', "Login successfully");
        }
    }
}
