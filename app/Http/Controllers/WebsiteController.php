<?php

namespace App\Http\Controllers;

use App\User;
use App\Ebet\BetResults;
use App\Models\LoginLog;
use App\Models\CreditLimit;
use App\Models\Transaction;
use Illuminate\Http\Request;
use App\Models\BettingDetail;
use App\Models\CasinoHistory;
use App\Models\ParlayEventId;
use App\Models\HorseBettingEvent;
use App\Models\HorseBettingDetail;
use Illuminate\Support\Facades\DB;
use App\Models\ParlayBettingDetail;
use Illuminate\Support\Facades\Auth;
use App\Ebet\Db\Elastic\StoreLiveOdds;
use App\Ebet\Db\Elastic\StoreStraightOdds;
use App\Ebet\Db\Elastic\StoreHorseOdds;

class WebsiteController extends Controller
{
    public function login()
    {
        return view('auth.login');
    }
    public function account()
    {
        $x = 0;
        $dataArray = [];
        for ($i = 0; $i < 6; $i++) {
            $previous_week = strtotime("-$i week +1 day");
            $start_week = strtotime("last sunday midnight", $previous_week);
            $end_week = strtotime("next saturday", $start_week);
            $start_week = date("Y-m-d", $start_week);
            $end_week = date("Y-m-d", $end_week);

            $dataArray[$x]['start_week'] = ($i == 0) ? 'Current Week' : date("m/d/Y", strtotime($start_week));

            for ($iOms = strtotime($start_week); $iOms <= strtotime($end_week); $iOms += 86400) {
                $iBetDet1 = BettingDetail::where('user_id', Auth::user()->id)->where('result', 1)->where(\DB::raw('DATE(created_at)'), date("Y-m-d", $iOms))->sum('betting_win');
                $iBetDet2 = BettingDetail::where('user_id', Auth::user()->id)->where('result', 2)->where('status', 2)->where(\DB::raw('DATE(created_at)'), date("Y-m-d", $iOms))->sum('risk_amount');
                $dataArray[$x]['date_val'][] = (object) [
                    'date' => date("Y-m-d", $iOms),
                    'value' => ($iBetDet1-$iBetDet2),
                ];
            }

            $iBetDet3  =    BettingDetail::where('user_id', Auth::user()->id)
                ->where('result', 0)
                ->where('status', 2)
                ->whereBetween(\DB::raw('DATE(created_at)'), [$start_week, $end_week])
                ->sum('risk_amount');
            $iBetDet4  =    ParlayEventId::where('user_id', Auth::user()->id)
                ->where('result', 0)
                ->whereBetween(\DB::raw('DATE(created_at)'), [$start_week, $end_week])
                ->sum('risk_amount');
            //dd($iBetDet3,$iBetDet4,$start_week,$end_week);
                $transWithdraw = Transaction::where('user_id', Auth::user()->id)->where('type', 1)->where('source', 1)->whereBetween(\DB::raw('DATE(created_at)'), [$start_week, $end_week])->sum('amount');

                $transDeposit = Transaction::where('user_id', Auth::user()->id)->where('type', 2)->where('source', 1)->whereBetween(\DB::raw('DATE(created_at)'), [$start_week, $end_week])->sum('amount');
                $pending_balance = \App\Models\UserWagerReport::where('user_id', Auth::user()->id)->whereBetween(\DB::raw('DATE(created_at)'), [$start_week, $end_week])->first();
                $pending = 0;
                $balance = 0;
                $casino = 0;
                if($pending_balance != null)
                {
                    $pending = $pending_balance->pending;
                    $balance = $pending_balance->balance;
                }
                $win_casion = CasinoHistory::where('user_id',Auth::user()->id)->where('type',2)->whereBetween(\DB::raw('DATE(created_at)'), [$start_week, $end_week])->sum('amount');
                $lose_casion = CasinoHistory::where('user_id',Auth::user()->id)->where('type',1)->whereBetween(\DB::raw('DATE(created_at)'), [$start_week, $end_week])->sum('amount');
                $casino = $win_casion - $lose_casion;
            $dataArray[$x]['pending'] = $pending;
            $dataArray[$x]['balance'] = $balance;
            $dataArray[$x]['casino'] = $casino;
            $dataArray[$x]['payment'] = ($transDeposit - $transWithdraw);
            $dataArray[$x]['start_week_date'] = $start_week;
            $dataArray[$x]['end_week_date'] = $end_week;
            $x++;
        }

        $x = 0;
        $parlayArray = [];
        $parlayRecords = ParlayEventId::where('user_id', Auth::user()->id)->where('result','!=',0)->orderBy('id', 'DESC')->get();

        foreach ($parlayRecords as $key => $val) {
            $parlayEventId = explode(',', $val->event_ids);
            $parlayEventType = explode(',', $val->types);
            $parlayArray[strtotime($val->created_at)][$x] = [
                'created_at' => $val->created_at,
                'ticket_id' => $val->ticket_id,
                'result'=>$val->result,
                'betting_type' => $val->betting_type,
                'risk_amount' => $val->risk_amount,
                'parlay_win_amount' => $val->parlay_win_amount,
                'count' => count($parlayEventId),
            ];
            for ($i = 0; $i < count($parlayEventId); $i++) {
                $parlayResults = ParlayBettingDetail::where('event_id', $parlayEventId[$i])
                    ->where('created_at', $val->created_at)
                    ->where('type', $parlayEventType[$i])
                    ->where('bet_type', $val->betting_type)
                    ->first();
                if (!empty($parlayResults))
                {
                    $parlayArray[strtotime($val->created_at)][$x]['detail'][] = [
                        'sport_league' => $parlayResults->sport_league,
                        'abrevation_name' => $parlayResults->abrevation_name,
                        'sport_name' => $parlayResults->sport_name,
                        'bet_extra_info'=>$parlayResults->bet_extra_info,
                        'event_date' => $parlayResults->event_date,
                        'type'=>$parlayResults->type,
                        'result'=>$parlayResults->result,
                        'points' => $parlayResults->points,
                        'betting_condition' => $parlayResults->betting_condition,
                        'created_at' => $parlayResults->created_at,
                    ];
                }
            }
            $x++;
        }
        $x=($x+1);
        $horsebettingHistories=HorseBettingDetail::where("user_id",Auth::user()->id)->where("result","!=",0)->orderBy('id',"DESC")->get();
        foreach($horsebettingHistories as $key=>$val)
        {
            $parlayArray[strtotime($val->created_at)][$x]=[
                'created_at'=>$val->created_at,
                'ticket_id'=>$val->ticket_id,
                'parlay_win_amount'=>$val->win_amount,
                "risk_amount"=>$val->risk_amount,
                "result"=>$val->result,
                "unique_id"=>$val->unique_id,
                "count"=>'',
                'betting_type'=>'horse'
            ];
            for ($i = 0; $i < 1; $i++)
            {
                $horseResults = HorseBettingDetail::where('user_id', Auth::user()->id)
                    ->where('created_at', $val->created_at)
                    ->where('unique_id', $val->unique_id)
                    ->first();
                if (!empty($horseResults))
                {
                    $parlayArray[strtotime($val->created_at)][$x]['horsedetail'][] = [
                        'tournament_name'=>$horseResults->tournament_name,
                        'race_name' => $horseResults->race_name,
                        'horse_name' => $horseResults->horse_name,
                        'race_type'=>$horseResults-> race_type,
                        'race_date'=>$horseResults->race_date,
                        'horse_number'=>$horseResults->horse_number,
                    ];
                }
            }
            $x++;
        }
        $x = ($x+1);
        $bettingHistories = BettingDetail::where('user_id', Auth::user()->id)->where('result','!=',0)->where('status', 2)->orderBy('id', 'DESC')->get();
        foreach ($bettingHistories as $key => $val) {
            $parlayArray[strtotime($val->created_at)][$x] = [
                'created_at' => $val->created_at,
                'ticket_id' => $val->ticket_id,
                'parlay_win_amount' => $val->betting_win,
                'risk_amount' => $val->risk_amount,
                'betting_condition_original' => $val->betting_condition_original,
                'betting_wager_type' => $val->type,
                'betting_type' => $val->bet_type,
                'sport_league' => $val->sport_league,
                'bet_extra_info' => $val->bet_extra_info,
                'result' => $val->result,
                'count' => '',
            ];
            $x++;
        }

        krsort($parlayArray);
        $dateRecords = json_decode(json_encode($parlayArray));
        $loginLogs = LoginLog::where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->limit(50)->get();
        //dd($dataArray);
        return view('frontend.account', compact('dataArray', 'loginLogs','dateRecords'));
    }

    public function accountWager(Request $request)
    {
        $x = 0;
        $parlayArray = [];
        $sql = ParlayEventId::where('user_id', Auth::user()->id)->where('result','!=',0)->orderBy('id', 'DESC');
        if ($request->start_week != '' && $request->end_week != '') {
            $sql->whereBetween(\DB::raw('DATE(created_at)'), [$request->start_week, $request->end_week]);
        } elseif ($request->date) {
            $sql->where(\DB::raw('DATE(created_at)'), $request->date);
        }
        $parlayRecords = $sql->get();

        foreach ($parlayRecords as $key => $val) {
            $parlayEventId = explode(',', $val->event_ids);
            $parlayEventType = explode(',', $val->types);
            $parlayArray[strtotime($val->created_at)][$x] = [
                'created_at' => $val->created_at,
                'ticket_id' => $val->ticket_id,
                'result'=>$val->result,
                'betting_type' => $val->betting_type,
                'risk_amount' => $val->risk_amount,
                'parlay_win_amount' => $val->parlay_win_amount,
                'count' => count($parlayEventId),
            ];
            for ($i = 0; $i < count($parlayEventId); $i++) {
                $parlayResults = ParlayBettingDetail::where('event_id', $parlayEventId[$i])
                    ->where('created_at', $val->created_at)
                    ->where('type', $parlayEventType[$i])
                    ->where('bet_type', $val->betting_type)
                    ->first();
                if (!empty($parlayResults))
                {
                    $parlayArray[strtotime($val->created_at)][$x]['detail'][] = [
                        'sport_league' => $parlayResults->sport_league,
                        'abrevation_name' => $parlayResults->abrevation_name,
                        'sport_name' => $parlayResults->sport_name,
                        'bet_extra_info'=>$parlayResults->bet_extra_info,
                        'event_date' => $parlayResults->event_date,
                        'type'=>$parlayResults->type,
                        'result'=>$parlayResults->result,
                        'points' => $parlayResults->points,
                        'betting_condition' => $parlayResults->betting_condition,
                        'created_at' => $parlayResults->created_at,
                    ];
                }
            }
            $x++;
        }
        $x=($x+1);
        $sql = HorseBettingDetail::where("user_id",Auth::user()->id)->where("result","!=",0)->orderBy('id',"DESC");
        if ($request->start_week != '' && $request->end_week != '') {
            $sql->whereBetween(\DB::raw('DATE(created_at)'), [$request->start_week, $request->end_week]);
        } elseif ($request->date) {
            $sql->where(\DB::raw('DATE(created_at)'), $request->date);
        }
        $horsebettingHistories = $sql->get();
        foreach($horsebettingHistories as $key=>$val)
        {
            $parlayArray[strtotime($val->created_at)][$x]=[
                'created_at'=>$val->created_at,
                'ticket_id'=>$val->ticket_id,
                'parlay_win_amount'=>$val->win_amount,
                "risk_amount"=>$val->risk_amount,
                "result"=>$val->result,
                "unique_id"=>$val->unique_id,
                "count"=>'',
                'betting_type'=>'horse'
            ];
            for ($i = 0; $i < 1; $i++)
            {
                $horseResults = HorseBettingDetail::where('user_id', Auth::user()->id)
                    ->where('created_at', $val->created_at)
                    ->where('unique_id', $val->unique_id)
                    ->first();
                if (!empty($horseResults))
                {
                    $parlayArray[strtotime($val->created_at)][$x]['horsedetail'][] = [
                        'tournament_name'=>$horseResults->tournament_name,
                        'race_name' => $horseResults->race_name,
                        'horse_name' => $horseResults->horse_name,
                        'race_type'=>$horseResults-> race_type,
                        'race_date'=>$horseResults->race_date,
                        'horse_number'=>$horseResults->horse_number,
                    ];
                }
            }
            $x++;
        }
        $x = ($x+1);
        $sql = BettingDetail::where('user_id', Auth::user()->id)->where('result','!=',0)->where('status', 2)->orderBy('id', 'DESC');
        if ($request->start_week != '' && $request->end_week != '') {
            $sql->whereBetween(\DB::raw('DATE(created_at)'), [$request->start_week, $request->end_week]);
        } elseif ($request->date) {
            $sql->where(\DB::raw('DATE(created_at)'), $request->date);
        }
        $bettingHistories = $sql->get();
        foreach ($bettingHistories as $key => $val) {
            $parlayArray[strtotime($val->created_at)][$x] = [
                'created_at' => $val->created_at,
                'ticket_id' => $val->ticket_id,
                'parlay_win_amount' => $val->betting_win,
                'risk_amount' => $val->risk_amount,
                'betting_condition_original' => $val->betting_condition_original,
                'betting_wager_type' => $val->type,
                'betting_type' => $val->bet_type,
                'sport_league' => $val->sport_league,
                'bet_extra_info' => $val->bet_extra_info,
                'result' => $val->result,
                'count' => '',
            ];
            $x++;
        }

        krsort($parlayArray);
        $dateRecords = json_decode(json_encode($parlayArray));

        // $sql = BettingDetail::where('user_id', Auth::user()->id)->orderBy('id', 'DESC');
        // if ($request->start_week != '' && $request->end_week != '') {
        //     $sql->whereBetween(\DB::raw('DATE(created_at)'), [$request->start_week, $request->end_week]);
        // } elseif ($request->date) {
        //     $sql->where(\DB::raw('DATE(created_at)'), $request->date);
        // }

        // $sql->where(function ($q) {
        //     $q->Where('result', 1)
        //         ->orWhere(function ($q) {
        //             $q->where('result', 2)->where('status', 2);
        //         });
        // });
        // $accountHistories = $sql->get();

        $start_week = $request->start_week;
        $end_week = $request->end_week;
        $date = $request->date;
        return view('frontend.account-wager', compact('dateRecords', 'start_week', 'end_week', 'date'));
    }

    public function pending(Request $request)
    {
        $x = 0;
        $parlayArray = [];
        $parlayRecords = ParlayEventId::where('user_id', Auth::user()->id)->where('result', 0)->orderBy('id', 'DESC')->get();
        foreach ($parlayRecords as $key => $val) {
            $parlayEventId = explode(',', $val->event_ids);
            $parlayEventType = explode(',', $val->types);
            $parlayArray[strtotime($val->created_at)][$x] = [
                'created_at' => $val->created_at,
                'ticket_id' => $val->ticket_id,
                'betting_type' => $val->betting_type,
                'risk_amount' => $val->risk_amount,
                'parlay_win_amount' => $val->parlay_win_amount,
                'count' => count($parlayEventId),
            ];
            for ($i = 0; $i < count($parlayEventId); $i++) {
                if(isset($parlayEventType[$i]))
                {
                    $parlayResults = ParlayBettingDetail::where('event_id', $parlayEventId[$i])
                    ->where('created_at', $val->created_at)
                    ->where('type', $parlayEventType[$i])
                    ->where('bet_type', $val->betting_type)
                    ->first();
                    if (!empty($parlayResults))
                    {
                        $parlayArray[strtotime($val->created_at)][$x]['detail'][] = [
                            'sport_league' => $parlayResults->sport_league,
                            'abrevation_name' => $parlayResults->abrevation_name,
                            'sport_name' => $parlayResults->sport_name,
                            'bet_extra_info'=>$parlayResults->bet_extra_info,
                            'event_date' => $parlayResults->event_date,
                            'type'=>$parlayResults->type,
                            'points' => $parlayResults->points,
                            'betting_condition' => $parlayResults->betting_condition,
                            'created_at' => $parlayResults->created_at,
                        ];
                    }
                }

            }
            $x++;
        }
        $x=($x+1);
        $horsebettingHistories=HorseBettingDetail::where("user_id",Auth::user()->id)->where("result",0)->orderBy('id',"DESC")->get();
        foreach($horsebettingHistories as $key=>$val)
        {
            $parlayArray[strtotime($val->created_at)][$x]=[
                'created_at'=>$val->created_at,
                'ticket_id'=>$val->ticket_id,
                'parlay_win_amount'=>$val->win_amount,
                "risk_amount"=>$val->risk_amount,
                "result"=>$val->result,
                "unique_id"=>$val->unique_id,
                "count"=>'',
                'betting_type'=>'horse'
            ];
            for ($i = 0; $i < 1; $i++)
            {
                $horseResults = HorseBettingDetail::where('user_id', Auth::user()->id)
                    ->where('created_at', $val->created_at)
                    ->where('unique_id', $val->unique_id)
                    ->first();
                if (!empty($horseResults))
                {
                    $parlayArray[strtotime($val->created_at)][$x]['horsedetail'][] = [
                        'tournament_name'=>$horseResults->tournament_name,
                        'race_name' => $horseResults->race_name,
                        'horse_name' => $horseResults->horse_name,
                        'race_type'=>$horseResults->race_type,
                        'race_date'=>$horseResults->race_date,
                        'horse_number'=>$horseResults->horse_number,
                    ];
                }
            }
            $x++;
        }
        $x = ($x+1);
        $bettingHistories = BettingDetail::where('user_id', Auth::user()->id)->where('result', 0)->where('status', 2)->orderBy('id', 'DESC')->get();
        foreach ($bettingHistories as $key => $val) {
            $parlayArray[strtotime($val->created_at)][$x] = [
                'created_at' => $val->created_at,
                'ticket_id' => $val->ticket_id,
                'parlay_win_amount' => $val->betting_win,
                'risk_amount' => $val->risk_amount,
                'betting_condition_original' => $val->betting_condition_original,
                'betting_type' => $val->bet_type,
                'betting_wager_type' => $val->type,
                'sport_league' => $val->sport_league,
                'bet_extra_info' => $val->bet_extra_info,
                'result' => $val->result,
                'count' => '',
            ];
            $x++;
        }

        krsort($parlayArray);
        $dateRecords = json_decode(json_encode($parlayArray));
        return view('frontend.pending', compact('dateRecords'));
    }

    public function pendingAjax()
    {
        $freeAmount = BettingDetail::where('user_id', Auth::user()->id)->where('result', 0)->where('status', 2)->where('free_play', 1)->sum('risk_amount');
        $bettingAmount = BettingDetail::where('user_id', Auth::user()->id)->where('result', 0)->where('status', 2)->where('free_play', 0)->sum('risk_amount');
        $parleyAmount = ParlayEventId::where('user_id', Auth::user()->id)->where('result', 0)->sum('risk_amount');
        $horseAmount=HorseBettingDetail::where("user_id",Auth::user()->id)->where('result',0)->sum('risk_amount');
        return response()->json([
            'free_pending' => number_format($freeAmount, 2),
            'pending' => number_format(($bettingAmount + $parleyAmount+$horseAmount), 2),
        ]);
    }

    // guide
    public function guide()
    {
        return view('frontend.guide');
    }

    // horses
    public function horses()
    {
        return view('frontend.horses');
    }

    // Live Casino with game
    public function casinoLive()
    {
        return view('frontend.live-casino');
    }

    // Casino with game
    public function casino()
    {
        return view('frontend.casino_game');
    }


    public function casinoGames($gameName)
    {
        if ($gameName == 'caribbean-stud-poker') {
            return view('frontend.casino.caribbean-stud-poker');
        } elseif ($gameName == 'jacks-or-better') {
            return view('frontend.casino.jacks-or-better');
        } elseif ($gameName == 'pai-gow-poker') {
            return view('frontend.casino.pai-gow-poker');
        } elseif ($gameName == '3card-poker') {
            return view('frontend.casino.3card-poker');
        } elseif ($gameName == 'baccarat') {
            return view('frontend.casino.baccarat');
        } elseif ($gameName == 'craps') {
            return view('frontend.casino.craps');
        } elseif ($gameName == 'red-dog') {
            return view('frontend.casino.red-dog');
        } elseif ($gameName == 'roulette') {
            return view('frontend.casino.roulette');
        } elseif ($gameName == 'joker-poker') {
            return view('frontend.casino.joker-poker');
        } elseif ($gameName == 'slot-ramses') {
            return view('frontend.casino.slot-ramses');
        } elseif ($gameName == 'slot-mr-chicken') {
            return view('frontend.casino.slot-mr-chicken');
        } elseif ($gameName == 'slot-arabian') {
            return view('frontend.casino.slot-arabian');
        } elseif ($gameName == 'slot-3d-soccer') {
            return view('frontend.casino.slot-3d-soccer');
        } elseif ($gameName == 'slot-space-adventure') {
            return view('frontend.casino.slot-space-adventure');
        } elseif ($gameName == 'slot-christmas') {
            return view('frontend.casino.slot-christmas');
        } elseif ($gameName == 'horse-racing') {
            return view('frontend.casino.horse-racing');
        } elseif ($gameName == 'bingo') {
            return view('frontend.casino.bingo');
        } elseif ($gameName == 'blackjack-21') {
            return view('frontend.casino.blackjack-21');
        } else {
            return 'This game not allowed';
        }
    }

    public function gamePlay($game)
    {
        $creditLimits = CreditLimit::where('user_id', Auth::user()->id)->first();
        if (empty($creditLimits)) {
            $creditLimits = (object) [
                'user_id' => 0,
                'all_min_amount' => 0,
                'all_max_amount' => 0,
                'straight_min_amount' => 0,
                'straight_max_amount' => 0,
                'casino_min_amount' => 0,
                'casino_max_amount' => 0
            ];
        }

        if ($game == 'jacks_or_better') {
            return view('game.jacks_or_better', compact('creditLimits'))->with('game_id', 1);
        } elseif ($game == 'pai_gow_poker') {
            return view('game.pai_gow_poker', compact('creditLimits'))->with('game_id', 2);
        } elseif ($game == 'caribbean_stud_poker') {
            return view('game.caribbean_stud_poker', compact('creditLimits'))->with('game_id', 3);
        } elseif ($game == '3card_poker') {
            return view('game.3card_poker', compact('creditLimits'))->with('game_id', 4);
        } elseif ($game == 'baccarat') {
            return view('game.baccarat', compact('creditLimits'))->with('game_id', 5);
        } elseif ($game == 'craps') {
            return view('game.craps', compact('creditLimits'))->with('game_id', 6);
        } elseif ($game == 'red_dog') {
            return view('game.red_dog', compact('creditLimits'))->with('game_id', 7);
        } elseif ($game == 'roulette') {
            return view('game.roulette', compact('creditLimits'))->with('game_id', 8);
        } elseif ($game == 'joker_poker') {
            return view('game.joker_poker', compact('creditLimits'))->with('game_id', 9);
        } elseif ($game == 'slot_ramses') {
            return view('game.slot_ramses', compact('creditLimits'))->with('game_id', 10);
        } elseif ($game == 'slot_mr_chicken') {
            return view('game.slot_mr_chicken', compact('creditLimits'))->with('game_id', 11);
        } elseif ($game == 'slot_arabian') {
            return view('game.slot_arabian', compact('creditLimits'))->with('game_id', 12);
        } elseif ($game == '3d_soccer_slot') {
            return view('game.3d_soccer_slot', compact('creditLimits'))->with('game_id', 13);
        } elseif ($game == 'slot_space_adventure') {
            return view('game.slot_space_adventure', compact('creditLimits'))->with('game_id', 14);
        } elseif ($game == 'horse_racing') {
            return view('game.horse_racing', compact('creditLimits'))->with('game_id', 15);
        } elseif ($game == 'bingo') {
            return view('game.bingo', compact('creditLimits'))->with('game_id', 16);
        } elseif ($game == 'blackjack_21') {
            return view('game.blackjack_21', compact('creditLimits'))->with('game_id', 17);
        } else {
            return view('game.not_found');
        }
    }

    public function casinoHistoryStore(Request $request)
    {
        $data = $request->all();
        $user = User::find(Auth::user()->id);
        $balance = $user->balance;
        $available = (float) $user->available;
        $palced_bid  = $data['place_bid'];
        $win_bid =  (float) $data['win_bid'];
        $game_id =   $data['game_id'];

        //dd($win_bid,$available,$game_id,$palced_bid);
        $db_balance = $balance;
        $db_available = $available;
        if($game_id =='12'
        || $game_id =='10'
        || $game_id =='11'
        || $game_id =='13'
        || $game_id =='14'
        || $game_id =='2'
        || $game_id =='15'
        || $game_id =='16')
        {
            if(($available - $palced_bid)  == $win_bid)
            {
                $type = 1;  //lost the bet
                $db_balance   = $balance - $palced_bid;
                $db_available = $available - $palced_bid;
            }
            else if($available   == $win_bid)
            {
                $type = 12;
            }
            else
            {
                $type = 2;  //win the bet
                $db_balance = $balance + $palced_bid;
                $db_available = $win_bid;
            }
        }
        else if($game_id =='3')
        {
            if( (($available - $palced_bid)  == $win_bid ))
            {
                $type = 1;  //lost the bet
                $db_balance   = $balance - $palced_bid;
                $db_available = $available - $palced_bid;
            }
            elseif($available -(($palced_bid/2)+ $palced_bid)  == $win_bid)
            {
                $type = 1;  //lost the bet   for this logic I know the question may arise, because I know this is n't right
                $db_balance   = $balance - ($palced_bid + ($palced_bid/2));
                $db_available = $available - ($palced_bid + ($palced_bid/2));
            }
            else if($available  == $win_bid)
            {
                $type = 12;
            }
            else
            {
                $type = 2;  //win the bet
                $db_balance = $balance + $palced_bid;
                $db_available = $win_bid;
            }
        }
        else if($game_id =='4')
        {
            if( (($available - $palced_bid)  == $win_bid ))
            {
                $type = 1;  //lost the bet
                $db_balance   = $balance - $palced_bid;
                $db_available = $available - $palced_bid;
            }
            elseif($available -($palced_bid*2)  == $win_bid)
            {
                $type = 1;  //lost the bet   for this logic I know the question may arise, because I know this is n't right
                $db_balance   = $balance - ($palced_bid*2);
                $db_available = $available - ($palced_bid*2);
            }
            else if($available  == $win_bid)
            {
                $type = 12;
            }
            else
            {
                $type = 2;  //win the bet
                $db_balance = $balance + $palced_bid;
                $db_available = $win_bid;
            }
        }

        $casino = new CasinoHistory();
        $casino->user_id     = $user->id;
        $casino->game_id     = $data['game_id'];
        $casino->random_id   = uniqid();
        $casino->amount      = $palced_bid;
        $casino->type        = $type;
        $casino->created_at  = date("Y-m-d H:i:s");
        if ($casino->save()) {
                $user->coins = $db_balance;
                $user->available_balance = $db_available;
            $user->save();
        }
    }
    public function getResult()
    {
        $dd = new BetResults();
        $dd();
    }

    public function storeStraightSport()
    {
       (new StoreStraightOdds())();
       (new StoreLiveOdds())();
       (new StoreHorseOdds())();
    }
    public function storeLiveSport()
    {
        $obj = new StoreLiveOdds();
        $obj();
    }
    public function storeHorse()
    {
        $obj=new StoreHorseOdds();
        $obj();
    }
}
