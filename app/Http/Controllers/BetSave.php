<?php

namespace App\Http\Controllers;

use App\User;
use App\Models\AgentRate;
use App\Models\AssignUser;
use App\Models\CreditLimit;
use App\Models\Transaction;
use Illuminate\Support\Str;
use App\Models\BettingEvent;
use Illuminate\Http\Request;
use App\Models\BettingDetail;

use App\Models\ParlayEventId;
use App\Models\UserWagerReport;
use App\Models\HorseBettingEvent;
use App\Models\HorseBettingDetail;
use App\Models\ParlayBettingEvent;
use App\Models\ParlayBettingDetail;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\GetMultiCurl;

class BetSave extends Controller
{
    public function index(Request $request)
    {
        if (!isset($request->status))
        {
            $objMultiCurl = new GetMultiCurl(json_decode($request->items));
            return response()->json(["changed" => $objMultiCurl->compareOdds()]);
        }
        else
        {
            return $request->status;
        }
    }

    public function checkBetPassword(Request $request)
    {
        $data = $this->checkPassword($request->password);
        return response()->json(['status' => $data]);
    }

    public function saveStraightBet(Request $request)
    {
       // dd($request->all());
        if (isset($request->status) && $request->status == "oka")
        {
            $user = Auth::user();
            $totalBettingPrice = 0;
            foreach ($request->items as $item) {
                $totalBettingPrice += $item['risk_amount'];
            }
            if (($request->freeplay == 1) && ($user->free_pay_balance < $totalBettingPrice))
            {
                return response()->json([
                    "status" => false,
                    "response" => "free pay balance doesn't enough"
                ]);
            }
            elseif ($user->available < $totalBettingPrice)
            {
                return response()->json([
                    "status" => false,
                    "response" => "user available balance doesn't enough"
                ]);
            }

            //Find User agent...
            $agentData = $this->agentRow();

            $tokenId = Str::random(10);
            $iTicketNumber = randomNumberGenerator(10);
            foreach ($request->items as $item) {
                if ($item['risk_amount'] != "" && $item['risk_amount'] != "undefined") {
                    $iFreePlay = ($request->freeplay == '') ? 0 : $request->freeplay;

                    $userCreditLimit = CreditLimit::where('user_id', Auth::user()->id)->first();

                    $isAway = ($item['away_name'] == $item['teamName']) ? 1 : 0;
                    $isHome = ($item['home_name'] == $item['teamName']) ? 1 : 0;

                    $teamId = ($item['home_name'] == $item['teamName']) ? $item['home_team_id'] : $item['away_team_id'];
                    $eventDate = date('Y-m-d', strtotime($item['date']));
                    $original = '';
                    if ($item['handicap'] != '') {
                        $original .= '(' . $item['handicap'] . ')';
                    }
                    $original .= ($item['odd'] > 0) ? '+' . $item['odd'] : $item['odd'];

                    $straight_event_save = [];
                    $type=0;

                   // dd($item);

                    if($item['bet_type'] != 'Props Bet')
                    {
                        if($item['subType']=="Spread")
                        {
                            $type=1;
                        }
                        else if($item['subType']=="Money Line")
                        {
                            $type=2;
                        }
                        elseif($item['subType'] =='Draw Line')
                        {
                            $type=123;
                            $original = '(Draw)';
                            $original .= ($item['odd'] > 0) ? '+' . $item['odd'] : $item['odd'];
                        }
                        else if($item['subType']=="Total")
                        {
                            $type=3;
                            if($item['row_line'] =='over')
                            {
                                $isHome =  1;
                                $isAway =  0;
                                $original = '('.$item['teamName'].')';
                                $original .= ($item['odd'] > 0) ? '+' . $item['odd'] : $item['odd'];
                            }
                            else if($item['row_line'] =='under')
                            {
                                $isHome =  0;
                                $isAway =  1;
                                $original = '('.$item['teamName'].')';
                                $original .= ($item['odd'] > 0) ? '+' . $item['odd'] : $item['odd'];
                            }
                        }
                    }
                    else {
                        //dd($item);
                        $type = '';
                        $type .= $item['sport_id'];

                        $betEI = $item['bet_extra_info'];
                        if($betEI['prop_bet_type'] == '1st half')         $type .='.1';
                        if($betEI['prop_bet_type'] == '1st Set')         $type .='.1';
                        else if($betEI['prop_bet_type'] == 'First five innings')         $type .='.1';
                        else if($betEI['prop_bet_type'] == 'First Inning')         $type .='.2';
                        else if($betEI['prop_bet_type'] == '2nd half')    $type .='.2';
                        else if($betEI['prop_bet_type'] == 'First Period')    $type .='.10';
                        else if($betEI['prop_bet_type'] == '1st Quarter table') $type .='.3';
                        else if($betEI['prop_bet_type'] == 'Over Time')   $type .='.4';
                        else if($betEI['prop_bet_type'] == 'Winning Margin') $type .='.5';
                        else if($betEI['prop_bet_type'] == 'Team Total - Home' || $betEI['prop_bet_type'] == 'Home Total Goals') $type .='.7';
                        else if($betEI['prop_bet_type'] == 'Team Total - Away' || $betEI['prop_bet_type'] == 'Away Total Goals') $type .='.8';
                        else if($betEI['prop_bet_type'] == 'Home team will score a Goal')     $type .='.14';
                        else if($betEI['prop_bet_type'] == 'Away team will score a Goal')     $type .='.15';

                        if($item['subType'] == 'First Half Goals') {
                            $type .='.17';
                            $original = '('.$item['teamName'].') '.$item['odd_sign'].$item['odd'];
                        }
                        else if($item['subType'] == 'Both Team To Score') {
                            $type .='.11';
                            $original = '('.$item['teamName'].') '.$item['odd_sign'].$item['odd'];
                        }
                        else if($item['subType'] == 'Correct Score') {
                            $type .='.18';
                            $original = '('.$item['teamName'].') '.$item['odd_sign'].$item['odd'];
                        }
                        else if($item['subType'] == 'Highest Scoring Half') {
                            $type .='.16';
                            $original = '('.$item['teamName'].') '.$item['odd_sign'].$item['odd'];
                        }
                        else if($item['subType'] == 'First Half Double Chance') {
                            $type .='.20';
                            $original = '('.$item['teamName'].') '.$item['odd_sign'].$item['odd'];
                        }
                        else if($item['subType'] == 'Exact Goal Numbers') {
                            $type .='.21';
                            $original = '('.$item['teamName'].') '.$item['odd_sign'].$item['odd'];
                        }
                        else if($item['subType'] == 'First Half Winner') {
                            $type .='.19';
                        }
                        else if($item['subType'] == 'Odd/Even') {
                            $type .='.9';
                            $original = '('.$item['teamName'].') '.$item['odd_sign'].$item['odd'];
                        }
                        else if($item['subType'] == 'Set Betting') {
                            $type .='.22';
                            $original = '('.$item['teamName'].') '.$item['odd_sign'].$item['odd'];
                        }
                        else if($item['subType'] == 'Tie Break') {
                            $type .='.23';
                            $original = '('.$item['teamName'].') '.$item['odd_sign'].$item['odd'];
                        }
                        else if($item['subType'] == 'Set Match') {
                            $type .='.24';
                            $original = '('.$item['teamName'].') '.$item['odd_sign'].$item['odd'];
                        }
                        else if($item['subType'] == '1st half') {
                            $type .='.1';
                        }
                        else if($item['subType'] == 'Highest Scoring Quarter') {
                            $type .='.6';
                            $original = '('.$item['teamName'].') '.$item['odd_sign'].$item['odd'];
                        }
                        else if($item['subType'] == '2nd half')    $type .='.2';
                        else if($item['subType'] == '1st Quarter') $type .='.3';
                        else if($item['subType'] == 'First five innings')         $type .='.1';
                        else if($item['subType'] == 'First Inning')        $type .='.2';
                        else if($item['subType'] == 'First Period')    $type .='.10';
                        else if($item['subType'] == '1st Set')    $type .='.1';

                        $tt = $item['slug_wager'];
                        if($tt[0] == 'Spread')
                            {
                                if($tt[1] == 'home')
                                {
                                    $isHome = 1;
                                    $isAway = 0;
                                }elseif($tt[1] == 'away')
                                {
                                    $isAway = 1;
                                    $isHome = 0;
                                }
                                $type .=".1";
                            }
                        elseif($tt[0] == 'Money Line')
                        {
                            if($tt[1] == 'home')
                            {
                                $isHome = 1;
                                $isAway = 0;
                            }elseif($tt[1] == 'away')
                            {
                                $isAway = 1;
                                $isHome = 0;
                            }
                            $type .=".2";
                        }
                        elseif($tt[0] == 'Total')
                        {
                            if($tt[1] == 'home')
                            {
                                $isHome = 1;
                                $isAway = 0;
                            }elseif($tt[1] == 'away')
                            {
                                $isAway = 1;
                                $isHome = 0;
                            }
                            $type .=".3";
                            $original = '('.$item['teamName'].') '.$item['odd_sign'].$item['odd'];
                        }
                        elseif($tt[0] == 'winning_margin_home')
                        {
                                $isHome = 1;
                                $isAway = 0;
                                $original .= ':('.json_encode($tt[1]).')';
                        }
                        elseif($tt[0] == 'winning_margin_away')
                        {
                                $isHome = 0;
                                $isAway = 1;
                                $original .= ':('.json_encode($tt[1]).')';
                        }

                    }

                     $straight_event_save['wager_type'] = $type;
                     $straight_event_save['wager_handicap'] = $item['handicap'];
                     $straight_event_save['team_type'] = ($isHome == 1) ? $isHome : 2 ; //1 home 2 away

                    $dataArr = [
                        'is_away' => $isAway,
                        'sport_league' => json_encode([$item['sport_id'], $item['league_name']]),
                        'is_home' => $isHome,
                        'token_id' => $tokenId,
                        'o_and_u' => 0,
                        'event_id' => $item['event_id'],
                        'team_id' => $teamId,
                        'sport_name' => $item['sport_id'],
                        'betting_win' => abs($item['win_amount']),
                        'betting_condition' => $item['odd'],
                        'betting_condition_original' => $original,
                        'risk_amount' => abs($item['risk_amount']),
                        'betting_amount' => $item['risk_amount'],
                        'event_date' => $eventDate,
                        'wager' => 1,
                        'type' => $type,
                        'ticket_id' => $iTicketNumber,
                        'user_id' => Auth::user()->id,
                        'created_at' => date("Y-m-d H:i:s"),
                        'status' => 2,
                        'user_current_credit_limit' => $user->credit_limit,
                        'user_current_max_limit' => $userCreditLimit->all_max_amount,
                        'user_current_balance' => $user->available_balance,
                        'result' => 0,
                        'free_play' => $iFreePlay,
                        'bet_type' => $item['bet_type'],
                        'bet_extra_info' => json_encode($item['bet_extra_info'])
                    ];
                    //dd($dataArr);
                    $this->dataSubmit($dataArr, $agentData,$straight_event_save);
                }
            }

            //Agent Weekly Balance Add...
            $this->addWeeklyBalance(1, $agentData);

            $userRow = User::find(Auth::user()->id);
            if ($request->freeplay == 1) {
                $updateData['free_pay_balance'] = ($userRow->free_pay_balance - $totalBettingPrice);
            } else {
                 $updateData['available_balance'] = $userRow->available_balance - $totalBettingPrice;
            }
            $userRow->update($updateData);

            $bettingRisk = BettingDetail::where('user_id', Auth::user()->id)->where('result', 0)->where('status', 2)->where('free_play', 0)->sum('risk_amount');
            $parlayRisk = ParlayEventId::where('user_id', Auth::user()->id)->where('result', 0)->sum('risk_amount');
            $pendingAmount = ($bettingRisk + $parlayRisk);
            return  response()->json([
                "status" => true,
                "user" => [
                    "available_balance" => number_format(floor($userRow->available)),
                    "pending_amount" => number_format($pendingAmount, 2)
                ]
            ]);
        }
    }

    public function saveLiveBet(Request $request)
    {

        if (isset($request->status) && $request->status == "oka") {
            $user = Auth::user();
            $totalBettingPrice = 0;
            foreach ($request->items as $item)
            {
                $totalBettingPrice += $item['risk_stake_slip'];
            }
            if (($request->freeplay == 1) && ($user->free_pay_balance < $totalBettingPrice))
            {
                return response()->json
                ([
                    "status" => false,
                    "response" => "free pay balance doesn't enough"
                ]);
            } elseif ($user->available < $totalBettingPrice) {
                return response()->json([
                    "status" => false,
                    "response" => "user available balance doesn't enough"
                ]);
            }

            //Find User agent...
            $agentData = $this->agentRow();

            $tokenId = Str::random(10);
            $iTicketNumber = randomNumberGenerator(10);
            foreach ($request->items as $item) {
                if ($item['risk_stake_slip'] != "" && $item['risk_stake_slip'] != "undefined") {
                    $iFreePlay = ($request->freeplay == '') ? 0 : $request->freeplay;
                    $userCreditLimit = CreditLimit::where('user_id', Auth::user()->id)->first();
                    $type= isset($item['column'])?$item['column']:-1;

                    $dataArr = [
                        'is_away' => $item['is_away'],
                        'sport_league' => $item['sport_league'],
                        'is_home' => $item['is_home'],
                        'token_id' => $tokenId,
                        'o_and_u' => 0,
                        'event_id' => $item['even_id'],
                        'team_id' => $item['team_id'],
                        'sport_name' => $item['sport_id'],
                        'betting_win' => abs($item['risk_win_slip']),
                        'betting_condition' => $item['scores'],
                        'betting_condition_original' =>  $item['original_money'],
                        'risk_amount' => abs($item['risk_stake_slip']),
                        'betting_amount' => $item['risk_stake_slip'],
                        'event_date' =>  date('Y-m-d H:i:s',time()),
                        'wager' => 1,
                        'type' => $type,
                        'ticket_id' => $iTicketNumber,
                        'user_id' => Auth::user()->id,
                        'created_at' => date("Y-m-d H:i:s"),
                        'status' => 2,
                        'user_current_credit_limit' => $user->credit_limit,
                        'user_current_max_limit' => $userCreditLimit->all_max_amount,
                        'user_current_balance' => $user->available_balance,
                        'result' => 0,
                        'free_play' => $iFreePlay,
                        'bet_type' => $item['bet_type'],
                        'bet_extra_info' => $item['bet_extra_info']
                    ];
                  $this->dataSubmit($dataArr, $agentData);
                }
            }

            //Agent Weekly Balance Add...
            $this->addWeeklyBalance(1, $agentData);

            $userRow = User::find(Auth::user()->id);
            if ($request->freeplay == 1)
            {
                $updateData['free_pay_balance'] = ($userRow->free_pay_balance - $totalBettingPrice);
            }
            else
            {
                $updateData['available_balance'] = ($userRow->available_balance - $totalBettingPrice);
            }
            $userRow->update($updateData);
            $bettingRisk = BettingDetail::where('user_id', Auth::user()->id)->where('result', 0)->where('status', 2)->where('free_play', 0)->sum('risk_amount');
            $parlayRisk = ParlayEventId::where('user_id', Auth::user()->id)->where('result', 0)->sum('risk_amount');
            $pendingAmount = ($bettingRisk + $parlayRisk);
            return  response()->json(["status" => true,
            "user" => [
                "available_balance" => number_format(floor($userRow->available_balance)),
                "pending_amount" => number_format($pendingAmount, 2)
                ]
            ], 200);
        }
    }

    public function saveParlayBet(Request $request)
    {
        //dd($request->all());
        if (isset($request->status) && $request->status == "parlay")
        {
            $user = Auth::user();
            $iTicketNumber = randomNumberGenerator(10);
            $totalBettingPrice = $request->risk_amount;
            $parlayrisk_amount=$request->risk_amount;
            $parlaywin_amount=$request->win_amount;
            $userRow=User::find(Auth::user()->id);
            $updateData['available_balance']=($userRow->available_balance-$parlayrisk_amount);
            $userRow->update($updateData);
            if (($request->freeplay == 1) && ($user->free_pay_balance < $totalBettingPrice))
            {
                return response()->json([
                    "status" => false,
                    "response" => "free pay balance doesn't enough"
                ]);
            }
            elseif ($user->available < $totalBettingPrice)
            {
                return response()->json([
                    "status" => false,
                    "response" => "user available balance doesn't enough"
                ]);
            }

            //Find User agent...
            $agentData = $this->agentRow();
            $event_ids="";
            $types="";
            foreach ($request->items as $item) {
               //dd($item);
                if ($parlayrisk_amount != "" && $parlayrisk_amount!= null) {
                    $ticket_id=$iTicketNumber;
                    $iFreePlay = ($request->freeplay == '') ? 0 : $request->freeplay;
                    $bettype="parlay";
                    if($event_ids=="")
                    {
                        $event_ids=$item['event_id'];
                    }
                    else if($event_ids!="")
                    {
                        $event_ids=$event_ids.",".$item['event_id'];
                    }
                    $userCreditLimit = CreditLimit::where('user_id', Auth::user()->id)->first();
                    $isAway = ($item['away_name'] == $item['teamName']) ? 1 : 0;
                    $isHome = ($item['home_name'] == $item['teamName']) ? 1 : 0;
                    $eventDate = date('Y-m-d H:i:s', strtotime($item['date']));
                    $original = '';
                    $type=0;
                    if($item['subType']=="Spread")
                    {
                        $type=1;
                    }
                    else if($item['subType']=="Money Line")
                    {
                        $type=2;

                    }
                    else if($item['subType']=="Total")
                    {
                        $type=3;
                        $item['handicap'] = $item['teamName'];
                    }
                    else if($item['subType']=="Draw Line")
                    {
                        $type=123;
                        $item['handicap'] ='Draw';
                    }
                    if ($item['handicap'] != '') {
                        $original .= '(' . $item['handicap'] . ')';
                    }
                    if($types=="")
                    {
                        $types=$type;
                    }
                    else if($types!="")
                    {
                        $types=$types.",".$type;
                    }
                    $original .= ($item['odd'] > 0) ? '+' . $item['odd'] : $item['odd'];
                    $dataArr = [
                        'ticket_id'=>$ticket_id,
                        'is_away' => $isAway,
                        'sport_league' => json_encode([$item['sport_id'], $item['league_name']]),
                        'is_home' => $isHome,
                        'points'=>$item['handicap'],
                        'event_id' => $item['event_id'],
                        'sport_name' => $item['sport_id'],
                        'betting_condition' => $item['odd'],
                        'betting_condition_original' => $original,
                        'risk_amount' => abs($parlayrisk_amount),
                        'parlay_win_amount' => abs($parlaywin_amount),
                        'event_date' => $eventDate,
                        'type' => $type,
                        'user_id' => Auth::user()->id,
                        'created_at' => date("Y-m-d H:i:s"),
                        'result' => 0,
                        'free_play' => $iFreePlay,
                        'bet_type' => $bettype,
                        'bet_extra_info' => json_encode($item['bet_extra_info'])
                    ];
                    //dd($dataArr);
                    $this->parlaydataSubmit($dataArr,$agentData,$event_ids,$types);
                }
            }
            $this->parlayEvent_idsSubmit($dataArr,$agentData,$event_ids,$types);
            $userRow=User::find(Auth::user()->id);
            $bettingRisk = BettingDetail::where('user_id', Auth::user()->id)->where('result', 0)->where('status', 2)->where('free_play', 0)->sum('risk_amount');
            $parlayRisk = ParlayEventId::where('user_id', Auth::user()->id)->where('result', 0)->sum('risk_amount');
            $horseRisk=HorseBettingDetail::where("user_id",Auth::user()->id)->where('result',0)->sum('risk_amount');
            $pendingAmount = ($bettingRisk + $parlayRisk+$horseRisk);
            return response()->json([
                "status" => true,
                "user" => [
                    "available_balance" => number_format(floor($userRow->available)),
                    "pending_amount" => number_format($pendingAmount, 2)
                ]
            ]);
        }
        if (isset($request->status) && $request->status == "tworound")
        {
            $user = Auth::user();
            $iTicketNumber = randomNumberGenerator(10);
            $totalBettingPrice = $request->risk_amount;
            $parlayrisk_amount=$request->risk_amount;
            $parlaywin_amount=$request->win_amount;
            $userRow=User::find(Auth::user()->id);
            $updateData['available_balance']=($userRow->available_balance-$parlayrisk_amount);
            $userRow->update($updateData);
            if (($request->freeplay == 1) && ($user->free_pay_balance < $totalBettingPrice))
            {
                return response()->json([
                    "status" => false,
                    "response" => "free pay balance doesn't enough"
                ]);
            }
            elseif ($user->available < $totalBettingPrice)
            {
                return response()->json([
                    "status" => false,
                    "response" => "user available balance doesn't enough"
                ]);
            }

            //Find User agent...
            $agentData = $this->agentRow();
            $event_ids="";
            $types="";
            foreach ($request->items as $item) {
                if ($parlayrisk_amount != "" && $parlayrisk_amount!= null) {
                    $ticket_id=$iTicketNumber;
                    $iFreePlay = ($request->freeplay == '') ? 0 : $request->freeplay;
                    $bettype="tworound";
                    if($event_ids=="")
                    {
                        $event_ids=$item['event_id'];
                    }
                    else if($event_ids!="")
                    {
                        $event_ids=$event_ids.",".$item['event_id'];
                    }
                    $userCreditLimit = CreditLimit::where('user_id', Auth::user()->id)->first();
                    $isAway = ($item['away_name'] == $item['teamName']) ? 1 : 0;
                    $isHome = ($item['home_name'] == $item['teamName']) ? 1 : 0;
                    $eventDate = date('Y-m-d H:i:s', strtotime($item['date']));
                    $original = '';
                    $type=0;
                    if($item['subType']=="Spread")
                    {
                        $type=1;
                    }
                    else if($item['subType']=="Money Line")
                    {
                        $type=2;

                    }
                    else if($item['subType']=="Total")
                    {
                        $type=3;
                        $item['handicap'] = $item['teamName'];
                    }
                    else if($item['subType']=="Draw Line")
                    {
                        $type=123;
                        $item['handicap'] ='Draw';
                    }
                    if ($item['handicap'] != '') {
                        $original .= '(' . $item['handicap'] . ')';
                    }
                    if($types=="")
                    {
                        $types=$type;
                    }
                    else if($types!="")
                    {
                        $types=$types.",".$type;
                    }
                    $original .= ($item['odd'] > 0) ? '+' . $item['odd'] : $item['odd'];
                    $dataArr = [
                        'ticket_id'=>$ticket_id,
                        'is_away' => $isAway,
                        'sport_league' => json_encode([$item['sport_id'], $item['league_name']]),
                        'is_home' => $isHome,
                        'points'=>$item['handicap'],
                        'event_id' => $item['event_id'],
                        'sport_name' => $item['sport_id'],
                        'betting_condition' => $item['odd'],
                        'betting_condition_original' => $original,
                        'risk_amount' => abs($parlayrisk_amount),
                        'parlay_win_amount' => abs($parlaywin_amount),
                        'event_date' => $eventDate,
                        'type' => $type,
                        'user_id' => Auth::user()->id,
                        'created_at' => date("Y-m-d H:i:s"),
                        'result' => 0,
                        'free_play' => $iFreePlay,
                        'bet_type' => $bettype,
                        'bet_extra_info' => json_encode($item['bet_extra_info'])
                    ];
                    $this->parlaydataSubmit($dataArr,$agentData,$event_ids,$types);
                }
            }
            $this->parlayEvent_idsSubmit($dataArr,$agentData,$event_ids,$types);
            $userRow=User::find(Auth::user()->id);
            $bettingRisk = BettingDetail::where('user_id', Auth::user()->id)->where('result', 0)->where('status', 2)->where('free_play', 0)->sum('risk_amount');
            $parlayRisk = ParlayEventId::where('user_id', Auth::user()->id)->where('result', 0)->sum('risk_amount');
            $pendingAmount = ($bettingRisk + $parlayRisk);
            return response()->json([
                "status" => true,
                "user" => [
                    "available_balance" => number_format(floor($userRow->available_balance)),
                    "pending_amount" => number_format($pendingAmount, 2)
                ]
            ]);
        }
        if (isset($request->status) && $request->status == "threeround")
        {
            $user = Auth::user();
            $iTicketNumber = randomNumberGenerator(10);
            $totalBettingPrice = $request->risk_amount;
            $parlayrisk_amount=$request->risk_amount;
            $parlaywin_amount=$request->win_amount;
            $userRow=User::find(Auth::user()->id);
            $updateData['available_balance']=($userRow->available_balance-$parlayrisk_amount);
            $userRow->update($updateData);
            if (($request->freeplay == 1) && ($user->free_pay_balance < $totalBettingPrice))
            {
                return response()->json([
                    "status" => false,
                    "response" => "free pay balance doesn't enough"
                ]);
            }
            elseif ($user->available < $totalBettingPrice)
            {
                return response()->json([
                    "status" => false,
                    "response" => "user available balance doesn't enough"
                ]);
            }

            //Find User agent...
            $agentData = $this->agentRow();
            $event_ids="";
            $types="";
            foreach ($request->items as $item) {
                if ($parlayrisk_amount != "" && $parlayrisk_amount!= null) {
                    $ticket_id=$iTicketNumber;
                    $iFreePlay = ($request->freeplay == '') ? 0 : $request->freeplay;
                    $bettype="threeround";
                    if($event_ids=="")
                    {
                        $event_ids=$item['event_id'];
                    }
                    else if($event_ids!="")
                    {
                        $event_ids=$event_ids.",".$item['event_id'];
                    }
                    $userCreditLimit = CreditLimit::where('user_id', Auth::user()->id)->first();
                    $isAway = ($item['away_name'] == $item['teamName']) ? 1 : 0;
                    $isHome = ($item['home_name'] == $item['teamName']) ? 1 : 0;
                    $eventDate = date('Y-m-d H:i:s', strtotime($item['date']));
                    $original = '';
                    $type=0;
                    if($item['subType']=="Spread")
                    {
                        $type=1;
                    }
                    else if($item['subType']=="Money Line")
                    {
                        $type=2;

                    }
                    else if($item['subType']=="Total")
                    {
                        $type=3;
                        $item['handicap'] = $item['teamName'];
                    }
                    else if($item['subType']=="Draw Line")
                    {
                        $type=123;
                        $item['handicap'] ='Draw';
                    }
                    if ($item['handicap'] != '') {
                        $original .= '(' . $item['handicap'] . ')';
                    }
                    if($types=="")
                    {
                        $types=$type;
                    }
                    else if($types!="")
                    {
                        $types=$types.",".$type;
                    }
                    $original .= ($item['odd'] > 0) ? '+' . $item['odd'] : $item['odd'];
                    $dataArr = [
                        'ticket_id'=>$ticket_id,
                        'is_away' => $isAway,
                        'sport_league' => json_encode([$item['sport_id'], $item['league_name']]),
                        'is_home' => $isHome,
                        'points'=>$item['handicap'],
                        'event_id' => $item['event_id'],
                        'sport_name' => $item['sport_id'],
                        'betting_condition' => $item['odd'],
                        'betting_condition_original' => $original,
                        'risk_amount' => abs($parlayrisk_amount),
                        'parlay_win_amount' => abs($parlaywin_amount),
                        'event_date' => $eventDate,
                        'type' => $type,
                        'user_id' => Auth::user()->id,
                        'created_at' => date("Y-m-d H:i:s"),
                        'result' => 0,
                        'free_play' => $iFreePlay,
                        'bet_type' => $bettype,
                        'bet_extra_info' => json_encode($item['bet_extra_info'])
                    ];
                    $this->parlaydataSubmit($dataArr,$agentData,$event_ids,$types);
                }
            }
            $this->parlayEvent_idsSubmit($dataArr,$agentData,$event_ids,$types);
            $userRow=User::find(Auth::user()->id);
            $bettingRisk = BettingDetail::where('user_id', Auth::user()->id)->where('result', 0)->where('status', 2)->where('free_play', 0)->sum('risk_amount');
            $parlayRisk = ParlayEventId::where('user_id', Auth::user()->id)->where('result', 0)->sum('risk_amount');
            $pendingAmount = ($bettingRisk + $parlayRisk);
            return response()->json([
                "status" => true,
                "user" => [
                    "available_balance" => number_format(floor($userRow->available_balance)),
                    "pending_amount" => number_format($pendingAmount, 2)
                ]
            ]);
        }
        if(isset($request->status)&&$request->status=="fourround")
        {
            $user = Auth::user();
            $iTicketNumber = randomNumberGenerator(10);
            $totalBettingPrice = $request->risk_amount;
            $parlayrisk_amount=$request->risk_amount;
            $parlaywin_amount=$request->win_amount;
            $userRow=User::find(Auth::user()->id);
            $updateData['available_balance']=($userRow->available_balance-$parlayrisk_amount);
            $userRow->update($updateData);
            if (($request->freeplay == 1) && ($user->free_pay_balance < $totalBettingPrice))
            {
                return response()->json([
                    "status" => false,
                    "response" => "free pay balance doesn't enough"
                ]);
            }
            elseif ($user->available < $totalBettingPrice)
            {
                return response()->json([
                    "status" => false,
                    "response" => "user available balance doesn't enough"
                ]);
            }

            //Find User agent...
            $agentData = $this->agentRow();
            $event_ids="";
            $types="";
            foreach ($request->items as $item) {
                if ($parlayrisk_amount != "" && $parlayrisk_amount!= null) {
                    $ticket_id=$iTicketNumber;
                    $iFreePlay = ($request->freeplay == '') ? 0 : $request->freeplay;
                    $bettype="fourround";
                    if($event_ids=="")
                    {
                        $event_ids=$item['event_id'];
                    }
                    else if($event_ids!="")
                    {
                        $event_ids=$event_ids.",".$item['event_id'];
                    }
                    $userCreditLimit = CreditLimit::where('user_id', Auth::user()->id)->first();
                    $isAway = ($item['away_name'] == $item['teamName']) ? 1 : 0;
                    $isHome = ($item['home_name'] == $item['teamName']) ? 1 : 0;
                    $eventDate = date('Y-m-d H:i:s', strtotime($item['date']));
                    $original = '';
                    $type=0;
                    if($item['subType']=="Spread")
                    {
                        $type=1;
                    }
                    else if($item['subType']=="Money Line")
                    {
                        $type=2;

                    }
                    else if($item['subType']=="Total")
                    {
                        $type=3;
                        $item['handicap'] = $item['teamName'];
                    }
                    else if($item['subType']=="Draw Line")
                    {
                        $type=123;
                        $item['handicap'] ='Draw';
                    }
                    if ($item['handicap'] != '') {
                        $original .= '(' . $item['handicap'] . ')';
                    }
                    if($types=="")
                    {
                        $types=$type;
                    }
                    else if($types!="")
                    {
                        $types=$types.",".$type;
                    }
                    $original .= ($item['odd'] > 0) ? '+' . $item['odd'] : $item['odd'];
                    $dataArr = [
                        'ticket_id'=>$ticket_id,
                        'is_away' => $isAway,
                        'sport_league' => json_encode([$item['sport_id'], $item['league_name']]),
                        'is_home' => $isHome,
                        'points'=>$item['handicap'],
                        'event_id' => $item['event_id'],
                        'sport_name' => $item['sport_id'],
                        'betting_condition' => $item['odd'],
                        'betting_condition_original' => $original,
                        'risk_amount' => abs($parlayrisk_amount),
                        'parlay_win_amount' => abs($parlaywin_amount),
                        'event_date' => $eventDate,
                        'type' => $type,
                        'user_id' => Auth::user()->id,
                        'created_at' => date("Y-m-d H:i:s"),
                        'result' => 0,
                        'free_play' => $iFreePlay,
                        'bet_type' => $bettype,
                        'bet_extra_info' => json_encode($item['bet_extra_info'])
                    ];
                    $this->parlaydataSubmit($dataArr,$agentData,$event_ids,$types);
                }
            }
            $this->parlayEvent_idsSubmit($dataArr,$agentData,$event_ids,$types);
            $userRow=User::find(Auth::user()->id);
            $bettingRisk = BettingDetail::where('user_id', Auth::user()->id)->where('result', 0)->where('status', 2)->where('free_play', 0)->sum('risk_amount');
            $parlayRisk = ParlayEventId::where('user_id', Auth::user()->id)->where('result', 0)->sum('risk_amount');
            $pendingAmount = ($bettingRisk + $parlayRisk);
            return response()->json([
                "status" => true,
                "user" => [
                    "available_balance" => number_format(floor($userRow->available_balance)),
                    "pending_amount" => number_format($pendingAmount, 2)
                ]
            ]);
        }
        if(isset($request->status)&&$request->status=="teaser")
        {
            $user = Auth::user();
            $iTicketNumber = randomNumberGenerator(10);
            $totalBettingPrice = $request->risk_amount;
            $teaserrisk_amount=$request->risk_amount;
            $teaserwin_amount=$request->win_amount;
            //*User available change*//
            $userRow=User::find(Auth::user()->id);
            $updateData['available_balance']=($userRow->available_balance-$teaserrisk_amount);
            $userRow->update($updateData);
            if (($request->freeplay == 1) && ($user->free_pay_balance < $totalBettingPrice))
            {
                return response()->json([
                    "status" => false,
                    "response" => "free pay balance doesn't enough"
                ]);
            }
            elseif ($user->available < $totalBettingPrice)
            {
                return response()->json([
                    "status" => false,
                    "response" => "user available balance doesn't enough"
                ]);
            }

            //Find User agent...
            $agentData = $this->agentRow();
            $event_ids="";
            $types="";
            foreach ($request->items as $item) {
                if ($teaserrisk_amount != "" && $teaserrisk_amount!= null) {
                    $ticket_id=$iTicketNumber;
                    $iFreePlay = ($request->freeplay == '') ? 0 : $request->freeplay;
                    $bettype="teaser";
                    if($event_ids=="")
                    {
                        $event_ids=$item['event_id'];
                    }
                    else if($event_ids!="")
                    {
                        $event_ids=$event_ids.",".$item['event_id'];
                    }
                    $userCreditLimit = CreditLimit::where('user_id', Auth::user()->id)->first();
                    $isAway = ($item['away_name'] == $item['teamName']) ? 1 : 0;
                    $isHome = ($item['home_name'] == $item['teamName']) ? 1 : 0;
                    $eventDate = date('Y-m-d H:i:s', strtotime($item['date']));
                    $original = '';
                    $type=0;
                    if($item['subType']=="Spread")
                    {
                        $type=1;
                    }
                    else if($item['subType']=="Money Line")
                    {
                        $type=2;
                    }
                    else if($item['subType']=="Total")
                    {
                        $type=3;
                        $item['handicap'] = $item['teamName'];
                        if($item['slug_wager'][1] == 'under')
                        {
                            $item['handicap'] ='Under '.$item['handicap'];
                        }
                        else
                        {
                            $item['handicap'] ='Over '.$item['handicap'];
                        }
                    }
                    else if($item['subType']=="Draw Line")
                    {
                        $type=123;
                        $item['handicap'] ='Draw';
                    }

                    if ($item['handicap'] != '') {
                        $original .= '(' . $item['handicap'] . ')';
                    }
                    if($types=="")
                    {
                        $types=$type;
                    }
                    else if($types!="")
                    {
                        $types=$types.",".$type;
                    }
                    $original .= ($item['odd'] > 0) ? '+' . $item['odd'] : $item['odd'];
                    $dataArr = [
                        'ticket_id'=>$ticket_id,
                        'is_away' => $isAway,
                        'sport_league' => json_encode([$item['sport_id'], $item['league_name']]),
                        'is_home' => $isHome,
                        'points'=>$item['handicap'],
                        'event_id' => $item['event_id'],
                        'sport_name' => $item['sport_id'],
                        'betting_condition' => $item['odd'],
                        'betting_condition_original' => $original,
                        'risk_amount' => abs($teaserrisk_amount),
                        'parlay_win_amount' => abs($teaserwin_amount),
                        'event_date' => $eventDate,
                        'type' => $type,
                        'user_id' => Auth::user()->id,
                        'created_at' => date("Y-m-d H:i:s"),
                        'result' => 0,
                        'free_play' => $iFreePlay,
                        'bet_type' => $bettype,
                        'bet_extra_info' =>  json_encode($item['bet_extra_info'])
                    ];
                    $this->parlaydataSubmit($dataArr,$agentData,$event_ids,$types);
                }
            }
            $this->parlayEvent_idsSubmit($dataArr,$agentData,$event_ids,$types);
        }
        if(isset($request->status)&&$request->status=="ifbet"){
            $user = Auth::user();
            $iTicketNumber = randomNumberGenerator(10);
            $totalBettingPrice = $request->risk_amount;
            $ifbetrisk_amount=$request->risk_amount;
            $ifbetwin_amount=$request->win_amount;
            $userRow=User::find(Auth::user()->id);
            $updateData['available_balance']=($userRow->available_balance-$ifbetrisk_amount);
            $userRow->update($updateData);
            if (($request->freeplay == 1) && ($user->free_pay_balance < $totalBettingPrice))
            {
                return response()->json([
                    "status" => false,
                    "response" => "free pay balance doesn't enough"
                ]);
            }
            elseif ($user->available < $totalBettingPrice)
            {
                return response()->json([
                    "status" => false,
                    "response" => "user available balance doesn't enough"
                ]);
            }
            //Find User agent...
            $agentData = $this->agentRow();
            $event_ids="";
            $types="";
            foreach ($request->items as $item) {
                if ($ifbetrisk_amount != "" && $ifbetrisk_amount!= null) {
                    $ticket_id=$iTicketNumber;
                    $iFreePlay = ($request->freeplay == '') ? 0 : $request->freeplay;
                    $bettype="ifbet";
                    if($event_ids=="")
                    {
                        $event_ids=$item['event_id'];
                    }
                    else if($event_ids!="")
                    {
                        $event_ids=$event_ids.",".$item['event_id'];
                    }
                    $userCreditLimit = CreditLimit::where('user_id', Auth::user()->id)->first();
                    $isAway = ($item['away_name'] == $item['teamName']) ? 1 : 0;
                    $isHome = ($item['home_name'] == $item['teamName']) ? 1 : 0;
                    $eventDate = date('Y-m-d H:i:s', strtotime($item['date']));
                    $original = '';
                    $type=0;
                    if($item['subType']=="Spread")
                    {
                        $type=1;
                    }
                    else if($item['subType']=="Money Line")
                    {
                        $type=2;
                    }
                    else if($item['subType']=="Total")
                    {
                        $type=3;
                        $item['handicap'] = $item['teamName'];
                    }
                    else if($item['subType']=="Draw Line")
                    {
                        $type=123;
                        $item['handicap'] ='Draw';
                    }
                    if ($item['handicap'] != '') {
                        $original .= '(' . $item['handicap'] . ')';
                    }
                    if($types=="")
                    {
                        $types=$type;
                    }
                    else if($types!="")
                    {
                        $types=$types.",".$type;
                    }
                    $original .= ($item['odd'] > 0) ? '+' . $item['odd'] : $item['odd'];
                    $dataArr = [
                        'ticket_id'=>$ticket_id,
                        'is_away' => $isAway,
                        'sport_league' => json_encode([$item['sport_id'], $item['league_name']]),
                        'is_home' => $isHome,
                        'points'=>$item['handicap'],
                        'event_id' => $item['event_id'],
                        'sport_name' => $item['sport_id'],
                        'betting_condition' => $item['odd'],
                        'betting_condition_original' => $original,
                        'risk_amount' => abs($ifbetrisk_amount),
                        'parlay_win_amount' => abs($ifbetwin_amount),
                        'event_date' => $eventDate,
                        'type' => $type,
                        'user_id' => Auth::user()->id,
                        'created_at' => date("Y-m-d H:i:s"),
                        'result' => 0,
                        'free_play' => $iFreePlay,
                        'bet_type' => $bettype,
                        'bet_extra_info' =>  json_encode($item['bet_extra_info'])
                    ];
                    $this->parlaydataSubmit($dataArr,$agentData,$event_ids,$types);
                }
            }
            $this->parlayEvent_idsSubmit($dataArr,$agentData,$event_ids,$types);
        }
        if(isset($request->status)&&$request->status=="reversebet")
        {
            $user = Auth::user();
            $iTicketNumber = randomNumberGenerator(10);
            $totalBettingPrice = $request->risk_amount;
            $reversebetrisk_amount=$request->risk_amount;
            $reversebetwin_amount=$request->win_amount;
            $userRow=User::find(Auth::user()->id);
            $updateData['available_balance']=($userRow->available_balance-$reversebetrisk_amount);
            $userRow->update($updateData);
            if (($request->freeplay == 1) && ($user->free_pay_balance < $totalBettingPrice))
            {
                return response()->json([
                    "status" => false,
                    "response" => "free pay balance doesn't enough"
                ]);
            }
            elseif ($user->available < $totalBettingPrice)
            {
                return response()->json([
                    "status" => false,
                    "response" => "user available balance doesn't enough"
                ]);
            }
            //Find User agent...
            $agentData = $this->agentRow();
            $event_ids="";
            $types="";
            foreach ($request->items as $item) {
                if ($reversebetrisk_amount != "" && $reversebetrisk_amount!= null) {
                    $ticket_id=$iTicketNumber;
                    $iFreePlay = ($request->freeplay == '') ? 0 : $request->freeplay;
                    $bettype="reversebet";
                    if($event_ids=="")
                    {
                        $event_ids=$item['event_id'];
                    }
                    else if($event_ids!="")
                    {
                        $event_ids=$event_ids.",".$item['event_id'];
                    }
                    $userCreditLimit = CreditLimit::where('user_id', Auth::user()->id)->first();
                    $isAway = ($item['away_name'] == $item['teamName']) ? 1 : 0;
                    $isHome = ($item['home_name'] == $item['teamName']) ? 1 : 0;
                    $eventDate = date('Y-m-d H:i:s', strtotime($item['date']));
                    $original = '';
                    $type=0;
                    if($item['subType']=="Spread")
                    {
                        $type=1;
                    }
                    else if($item['subType']=="Money Line")
                    {
                        $type=2;
                    }
                    else if($item['subType']=="Total")
                    {
                        $type=3;
                        $item['handicap'] = $item['teamName'];
                    }
                    else if($item['subType']=="Draw Line")
                    {
                        $type=123;
                        $item['handicap'] ='Draw';
                    }
                    if ($item['handicap'] != '') {
                        $original .= '(' . $item['handicap'] . ')';
                    }
                    if($types=="")
                    {
                        $types=$type;
                    }
                    else if($types!="")
                    {
                        $types=$types.",".$type;
                    }
                    $original .= ($item['odd'] > 0) ? '+' . $item['odd'] : $item['odd'];
                    $dataArr = [
                        'ticket_id'=>$ticket_id,
                        'is_away' => $isAway,
                        'sport_league' => json_encode([$item['sport_id'], $item['league_name']]),
                        'is_home' => $isHome,
                        'points'=>$item['handicap'],
                        'event_id' => $item['event_id'],
                        'sport_name' => $item['sport_id'],
                        'betting_condition' => $item['odd'],
                        'betting_condition_original' => $original,
                        'risk_amount' => abs($reversebetrisk_amount),
                        'parlay_win_amount' => abs($reversebetwin_amount),
                        'event_date' => $eventDate,
                        'type' => $type,
                        'user_id' => Auth::user()->id,
                        'created_at' => date("Y-m-d H:i:s"),
                        'result' => 0,
                        'free_play' => $iFreePlay,
                        'bet_type' => $bettype,
                        'bet_extra_info' => json_encode($item['bet_extra_info'])
                    ];
                    $this->parlaydataSubmit($dataArr,$agentData);
                }
            }
            $this->parlayEvent_idsSubmit($dataArr,$agentData,$event_ids,$types);
        }
    }
    private function parlaydataSubmit($array,$agentData)
    {
        $details=ParlayBettingDetail::create($array);
        _updateUserBalance();
        $eventData=[
            'user_id'=>$array['user_id'],
            'ticket_id'=>$array['ticket_id'],
            'event_id'=>$array['event_id'],
            'score_away'=>0,
            'score_home'=>0,
            'winner_away'=>0,
            'winner_home'=>0,
            'score_away_by_period'=>null,
            'score_home_by_period'=>null,
            'event_status'=>"FINAL",
            'type'=>$array['type'],
            'betting_type'=>$array['bet_type'],
            'created_at'=>date('Y-m-d H:i:s'),
        ];
        ParlayBettingEvent::create($eventData);
    }

    public function parlayEvent_idsSubmit($array,$agentData,$event_ids,$types)
    {
        $tokenId = Str::random(10);
        $eventidData=[
            'user_id'=>Auth::user()->id,
            'token_id'=>$tokenId,
            'ticket_id'=>$array['ticket_id'],
            'event_ids'=>$event_ids,
            'risk_amount'=>$array['risk_amount'],
            'parlay_win_amount'=>$array['parlay_win_amount'],
            'result'=>0,
            'types'=>$types,
            'free_play'=>$array['free_play'],
            'betting_type'=>$array['bet_type']
        ];
        ParlayEventId::create($eventidData);
    }

    public function saveHorseBet(Request $request)
    {
        if (isset($request->status) && $request->status == "horse")
        {

            $user = Auth::user();
            foreach ($request->items as $item) {
            $userRow=User::find(Auth::user()->id);
            $updateData['available_balance']=($userRow->available_balance-$item['risk_amount']);
            $userRow->update($updateData);
                    $horsedataArr = [
                        'user_id'=>$user->id,
                        'ticket_id' => '111',
                        'tournament_id' =>$item['tournament_id'],
                        'tournament_name' => $item['tournament_name'],
                        'race_id' =>$item['Race_id'],
                        'race_name' => $item['Race_name'],
                        'race_type'=>$item['Race_type'],
                        'race_date'=>$item['Race_date'],
                        'horse_id' => $item['horse_id'],
                        'horse_number' => $item['horse_num'],
                        'horse_name' => $item['horse_name'],
                        'unique_id'=>$item['unique_id'],
                        'risk_amount'=>$item['risk_amount'],
                        'created_at' => date('Y-m-d H:i:s')
                    ];
                 $this->horsedataSubmit($horsedataArr);
            }
            $userRow=User::find(Auth::User()->id);
            $bettingRisk = BettingDetail::where('user_id', Auth::user()->id)->where('result', 0)->where('status', 2)->where('free_play', 0)->sum('risk_amount');
            $parlayRisk = ParlayEventId::where('user_id', Auth::user()->id)->where('result', 0)->sum('risk_amount');
            $horseRisk=HorseBettingDetail::where('user_id',Auth::user()->id)->where('result',0)->sum('risk_amount');
            $pendingAmount = ($bettingRisk + $parlayRisk+$horseRisk);
            $return_data=[
                "status" => true,
                "user" => [
                    "available_balance" => number_format(floor($userRow->available)),
                    "pending_amount" => number_format($pendingAmount, 2),
                ]
            ];
         return response()->json($return_data);
        }
    }
    private function horsedataSubmit($horsedataArr)
    {
        $horseeventData = [
            'user_id' => $horsedataArr['user_id'],
            'ticket_id' => $horsedataArr['ticket_id'],
            'tournament_id' => $horsedataArr['tournament_id'],
            'tournament_name' => $horsedataArr['tournament_name'],
            'race_id' => $horsedataArr['race_id'],
            'race_name' => $horsedataArr['race_name'],
            'race_date'=>$horsedataArr['race_date'],
            'horse_id' => $horsedataArr['horse_id'],
            'horse_number' => $horsedataArr['horse_number'],
            'horse_name' => $horsedataArr['horse_name'],
            'unique_id' => $horsedataArr['unique_id'],
            'created_at' => date('Y-m-d H:i:s'),
        ];
        $horseevent =HorseBettingDetail::where('unique_id', $horsedataArr['unique_id'])->first();

        $event =HorseBettingEvent::where('unique_id', $horsedataArr['unique_id'])->first();
        _updateUserBalance();
        if(!empty($horseevent))
        {
            $result=$horseevent->update($horsedataArr);
             $event->update($horseeventData);
        }
        else
        {
            $result=HorseBettingDetail::create($horsedataArr);
            HorseBettingEvent::create($horseeventData);
        }

    }
    private function dataSubmit($array, $agentData)
    {
         //dd($eventArr);
        $details = BettingDetail::create($array);

        _updateUserBalance();
            $eventData = [
                'event_id' => $array['event_id'],
                'score_away' => $array['betting_win'],
                'score_home' => $array['betting_amount'],
                'winner_away' => 0,
                'winner_home' => 0,
                'score_away_by_period' => null,
                'score_home_by_period' => null,
                'event_status' => 'FINAL',
                'created_at' => date('Y-m-d H:i:s'),
            ];

        $event = BettingEvent::where('event_id', $array['event_id'])->where('event_status', 'FINAL')->first();
        if (!empty($event)) {
            $event->update($eventData);
        } else {
            BettingEvent::create($eventData);
        }

        if ($details) {
            $assignId =  (!empty($agentData))?$agentData->agent_id:0;
            $traData = [
                'user_id' => Auth::user()->id,
                'assign_id' => $assignId,
                'amount' => $array['risk_amount'],
                'betting_id' => $details->id,
                'type' => 0,
                'status' => 0,
                'agent_id' => 0,
                'description' => 'For Betting',
                'created_at' => date("Y-m-d H:i:s")
            ];
            if ($array['free_play'] != 0) {
                $traData['source'] = 5;
            } else {
                $traData['source'] = 0;
            }
            Transaction::create($traData);
        }
    }

    private function addWeeklyBalance($iTYpe, $agentData)
    {
        //  $iTYpe  1 for user, 2 for Agent,
        $current_start_week = strtotime('monday this week');
        $current_end_week     = strtotime('sunday this week');
        $current_start_week = date("Y-m-d", $current_start_week);
        $current_end_week = date("Y-m-d", $current_end_week);

        if (!empty($agentData)) {

            $agentRateData = AgentRate::where('user_id', $agentData->agent_id)
                ->where('player_id', Auth::user()->id)
                ->whereBetween('create_date', [$current_start_week, $current_end_week])
                ->first();
            if (empty($agentRateData)) {
                AgentRate::create([
                    'player_id' => Auth::user()->id,
                    'user_id' => $agentData->agent_id,
                    'rate' => $agentData->rate,
                    'create_date' => date("Y-m-d"),
                    'week_start' => $current_start_week,
                    'week_end' => $current_end_week,
                    'week_number' => date('W'),
                    'created_at' => date("Y-m-d H:i:s"),
                ]);
            }
            AgentRate::where('user_id', $agentData->agent_id)->whereBetween('create_date', [$current_start_week, $current_end_week])->update(['rate' =>  $agentData->rate]);
        }
    }

    private function agentRow()
    {
        //Find User agent...
        return AssignUser::select('assign_users.agent_id', 'users.rate')
            ->join('users', 'assign_users.agent_id', '=', 'users.id')
            ->where('assign_users.user_id', Auth::user()->id)
            ->first();
    }

}
