<?php

namespace App\Http\Controllers\Admin;

use Auth;
use App\User;
use Validator;
use App\Models\Maintenance;
use Illuminate\Http\Request;
use App\Utility\DateTime\DateTime;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Admin\Dasboards\AdminDashboard;
use MT;

class AdminController extends Controller
{
    private $ada;
    public function __construct()
    {
        $this->ada = new AdminDashboard();
    }
    public function dashboard()
    {
        if (Auth::user()->role == 'admin' || Auth::user()->role == 'agent' || Auth::user()->role == 'subagent') {
            $ad = $this->ada;
           //dd($ad->salesGraphDashboard());
            return view('admin.index', compact('ad'));
        } else {
            return redirect('straight-bet');
        }
    }

    public function index()
    {
        $data = User::where('role', 'admin')
            ->with('lastLogin')
            ->orderBy('id', 'DESC')
            ->get();
        return view('admin.admin_manage.index', compact('data'));
    }

    public function activeUsersList()
    {   $active_users = $this->ada->activeUsersLists();
       return view('admin.users.active-user',compact('active_users'));
    }
    public function todayReprestsList()
    {   $represnt_lists = $this->ada->todayRepresentLists();
       return view('admin.today_represnt_lists',compact('represnt_lists'));
    }
    public function weeklyReprestsList()
    {   $represnt_lists = $this->ada->weeklyRepresentLists();
       return view('admin.weekly_represnt_lists',compact('represnt_lists'));
    }
    public function deletedWagerListsby()
    {   $deleted_lists = $this->ada->deletedWagerLists();
       return view('admin.deleted_wagers',compact('deleted_lists'));
    }
    public function weeklyWager()
    {   $weekly_wagers = $this->ada->weeklyWagerLists();
       return view('admin.weekly_wager_lists',compact('weekly_wagers'));
    }
    public function totalWager()
    {   $total_wagers = $this->ada->allWagerLists();
       return view('admin.total_wager_lists',compact('total_wagers'));
    }

    public function store(Request $request)
    {
        $data = $request->all();
        if (isset($data['email'])) {
            $validator = Validator::make($data, [
                'name' => 'required|max:255',
                'username' => 'required|max:191|unique:users',
                'email' => 'max:191|unique:users',
                'password' => 'required|confirmed|min:8|max:50',
            ]);
        } else {
            $validator = Validator::make($data, [
                'name' => 'required|max:255',
                'username' => 'required|max:191|unique:users',
                'password' => 'required|confirmed|min:8|max:50',
            ]);
        }

        if ($validator->fails()) {
            return redirect()->back()
                ->with('error', 'Something is wrong! Please check your input data.')
                ->withErrors($validator)
                ->withInput();
        }

        $user              = new User();
        $user->role        = 'admin';
        $user->name        = $data['name'];
        $user->email       = $data['email'];
        $user->username    = $data['username'];
        $user->password    = bcrypt($data['password']);
        $user->status      = 1;
        $user->save();
        return redirect()->back()->with('success', 'New admin save successfully');
    }

    public function show($id)
    {
        $data = User::find($id);
        return view('admin.admin_manage.view', compact('data'));
    }

    public function maintenance()
    {
        return view('maintenance');
    }

    public function siteMaintenance()
    {
        $data = Maintenance::orderBy('id', 'desc')->first();
        return view('admin.site_maintenance', compact('data'));
    }

    public function siteMaintenanceStore(Request $request)
    {
        Maintenance::updateOrCreate(
            ['type'    => 'maintenance'],
            ['status'  => $request->maintenance_status]
        );
        if ($request->maintenance_status == 1) {
            return redirect()->back()->with('success', 'Site maintenance enable successfully!');
        } else {
            return redirect()->back()->with('error', 'Site maintenance disabled!');
        }
    }

    public function siteMaintenanceCheck()
    {
        $data = Maintenance::orderBy('id', 'desc')->first();
        if (!empty($data) && $data['status'] == 1) {
            Auth::logout();
            echo json_encode(['success' => true]);
        } else {
            echo json_encode(['success' => false]);
        }
    }
}
