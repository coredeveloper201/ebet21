<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Validator;
use App\Models\AgentRate;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class InvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $monthName = [
        'January',
        'February',
        'March',
        'April',
        'May',
        'June',
        'July',
        'August',
        'September',
        'October',
        'November',
        'December'
    ];
    public function getWeekMonSun($weekOffset) {
        $dt = new \DateTime();
        $dt->setIsoDate($dt->format('o'), $dt->format('W') + $weekOffset);
        return (object) [
            'start' => $dt->format('Y-m-d'),
            'end' => $dt->modify('+6 day')->format('Y-m-d'),
        ];
    }

    public function index()
    {
        $week1 = $this->getWeekMonSun(-1);
        $week2 = $this->getWeekMonSun(-2);
        $week3 = $this->getWeekMonSun(-3);
        $week4 = $this->getWeekMonSun(-4);

        $agentQuery = '';
        if (Auth::user()->role == 'agent' || Auth::user()->role == 'subagent') {
            $agentQuery = "AND user_id=".Auth::user()->id;
        }

        $sql = User::select('users.id', 'users.username', 'users.name', 'B.total1', 'C.total2', 'D.total3', 'E.total4', 'B.count1', 'C.count2', 'D.count3', 'E.count4', 'B.is_paid1', 'C.is_paid2', 'D.is_paid3', 'E.is_paid4')->where('role', 'agent')->orderBy('users.id', 'desc')
            ->leftJoin(\DB::raw("(SELECT user_id, SUM(rate) AS total1, COUNT(id) AS count1, GROUP_CONCAT(DISTINCT is_paid) AS is_paid1 FROM agent_rates WHERE create_date between '".$week1->start."' and '".$week1->end."' $agentQuery GROUP BY user_id) AS B"), 'B.user_id', '=', 'users.id')
            ->leftJoin(\DB::raw("(SELECT user_id, SUM(rate) AS total2, COUNT(id) AS count2, GROUP_CONCAT(DISTINCT is_paid) AS is_paid2 FROM agent_rates WHERE create_date between '".$week2->start."' and '".$week2->end."' $agentQuery GROUP BY user_id) AS C"), 'C.user_id', '=', 'users.id')
            ->leftJoin(\DB::raw("(SELECT user_id, SUM(rate) AS total3, COUNT(id) AS count3, GROUP_CONCAT(DISTINCT is_paid) AS is_paid3 FROM agent_rates WHERE create_date between '".$week3->start."' and '".$week3->end."' $agentQuery GROUP BY user_id) AS D"), 'D.user_id', '=', 'users.id')
            ->leftJoin(\DB::raw("(SELECT user_id, SUM(rate) AS total4, COUNT(id) AS count4, GROUP_CONCAT(DISTINCT is_paid) AS is_paid4 FROM agent_rates WHERE create_date between '".$week4->start."' and '".$week4->end."' $agentQuery GROUP BY user_id) AS E"), 'E.user_id', '=', 'users.id');

            if (Auth::user()->role == 'agent' || Auth::user()->role == 'subagent') {
                $sql->where('id', Auth::user()->id);
            }

            $data = $sql->get();

        return view('admin.invoice.index', compact('data', 'week1', 'week2', 'week3', 'week4'));
    }

    public function show(Request $request, $id)
    {
      /*   $sql = User::select('users.id', 'users.username', 'users.name', 'agent_rates.rate', 'agent_rates.create_date', 'agent_rates.is_paid', 'agent_rates.paid_at')
            ->join('agent_rates', 'agent_rates.player_id', '=', 'users.id')
            ->join('assign_users', 'assign_users.user_id', '=', 'users.id')
            ->where('assign_users.agent_id', $id)
            ->orderBy('users.id', 'desc');

        $historyDate = 'All';
        if ($request->week) {
            $week = $this->getWeekMonSun(-$request->week);
            $sql->whereBetween('agent_rates.create_date', [$week->start, $week->end]);
            $historyDate = $week->start.' to '.$week->end;
        } elseif ($request->allweek) {
            $week1 = $this->getWeekMonSun(-1);
            $week4 = $this->getWeekMonSun(-4);
            $sql->whereBetween('agent_rates.create_date', [$week4->start, $week1->end]);
            $historyDate = $week4->start.' to '.$week1->end;
        }
        $data = $sql->get(); */
        // $sql = "select count(*) as counts,rate, MONTH(create_date) as month, str_to_date(concat(yearweek(`create_date`), ' monday'), '%X%V %W') as `date` from agent_rates WHERE user_id ='3' group by yearweek(`create_date`)";
        // $data = \DB::select($sql)->get();
        //   $data = \DB::table('agent_rates')
        //   ->select(\DB::raw('count(*) as counts'), 'rate', \DB::raw('MONTH(create_date) as month'), \DB::raw("str_to_date(concat(yearweek(`create_date`), ' monday'), '%X%V %W') as `date`"))
        //   ->where('user_id',$id)
        //   ->groupBy(\DB::raw('yearweek(`create_date`)'))
        //   ->get();



        $data = \DB::select('call rateData('.$id.')');
        $realArray = [];
        foreach($data as $val) {
            $days = cal_days_in_month (CAL_GREGORIAN, $val->month , $val->year)/4;
            $days = ceil($days/4);


            $realArray[$val->year][$val->month][] = [
                'user_counts' => $val->counts,
                'week_number' => $this->getStartAndEndDate($val->date),
                'rate' => $val->rate,
                'month_name' => $this->monthName[$val->month-1],
                'week_start' => $val->date,
                'week_end' => date('Y-m-d', strtotime($val->date. ' + 6 days')),
            ];
        }
        //dd( $realArray);
        $agentInfo = User::find($id);
        return view('admin.invoice.show', compact('realArray','agentInfo'));
    }

   private function getStartAndEndDate($date) {
        $dto = new \DateTime($date);
        $year = $dto->format('Y');
        $month = $dto->format('m');
        $week = $dto->format("W");
        $dto->setISODate($year, $week);
        $ret['week_start'] = $dto->format('Y-m-d');
        $dto->modify('+6 days');
        $ret['week_end'] = $dto->format('Y-m-d');


        $start = $week-5;
        $end = $week+5;
        $weeks = [];
        for($i = $start; $i<=$end; $i++){
            $dto = new \DateTime();
            $dto->setISODate($year, $i);
            $ret['week_start_month'] = $dto->format('m');
            $ret['week_start'] = $dto->format('Y-m-d');
            $dto->modify('+6 days');
            $ret['week_end_month'] = $dto->format('m');
            $ret['week_end'] = $dto->format('Y-m-d');
            if($ret['week_start_month'] == $month || $ret['week_end_month'] == $month){
                if($ret['week_end_month'] <= $month){
                    $weeks[] = array('start'=>$ret['week_start'], 'end'=>$ret['week_end']);
                }
            }

        }
        $index = 1;
        foreach($weeks as $k=>$v){
            if($date >= $v['start'] && $date <= $v['end']){
                $index = $k+1;
            }
        }
        return $index;
}

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'user_id' => 'required',
            'amount' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->with('error', 'Something is wrong! Please check your input data.')
                ->withErrors($validator)
                ->withInput();
        }

        $week1 = $this->getWeekMonSun(-1);
        //$week2 = $this->getWeekMonSun(-2);
        //$week3 = $this->getWeekMonSun(-3);
        $week4 = $this->getWeekMonSun(-4);

        $saveData = [
            'is_paid' => 1,
            'paid_at' => date('Y-m-d H:i:s'),
        ];

        $user = User::find($data['user_id']);
        $dataUp = AgentRate::where('user_id', $data['user_id'])->whereBetween('create_date', [$week4->start, $week1->end])->update($saveData);
        if ($dataUp) {
            $user->update([
                'available_balance' => ($user->available_balance-$data['amount'][$k])
            ]);
        }
        return redirect()->back()->with('success', 'New agent save successfully');
    }
}
