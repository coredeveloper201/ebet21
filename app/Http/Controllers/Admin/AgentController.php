<?php

namespace App\Http\Controllers\Admin;

use App\Models\AgentRate;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class AgentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $previous_week = strtotime("-0 week +0 day");
        $start_week = strtotime("last sunday midnight",$previous_week);
        $end_week = strtotime("next saturday",$start_week);
        $start_week = date("Y-m-d",$start_week);
        $end_week = date("Y-m-d",$end_week);

        $data = User::select('users.*', 'B.total_amount', 'B.total_player')
            ->where('role', 'agent')->with('allAssignUser')->orderBy('users.id', 'desc')
            ->leftJoin(\DB::raw("(SELECT user_id, SUM(rate) AS total_amount, COUNT(id) AS total_player FROM agent_rates WHERE create_date between '".$start_week."' and '".$end_week."' GROUP BY user_id) AS B"), 'B.user_id', '=', 'users.id')
            ->get();

        return view('admin.agents.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        if (isset($data['email'])){
            $validator = Validator::make($data, [
                'name' => 'required|max:255',
                'username' => 'required|max:191|unique:users',
                'email' => 'max:191|unique:users',
                'password' => 'required|confirmed|min:8|max:50',
            ]);
        }else{
            $validator = Validator::make($data, [
                'name' => 'required|max:255',
                'username' => 'required|max:191|unique:users',
                'password' => 'required|confirmed|min:8|max:50',
            ]);
        }

        if ($validator->fails()) {
            return redirect()->back()
                ->with('error', 'Something is wrong! Please check your input data.')
                ->withErrors($validator)
                ->withInput();
        }

        $user              = new User();
        $user->role        = 'agent';
        $user->name        = $data['name'];
        $user->email       = $data['email'];
        $user->username    = $data['username'];
        $user->password    = bcrypt($data['password']);
        $user->referred_by = $data['referred_by'];
        $user->status      = 1;
        $user->save();
        return redirect()->back()->with('success', 'New agent save successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = User::find($id);
        //dd($data);
        return view('admin.agents.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        if (isset($data['email'])){
            $validator = Validator::make($data, [
                'email' => 'max:191|unique:users,email,'.$id,
            ]);
        }else{
            $validator = Validator::make($data, [
                'name' => 'required|max:255',
                'username' => 'required|max:191|unique:users,username,'.$id,
            ]);
        }
        if ($validator->fails()) {
            return redirect()->back()
                ->with('error', 'Something is wrong! Please check your input data.')
                ->withErrors($validator)
                ->withInput();
        }

        $user              = User::find($id);
        $user->name        = $data['name'];
        $user->email       = $data['email'];
        $user->username    = $data['username'];
        $user->rate        = $data['rate'];
        $user->referred_by = $data['referred_by'];
        $user->status      = $data['account_status'];
        if ($user->save()) {
            $current_start_week = strtotime('monday this week');
            $current_end_week     = strtotime('sunday this week');
            $current_start_week = date("Y-m-d", $current_start_week);
            $current_end_week = date("Y-m-d", $current_end_week);
            AgentRate::where('user_id', $user->id)->whereBetween('create_date', [$current_start_week, $current_end_week])->update(['rate' =>  $data['rate']]);
        }

        return redirect()->back()->with('success', 'Agent Update successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
