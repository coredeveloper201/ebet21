<?php

namespace App\Http\Controllers\Admin;

use App\Models\AgentRate;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class DistributionController extends Controller
{
    public function distributionLog(){

        $iTotAMounNew = 0;
        $dataArray = [];
        for($i=0; $i<6; $i++) {
            $previous_week 	= strtotime("-$i week +$i day");
            $start_weekNew 	= strtotime("last sunday midnight", $previous_week);
            $end_weekNew 	= strtotime("next saturday", $start_weekNew);
            $start_week 	= date("Y-m-d", $start_weekNew);
            $end_week 		= date("Y-m-d", $end_weekNew);
            $distributions  = AgentRate::where('user_id', Auth::user()->id)->whereBetween('create_date', [$start_week, $end_week])->orderBy('id', 'DESC')->get();
            foreach ($distributions as $dis) {
                $iTotAMounNew += $dis->total_amount;
                $dataArray[] = (object) [
                    'start_week' => $start_week,
                    'rate' => $dis->rate,
                    'total_amount' => $dis->total_amount,
                    'total_amount_new' => $iTotAMounNew,
                    'total_player' => $dis->total_player,
                ];
            }
        }

        return view('admin.distribution.log', compact('dataArray'));
    }
}
