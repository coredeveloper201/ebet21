<?php

namespace App\Http\Controllers\Admin;

use App\Traits\Wagers;
use App\Models\AssignUser;
use Illuminate\Http\Request;
use App\Models\BettingDetail;
use App\Models\ParlayEventId;
use App\Models\HorseBettingDetail;
use App\Models\ParlayBettingDetail;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class RecordController extends Controller
{
    public function wagers()
    {
        if(Auth::user()->role == 'admin')
        {
            $x = 0;
            $parlayArray = [];
            $parlayRecords = ParlayEventId::orderBy('id', 'DESC')->get();
            foreach ($parlayRecords as $key => $val) {
                $parlayEventId = explode(',', $val->event_ids);
                $parlayEventType = explode(',', $val->types);
                $parlayArray[strtotime($val->created_at)][$x] = [
                    'created_at' => $val->created_at,
                    'ticket_id' => $val->ticket_id,
                    'user_id' => $val->user_id,
                    'betting_type' => $val->betting_type,
                    'risk_amount' => $val->risk_amount,
                    'parlay_win_amount' => $val->parlay_win_amount,
                    'count' => count($parlayEventId),
                ];
                for ($i = 0; $i < count($parlayEventId); $i++) {
                    if(isset($parlayEventType[$i]))
                    {
                        $parlayResults = ParlayBettingDetail::where('event_id', $parlayEventId[$i])
                        ->where('created_at', $val->created_at)
                        ->where('type', $parlayEventType[$i])
                        ->where('bet_type', $val->betting_type)
                        ->first();
                        if (!empty($parlayResults))
                        {
                            $parlayArray[strtotime($val->created_at)][$x]['detail'][] = [
                                'sport_league' => $parlayResults->sport_league,
                                'abrevation_name' => $parlayResults->abrevation_name,
                                'user_id' => $val->user_id,
                                'sport_name' => $parlayResults->sport_name,
                                'bet_extra_info'=>$parlayResults->bet_extra_info,
                                'event_date' => $parlayResults->event_date,
                                'type'=>$parlayResults->type,
                                'points' => $parlayResults->points,
                                'betting_condition' => $parlayResults->betting_condition,
                                'created_at' => $parlayResults->created_at,
                            ];
                        }
                    }

                }
                $x++;
            }
            $x=($x+1);
            $horsebettingHistories=HorseBettingDetail::orderBy('id',"DESC")->get();
            foreach($horsebettingHistories as $key=>$val)
            {
                $parlayArray[strtotime($val->created_at)][$x]=[
                    'created_at'=>$val->created_at,
                    'ticket_id'=>$val->ticket_id,
                    'parlay_win_amount'=>$val->win_amount,
                    "risk_amount"=>$val->risk_amount,
                    'user_id' => $val->user_id,
                    "result"=>$val->result,
                    "unique_id"=>$val->unique_id,
                    "count"=>'',
                    'betting_type'=>'horse'
                ];
                for ($i = 0; $i < 1; $i++)
                {
                    $horseResults = HorseBettingDetail::
                        where('created_at', $val->created_at)
                        ->where('unique_id', $val->unique_id)
                        ->first();
                    if (!empty($horseResults))
                    {
                        $parlayArray[strtotime($val->created_at)][$x]['horsedetail'][] = [
                            'tournament_name'=>$horseResults->tournament_name,
                            'race_name' => $horseResults->race_name,
                            'horse_name' => $horseResults->horse_name,
                            'race_type'=>$horseResults->race_type,
                            'race_date'=>$horseResults->race_date,
                            'horse_number'=>$horseResults->horse_number,
                        ];
                    }
                }
                $x++;
            }
            $x = ($x+1);
            $bettingHistories = BettingDetail::orderBy('id', 'DESC')->get();
            foreach ($bettingHistories as $key => $val) {
                $parlayArray[strtotime($val->created_at)][$x] = [
                    'created_at' => $val->created_at,
                    'ticket_id' => $val->ticket_id,
                    'parlay_win_amount' => $val->betting_win,
                    'risk_amount' => $val->risk_amount,
                    'user_id' => $val->user_id,
                    'betting_condition_original' => $val->betting_condition_original,
                    'betting_type' => $val->bet_type,
                    'betting_wager_type' => $val->type,
                    'sport_league' => $val->sport_league,
                    'bet_extra_info' => $val->bet_extra_info,
                    'result' => $val->result,
                    'count' => '',
                ];
                $x++;
            }

            krsort($parlayArray);
            $dateRecords = json_decode(json_encode($parlayArray));
           // dd($dateRecords);

        }
        else if(Auth::user()->role == 'agent'){

            $wagers = AssignUser::select('users.username','betting_details.*','assign_users.user_id')
                      ->join('users','users.id','=','assign_users.user_id')
                      ->join('betting_details','users.id','=','betting_details.user_id')
                      ->where('agent_id','=',Auth::user()->id)
                      ->get();
        }


        return view('admin.record.wagers',compact('dateRecords'));
    }

    public function pendingWager()
    {
        if(Auth::user()->role == 'admin')
        {
            $x = 0;
            $parlayArray = [];
            $parlayRecords = ParlayEventId::where('result','0')->orderBy('id', 'DESC')->get();
            foreach ($parlayRecords as $key => $val) {
                $parlayEventId = explode(',', $val->event_ids);
                $parlayEventType = explode(',', $val->types);
                $parlayArray[strtotime($val->created_at)][$x] = [
                    'created_at' => $val->created_at,
                    'ticket_id' => $val->ticket_id,
                    'user_id' => $val->user_id,
                    'betting_type' => $val->betting_type,
                    'risk_amount' => $val->risk_amount,
                    'parlay_win_amount' => $val->parlay_win_amount,
                    'count' => count($parlayEventId),
                ];
                for ($i = 0; $i < count($parlayEventId); $i++) {
                    if(isset($parlayEventType[$i]))
                    {
                        $parlayResults = ParlayBettingDetail::where('result','0')->where('event_id', $parlayEventId[$i])
                        ->where('created_at', $val->created_at)
                        ->where('type', $parlayEventType[$i])
                        ->where('bet_type', $val->betting_type)
                        ->first();
                        if (!empty($parlayResults))
                        {
                            $parlayArray[strtotime($val->created_at)][$x]['detail'][] = [
                                'sport_league' => $parlayResults->sport_league,
                                'abrevation_name' => $parlayResults->abrevation_name,
                                'user_id' => $val->user_id,
                                'sport_name' => $parlayResults->sport_name,
                                'bet_extra_info'=>$parlayResults->bet_extra_info,
                                'event_date' => $parlayResults->event_date,
                                'type'=>$parlayResults->type,
                                'points' => $parlayResults->points,
                                'betting_condition' => $parlayResults->betting_condition,
                                'created_at' => $parlayResults->created_at,
                            ];
                        }
                    }

                }
                $x++;
            }
            $x=($x+1);
            $horsebettingHistories=HorseBettingDetail::where('result','0')->orderBy('id',"DESC")->get();
            foreach($horsebettingHistories as $key=>$val)
            {
                $parlayArray[strtotime($val->created_at)][$x]=[
                    'created_at'=>$val->created_at,
                    'ticket_id'=>$val->ticket_id,
                    'parlay_win_amount'=>$val->win_amount,
                    "risk_amount"=>$val->risk_amount,
                    'user_id' => $val->user_id,
                    "result"=>$val->result,
                    "unique_id"=>$val->unique_id,
                    "count"=>'',
                    'betting_type'=>'horse'
                ];
                for ($i = 0; $i < 1; $i++)
                {
                    $horseResults = HorseBettingDetail::where('result','0')->
                        where('created_at', $val->created_at)
                        ->where('unique_id', $val->unique_id)
                        ->first();
                    if (!empty($horseResults))
                    {
                        $parlayArray[strtotime($val->created_at)][$x]['horsedetail'][] = [
                            'tournament_name'=>$horseResults->tournament_name,
                            'race_name' => $horseResults->race_name,
                            'horse_name' => $horseResults->horse_name,
                            'race_type'=>$horseResults->race_type,
                            'race_date'=>$horseResults->race_date,
                            'horse_number'=>$horseResults->horse_number,
                        ];
                    }
                }
                $x++;
            }
            $x = ($x+1);
            $bettingHistories = BettingDetail::where('result','0')->orderBy('id', 'DESC')->get();
            foreach ($bettingHistories as $key => $val) {
                $parlayArray[strtotime($val->created_at)][$x] = [
                    'created_at' => $val->created_at,
                    'ticket_id' => $val->ticket_id,
                    'parlay_win_amount' => $val->betting_win,
                    'risk_amount' => $val->risk_amount,
                    'user_id' => $val->user_id,
                    'betting_condition_original' => $val->betting_condition_original,
                    'betting_type' => $val->bet_type,
                    'betting_wager_type' => $val->type,
                    'sport_league' => $val->sport_league,
                    'bet_extra_info' => $val->bet_extra_info,
                    'result' => $val->result,
                    'count' => '',
                ];
                $x++;
            }

            krsort($parlayArray);
            $dateRecords = json_decode(json_encode($parlayArray));
        }
        else if(Auth::user()->role == 'agent'){
            $wagers = AssignUser::select('users.username','betting_details.*','assign_users.user_id')
            ->join('users','users.id','=','assign_users.user_id')
            ->join('betting_details','users.id','=','betting_details.user_id')
            ->where('agent_id','=',Auth::user()->id)
            ->where('betting_details.result','=','0')
            ->get();
       }
        return view('admin.record.pending_wagers',compact('dateRecords'));
    }

    public function deleteWager()
    {
        if(Auth::user()->role == 'admin')
        {

           $wagers = BettingDetail::select('users.username','betting_details.*')->join('users','users.id','=','betting_details.user_id')->onlyTrashed()->get();
        }
        else if(Auth::user()->role == 'agent'){

            $wagers = AssignUser::select('users.username','betting_details.*','assign_users.user_id')
            ->join('users','users.id','=','assign_users.user_id')
            ->join('betting_details','users.id','=','betting_details.user_id')
            ->where('agent_id','=',Auth::user()->id)
            ->whereNotNull('betting_details.deleted_at')
            ->get();
        }
        return view('admin.record.delete_wagers',compact('wagers'));
    }

    public function weeklyFigures()
    {
        return view('admin.record.weekly_figures');
    }
}
