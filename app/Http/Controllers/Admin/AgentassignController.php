<?php

namespace App\Http\Controllers\Admin;

use App\Models\AgentRate;
use App\Models\AssignUser;
use App\Models\CreditLimit;
use App\Models\LoginLog;
use App\Models\Notification;
use App\Models\Transaction;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\BettingDetail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Validator;

class AgentassignController extends Controller
{
    //transaction
    public function transaction($id){
        $data = User::find($id);
        $transactions = Transaction::where('user_id', $id)->orderBy('created_at', 'DESC')->get();
        return view('admin.agentassign.transaction', compact('data', 'transactions'));
    }

    public function transactionStore(Request $request){
        $data = $request->all();
        $validator = Validator::make($data, [
            'amount' => 'required|regex:/^\d+(\.\d{1,2})?$/',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->with('error', 'Something is wrong! Please check your input data.')
                ->withErrors($validator)
                ->withInput();
        }
        $transaction               = new Transaction();
        $transaction->assign_id    = Auth::user()->id;
        $transaction->user_id      = $data['user_id'];
        $transaction->betting_id   = 0;
        $transaction->amount       = $data['amount'];
        $transaction->type         = $data['type'];
        $transaction->status       = 1;
        $transaction->description  = $data['brief_description'];
        $transaction->private_note = $data['private_note'];
        $transaction->source       = 1;
        $transaction->created_at   = date("Y-m-d H:i:s");
        if($transaction->save()){
            $thisUser = User::findOrFail($data['user_id']);
            if ($thisUser->role == 'user'){
                if ($data['type'] == 1){
                    $thisUser->available_balance = $thisUser->available_balance - $data['amount'];
                    $thisUser->coins             = $thisUser->coins - $data['amount'];
                }elseif ($data['type'] == 2){
                    $thisUser->available_balance = $thisUser->available_balance + $data['amount'];
                    $thisUser->coins             = $thisUser->coins + $data['amount'];
                }elseif ($data['type'] == 3){
                    $thisUser->free_pay_balance = $thisUser->free_pay_balance + $data['amount'];
                }else{
                    $thisUser->free_pay_balance = $thisUser->free_pay_balance - $data['amount'];
                }
            }else{
                if($data['type'] == 1){
                    $thisUser->agent_total = $thisUser->agent_total + $data['amount'];
                }else{
                    $thisUser->agent_total = $thisUser->agent_total - $data['amount'];
                }
            }
            $thisUser->save();
        }
        return redirect()->back()->with('success', 'Transaction successfully');
    }

    // limits
    public function limits($id){
        $data = User::find($id);
        $limit = CreditLimit::where('user_id', $id)->first();
        return view('admin.agentassign.limits', compact('data', 'limit'));
    }

    public function limitStore(Request $request){

        $thisUser = User::find($request->user_id);
        $thisUser->available_balance += ($request->credit_limit - $thisUser->credit_limit);
        $thisUser->credit_limit =  $request->credit_limit;

        if ($thisUser->save()){
            CreditLimit::updateOrCreate(
                ['user_id' => $request->user_id],
                [
                    'all_min_amount'      => $request->all_min_amount,
                    'all_max_amount'      => $request->all_max_amount,
                    'straight_min_amount' => $request->straight_min_amount,
                    'straight_max_amount' => $request->straight_max_amount,
                    'casino_min_amount'   => $request->casino_min_amount,
                    'casino_max_amount'   => $request->casino_max_amount,
                ]
            );
        }
        return redirect()->back()->with('success', 'Limit Store successfully');
    }

    public function pending($id){
        $data = User::find($id);
        $pendings = BettingDetail::where('user_id',$id)->where('result',0)->get();
        return view('admin.agentassign.pending', compact('data','pendings'));
    }

    public function pendingDelete($id){
        $prot = BettingDetail::find($id);
        $prot->delete();
        return redirect()->back()->withSuccess("wager has been Deleted");
    }



    public function wager($id){
        $data = User::find($id);
        $wagers = BettingDetail::where('user_id',$id)->get();
        return view('admin.agentassign.wager', compact('data','wagers'));
    }

    //notification
    public function notification($id){
        $data = User::find($id);
        $notifications = Notification::where('user_id', $id)->orderBy('created_at', 'DESC')->get();
        return view('admin.agentassign.notification', compact('data', 'notifications'));
    }

    public function notificationStore(Request $request){
        $data = $request->all();
        $validator = Validator::make($data, [
            'message' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->with('error', 'Something is wrong! Please check your input data.')
                ->withErrors($validator)
                ->withInput();
        }
        $notification             = new Notification();
        $notification->user_id    = $data['user_id'];
        $notification->message    = $data['message'];
        $notification->random_id  = uniqid();
        $notification->sent_by    = Auth::user()->id;
        $notification->is_seen    = 0;
        $notification->created_at = date("Y-m-d H:i:s");
        $notification->save();
        return redirect()->back()->with('success', 'Notification send successfully');
    }

    public function internetLog($id){
        $data = User::find($id);
        $internetLogs = LoginLog::where('user_id', $id)->orderBy('created_at', 'DESC')->get();
        return view('admin.agentassign.internet_log', compact('data', 'internetLogs'));
    }

    public function distribution($id){
        $data = User::find($id);

        //Calc...
        $iTotAMounNew = 0;
        $dataArray = [];
        for($i=0; $i<6; $i++) {
            $previous_week 	= strtotime("-$i week +$i day");
            $start_weekNew 	= strtotime("last sunday midnight", $previous_week);
            $end_weekNew 	= strtotime("next saturday", $start_weekNew);
            $start_week 	= date("Y-m-d", $start_weekNew);
            $end_week 		= date("Y-m-d", $end_weekNew);
            $distributions  = AgentRate::where('user_id', $id)->whereBetween('create_date', [$start_week, $end_week])->orderBy('id', 'DESC')->get();
            foreach ($distributions as $dis) {
                $iTotAMounNew += $dis->total_amount;
                $dataArray[] = (object) [
                    'start_week' => $start_week,
                    'rate' => $dis->rate,
                    'total_amount' => $dis->total_amount,
                    'total_amount_new' => $iTotAMounNew,
                    'total_player' => $dis->total_player,
                ];
            }
        }

        return view('admin.agentassign.distribution', compact('data', 'dataArray'));
    }

    public function deleteAccount($id){
        $data = User::find($id);
        return view('admin.agentassign.delete_account', compact('data'));
    }

    public function deleteAccountAction($id){
        $user = User::find($id);
        $user->delete();
        return redirect(route('user.index'))->with('success', "This '$user->username' account delete successfully");
    }

      //assign user
      public function assignUsers(){
        $data = User::where('role', 'agent')->orderBy('id', 'DESC')->get();
        return view('admin.agentassign.assign_users', compact('data'));
    }

    public function assignUsersCreate($id){
        $data = User::find($id);
        //$users = User::select('id', 'name', 'username')->where('role', 'user')->with('assignUser')->orderBy('id', 'DESC')->get();
        $users = User::select('users.id', 'users.name', 'users.username', 'assign_users.id AS assign_id', 'assign_users.agent_id', 'U.username as agent_username','U.name as agent_name')
        ->where('users.role', 'user')
        ->leftJoin('assign_users', 'users.id', '=', 'assign_users.user_id')
        ->leftJoin('users AS U', 'U.id','=','assign_users.agent_id')
        ->orderBy(\DB::raw('IFNULL(assign_users.id, 0)'), 'ASC')
        ->orderBy('users.id', 'DESC')->get();
       // dd($users);
        return view('admin.agentassign.assign_user_create', compact('data', 'users'));
    }
  /*   public function agentUsers(Request $r){
        $data = User::where('role','=','agent')->get();
        $unAssignedUsers =  User::select('users.*')->leftJoin('assign_users','assign_users.user_id','=','users.id')
            ->whereNull('assign_users.user_id')
            ->where('users.role', '=', 'user')
            ->get();
        return view('admin.agentassign.assign_users_', compact('data', 'unAssignedUsers'));
    }
    public function assignedUsersLists($agent_id){
        $listsUser = AssignUser::select('users.id','users.username')->join('users','assign_users.user_id','=','users.id')->where('assign_users.agent_id','=',$agent_id)->get();
        return response()->json(['usersList'=>$listsUser]);
    } */

    public function assignUsersRemove($id){
         $assignuser = AssignUser::find($id);
         if($assignuser->delete())
            return redirect()->back()->withSuccess("User is deleted successfully");
        else
          return redirect()->back()->withError("Problem deleting user");
    }

    public function assignUsersStore(Request $request){
        $data = $request->all();
        $assignCheck = AssignUser::where('agent_id', $data['agent_id'])->delete();
        if (!empty($data['user'])) {
            $userIds = $data['user'];
            $userIds = !empty($userIds) ? array_values(array_filter($userIds)) : array();
            $listData = [];
            foreach ($userIds as $k => $userId) {
                $listData[] = [
                    'agent_id' => $data['agent_id'],
                    'user_id'   => $userId,
                ];
            }
            if (!empty($listData)) {
                AssignUser::insert($listData);
            }
        }else{
            return redirect()->back()->with('error', 'Users can be not selected! Please select users!');
        }

        return redirect()->back()->with('success', 'User assign successfully!');
    }

   /*  public function __assignUsersStore(Request $request){
        $data = $request->all();
        if(empty($data['agent_id']))
            return redirect()->back()->with('error', 'No agent is selected, please, select an agent');
        else
        {
            $assignCheck = AssignUser::where('agent_id', $data['agent_id'])->delete();
        }

        if (!empty($data['user'])) {
            $userIds = $data['user'];
            $userIds = !empty($userIds) ? array_values(array_filter($userIds)) : array();
            $listData = [];
            foreach ($userIds as $k => $userId) {
                $listData[] = [
                    'agent_id' => $data['agent_id'],
                    'user_id'   => $userId,
                ];
            }
            if (!empty($listData)) {
                AssignUser::insert($listData);
            }
        }else{
            return redirect()->back()->with('error', 'Users can be not selected! Please select users!');
        }

        return redirect()->back()->with('success', 'User assign successfully!');
    } */




}
