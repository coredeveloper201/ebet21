<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class SubAgentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = User::where('role', 'subagent')->orderBy('id', 'DESC')->get();
        return view('admin.subagent.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        if (isset($data['email'])){
            $validator = Validator::make($data, [
                'name' => 'required|max:255',
                'username' => 'required|max:191|unique:users',
                'email' => 'max:191|unique:users',
                'password' => 'required|confirmed|min:8|max:50',
            ]);
        }else{
            $validator = Validator::make($data, [
                'name' => 'required|max:255',
                'username' => 'required|max:191|unique:users',
                'password' => 'required|confirmed|min:8|max:50',
            ]);
        }

        if ($validator->fails()) {
            return redirect()->back()
                ->with('error', 'Something is wrong! Please check your input data.')
                ->withErrors($validator)
                ->withInput();
        }

        $user              = new User();
        $user->role        = 'subagent';
        $user->name        = $data['name'];
        $user->email       = $data['email'];
        $user->username    = $data['username'];
        $user->password    = bcrypt($data['password']);
        $user->referred_by = $data['referred_by'];
        $user->status      = 1;
        $user->save();
        return redirect()->back()->with('success', 'New sub-agent save successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = User::find($id);
        return view('admin.subagent.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'name' => 'required|max:255',
            'username' => 'required|max:191|unique:users,username,'.$id,
            'email' => 'required|max:191|unique:users,email,'.$id,
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->with('error', 'Something is wrong! Please check your input data.')
                ->withErrors($validator)
                ->withInput();
        }

        $user              = User::find($id);
        $user->name        = $data['name'];
        $user->email       = $data['email'];
        $user->username    = $data['username'];
        $user->referred_by = $data['referred_by'];
        $user->status      = $data['account_status'];
        $user->save();
        return redirect()->back()->with('success', 'Sub Agent Update successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function assignUsers(){
        $data = User::where('role', 'subagent')->orderBy('id', 'DESC')->get();
        return view('admin.subagent.assign_users', compact('data'));
    }

}
