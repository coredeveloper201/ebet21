<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Models\AssignUser;
use App\Models\CreditLimit;
use App\Models\Transaction;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()->role == 'admin') {
            $data = User::select('users.*', 'A.user_current_credit_limit', 'A.user_current_max_limit', 'A.user_current_pending', 'A.user_current_balance')
            ->leftJoin(\DB::raw("(SELECT A.user_id, A.user_current_credit_limit, A.user_current_max_limit, A.user_current_pending, A.user_current_balance
                FROM betting_details AS A
                WHERE A.id=(SELECT MAX(id) FROM betting_details WHERE user_id=A.user_id)) AS A"), 'users.id','=','A.user_id')
                    ->where('users.role', 'user')
                    ->orderBy('users.id', 'desc')
                    ->get();
            $agents = User::where('role','=','agent')->pluck('username','id');
        }
        else if(Auth::user()->role == 'agent')
        {
            $data = User::select('users.*', 'A.user_current_credit_limit', 'A.user_current_max_limit', 'A.user_current_pending', 'A.user_current_balance')
            ->leftJoin(\DB::raw("(SELECT A.user_id, A.user_current_credit_limit, A.user_current_max_limit, A.user_current_pending, A.user_current_balance
                FROM betting_details AS A
                WHERE A.id=(SELECT MAX(id) FROM betting_details WHERE user_id=A.user_id)) AS A"), 'users.id','=','A.user_id')
                ->join('assign_users', 'users.id', '=', 'assign_users.user_id')
                ->where('assign_users.agent_id', Auth::user()->id)
                ->orderBy('users.id', 'desc')->get();
        }
       else {
            $data = User::select('users.*', 'A.user_current_credit_limit', 'A.user_current_max_limit', 'A.user_current_pending', 'A.user_current_balance')
            ->leftJoin(\DB::raw("(SELECT A.user_id, A.user_current_credit_limit, A.user_current_max_limit, A.user_current_pending, A.user_current_balance
                FROM betting_details AS A
                WHERE A.id=(SELECT MAX(id) FROM betting_details WHERE user_id=A.user_id)) AS A"), 'users.id','=','A.user_id')
                ->join('assign_users', 'users.id', '=', 'assign_users.user_id')
                ->where('assign_users.agent_id', Auth::user()->id)
                ->orderBy('users.id', 'desc')->get();
        }
        return view('admin.users.index', compact('data','agents'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        if (isset($data['email'])) {
            $validator = Validator::make($data, [
                'name' => 'required|max:255',
                'agent_id' => 'required',
                'username' => 'required|max:191|unique:users',
                'email' => 'max:191|unique:users',
                'password' => 'required|confirmed|min:8|max:50',
            ]);
        } else {
            $validator = Validator::make($data, [
                'name' => 'required|max:255',
                'agent_id' => 'required',
                'username' => 'required|max:191|unique:users',
                'password' => 'required|confirmed|min:8|max:50',
            ]);
        }

        if ($validator->fails()) {
            return redirect()->back()
                ->with('error', 'Something is wrong! Please check your input data.')
                ->withErrors($validator)
                ->withInput();
        }
        $user              = new User();
        $user->role        = 'user';
        $user->name        = $data['name'];
        $user->email       = $data['email'];
        $user->username    = $data['username'];
        $user->coins    = "0";
        $user->password    = bcrypt($data['password']);
        $user->referred_by = $data['referred_by'];
        $user->status      = 1;
        if ($user->save())
        {


            $user->credit_limit = 0;
            CreditLimit::updateOrCreate(
                ['user_id' => $user->id],
                [
                    'all_min_amount'      => 0,
                    'all_max_amount'      => 0,
                    'straight_min_amount' => 0,
                    'straight_max_amount' => 0,
                    'casino_min_amount'   => 0,
                    'casino_max_amount'   => 0,
                ]

            );

            AssignUser::create([
                'agent_id' => $data['agent_id'],
                'user_id'   => $user->id,
            ]);

            $transaction               = new Transaction();
            $transaction->assign_id    = Auth::user()->id;
            $transaction->user_id      = $user->id;
            $transaction->betting_id   = 0;
            $transaction->amount       = 0;
            $transaction->type         = 0;
            $transaction->status       = 1;
            $transaction->description  = "";
            $transaction->private_note = "";
            $transaction->source       = 1;
            $transaction->created_at   = date("Y-m-d H:i:s");
            if($transaction->save()){
                    $user->available_balance = 0;
                    $user->coins             = 0;
                $user->save();
            }
        }
        return redirect()->back()->with('success', 'New user save successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = User::find($id);
        return view('admin.users.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'name' => 'required|max:255',
            'username' => 'required|max:191|unique:users,username,' . $id,
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->with('error', 'Something is wrong! Please check your input data.')
                ->withErrors($validator)
                ->withInput();
        }

        $user              = User::find($id);
        $user->name        = $data['name'];
        $user->email       = $data['email'];
        $user->username    = $data['username'];
        $user->referred_by = $data['referred_by'];
        $user->status      = $data['account_status'];
        $user->is_casino   = $data['casino_status'];
        $user->save();
        return redirect()->back()->with('success', 'User Update successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function resetPassword(Request $request, $id)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'password' => 'required|confirmed|min:8|max:50',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->with('error', 'Something is wrong! Please check your input data.')
                ->withErrors($validator)
                ->withInput();
        }

        $user              = User::find($id);
        $user->password    = bcrypt($data['password']);
        $user->save();
        return redirect()->back()->with('success', 'User password reset successfully!');
    }
}
