<?php

if (
            $wager_type == "12.1.1"
            ||  $wager_type == "18.1.1"
            ||  $wager_type == "12.1.2"
            ||  $wager_type == "18.1.2"
            ||  $wager_type == "12.1.3"
            ||  $wager_type == "18.1.3"
            ) {
                $wager = '1st Half '.$extraInfo->betting_wager;
        }
       else if (
            $wager_type == "16.1.1"
            ||  $wager_type == "16.1.2"
            ||  $wager_type == "16.1.3"
            ) {
                $wager = 'First Five Innings '.$extraInfo->betting_wager;
        }
      else  if (
            $wager_type == "16.2.1"
            ||  $wager_type == "16.2.2"
            ||  $wager_type == "16.2.3"
            ) {
                $wager = 'First Inning '.$extraInfo->betting_wager;
        }
        else if (
            $wager_type == "12.2.1"
            ||  $wager_type == "12.2.2"
            ||  $wager_type == "12.2.3"
            ) {
                $wager = '2nd Half '.$extraInfo->betting_wager;
            }
        else if (
            $wager_type == "17.10.1"
            ||  $wager_type == "17.10.2"
            ||  $wager_type == "17.10.3"
            ) {
                $wager = 'First Period '.$extraInfo->betting_wager;
            }
        else if (
            $wager_type == "13.1.1"
            ||$wager_type == "14.1.1"
            ||  $wager_type == "13.1.2"
            ||  $wager_type == "13.1.3"
            ) {
                $wager = '1st Set '.$extraInfo->betting_wager;
            }
        else if (
            $wager_type == "18.3.1" || $wager_type == "12.3.1"
            ||  $wager_type == "18.3.2" || $wager_type == "12.3.2"
            ||  $wager_type == "18.3.3"  ||  $wager_type == "12.3.3"
            ) {
                $wager = '1st Quarter '.$extraInfo->betting_wager;
        }
        elseif ($wager_type == "12.4") {
            $wager = 'Overtime';
        }
        elseif ($wager_type == "17.11" || $wager_type == "1.11") {
            $wager = 'Both Team To Score';
            $show_for = false;
        }
        elseif ($wager_type == "17.14") {
            $wager = $extraInfo->bet_on_team_name.' will score a Goal';
            $show_for = false;
        }
        elseif ($wager_type == "17.15") {
            $wager = $extraInfo->bet_on_team_name.' will score a Goal';
            $show_for = false;
        }
        elseif ($wager_type == "17.16" || $wager_type == "1.16") {
            $wager = 'Highest Scoring Period';
            $show_for = false;
        }
        elseif ($wager_type == "18.16") {
            $wager = 'Highest Scoring Half';
            $show_for = false;
        }
        elseif ($wager_type == "12.5" || $wager_type == "18.5") {
            $wager = "Winning Margin";
            $ranges = explode(':(',$val->betting_condition_original);
            $original_condition =  $ranges[0];
        }
        elseif ($wager_type == "1") {
            if($sportLeague[0] == '13' &&  $val->betting_type =='live')
                $wager = 'Current Set';
            else  $wager = 'Spread';
        }
        elseif ($wager_type == "2") {

            if($sportLeague[0] == '13' &&  $val->betting_type =='live')
                $wager = 'Winner';
            else  $wager = 'Money Line';
        }
        elseif ($wager_type == "3") {
            if($sportLeague[0] == '13' &&  $val->betting_type =='live')
                $wager = 'Next Set';
            else  {
                $wager = 'Total';
                $show_for = false;
            }

        }
        elseif ($wager_type == "12.9" || $wager_type == "18.9" || $wager_type == "17.9" || $wager_type == "1.9" || $wager_type == "14.9") {
            $show_for = false;
        }
        elseif ($wager_type == "18.6" || $wager_type == "12.6") {
            $wager = 'Highest Scoring Quarter ';
            $show_for = false;
        }
        elseif ($wager_type == "18.8"
            || $wager_type == "12.8"
            || $wager_type == "18.7"
            || $wager_type == "12.7"
            || $wager_type == "17.7"
            || $wager_type == "17.8"
            || $wager_type == "16.8"
            || $wager_type == "16.7"
            || $wager_type == "14.7"
            || $wager_type == "14.8"
            ) {
            $wager = 'Team Total';
        }
        elseif ($wager_type == "1.8"
            || $wager_type == "1.7"
            ) {
            $wager = 'Team Goals';
        }
        elseif ($wager_type == "1.17") {
            $wager = 'First Half Goals';
            $show_for = false;
        }
        elseif ($wager_type == "1.18") {
            $wager = 'Correct Score';
            $show_for = false;
        }
        elseif ($wager_type == "1.19") {
            $wager = 'First Half Winner';
        }
        elseif ($wager_type == "1.20") {
            $wager = 'First Half Double Chance';
            $show_for = false;
        }
        elseif ($wager_type == "1.21") {
            $wager = 'Exact Number of Goals';
            $show_for = false;
        }
        elseif ($wager_type == "13.22") {
            $wager = 'Set Betting';
            $show_for = false;
        }
        elseif ($wager_type == "13.23") {
            $wager = 'Tie Break';
            $show_for = false;
        }
        elseif ($wager_type == "13.24") {
            $wager = 'Set Match';
            $show_for = false;
        }
        elseif ($wager_type == "123") {
            $wager = 'Money Line ';
            $show_for = false;
        }

        if($wager_type == "12.1.3"
         ||  $wager_type == "18.1.3"
         ||  $wager_type == "16.2.3"
         ||  $wager_type == "12.2.3"
         ||  $wager_type == "17.10.3"
         ||  $wager_type == "13.1.3"
         ||  $wager_type == "18.3.3"
         ||  $wager_type == "12.3.3"
         )
         {
            $show_for = false;
         }
