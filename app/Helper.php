<?php

use App\User;
use Illuminate\Support\Facades\Auth;

if (!function_exists('getWagerDisableCheck')) {
    function getWagerDisableCheck($sportOrLeagueId)
    {
        $wagerDisableCheck = \App\Models\WagerDisable::where('sport_league', $sportOrLeagueId)->where('type', 'active')->first();
        return $wagerDisableCheck;
    }
}


if (!function_exists('__liveBetResult')) {
    function __liveBetResult($date,$id)
    {
        $url = 'http://inplay.goalserve.com/results/'.$date.'/'.$id.'.json';
        $arrContextOptions=array(
            "ssl"=>array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ),
        );
        $resultree =  @file_get_contents($url, false, stream_context_create($arrContextOptions));
        if($resultree === false)
        {
            echo "nothing has found with this id or date";
            return false;
        }
        else
        {
            $resultree = str_replace('\'', '`', $resultree);
            $resultree = preg_replace('/[^{A-Za-z0-9,\:`"}\-]/', '', $resultree);
            $resultree = rtrim($resultree, "\0");
            $resultree = json_decode($resultree, true);
            return $resultree;
        }

    }
}


if (!function_exists('_usaSort')) {
    function _usaSort($arr)
    {
        $rr = [];
            foreach($arr as $key => $ar)
            {
                $pos = strpos($ar,"Usa");
                if($pos !== false)
                {
                    $rr[$key] = $ar;
                }
            }
        return $rr+$arr;
    }
}

if (!function_exists('_getDate')) {
    function _getDate($strtime)
    {
        date_default_timezone_set('UTC');
        $date = date('Y-m-d H:i:sP', $strtime);
        date_default_timezone_set('America/Chicago');
        $date = date('m/d/Y', strtotime($date));
        date_default_timezone_set('UTC');
        return $date;
    }
}
if (!function_exists('_preccedSign')) {
    function _preccedSign($value)
    {
        return (intval($value)>0)?"+".$value:$value;
    }
}

if (!function_exists('_getTime')) {
    function _getTime($strtime)
    {
        date_default_timezone_set('UTC');
        $time = date('H:i:sP', $strtime);
        date_default_timezone_set('America/Chicago');
        $time = date('h:i a', strtotime($time));
        date_default_timezone_set('UTC');
        return $time;
    }
}

if (!function_exists('_toAmericanDecimal')) {
    function _toAmericanDecimal($number)
    {1 == $number ? $number = 2 : '';
        $nu = $number < 2 ? round((-100) / ($number - 1)) : round(($number - 1) * 100);
        return (round($nu/5) * 5);
    }
}

if (!function_exists('_getHeadlineDate')) {
    function _getHeadlineDate($strtime)
    {
        date_default_timezone_set('UTC');
        $date = date('Y-m-d H:i:sP', $strtime);
        date_default_timezone_set('America/Chicago');
        $date = date('F, jS Y', strtotime($date));
        date_default_timezone_set('UTC');
        return $date;
    }
}

if (!function_exists('_getHeadlineTime')) {
    function _getHeadlineTime($strtime)
    {
        date_default_timezone_set('UTC');
        $date = date('Y-m-d H:i:sP', $strtime);
        date_default_timezone_set('America/Chicago');
        $date = date('h:ia', strtotime($date));
        date_default_timezone_set('UTC');
        return $date;
    }
}

if (!function_exists('getUserById')) {
    function getUserById($userId)
    {
        $userInfo = \App\User::find($userId);
        return $userInfo;
    }
}

if (!function_exists('_updateUserBalance')) {
    function _updateUserBalance($user_id=null)
    {
        $current_start_week = strtotime('monday this week');
        $current_end_week     = strtotime('sunday this week');
        $current_start_week = date("Y-m-d", $current_start_week);
        $current_end_week = date("Y-m-d", $current_end_week);
        $if =  App\Models\UserWagerReport::where('user_id', Auth::user()->id)->whereBetween('created_at', [$current_start_week, $current_end_week])->first();
        if($if != null)
        {
            $if->pending = Auth::user()->pending;
            $if->balance = Auth::user()->balance;
            $if->save();
        }
        else
        {
            App\Models\UserWagerReport::create([
                'user_id' => Auth::user()->id,
                'pending' => Auth::user()->pending,
                'balance' => Auth::user()->balance,
            ]);
        }
    }
}

if (!function_exists('getMaintenanceStatus')) {
    function getMaintenanceStatus()
    {
        $maintenance = \App\Models\Maintenance::orderBy('id', 'DESC')->first();
        return $maintenance;
    }
}

if (!function_exists('randomNumberGenerator')) {
    function randomNumberGenerator($n)
    {
        $generated_string = "";
        $domain = "1234567890";
        $len = strlen($domain);
        for ($i = 0; $i < $n; $i++) {
            $index = rand(0, $len - 1);
            $generated_string = $generated_string . $domain[$index];
        }

        return $generated_string;
    }
}

if (!function_exists('sportsName')) {
    function sportsName($key)
    {
        $array = [
        '16' => "Baseball",
        '17' => "Hockey",
        '9'  => "MMA",
        '13' => "Tennis",
        '12' => "Football",
        '151' => "E-Sports",
        '18' => "Basketball",
        '133' => "Golf",   //this is actually 13 to remove the conflict I used 133
        '6'  => "MotorSport",
        '10' => "Cycling",
        '11' => "Boxing",
        '14' => "volleyball",
        '15' => "Cricket",
        '19' => "Handball",
        '20' => "Rugby",
        '21' => "Rugby League",
        '1' => "Soccer",
        ];

        if ($key>0) {
            return $array[$key];
        }
        return $array;
    }
}

if(!function_exists('__getUsername'))
{
    function __getUsername($user_id)
    {
        return User::select('username')->where('id',$user_id)->first();
    }
}

