<?php
namespace App\Ebet\Sports\Live;


use stdClass;
use App\Ebet\Sports\LiveSportAbstract;

class Hockey extends LiveSportAbstract{

  public $spread = 'puck-line';
  public $money_line = 'money-line';
  public $total = 'game-total';

  protected $dataSet = [
    'puck-line'  => '170153',
    'money-line'  => '170152',
    'game-total'  => '170154',
  ];

   public function __construct($url)
   {
        parent::__construct($url);
   }


   public function getFormatted()
    {

        error_reporting(0);
        $allData = $this->getDataArr();
        $sortedbyleague = [];
        foreach($allData as $data)
        {
            $item  = new stdClass;
            $item->time = $data->info->secunds;
            $item->datetime = $data->info->start_date;
            $item->hr =  $data->info->period;
            $item->ss =  new stdClass;
            $item->ss->home = $this->breakdownss($data->info->score)[0];
            $item->ss->away = $this->breakdownss($data->info->score)[1];
            $item->id = $data->info->id;
            $team_names =  explode('vs',$data->info->name);
            $item->home = new stdClass;
            $item->home->name  =  $team_names[0];
            $item->away = new stdClass;
            $item->away->name =   $team_names[1];
            $item->league->name =  $data->info->league;
            foreach($data->sport_odd as $key => $odd)
            {

                if($key == $this->spread)
                {
                    foreach($odd->participants as $participant)
                    {
                        if($participant->short_name  == 'Home')
                        {
                            $item->spread_line->home_odd = $this->numberFormatted($participant->value_us);
                            $item->spread_line->home->handicap =$participant->handicap;
                            $item->home->id  =  $participant->id;
                        }
                        if($participant->short_name  == 'Away')
                        {
                            $item->spread_line->away_odd = $this->numberFormatted($participant->value_us);
                            $item->spread_line->away->handicap = $participant->handicap;
                            $item->away->id  =  $participant->id;
                        }

                    }

                }


                if($key == $this->money_line)
                {
                    foreach($odd->participants as $participant)
                    {
                        if($participant->short_name  == 'Home')
                        {
                            $item->money_line->home_odd = $this->numberFormatted($participant->value_us);
                        }

                        if($participant->short_name  == 'Away')
                        {
                            $item->money_line->away_odd = $this->numberFormatted($participant->value_us);
                        }


                        if($participant->name =='Draw')
                        {
                            $item->draw = $this->numberFormatted($participant->value_us);
                        }
                    }
                }

                if($key == $this->total)
                {
                    foreach($odd->participants as $participant)
                    {
                        if($participant->name  == 'Over')
                        {
                            $item->over->home_odd  =  $this->numberFormatted($participant->value_us);
                            $item->over->handicap  =  $participant->name.' '.$participant->handicap;
                        }
                        if($participant->name  == 'Under')
                        {
                            $item->under->away_odd = $this->numberFormatted($participant->value_us);
                            $item->under->handicap =  $participant->name.' '.$participant->handicap;
                        }
                    }
                }
            }
            $tag = false;
            foreach ($sortedbyleague as  $sbl) {
                if ($sbl->even_name == $item->league->name) {
                    $tag = true;
                    array_push($sbl->results, $item);
                    break;
                }
            }
            if (!$tag) {
             $obj = new stdClass();
             $obj->even_id   = $item->id;
             $obj->even_name = $item->league->name;
             $obj->results  =  array();
             array_push($obj->results, $item);
             array_push($sortedbyleague, $obj);
            }

        }
        return $sortedbyleague;
    }
}
