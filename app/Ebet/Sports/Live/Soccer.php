<?php
namespace App\Ebet\Sports\Live;


use stdClass;
use App\Ebet\Sports\LiveSportAbstract;

class Soccer extends LiveSportAbstract{


  protected $dataSet = [
    'spread'      => '10159',
    'money_line'  => '1777',
    'total'       => '10124',
  ];

   public function __construct($url)
   {
        parent::__construct($url);
   }
   public function dataTest()
   {
       return $this->getDataArr();
   }
   public function getFormatted()
    {
        error_reporting(0);
        $allData = $this->getDataArr();
        $sortedbyleague = [];
        foreach($allData as $data)
        {
            $item  = new stdClass();
            $item->time = $data->info->secunds;
            $item->datetime = $data->info->start_date;
            $item->hr = $data->info->period;
            $item->id = $data->info->id;
            $team_names =  explode('vs',$data->info->name);
            $item->ss =  new stdClass;
            $item->ss->home = $this->breakdownss($data->info->score)[0];
            $item->ss->away = $this->breakdownss($data->info->score)[1];
            $item->home = new stdClass;
            $item->home->name  =  $team_names[0];
            $item->away = new stdClass;
            $item->away->name =   $team_names[1];
            $item->eventimers = $data->info->secunds;
            $item->league->name =  $data->info->league;
            foreach($data->sport_odd as $key => $odd)
            {
                 //Halftime Result

                if($key == $this->spread)
                {
                    foreach($odd->participants as $participant)
                    {
                        if($participant->short_name  == 'Home' && $participant->order == 0)
                        {
                            $item->next->first = $this->numberFormatted($participant->value_us);
                            $item->spread_line->home->handicap =$participant->handicap;
                            $item->home->id  =  $participant->id;
                        }
                        if($participant->short_name  == 'Away' && $participant->order == 0)
                        {
                            $item->next->second = $this->numberFormatted($participant->value_us);
                            $item->spread_line->away->handicap = $participant->handicap;
                            $item->away->id  =  $participant->id;

                        }

                    }

                }

                 //1X2

                if($key == $this->money_line)
                {
                    foreach($odd->participants as $participant)
                    {
                        if($participant->name=='Draw')
                        {
                            $item->draw = $this->numberFormatted($participant->value_us);
                        }
                        if($participant->short_name  == 'Home')
                        {
                            $item->odd->first = $this->numberFormatted($participant->value_us);
                            $item->home->id  =  $participant->id;
                        }

                        if($participant->short_name  == 'Away')
                        {
                            $item->odd->second = $this->numberFormatted($participant->value_us);
                            $item->away->id  =  $participant->id;
                        }
                    }
                }

                  //match goals

                if($key == $this->total)
                {
                    foreach($odd->participants as $participant)
                    {
                        if($participant->name == "Over")
                        {
                            $item->match->over->odd  =  $this->numberFormatted($participant->value_us);
                            $item->match->over->val  =  $participant->name.' '.$participant->handicap;
                        }
                        if($participant->name == "Under")
                        {
                            $item->match->under->odd   = $this->numberFormatted($participant->value_us);
                            $item->match->under->val  =  $participant->name.' '.$participant->handicap;
                        }
                    }
                }
                $item->no_of_innings = 0;
            }

            $tag = false;
            foreach ($sortedbyleague as  $sbl) {
                if ($sbl->even_name == $item->league->name) {
                    $tag = true;
                    array_push($sbl->results, $item);
                    break;
                }
            }
            if (!$tag) {
             $obj = new stdClass();
             $obj->even_id   = $item->id;
             $obj->even_name = $item->league->name;
             $obj->results  =  array();
             array_push($obj->results, $item);
             array_push($sortedbyleague, $obj);
            }

        }
        return $sortedbyleague;
    }
}
