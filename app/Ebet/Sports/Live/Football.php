<?php
namespace App\Ebet\Sports\Live;


use stdClass;
use App\Ebet\Sports\LiveSportAbstract;

class Football extends LiveSportAbstract{
 protected $spread = 'spread';

  protected $dataSet = [
    'spread'  => '120201',
    'money_line'  => '120200',
    'total'  => '120211',
  ];

   public function __construct($url)
   {
        parent::__construct($url);
   }

   public function getItem()
   {
       return $this->getDataArr();
   }

   public function getFormatted()
    {

        error_reporting(0);
        $allData = $this->getDataArr();
        $sortedbyleague = [];
        foreach($allData as $data)
        {
            $item  = new stdClass;
            $item->time = $data->info->secunds;
            $item->inning_period = $data->info->period;
            $item->datetime = $data->info->start_date;
            $item->id = $data->info->id;
            $team_names =  explode('vs',$data->info->name);
            $item->home = new stdClass;
            $item->home->name  =  $team_names[0];
            $item->away = new stdClass;
            $item->away->name =   $team_names[1];
            $item->ss =  new stdClass;
            $item->ss->home = $this->breakdownss($data->info->score)[0];
            $item->ss->away = $this->breakdownss($data->info->score)[1];
            $item->league->name =  $data->info->league;
            foreach($data->sport_odd as $key => $odd)
            {
                if($key == $this->spread)
                {
                    foreach($odd->participants as $participant)
                    {
                        if($participant->short_name  == 'Home')
                        {
                            $item->odd->first = $this->numberFormatted($participant->value_us);
                            $item->first->handicap =   $participant->handicap;
                            $item->home->id  =  $participant->id;
                        }
                        if($participant->short_name  == 'Away')
                        {
                            $item->odd->second = $this->numberFormatted($participant->value_us);
                            $item->second->handicap = $participant->handicap;
                            $item->away->id  =  $participant->id;
                        }

                    }

                }

                if( $key == $this->total)
                {
                    foreach($odd->participants as $participant)
                    {
                        if($participant->short_name  == 'Over')
                        {
                            $item->match->over->odd = $this->numberFormatted($participant->value_us);
                            $item->match->over->val = $participant->short_name.' '.$participant->handicap;
                        }

                        if($participant->short_name  == 'Under')
                        {
                            $item->match->under->odd = $this->numberFormatted($participant->value_us);
                            $item->match->under->val = $participant->short_name.' '.$participant->handicap;
                        }
                    }
                }

                if($key == $this->money_line)
                {
                    foreach($odd->participants as $participant)
                    {
                        if($participant->short_name  == 'Home')
                        {
                            $item->next->first  =  $this->numberFormatted($participant->value_us);
                        }
                        if($participant->short_name  == 'Away')
                        {
                            $item->next->second   = $this->numberFormatted($participant->value_us);
                        }
                    }
                }
                $item->no_of_innings = 0;
            }
            $tag = false;
            foreach ($sortedbyleague as  $sbl) {
                if ($sbl->even_name == $item->league->name) {
                    $tag = true;
                    array_push($sbl->results, $item);
                    break;
                }
            }
            if (!$tag) {
             $obj = new stdClass();
             $obj->even_id   = $item->id;
             $obj->even_name = $item->league->name;
             $obj->results  =  array();
             array_push($obj->results, $item);
             array_push($sortedbyleague, $obj);
            }

        }
        return $sortedbyleague;
    }
}
