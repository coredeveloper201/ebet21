<?php
namespace App\Ebet\Sports\Straight;

use stdClass;
use App\Ebet\Sports\SportAbstract;
use App\Ebet\Sports\Contributor\WagerColumn;

class Baseball extends SportAbstract{

    use WagerColumn;
    public function __construct($url,$sport_id)
    {
        parent::__construct($url,$sport_id);
        $this->getOdds();
        $this->getPropsBet();
    }

    public function getOdds()
    {
        foreach($this->dataArr->category as $cat)
        {
            if($this->ifLeague($cat->attributes()->id) !=null)
            {
                if(isset($cat->matches))
                {
                    foreach($cat->matches->match as $match)
                    {
                        if(isset($match->attributes()->id))
                        {
                            $if_event = $this->ifEvent($match->attributes()->id);
                            if($if_event != null)
                            {
                                $item = $if_event;
                                if(isset($match->odds))
                                {
                                    foreach($match->odds->type as $type)
                                    {

                                        if(isset($type->attributes()->value) && ($type->attributes()->value == 'Home/Away' && $type->attributes()->id =='2'))
                                        {
                                            if(isset($type->bookmaker))
                                            {
                                                foreach($type->bookmaker as $bm){
                                                    $extn = new stdClass;
                                                    $extn->sport_id = 16;
                                                    $this->moneyLine($item->event_odd,$bm,$extn);
                                                }
                                            }
                                        }

                                        if(isset($type->attributes()->value) && ($type->attributes()->value =='Asian Handicap' && $type->attributes()->id =='4'))
                                        {
                                            if(isset($type->bookmaker))
                                            {
                                                foreach($type->bookmaker as $bm)
                                                {
                                                    if(isset($bm->handicap))
                                                    {
                                                        foreach($bm->handicap as $handicap)
                                                        {
                                                            $bmrk['name'] = isset($bm->attributes()->name)?$bm->attributes()->name:"";
                                                            $bmrk['id']   = isset($bm->attributes()->id)?$bm->attributes()->id:"";
                                                            $handicap->bm =  json_encode($bmrk);
                                                            $extn = new stdClass;
                                                            $extn->sport_id = 16;
                                                            $this->spread($item->event_odd,$handicap,$extn);
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                        if(isset($type->attributes()->value) && ($type->attributes()->value == 'Over/Under' && $type->attributes()->id == '5'))
                                        {
                                            if(isset($type->bookmaker))
                                            {
                                                foreach($type->bookmaker as $bm){

                                                    if(isset($bm->total))
                                                    {
                                                        foreach($bm->total as $total)
                                                        {
                                                            $bmrk['name'] = isset($bm->attributes()->name)?$bm->attributes()->name:"";
                                                            $bmrk['id']   = isset($bm->attributes()->id)?$bm->attributes()->id:"";
                                                            $total->bm =  json_encode($bmrk);
                                                            $extn = new stdClass;
                                                            $extn->sport_id = 16;
                                                            $this->total($item->event_odd,$total,$extn);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }


                    }
                }
                else{
                    dd($cat->attributes());
                }
            }
        }
    }

    public function getPropsBet(){
        foreach($this->dataArr->category as $cat)
        {
            if($this->ifLeague($cat->attributes()->id) !=null)
            {
                if(isset($cat->matches))
                {
                    foreach($cat->matches->match as $match)
                    {
                        if(isset($match->attributes()->id))
                        {
                            $if_event = $this->ifEvent($match->attributes()->id);
                            if($if_event != null)
                            {

                                $item = $if_event;
                                $item->prop_bets = [];
                                $item->prop_bets['first_five_innings'] = [];  //16.1
                                $item->prop_bets['first_innings'] = [];  //16.2
                                $item->prop_bets['odd_even'] = [];   //16.9
                                $item->prop_bets['team_total_home'] = [];  //16.7
                                $item->prop_bets['team_total_away'] = [];  //16.8
                                if(isset($match->odds))
                                {
                                    foreach($match->odds->type as $type)
                                    {
                                        //first_five_innings spread

                                        if(isset($type->attributes()->value) && ($type->attributes()->value == 'Asian Handicap (1st 5 Innings)' && $type->attributes()->id =='22640'))
                                        {
                                            if(isset($type->bookmaker))
                                            {
                                                foreach ($type->bookmaker as $bm) {
                                                    if(isset($bm->handicap))
                                                    {
                                                        foreach($bm->handicap as $handicap)
                                                        {
                                                            if($handicap->odd)
                                                            {
                                                                $bmrk['name'] = isset($bm->attributes()->name)?$bm->attributes()->name:"";
                                                                $bmrk['id'] = isset($bm->attributes()->id)?$bm->attributes()->id:"";
                                                                $handicap->bm =  json_encode($bmrk);
                                                                $extn = new stdClass;
                                                                $extn->sport_id = 18;
                                                                if($this->spread($item->prop_bets['first_five_innings'],$handicap,$extn)) break 2;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        //money line
                                        if(isset($type->attributes()->value) && ($type->attributes()->value == '1x2 (1st 5 Innings)' && $type->attributes()->id =='22721'))
                                        {
                                            if(isset($type->bookmaker))
                                            {
                                                foreach ($type->bookmaker as $bm) {

                                                    if(isset($bm->odd))
                                                    {
                                                        $extn = new stdClass;
                                                        $extn->sport_id = 18;
                                                        if($this->moneyLine($item->prop_bets['first_five_innings'],$bm,$extn))
                                                        {
                                                            break;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        //total
                                        if(isset($type->attributes()->value) && ($type->attributes()->value == 'Over/Under (1st 5 Innings)' && $type->attributes()->id =='22633'))
                                        {
                                            if(isset($type->bookmaker))
                                            {
                                                foreach ($type->bookmaker as $bm) {
                                                    if(isset($bm->total))
                                                    {
                                                        foreach($bm->total as $total)
                                                        {
                                                            $bmrk['name'] = isset($bm->attributes()->name)?$bm->attributes()->name:"";
                                                            $bmrk['id'] = isset($bm->attributes()->id)?$bm->attributes()->id:"";
                                                            $total->bm =  json_encode($bmrk);
                                                            $extn = new stdClass;
                                                            $extn->sport_id = 18;
                                                            if($this->total($item->prop_bets['first_five_innings'],$total,$extn)) break 2;
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                        //1st innings spread
                                        if(isset($type->attributes()->value) && ($type->attributes()->value == 'Asian Handicap (1st Inning)' && $type->attributes()->id =='22736'))
                                        {

                                            if(isset($type->bookmaker))
                                            {
                                                foreach ($type->bookmaker as $bm) {
                                                    if(isset($bm->handicap))
                                                    {
                                                        foreach($bm->handicap as $handicap)
                                                        {
                                                            if($handicap->odd)
                                                            {
                                                                $bmrk['name'] = isset($bm->attributes()->name)?$bm->attributes()->name:"";
                                                                $bmrk['id'] = isset($bm->attributes()->id)?$bm->attributes()->id:"";
                                                                $handicap->bm =  json_encode($bmrk);
                                                                $extn = new stdClass;
                                                                $extn->sport_id = 18;
                                                                    if( $this->spread($item->prop_bets['first_innings'],$handicap,$extn)) break 2;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        // money line
                                        if(isset($type->attributes()->value) && ($type->attributes()->value == '1x2 (1st Inning)' && $type->attributes()->id =='22634'))
                                        {
                                            if(isset($type->bookmaker))
                                            {
                                                foreach ($type->bookmaker as $bm) {

                                                    if(isset($bm->odd))
                                                    {
                                                        $extn = new stdClass;
                                                        $extn->sport_id = 18;
                                                        if( $this->moneyLine($item->prop_bets['first_innings'],$bm,$extn)) break;
                                                    }
                                                }
                                            }
                                        }

                                        if(isset($type->attributes()->value) && ($type->attributes()->value == 'Over/Under (1st Inning)' && $type->attributes()->id =='22722'))
                                        {
                                            if(isset($type->bookmaker))
                                            {
                                                foreach ($type->bookmaker as $bm) {

                                                    if(isset($bm->total))
                                                    {
                                                        foreach ($bm->total as $total) {
                                                            $bmrk['name'] = isset($bm->attributes()->name)?$bm->attributes()->name:"";
                                                            $bmrk['id'] = isset($bm->attributes()->id)?$bm->attributes()->id:"";
                                                            $total->bm =  json_encode($bmrk);
                                                            $extn = new stdClass;
                                                            $extn->sport_id = 18;
                                                            if( $this->total($item->prop_bets['first_innings'],$total,$total)) break 2;
                                                        }

                                                    }
                                                }
                                            }
                                        }




                                        //team total home
                                        if(isset($type->attributes()->value) && ($type->attributes()->value == 'Total - Home' && $type->attributes()->id =='22124'))
                                        {
                                            if(isset($type->bookmaker))
                                            {
                                                foreach($type->bookmaker as $bm)
                                                {
                                                    if(isset($bm->total))
                                                    {
                                                        foreach($bm->total as $total)
                                                        {
                                                            if(isset($total->attributes()->stop) &&  $total->attributes()->stop == "False")
                                                            {
                                                                $bmrk['name'] = isset($bm->attributes()->name)?$bm->attributes()->name:"";
                                                                $bmrk['id'] = isset($bm->attributes()->id)?$bm->attributes()->id:"";
                                                                $total->bm =  json_encode($bmrk);
                                                                $extn = new stdClass;
                                                                $extn->sport_id = 18;
                                                                if($this->total($item->prop_bets['team_total_home'],$total,$extn))
                                                                {
                                                                    break 2;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                        //team total away
                                        if(isset($type->attributes()->value) && ($type->attributes()->value == 'Total - Away' && $type->attributes()->id =='22125'))
                                        {
                                            if(isset($type->bookmaker))
                                            {
                                                foreach($type->bookmaker as $bm)
                                                {
                                                    if(isset($bm->total))
                                                    {
                                                        foreach($bm->total as $total)
                                                        {
                                                            if(isset($total->attributes()->stop) &&  $total->attributes()->stop == "False")
                                                            {
                                                                $bmrk['name'] = isset($bm->attributes()->name)?$bm->attributes()->name:"";
                                                                $bmrk['id'] = isset($bm->attributes()->id)?$bm->attributes()->id:"";
                                                                $total->bm =  json_encode($bmrk);
                                                                $extn = new stdClass;
                                                                $extn->sport_id = 18;
                                                                if($this->total($item->prop_bets['team_total_away'],$total,$extn))
                                                                {
                                                                    break 2;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                        // odd even
                                        if(isset($type->attributes()->value) && ($type->attributes()->value =='Odd/Even (Including OT)' && $type->attributes()->id =='22607'))
                                        {
                                            if(isset($type->bookmaker))
                                            {
                                                foreach($type->bookmaker as $bm)
                                                {
                                                    if(isset($bm->odd))
                                                    {
                                                        $this->oddEven($item->prop_bets['odd_even'],$bm);
                                                    }
                                                }

                                            }
                                        }

                                    }
                                }
                            }
                        }
                    }
                }
                else{
                    dd($cat->attributes());
                }
            }
        }
    }

    public function getItem()
    {
        return $this->leagues;
    }


    public function oddEven(array &$arr,$odd_obj)
    {
        $item  =   json_decode( json_encode( $odd_obj),true);
        if(isset($item['odd']))
        {
           if(is_array($item['odd']))
           {
                foreach($item['odd'] as $moneyLine)
                {
                    if(isset($moneyLine['@attributes']))
                    {
                        $arr[$moneyLine['@attributes']['name']] = $moneyLine['@attributes']['value'];
                    }
                }
           }
        }
    }







}



