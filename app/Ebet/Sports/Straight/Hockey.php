<?php
namespace App\Ebet\Sports\Straight;

use stdClass;
use App\Ebet\Sports\SportAbstract;
use App\Ebet\Sports\Contributor\WagerColumn;

class Hockey extends SportAbstract{
    use WagerColumn;
    public function __construct($url,$sport_id)
    {
        parent::__construct($url,$sport_id);
        $this->getOdds();
        $this->getPropsBet();
    }

    public function getOdds()
    {
        foreach($this->dataArr->category as $cat)
        {
            if($this->ifLeague($cat->attributes()->id) !=null)
            {
                if(isset($cat->matches))
                {
                    foreach($cat->matches->match as $match)
                    {
                        if(isset($match->attributes()->id))
                        {
                            $if_event = $this->ifEvent($match->attributes()->id);
                            if($if_event != null)
                            {
                                $item = $if_event;
                                if(isset($match->odds))
                                {
                                    foreach($match->odds->type as $type)
                                    {
                                        if(isset($type->attributes()->value) && ($type->attributes()->value =='3Way Result' && $type->attributes()->id =='1'))
                                        {
                                            if(isset($type->bookmaker))
                                            {
                                                foreach($type->bookmaker as $bm)
                                                {
                                                    if($bm->attributes()->name == 'Marathon' && $bm->attributes()->id == 17)
                                                    {
                                                        $extn = new stdClass;
                                                        $extn->sport_id = 17;
                                                        $this->moneyLine($item->event_odd,$bm,$extn);
                                                    }
                                                }
                                            }
                                        }

                                        if(isset($type->attributes()->value) && ($type->attributes()->value =='Asian Handicap' && $type->attributes()->id =='4'))
                                        {
                                            if(isset($type->bookmaker))
                                            {
                                                foreach($type->bookmaker as $bm)
                                                {

                                                    if(isset($bm->handicap))
                                                    {
                                                        foreach($bm->handicap as $handicap)
                                                        {
                                                            $bmrk['name'] = isset($bm->attributes()->name)?$bm->attributes()->name:"";
                                                            $bmrk['id'] = isset($bm->attributes()->id)?$bm->attributes()->id:"";
                                                            $handicap->bm =  json_encode($bmrk);
                                                            $extn = new stdClass;
                                                            $extn->sport_id = 17;
                                                            $this->spread($item->event_odd,$handicap,$extn);
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                        if(isset($type->attributes()->value) && ($type->attributes()->value == 'Over/Under' && $type->attributes()->id == '5'))
                                        {
                                            if(isset($type->bookmaker))
                                            {
                                                foreach($type->bookmaker as $bm)
                                                {
                                                    if(isset($bm->total))
                                                    {
                                                        foreach($bm->total as $total)
                                                        {
                                                            $bmrk['name'] = isset($bm->attributes()->name)?$bm->attributes()->name:"";
                                                            $bmrk['id'] = isset($bm->attributes()->id)?$bm->attributes()->id:"";
                                                            $total->bm =  json_encode($bmrk);
                                                            $extn = new stdClass;
                                                            $extn->sport_id = 17;
                                                            $this->total($item->event_odd,$total,$extn);
                                                        }

                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else{
                    dd($cat['@attributes']);
                }
            }
        }
    }

    public function getPropsBet(){
        foreach($this->dataArr->category as $cat)
        {
            if($this->ifLeague($cat->attributes()->id) !=null)
            {
                if(isset($cat->matches))
                {
                    foreach($cat->matches->match as $match)
                    {
                        if(isset($match->attributes()->id))
                        {
                            $if_event = $this->ifEvent($match->attributes()->id);
                            if($if_event != null)
                            {
                                $item = $if_event;
                                $item->prop_bets = [];
                                $item->prop_bets['first_period'] = array();  //17.10
                                $item->prop_bets['home_total_goals'] = [];  //17.7  d
                                $item->prop_bets['away_total_goals'] = [];  //17.8  d
                                $item->prop_bets['odd_even_ot'] = [];       //17.9
                                $item->prop_bets['odd_even'] = [];    //inactive
                                $item->prop_bets['highest_scoring_half'] = [];  //17.16
                                $item->prop_bets['both_teams_to_score'] = []; //17.11
                                $item->prop_bets['total_home'] = [];     //inactive
                                $item->prop_bets['total_away'] = [];     //inactive
                                $item->prop_bets['team_to_score_first'] = []; //inactive
                                $item->prop_bets['team_to_score_last'] = [];  //inactive
                                $item->prop_bets['home_team_will_score_a_goal'] = []; //17.14
                                $item->prop_bets['away_team_will_score_a_goal'] = []; //17.15
                                if(isset($match->odds))
                                {
                                    foreach($match->odds->type as $type)
                                    {
                                            //first period
                                            if(isset($type->attributes()->value) && ($type->attributes()->value =='Asian Handicap (1st Period)' && $type->attributes()->id =='22687'))
                                            {
                                                if(isset($type->bookmaker))
                                                {
                                                    if( ($type->bookmaker->attributes()->name == 'bet365' && $type->bookmaker->attributes()->id == 16)
                                                    ||  ($type->bookmaker->attributes()->name == 'Marathon' && $type->bookmaker->attributes()->id == 17)
                                                    )
                                                    {
                                                        if(isset($type->bookmaker->handicap))
                                                        {
                                                            foreach($type->bookmaker->handicap as $handicap)
                                                            {
                                                                if(isset($handicap->odd))
                                                                {
                                                                  $this->spread($item->prop_bets['first_period'],$handicap);
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }

                                            if(isset($type->attributes()->value) && ($type->attributes()->value =='Home/Away (1st Period)' && $type->attributes()->id =='22781'))
                                            {
                                                if(isset($type->bookmaker))
                                                {
                                                    if( ($type->bookmaker->attributes()->name == 'bet365' && $type->bookmaker->attributes()->id == 16)
                                                    ||  ($type->bookmaker->attributes()->name == 'Marathon' && $type->bookmaker->attributes()->id == 17)
                                                     )
                                                    {
                                                        if(isset($type->bookmaker->odd))
                                                        {
                                                            $this->moneyLine($item->prop_bets['first_period'],$type->bookmaker);
                                                        }
                                                    }
                                                }
                                            }


                                            if(isset($type->attributes()->value) && ($type->attributes()->value == 'Over/Under (1st Period)' && $type->attributes()->id == '22623'))
                                            {
                                                if(isset($type->bookmaker))
                                                {
                                                    if( ($type->bookmaker->attributes()->name == 'bet365' && $type->bookmaker->attributes()->id == 16)
                                                    ||  ($type->bookmaker->attributes()->name == 'Marathon' && $type->bookmaker->attributes()->id == 17)
                                                    )
                                                    {
                                                        if(isset($type->bookmaker->total))
                                                        {
                                                            $this->total($item->prop_bets['first_period'],$type->bookmaker->total[0]);
                                                        }
                                                    }
                                                }
                                            }

                                            //home_total_goals
                                            if(isset($type->attributes()->value) && ($type->attributes()->value == 'Home Team Total Goals (Including OT)' && $type->attributes()->id =='22871'))
                                            {
                                                if(isset($type->bookmaker))
                                                {
                                                    if( ($type->bookmaker->attributes()->name == 'bet365' && $type->bookmaker->attributes()->id == 16)
                                                    ||  ($type->bookmaker->attributes()->name == 'Marathon' && $type->bookmaker->attributes()->id == 17)
                                                    )
                                                    {
                                                       $this->home_total_goals( $item->prop_bets['home_total_goals'], $type->bookmaker);
                                                    }
                                                }
                                            }

                      /*                       //home total
                                            if(isset($type->attributes()->value) && ($type->attributes()->value == 'Total - Home' && $type->attributes()->id =='22124'))
                                            {
                                                if(isset($type->bookmaker))
                                                {
                                                    foreach($type->bookmaker as $bm)
                                                    {
                                                        if( ($bm->attributes()->name == 'bet365' && $bm->attributes()->id == 16)
                                                        ||  ($bm->attributes()->name == 'Marathon' && $bm->attributes()->id == 17)
                                                        )
                                                        {
                                                           $this->home_total($item->prop_bets['total_home'], $bm);
                                                        }
                                                    }

                                                }
                                            }

                                            //away total
                                            if(isset($type->attributes()->value) && ($type->attributes()->value == 'Total - Away' && $type->attributes()->id =='22125'))
                                            {
                                                if(isset($type->bookmaker))
                                                {
                                                    foreach($type->bookmaker as $bm)
                                                    {
                                                        if( ($bm->attributes()->name == 'bet365' && $bm->attributes()->id == 16)
                                                        ||  ($bm->attributes()->name == 'Marathon' && $bm->attributes()->id == 17)
                                                        )
                                                        {
                                                           $this->away_total($item->prop_bets['total_away'], $bm);
                                                        }
                                                    }

                                                }
                                            } */
                                            //away_total_goals
                                            if(isset($type->attributes()->value) && ($type->attributes()->value == 'Away Team Total Goals (Including OT)' && $type->attributes()->id =='22872'))
                                            {
                                                if(isset($type->bookmaker))
                                                {
                                                    if( ($type->bookmaker->attributes()->name == 'bet365' && $type->bookmaker->attributes()->id == 16)
                                                    ||  ($type->bookmaker->attributes()->name == 'Marathon' && $type->bookmaker->attributes()->id == 17)
                                                    )
                                                    {
                                                        $this->away_total_goals( $item->prop_bets['away_total_goals'],$type->bookmaker);
                                                    }
                                                }
                                            }
                                            //odd/even OT
                                            if(isset($type->attributes()->value) && ($type->attributes()->value == 'Odd/Even (Including OT)' && $type->attributes()->id =='22607'))
                                            {
                                                if(isset($type->bookmaker))
                                                {
                                                    foreach ($type->bookmaker as $bm) {
                                                        if( ($bm->attributes()->name == 'bet365' && $bm->attributes()->id == 16)
                                                        ||  ($bm->attributes()->name == 'Marathon' && $bm->attributes()->id == 17) )
                                                        {
                                                            $this->oddEven($item->prop_bets['odd_even_ot'], $bm);
                                                        }
                                                    }

                                                }
                                            }

                                          /*   //odd/even
                                            if(isset($type->attributes()->value) && ($type->attributes()->value == 'Odd/Even' && $type->attributes()->id =='22608'))
                                            {
                                                if(isset($type->bookmaker))
                                                {
                                                    foreach ($type->bookmaker as $bm) {
                                                        if( ($bm->attributes()->name == 'bet365' && $bm->attributes()->id == 16)
                                                        ||  ($bm->attributes()->name == 'Marathon' && $bm->attributes()->id == 17) )
                                                        {
                                                            $this->oddEven($item->prop_bets['odd_even'], $bm);
                                                        }
                                                    }

                                                }
                                            } */

                                            //Home team will score a goal
                                            if(isset($type->attributes()->value) && ($type->attributes()->value == 'Home team will score a goal' && $type->attributes()->id =='22927'))
                                            {
                                                if(isset($type->bookmaker))
                                                {
                                                    foreach ($type->bookmaker as $bm) {
                                                        if( ($bm->attributes()->name == 'bet365' && $bm->attributes()->id == 16)
                                                        ||  ($bm->attributes()->name == 'Marathon' && $bm->attributes()->id == 17) )
                                                        {
                                                            $this->oddEven($item->prop_bets['home_team_will_score_a_goal'], $bm);
                                                        }
                                                    }

                                                }
                                            }

                                            //Away team will score a goal
                                            if(isset($type->attributes()->value) && ($type->attributes()->value == 'Away team will score a goal' && $type->attributes()->id =='22928'))
                                            {
                                                if(isset($type->bookmaker))
                                                {
                                                    foreach ($type->bookmaker as $bm) {
                                                        if( ($bm->attributes()->name == 'bet365' && $bm->attributes()->id == 16)
                                                        ||  ($bm->attributes()->name == 'Marathon' && $bm->attributes()->id == 17) )
                                                        {
                                                            $this->oddEven($item->prop_bets['away_team_will_score_a_goal'], $bm);
                                                        }
                                                    }

                                                }
                                            }
                                             //highest_scoring_half
                                            if(isset($type->attributes()->value) && ($type->attributes()->value == 'Highest Scoring Half' && $type->attributes()->id =='91'))
                                            {
                                                if(isset($type->bookmaker))
                                                {
                                                    if( ($type->bookmaker->attributes()->name == 'bet365' && $type->bookmaker->attributes()->id == 16)
                                                    ||  ($type->bookmaker->attributes()->name == 'Marathon' && $type->bookmaker->attributes()->id == 17)
                                                    )
                                                    {
                                                        $this->highestScoreHalf($item->prop_bets['highest_scoring_half'], $type->bookmaker);
                                                    }
                                                }
                                            }

                                            //both team to score
                                            if(isset($type->attributes()->value) && ($type->attributes()->value == 'Both Teams To Score' && $type->attributes()->id =='15'))
                                            {
                                                if(isset($type->bookmaker))
                                                {
                                                    foreach ($type->bookmaker as $bm) {

                                                        if( ($bm->attributes()->name == 'bet365' && $bm->attributes()->id == 16)
                                                        ||  ($bm->attributes()->name == 'Marathon' && $bm->attributes()->id == 17)
                                                        )
                                                        {
                                                            $this->bothTeamToScore($item->prop_bets['both_teams_to_score'], $bm);
                                                        }
                                                    }

                                                }
                                            }

                                           /*  //Team to score first
                                            if(isset($type->attributes()->value) && ($type->attributes()->value =='Team To Score First' && $type->attributes()->id =='2224'))
                                            {
                                                if(isset($type->bookmaker))
                                                {
                                                    foreach ($type->bookmaker as $bm) {

                                                        if( ($bm->attributes()->name == 'bet365' && $bm->attributes()->id == 16)
                                                        ||  ($bm->attributes()->name == 'Marathon' && $bm->attributes()->id == 17)
                                                        )
                                                        {
                                                            $this->highestScoreHalf($item->prop_bets['team_to_score_first'], $bm);
                                                        }
                                                    }

                                                }
                                            }

                                            //Team to score last
                                            if(isset($type->attributes()->value) && ($type->attributes()->value =='Team To Score Last' && $type->attributes()->id =='2225'))
                                            {
                                                if(isset($type->bookmaker))
                                                {
                                                    foreach ($type->bookmaker as $bm) {

                                                        if( ($bm->attributes()->name == 'bet365' && $bm->attributes()->id == 16)
                                                        ||  ($bm->attributes()->name == 'Marathon' && $bm->attributes()->id == 17)
                                                        )
                                                        {
                                                            $this->highestScoreHalf($item->prop_bets['team_to_score_last'], $bm);
                                                        }
                                                    }

                                                }
                                            } */

                                    }
                                }
                            }
                        }


                    }
                }
                else{
                    dd($cat['@attributes']);
                }
            }
        }
    }

    public function getItem()
    {
        return $this->leagues;
    }

    public function oddEven(array &$arr,$odd_obj)
    {
        $item  =   json_decode( json_encode( $odd_obj),true);
        if(isset($item['odd']))
        {
           if(is_array($item['odd']))
           {
                foreach($item['odd'] as $moneyLine)
                {
                    if(isset($moneyLine['@attributes']))
                    {
                        $arr[$moneyLine['@attributes']['name']] = $moneyLine['@attributes']['value'];
                    }
                }
           }
        }
    }
    public function bothTeamToScore(array &$arr,$odd_obj)
    {
        $item  =   json_decode( json_encode( $odd_obj),true);
        if(isset($item['odd']))
        {
           if(is_array($item['odd']))
           {
                foreach($item['odd'] as $moneyLine)
                {
                    if(isset($moneyLine['@attributes']))
                    {
                        $arr[$moneyLine['@attributes']['name']] = $moneyLine['@attributes']['value'];
                    }
                }
           }
        }
    }

    public function highestScoreHalf(array &$arr,$odd_obj)
    {
        $item  =   json_decode( json_encode( $odd_obj),true);
        if(isset($item['odd']))
        {
            if(is_array($item['odd']))
            {
                foreach($item['odd'] as $spread)
                {
                    if(isset($spread['@attributes']))
                    {
                        if(isset($spread['@attributes']['name']))
                        {
                            $arr[]= [
                                'name' =>   $spread['@attributes']['name'],
                                'value' =>  $spread['@attributes']['value'],
                            ];
                        }
                    }
                }
            }
        }
    }

    public function home_total_goals(array &$arr,$odd_obj)
    {
        $item  = json_decode(json_encode($odd_obj),true);

        if(isset($item['total']))
        {
            if(is_array($item['total']))
            {
                foreach($item['total'] as $total)
                {
                    if(isset($total['odd']))
                    {
                        if(is_array($total['odd']))
                        {
                            foreach($total['odd'] as $total_odd)
                            {
                                if(isset($total_odd['@attributes']))
                                {
                                    $arr['home'][$total_odd['@attributes']['name']][]  = $total_odd['@attributes']['value'];
                                }
                            }
                        }
                    }
                    if(isset($total['@attributes'])) {
                        if(isset($total['@attributes']['name']) && !empty($total['@attributes']['name']))
                        {
                            $arr['home']['wager'][] = $total['@attributes']['name'];
                        }
                    }

                }
            }
        }
    }

    public function home_total(array &$arr,$odd_obj)
    {
        $item  = json_decode(json_encode($odd_obj),true);

        if(isset($item['total']))
        {
            if(is_array($item['total']))
            {
                foreach($item['total'] as $total)
                {
                    if(isset($total['odd']))
                    {
                        if(is_array($total['odd']))
                        {
                            foreach($total['odd'] as $total_odd)
                            {
                                if(isset($total_odd['@attributes']))
                                {
                                    $arr['home'][$total_odd['@attributes']['name']][]  = $total_odd['@attributes']['value'];
                                }
                            }
                        }
                    }
                    if(isset($total['@attributes'])) {
                        if(isset($total['@attributes']['name']) && !empty($total['@attributes']['name']))
                        {
                            $arr['home']['wager'][] = $total['@attributes']['name'];
                        }
                    }

                }
            }
        }
    }

    public function away_total_goals(array &$arr,$odd_obj)
    {
        $item  = json_decode(json_encode($odd_obj),true);

        if(isset($item['total']))
        {
            if(is_array($item['total']))
            {
                foreach($item['total'] as $total)
                {
                    if(isset($total['odd']))
                    {
                        if(is_array($total['odd']))
                        {
                            foreach($total['odd'] as $total_odd)
                            {
                                if(isset($total_odd['@attributes']))
                                {
                                        $arr['away'][$total_odd['@attributes']['name']][]  = $total_odd['@attributes']['value'];
                                }
                            }
                        }
                    }
                    if(isset($total['@attributes'])) {
                        if(isset($total['@attributes']['name']) && !empty($total['@attributes']['name']))
                        {
                            $arr['away']['wager'][] = $total['@attributes']['name'];
                        }
                    }

                }
            }
        }
    }

    public function away_total(array &$arr,$odd_obj)
    {
        $item  = json_decode(json_encode($odd_obj),true);

        if(isset($item['total']))
        {
            if(is_array($item['total']))
            {
                foreach($item['total'] as $total)
                {
                    if(isset($total['odd']))
                    {
                        if(is_array($total['odd']))
                        {
                            foreach($total['odd'] as $total_odd)
                            {
                                if(isset($total_odd['@attributes']))
                                {
                                        $arr['away'][$total_odd['@attributes']['name']][]  = $total_odd['@attributes']['value'];
                                }
                            }
                        }
                    }
                    if(isset($total['@attributes'])) {
                        if(isset($total['@attributes']['name']) && !empty($total['@attributes']['name']))
                        {
                            $arr['away']['wager'][] = $total['@attributes']['name'];
                        }
                    }

                }
            }
        }
    }








}



