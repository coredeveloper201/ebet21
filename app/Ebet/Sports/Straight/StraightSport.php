<?php
namespace App\Ebet\Sports\Straight;


use App\Ebet\Sports\SportAbstract;

class StraightSport
{
    public $straightSport;
    public function __construct(SportAbstract $sportAbstract)
    {
        $this->straightSport = $sportAbstract;
    }

    public function getLeagues()
    {
        return $this->straightSport->getItem();
    }
}
