<?php
namespace App\Ebet\Sports\Straight;

use stdClass;
use App\Ebet\Sports\SportAbstract;
use App\Ebet\Sports\Contributor\WagerColumn;

class Esports extends SportAbstract{
    use WagerColumn;
    private $url;
    public function __construct($url,$sport_id)
    {
        parent::__construct($url,$sport_id);
        $this->getOdds();
    }


    public function getOdds()
    {
        foreach($this->dataArr->match as $cat)
        {
            if($this->ifLeague($cat->attributes()->league_id) !=null)
            {
                  if(isset($cat->attributes()->id))
                    {
                        $if_event = $this->ifEvent($cat->attributes()->id);
                        if($if_event != null)
                        {
                            $item = $if_event;
                            if(isset($cat->odds))
                            {
                                foreach($cat->odds->type as $type)
                                {
                                    if(!empty($type->attributes()->value) && ($type->attributes()->value =='Home/Away' && $type->attributes()->id =='2'))
                                    {
                                        if(isset($type->bookmaker) && count($type->bookmaker->attributes()) > 0)
                                        {
                                            foreach($type->bookmaker as $bm)
                                            {
                                                $extn = new stdClass;
                                                $extn->sport_id = 151;
                                                $this->moneyLine($item->event_odd , $bm, $extn);
                                            }
                                        }
                                    }


                                    if(!empty($type->attributes()->value) && ($type->attributes()->value =='Home/Away (map 1)' && $type->attributes()->id =='22649'))
                                    {

                                        if(isset($type->bookmaker) && count($type->bookmaker->attributes()) > 0)
                                        {
                                            foreach($type->bookmaker as $bm)
                                            {
                                                $extn = new stdClass;
                                                $extn->sport_id = 151;
                                                $this->moneyLine2($item->event_odd , $bm, $extn);
                                            }
                                        }
                                    }

                                    if($type->attributes()->value =='Maps Handicap' && $type->attributes()->id =='22662')
                                    {

                                        if(isset($type->bookmaker) && count($type->bookmaker->attributes()) > 0)
                                        {
                                            foreach($type->bookmaker as $bm){

                                                if(isset($bm->handicap))
                                                {
                                                    foreach($bm->handicap as $handicap)
                                                    {
                                                        $bmrk['name'] = isset($bm->attributes()->name)?$bm->attributes()->name:"";
                                                        $bmrk['id'] = isset($bm->attributes()->id)?$bm->attributes()->id:"";
                                                        $handicap->bm =  json_encode($bmrk);
                                                        $extn = new stdClass;
                                                        $extn->sport_id = 151;
                                                        $this->spread($item->event_odd,$handicap,$extn);
                                                    }
                                                }
                                            }
                                        }
                                    }

                                }
                            }
                        }
                    }

                else{
                    dd($cat['@attributes']);
                }
            }
        }
    }

    public function getItem()
    {
        return $this->leagues;
    }








}



