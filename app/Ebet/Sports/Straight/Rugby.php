<?php
namespace App\Ebet\Sports\Straight;

use stdClass;
use App\Ebet\Sports\SportAbstract;
use App\Ebet\Sports\Contributor\WagerColumn;

class Rugby extends SportAbstract{

    use WagerColumn;
    private $url;
    public function __construct($url,$sport_id)
    {
        parent::__construct($url,$sport_id);
        $this->getOdds();
    }


    public function getOdds()
    {
        foreach($this->dataArr->category as $cat)
        {
            if($this->ifLeague($cat->attributes()->id) !=null)
            {
                if(isset($cat->matches))
                {
                    foreach($cat->matches->match as $match)
                    {
                        if(isset($match->attributes()->id))
                        {
                            $if_event = $this->ifEvent($match->attributes()->id);
                            if($if_event != null)
                            {
                                $item = $if_event;
                                if(isset($match->odds))
                                {
                                    foreach($match->odds->type as $type)
                                    {
                                        if(isset($type->attributes()->value) && ($type->attributes()->value == 'Home/Away' && $type->attributes()->id =='2'))
                                        {
                                            if(isset($type->bookmaker))
                                            {
                                                foreach($type->bookmaker as $bm){
                                                    $extn = new stdClass;
                                                    $extn->sport_id = 20;
                                                    $this->moneyLine($item->event_odd,$bm,$extn);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }


                    }
                }
                else{
                    dd($cat['@attributes']);
                }
            }
        }
    }

    public function getItem()
    {
        return $this->leagues;
    }








}



