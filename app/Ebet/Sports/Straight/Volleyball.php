<?php
namespace App\Ebet\Sports\Straight;

use stdClass;
use App\Ebet\Sports\SportAbstract;
use App\Ebet\Sports\Contributor\WagerColumn;

class Volleyball extends SportAbstract{

    use WagerColumn;

    public function __construct($url,$sport_id)
    {
        parent::__construct($url,$sport_id);
        $this->getOdds();
        $this->getPropsBet();
    }

    public function getOdds()
    {
        foreach($this->dataArr->category as $cat)
        {
            if($this->ifLeague($cat->attributes()->id) !=null)
            {
                if(isset($cat->matches))
                {
                    foreach($cat->matches->match as $match)
                    {
                        if(count($match->attributes()) > 0 && isset($match->attributes()->id))
                        {
                            $if_event = $this->ifEvent($match->attributes()->id);
                            if($if_event != null)
                            {
                                $item = $if_event;
                                if(isset($match->odds))
                                {
                                    foreach($match->odds->type as $type)
                                    {
                                        if($type->attributes()->value =='Home/Away' && $type->attributes()->id =='2')
                                        {
                                            if(isset($type->bookmaker) && count($type->bookmaker->attributes()) > 0)
                                            {
                                                foreach($type->bookmaker as $bm)
                                                {
                                                        $extn = new stdClass;
                                                        $extn->sport_id = 14;
                                                        if($this->moneyLine($item->event_odd,$bm,$extn)){
                                                            break;
                                                        }

                                                }
                                            }
                                        }

                                        if($type->attributes()->id =='22631')
                                        {
                                            if(isset($type->bookmaker) && count($type->bookmaker->attributes()) > 0)
                                            {
                                                foreach($type->bookmaker as $bm)
                                                {
                                                    if(isset($bm->handicap))
                                                    {
                                                        foreach($bm->handicap as $handicap)
                                                        {
                                                            $bmrk['name'] = isset($bm->attributes()->name)?$bm->attributes()->name:"";
                                                            $bmrk['id'] = isset($bm->attributes()->id)?$bm->attributes()->id:"";
                                                            $handicap->bm =  json_encode($bmrk);
                                                            $extn = new stdClass;
                                                            $extn->sport_id = 14;
                                                           if($this->spread($item->event_odd,$handicap,$extn))
                                                            {
                                                                break 2;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                        if(count($type->attributes()) > 0 && ($type->attributes()->value =='Over/Under by Games in Match' && $type->attributes()->id =='22624'))
                                        {
                                            if(isset($type->bookmaker) && count($type->bookmaker->attributes()) > 0)
                                            {
                                                foreach($type->bookmaker as $bm){
                                                    if(isset($bm->total))
                                                    {
                                                        foreach($bm->total as $total){
                                                            $bmrk['name'] = isset($bm->attributes()->name)?$bm->attributes()->name:"";
                                                            $bmrk['id'] = isset($bm->attributes()->id)?$bm->attributes()->id:"";
                                                            $total->bm =  json_encode($bmrk);
                                                            $extn = new stdClass;
                                                            $extn->sport_id = 14;
                                                            if($this->total($item->event_odd,$total,$extn))
                                                            {
                                                                break 2;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else{
                    dd($cat['@attributes']);
                }
            }
        }
    }


    public function getPropsBet(){
        foreach($this->dataArr->category as $cat)
        {
            if($this->ifLeague($cat->attributes()->id) !=null)
            {
                if(isset($cat->matches))
                {
                    foreach($cat->matches->match as $match)
                    {
                        if(count($match->attributes()) > 0 && isset($match->attributes()->id))
                        {
                            $if_event = $this->ifEvent($match->attributes()->id);
                            if($if_event != null)
                            {
                                $item = $if_event;
                                $item->prop_bets = [];
                                $item->prop_bets['team_total'] = [];  //14.7  14.8
                                $item->prop_bets['first_set'] = []; //14.1
                                $item->prop_bets['odd_even'] = [];   //14.9
                                if(isset($match->odds))
                                {
                                    foreach($match->odds->type as $type)
                                    {
                                        //first set
                                        if(isset($type->attributes()->value) && ($type->attributes()->value =='Asian Handicap (1st Set)' && $type->attributes()->id =='22629'))
                                        {
                                            if(isset($type->bookmaker))
                                            {
                                                foreach ($type->bookmaker as $bm) {


                                                        if(isset($bm->handicap))
                                                        {
                                                            foreach($bm->handicap as $handicap)
                                                            {
                                                                $bmrk['name'] = isset($bm->attributes()->name)?$bm->attributes()->name:"";
                                                                $bmrk['id'] = isset($bm->attributes()->id)?$bm->attributes()->id:"";
                                                                $handicap->bm =  json_encode($bmrk);
                                                                $extn = new stdClass;
                                                                $extn->sport_id = 14;
                                                                if($this->spread($item->prop_bets['first_set'],$handicap,$extn)) break 2;
                                                            }
                                                        }

                                                }

                                            }
                                        }
                                        if($type->attributes()->value =='Home/Away (1st Set)' && $type->attributes()->id =='22628')
                                        {
                                            if(isset($type->bookmaker))
                                            {
                                                foreach ($type->bookmaker as $bm) {
                                                    $extn = new stdClass;
                                                    $extn->sport_id = 14;
                                                    if($this->moneyLine($item->prop_bets['first_set'],$bm,$extn))
                                                    {
                                                        break;
                                                    }
                                                }
                                            }
                                        }

                                        if(isset($type->attributes()->value) && ($type->attributes()->value == 'Over/Under (1st Set)' && $type->attributes()->id == '22627'))
                                        {
                                            if(isset($type->bookmaker))
                                            {
                                                foreach ($type->bookmaker as $bm) {

                                                    if(isset($bm->total))
                                                    {
                                                        foreach($bm->total as $total)
                                                        {
                                                            $bmrk['name'] = isset($bm->attributes()->name)?$bm->attributes()->name:"";
                                                            $bmrk['id'] = isset($bm->attributes()->id)?$bm->attributes()->id:"";
                                                            $total->bm =  json_encode($bmrk);
                                                            $extn = new stdClass;
                                                            $extn->sport_id = 14;
                                                            if($this->total($item->prop_bets['first_set'],$total,$extn)) break 2;
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                        // odd even
                                        if(isset($type->attributes()->value) && ($type->attributes()->value =='Odd/Even' && $type->attributes()->id =='22608'))
                                        {
                                            if(isset($type->bookmaker))
                                            {
                                                if($type->bookmaker->attributes()->name == 'bet365' && $type->bookmaker->attributes()->id == 16)
                                                {
                                                    if(isset($type->bookmaker->odd))
                                                    {
                                                        $this->oddEven($item->prop_bets['odd_even'],$type->bookmaker);
                                                    }
                                                }
                                            }
                                        }

                                        // team total home
                                        if(count($type->attributes()) > 0 && ($type->attributes()->value =='Total - Home' && $type->attributes()->id =='22124'))
                                        {
                                            if(isset($type->bookmaker) && count($type->bookmaker->attributes()) > 0)
                                            {
                                                if($type->bookmaker->attributes()->name == 'bet365' && $type->bookmaker->attributes()->id == 16)
                                                {
                                                    if(isset($type->bookmaker->total))
                                                    {
                                                        $this->teamTotalHome($item->prop_bets['team_total'], $type->bookmaker->total[0]);
                                                    }
                                                }
                                            }
                                        }
                                        // team total away
                                        if(count($type->attributes()) > 0 && ($type->attributes()->value =='Total - Away' && $type->attributes()->id =='22125'))
                                        {
                                            if(isset($type->bookmaker) && count($type->bookmaker->attributes()) > 0)
                                            {
                                                if($type->bookmaker->attributes()->name == 'bet365' && $type->bookmaker->attributes()->id == 16)
                                                {
                                                    if(isset($type->bookmaker->total))
                                                    {
                                                      $this->teamTotalAway($item->prop_bets['team_total'] , $type->bookmaker->total[0]);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                            }
                        }


                    }
                }
                else{
                    dd($cat['@attributes']);
                }
            }
        }
    }

    public function oddEven(array &$arr,$odd_obj)
    {
        $item  =   json_decode( json_encode( $odd_obj),true);
        if(isset($item['odd']))
        {
           if(is_array($item['odd']))
           {
                foreach($item['odd'] as $moneyLine)
                {
                    if(isset($moneyLine['@attributes']))
                    {
                        $arr[$moneyLine['@attributes']['name']] = $moneyLine['@attributes']['value'];
                    }
                }
           }
        }
    }

    public function teamTotalHome(array &$arr,$odd_obj)
    {
        $item  = json_decode(json_encode($odd_obj),true);

        if(isset($item['odd']))
        {
            if(is_array($item['odd']))
            {
                foreach($item['odd'] as $total)
                {
                    if(isset($total['@attributes']))
                    {
                        $arr['home'][$total['@attributes']['name']]  =  $total['@attributes']['value'];
                    }
                }
            }
        }

        if(isset($item['@attributes']))
        {
            if(isset($item['@attributes']['name']) && !empty($item['@attributes']['name']))
            {
                $arr['home']['wager']= $item['@attributes']['name'];
            }
        }
    }
    public function teamTotalAway(array &$arr,$odd_obj)
    {
        $item  = json_decode(json_encode($odd_obj),true);

        if(isset($item['odd']))
        {
            if(is_array($item['odd']))
            {
                foreach($item['odd'] as $total)
                {
                    if(isset($total['@attributes']))
                    {
                        $arr['away'][$total['@attributes']['name']]  =  $total['@attributes']['value'];
                    }
                }
            }
        }

        if(isset($item['@attributes']))
        {
            if(isset($item['@attributes']['name']) && !empty($item['@attributes']['name']))
            {
                $arr['away']['wager']= $item['@attributes']['name'];
            }
        }
    }
    public function getItem()
    {
        return $this->leagues;
    }








}



