<?php
namespace App\Ebet\Sports\Straight;

use stdClass;
use App\Ebet\Sports\SportAbstract;
use App\Ebet\Sports\Contributor\WagerColumn;

class Football extends SportAbstract{

    use WagerColumn;
    public function __construct($url,$sport_id)
    {
        parent::__construct($url,$sport_id);
        $this->getOdds();
        $this->getPropsBet();
    }

    public function getOdds()
    {
        foreach($this->dataArr->category as $cat)
        {
            if($this->ifLeague($cat->attributes()->id) !=null)
            {
                if(isset($cat->matches))
                {
                    foreach($cat->matches->match as $match)
                    {
                        if(isset($match->attributes()->id))
                        {
                            $if_event = $this->ifEvent($match->attributes()->id);
                            if($if_event != null)
                            {
                                $item = $if_event;
                                if(isset($match->odds))
                                {
                                    foreach($match->odds->type as $type)
                                    {
                                        if(isset($type->attributes()->value) && ($type->attributes()->value == 'Home/Away' && $type->attributes()->id =='2'))
                                        {
                                            if(isset($type->bookmaker))
                                            {
                                                foreach($type->bookmaker as $bm)
                                                {
                                                    if($bm->attributes()->name == 'Sbo' && $bm->attributes()->id == 88)
                                                    {
                                                        if(isset($bm->odd))
                                                        {
                                                            $this->moneyLine($item->event_odd,$bm);
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                        if(isset($type->attributes()->value) && ($type->attributes()->value =='Asian Handicap' && $type->attributes()->id =='4'))
                                        {
                                            if(isset($type->bookmaker))
                                            {
                                                foreach($type->bookmaker as $bm)
                                                {
                                                    if($bm->attributes()->name == 'Sbo' && $bm->attributes()->id ==88)
                                                    {
                                                        if(isset($bm->handicap))
                                                        {
                                                            foreach($bm->handicap as $handicap)
                                                            {
                                                                if($handicap->attributes()->stop == "False")
                                                                {
                                                                    $bmrk['name'] = isset($bm->attributes()->name)?$bm->attributes()->name:"";
                                                                    $bmrk['id'] = isset($bm->attributes()->id)?$bm->attributes()->id:"";
                                                                    $handicap->bm =  json_encode($bmrk);
                                                                    $this->spread($item->event_odd,$handicap);
                                                                }

                                                            }
                                                        }
                                                    }
                                                }

                                            }
                                        }

                                        if(isset($type->attributes()->value) && ($type->attributes()->value == 'Over/Under' && $type->attributes()->id == '5'))
                                        {
                                            if(isset($type->bookmaker))
                                            {
                                                foreach($type->bookmaker as $bm)
                                                {
                                                    if($bm->attributes()->name == 'Sbo' && $bm->attributes()->id == 88)
                                                    {
                                                        if(isset($bm->total))
                                                        {
                                                            foreach($bm->total as $total) {

                                                                if($total->attributes()->stop == "False")
                                                                {
                                                                    $bmrk['name'] = isset($bm->attributes()->name)?$bm->attributes()->name:"";
                                                                    $bmrk['id'] = isset($bm->attributes()->id)?$bm->attributes()->id:"";
                                                                    $total->bm =  json_encode($bmrk);
                                                                    $this->total($item->event_odd,$total);
                                                                }
                                                            }
                                                        }

                                                    }
                                                }

                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else{
                    dd($cat->attributes());
                }
            }
        }
    }

    public function getPropsBet(){
        foreach($this->dataArr->category as $cat)
        {
            if($this->ifLeague($cat->attributes()->id) !=null)
            {
                if(isset($cat->matches))
                {
                    foreach($cat->matches->match as $match)
                    {
                        if(isset($match->attributes()->id))
                        {
                            $if_event = $this->ifEvent($match->attributes()->id);
                            if($if_event != null)
                            {
                                $item = $if_event;
                                $item->prop_bets = [];
                                $item->prop_bets['first_half'] = [];  //12.1
                                $item->prop_bets['second_half'] = [];  //12.2
                                $item->prop_bets['first_qtr'] = [];    //12.3
                                $item->prop_bets['overtime'] = [];     //12.4
                                $item->prop_bets['winning_margin'] = [];  //12.5
                                $item->prop_bets['highest_scoring_qtr'] = [];  //12.6
                                $item->prop_bets['team_total_home'] = [];  //12.7
                                $item->prop_bets['team_total_away'] = [];  //12.8
                                $item->prop_bets['odd_even'] = [];   //12.9
                                if(isset($match->odds))
                                {
                                    foreach($match->odds->type as $type)
                                    {
                                        //first half

                                        if(isset($type->attributes()->value) && ($type->attributes()->value == 'Asian Handicap First Half' && $type->attributes()->id =='22601'))
                                        {
                                            if(isset($type->bookmaker))
                                            {
                                                foreach ($type->bookmaker as $bm) {

                                                    if($bm->attributes()->name == 'bet365' && $bm->attributes()->id == 16)
                                                    {
                                                        if(isset($bm->handicap))
                                                        {
                                                            foreach($bm->handicap as $handicap)
                                                            {
                                                                if($handicap->odd)
                                                                {
                                                                    $bmrk['name'] = isset($bm->attributes()->name)?$bm->attributes()->name:"";
                                                                    $bmrk['id'] = isset($bm->attributes()->id)?$bm->attributes()->id:"";
                                                                    $handicap->bm =  json_encode($bmrk);
                                                                    $extn = new stdClass;
                                                                    $extn->sport_id = 18;
                                                                    if($this->spread($item->prop_bets['first_half'],$handicap,$extn)) break 2;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                        if(isset($type->attributes()->value) && ($type->attributes()->value == 'Home/Away - 1st Half' && $type->attributes()->id =='22678'))
                                        {
                                            if(isset($type->bookmaker))
                                            {
                                                foreach ($type->bookmaker as $bm) {

                                                    if($bm->attributes()->name == 'bet365' && $bm->attributes()->id == 16)
                                                    {
                                                        if(isset($bm->odd))
                                                        {
                                                            $extn = new stdClass;
                                                            $extn->sport_id = 18;
                                                            if($this->moneyLine($item->prop_bets['first_half'],$bm,$extn))
                                                            {
                                                                break;
                                                            }
                                                        }
                                                    }

                                                }
                                            }
                                        }

                                        if(isset($type->attributes()->value) && ($type->attributes()->value == 'Over/Under 1st Half' && $type->attributes()->id =='6'))
                                        {
                                            if(isset($type->bookmaker))
                                            {
                                                foreach ($type->bookmaker as $bm) {
                                                    if($bm->attributes()->name == 'bet365' && $bm->attributes()->id == 16)
                                                    {
                                                        if(isset($bm->total))
                                                        {
                                                            foreach($bm->total as $total)
                                                            {
                                                                $bmrk['name'] = isset($bm->attributes()->name)?$bm->attributes()->name:"";
                                                                $bmrk['id'] = isset($bm->attributes()->id)?$bm->attributes()->id:"";
                                                                $total->bm =  json_encode($bmrk);
                                                                $extn = new stdClass;
                                                                $extn->sport_id = 18;
                                                                if($this->total($item->prop_bets['first_half'],$total,$extn)) break 2;
                                                            }
                                                        }
                                                    }

                                                }
                                            }
                                        }

                                        //second half
                                        if(isset($type->attributes()->value) && ($type->attributes()->value == 'Asian Handicap (2nd Half)' && $type->attributes()->id =='22664'))
                                        {

                                            if(isset($type->bookmaker))
                                            {
                                                foreach ($type->bookmaker as $bm) {
                                                    if(isset($bm->handicap))
                                                    {
                                                        foreach($bm->handicap as $handicap)
                                                        {
                                                            if($handicap->odd)
                                                            {
                                                                $bmrk['name'] = isset($bm->attributes()->name)?$bm->attributes()->name:"";
                                                                $bmrk['id'] = isset($bm->attributes()->id)?$bm->attributes()->id:"";
                                                                $handicap->bm =  json_encode($bmrk);
                                                                $extn = new stdClass;
                                                                $extn->sport_id = 18;
                                                                    if( $this->spread($item->prop_bets['second_half'],$handicap,$extn)) break 2;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                        if(isset($type->attributes()->value) && ($type->attributes()->value == '2nd Half 3Way Result' && $type->attributes()->id =='3'))
                                        {
                                            if(isset($type->bookmaker))
                                            {
                                                foreach ($type->bookmaker as $bm) {

                                                    if(isset($bm->odd))
                                                    {
                                                        $extn = new stdClass;
                                                        $extn->sport_id = 18;
                                                        if( $this->moneyLine($item->prop_bets['second_half'],$bm,$extn)) break;

                                                    }
                                                }
                                            }
                                        }

                                        if(isset($type->attributes()->value) && ($type->attributes()->value == 'Over/Under 2nd Half' && $type->attributes()->id =='7'))
                                        {
                                            if(isset($type->bookmaker))
                                            {
                                                foreach ($type->bookmaker as $bm) {

                                                    if(isset($bm->total))
                                                    {
                                                        foreach ($bm->total as $total) {
                                                            $bmrk['name'] = isset($bm->attributes()->name)?$bm->attributes()->name:"";
                                                            $bmrk['id'] = isset($bm->attributes()->id)?$bm->attributes()->id:"";
                                                            $total->bm =  json_encode($bmrk);
                                                            $extn = new stdClass;
                                                            $extn->sport_id = 18;

                                                            if( $this->total($item->prop_bets['second_half'],$total,$total)) break 2;

                                                        }

                                                    }
                                                }
                                            }
                                        }


                                        //first quatar
                                        if(isset($type->attributes()->value) && ($type->attributes()->value == 'Asian Handicap 1st Qtr' && $type->attributes()->id =='22661'))
                                        {

                                            if(isset($type->bookmaker))
                                            {
                                                foreach ($type->bookmaker as $bm) {
                                                    if(isset($bm->handicap))
                                                    {
                                                        foreach($bm->handicap as $handicap)
                                                        {
                                                            if($handicap->odd)
                                                            {
                                                                $bmrk['name'] = isset($bm->attributes()->name)?$bm->attributes()->name:"";
                                                                $bmrk['id'] = isset($bm->attributes()->id)?$bm->attributes()->id:"";
                                                                $handicap->bm =  json_encode($bmrk);
                                                                $extn = new stdClass;
                                                                $extn->sport_id = 18;
                                                                if($this->spread($item->prop_bets['first_qtr'],$handicap,$extn)) break 2;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                        if(isset($type->attributes()->value) && ($type->attributes()->value == 'Home/Away - 1st Qtr' && $type->attributes()->id =='22680'))
                                        {
                                            if(isset($type->bookmaker))
                                            {
                                                foreach($type->bookmaker as $bm)
                                                {

                                                    if(isset($bm->odd))
                                                    {
                                                        $extn = new stdClass;
                                                        $extn->sport_id = 18;
                                                        if($this->moneyLine($item->prop_bets['first_qtr'],$bm,$extn)) break;
                                                    }


                                                }
                                            }
                                        }

                                        if(isset($type->attributes()->value) && ($type->attributes()->value == 'Over/Under 1st Qtr' && $type->attributes()->id =='22646'))
                                        {
                                            if(isset($type->bookmaker))
                                            {
                                                foreach ($type->bookmaker as $bm) {

                                                    if(isset($bm->total))
                                                    {
                                                        foreach($bm->total as $total)
                                                        {
                                                            $bmrk['name'] = isset($bm->attributes()->name)?$bm->attributes()->name:"";
                                                            $bmrk['id'] = isset($bm->attributes()->id)?$bm->attributes()->id:"";
                                                            $total->bm =  json_encode($bmrk);
                                                            $extn = new stdClass;
                                                            $extn->sport_id = 18;
                                                            $this->total($item->prop_bets['first_qtr'],$total,$extn);
                                                        }

                                                    }

                                                }
                                            }
                                        }

                                        //overtime
                                        if(isset($type->attributes()->value) && ($type->attributes()->value == 'Overtime' && $type->attributes()->id =='22606'))
                                        {
                                            if(isset($type->bookmaker))
                                            {
                                                foreach ($type->bookmaker as $bm) {
                                                    if(isset($bm->odd))
                                                    {
                                                        $this->overTimePropBets($item->prop_bets['overtime'],$bm);
                                                    }
                                                }
                                            }
                                        }



                                         //winning_margin home
                                         if(isset($type->attributes()->value) && ($type->attributes()->value == 'Home Winning Margin (14-Way)' && $type->attributes()->id =='22619'))
                                         {
                                             if(isset($type->bookmaker))
                                             {
                                                 foreach ($type->bookmaker as $bm) {
                                                     $bmrk['name'] = isset($bm->attributes()->name)?$bm->attributes()->name:"";
                                                     $bmrk['id'] = isset($bm->attributes()->id)?$bm->attributes()->id:"";
                                                     $bm->bm =  json_encode($bmrk);
                                                     $this->winningMargin($item->prop_bets['winning_margin'] , $bm,'home');
                                                 }
                                             }
                                         }
                                         //winning_margin away
                                         if(isset($type->attributes()->value) && ($type->attributes()->value == 'Away Winning Margin (14-Way)' && $type->attributes()->id =='22620'))
                                         {
                                             if(isset($type->bookmaker))
                                             {
                                                 foreach ($type->bookmaker as $bm) {
                                                     $bmrk['name'] = isset($bm->attributes()->name)?$bm->attributes()->name:"";
                                                     $bmrk['id'] = isset($bm->attributes()->id)?$bm->attributes()->id:"";
                                                     $bm->bm =  json_encode($bmrk);
                                                     $this->winningMargin($item->prop_bets['winning_margin'] , $bm,'away');
                                                 }
                                             }
                                         }

                                         //highest_scoring_qtr
                                        if(isset($type->attributes()->value) && ($type->attributes()->value == 'Highest Scoring Quarter' && $type->attributes()->id =='22703'))
                                        {
                                            if(isset($type->bookmaker))
                                            {
                                                foreach($type->bookmaker as $bm)
                                                {
                                                    $bmrk['name'] = isset($bm->attributes()->name)?$bm->attributes()->name:"";
                                                    $bmrk['id'] = isset($bm->attributes()->id)?$bm->attributes()->id:"";
                                                    $bm->bm =  json_encode($bmrk);
                                                    if($this->highestScoreHalf($item->prop_bets['highest_scoring_qtr'],$bm)) break;
                                                }
                                            }
                                        }

                                        //team total home
                                        if(isset($type->attributes()->value) && ($type->attributes()->value == 'Total - Home' && $type->attributes()->id =='22124'))
                                        {
                                            if(isset($type->bookmaker))
                                            {
                                                foreach($type->bookmaker as $bm)
                                                {
                                                    if($bm->attributes()->name == 'bet365' && $bm->attributes()->id == 16)
                                                    {
                                                        if(isset($bm->total))
                                                        {
                                                            foreach($bm->total as $total)
                                                            {
                                                                if(isset($total->attributes()->stop) &&  $total->attributes()->stop == "False")
                                                                {
                                                                    $bmrk['name'] = isset($bm->attributes()->name)?$bm->attributes()->name:"";
                                                                    $bmrk['id'] = isset($bm->attributes()->id)?$bm->attributes()->id:"";
                                                                    $total->bm =  json_encode($bmrk);
                                                                    $extn = new stdClass;
                                                                    $extn->sport_id = 18;
                                                                    if($this->total($item->prop_bets['team_total_home'],$total,$extn))
                                                                    {
                                                                        break 2;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                        //team total away
                                        if(isset($type->attributes()->value) && ($type->attributes()->value == 'Total - Away' && $type->attributes()->id =='22125'))
                                        {
                                            if(isset($type->bookmaker))
                                            {
                                                foreach($type->bookmaker as $bm)
                                                {
                                                    if($bm->attributes()->name == 'bet365' && $bm->attributes()->id == 16)
                                                    {
                                                        if(isset($bm->total))
                                                        {
                                                            foreach($bm->total as $total)
                                                            {
                                                                if(isset($total->attributes()->stop) &&  $total->attributes()->stop == "False")
                                                                {
                                                                    $bmrk['name'] = isset($bm->attributes()->name)?$bm->attributes()->name:"";
                                                                    $bmrk['id'] = isset($bm->attributes()->id)?$bm->attributes()->id:"";
                                                                    $total->bm =  json_encode($bmrk);
                                                                    $extn = new stdClass;
                                                                    $extn->sport_id = 18;
                                                                    if($this->total($item->prop_bets['team_total_away'],$total,$extn))
                                                                    {
                                                                        break 2;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                        // odd even
                                        if(isset($type->attributes()->value) && ($type->attributes()->value =='Odd/Even' && $type->attributes()->id =='22608'))
                                        {
                                            if(isset($type->bookmaker))
                                            {
                                                foreach($type->bookmaker as $bm)
                                                {
                                                    if($bm->attributes()->name == 'Sbo' && $bm->attributes()->id == 88)
                                                    {
                                                        if(isset($bm->odd))
                                                        {
                                                            $this->oddEven($item->prop_bets['odd_even'],$bm);
                                                        }
                                                    }
                                                }

                                            }
                                        }

                                    }
                                }
                            }
                        }
                    }
                }
                else{
                    dd($cat->attributes());
                }
            }
        }
    }

    public function getItem()
    {
        return $this->leagues;
    }

    public function oddEven(array &$arr,$odd_obj)
    {
        $item  =   json_decode( json_encode( $odd_obj),true);
        if(isset($item['odd']))
        {
           if(is_array($item['odd']))
           {
                foreach($item['odd'] as $moneyLine)
                {
                    if(isset($moneyLine['@attributes']))
                    {
                        $arr[$moneyLine['@attributes']['name']] = $moneyLine['@attributes']['value'];
                    }
                }
           }
        }
    }


    public function overTimePropBets(array &$arr,$odd_obj)
    {
        $item  =   json_decode( json_encode( $odd_obj),true);
        if(isset($item['odd']))
        {
           if(is_array($item['odd']))
           {
                foreach($item['odd'] as $moneyLine)
                {
                    if(isset($moneyLine['@attributes']))
                    {
                        if($moneyLine['@attributes']['name'] == 'Yes')
                        {
                            $arr['home'] =  $moneyLine['@attributes']['value'];
                        }

                        if($moneyLine['@attributes']['name'] == 'No')
                        {
                            $arr['away'] =  $moneyLine['@attributes']['value'];
                        }
                    }
                }
           }
        }
    }


    public function highestScoreHalf(array &$arr,$odd_obj)
    {
        $item  =  json_decode( json_encode( $odd_obj),true);
        $bbm   =  isset($item['bm'])? json_decode($item['bm'],true): [];
        if(isset($item['odd']))
        {
            if(is_array($item['odd']))
            {
                foreach($item['odd'] as $spread)
                {
                    if(isset($spread['@attributes']))
                    {
                        if(isset($spread['@attributes']['name']))
                        {
                            $arr['odds'][]= [
                                'name' =>   $spread['@attributes']['name'],
                                'value' =>  $spread['@attributes']['value'],
                            ];
                        }
                    }
                }
            }
        }

        if(empty($arr)) return;
        else{

            if(!empty($bbm))
            {
                $arr['bm']['name'] =  $bbm['name'];
                $arr['bm']['id']   =  $bbm['id'];
            }
            return true;
        }

    }

    public function winningMargin(array &$arr,$odd_obj,$flag='')
    {
            $item  = json_decode(json_encode($odd_obj),true);
            $bbm   =  isset($item['bm'])? json_decode($item['bm'],true): [];
            if(isset($item['odd']))
            {
                if(is_array($item['odd']))
                {
                    foreach($item['odd'] as $total_odd)
                    {
                        if(isset($total_odd['@attributes']))
                        {
                            if(!empty($flag) && $flag == 'home')
                            {
                                $arr['odds'][$total_odd['@attributes']['name']]['home'] = $total_odd['@attributes']['value'];
                            }
                            elseif(!empty($flag) && $flag == 'away'){
                                $arr['odds'][$total_odd['@attributes']['name']]['away'] = $total_odd['@attributes']['value'];
                            }
                        }
                    }
                }
            }
            if(empty($arr)) return;

            if(!empty($bbm))
            {
                $arr['bm']['name'] =  $bbm['name'];
                $arr['bm']['id']   =  $bbm['id'];
            }
    }

}



