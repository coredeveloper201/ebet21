<?php
namespace App\Ebet\Sports\Straight;

use stdClass;
use App\Ebet\Sports\SportAbstract;
use App\Ebet\Sports\Contributor\WagerColumn;

class Basketball extends SportAbstract{

    use WagerColumn;
    public function __construct($url,$sport_id)
    {
        parent::__construct($url,$sport_id);
        $this->getOdds();
        $this->getPropsBet();
    }

    public function getOdds()
    {
        foreach($this->dataArr->category as $cat)
        {
            if($this->ifLeague($cat->attributes()->id) !=null)
            {
                if(isset($cat->matches))
                {

                    foreach($cat->matches->match as $match)
                    {
                        if(isset($match->attributes()->id))
                        {
                            $if_event = $this->ifEvent($match->attributes()->id);
                            if($if_event != null)
                            {
                                $item = $if_event;
                                if(isset($match->odds))
                                {
                                    foreach($match->odds->type as $type)
                                    {

                                        if(isset($type->attributes()->value) && ($type->attributes()->value == 'Home/Away' && $type->attributes()->id =='2'))
                                        {
                                            if(isset($type->bookmaker))
                                            {
                                                foreach($type->bookmaker as $bm)
                                                {
                                                    $extn = new stdClass;
                                                    $extn->sport_id = 18;
                                                    $this->moneyLine($item->event_odd,$bm,$extn);
                                                }

                                            }
                                        }

                                        if(isset($type->attributes()->value) && ($type->attributes()->value =='Asian Handicap' && $type->attributes()->id =='4'))
                                        {
                                            if(isset($type->bookmaker))
                                            {
                                                foreach($type->bookmaker as $bm)
                                                {
                                                    if(isset($bm->handicap))
                                                        {
                                                            foreach($bm->handicap as $handicap)
                                                            {
                                                                if(isset($handicap->attributes()->ismain) &&  $handicap->attributes()->ismain == "True")
                                                                {
                                                                    $bmrk['name'] = isset($bm->attributes()->name)?$bm->attributes()->name:"";
                                                                    $bmrk['id'] = isset($bm->attributes()->id)?$bm->attributes()->id:"";
                                                                    $handicap->bm =  json_encode($bmrk);
                                                                    $extn = new stdClass;
                                                                    $extn->sport_id = 18;
                                                                    $this->spread($item->event_odd,$handicap,$extn);
                                                                }
                                                            }
                                                        }
                                                }

                                            }
                                        }

                                        if(isset($type->attributes()->value) && ($type->attributes()->value =='Over/Under' && $type->attributes()->id =='5'))
                                        {
                                            if(isset($type->bookmaker))
                                            {
                                                foreach($type->bookmaker as $bm)
                                                {
                                                    if(isset($bm->total))
                                                    {
                                                        foreach($bm->total as $total)
                                                        {
                                                            if(isset($total->attributes()->ismain) &&  $total->attributes()->ismain == "True")
                                                            {
                                                                $bmrk['name'] = isset($bm->attributes()->name)?$bm->attributes()->name:"";
                                                                $bmrk['id'] = isset($bm->attributes()->id)?$bm->attributes()->id:"";
                                                                $total->bm =  json_encode($bmrk);
                                                                $extn = new stdClass;
                                                                $extn->sport_id = 18;
                                                                $this->total($item->event_odd,$total,$extn);
                                                            }
                                                        }

                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                            }
                        }
                    }
                }
                else{
                    dd($cat->attributes());
                }
            }
        }
    }

    public function getPropsBet(){
        foreach($this->dataArr->category as $cat)
        {
            if($this->ifLeague($cat->attributes()->id) !=null)
            {
                if(isset($cat->matches))
                {
                    foreach($cat->matches->match as $match)
                    {
                        if(isset($match->attributes()->id))
                        {
                            $if_event = $this->ifEvent($match->attributes()->id);
                            if($if_event != null)
                            {
                                $item = $if_event;
                                $item->prop_bets = [];
                                $item->prop_bets['first_half'] = []; //18.1 d
                                $item->prop_bets['first_qtr'] = [];  //18.3 d
                                $item->prop_bets['highest_scoring_half'] = []; //18.16
                                $item->prop_bets['highest_scoring_qtr'] = []; //12.6  d
                                $item->prop_bets['home_total_goals'] = [];  //18.7  d
                                $item->prop_bets['away_total_goals'] = [];  //18.8  d
                                $item->prop_bets['winning_margin'] = []; //18.5 d
                                $item->prop_bets['odd_even'] = []; //18.9  d
                                if(isset($match->odds))
                                {
                                    foreach($match->odds->type as $type)
                                    {
                                        //first half
                                        if(isset($type->attributes()->value) && ($type->attributes()->value =='Asian Handicap First Half' && $type->attributes()->id =='22601'))
                                        {
                                            if(isset($type->bookmaker))
                                            {
                                                foreach($type->bookmaker as $bm)
                                                {
                                                    if(isset($bm->handicap))
                                                    {
                                                        foreach($bm->handicap as $handicap)
                                                        {

                                                                if(isset($handicap->attributes()->ismain) &&  $handicap->attributes()->ismain == "True")
                                                                {
                                                                    $bmrk['name'] = isset($bm->attributes()->name)?$bm->attributes()->name:"";
                                                                    $bmrk['id'] = isset($bm->attributes()->id)?$bm->attributes()->id:"";
                                                                    $handicap->bm =  json_encode($bmrk);
                                                                    $extn = new stdClass;
                                                                    $extn->sport_id = 18;
                                                                    $this->spread($item->prop_bets['first_half'],$handicap,$extn);
                                                                }

                                                        }
                                                    }
                                                }
                                            }
                                        }

                                        if(isset($type->attributes()->value) && ($type->attributes()->value == 'Home/Away - 1st Half' && $type->attributes()->id =='22678'))
                                        {
                                            if(isset($type->bookmaker))
                                            {
                                                foreach($type->bookmaker as $bm)
                                                {
                                                    $extn = new stdClass;
                                                    $extn->sport_id = 18;
                                                    $this->moneyLine($item->prop_bets['first_half'],$bm,$extn);
                                                }
                                            }
                                        }

                                        if(isset($type->attributes()->value) && ($type->attributes()->value =='Over/Under 1st Half' && $type->attributes()->id =='6'))
                                        {
                                            if(isset($type->bookmaker))
                                            {
                                                foreach($type->bookmaker as $bm)
                                                {
                                                    if(isset($bm->total))
                                                    {
                                                        foreach($bm->total as $total)
                                                        {
                                                            if(isset($total->attributes()->ismain) &&  $total->attributes()->ismain == "True")
                                                            {
                                                                $bmrk['name'] = isset($bm->attributes()->name)?$bm->attributes()->name:"";
                                                                $bmrk['id'] = isset($bm->attributes()->id)?$bm->attributes()->id:"";
                                                                $total->bm =  json_encode($bmrk);
                                                                $extn = new stdClass;
                                                                $extn->sport_id = 18;
                                                                $this->total($item->prop_bets['first_half'],$total,$extn);
                                                            }
                                                        }

                                                    }
                                                }
                                            }
                                        }
                                        //first qtr
                                        if(isset($type->attributes()->value) && ($type->attributes()->value =='Asian Handicap 1st Qtr' && $type->attributes()->id =='22661'))
                                        {
                                            if(isset($type->bookmaker))
                                            {
                                                foreach($type->bookmaker as $bm)
                                                {
                                                    if(isset($bm->handicap))
                                                    {
                                                        foreach($bm->handicap as $handicap)
                                                        {
                                                            if(isset($handicap->attributes()->ismain) &&  $handicap->attributes()->ismain == "True")
                                                            {
                                                                $bmrk['name'] = isset($bm->attributes()->name)?$bm->attributes()->name:"";
                                                                $bmrk['id'] = isset($bm->attributes()->id)?$bm->attributes()->id:"";
                                                                $handicap->bm =  json_encode($bmrk);
                                                                $extn = new stdClass;
                                                                $extn->sport_id = 18;
                                                                $this->spread($item->prop_bets['first_qtr'],$handicap,$extn);
                                                            }

                                                        }
                                                    }
                                                }
                                            }
                                        }

                                        if(isset($type->attributes()->value) && ($type->attributes()->value == 'Home/Away - 1st Qtr' && $type->attributes()->id =='22680'))
                                        {
                                            if(isset($type->bookmaker))
                                            {
                                                foreach($type->bookmaker as $bm)
                                                {
                                                    $extn = new stdClass;
                                                    $extn->sport_id = 18;
                                                    $this->moneyLine($item->prop_bets['first_qtr'],$bm,$extn);
                                                }
                                            }
                                        }

                                        if(isset($type->attributes()->value) && ($type->attributes()->value =='Over/Under 1st Qtr' && $type->attributes()->id =='22646'))
                                        {
                                            if(isset($type->bookmaker))
                                            {
                                                foreach ($type->bookmaker as $bm) {

                                                    if(isset($bm->total))
                                                    {
                                                        foreach($bm->total as $total)
                                                        {
                                                            if(isset($total->attributes()->ismain) &&  $total->attributes()->ismain == "True")
                                                            {
                                                                $bmrk['name'] = isset($bm->attributes()->name)?$bm->attributes()->name:"";
                                                                $bmrk['id'] = isset($bm->attributes()->id)?$bm->attributes()->id:"";
                                                                $total->bm =  json_encode($bmrk);
                                                                $extn = new stdClass;
                                                                $extn->sport_id = 18;
                                                                $this->total($item->prop_bets['first_qtr'],$total,$extn);
                                                            }
                                                        }

                                                    }
                                                }
                                            }
                                        }

                                        //highest_scoring_half
                                        if(isset($type->attributes()->value) && ($type->attributes()->value == 'Highest Scoring Half' && $type->attributes()->id =='91'))
                                        {
                                            if(isset($type->bookmaker))
                                            {
                                                foreach ($type->bookmaker as $bm) {
                                                    $bmrk['name'] = isset($bm->attributes()->name)?$bm->attributes()->name:"";
                                                    $bmrk['id'] = isset($bm->attributes()->id)?$bm->attributes()->id:"";
                                                    $bm->bm =  json_encode($bmrk);
                                                    $this->highestScoreHalf($item->prop_bets['highest_scoring_half'],$bm);
                                                }
                                            }
                                        }

                                        //highest_scoring_qtr
                                        if(isset($type->attributes()->value) && ($type->attributes()->value == 'Highest Scoring Quarter' && $type->attributes()->id =='22703'))
                                        {
                                            if(isset($type->bookmaker))
                                            {
                                                foreach($type->bookmaker as $bm)
                                                {
                                                    $bmrk['name'] = isset($bm->attributes()->name)?$bm->attributes()->name:"";
                                                    $bmrk['id'] = isset($bm->attributes()->id)?$bm->attributes()->id:"";
                                                    $bm->bm =  json_encode($bmrk);
                                                    $this->highestScoreHalf($item->prop_bets['highest_scoring_qtr'],$bm);
                                                }
                                            }
                                        }
                                        //home_total_goals
                                        if(isset($type->attributes()->value) && ($type->attributes()->value == 'Home Team Total Goals (Including OT)' && $type->attributes()->id =='22871'))
                                        {
                                            if(isset($type->bookmaker))
                                            {
                                                foreach ($type->bookmaker as $bm) {
                                                    $bmrk['name'] = isset($bm->attributes()->name)?$bm->attributes()->name:"";
                                                    $bmrk['id'] = isset($bm->attributes()->id)?$bm->attributes()->id:"";
                                                    $bm->bm =  json_encode($bmrk);
                                                    $this->home_total_goals($item->prop_bets['home_total_goals'],$bm);
                                                }
                                            }
                                        }
                                        //away_total_goals
                                        if(isset($type->attributes()->value) && ($type->attributes()->value == 'Away Team Total Goals (Including OT)' && $type->attributes()->id =='22872'))
                                        {
                                            if(isset($type->bookmaker))
                                            {
                                                foreach ($type->bookmaker as $bm) {
                                                    $bmrk['name'] = isset($bm->attributes()->name)?$bm->attributes()->name:"";
                                                    $bmrk['id'] = isset($bm->attributes()->id)?$bm->attributes()->id:"";
                                                    $bm->bm =  json_encode($bmrk);
                                                    $this->away_total_goals($item->prop_bets['away_total_goals'],$bm);
                                                }
                                            }
                                        }
                                        //winning_margin home
                                        if(isset($type->attributes()->value) && ($type->attributes()->value == 'Home Winning Margin (14-Way)' && $type->attributes()->id =='22619'))
                                        {
                                            if(isset($type->bookmaker))
                                            {
                                                foreach ($type->bookmaker as $bm) {
                                                    $bmrk['name'] = isset($bm->attributes()->name)?$bm->attributes()->name:"";
                                                    $bmrk['id'] = isset($bm->attributes()->id)?$bm->attributes()->id:"";
                                                    $bm->bm =  json_encode($bmrk);
                                                    $this->winningMargin($item->prop_bets['winning_margin'] , $bm,'home');
                                                }
                                            }
                                        }
                                        //winning_margin away
                                        if(isset($type->attributes()->value) && ($type->attributes()->value == 'Away Winning Margin (14-Way)' && $type->attributes()->id =='22620'))
                                        {
                                            if(isset($type->bookmaker))
                                            {
                                                foreach ($type->bookmaker as $bm) {
                                                    $bmrk['name'] = isset($bm->attributes()->name)?$bm->attributes()->name:"";
                                                    $bmrk['id'] = isset($bm->attributes()->id)?$bm->attributes()->id:"";
                                                    $bm->bm =  json_encode($bmrk);
                                                    $this->winningMargin($item->prop_bets['winning_margin'] , $bm,'away');
                                                }
                                            }
                                        }

                                        // Odd/Even (Including OT)
                                        if(isset($type->attributes()->value) && ($type->attributes()->value =='Odd/Even (Including OT)' && $type->attributes()->id =='22607'))
                                        {
                                            if(isset($type->bookmaker))
                                            {
                                                foreach($type->bookmaker as $bm)
                                                {
                                                    if($bm->attributes()->name == 'bet365' && $bm->attributes()->id == 16)
                                                    {
                                                        if(isset($bm->odd))
                                                        {
                                                            $this->oddEven($item->prop_bets['odd_even'],$bm);
                                                        }
                                                    }
                                                }

                                            }
                                        }



                                    }
                                }
                            }
                        }
                    }
                }
                else{
                    dd($cat->attributes());
                }
            }
        }
    }


    public function oddEven(array &$arr,$odd_obj)
    {
        $item  =   json_decode( json_encode( $odd_obj),true);
        if(isset($item['odd']))
        {
           if(is_array($item['odd']))
           {
                foreach($item['odd'] as $moneyLine)
                {
                    if(isset($moneyLine['@attributes']))
                    {
                        $arr[$moneyLine['@attributes']['name']] = $moneyLine['@attributes']['value'];
                    }
                }
           }
        }
    }

    public function highestScoreHalf(array &$arr,$odd_obj)
    {
        $item  =  json_decode( json_encode( $odd_obj),true);
        $bbm   =  isset($item['bm'])? json_decode($item['bm'],true): [];
        if(isset($item['odd']))
        {
            if(is_array($item['odd']))
            {
                foreach($item['odd'] as $spread)
                {
                    if(isset($spread['@attributes']))
                    {
                        if(isset($spread['@attributes']['name']))
                        {
                            $arr['odds'][]= [
                                'name' =>   $spread['@attributes']['name'],
                                'value' =>  $spread['@attributes']['value'],
                            ];
                        }
                    }
                }
            }
        }

        if(empty($arr)) return;

        if(!empty($bbm))
        {
            $arr['bm']['name'] =  $bbm['name'];
            $arr['bm']['id']   =  $bbm['id'];
        }
    }

    public function home_total_goals(array &$arr,$odd_obj)
    {
        $item  = json_decode(json_encode($odd_obj),true);
        $bbm   =  isset($item['bm'])? json_decode($item['bm'],true): [];

        if(isset($item['total']))
        {
            if(is_array($item['total']))
            {
                foreach($item['total'] as $total)
                {
                    if(isset($total['odd']))
                    {
                        if(is_array($total['odd']))
                        {
                            foreach($total['odd'] as $total_odd)
                            {
                                if(isset($total_odd['@attributes']))
                                {
                                    $arr['home'][$total_odd['@attributes']['name']][]  = $total_odd['@attributes']['value'];
                                }
                            }
                        }
                    }
                    if(isset($total['@attributes'])) {
                        if(isset($total['@attributes']['name']) && !empty($total['@attributes']['name']))
                        {
                            $arr['home']['wager'][] = $total['@attributes']['name'];
                        }
                    }

                }
            }
        }

        if(empty($arr)) return;

        if(!empty($bbm))
        {
            $arr['home']['bm']['name'] =  $bbm['name'];
            $arr['home']['bm']['id']   =  $bbm['id'];
        }
    }

    public function away_total_goals(array &$arr,$odd_obj)
    {
        $item  = json_decode(json_encode($odd_obj),true);
        $bbm   =  isset($item['bm'])? json_decode($item['bm'],true): [];
        if(isset($item['total']))
        {
            if(is_array($item['total']))
            {
                foreach($item['total'] as $total)
                {
                    if(isset($total['odd']))
                    {
                        if(is_array($total['odd']))
                        {
                            foreach($total['odd'] as $total_odd)
                            {
                                if(isset($total_odd['@attributes']))
                                {
                                        $arr['away'][$total_odd['@attributes']['name']][]  = $total_odd['@attributes']['value'];
                                }
                            }
                        }
                    }
                    if(isset($total['@attributes'])) {
                        if(isset($total['@attributes']['name']) && !empty($total['@attributes']['name']))
                        {
                            $arr['away']['wager'][] = $total['@attributes']['name'];
                        }
                    }

                }
            }
        }
        if(empty($arr)) return;

        if(!empty($bbm))
        {
            $arr['away']['bm']['name'] =  $bbm['name'];
            $arr['away']['bm']['id']   =  $bbm['id'];
        }
    }

    public function winningMargin(array &$arr,$odd_obj,$flag='')
    {
            $item  = json_decode(json_encode($odd_obj),true);
            $bbm   =  isset($item['bm'])? json_decode($item['bm'],true): [];
            if(isset($item['odd']))
            {
                if(is_array($item['odd']))
                {
                    foreach($item['odd'] as $total_odd)
                    {
                        if(isset($total_odd['@attributes']))
                        {
                            if(!empty($flag) && $flag == 'home')
                            {
                                $arr['odds'][$total_odd['@attributes']['name']]['home'] = $total_odd['@attributes']['value'];
                            }
                            elseif(!empty($flag) && $flag == 'away'){
                                $arr['odds'][$total_odd['@attributes']['name']]['away'] = $total_odd['@attributes']['value'];
                            }
                        }
                    }
                }
            }
            if(empty($arr)) return;

            if(!empty($bbm))
            {
                $arr['bm']['name'] =  $bbm['name'];
                $arr['bm']['id']   =  $bbm['id'];
            }
    }

    public function getItem()
    {
        return $this->leagues;
    }








}



