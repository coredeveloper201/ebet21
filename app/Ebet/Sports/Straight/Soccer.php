<?php
namespace App\Ebet\Sports\Straight;

use stdClass;
use App\Ebet\Sports\SportAbstract;
use App\Ebet\Sports\Contributor\WagerColumn;

class Soccer extends SportAbstract{

    use WagerColumn;

    public function __construct($url,$sport_id)
    {
        parent::__construct($url,$sport_id);
        $this->getOdds();
        $this->getPropsBet();
    }

    public function getOdds()
    {
        foreach($this->dataArr->category as $cat)
        {
            if($this->ifLeague($cat->attributes()->id) !=null)
            {
                if(isset($cat->matches))
                {
                    foreach($cat->matches->match as $match)
                    {
                        if(isset($match->attributes()->id))
                        {
                            $if_event = $this->ifEvent($match->attributes()->id);
                            if($if_event != null)
                            {
                                $item = $if_event;

                                if(isset($match->odds))
                                {
                                    foreach($match->odds->type as $type)
                                    {
                                        if(isset($type->attributes()->value) && ($type->attributes()->value =='Match Winner' && $type->attributes()->id =='1'))
                                        {
                                            if(isset($type->bookmaker))
                                            {
                                                foreach($type->bookmaker as $bm)
                                                {
                                                    if($bm->attributes()->name == '188bet' && $bm->attributes()->id == 56)
                                                    {
                                                        $extn = new stdClass;
                                                        $extn->sport_id = 1;
                                                        $this->moneyLine($item->event_odd,$bm,$extn);
                                                    }

                                                }
                                            }
                                        }
                                        if($type->attributes()->value =='Handicap Result' && $type->attributes()->id =='79')
                                        {

                                            if(isset($type->bookmaker) && count($type->bookmaker->attributes()) > 0)
                                            {
                                                foreach($type->bookmaker as $bm){

                                                    if($bm->attributes()->name == '188bet' && $bm->attributes()->id == 56)
                                                    {
                                                        if(isset($bm->handicap))
                                                        {
                                                            foreach($bm->handicap as $handicap)
                                                            {
                                                                $bmrk['name'] = isset($bm->attributes()->name)?$bm->attributes()->name:"";
                                                                $bmrk['id'] = isset($bm->attributes()->id)?$bm->attributes()->id:"";
                                                                $handicap->bm =  json_encode($bmrk);
                                                                $extn = new stdClass;
                                                                $extn->sport_id = 1;
                                                                $this->spread($item->event_odd,$bm->handicap,$extn);
                                                            }
                                                        }
                                                    }

                                                }
                                            }
                                        }

                                        if($type->attributes()->value == 'Goals Over/Under' && $type->attributes()->id =='5')
                                        {

                                            if(isset($type->bookmaker) && count($type->bookmaker->attributes()) > 0)
                                            {
                                                foreach($type->bookmaker as $bm)
                                                {
                                                    if($bm->attributes()->name == '188bet' && $bm->attributes()->id == 56)
                                                    {
                                                        if(isset($bm->total))
                                                        {
                                                            foreach($bm->total as $total)
                                                            {
                                                                $bmrk['name'] = isset($bm->attributes()->name)?$bm->attributes()->name:"";
                                                                $bmrk['id'] = isset($bm->attributes()->id)?$bm->attributes()->id:"";
                                                                $total->bm =  json_encode($bmrk);
                                                                $extn = new stdClass;
                                                                $extn->sport_id = 1;
                                                                $this->total($item->event_odd, $total,$extn);
                                                            }

                                                        }
                                                    }

                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else{
                    dd($cat['@attributes']);
                }
            }
        }
    }

    public function getPropsBet(){
        foreach($this->dataArr->category as $cat)
        {
            if($this->ifLeague($cat->attributes()->id) !=null)
            {
                if(isset($cat->matches))
                {
                    foreach($cat->matches->match as $match)
                    {
                        if(isset($match->attributes()->id))
                        {
                            $if_event = $this->ifEvent($match->attributes()->id);
                            if($if_event != null)
                            {
                                $item = $if_event;
                                $item->prop_bets = [];
                                $item->prop_bets['first_half_goal'] = []; // 1.17 F
                                $item->prop_bets['yes_no'] = [];   //1.11  both team to score
                                $item->prop_bets['correct_score'] = [];  //1.18
                                $item->prop_bets['highest_scoring_half'] = []; //1.16
                                $item->prop_bets['first_half_winner'] = [];  //1.19
                                $item->prop_bets['home_total_goals'] = [];   //1.7
                                $item->prop_bets['away_total_goals'] = [];   //1.8
                                $item->prop_bets['first_half_double_chance'] = [];  //1.20
                                $item->prop_bets['odd_even'] = [];    //1.9
                                $item->prop_bets['exact_goal_numbers'] = [];   //1.21

                                if(isset($match->odds))
                                {
                                    foreach($match->odds->type as $type)
                                    {
                                        // first Half Goals
                                        if(isset($type->attributes()->value) && ($type->attributes()->value == 'Goals Over/Under 1st Half' && $type->attributes()->id =='6'))
                                        {
                                            if(isset($type->bookmaker))
                                            {
                                                if($type->bookmaker->attributes()->name == 'bet365' && $type->bookmaker->attributes()->id == 16)
                                                {
                                                    if(isset($type->bookmaker))
                                                    {
                                                       $this->home_total_goals($item->prop_bets['first_half_goal'],$type->bookmaker);
                                                    }
                                                }
                                            }
                                        }

                                        //first half winner
                                        if(isset($type->attributes()->value) && ($type->attributes()->value == '1st Half Winner' && $type->attributes()->id =='2102'))
                                        {
                                            if(isset($type->bookmaker))
                                            {
                                                if($type->bookmaker->attributes()->name == 'bet365' && $type->bookmaker->attributes()->id == 16)
                                                {
                                                    $this->moneyLine($item->prop_bets['first_half_winner'],$type->bookmaker);
                                                }
                                            }
                                        }

                                        //first half double chance
                                        if(isset($type->attributes()->value) && ($type->attributes()->value == 'Double Chance - 1st Half' && $type->attributes()->id =='22602'))
                                        {
                                            if(isset($type->bookmaker))
                                            {
                                                if($type->bookmaker->attributes()->name == 'bet365' && $type->bookmaker->attributes()->id == 16)
                                                {
                                                    $this->yesNo($item->prop_bets['first_half_double_chance'],$type->bookmaker);
                                                }
                                            }
                                        }

                                        //exact goal number
                                        if(isset($type->attributes()->value) && ($type->attributes()->value == 'Exact Goals Number' && $type->attributes()->id =='22614'))
                                        {
                                            if(isset($type->bookmaker))
                                            {
                                                if($type->bookmaker->attributes()->name == 'bet365' && $type->bookmaker->attributes()->id == 16)
                                                {
                                                    $this->yesNo($item->prop_bets['exact_goal_numbers'],$type->bookmaker);
                                                }
                                            }
                                        }

                                        //both team to score
                                        if(isset($type->attributes()->value) && ($type->attributes()->value == 'Both Teams To Score' && $type->attributes()->id =='15'))
                                        {
                                            if(isset($type->bookmaker))
                                            {
                                                if($type->bookmaker->attributes()->name == 'bet365' && $type->bookmaker->attributes()->id == 16)
                                                {
                                                    if(isset($type->bookmaker))
                                                    {
                                                       $this->yesNo($item->prop_bets['yes_no'],$type->bookmaker);
                                                    }
                                                }
                                            }
                                        }

                                        //correct score
                                        if(isset($type->attributes()->value) && ($type->attributes()->value == 'Correct Score' && $type->attributes()->id =='81'))
                                        {
                                            if(isset($type->bookmaker))
                                            {
                                                if($type->bookmaker->attributes()->name == 'bet365' && $type->bookmaker->attributes()->id == 16)
                                                {
                                                    if(isset($type->bookmaker))
                                                    {
                                                       $this->yesNo($item->prop_bets['correct_score'],$type->bookmaker);
                                                    }
                                                }
                                            }
                                        }


                                        //highest_scoring_half
                                        if(isset($type->attributes()->value) && ($type->attributes()->value == 'Highest Scoring Half' && $type->attributes()->id =='91'))
                                        {
                                            if(isset($type->bookmaker))
                                            {
                                                if($type->bookmaker->attributes()->name == 'bet365' && $type->bookmaker->attributes()->id == 16)
                                                {
                                                    $this->highestScoreHalf($item->prop_bets['highest_scoring_half'],$type->bookmaker);
                                                }
                                            }
                                        }


                                        //home total goals
                                        if(isset($type->attributes()->value) && ($type->attributes()->value == 'Total - Home' && $type->attributes()->id =='22124'))
                                        {
                                            if(isset($type->bookmaker))
                                            {
                                                if($type->bookmaker->attributes()->name == 'bet365' && $type->bookmaker->attributes()->id == 16)
                                                {
                                                    if(isset($type->bookmaker))
                                                    {
                                                       $this->home_total_goals($item->prop_bets['home_total_goals'],$type->bookmaker);
                                                    }
                                                }
                                            }
                                        }


                                        //away total goals
                                        if(isset($type->attributes()->value) && ($type->attributes()->value == 'Total - Home' && $type->attributes()->id =='22124'))
                                        {
                                            if(isset($type->bookmaker))
                                            {
                                                if($type->bookmaker->attributes()->name == 'bet365' && $type->bookmaker->attributes()->id == 16)
                                                {
                                                    if(isset($type->bookmaker))
                                                    {
                                                       $this->away_total_goals($item->prop_bets['away_total_goals'],$type->bookmaker);
                                                    }
                                                }
                                            }
                                        }

                                        //odd/even
                                        if(isset($type->attributes()->value) && ($type->attributes()->value == 'Odd/Even' && $type->attributes()->id =='22608'))
                                        {
                                            if(isset($type->bookmaker))
                                            {
                                                if($type->bookmaker->attributes()->name == 'bet365' && $type->bookmaker->attributes()->id == 16)
                                                {
                                                    if(isset($type->bookmaker))
                                                    {
                                                       $this->oddEven($item->prop_bets['odd_even'],$type->bookmaker);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else{
                    dd($cat['@attributes']);
                }
            }
        }
    }

    public function getItem()
    {
        return $this->leagues;
    }


    public function oddEven(array &$arr,$odd_obj)
    {
        $item  =   json_decode( json_encode( $odd_obj),true);
        if(isset($item['odd']))
        {
           if(is_array($item['odd']))
           {
                foreach($item['odd'] as $moneyLine)
                {
                    if(isset($moneyLine['@attributes']))
                    {
                        $arr[$moneyLine['@attributes']['name']] = $moneyLine['@attributes']['value'];
                    }
                }
           }
        }
    }

    public function home_total_goals(array &$arr,$odd_obj)
    {
        $item  = json_decode(json_encode($odd_obj),true);

        if(isset($item['total']))
        {
            if(is_array($item['total']))
            {
                foreach($item['total'] as $total)
                {
                    if(isset($total['odd']))
                    {
                        if(is_array($total['odd']))
                        {
                            foreach($total['odd'] as $total_odd)
                            {
                                if(isset($total_odd['@attributes']))
                                {
                                    $arr['home'][$total_odd['@attributes']['name']][]  = $total_odd['@attributes']['value'];
                                }
                            }
                        }
                    }
                    if(isset($total['@attributes'])) {
                        if(isset($total['@attributes']['name']) && !empty($total['@attributes']['name']))
                        {
                            $arr['home']['wager'][] = $total['@attributes']['name'];
                        }
                    }

                }
            }
        }
    }



    public function away_total_goals(array &$arr,$odd_obj)
    {
        $item  = json_decode(json_encode($odd_obj),true);
        if(isset($item['total']))
        {
            if(is_array($item['total']))
            {
                foreach($item['total'] as $total)
                {
                    if(isset($total['odd']))
                    {
                        if(is_array($total['odd']))
                        {
                            foreach($total['odd'] as $total_odd)
                            {
                                if(isset($total_odd['@attributes']))
                                {
                                        $arr['away'][$total_odd['@attributes']['name']][]  = $total_odd['@attributes']['value'];
                                }
                            }
                        }
                    }
                    if(isset($total['@attributes'])) {
                        if(isset($total['@attributes']['name']) && !empty($total['@attributes']['name']))
                        {
                            $arr['away']['wager'][] = $total['@attributes']['name'];
                        }
                    }

                }
            }
        }
    }



    public function yesNo(array &$arr,$odd_obj)
    {
        $item  =   json_decode( json_encode( $odd_obj),true);
        if(isset($item['odd']))
        {
           if(is_array($item['odd']))
           {
                foreach($item['odd'] as $moneyLine)
                {
                    if(isset($moneyLine['@attributes']))
                    {
                        $arr[$moneyLine['@attributes']['name']] = $moneyLine['@attributes']['value'];
                    }
                }
           }
        }
    }




    public function highestScoreHalf(array &$arr,$odd_obj)
    {
        $item  =   json_decode( json_encode( $odd_obj),true);
        if(isset($item['odd']))
        {
            if(is_array($item['odd']))
            {
                foreach($item['odd'] as $spread)
                {
                    if(isset($spread['@attributes']))
                    {
                        if(isset($spread['@attributes']['name']))
                        {
                            $arr[]= [
                                'name' =>   $spread['@attributes']['name'],
                                'value' =>  $spread['@attributes']['value'],
                            ];
                        }
                    }
                }
            }
        }
    }









}



