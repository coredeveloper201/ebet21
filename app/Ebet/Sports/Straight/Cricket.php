<?php
namespace App\Ebet\Sports\Straight;

use stdClass;
use App\Ebet\Sports\SportAbstract;
use App\Ebet\Sports\Contributor\WagerColumn;

class Cricket extends SportAbstract{
   use WagerColumn;

    public function __construct($url,$sport_id)
    {
        parent::__construct($url,$sport_id);
        $this->getOdds();
        // $this->getPropsBet();
    }

    public function getOdds()
    {
        foreach($this->dataArr->category as $cat)
        {
            if($this->ifLeague($cat->attributes()->id) !=null)
            {
                if(isset($cat->matches))
                {
                    foreach($cat->matches->match as $match)
                    {
                        if(isset($match->attributes()->id))
                        {
                            $if_event = $this->ifEvent($match->attributes()->id);
                            if($if_event != null)
                            {
                                $item = $if_event;
                                if(isset($match->odds))
                                {
                                    foreach($match->odds->type as $type)
                                    {

                                        if(isset($type->attributes()->value) && ($type->attributes()->value == 'Home/Away' && $type->attributes()->id =='2'))
                                        {
                                            if(isset($type->bookmaker))
                                            {
                                                foreach($type->bookmaker as $bm){
                                                    $extn = new stdClass;
                                                    $extn->sport_id = 15;
                                                    $this->moneyLine($item->event_odd,$bm,$extn);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else{
                    dd($cat->attributes());
                }
            }
        }
    }

    public function getPropsBet(){
        foreach($this->dataArr['scores']['category'] as $cat) {
            if($this->ifLeague($cat['@id']) !=null) {
                if(isset($cat['matches'])) {
                    foreach($cat['matches']['match'] as $match) {
                        if(isset($match['@id'])) {
                            $if_event = $this->ifEvent($match['@id']);
                            if($if_event != null) {
                                $propsBet = $if_event;
                                $propsBet->prop_bet = new stdClass();
                                //1st half
                                $propsBet->prop_bet->sport_id = 12;
                                $propsBet->prop_bet->first_half_point_spread_2_way = null;
                                $propsBet->prop_bet->first_half_money_line_2_way = null;
                                $propsBet->prop_bet->first_half_total_2_way = null;
                                //2nd half
                                $propsBet->prop_bet->second_half_point_spread_2_way = null;
                                $propsBet->prop_bet->second_half_money_line_2_way = null;
                                $propsBet->prop_bet->second_half_total_2_way = null;
                                //1st quarter
                                $propsBet->prop_bet->first_quarter_point_spread_2_way = null;
                                $propsBet->prop_bet->first_quarter_money_line_2_way = null;
                                $propsBet->prop_bet->first_quarter_total_2_way = null;

                                /* $item->money_line = isset($match['odds']['type'][0]['bookmaker']) ? $match['odds']['type'][0]['bookmaker']['odd'] : []; */
                                if(isset($match['odds'])) {
                                    foreach($match['odds']['type'] as $type) {
                                        // all first half
                                        // first half money line
                                        if(isset($type['@value']) && ($type['@value'] =='Home/Away - 1st Half' && $type['@id'] =='22678')) {
                                            if(isset($type['bookmaker'])) {
                                                if($type['bookmaker']['@name']=='bet365' && $type['bookmaker']['@id'] == 16) {
                                                    if(isset($type['bookmaker']['odd'])) {
                                                        $propsBet->prop_bet->first_half_money_line_2_way = $type['bookmaker']['odd'];
                                                    }
                                                }
                                            }
                                        }

                                        // first_half point spread
                                        if(isset($type['@value']) && ($type['@value'] =='Asian Handicap First Half' && $type['@id'] =='22601')) {
                                            if(isset($type['bookmaker'])) {
                                                if($type['bookmaker']['@name']=='bet365' && $type['bookmaker']['@id'] == 16) {
                                                    if(isset($type['bookmaker']['handicap']['odd'])) {
                                                        $propsBet->prop_bet->first_half_point_spread_2_way = $type['bookmaker']['handicap']['odd'];
                                                    }
                                                }
                                            }
                                        }

                                        // first half total
                                        if(isset($type['@value']) && ($type['@value'] =='Over/Under 1st Half' && $type['@id'] =='6')) {
                                            if(isset($type['bookmaker'])) {
                                                if($type['bookmaker']['@name']=='bet365' && $type['bookmaker']['@id'] == 16) {
                                                    if(isset($type['bookmaker']['total']['odd'])) {
                                                        $propsBet->prop_bet->first_half_total_2_way = $type['bookmaker']['total']['odd'];
                                                    }
                                                }
                                            }
                                        }

                                        // all quarter
                                        //first quarter money line
                                        if(isset($type['@value']) && ($type['@value'] =='Home/Away - 1st Qtr' && $type['@id'] =='22680')) {
                                            if(isset($type['bookmaker'])) {
                                                if($type['bookmaker']['@name']=='bet365' && $type['bookmaker']['@id'] == 16) {
                                                    if(isset($type['bookmaker']['odd'])) {
                                                        $propsBet->prop_bet->first_quarter_money_line_2_way = $type['bookmaker']['odd'];
                                                    }
                                                }
                                            }
                                        }

                                        // first quarter spread
                                        if(isset($type['@value']) && ($type['@value'] =='Asian Handicap 1st Qtr' && $type['@id'] =='22661')) {
                                            if(isset($type['bookmaker'])) {
                                                if($type['bookmaker']['@name']=='bet365' && $type['bookmaker']['@id'] == 16) {
                                                    if(isset($type['bookmaker']['handicap']['odd'])) {
                                                        $propsBet->prop_bet->first_quarter_point_spread_2_way = $type['bookmaker']['handicap']['odd'];
                                                    }
                                                }
                                            }
                                        }

                                        // first quarter total
                                        if(isset($type['@value']) && ($type['@value'] =='Over/Under 1st Qtr' && $type['@id'] =='22646')) {
                                            if(isset($type['bookmaker'])) {
                                                if($type['bookmaker']['@name']=='bet365' && $type['bookmaker']['@id'] == 16) {
                                                    if(isset($type['bookmaker']['total']['odd'])) {
                                                        $propsBet->prop_bet->first_quarter_total_2_way = $type['bookmaker']['total']['odd'];
                                                    }
                                                }
                                            }
                                        }

                                    }
                                }
                            }
                        }
                    }
                }
                else{
                    dd($cat['@attributes']);
                }
            }
        }
    }

    public function getItem()
    {
        return $this->leagues;
    }








}



