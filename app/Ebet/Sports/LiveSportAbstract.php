<?php
namespace App\Ebet\Sports;

use stdClass;
use App\Contracts\LiveSport\LiveSportGeneral;



abstract class LiveSportAbstract implements LiveSportGeneral{

    private $dataArr = array();
    private $oddsHolder = [];


    protected $spread = 'spread';
    protected $money_line = 'money_line';
    protected $total = 'total';

    protected $dataSet = [];
    public function __construct($url)
    {
        $this->dataArr = $this->getData($url);
        $this->prepareData();
    }


    abstract public function getFormatted();
    private function getData($url)
    {
        $arrContextOptions=array(
            "ssl"=>array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ),
        );
        return json_decode(file_get_contents($url, false, stream_context_create($arrContextOptions)));
    }

    public function breakdownss($str)
    {
        $ss = explode(",",$str);
        $home = [];
        $away = [];
        foreach($ss as $s)
        {
            $break = explode(':',$s);
            $home[] = $break[0];
            $away[] = $break[1];
        }
        return [$home,$away];
    }

    public function getDataArr()
    {
        return $this->oddsHolder;
    }

    public function prepareData()
    {
        foreach($this->dataArr->events as $event)
        {
           $obj = new stdClass;
           $obj->core = $event->core;
           $obj->info = $event->info;
           $obj->stats = $event->stats;
           $obj->sport_odd = [];
           $obj->sport_odd[$this->spread] = null;
           $obj->sport_odd[$this->money_line] = null;
           $obj->sport_odd[$this->total] =  null;

           if(isset($event->odds))
           {
               if(!empty($this->dataSet) && count($this->dataSet)==3)
               {
                foreach($event->odds as $odd)
                {
                    if($odd->id == $this->dataSet[$this->spread])  // spread
                    {
                       $obj->sport_odd[$this->spread] = $odd;
                    }

                    if($odd->id ==$this->dataSet[$this->money_line])  // Money_line
                    {
                       $obj->sport_odd[$this->money_line] = $odd;
                    }

                    if($odd->id == $this->dataSet[$this->total])  // Total
                    {
                       $obj->sport_odd[$this->total] = $odd;
                    }
                }
               } else {
                   return "Data set variable must be filled";
               }

             $this->oddsHolder[] = $obj;
           }
        }
    }


    public function numberFormatted($nmbr)
    {
        return (int)$nmbr;
    }



}
