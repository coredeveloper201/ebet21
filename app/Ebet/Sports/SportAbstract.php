<?php
namespace App\Ebet\Sports;

use stdClass;

abstract class SportAbstract{

    private $url;
    protected $dataArr = [];
    protected $leagues =[];
    protected $events = [];

    protected  $sport_id;


    public function __construct($url,$sport_id)
    {
        set_time_limit(0);
        ini_set('memory_limit', '200000000M');
        $this->url = $url;
        $this->sport_id = $sport_id;
        $this->getJsonFormatted();
        $this->getLeagues();
        $this->getEvents();
    }
    abstract public function getItem();

    public function getLeagues(){

        if($this->sport_id == 151)
        {
            foreach($this->dataArr->match as $cat)
            {
                $std_obj = new stdClass;
                $std_obj->league_name = ((array)$cat->attributes()->type) [0].": ".( (array) $cat->attributes()->league)[0];
                $std_obj->league_id   = ( (array) $cat->attributes()->league_id)[0];
                $std_obj->events = [];
                if(!empty($std_obj->league_id))
                {
                    $this->leagues[$std_obj->league_id] = $std_obj;
                }
            }
        }
        else
        {
            foreach($this->dataArr->category as $cat)
            {
                $std_obj = new stdClass;
                $std_obj->league_name = ( (array) $cat->attributes()->name)[0];
                $std_obj->league_id   = ( (array) $cat->attributes()->id)[0];
                if(!empty($std_obj->league_id))
                {
                    $this->leagues[$std_obj->league_id] = $std_obj;
                }
            }

        }

    }

    public function getEvents(){

        foreach($this->leagues as $league)
        {
            if($this->sport_id == 151)
            {
                foreach($this->dataArr->match as $cat)
                 {
                    if($league->league_id  == $cat->attributes()->league_id)
                    {
                        $item = $league;
                        $event_obj = new stdClass;
                        $event_obj->event_info = new stdClass;
                        $event_obj->event_info->home_name = "";
                        $event_obj->event_info->home_id = "";
                        $event_obj->event_info->away_id = "";
                        $event_obj->event_info->away_name  = "";
                        if(isset($cat->localteam))
                            {
                                $event_obj->event_info->home_name =  ( (array) $cat->localteam->attributes()->name)[0];
                                $event_obj->event_info->home_id =   ( (array)  $cat->localteam->attributes()->id)[0];
                            }

                            if(isset($cat->awayteam))
                            {
                                $event_obj->event_info->away_name = ( (array) $cat->awayteam->attributes()->name)[0];
                                $event_obj->event_info->away_id =   ( (array) $cat->awayteam->attributes()->id)[0];
                            }

                            $event_obj->event_info->id   = isset($cat->attributes()->id) ?  ((array)$cat->attributes()->id)[0] : "";
                            $event_obj->event_info->e_time =      ((array)$cat->attributes()->time)[0];
                            $event_obj->event_info->e_date =      ((array)$cat->attributes()->date)[0];

                        $v_item = array();
                        $v_item['spread']['home']['odd']      = "";
                        $v_item['spread']['home']['handicap'] = "";
                        $v_item['spread']['away']['handicap'] = "";
                        $v_item['spread']['away']['odd']      = "";
                        $v_item['money']['home']              = "";
                        $v_item['money']['away']              = "";
                        $v_item['total']['home']['odd']       = "";
                        $v_item['total']['home']['under_over']= "";
                        $v_item['total']['away']['odd']       = "";
                        $v_item['total']['away']['under_over']= "";
                        $v_item['draw'] = "";
                        $event_obj->event_odd = $v_item;
                        if(!empty($event_obj->event_info->id))
                        {
                            $this->events[$event_obj->event_info->id] = $event_obj;
                        }
                        $item->events[] = $event_obj;
                    }
                }
            }
            else
            {
                foreach($this->dataArr->category as $cat)
                {
                    if($league->league_id  == $cat->attributes()->id)
                    {
                        $item = $league;
                        $events = $cat->matches->match;
                        //$item->events = [];
                        foreach($events as $event)
                        {
                            $event_obj = new stdClass;
                            $event_obj->event_info = new stdClass;
                            $event_obj->event_info->home_name = "";
                            $event_obj->event_info->home_id = "";
                            $event_obj->event_info->away_id = "";
                            $event_obj->event_info->away_name  = "";
                            if(isset($event->localteam))
                            {
                                $event_obj->event_info->home_name =  ( (array) $event->localteam->attributes()->name)[0];
                                $event_obj->event_info->home_id =   ( (array)  $event->localteam->attributes()->id)[0];
                            }
                            elseif (isset($event->player[0]))
                            {
                                $event_obj->event_info->home_name = ( (array) $event->player[0]->attributes()->name)[0];
                                $event_obj->event_info->home_id =   ( (array) $event->player[0]->attributes()->id)[0];
                            }

                            if(isset($event->visitorteam))
                            {
                                $event_obj->event_info->away_name = ( (array) $event->visitorteam->attributes()->name)[0];
                                $event_obj->event_info->away_id =   ( (array) $event->visitorteam->attributes()->id)[0];
                            }

                            if(isset($event->awayteam))
                            {
                                $event_obj->event_info->away_name = ( (array) $event->awayteam->attributes()->name)[0];
                                $event_obj->event_info->away_id =   ( (array) $event->awayteam->attributes()->id)[0];
                            }
                            elseif (isset($event->player[1]))
                            {
                                $event_obj->event_info->away_name = ( (array) $event->player[1]->attributes()->name)[0];
                                $event_obj->event_info->away_id =   ( (array) $event->player[1]->attributes()->id)[0];
                            }

                            if($this->sport_id == 17) {
                                $event_obj->event_info->id   =  isset($event->attributes()->id) ?  ((array)$event->attributes()->id)[0] : "";
                            }
                            else {
                                $event_obj->event_info->id   = isset($event->attributes()->id) ?  ((array)$event->attributes()->id)[0] : "";
                            }
                            $event_obj->event_info->e_time =      ((array)$event->attributes()->time)[0];
                            $event_obj->event_info->e_date =      ((array)$event->attributes()->date)[0];
                            $v_item = array();
                            $v_item['spread']['home']['odd']      = "";
                            $v_item['spread']['home']['handicap'] = "";
                            $v_item['spread']['away']['handicap'] = "";
                            $v_item['spread']['away']['odd']      = "";
                            $v_item['money']['home']              = "";
                            $v_item['money']['away']              = "";
                            $v_item['total']['home']['odd']       = "";
                            $v_item['total']['home']['under_over']= "";
                            $v_item['total']['away']['odd']       = "";
                            $v_item['total']['away']['under_over']= "";
                            $v_item['draw'] = "";
                            $event_obj->event_odd = $v_item;
                            if(!empty($event_obj->event_info->id))
                            {
                                $this->events[$event_obj->event_info->id] = $event_obj;
                            }
                            $item->events[] = $event_obj;
                        }
                    }
                }
            }

        }
    }


    public function ifLeague($league_id)
    {
        foreach($this->leagues as $league)
        {
            if($league->league_id == $league_id) return $league;
        }
        return null;
    }

    public function ifEvent($event_id)
    {
        foreach($this->leagues as $league)
        {
            foreach($league->events as $event)
            {
                if($event->event_info->id == $event_id) return $event;
            }
        }
        return null;
    }

   //abstract protected function prefixName($name);

   protected function getJsonFormatted()
   {
        $opts = array(
            'http'=>array(
                'method'=>"GET",
                'header'=>"Accept-Language: en-US,en;q=0.8rn" .
                    "Accept-Encoding: gzip,deflate,sdchrn" .
                    "Accept-Charset:UTF-8,*;q=0.5rn" .
                    "User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:19.0) Gecko/20100101 Firefox/19.0 FirePHP/0.4rn"
            )
        );

        $context = stream_context_create($opts);
        $content = simplexml_load_string(file_get_contents($this->url,false,$context));
        $this->dataArr = $content;
   }

}

