<?php
namespace App\Ebet\Sports\Contributor;

trait PropBet{



    public function getAPI($iFI)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, 'https://api.betsapi.com/v1/bet365/prematch?token=26928-6HWCoyS7zhskxm&FI=' . $iFI);
        $result = curl_exec($ch);
        curl_close($ch);
        return  $obj = json_decode($result, true);
    }

    protected function showBetProps($result, $type)
    {
        $result = json_decode(json_encode($result), true);
        if(!isset($result['main'])) {
            return true;
        }
        $mainsp = array_key_exists('main',$result)? array_key_exists('sp',$result['main'])?$result['main']['sp']:false:false;
        $count = 0;
        if($mainsp)
        {
            if ($type == 16) { //baseball
                $arit1i = array_key_exists("a_run_in_the_1st_innings", $mainsp) ? true : false;
                if ($arit1i) $count++;
                $ftts = array_key_exists("first_team_to_score", $mainsp) ? true : false;
                if ($ftts) $count++;
                $arl = array_key_exists("alternative_run_line", $mainsp) ? true : false;
                if ($arl) $count++;
                $wm = array_key_exists("winning_margins", $mainsp) ? true : false;
                if ($wm) $count++;
                $fil = array_key_exists("5_innings_line", $mainsp) ? true : false;
                if ($fil) $count++;
                return $count == 0;
            } else if ($type == 12) {
                //1st half
                $fhps2w = array_key_exists("1st_half_point_spread_2_way", $mainsp) ? true : false;
                if ($fhps2w) $count++;
                $fhml2w = array_key_exists("1st_half_money_line_2_way", $mainsp) ? true : false;
                if ($fhml2w) $count++;
                $fht2w = array_key_exists("1st_half_total_2_way", $mainsp) ? true : false;
                if ($fht2w) $count++;
                //2nd half
                $shps2w = array_key_exists("2nd_half_point_spread_2_way", $mainsp) ? true : false;
                if ($shps2w) $count++;
                $shml2w = array_key_exists("2nd_half_money_line_2_way", $mainsp) ? true : false;
                if ($shml2w) $count++;
                $sht2w = array_key_exists("2nd_half_total_2_way", $mainsp) ? true : false;
                if ($sht2w) $count++;
                //1st quarter
                $fqps2w = array_key_exists("1st_quarter_point_spread_2_way", $mainsp) ? true : false;
                if ($fqps2w) $count++;
                $fqml2w = array_key_exists("1st_quarter_money_line_2_way", $mainsp) ? true : false;
                if ($fqml2w) $count++;
                $fqt2w = array_key_exists("1st_quarter_total_2_way", $mainsp) ? true : false;
                if ($fqt2w) $count++;
                return $count == 0;
            } else if ($type == 1) {
                $htft = array_key_exists("half_time_full_time", $mainsp) ? true : false;
                if ($htft) $count++;
                $btts = array_key_exists("both_teams_to_score", $mainsp) ? true : false;
                if ($btts) $count++;
                $wm = array_key_exists("winning_margin", $mainsp) ? true : false;
                if ($wm) $count++;
                $ahr = array_key_exists("alternative_handicap_result", $mainsp) ? true : false;
                if ($ahr) $count++;
                $goe =  array_key_exists('goals',$result)? array_key_exists('sp',$result['goals']):false?array_key_exists("goals_odd_even", $result['goals']['sp']) ? true : false:false;
                if ($goe) $count++;
                return $count == 0;
            } else if ($type == 13) {
                $sb = array_key_exists("set_betting", $mainsp) ? true : false;
                if ($sb) $count++;
                $fsw = array_key_exists("first_set_winner", $mainsp) ? true : false;
                if ($fsw) $count++;
                $fbfs = array_key_exists("first_break_of_serve", $mainsp) ? true : false;
                if ($fbfs) $count++;
                return $count == 0;
            } else if ($type == 18) {
                //first half
                $fhs = array_key_exists("1st_half_spread", $mainsp) ? true : false;
                if ($fhs) $count++;
                $fhml = array_key_exists("1st_half_money_line", $mainsp) ? true : false;
                if ($fhml) $count++;
                $fht = array_key_exists("1st_half_totals", $mainsp) ? true : false;
                if ($fht) $count++;

                //quarter half
                $fqs = array_key_exists("1st_quarter_spread", $mainsp) ? true : false;
                if ($fqs) $count++;
                $fqml = array_key_exists("1st_quarter_money_line", $mainsp) ? true : false;
                if ($fqml) $count++;
                $fqt = array_key_exists("1st_quarter_totals", $mainsp) ? true : false;
                if ($fqt) $count++;

                $rt20p = array_key_exists('main_props',$result)? array_key_exists('sp',$result['main_props']):false ? array_key_exists("race_to_20_points", $result['main_props']['sp']) ? true : false:false;
                if ($rt20p) $count++;

                $bwm =  array_key_exists('main_props',$result)? array_key_exists('sp',$result['main_props']):false ? array_key_exists("winning_margin", $result['main_props']['sp']) ? true : false:false;
                if ($bwm) $count++;
                return $count == 0;
            } else {
                return true;
            }
        }

    }


    public function getHockey($response_obj)
    {
        $result = $response_obj['results'][0];
        $mainsp = $result['main']['sp'];
        //first period

        $prop_bet = array();
        $prop_bet['first_period_totals'] = array_key_exists("1st_period_totals", $mainsp) ? $mainsp['1st_period_totals'] : [];
        $prop_bet['first_period_money_line_2_way'] = array_key_exists("1st_period_money_line_2_way", $mainsp) ? $mainsp['1st_period_money_line_2_way'] : [];
        $prop_bet['first_period_puck_line_3_way'] = array_key_exists("1st_period_puck_line_3_way", $mainsp) ? $mainsp['1st_period_puck_line_3_way'] : [];

        //game_total_odd_even
        if (!empty($result['score']['sp'])) {
            $prop_bet['game_total_odd_even'] = array_key_exists("game_total_odd_even", $result['score']['sp']) ? $result['score']['sp']['game_total_odd_even'] : [];
        } else {
            $prop_bet['game_total_odd_even'] = [];
        }
        return $prop_bet;
    }
    public function getSoccerDetails($response_obj)
    {
        $result = $response_obj['results'][0];
        $mainsp = $result['main']['sp'];
        $halfsp = [];
        if (!empty($result['half']['sp'])) {
            $halfsp = $result['half']['sp'];
        }

        //soccer
        $prop_bet = array();
        $prop_bet['half_time_full_time'] = array_key_exists("half_time_result", $halfsp) ? $halfsp['half_time_result'] : [];
        $prop_bet['both_teams_to_score'] = array_key_exists("both_teams_to_score", $mainsp) ? $mainsp['both_teams_to_score'] : [];
        $prop_bet['winning_margin'] = array_key_exists("winning_margin", $mainsp) ? $mainsp['winning_margin'] : [];
        $prop_bet['alternative_handicap_result'] = array_key_exists("alternative_handicap_result", $mainsp) ? $mainsp['alternative_handicap_result'] : [];
        $prop_bet['goals_odd_even'] = array_key_exists("goals_odd_even", $result['goals']['sp']) ? $result['goals']['sp']['goals_odd_even'] : [];
        return $prop_bet;
    }
    public function getFootBall($response_obj)
    {
        $result = $response_obj['results'][0];
        $mainsp = $result['main']['sp'];
        //football

        $prop_bet = array();
        //1st half
        $prop_bet['first_half_point_spread_2_way'] = array_key_exists("1st_half_point_spread_2_way", $mainsp) ? $mainsp['1st_half_point_spread_2_way'] : [];
        $prop_bet['first_half_money_line_2_way'] = array_key_exists("1st_half_money_line_2_way", $mainsp) ? $mainsp['1st_half_money_line_2_way'] : [];
        $prop_bet['first_half_total_2_way'] = array_key_exists("1st_half_total_2_way", $mainsp) ? $mainsp['1st_half_total_2_way'] : [];
        //2nd half
        $prop_bet['second_half_point_spread_2_way'] = array_key_exists("2nd_half_point_spread_2_way", $mainsp) ? $mainsp['2nd_half_point_spread_2_way'] : [];
        $prop_bet['second_half_money_line_2_way'] = array_key_exists("2nd_half_money_line_2_way", $mainsp) ? $mainsp['2nd_half_money_line_2_way'] : [];
        $prop_bet['second_half_total_2_way'] = array_key_exists("2nd_half_total_2_way", $mainsp) ? $mainsp['2nd_half_total_2_way'] : [];
        //1st quarter
        $prop_bet['first_quarter_point_spread_2_way'] = array_key_exists("1st_quarter_point_spread_2_way", $mainsp) ? $mainsp['1st_quarter_point_spread_2_way'] : [];
        $prop_bet['first_quarter_money_line_2_way'] = array_key_exists("1st_quarter_money_line_2_way", $mainsp) ? $mainsp['1st_quarter_money_line_2_way'] : [];
        $prop_bet['first_quarter_total_2_way'] = array_key_exists("1st_quarter_total_2_way", $mainsp) ? $mainsp['1st_quarter_total_2_way'] : [];
        return $prop_bet;
    }
    public function getBaseball($response_obj)
    {
        $result = $response_obj['results'][0];
        $mainsp = $result['main']['sp'];

        //  dd($mainsp['a_run_in_the_1st_inning']);
        //baseball variables
        $prop_bet = array();
        $prop_bet['a_run_in_the_1st_inning']  = array_key_exists("a_run_in_the_1st_inning", $mainsp) ? $mainsp['a_run_in_the_1st_inning'] : [];
        $prop_bet['first_team_to_score']      = array_key_exists("first_team_to_score", $mainsp) ? $mainsp['first_team_to_score'] : [];
        $prop_bet['alternative_run_line']     = array_key_exists("alternative_run_line", $mainsp) ? $mainsp['alternative_run_line'] : [];
        $prop_bet['winning_margins']          = array_key_exists("winning_margins", $mainsp) ? $mainsp['winning_margins'] : [];
        $prop_bet['five_innings_line']        = array_key_exists("5_innings_line", $mainsp) ? $mainsp['5_innings_line'] : [];
        $prop_bet['team_totals']              = array_key_exists("team_totals", $mainsp) ? $mainsp['team_totals'] : [];
        return $prop_bet;
    }
    public function getTennis($response_obj)
    {
        $result = $response_obj['results'][0];
        $mainsp = $result['main']['sp'];
        //Tennies
        $prop_bet = array();
        $prop_bet['set_betting']          = array_key_exists("set_betting", $mainsp) ? $mainsp['set_betting'] : [];
        $prop_bet['first_set_winner']     = array_key_exists("first_set_winner", $mainsp) ? $mainsp['first_set_winner'] : [];
        $prop_bet['first_break_of_serve'] = array_key_exists("first_break_of_serve", $mainsp) ? $mainsp['first_break_of_serve'] : [];
        return $prop_bet;
    }
    public function getBasketBall($response_obj)
    {
        $result = $response_obj['results'][0];
        $mainsp = $result['main']['sp'];

        //basketball
        $prop_bet = array();
        //first half
        $prop_bet['first_half_spread']        = array_key_exists("1st_half_spread", $mainsp) ? $mainsp['1st_half_spread'] : [];
        $prop_bet['first_half_money_line']    = array_key_exists("1st_half_money_line", $mainsp) ? $mainsp['1st_half_money_line'] : [];
        $prop_bet['first_half_totals']        = array_key_exists("1st_half_totals", $mainsp) ? $mainsp['1st_half_totals'] : [];
        //quarter half
        $prop_bet['first_quarter_spread']     = array_key_exists("1st_quarter_spread", $mainsp) ? $mainsp['1st_quarter_spread'] : [];
        $prop_bet['first_quarter_money_line'] = array_key_exists("1st_quarter_money_line", $mainsp) ? $mainsp['1st_quarter_money_line'] : [];
        $prop_bet['first_quarter_totals']     = array_key_exists("1st_quarter_totals", $mainsp) ? $mainsp['1st_quarter_totals'] : [];
        $prop_bet['race_to_20_points']        = array_key_exists("race_to_20_points", $result['main_props']['sp']) ? $result['main_props']['sp']['race_to_20_points'] : [];
        $prop_bet['b_winning_margin']         = array_key_exists("winning_margin", $result['main_props']['sp']) ? $result['main_props']['sp']['winning_margin'] : [];

        return $prop_bet;
    }

}
