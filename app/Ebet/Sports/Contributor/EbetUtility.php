<?php
namespace App\Ebet\Sports\Contributor;


trait EbetUtility{
    protected function toAmericanDecimal($number)
    {
        if (empty($number) || $number == "OTB")
            return "OTB";
        else {
            $number == 1 ? $number = 2 : '';
            return $number < 2 ? round((-100) / ($number - 1)) : round(($number - 1) * 100);
        }
    }
}

