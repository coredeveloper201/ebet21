<?php
namespace App\Ebet\Sports\Contributor;

use App\Ebet\Sports\Contributor\EbetUtility;


trait WagerColumn
{
    use EbetUtility;
    public function moneyLine(array &$arr,$odd_obj,$extn = null)
    {
        $item  =   json_decode( json_encode( $odd_obj),true);
        if(isset($item['odd']))
        {
           if(is_array($item['odd']))
           {
                foreach($item['odd'] as $moneyLine)
                {
                    if(isset($moneyLine['@attributes']))
                    {
                        if($moneyLine['@attributes']['name'] == 'Home')
                        {
                            $arr['money']['home'] =  $moneyLine['@attributes']['value'];
                        }

                        if($extn !=null && ($extn->sport_id == 1 || $extn->sport_id == 17 ||  $extn->sport_id == 19))
                        {
                            if($moneyLine['@attributes']['name'] == 'Draw')
                            {
                                $arr['draw'] =  $moneyLine['@attributes']['value'];
                            }
                        }

                        if($moneyLine['@attributes']['name'] == 'Away')
                        {
                            $arr['money']['away'] =  $moneyLine['@attributes']['value'];
                        }
                    }
                }
           }
        }

        if($extn !=null && $extn->sport_id != 12)
        {
            if(isset($arr['money']['home']) &&  $arr['money']['home'] == '' && isset($arr['money']['away']) && $arr['money']['away'] == '') return;
            else{
                $arr['money']['bm']['name'] =  $item['@attributes']['name'];
                $arr['money']['bm']['id'] =    $item['@attributes']['id'];
                return true;
            }
        }


    }

    public function spread(array &$arr,$odd_obj,$extn = null)
    {

        $item  =   json_decode( json_encode( $odd_obj),true);
        $bbm =     isset($item['bm'])? json_decode($item['bm'],true): [];
        if(isset($item['odd']))
        {
            if(is_array($item['odd']))
            {
                foreach($item['odd'] as $spread)
                {
                    if(isset($spread['@attributes']))
                    {
                        if($spread['@attributes']['name'] == 'Home')
                        {
                            $arr['spread']['home']['odd'] =  $spread['@attributes']['value'];
                        }
                        if($spread['@attributes']['name'] == 'Away')
                        {
                            $arr['spread']['away']['odd']=  $spread['@attributes']['value'];
                        }
                    }
                }
            }
        }
        if(isset($item['@attributes']))
        {
            if(isset($item['@attributes']['name']) && !empty($item['@attributes']['name']))
            {
                $arr['spread']['home']['handicap'] = $item['@attributes']['name'];
                $arr['spread']['away']['handicap'] = $item['@attributes']['name'];
            }
        }

        if($extn !=null && $extn->sport_id != 12)
        {
            if($arr['spread']['home']['odd'] =='' && $arr['spread']['away']['odd'] == '') return;
            else {
                if(!empty($bbm))
                {
                    $arr['spread']['bm']['name'] =  $bbm['name'];
                    $arr['spread']['bm']['id']   =  $bbm['id'];
                }
                return true;
            }
        }


    }



    public function total(array &$arr,$odd_obj,$extn = null)
    {
        $item  = json_decode(json_encode($odd_obj),true);
        $bbm   =  isset($item['bm'])? json_decode($item['bm'],true): [];

        if(isset($item['odd']))
        {
            if(is_array($item['odd']))
            {
                foreach($item['odd'] as $total)
                {

                    if(isset($total['@attributes']))
                    {

                        if($total['@attributes']['name'] == 'Over')
                        {

                            $arr['total']['home']['odd']  =  $total['@attributes']['value'];
                        }

                        if($total['@attributes']['name'] == 'Under')
                        {
                            $arr['total']['away']['odd'] =  $total['@attributes']['value'];
                        }
                    }
                }
            }
        }

        if(isset($item['@attributes']))
        {
            if(isset($item['@attributes']['name']) && !empty($item['@attributes']['name']))
            {
                $arr['total']['home']['under_over'] = "Over ".$item['@attributes']['name'];
                $arr['total']['away']['under_over'] = "Under ".$item['@attributes']['name'];
            }
        }
        if($extn !=null && $extn->sport_id != 12)
        {
            if($arr['total']['home']['odd'] =='' && $arr['total']['away']['odd'] == '') return;
            else{
                if(!empty($bbm))
                {
                    $arr['total']['bm']['name'] =  $bbm['name'];
                    $arr['total']['bm']['id']   =  $bbm['id'];
                }
                return true;
            }
        }


    }

    public function moneyLine2(array &$arr,$odd_obj,$extn = null)
    {
        $item  = json_decode(json_encode($odd_obj),true);
        $bbm   =  isset($item['bm'])? json_decode($item['bm'],true): [];
        if(isset($item['odd']))
        {
            if(is_array($item['odd']))
            {
                foreach($item['odd'] as $total)
                {
                    if(isset($total['@attributes']))
                    {
                        if($total['@attributes']['name'] == 'Home')
                        {
                            $arr['total']['home']['odd']  =  $total['@attributes']['value'];
                        }

                        if($total['@attributes']['name'] == 'Away')
                        {
                            $arr['total']['away']['odd'] =  $total['@attributes']['value'];
                        }
                    }
                }
            }
        }

        if($arr['total']['home']['odd'] =='' && $arr['total']['away']['odd'] == '') return;

        if(!empty($bbm))
        {
            $arr['total']['bm']['name'] =  $bbm['name'];
            $arr['total']['bm']['id']   =  $bbm['id'];
        }
    }
}
