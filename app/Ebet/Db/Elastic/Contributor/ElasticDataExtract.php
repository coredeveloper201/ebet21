<?php
namespace App\Ebet\Db\Elastic\Contributor;


trait ElasticDataExtract{


    public function extracted($response,$sport)
    {
        return $response['hits']['hits'][0]['_source'][$sport.'_sports'];
    }
}
