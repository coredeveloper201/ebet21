<?php
namespace App\Ebet\Db\Elastic\Contributor;

use Elasticsearch\ClientBuilder;


trait ElasticDataFilter{

        private $query_params  = [
            'index' => 'baseball',
            'body'  =>
               ["query"=>
                   ["bool"=>
                       ["must"=>[],
                       "must_not"=>[],
                       "should"=>[]
                       ]
                   ],
                       "from"=>0,
                       "size"=>10,
                       "sort"=>[],
               ]
           ];
        private $params;

        public function &_mustMatch()
        {
               return $this->query_params['body']['query']['bool']['must'];
        }

        public function _LeagueBy(&$func,$sport,$league_ids)
        {
            $this->query_params['index'] = $sport;
            $ar = &$func;
            foreach($league_ids as $key => $val)
            {
                    $ar[] = ["match"=>
                      [$sport."_sports.league_id"=>$val]
                     ];
            }
            return $this;
        }



        public function queryExecute()
        {
          $client = ClientBuilder::create()->build();

          dd($this->query_params);
           $response = $client->search($this->query_params);
           return $response;
        }


    public function _getLeagueData($league_id,$sport_id)
    {
        $client = ClientBuilder::create()->build();
        $get_index = [
            'index' => "league_wise_data",
            'id'    => $sport_id.'_'.$league_id
        ];
       return  $client->getSource($get_index);
    }

}
