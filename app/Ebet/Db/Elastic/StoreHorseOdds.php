<?php
namespace App\Ebet\Db\Elastic;

use App\Ebet\Sports\Horse\TodayHorse;
use Elasticsearch\ClientBuilder;

class StoreHorseOdds {
    private $horses=[];
    /**
     * Class constructor.
     */
    public function __construct()
    {
       $this->horses['today']=$this->get_compressed('https://www.goalserve.com/getfeed/4d5f7e372939425c2b7d08d76ad955ff/racing/usa');
       $this->horses['tomorrow']=$this->get_compressed('https://www.goalserve.com/getfeed/4d5f7e372939425c2b7d08d76ad955ff/racing/usa_tomorrow');
    }
    public function get_compressed($url)
    {
        $opts = array(
            'http'=>array(
                'method'=>"GET",
                'header'=>"Accept-Language: en-US,en;q=0.8rn" .
                    "Accept-Encoding: gzip,deflate,sdchrn" .
                    "Accept-Charset:UTF-8,*;q=0.5rn" .
                    "User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:19.0) Gecko/20100101 Firefox/19.0 FirePHP/0.4rn"
            )
        );
        $context = stream_context_create($opts);
        $content = file_get_contents($url ,false,$context);
        foreach($http_response_header as $c => $h)
        {
            if(stristr($h, 'content-encoding') and stristr($h, 'gzip'))
            {
                $content = gzinflate( substr($content,10,-8) );
            }
        }
        //dd($content);
        return $content;
    }

    public function storeHorseData()
    {
        //d($this->horses);
        $client = ClientBuilder::create()->build();
        $arr = [];
        if(!empty($this->horses))
        {
            foreach($this->horses as $sprtKey => $sprt) {
                $params =
                    [
                        'index' => 'horse_'.$sprtKey,
                        'id' => 'horse_'.$sprtKey.'_id',
                        'body' => [$sprtKey.'_horse_data' => $this->horses[$sprtKey]]
                    ];
                $arr[] = $client->index($params);
            }
        }
        print_r($arr);
    }

    public function __invoke()
    {
        $this->storeHorseData();
    }

}
