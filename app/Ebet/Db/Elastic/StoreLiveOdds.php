<?php
namespace App\Ebet\Db\Elastic;

use App\Ebet\Sports\Live\Basket;
use App\Ebet\Sports\Live\Hockey;
use App\Ebet\Sports\Live\Soccer;
use App\Ebet\Sports\Live\Tennis;
use Elasticsearch\ClientBuilder;
use App\Ebet\Sports\Live\Baseball;
use App\Ebet\Sports\Live\Football;
use App\Ebet\Sports\Live\LiveBetFormat;

class StoreLiveOdds{

    private $sports = [];

    public function __construct()
    {
        $this->sports['hockey'] = new LiveBetFormat(new Hockey('http://inplay.goalserve.com/inplay-hockey.json'));
        $this->sports['basket'] = new LiveBetFormat(new Basket('http://inplay.goalserve.com/inplay-basket.json'));
        $this->sports['baseball'] = new LiveBetFormat(new Baseball('http://inplay.goalserve.com/inplay-baseball.json'));
        $this->sports['soccer'] = new LiveBetFormat(new Soccer('http://inplay.goalserve.com/inplay-soccer.json'));
        $this->sports['tennis'] = new LiveBetFormat( new Tennis('http://inplay.goalserve.com/inplay-tennis.json'));
        $this->sports['football'] = new LiveBetFormat( new Football('http://inplay.goalserve.com/inplay-amfootball.json'));
    }

    public function storeLiveSportsData()
    {
        $client = ClientBuilder::create()->build();
        $arr = [];
        if(!empty($this->sports))
        {
            foreach($this->sports as $sprtKey => $sprt)
            {
                $params = [
                    'index' => 'live_'.$sprtKey,
                    'id'    => 'live_'.$sprtKey.'_id',
                    'body'  => [$sprtKey.'_sports_data' => $sprt->getData()]
                ];
                $arr[] = $client->index($params);
            }
        }
        print_r($arr);
    }

    public function __invoke()
    {
        $this->storeLiveSportsData();
    }


}
