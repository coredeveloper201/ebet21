<?php
namespace App\Ebet\Results\Contributor;

trait WagerWiseResult{


    public function spreadWinner($event,$handicap)
    {
        $home_score = $event->event_info->home_score + floatval($handicap);
        $away_score = $event->event_info->away_score + floatval($handicap);
        return $home_score > $away_score ? 1: 0;
    }



    public function moneyWinner($event)
    {
        $home_score = $event->event_info->home_score;
        $away_score = $event->event_info->away_score;
        return $home_score > $away_score ? 1: 0;
    }


    public function totalWinner($event,$over_under)
    {
        $home_score = $event->event_info->home_score;
        $away_score = $event->event_info->away_score;
        return ($home_score+$away_score) > $over_under;
    }


    public function breakDownSs($ss)
    {
        $first_column = array();
        $second_column = array();
        $first_counter = 0;
        $second_counter = 0;
        $iFinalScoreArr = explode(",", $ss);
        if (count($iFinalScoreArr) != 0 && count($iFinalScoreArr) > 1) {
            foreach ($iFinalScoreArr as $key => $value) {
                $results = explode('-', $value);
                $first_column[] = $results[0];
                $second_column[] = $results[1];
            }
            for ($i = 0; $i < count($first_column); $i++) {
                if ($first_column[$i] > $second_column[$i])
                    $first_counter++;
                else
                    $second_counter++;
            }
            $res[] = $first_counter;
            $res[] = $second_counter;
            return $res;
        }
        return explode('-', $iFinalScoreArr[0]);
    }
}
