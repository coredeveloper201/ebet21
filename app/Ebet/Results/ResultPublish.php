<?php
namespace App\Ebet\Results;

use App\Ebet\Results\BaseResult;
use App\Ebet\Results\Contributor\WagerWiseResult;



class ResultPublish{

    use WagerWiseResult;

    private $sport;

     public function __construct(BaseResult $sport)
     {
        $this->sport = $sport;
     }


     public function getObj()
     {
         return $this->sport->evaluateResult();
     }

}
