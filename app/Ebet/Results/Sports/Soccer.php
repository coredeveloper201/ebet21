<?php
namespace App\Ebet\Results\Sports;

use App\Ebet\Results\BaseResult;

class Soccer extends BaseResult
{
    public function __construct($url,$sport_id)
    {
        parent::__construct($url,$sport_id);
        $this->getAdditionalResult();
    }
    public function getAdditionalResult()
    {
        foreach($this->dataArr->category as $cat)
        {

            if($this->ifLeague($cat->attributes()->id) !=null)
            {

                if(isset($cat->matches))
                {
                    foreach($cat->matches as $match)
                    {
                        if(isset($match->match))
                        {
                            foreach ($match->match as $ma) {

                                if(isset($ma->attributes()->id))
                                {
                                    $if_event = $this->ifEvent($ma->attributes()->id);
                                    if($if_event != null)
                                    {
                                        $item = $if_event;
                                        $item->first_quarter = "";
                                        $item->second_quarter = "";
                                        $item->third_quarter = "";
                                        $item->fourth_quarter = "";
                                        $item->overtime = "";

                                        $item->event_info->status = ( (array) $ma->attributes()->status)[0];

                                        if(isset($ma->ht) && isset($ma->ht->attributes()->score))
                                        {
                                            $item->first_quarter = str_replace('[','',explode(']',( (array) $ma->ht->attributes()->score)[0])[0]);
                                        }

                                    }
                                }
                            }

                        }

                    }
                }
            }
        }
    }

    public function evaluateResult()
    {
        return $this->events;
    }
}
