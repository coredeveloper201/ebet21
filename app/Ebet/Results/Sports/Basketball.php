<?php
namespace App\Ebet\Results\Sports;

use App\Ebet\Results\BaseResult;

class Basketball extends BaseResult
{
    public function __construct($url,$sport_id)
    {
        parent::__construct($url,$sport_id);
        $this->getAdditionalResult();
    }

    public function evaluateResult()
    {
        return $this->events;
    }

    public function getAdditionalResult()
    {
        foreach($this->dataArr->category as $cat)
        {
            if($this->ifLeague($cat->attributes()->id) !=null)
            {
                if(isset($cat->match))
                {
                    foreach($cat->match as $match)
                    {
                        if(isset($match->attributes()->id))
                        {
                            $if_event = $this->ifEvent($match->attributes()->id);
                            if($if_event != null)
                            {

                                $item = $if_event;
                                $item->first_quarter = "";
                                $item->second_quarter = "";
                                $item->third_quarter = "";
                                $item->fourth_quarter = "";
                                $item->overtime = "";

                                $item->event_info->status = ( (array) $match->attributes()->status)[0];

                                if(isset($match->localteam))
                                {
                                        $event = $match->localteam;

                                        if(isset($event->attributes()->q1))
                                        {
                                            $item->first_quarter .= ( (array)  $event->attributes()->q1)[0];
                                        }

                                        if(isset($event->attributes()->q2))
                                        {
                                            $item->second_quarter .=  ( (array) $event->attributes()->q2)[0];
                                        }

                                        if(isset($event->attributes()->q3))
                                        {
                                            $item->third_quarter .=  ( (array) $event->attributes()->q3)[0];
                                        }
                                        if(isset($event->attributes()->q4))
                                        {
                                            $item->fourth_quarter .=  ( (array) $event->attributes()->q4)[0];
                                        }
                                        if(isset($event->ot))
                                        {
                                            $item->overtime .=  ( (array) $event->ot)[0];
                                        }

                                }

                                if(isset($match->awayteam))
                                {
                                    $event = $match->awayteam;
                                    if(isset($event->attributes()->q1))
                                    {
                                        if(( (array)  $event->attributes()->q1)[0] !='')
                                        $item->first_quarter .= "-".( (array)  $event->attributes()->q1)[0];
                                    }

                                    if(isset($event->attributes()->q2))
                                    {
                                        if(( (array) $event->attributes()->q2)[0] !='')
                                        $item->second_quarter .= "-". ( (array) $event->attributes()->q2)[0];
                                    }

                                    if(isset($event->attributes()->q3))
                                    {
                                        if(( (array)  $event->attributes()->q3)[0] !='')
                                        $item->third_quarter .= "-". ( (array) $event->attributes()->q3)[0];
                                    }
                                    if(isset($event->attributes()->q4))
                                    {
                                        if(( (array)  $event->attributes()->q4)[0] !='')
                                        $item->fourth_quarter .= "-". ( (array) $event->attributes()->q4)[0];
                                    }
                                    if(isset($event->ot))
                                    {
                                        if(((array) $event->ot)[0])
                                        $item->overtime .=  "-".( (array) $event->ot)[0];
                                    }
                                }

                            }
                        }
                    }
                }
            }
        }
    }
}
