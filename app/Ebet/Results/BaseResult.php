<?php
namespace App\Ebet\Results;

use stdClass;
use App\Ebet\Sports\SportAbstract;


abstract class BaseResult extends SportAbstract{

    public function __construct($url,$sport_id)
    {
        parent::__construct($url,$sport_id);
    }

    public function getItem(){
        echo 'nothing has implemented yet';
    }

    public function getEvents(){

        foreach($this->leagues as $league)
        {
            if($this->sport_id == 151)
            {
                foreach($this->dataArr->match as $cat)
                 {
                    if($league->league_id  == $cat->attributes()->league_id)
                    {
                        $item = $league;
                        $event_obj = new stdClass;
                        $event_obj->event_info = new stdClass;
                        $event_obj->event_info->home_name = "";
                        $event_obj->event_info->home_id = "";
                        $event_obj->event_info->home_score = "";
                        $event_obj->event_info->away_id = "";
                        $event_obj->event_info->away_name  = "";
                        $event_obj->event_info->away_score  = "";
                        $event_obj->event_info->status  = "";
                        if(isset($cat->localteam))
                            {
                                $event_obj->event_info->home_name =  ( (array) $cat->localteam->attributes()->name)[0];
                                $event_obj->event_info->home_id =   ( (array)  $cat->localteam->attributes()->id)[0];
                                $event_obj->event_info->home_score = ( (array)  $cat->localteam->attributes()->score)[0];
                            }

                            if(isset($cat->awayteam))
                            {
                                $event_obj->event_info->away_name = ( (array) $cat->awayteam->attributes()->name)[0];
                                $event_obj->event_info->away_id =   ( (array) $cat->awayteam->attributes()->id)[0];
                                $event_obj->event_info->away_score = ( (array)$cat->awayteam->attributes()->score)[0];
                            }


                            $event_obj->event_info->id   =        isset($cat->attributes()->id) ?  ((array)$cat->attributes()->id)[0] : "";
                            $event_obj->event_info->e_time =      ((array)$cat->attributes()->time)[0];
                            $event_obj->event_info->e_date =      ((array)$cat->attributes()->date)[0];
                            if(!empty($event_obj->event_info->id))
                            {
                                $this->events[$event_obj->event_info->id] = $event_obj;
                            }
                            $item->events[] = $event_obj;

                    }
                }
            }
            else
            {
                foreach($this->dataArr->category as $cat)
                {
                    if($league->league_id  == $cat->attributes()->id)
                    {
                        $item = $league;

                        $events = $cat->match;
                        if($this->sport_id == 1)
                        {
                            $events =  $cat->matches->match;
                        }

                        $item->events = [];
                        foreach($events as $event)
                        {
                            $event_obj = new stdClass;
                            $event_obj->event_info = new stdClass;
                            $event_obj->event_info->home_name = "";
                            $event_obj->event_info->home_id = "";
                            $event_obj->event_info->home_score = "";
                            $event_obj->event_info->away_id = "";
                            $event_obj->event_info->away_name  = "";
                            $event_obj->event_info->away_score  = "";
                            $event_obj->event_info->status  = "";
                            if(isset($event->localteam))
                            {
                                $event_obj->event_info->home_name =  ( (array) $event->localteam->attributes()->name)[0];
                                $event_obj->event_info->home_id =   ( (array)  $event->localteam->attributes()->id)[0];
                                if($this->sport_id == 1)
                                {

                                    $event_obj->event_info->home_score = ( (array)  $event->localteam->attributes()->goals)[0];
                                }
                                else if($this->sport_id == 15)
                                {
                                    $event_obj->event_info->home_score =  ( (array)  $event->localteam->attributes()->runs)[0];
                                }
                                else
                                {
                                     $event_obj->event_info->home_score = ( (array)  $event->localteam->attributes()->totalscore)[0];
                                }
                            }
                            elseif (isset($event->player[0]))
                            {
                                $event_obj->event_info->home_name = ( (array) $event->player[0]->attributes()->name)[0];
                                $event_obj->event_info->home_id   = ( (array) $event->player[0]->attributes()->id)[0];
                                $event_obj->event_info->home_score = ( (array)$event->player[0]->attributes()->totalscore)[0];
                            }

                            if(isset($event->visitorteam))
                            {
                                $event_obj->event_info->away_name = ( (array) $event->visitorteam->attributes()->name)[0];
                                $event_obj->event_info->away_id =   ( (array) $event->visitorteam->attributes()->id)[0];
                                if($this->sport_id == 1)
                                {
                                    $event_obj->event_info->away_score = ( (array)$event->visitorteam->attributes()->goals)[0];
                                }
                                else
                                {
                                    $event_obj->event_info->away_score = ( (array)$event->visitorteam->attributes()->totalscore)[0];
                                }
                            }
                            if(isset($event->awayteam))
                            {
                                $event_obj->event_info->away_name  = ( (array) $event->awayteam->attributes()->name)[0];
                                $event_obj->event_info->away_id    =   ( (array) $event->awayteam->attributes()->id)[0];

                                if($this->sport_id == 15)
                                {
                                    $event_obj->event_info->away_score = ( (array)$event->awayteam->attributes()->runs)[0];
                                }
                                else
                                {
                                    $event_obj->event_info->away_score = ( (array)$event->awayteam->attributes()->totalscore)[0];
                                }

                            }
                            elseif (isset($event->player[1]))
                            {
                                $event_obj->event_info->away_name = ( (array) $event->player[1]->attributes()->name)[0];
                                $event_obj->event_info->away_id   = ( (array) $event->player[1]->attributes()->id)[0];
                                $event_obj->event_info->away_score = ( (array)$event->player[1]->attributes()->totalscore)[0];
                            }

                            if($this->sport_id == 17) {
                                $event_obj->event_info->id          =  isset($event->attributes()->id) ?  ((array)$event->attributes()->id)[0] : "";
                            }
                            else {
                                $event_obj->event_info->id          =  isset($event->attributes()->id) ?  ((array)$event->attributes()->id)[0] : "";
                            }

                            $event_obj->event_info->time            =   isset($event->attributes()->formatted_date) ? strtotime($event->attributes()->formatted_date .' '. $event->attributes()->time) : time();


                            if(!empty($event_obj->event_info->id))
                            {
                                $this->events[$event_obj->event_info->id] = $event_obj;
                            }
                            $item->events[] = $event_obj;
                        }
                    }
                }
            }

        }
    }


}



