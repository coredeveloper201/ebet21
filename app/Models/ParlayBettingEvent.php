<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ParlayBettingEvent extends Model
{
    //
    protected $table="parlay_betting_events";
    protected $fillable=['user_id','ticket_id','event_id','score_away','score_home','winner_away','winner_home','score_home_by_period','score_away_by_period',
        'event_status','type','betting_type'];


    public function details()
    {
        return $this->hasMany('App\Models\ParlayBettingDetail', 'event_id', 'event_id');
    }
}
