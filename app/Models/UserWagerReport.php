<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserWagerReport extends Model
{
    protected $fillable = ['user_id','pending','balance'];
}
