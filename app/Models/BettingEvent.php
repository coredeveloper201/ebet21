<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BettingEvent extends Model
{
    protected $fillable = [
        'event_id'
        , 'score_away'
        , 'score_home'
        , 'winner_away'
        , 'winner_home'
        , 'score_away_by_period'
        , 'score_home_by_period'
        , 'event_status'
        , 'betting_type'
        , 'handicap'
        , 'wager'
        , 'team_type'
    ];

    public function details()
    {
        return $this->hasMany('App\Models\BettingDetail', 'event_id', 'event_id');
    }
}


