<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CreditLimit extends Model
{
    protected $fillable = ['user_id', 'all_min_amount', 'all_max_amount', 'straight_min_amount', 'straight_max_amount', 'casino_min_amount', 'casino_max_amount'];

    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }
}
