<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ParlayEventId extends Model
{
    protected $table="parlay_event_ids";
    protected $fillable = [
        'user_id', 'ticket_id', 'token_id','event_ids', 'risk_amount', 'parlay_win_amount', 'result', 'types', 'free_play', 'betting_type',
    ];
}
