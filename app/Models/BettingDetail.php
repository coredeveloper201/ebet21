<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BettingDetail extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'sport_league','user_id','ticket_id','o_and_u','event_id','event_date', 'event_date', 'team_id', 'sport_name', 'is_away',
        'is_home', 'betting_win', 'betting_condition', 'betting_condition_original', 'type', 'wager', 'risk_amount', 'betting_amount', 'token_id', 'status',
        'result', 'user_current_credit_limit', 'user_current_max_limit', 'user_current_pending', 'user_current_balance', 'free_play', 'bet_type', 'bet_extra_info',
    ];
    public static function getWagers()
    {
        return static::select('users.username','betting_details.*')
        ->join('users','users.id','=','betting_details.user_id')
        ->where('betting_details.result','=','0')
        ->get();
    }

}
