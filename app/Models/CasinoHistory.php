<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CasinoHistory extends Model
{
    protected $fillable = [
        'user_id', 'game_id', 'random_id', 'amount', 'type',
    ];
}
