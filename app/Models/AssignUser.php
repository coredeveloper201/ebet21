<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AssignUser extends Model
{
    protected $fillable = [
        'agent_id', 'user_id',
    ];
}
