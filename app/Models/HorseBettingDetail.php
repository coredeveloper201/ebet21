<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HorseBettingDetail extends Model
{
    protected $table = "horse_betting_details";
    protected $fillable = [
        'user_id','ticket_id', 'tournament_id', 'tournament_name', 'race_id',  'race_name',  'race_type',  'race_date',  'horse_id',  'horse_number', 'horse_name', 'unique_id','result','risk_amount','win_amount'
    ];
}
