<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HorseBettingEvent extends Model
{
    protected $table = "horse_betting_events";
    protected $fillable = [
        'user_id','ticket_id', 'tournament_id', 'tournament_name', 'race_id', 'race_name','race_date', 'horse_id', 'horse_number', 'unique_id','horse_name', 'result','win','place','show'
    ];
}
