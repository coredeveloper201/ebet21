<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AgentRate extends Model
{
    protected $fillable = [
        'player_id', 'user_id', 'rate', 'create_date', 'week_start', 'week_end', 'week_number',
    ];
}
