console.groupCollapsed("live hockey");
liveBet.controller('realTimeBettingController3cvc', function ($scope, $http, $filter) {
     $scope.bettingResult = [];
     $scope.oldBettingData = [];
     $scope.confirmData = [];
     $scope.loading = true;
     $scope.selectedData = [];
     $scope.timePeriod = 20000;
     $scope.totalStake = 0;
     $scope.possibleWin = 0;
     $scope.confirmSuccess = false;
     $scope.confirmFail = false;
     let getApi;

     $scope.isChanged = false;
     $scope.slipOddTotal = 0;

     $scope.changeNow = false;


     $scope.timeFormate = function(timestap)
     {
         timestap = parseInt(timestap);
         let h = padZero(new Date(timestap).getHours());
         let m = padZero(new Date(timestap).getMinutes());
         return h+":"+m;
     }

     function padZero(n) {
         if (n < 10) return '0' + n;
         return n;
     }

     getBetData();

     $scope.$on('activeGame',function(event,opt){
        for(let item in opt)
        {
            if(item === 'hockey')
            {
                getBetData();
            }
        }
    });


     function getApiResponse() {
         if($scope.$parent.tab_contents.hockey)
         {
            getApi = setInterval(function () {
                getBetData();
            }, $scope.timePeriod);
         }

     }

     function getBetData() {
         $http.get('/live-hockey').then(function(response) {

             if (response.data) {
                $scope.$parent.loading = false;
                 $scope.oldBettingData = $scope.bettingResult;
                 $scope.bettingResult = response.data;
                 $scope.$parent.sports_avaiable.hockey = ($scope.bettingResult.length == 0) ? false:true;

                 console.log(response.data);
                 betRefresh();
                 $scope.loading = false;
                 if ($scope.bettingResult.length < 5)
                 {
                    clearInterval(getApi);
                    $scope.timePeriod = 3000;
                    getApiResponse();
                }
                 else if ($scope.bettingResult.length >= 5 &&  $scope.bettingResult.length < 10)
                 {
                     clearInterval(getApi);
                     $scope.timePeriod = 5000;
                     getApiResponse();
                }
                 else if ($scope.bettingResult.length >= 10 && $scope.bettingResult.length < 25)
                 {
                     clearInterval(getApi);
                     $scope.timePeriod = 10000;
                     getApiResponse();
                }
                 else if ($scope.bettingResult.length >= 25 && $scope.bettingResult.length < 35) {
                      clearInterval(getApi);
                      $scope.timePeriod = 15000;
                      getApiResponse();
                    }
                 else if ($scope.bettingResult.length >= 35) {
                      clearInterval(getApi);
                      $scope.timePeriod = 20000;
                      getApiResponse();
                    }
             }
         });
     }

     // SET selected value to latest value.
     function betRefresh() {
         let newTotal = 0;
         $scope.selectedData.forEach((item) => {
             let replaced = false;
             $scope.bettingResult.forEach((rs) => {
                 rs.results.forEach(result=>{
                     if(item.id === result.id) {
                         switch (item.type) {
                             case 'spread_home':
                                 if (result.spread_line.home_odd) {
                                     item.score = result.spread_line.home_odd;
                                     replaced = true;
                                 }
                                 break;
                             case 'spread_away':
                                 if (result.spread_line.away_odd) {
                                     item.score = result.spread_line.away_odd;
                                     replaced = true;
                                 }
                                 break;
                             case 'total_home':
                                 if ( result.over.home_odd) {
                                    item.score = result.over.home_odd;
                                    replaced = true;
                                 }
                                 break;
                             case 'total_away':
                                 if (result.under.away_odd) {
                                    item.score = result.under.away_odd;
                                    replaced = true;
                                 }
                                 break;
                             case 'money_home':
                                 if (result.money_line.home_odd) {
                                     item.score = result.money_line.home_odd;
                                     replaced = true;
                                 }
                                 break;
                             case 'money_away':
                                 if (result.money_line.away_odd) {
                                    item.score = result.money_line.away_odd;
                                    replaced = true;
                                 }
                                 break;

                             case 'draw_line':
                                 if (result.draw) {
                                     item.score = result.draw;
                                     replaced = true;
                                 }
                                 break;
                         }
                         if ($scope.changeNow) {
                             newTotal += item.score;
                         }

                     }
                 });

             });
             if (!replaced) $scope.selectedData.splice($scope.selectedData.indexOf(item), 1);
             calculateReport();
         });
         if ($scope.changeNow) {
             if (newTotal !== $scope.slipOddTotal)
                 $scope.isChanged  = true;
         }

     }

     // Increase, Decrease Icon && Add active class
     $scope.compareODD = function(id, type) {

         let oldFirstVal = 0;
         let curFirstVal = 0;
         let selected = false;

         if ($scope.oldBettingData.length < 1) return;

         if (type === 'spread_home') {

             $scope.selectedData.forEach((item) => {
                 if (item.id === id && item.type === type) {
                     $('#spread_home'+id).addClass('active');
                     selected = true;
                 }
             });

             if (!selected) $('#spread_home'+id).removeClass('active');

             angular.forEach($scope.oldBettingData, function (ite) {
                 ite.results.forEach(item=>{
                    if (item.id === id)
                    {
                        oldFirstVal = item.spread_line.home_odd;
                    }
                 });
             });

             angular.forEach($scope.bettingResult, function (ite) {
                 ite.results.forEach(item=> {
                     if (item.id === id) {
                         curFirstVal = item.spread_line.home_odd;
                     }
                 });
             });
             $("#betSlip_"+id+"_"+type+"_decrease").hide();
             $("#betSlip_"+id+"_"+type+"_increase").hide();
             if (oldFirstVal > curFirstVal) {
                 $('#spread_home'+id).css('color', 'red');
                 $('#spread_home'+id+'_decrease').show();
                 $('#spread_home' + id + '_decrease').closest('li').addClass('__blink');
                 $("#betSlip_"+id+"_"+type+"_decrease").show();
                 betChanges(id,type);
             }
             else if (oldFirstVal < curFirstVal) {
                 $('#spread_home'+id).css('color', 'green');
                 $('#spread_home'+id+'_increase').show();
                 $('#spread_home' + id + '_increase').closest('li').addClass('__blink');
                 $("#betSlip_"+id+"_"+type+"_increase").show();
                 betChanges(id,type);
             }
             setTimeout(function(){
                $('#spread_home'+id+'_decrease').hide();
                $('#spread_home'+id+'_increase').hide();
                $('#spread_home'+id).css('color', 'inherit');
             }, 3000)
         }
         else if (type === 'spread_away') {
             $scope.selectedData.forEach((item) => {
                 if (item.id === id && item.type === type) {
                     $('#spread_away'+id).addClass('active');
                     selected = true;
                 }
             });

             if (!selected) $('#spread_away'+id).removeClass('active');

             angular.forEach($scope.oldBettingData, function (ite) {
                 ite.results.forEach(item=> {
                     if (item.id === id) {
                         oldFirstVal = item.spread_line.away_odd;
                     }
                 });
             });

             angular.forEach($scope.bettingResult, function (ite) {
                 ite.results.forEach(item=> {
                     if (item.id === id) {
                         curFirstVal = item.spread_line.away_odd;
                     }
                 });
             });
             $("#betSlip_"+id+"_"+type+"_decrease").hide();
             $("#betSlip_"+id+"_"+type+"_increase").hide();
             if (oldFirstVal > curFirstVal) {
                 $('#spread_away'+id).css('color', 'red');
                 $('#spread_away'+id+'_decrease').show();
                 $('#spread_away' + id + '_decrease').closest('li').addClass('__blink');
                 $("#betSlip_"+id+"_"+type+"_decrease").show();
                 betChanges(id,type);
             }
             else if (oldFirstVal < curFirstVal) {
                 $('#spread_away'+id).css('color', 'green');
                 $('#spread_away'+id+'_increase').show();
                 $('#spread_away' + id + '_increase').closest('li').addClass('__blink');

                 $("#betSlip_"+id+"_"+type+"_increase").show();
                 betChanges(id,type);
             }
             setTimeout(function(){
                $('#spread_away'+id+'_decrease').hide();
                $('#spread_away'+id+'_increase').hide();
                $('#spread_away'+id).css('color', 'inherit');
             }, 3000)
         }
         else if (type === 'total_home') {
             $scope.selectedData.forEach((item) => {
                 if (item.id === id && item.type === type) {
                     $('#total_home'+id).addClass('active');
                     selected = true;
                 }
             });

             if (!selected) $('#total_home'+id).removeClass('active');

             angular.forEach($scope.oldBettingData, function (ite) {
                 ite.results.forEach(item=> {
                     if (item.id === id) {
                         oldFirstVal = item.over.home_odd;
                     }
                 });
             });

             angular.forEach($scope.bettingResult, function (ite) {
                 ite.results.forEach(item=> {
                     if (item.id === id) {
                         curFirstVal = item.over.home_odd;
                     }
                 });
             });
             $("#betSlip_"+id+"_"+type+"_decrease").hide();
             $("#betSlip_"+id+"_"+type+"_increase").hide();
             if (oldFirstVal > curFirstVal) {
                 $('#total_home'+id).css('color', 'red');
                 $('#total_home'+id+'_decrease').show();
                 $('#total_home' + id + '_decrease').closest('li').addClass('__blink');
                 $("#betSlip_"+id+"_"+type+"_decrease").show();
                 betChanges(id,type);
             }
             else if (oldFirstVal < curFirstVal) {
                 $('#total_home'+id).css('color', 'green');
                 $('#total_home'+id+'_increase').show();
                 $('#total_home' + id + '_increase').closest('li').addClass('__blink');
                 $("#betSlip_"+id+"_"+type+"_increase").show();
                 betChanges(id,type);
             }
             setTimeout(function(){
                $('#total_home'+id+'_decrease').hide();
                $('#total_home'+id+'_increase').hide();
                $('#total_home'+id).css('color', 'inherit');
             }, 3000)
         }
         else if (type === 'total_away') {
             $scope.selectedData.forEach((item) => {
                 if (item.id === id && item.type === type) {
                     $('#total_away'+id).addClass('active');
                     selected = true;
                 }
             });

             if (!selected) $('#total_away'+id).removeClass('active');

             angular.forEach($scope.oldBettingData, function (ite) {
                 ite.results.forEach(item=> {
                     if (item.id === id) {
                         oldFirstVal = item.under.away_odd;
                     }
                 });
             });

             angular.forEach($scope.bettingResult, function (ite) {
                 ite.results.forEach(item=> {
                     if (item.id === id) {
                         curFirstVal = item.under.away_odd;
                     }
                 });
             });
             $("#betSlip_"+id+"_"+type+"_decrease").hide();
             $("#betSlip_"+id+"_"+type+"_increase").hide();
             if (oldFirstVal > curFirstVal) {
                 $('#total_away'+id).css('color', 'red');
                 $('#total_away'+id+'_decrease').show();
                 $('#total_away' + id + '_decrease').closest('li').addClass('__blink');

                 $("#betSlip_"+id+"_"+type+"_decrease").show();
                 betChanges(id,type);
             }
             else if (oldFirstVal < curFirstVal) {
                 $('#total_away'+id).css('color', 'green');
                 $('#total_away'+id+'_increase').show();
                 $('#total_away' + id + '_increase').closest('li').addClass('__blink');

                 $("#betSlip_"+id+"_"+type+"_increase").show();
                 betChanges(id,type);
             }
             setTimeout(function(){
                $('#total_away'+id+'_decrease').hide();
                $('#total_away'+id+'_increase').hide();
                $('#total_away'+id).css('color', 'inherit');
             }, 3000)
         }
         else if (type === 'money_home') {
             $scope.selectedData.forEach((item) => {
                 if (item.id === id && item.type === type) {
                     $('#money_home'+id).addClass('active');
                     selected = true;
                 }
             });

             if (!selected) $('#money_home'+id).removeClass('active');

             angular.forEach($scope.oldBettingData, function (ite) {
                 ite.results.forEach(item=> {
                     if (item.id === id) {
                         oldFirstVal = item.money_line.home_odd;
                     }
                 });
             });

             angular.forEach($scope.bettingResult, function (ite) {
                 ite.results.forEach(item=> {
                     if (item.id === id) {
                         curFirstVal = item.money_line.home_odd;
                     }
                 });
             });
             $("#betSlip_"+id+"_"+type+"_decrease").hide();
             $("#betSlip_"+id+"_"+type+"_increase").hide();
             if (oldFirstVal > curFirstVal) {
                 $('#money_home'+id).css('color', 'red');
                 $('#money_home'+id+'_decrease').show();
                 $('#money_home' + id + '_decrease').closest('li').addClass('__blink');

                 $("#betSlip_"+id+"_"+type+"_decrease").show();
                 betChanges(id,type);
             }
             else if (oldFirstVal < curFirstVal) {
                 $('#money_home'+id).css('color', 'green');
                 $('#money_home'+id+'_increase').show();
                 $('#money_home' + id + '_increase').closest('li').addClass('__blink');

                 $("#betSlip_"+id+"_"+type+"_increase").show();
                 betChanges(id,type);
             }
             setTimeout(function(){
                $('#money_home'+id+'_decrease').hide();
                $('#money_home'+id+'_increase').hide();
                $('#money_home'+id).css('color', 'inherit');
             }, 3000)
         }
         else if (type === 'money_away') {
             $scope.selectedData.forEach((item) => {
                 if (item.id === id && item.type === type) {
                     $('#money_away'+id).addClass('active');
                     selected = true;
                 }
             });

             if (!selected) $('#money_away'+id).removeClass('active');

             angular.forEach($scope.oldBettingData, function (ite) {
                 ite.results.forEach(item=> {
                     if (item.id === id) {
                         oldFirstVal = item.money_line.away_odd;
                     }
                 });
             });

             angular.forEach($scope.bettingResult, function (ite) {
                 ite.results.forEach(item=> {
                     if (item.id === id) {
                         curFirstVal = item.money_line.away_odd;
                     }
                 });
             });
             $("#betSlip_"+id+"_"+type+"_decrease").hide();
             $("#betSlip_"+id+"_"+type+"_increase").hide();
             if (oldFirstVal > curFirstVal) {
                 $('#money_away'+id).css('color', 'red');
                 $('#money_away'+id+'_decrease').show();
                 $('#money_away' + id + '_decrease').closest('li').addClass('__blink');

                 $("#betSlip_"+id+"_"+type+"_decrease").show();
                 betChanges(id,type);
             }
             else if (oldFirstVal < curFirstVal) {
                 $('#money_away'+id).css('color', 'green');
                 $('#money_away'+id+'_increase').show();
                 $('#money_away' + id + '_increase').closest('li').addClass('__blink');

                 $("#betSlip_"+id+"_"+type+"_increase").show();
                 betChanges(id,type);
             }
             setTimeout(function(){
                $('#money_away'+id+'_decrease').hide();
                $('#money_away'+id+'_increase').hide();
                $('#money_away'+id).css('color', 'inherit');
             }, 3000)
         }
         else if (type === 'draw_line') {
             $scope.selectedData.forEach((item) => {
                 if (item.id === id && item.type === type) {
                     $('#draw_line'+id).addClass('active');
                     selected = true;
                 }
             });

             if (!selected) $('#draw_line'+id).removeClass('active');

             angular.forEach($scope.oldBettingData, function (ite) {
                 ite.results.forEach(item=> {
                     if (item.id === id) {
                         oldFirstVal = item.draw;
                     }
                 });
             });

             angular.forEach($scope.bettingResult, function (ite) {
                 ite.results.forEach(item=> {
                     if (item.id === id) {
                         curFirstVal = item.draw;
                     }
                 });
             });
             $("#betSlip_"+id+"_"+type+"_decrease").hide();
             $("#betSlip_"+id+"_"+type+"_increase").hide();
             if (oldFirstVal > curFirstVal) {
                 $('#draw_line'+id).css('color', 'red');
                 $('#draw_line'+id+'_decrease').show();
                 $('#draw_line' + id + '_decrease').closest('li').addClass('__blink');
                 $("#betSlip_"+id+"_"+type+"_decrease").show();
                 betChanges(id,type);
             }
             else if (oldFirstVal < curFirstVal) {
                 $('#draw_line'+id).css('color', 'green');
                 $('#draw_line'+id+'_increase').show();
                 $('#draw_line' + id + '_increase').closest('li').addClass('__blink');
                 $("#betSlip_"+id+"_"+type+"_increase").show();
                 betChanges(id,type);
             }
             setTimeout(function(){
                $('#draw_line'+id+'_decrease').hide();
                $('#draw_line'+id+'_increase').hide();
                $('#draw_line'+id).css('color', 'inherit');
             }, 3000)
         }
     };

     // Add To Cart ( BET Slip function )
     $scope.addtocart = function (id, type){
         let selected = false;
         let result = [];
         let league_name ='';

         $scope.selectedData.forEach((item) => {
             if (item.id === id && item.type === type) {
                 $scope.selectedData.splice($scope.selectedData.indexOf(item), 1);
                 $('#'+type+id).removeClass('active');
                 selected = true;
             }
         });

         if (!selected) {
             $scope.bettingResult.forEach((dt) => {
                 dt.results.forEach(data=>{
                     if (data.id === id)
                     {
                         result = data;
                         league_name =dt.even_name;
                     }
                 })
             });

             let date = new Date(result.time*1000);

             let betObject = {
                 id: id,
                 type: type,
                 win: "",
                 risk: "",
                 score: 0,
                 teamName: league_name,
                 displayName: '',
                 column: '',
                 wager: '',
                 isHome: 0,
                 isAway: 0,
                 spread_handicap : null,
                 teamHomeName: result.home.name,
                 teamAwayName: result.away.name,
                 dateTime: result.datetime,
                 event_date: result.time,
                 bet_extra_info:{},
             };
             let slug;
             switch (type) {
                 case 'spread_home':
                     betObject.isHome = 1;
                     betObject.displayName = result.home.name;
                     betObject.wager = 'Spread';
                     betObject.column = '1';
                     betObject.score = result.spread_line.home_odd;
                     betObject.spread_handicap = result.spread_line.home.handicap;
                     slug = "home";
                     break;
                 case 'spread_away':
                     betObject.isAway = 1;
                     betObject.displayName = result.away.name;
                     betObject.wager = 'Spread';
                     betObject.column = '1';
                     betObject.score = result.spread_line.away_odd;
                     betObject.spread_handicap = result.spread_line.away.handicap;
                     slug = "away";
                     break;
                 case 'total_home':
                     betObject.isHome = 1;
                     betObject.displayName = result.over.handicap;
                     betObject.wager = 'Total';
                     betObject.score = result.over.home_odd;
                     betObject.column = '3';
                     slug = "home";
                     break;
                 case 'total_away':
                     betObject.isAway = 1;
                     betObject.displayName = result.under.handicap;
                     betObject.wager = 'Total';
                     betObject.score = result.over.home_odd;
                     betObject.column = '3';
                     slug = "away";
                     break;
                 case 'money_home':
                     betObject.isHome = 1;
                     betObject.displayName = result.home.name;
                     betObject.wager = 'Money Line';
                     betObject.column = '2';
                     betObject.score = result.money_line.home_odd;
                     slug = "home";
                     break;
                 case 'money_away':
                     betObject.isAway = 1;
                     betObject.displayName = result.away.name;
                     betObject.wager = 'Money Line';
                     betObject.column = '2';
                     betObject.score = result.money_line.away_odd;
                     slug = "away";
                     break;
                 case 'draw_line':
                     betObject.displayName =  "Draw";
                     betObject.column = '123';
                     betObject.score = result.draw;
                     break;
             }
             if(type === "draw_line")
             {
                betObject.bet_extra_info = {
                    bet_on_team_name : result.home.name,
                    other_team_name  : result.away.name,
                    betting_slug     : slug,
                    betting_wager    : betObject.wager
                };
             }
             else
             {
                betObject.bet_extra_info = {
                    bet_on_team_name : betObject.teamName,
                    other_team_name  : (betObject.teamName === result.home.name) ? result.away.name : result.home.name,
                    betting_slug     : slug,
                    betting_wager    : betObject.wager
                };
             }

             $scope.slipOddTotal += betObject.score;
             $scope.selectedData.push(betObject);
             $('#'+type+id).addClass('active');
         }
         calculateReport();
     };

     // Remove Card Item
     $scope.removeCardItem = function (id, type) {
         $scope.selectedData.forEach((item) => {
             if (item.id === id && item.type === type) {
                 $scope.selectedData.splice($scope.selectedData.indexOf(item), 1);
                 $('#'+type+id).removeClass('active')
             }
         });
         calculateReport();
     };
     $scope.clearBetSlip = function(){

         if ($scope.selectedData.length === 0)
         {
             alert("You haven't selected a slip yet");
         }
         else
         {
             $scope.selectedData.forEach(function (item) {
                 $('#'+item.type+item.id).removeClass('active');
             });
             $scope.selectedData = [];
             calculateReport();
         }

     };


     function betChanges(id, type) {

        $scope.selectedData.forEach((item) => {
            if (item.id === id && item.type === type) {

                if (item.score >= 0) item.win = Math.round(item.score * item.risk) / 100;
                else item.win = Math.round((10000 / item.score) * item.risk) * (-1) / 100;

                if (item.score >= 0) item.risk = Math.round((10000 / item.score) * item.win) / 100;
                else item.risk = Math.round(item.win * item.score) * (-1) / 100;

            }
        });
        calculateReport();
    };
     $scope.betChange = function (id, type, model) {
         $scope.selectedData.forEach((item) => {
             if (item.id === id && item.type === type) {
                 if (model === 'hockey_risk_') {
                     if (item.score >= 0) item.win = Math.round(item.score*item.risk)/100;
                     else item.win = Math.round((10000 / item.score)*item.risk)* (-1) / 100;
                 } else {
                     if (item.score >= 0) item.risk = Math.round((10000 / item.score)*item.win)/100;
                     else item.risk = Math.round(item.win *item.score)* (-1) / 100;
                 }
             }
         });
         calculateReport();
     };

     function calculateReport() {
         $scope.possibleWin = 0;
         $scope.totalStake = 0;
         $scope.selectedData.forEach((item) => {
             $scope.possibleWin += parseFloat(item.win);
             $scope.totalStake += parseFloat(item.risk);
         })
     }

     $scope.confirmBet = function () {
         if ($scope.selectedData.length != 0)
         {
             $scope.changeNow = true;
             $("#confirm_passwordHockey").removeClass('d-none');

             $("#submit_confirm_passwordHockey").off().click(function (e) {
                 let pass = $("#bet_confirm_passwordHockey").val();
                 if (pass.length === 0)
                 {
                     alert("password fields can't be empty")
                 }
                 else
                 {
                     $http.post("/check-user-password", {
                         "_token": $('meta[name="csrf-token"]').attr('content'),
                          pass: pass
                         }).then(function(fgf)
                         {
                             let response = fgf.data;
                             if (response.status)
                             {
                                 $("#confirm_passwordHockey").addClass('d-none');
                                 if (!$scope.isChanged)
                                 {

                                     $scope.selectedData.forEach((item) => {
                                         $scope.bettingResult.forEach((ad) => {
                                             ad.results.forEach(apiData=>{
                                                 if (item.id === apiData.id) {
                                                     let carditem = {};
                                                     carditem.sport_league = JSON.stringify(['17', item.teamName]);
                                                     carditem.is_away            = item.isAway;
                                                     carditem.is_home            = item.isHome;
                                                     carditem.column             = item.column;
                                                     carditem.o_and_u            = '';
                                                     carditem.teamname           = item.teamName;
                                                     carditem.team_id            = item.teamName === apiData.home.name? apiData.home.id: apiData.away.id;
                                                     carditem.even_id            = item.id;
                                                     carditem.scores             = item.score;
                                                     carditem.event_date         = item.event_date;
                                                     carditem.sport_id = '12';
                                                     let scorre = (item.score>0)?'+'+item.score:item.score;
                                                     if (carditem.column === '1')
                                                     {
                                                        carditem.original_money     = item.spread_handicap !== null ? "("+item.spread_handicap+") "+scorre : scorre;
                                                     }
                                                     else if(carditem.column === '3')
                                                     {
                                                        carditem.original_money     = item.displayName !== null ? "("+item.displayName+") "+scorre : scorre;
                                                     }
                                                     else if(carditem.column === '123')
                                                     {
                                                        carditem.original_money     = item.displayName !== null ? "("+item.displayName+") "+scorre : scorre;
                                                     }
                                                     else
                                                     {
                                                        carditem.original_money     =  scorre;
                                                     }

                                                     carditem.risk_stake_slip    = parseFloat(item.risk);
                                                     carditem.risk_win_slip      = parseFloat(item.win);
                                                     carditem.bet_type           = 'live';
                                                     carditem.bet_extra_info = JSON.stringify(item.bet_extra_info);
                                                     $scope.confirmData.push(carditem);
                                                 }
                                             });

                                         });
                                     });
                                     $http.post("/save-live-data",{
                                        "_token": $('meta[name="csrf-token"]').attr('content'),
                                             items :  $scope.confirmData,
                                             status: "oka"
                                         }).then(function(ddd){
                                            let resp = ddd.data;
                                             if (resp.status) {
                                                 $scope.isChanged = false;
                                                 $scope.slipOddTotal = 0;
                                                 $scope.changeNow = false;

                                                 $('#header_available_balance').html(resp.user.available_balance);
                                                 $('#header_pending_amount').html(resp.user.pending_amount);
                                                 $scope.selectedData.forEach((item) => {
                                                     $('#'+item.type+item.id).removeClass('active');
                                                 });

                                                 $scope.confirmData  = [];
                                                 $scope.selectedData = [];

                                                 $scope.totalStake = 0;
                                                 $scope.possibleWin = 0;

                                                  $scope.confirmSuccess = true;
                                                 $("#confirmSuccess").show();
                                                 setTimeout(function () {
                                                     $scope.confirmSuccess = false;
                                                 }, 3000);
                                             } else {
                                                 $scope.confirmFail = true;
                                                 setTimeout(function () {
                                                     $scope.confirmFail = false;
                                                 }, 5000);
                                             }
                                     });
                                 }
                                 else
                                 {
                                     $("#bet_noticeFootball").removeClass('d-none');
                                     $scope.isChanged = false;
                                     $scope.changeNow = false;
                                 }
                             }
                             else
                             {
                                 $("#mesageFootball").text("password didn't match, try again !");
                             }

                     });
                 }
             });



         }
         else
         {
             alert("No Item is selected");
         }
     }

     $scope.passwordCloseIcon =  function(){
         $(".popup-box").addClass("d-none");
     }
 });
// angular.bootstrap(document.getElementById("realTimeBetting3cvc"), ['realTimeBetting3cvc']);

 $(document).ready(function () {
     $(".abul").css('opacity',"1");
     $("#sidebar-content").css('opacity',"1");
 });

console.groupEnd();
