var s_iOffsetX, s_iOffsetY, s_iScaleFactor = 1,
    s_bIsIphone = !1;
(function(a) {
    (jQuery.browser = jQuery.browser || {}).mobile = /android|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(ad|hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|symbian|tablet|treo|up\.(browser|link)|vodafone|wap|webos|windows (ce|phone)|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|e\-|e\/|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(di|rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|xda(\-|2|g)|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))
})(navigator.userAgent ||
    navigator.vendor || window.opera);
$(window).resize(function() {
    sizeHandler()
});

function trace(a) {
    console.log(a)
}

function getSize(a) {
    var d = a.toLowerCase(),
        c = window.document,
        b = c.documentElement;
    if (void 0 === window["inner" + a]) a = b["client" + a];
    else if (window["inner" + a] != b["client" + a]) {
        var f = c.createElement("body");
        f.id = "vpw-test-b";
        f.style.cssText = "overflow:scroll";
        var e = c.createElement("div");
        e.id = "vpw-test-d";
        e.style.cssText = "position:absolute;top:-1000px";
        e.innerHTML = "<style>@media(" + d + ":" + b["client" + a] + "px){body#vpw-test-b div#vpw-test-d{" + d + ":7px!important}}</style>";
        f.appendChild(e);
        b.insertBefore(f, c.head);
        a = 7 == e["offset" + a] ? b["client" + a] : window["inner" + a];
        b.removeChild(f)
    } else a = window["inner" + a];
    return a
}
window.addEventListener("orientationchange", onOrientationChange);

function onOrientationChange() {
    window.matchMedia("(orientation: portrait)").matches && sizeHandler();
    window.matchMedia("(orientation: landscape)").matches && sizeHandler()
}

function isIOS() {
    var a = "iPad Simulator;iPhone Simulator;iPod Simulator;iPad;iPhone;iPod".split(";");
    for (-1 !== navigator.userAgent.toLowerCase().indexOf("iphone") && (s_bIsIphone = !0); a.length;)
        if (navigator.platform === a.pop()) return !0;
    return s_bIsIphone = !1
}

function getIOSWindowHeight() {
    return document.documentElement.clientWidth / window.innerWidth * window.innerHeight
}

function getHeightOfIOSToolbars() {
    var a = (0 === window.orientation ? screen.height : screen.width) - getIOSWindowHeight();
    return 1 < a ? a : 0
}

function sizeHandler() {
    window.scrollTo(0, 1);
    if ($("#canvas")) {
        var a = navigator.userAgent.match(/(iPad|iPhone|iPod)/g) ? getIOSWindowHeight() : getSize("Height");
        var d = getSize("Width");
        _checkOrientation(d, a);
        var c = Math.min(a / CANVAS_HEIGHT, d / CANVAS_WIDTH),
            b = CANVAS_WIDTH * c,
            c = CANVAS_HEIGHT * c;
        if (c < a) {
            var f = a - c;
            c += f;
            b += CANVAS_WIDTH / CANVAS_HEIGHT * f
        } else b < d && (f = d - b, b += f, c += CANVAS_HEIGHT / CANVAS_WIDTH * f);
        f = a / 2 - c / 2;
        var e = d / 2 - b / 2,
            g = CANVAS_WIDTH / b;
        if (e * g < -EDGEBOARD_X || f * g < -EDGEBOARD_Y) c = Math.min(a / (CANVAS_HEIGHT - 2 *
            EDGEBOARD_Y), d / (CANVAS_WIDTH - 2 * EDGEBOARD_X)), b = CANVAS_WIDTH * c, c *= CANVAS_HEIGHT, f = (a - c) / 2, e = (d - b) / 2, g = CANVAS_WIDTH / b;
        s_iOffsetX = -1 * e * g;
        s_iOffsetY = -1 * f * g;
        0 <= f && (s_iOffsetY = 0);
        0 <= e && (s_iOffsetX = 0);
        null !== s_oInterface && s_oInterface.refreshButtonPos(s_iOffsetX, s_iOffsetY);
        null !== s_oMenu && s_oMenu.refreshButtonPos(s_iOffsetX, s_iOffsetY);
        s_bIsIphone ? (canvas = document.getElementById("canvas"), s_oStage.canvas.width = 2 * b, s_oStage.canvas.height = 2 * c, canvas.style.width = b + "px", canvas.style.height = c + "px", a = Math.min(b /
            CANVAS_WIDTH, c / CANVAS_HEIGHT), s_iScaleFactor = 2 * a, s_oStage.scaleX = s_oStage.scaleY = 2 * a) : s_bMobile && !1 === isIOS() ? ($("#canvas").css("width", b + "px"), $("#canvas").css("height", c + "px")) : (s_oStage.canvas.width = b, s_oStage.canvas.height = c, s_iScaleFactor = Math.min(b / CANVAS_WIDTH, c / CANVAS_HEIGHT), s_oStage.scaleX = s_oStage.scaleY = s_iScaleFactor);
        0 > f ? $("#canvas").css("top", f + "px") : $("#canvas").css("top", "0px");
        $("#canvas").css("left", e + "px")
    }
}

function _checkOrientation(a, d) {
    s_bMobile && ENABLE_CHECK_ORIENTATION && (a > d ? "landscape" === $(".orientation-msg-container").attr("data-orientation") ? ($(".orientation-msg-container").css("display", "none"), s_oMain.startUpdate()) : ($(".orientation-msg-container").css("display", "block"), s_oMain.stopUpdate()) : "portrait" === $(".orientation-msg-container").attr("data-orientation") ? ($(".orientation-msg-container").css("display", "none"), s_oMain.startUpdate()) : ($(".orientation-msg-container").css("display", "block"),
        s_oMain.stopUpdate()))
}

function inIframe() {
    try {
        return window.self !== window.top
    } catch (a) {
        return !0
    }
}

function createBitmap(a, d, c) {
    var b = new createjs.Bitmap(a),
        f = new createjs.Shape;
    d && c ? f.graphics.beginFill("#fff").drawRect(0, 0, d, c) : f.graphics.beginFill("#ff0").drawRect(0, 0, a.width, a.height);
    b.hitArea = f;
    return b
}

function createSprite(a, d, c, b, f, e) {
    a = null !== d ? new createjs.Sprite(a, d) : new createjs.Sprite(a);
    d = new createjs.Shape;
    d.graphics.beginFill("#000000").drawRect(-c, -b, f, e);
    a.hitArea = d;
    return a
}

function randomFloatBetween(a, d, c) {
    "undefined" === typeof c && (c = 2);
    return parseFloat(Math.min(a + Math.random() * (d - a), d).toFixed(c))
}

function shuffle(a) {
    for (var d = a.length, c, b; 0 !== d;) b = Math.floor(Math.random() * d), --d, c = a[d], a[d] = a[b], a[b] = c;
    return a
}

function formatTime(a) {
    a /= 1E3;
    var d = Math.floor(a / 60);
    a = parseFloat(a - 60 * d).toFixed(1);
    var c = "",
        c = 10 > d ? c + ("0" + d + ":") : c + (d + ":");
    return 10 > a ? c + ("0" + a) : c + a
}
Array.prototype.sortOn = function() {
    var a = this.slice();
    if (!arguments.length) return a.sort();
    var d = Array.prototype.slice.call(arguments);
    return a.sort(function(a, b) {
        for (var c = d.slice(), e = c.shift(); a[e] == b[e] && c.length;) e = c.shift();
        return a[e] == b[e] ? 0 : a[e] > b[e] ? 1 : -1
    })
};

function roundDecimal(a, d) {
    var c = Math.pow(10, d);
    return Math.round(c * a) / c
}

function tweenVectors(a, d, c, b) {
    b.set(a.getX() + c * (d.getX() - a.getX()), a.getY() + c * (d.getY() - a.getY()));
    return b
}

function NoClickDelay(a) {
    this.element = a;
    window.Touch && this.element.addEventListener("touchstart", this, !1)
}
NoClickDelay.prototype = {
    handleEvent: function(a) {
        switch (a.type) {
            case "touchstart":
                this.onTouchStart(a);
                break;
            case "touchmove":
                this.onTouchMove(a);
                break;
            case "touchend":
                this.onTouchEnd(a)
        }
    },
    onTouchStart: function(a) {
        a.preventDefault();
        this.moved = !1;
        this.element.addEventListener("touchmove", this, !1);
        this.element.addEventListener("touchend", this, !1)
    },
    onTouchMove: function(a) {
        this.moved = !0
    },
    onTouchEnd: function(a) {
        this.element.removeEventListener("touchmove", this, !1);
        this.element.removeEventListener("touchend",
            this, !1);
        if (!this.moved) {
            a = document.elementFromPoint(a.changedTouches[0].clientX, a.changedTouches[0].clientY);
            3 === a.nodeType && (a = a.parentNode);
            var d = document.createEvent("MouseEvents");
            d.initEvent("click", !0, !0);
            a.dispatchEvent(d)
        }
    }
};

function playSound(a, d, c) {
    return !1 === DISABLE_SOUND_MOBILE || !1 === s_bMobile ? createjs.Sound.play(a, {
        loop: c,
        volume: d
    }) : null
}

function stopSound(a) {
    !1 !== DISABLE_SOUND_MOBILE && !1 !== s_bMobile || a.stop()
}

function setVolume(a, d) {
    if (!1 === DISABLE_SOUND_MOBILE || !1 === s_bMobile) a.volume = d
}

function setMute(a, d) {
    !1 !== DISABLE_SOUND_MOBILE && !1 !== s_bMobile || a.setMute(d)
}

function ctlArcadeResume() {
    null !== s_oMain && s_oMain.startUpdate()
}

function ctlArcadePause() {
    null !== s_oMain && s_oMain.stopUpdate()
}

function getParamValue(a) {
    for (var d = window.location.search.substring(1).split("&"), c = 0; c < d.length; c++) {
        var b = d[c].split("=");
        if (b[0] == a) return b[1]
    }
}

function CSpriteLibrary() {
    var a, d, c, b, f, e;
    this.init = function(g, h, k) {
        c = d = 0;
        b = g;
        f = h;
        e = k;
        a = {}
    };
    this.addSprite = function(b, c) {
        a.hasOwnProperty(b) || (a[b] = {
            szPath: c,
            oSprite: new Image
        }, d++)
    };
    this.getSprite = function(b) {
        return a.hasOwnProperty(b) ? a[b].oSprite : null
    };
    this._onSpritesLoaded = function() {
        f.call(e)
    };
    this._onSpriteLoaded = function() {
        b.call(e);
        ++c === d && this._onSpritesLoaded()
    };
    this.loadSprites = function() {
        for (var b in a) a[b].oSprite.oSpriteLibrary = this, a[b].oSprite.onload = function() {
                this.oSpriteLibrary._onSpriteLoaded()
            },
            a[b].oSprite.src = a[b].szPath
    };
    this.getNumSprites = function() {
        return d
    }
}
var CANVAS_WIDTH = 1700,
    CANVAS_HEIGHT = 768,
    EDGEBOARD_X = 250,
    EDGEBOARD_Y = 0,
    FPS_TIME = 1E3 / 24,
    DISABLE_SOUND_MOBILE = !1,
    FONT_GAME_1 = "arialbold",
    FONT_GAME_2 = "Digital-7",
    STATE_LOADING = 0,
    STATE_MENU = 1,
    STATE_HELP = 1,
    STATE_GAME = 3,
    STATE_GAME_WAITING_FOR_BET = 0,
    STATE_GAME_DEALING = 1,
    STATE_GAME_PLAYER_TURN = 2,
    STATE_GAME_SHOWDOWN = 3,
    STATE_GAME_DISTRIBUTE_FICHES = 4,
    STATE_GAME_SHOW_WINNER = 5,
    STATE_CARD_DEALING = 0,
    STATE_CARD_REMOVING = 1,
    ON_MOUSE_DOWN = 0,
    ON_MOUSE_UP = 1,
    ON_MOUSE_OVER = 2,
    ON_MOUSE_OUT = 3,
    ON_DRAG_START = 4,
    ON_DRAG_END = 5,
    ASSIGN_FICHES =
    "ASSIGN_FICHES",
    END_HAND = "END_HAND",
    ON_CARD_SHOWN = "ON_CARD_SHOWN",
    ON_CARD_ANIMATION_ENDING = "ON_CARD_ANIMATION_ENDING",
    ON_CARD_TO_REMOVE = "ON_CARD_TO_REMOVE",
    NUM_FICHES = 6,
    CARD_WIDTH = 66,
    CARD_HEIGHT = 102,
    MIN_BET, MAX_BET, TOTAL_MONEY, FICHE_WIDTH, WIN_OCCURRENCE, BET_OCCURRENCE, TIME_FICHES_MOV = 600,
    TIME_CARD_DEALING = 250,
    TIME_CARD_REMOVE = 1E3,
    TIME_SHOW_FINAL_CARDS = 4E3,
    TIME_END_HAND, BET_TIME = 1E4,
    AD_SHOW_COUNTER, CARD_TO_DEAL = 3,
    PAYOUT_ANTE, PAYOUT_PLUS, STRAIGHT_FLUSH = 0,
    THREE_OF_A_KIND = 1,
    STRAIGHT = 2,
    FLUSH = 3,
    ONE_PAIR =
    4,
    HIGH_CARD = 5,
    NO_HAND = 6,
    CARD_TWO = 2,
    CARD_THREE = 3,
    CARD_FOUR = 4,
    CARD_FIVE = 5,
    CARD_SIX = 6,
    CARD_SEVEN = 7,
    CARD_EIGHT = 8,
    CARD_NINE = 9,
    CARD_TEN = 10,
    CARD_JACK = 11,
    CARD_QUEEN = 12,
    CARD_KING = 13,
    CARD_ACE = 14,
    BET_ANTE = 0,
    BET_PLAY = 1,
    BET_PLUS = 2,
    POS_BET = [],
    MULTIPLIERS = [],
    ENABLE_FULLSCREEN, ENABLE_CHECK_ORIENTATION, SHOW_CREDITS, TEXT_DEAL = "DEAL",
    TEXT_MIN_BET = "MIN BET",
    TEXT_MAX_BET = "MAX BET",
    TEXT_RECHARGE = "RECHARGE",
    TEXT_EXIT = "EXIT",
    TEXT_MONEY = "MONEY",
    TEXT_CURRENCY = "$",
    TEXT_PLAY = "PLAY",
    TEXT_FOLD = "FOLD",
    TEXT_ANTE_BONUS = "ANTE BONUS PAYS",
    TEXT_PAIR_PLUS = "PAIR PLUS PAYS",
    TEXT_ARE_SURE = "ARE YOU SURE?",
    TEXT_CREDITS_DEVELOPED = "DEVELOPED BY",
    TEXT_DISPLAY_MSG_WAITING_BET = "WAITING FOR YOUR BET",
    TEXT_DISPLAY_MSG_PLAYER_LOSE = "PLAYER LOSES THIS HAND!",
    TEXT_DISPLAY_MSG_STANDOFF = "STAND OFF",
    TEXT_DISPLAY_MSG_PLAYER_WIN = "PLAYER WINS",
    TEXT_DISPLAY_MSG_USER_TURN = "PLAYER TURN. PLAY OR FOLD?",
    TEXT_DISPLAY_MSG_SHOWDOWN = "SHOWDOWN!",
    TEXT_DISPLAY_MSG_DEALING = "DEALING...",
    TEXT_DISPLAY_MSG_NOT_QUALIFY = "DEALER DOES NOT QUALIFY",
    TEXT_NO_MONEY = "YOU DON'T HAVE ENOUGH MONEY!!!",
    TEXT_NO_MONEY_FOR_ANTE = "YOU DON'T HAVE ENOUGH MONEY FOR ANTE BET!!!",
    TEXT_NO_MONEY_FOR_PLAY = "YOU DON'T HAVE ENOUGH MONEY FOR PLAY BET EVENTUALLY!!",
    TEXT_HAND_WON_PLAYER = "HAND WON BY THE PLAYER",
    TEXT_HAND_WON_DEALER = "HAND WON BY THE DEALER",
    TEXT_ERROR_MIN_BET = "YOUR BET IS LOWER THAN MINIMUM BET!!",
    TEXT_ERROR_MAX_BET = "YOUR BET IS HIGHER THAN MAXIMUM BET!!",
    TEXT_EVALUATOR = "STRAIGHT FLUSH;THREE OF A KIND;STRAIGHT;FLUSH;ONE PAIR;HIGH CARD;NO HAND".split(";"),
    TEXT_SHARE_IMAGE = "200x200.jpg",
    TEXT_SHARE_TITLE =
    "Congratulations!",
    TEXT_SHARE_MSG1 = "You collected <strong>",
    TEXT_SHARE_MSG2 = " points</strong>!<br><br>Share your score with your friends!",
    TEXT_SHARE_SHARE1 = "My score is ",
    TEXT_SHARE_SHARE2 = " points! Can you do better?";

function CPreloader() {
    var a, d, c, b, f;
    this._init = function() {
        s_oSpriteLibrary.init(this._onImagesLoaded, this._onAllImagesLoaded, this);
        s_oSpriteLibrary.addSprite("bg_preloader", "./sprites/bg_preloader.jpg");
        s_oSpriteLibrary.addSprite("progress_bar", "./sprites/progress_bar.png");
        s_oSpriteLibrary.loadSprites();
        f = new createjs.Container;
        s_oStage.addChild(f)
    };
    this.unload = function() {
        f.removeAllChildren()
    };
    this._onImagesLoaded = function() {};
    this._onAllImagesLoaded = function() {
        this.attachSprites();
        s_oMain.preloaderReady()
    };
    this.attachSprites = function() {
        var e = createBitmap(s_oSpriteLibrary.getSprite("bg_preloader"));
        f.addChild(e);
        c = createBitmap(s_oSpriteLibrary.getSprite("progress_bar"));
        c.x = 599;
        c.y = CANVAS_HEIGHT - 50;
        f.addChild(c);
        a = 476;
        b = new createjs.Shape;
        b.graphics.beginFill("rgba(255,0,0,0.01)").drawRect(599, CANVAS_HEIGHT - 50, 1, 30);
        f.addChild(b);
        c.mask = b;
        d = new createjs.Text("0%", "30px " + FONT_GAME_1, "#fff");
        d.x = 638;
        d.y = CANVAS_HEIGHT - 56;
        d.textAlign = "center";
        d.textBaseline = "middle";
        f.addChild(d)
    };
    this.refreshLoader = function(c) {
        d.text =
            c + "%";
        c = Math.floor(c * a / 100);
        b.graphics.clear();
        b.graphics.beginFill("rgba(255,0,0,0.01)").drawRect(599, CANVAS_HEIGHT - 50, c, 30)
    };
    this._init()
}

function CMain(a) {
    var d, c = 0,
        b = 0,
        f = STATE_LOADING,
        e, g;
    this.initContainer = function() {
        var a = document.getElementById("canvas");
        s_oStage = new createjs.Stage(a);
        createjs.Touch.enable(s_oStage);
        s_bMobile = jQuery.browser.mobile;
        !1 === s_bMobile && s_oStage.enableMouseOver(20);
        s_iPrevTime = (new Date).getTime();
        createjs.Ticker.setFPS(30);
        createjs.Ticker.addEventListener("tick", this._update);
        navigator.userAgent.match(/Windows Phone/i) && (DISABLE_SOUND_MOBILE = !0);
        s_oSpriteLibrary = new CSpriteLibrary;
        e = new CPreloader;
        s_oGameSettings =
            new CGameSettings;
        d = !0
    };
    this.preloaderReady = function() {
        this._loadImages();
        !1 !== DISABLE_SOUND_MOBILE && !1 !== s_bMobile || this._initSounds()
    };
    this.soundLoaded = function() {
        c++;
        e.refreshLoader(Math.floor(c / b * 100));
        c === b && (e.unload(), this.gotoMenu())
    };
    this._initSounds = function() {
        createjs.Sound.initializeDefaultPlugins() && (0 < navigator.userAgent.indexOf("Opera") || 0 < navigator.userAgent.indexOf("OPR") ? (createjs.Sound.alternateExtensions = ["mp3"], createjs.Sound.addEventListener("fileload", createjs.proxy(this.soundLoaded,
            this)), createjs.Sound.registerSound("./sounds/card.ogg", "card"), createjs.Sound.registerSound("./sounds/chip.ogg", "chip"), createjs.Sound.registerSound("./sounds/fiche_collect.ogg", "fiche_collect"), createjs.Sound.registerSound("./sounds/press_but.ogg", "press_but"), createjs.Sound.registerSound("./sounds/win.ogg", "win"), createjs.Sound.registerSound("./sounds/lose.ogg", "lose")) : (createjs.Sound.alternateExtensions = ["ogg"], createjs.Sound.addEventListener("fileload", createjs.proxy(this.soundLoaded, this)),
            createjs.Sound.registerSound("./sounds/card.mp3", "card", 4), createjs.Sound.registerSound("./sounds/chip.mp3", "chip", 4), createjs.Sound.registerSound("./sounds/fiche_collect.mp3", "fiche_collect"), createjs.Sound.registerSound("./sounds/press_but.mp3", "press_but"), createjs.Sound.registerSound("./sounds/win.mp3", "win"), createjs.Sound.registerSound("./sounds/lose.mp3", "lose")), b += 6)
    };
    this._loadImages = function() {
        s_oSpriteLibrary.init(this._onImagesLoaded, this._onAllImagesLoaded, this);
        s_oSpriteLibrary.addSprite("but_menu_bg",
            "./sprites/but_menu_bg.png");
        s_oSpriteLibrary.addSprite("but_game_bg", "./sprites/but_game_bg.png");
        s_oSpriteLibrary.addSprite("but_exit", "./sprites/but_exit.png");
        s_oSpriteLibrary.addSprite("bg_menu", "./sprites/bg_menu.jpg");
        s_oSpriteLibrary.addSprite("audio_icon", "./sprites/audio_icon.png");
        s_oSpriteLibrary.addSprite("bg_game", "./sprites/bg_game.jpg");
        s_oSpriteLibrary.addSprite("card_spritesheet", "./sprites/card_spritesheet.png");
        s_oSpriteLibrary.addSprite("msg_box", "./sprites/msg_box.png");
        s_oSpriteLibrary.addSprite("display_bg",
            "./sprites/display_bg.png");
        s_oSpriteLibrary.addSprite("fiche_highlight", "./sprites/fiche_highlight.png");
        s_oSpriteLibrary.addSprite("win_bg", "./sprites/win_bg.png");
        s_oSpriteLibrary.addSprite("but_clear", "./sprites/but_clear.png");
        s_oSpriteLibrary.addSprite("but_generic", "./sprites/but_generic.png");
        s_oSpriteLibrary.addSprite("but_rebet", "./sprites/but_rebet.png");
        s_oSpriteLibrary.addSprite("gui_bg", "./sprites/gui_bg.png");
        s_oSpriteLibrary.addSprite("bet_ante", "./sprites/bet_ante.png");
        s_oSpriteLibrary.addSprite("bet_play",
            "./sprites/bet_play.png");
        s_oSpriteLibrary.addSprite("bet_pair_plus", "./sprites/bet_pair_plus.png");
        s_oSpriteLibrary.addSprite("paytable_ante_bg", "./sprites/paytable_ante_bg.png");
        s_oSpriteLibrary.addSprite("paytable_pair_plus_bg", "./sprites/paytable_pair_plus_bg.png");
        s_oSpriteLibrary.addSprite("help_cursor", "./sprites/help_cursor.png");
        s_oSpriteLibrary.addSprite("but_fullscreen", "./sprites/but_fullscreen.png");
        s_oSpriteLibrary.addSprite("but_credits", "./sprites/but_credits.png");
        s_oSpriteLibrary.addSprite("logo_ctl",
            "./sprites/logo_ctl.png");
        s_oSpriteLibrary.addSprite("but_no", "./sprites/but_no.png");
        s_oSpriteLibrary.addSprite("but_yes", "./sprites/but_yes.png");
        for (var a = 0; a < NUM_FICHES; a++) s_oSpriteLibrary.addSprite("fiche_" + a, "./sprites/fiche_" + a + ".png");
        b += s_oSpriteLibrary.getNumSprites();
        s_oSpriteLibrary.loadSprites()
    };
    this._onImagesLoaded = function() {
        c++;
        e.refreshLoader(Math.floor(c / b * 100));
        c === b && (e.unload(), this.gotoMenu())
    };
    this._onAllImagesLoaded = function() {};
    this.onAllPreloaderImagesLoaded = function() {
        this._loadImages()
    };
    this.gotoMenu = function() {
        new CMenu;
        f = STATE_MENU
    };
    this.gotoGame = function() {
        g = new CGame(h);
        f = STATE_GAME
    };
    this.gotoHelp = function() {
        new CHelp;
        f = STATE_HELP
    };
    this.stopUpdate = function() {
        d = !1;
        createjs.Ticker.paused = !0;
        $("#block_game").css("display", "block");
        createjs.Sound.setMute(!0)
    };
    this.startUpdate = function() {
        s_iPrevTime = (new Date).getTime();
        d = !0;
        createjs.Ticker.paused = !1;
        $("#block_game").css("display", "none");
        s_bAudioActive && createjs.Sound.setMute(!1)
    };
    this._update = function(a) {
        if (d) {
            var b = (new Date).getTime();
            s_iTimeElaps = b - s_iPrevTime;
            s_iCntTime += s_iTimeElaps;
            s_iCntFps++;
            s_iPrevTime = b;
            1E3 <= s_iCntTime && (s_iCurFps = s_iCntFps, s_iCntTime -= 1E3, s_iCntFps = 0);
            f === STATE_GAME && g.update();
            s_oStage.update(a)
        }
    };
    s_oMain = this;
    var h = a;
    ENABLE_CHECK_ORIENTATION = h.check_orientation;
    ENABLE_FULLSCREEN = h.fullscreen;
    SHOW_CREDITS = a.show_credits;
    this.initContainer()
}
var s_bMobile, s_bAudioActive = !0,
    s_iCntTime = 0,
    s_iTimeElaps = 0,
    s_iPrevTime = 0,
    s_iCntFps = 0,
    s_iCurFps = 0,
    s_bFullscreen = !1,
    s_oDrawLayer, s_oStage, s_oMain, s_oSpriteLibrary, s_oGameSettings;

function CTextButton(a, d, c, b, f, e, g, h) {
    var k, n, m, l, w, v, p, t;
    this._init = function(a, b, c, d, f, e, h, g) {
        k = !1;
        n = [];
        m = [];
        t = g;
        w = createBitmap(c);
        g = Math.ceil(h / 20);
        v = new createjs.Text(d, h + "px " + f, "#000000");
        var D = v.getBounds();
        v.textAlign = "center";
        v.textBaseline = "alphabetic";
        v.x = c.width / 2 + g;
        v.y = Math.floor(c.height / 2) + D.height / 3 + g;
        p = new createjs.Text(d, h + "px " + f, e);
        p.textAlign = "center";
        p.textBaseline = "alphabetic";
        p.x = c.width / 2;
        p.y = Math.floor(c.height / 2) + D.height / 3;
        l = new createjs.Container;
        l.x = a;
        l.y = b;
        l.regX = c.width /
            2;
        l.regY = c.height / 2;
        l.cursor = "pointer";
        l.addChild(w, p);
        t.addChild(l);
        this._initListener()
    };
    this.unload = function() {
        l.off("mousedown");
        l.off("pressup");
        t.removeChild(l)
    };
    this.setVisible = function(a) {
        l.visible = a
    };
    this.enable = function() {
        k = !1;
        p.color = "#fff"
    };
    this.disable = function() {
        k = !0;
        p.color = "#a39b9d"
    };
    this._initListener = function() {
        oParent = this;
        l.on("mousedown", this.buttonDown);
        l.on("pressup", this.buttonRelease)
    };
    this.addEventListener = function(a, b, c) {
        n[a] = b;
        m[a] = c
    };
    this.buttonRelease = function() {
        k || (!1 !==
            DISABLE_SOUND_MOBILE && !1 !== s_bMobile || playSound("press_but", 1, 0), l.scaleX = 1, l.scaleY = 1, n[ON_MOUSE_UP] && n[ON_MOUSE_UP].call(m[ON_MOUSE_UP]))
    };
    this.buttonDown = function() {
        k || (l.scaleX = .9, l.scaleY = .9, n[ON_MOUSE_DOWN] && n[ON_MOUSE_DOWN].call(m[ON_MOUSE_DOWN]))
    };
    this.setPosition = function(a, b) {
        l.x = a;
        l.y = b
    };
    this.changeText = function(a) {
        p.text = a;
        v.text = a
    };
    this.setX = function(a) {
        l.x = a
    };
    this.setY = function(a) {
        l.y = a
    };
    this.getButtonImage = function() {
        return l
    };
    this.getX = function() {
        return l.x
    };
    this.getY = function() {
        return l.y
    };
    this._init(a, d, c, b, f, e, g, h);
    return this
}

function CGfxButton(a, d, c, b) {
    var f, e, g, h, k, n = [],
        m;
    this._init = function(a, b, c) {
        f = !1;
        h = [];
        k = [];
        e = c.width;
        g = c.height;
        m = createBitmap(c);
        m.x = a;
        m.y = b;
        m.regX = c.width / 2;
        m.regY = c.height / 2;
        m.cursor = "pointer";
        l.addChild(m);
        this._initListener()
    };
    this.unload = function() {
        m.off("mousedown", this.buttonDown);
        m.off("pressup", this.buttonRelease);
        l.removeChild(m)
    };
    this.setVisible = function(a) {
        m.visible = a
    };
    this._initListener = function() {
        m.on("mousedown", this.buttonDown);
        m.on("pressup", this.buttonRelease)
    };
    this.addEventListener =
        function(a, b, c) {
            h[a] = b;
            k[a] = c
        };
    this.addEventListenerWithParams = function(a, b, c, d) {
        h[a] = b;
        k[a] = c;
        n = d
    };
    this.buttonRelease = function() {
        f || (!1 !== DISABLE_SOUND_MOBILE && !1 !== s_bMobile || playSound("press_but", 1, 0), h[ON_MOUSE_UP] && h[ON_MOUSE_UP].call(k[ON_MOUSE_UP], n))
    };
    this.buttonDown = function() {
        f || h[ON_MOUSE_DOWN] && h[ON_MOUSE_DOWN].call(k[ON_MOUSE_DOWN], n)
    };
    this.setPosition = function(a, b) {
        m.x = a;
        m.y = b
    };
    this.setX = function(a) {
        m.x = a
    };
    this.setY = function(a) {
        m.y = a
    };
    this.enable = function() {
        f = !1;
        m.filters = [];
        m.cache(0,
            0, e, g)
    };
    this.disable = function() {
        f = !0;
        var a = (new createjs.ColorMatrix).adjustSaturation(-100);
        m.filters = [new createjs.ColorMatrixFilter(a)];
        m.cache(0, 0, e, g)
    };
    this.getButtonImage = function() {
        return m
    };
    this.getX = function() {
        return m.x
    };
    this.getY = function() {
        return m.y
    };
    var l = b;
    this._init(a, d, c);
    return this
}

function CToggle(a, d, c, b, f) {
    var e, g, h, k, n, m, l;
    this._init = function(a, b, c, d, f) {
        l = void 0 !== f ? f : s_oStage;
        g = [];
        h = [];
        f = new createjs.SpriteSheet({
            images: [c],
            frames: {
                width: c.width / 2,
                height: c.height,
                regX: c.width / 2 / 2,
                regY: c.height / 2
            },
            animations: {
                state_true: [0],
                state_false: [1]
            }
        });
        e = d;
        k = createSprite(f, "state_" + e, c.width / 2 / 2, c.height / 2, c.width / 2, c.height);
        k.x = a;
        k.y = b;
        k.stop();
        s_bMobile || (k.cursor = "pointer");
        l.addChild(k);
        this._initListener()
    };
    this.unload = function() {
        k.off("mousedown", n);
        k.off("pressup", m);
        l.removeChild(k)
    };
    this._initListener = function() {
        n = k.on("mousedown", this.buttonDown);
        m = k.on("pressup", this.buttonRelease)
    };
    this.addEventListener = function(a, c, b) {
        g[a] = c;
        h[a] = b
    };
    this.setCursorType = function(a) {
        k.cursor = a
    };
    this.setActive = function(a) {
        e = a;
        k.gotoAndStop("state_" + e)
    };
    this.buttonRelease = function() {
        k.scaleX = 1;
        k.scaleY = 1;
        playSound("press_but", 1, 0);
        e = !e;
        k.gotoAndStop("state_" + e);
        g[ON_MOUSE_UP] && g[ON_MOUSE_UP].call(h[ON_MOUSE_UP], e)
    };
    this.buttonDown = function() {
        k.scaleX = .9;
        k.scaleY = .9;
        g[ON_MOUSE_DOWN] && g[ON_MOUSE_DOWN].call(h[ON_MOUSE_DOWN])
    };
    this.setPosition = function(a, c) {
        k.x = a;
        k.y = c
    };
    this._init(a, d, c, b, f)
}

function CMenu() {
    var a, d, c, b, f, e, g, h, k, n, m, l = null,
        w = null,
        v;
    this._init = function() {
        g = createBitmap(s_oSpriteLibrary.getSprite("bg_menu"));
        s_oStage.addChild(g);
        var p = s_oSpriteLibrary.getSprite("but_menu_bg");
        h = new CGfxButton(CANVAS_WIDTH / 2, CANVAS_HEIGHT - 164, p, s_oStage);
        h.addEventListener(ON_MOUSE_UP, this._onButPlayRelease, this);
        if (!1 === DISABLE_SOUND_MOBILE || !1 === s_bMobile) p = s_oSpriteLibrary.getSprite("audio_icon"), f = CANVAS_WIDTH - p.width / 4 - 10, e = p.height / 2 + 10, k = new CToggle(f, e, p, s_bAudioActive, s_oStage),
            k.addEventListener(ON_MOUSE_UP, this._onAudioToggle, this);
        p = s_oSpriteLibrary.getSprite("but_credits");
        SHOW_CREDITS ? (a = 10 + p.width / 2, d = p.height / 2 + 10, n = new CGfxButton(a, d, p, s_oStage), n.addEventListener(ON_MOUSE_UP, this._onCredits, this), c = a + p.width + 10, b = d) : (c = 10 + p.width / 2, b = p.height / 2 + 10);
        p = window.document;
        var t = p.documentElement;
        l = t.requestFullscreen || t.mozRequestFullScreen || t.webkitRequestFullScreen || t.msRequestFullscreen;
        w = p.exitFullscreen || p.mozCancelFullScreen || p.webkitExitFullscreen || p.msExitFullscreen;
        !1 === ENABLE_FULLSCREEN && (l = !1);
        l && !1 === inIframe() && (p = s_oSpriteLibrary.getSprite("but_fullscreen"), m = new CToggle(c, b, p, s_bFullscreen, s_oStage), m.addEventListener(ON_MOUSE_UP, this._onFullscreenRelease, this));
        v = new createjs.Shape;
        v.graphics.beginFill("black").drawRect(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT);
        s_oStage.addChild(v);
        createjs.Tween.get(v).to({
            alpha: 0
        }, 400).call(function() {
            v.visible = !1
        });
        this.refreshButtonPos(s_iOffsetX, s_iOffsetY)
    };
    this.refreshButtonPos = function(h, g) {
        !1 !== DISABLE_SOUND_MOBILE && !1 !==
            s_bMobile || k.setPosition(f - h, g + e);
        l && !1 === inIframe() && m.setPosition(c + h, b + g);
        SHOW_CREDITS && n.setPosition(a + h, d + g)
    };
    this.unload = function() {
        h.unload();
        h = null;
        if (!1 === DISABLE_SOUND_MOBILE || !1 === s_bMobile) k.unload(), k = null;
        SHOW_CREDITS && n.unload();
        l && !1 === inIframe() && m.unload();
        s_oStage.removeAllChildren();
        s_oMenu = null
    };
    this._onButPlayRelease = function() {
        this.unload();
        s_oMain.gotoGame();
        $(s_oMain).trigger("start_session")
    };
    this._onAudioToggle = function() {
        createjs.Sound.setMute(s_bAudioActive);
        s_bAudioActive = !s_bAudioActive
    };
    this._onCredits = function() {
        _oCreditsPanel = new CCreditsPanel
    };
    this._onFullscreenRelease = function() {
        s_bFullscreen ? (w.call(window.document), s_bFullscreen = !1) : (l.call(window.document.documentElement), s_bFullscreen = !0);
        sizeHandler()
    };
    s_oMenu = this;
    this._init()
}
var s_oMenu = null;

function CGame(a) {
    var d = !1,
        c, b, f, e, g, h, k, n, m, l, w, v, p, t, D, z, I, A, G, F, E, C, M, L, J, B, H, x, r, N, y, Q, u, q, O, P;
    this._init = function() {
        e = MAX_BET;
        g = -1;
        h = p = f = 0;
        s_oTweenController = new CTweenController;
        Q = createBitmap(s_oSpriteLibrary.getSprite("bg_game"));
        s_oStage.addChild(Q);
        u = new CInterface(TOTAL_MONEY);
        r = new createjs.Container;
        s_oStage.addChild(r);
        N = new CHandEvaluator;
        q = new CSeat;
        q.setCredit(TOTAL_MONEY);
        y = new CHelpCursor(636, 436, s_oSpriteLibrary.getSprite("help_cursor"), s_oStage);
        this.reset(!1);
        L = new CVector2;
        L.set(1214, 228);
        J = new CVector2;
        J.set(CANVAS_WIDTH / 2 - 119, 230);
        B = new CVector2;
        B.set(418, 820);
        H = new CVector2;
        H.set(0, -CANVAS_HEIGHT);
        x = new CVector2(454, 230);
        F = [q.getCardOffset(), J];
        P = new CGameOver;
        q.getCredit() < s_oGameSettings.getFichesValueAt(0) ? (this._gameOver(), this.changeState(-1)) : d = !0;
        M = new CVector2(L.getX(), L.getY());
        O = new CMsgBox;
        this.changeState(STATE_GAME_WAITING_FOR_BET)
    };
    this.unload = function() {
        d = !1;
        !1 !== DISABLE_SOUND_MOBILE && !1 !== s_bMobile || createjs.Sound.stop();
        for (var a = 0; a < z.length; a++) z[a].unload();
        u.unload();
        P.unload();
        O.unload();
        s_oStage.removeAllChildren()
    };
    this.reset = function(a) {
        k = h = f = 0;
        b = !1;
        q.reset();
        z = [];
        z.splice(0);
        I = [];
        G = [];
        u.reset();
        u.enableBetFiches(a);
        this.shuffleCard()
    };
    this.shuffleCard = function() {
        A = [];
        A = s_oGameSettings.getShuffledCardDeck()
    };
    this.changeState = function(a) {
        g = a;
        switch (a) {
            case STATE_GAME_WAITING_FOR_BET:
                u.displayMsg(TEXT_DISPLAY_MSG_WAITING_BET, TEXT_MIN_BET + ": " + MIN_BET + "\n" + TEXT_MAX_BET + ": " + MAX_BET);
                break;
            case STATE_GAME_DEALING:
                u.disableButtons(), u.displayMsg(TEXT_DISPLAY_MSG_DEALING),
                    this._dealing()
        }
    };
    this.cardFromDealerArrived = function(a, c, b) {
        !1 === c && a.showCard();
        b < 2 * CARD_TO_DEAL && s_oGame._dealing()
    };
    this._calculatePairPlus = function() {};
    this._showWin = function() {
		
		
		
        c ? this._playerLose() : "player" === t && v <= STRAIGHT ? this._playerWin(TEXT_HAND_WON_PLAYER) : w === NO_HAND ? this._playerWin(TEXT_DISPLAY_MSG_NOT_QUALIFY) : "player" === t ? this._playerWin(TEXT_HAND_WON_PLAYER) : "dealer" === t ? this._playerLose() : this._standOff();
        "player" === t ? playSound("win", 1, 0) : playSound("lose", 1, 0);
        this.changeState(STATE_GAME_DISTRIBUTE_FICHES);
        u.refreshCredit(q.getCredit());
        setTimeout(function() {
            q.resetBet();
            s_oGame.changeState(STATE_GAME_WAITING_FOR_BET);
            u.enableBetFiches(!0)
        }, 3 * TIME_CARD_REMOVE);
  
		 var sfs = q.getCredit();
	
		 document.getElementById("win_bid").value =  sfs;
		 makebeteasy();

  };
    this._playerWin = function(a) {
		
		
        q.increaseCredit(n);
        K -= n;
        u.displayMsg(TEXT_DISPLAY_MSG_SHOWDOWN, TEXT_DISPLAY_MSG_PLAYER_WIN + " " + (n + m) + TEXT_CURRENCY);
        q.initMovement(BET_ANTE, B.getX(), B.getY());
        q.initMovement(BET_PLAY, B.getX(), B.getY());
        this._checkPlusWin();
		 var sfs = q.getCredit();
		 
		document.getElementById("win_bid").value =  sfs;
		 makebeteasy();
		
        u.showResultText(a)
    };
    this._playerLose = function(a) {
		
		
	
        u.displayMsg(TEXT_DISPLAY_MSG_SHOWDOWN, TEXT_DISPLAY_MSG_PLAYER_LOSE);
        q.initMovement(BET_ANTE, H.getX(), H.getY());
        a || q.initMovement(BET_PLAY, H.getX(), H.getY());
        this._checkPlusWin();
		
		 var sfs = q.getCredit();

		document.getElementById("win_bid").value =  sfs;
		 makebeteasy();
        u.showResultText(TEXT_HAND_WON_DEALER)
    };
    this._standOff = function() {
        q.increaseCredit(n);
        K -= n;
        u.displayMsg(TEXT_DISPLAY_MSG_SHOWDOWN, TEXT_DISPLAY_MSG_STANDOFF);
        q.initMovement(BET_ANTE, B.getX(), B.getY());
        q.initMovement(BET_PLAY, B.getX(), B.getY());
        this._checkPlusWin();
        u.showResultText(TEXT_DISPLAY_MSG_STANDOFF)
    };
    this._checkPlusWin = function() {
        b && (0 < m ? (q.increaseCredit(m), K -= m, q.initMovement(BET_PLUS,
            B.getX(), B.getY())) : q.initMovement(BET_PLUS, H.getX(), H.getY()))
    };
    this._dealing = function() {
        if (k < 2 * CARD_TO_DEAL) {
            var a = new CCard(L.getX(), L.getY(), r);
            if (1 === k % F.length) {
                var c = new CVector2(J.getX() + (CARD_WIDTH / 2 + 7) * k, J.getY());
                var b = C.splice(0, 1);
                var d = b[0].fotogram;
                b = b[0].rank;
                a.setInfo(M, c, d, b, !0, k);
                a.addEventListener(ON_CARD_SHOWN, this._onCardShown);
                I.push(a)
            } else b = E.splice(0, 1), d = b[0].fotogram, b = b[0].rank, a.setInfo(M, q.getAttachCardOffset(), d, b, !1, k), q.newCardDealed(), G.push(a);
            z.push(a);
            k++;
            a.addEventListener(ON_CARD_ANIMATION_ENDING,
                this.cardFromDealerArrived);
            playSound("card", 1, 0)
        } else setTimeout(function() {
            s_oGame.changeState(STATE_GAME_PLAYER_TURN);
            u.displayMsg(TEXT_DISPLAY_MSG_USER_TURN);
            u.enable(!1, !0, !0)
        }, 1E3)
    };
    this._onEndHand = function() {
        for (var a = new CVector2(x.getX(), x.getY()), b = 0; b < z.length; b++) z[b].initRemoving(a), z[b].hideCard();
        u.clearCardValueText();
        f = 0;
        s_oGame.changeState(STATE_GAME_SHOW_WINNER);
        playSound("fiche_collect", 1, 0);
        p++;
        p === AD_SHOW_COUNTER && (p = 0, $(s_oMain).trigger("show_interlevel_ad"));
        $(s_oMain).trigger("save_score", [q.getCredit()])
    };
    this._onCardShown = function() {
        g === STATE_GAME_PLAYER_TURN && (l === CARD_TO_DEAL ? (u.showHandValue(w, v), g = STATE_GAME_SHOWDOWN, s_oGame._showWin()) : s_oGame._showNextDealerCard())
    };
    this.setBet = function(a, b) {
        if (u.isResultPanelvisible()) u.disableBetFiches(), q.clearBet(), D = this.setBet, this._onEndHand();
        else {
            var c = s_oGameSettings.getFichesValues()[a];
            if (b === BET_ANTE) {
                f = 0;
                y.hide();
                var d = q.getBetAnte() + c;
                if (d > e) {
                    O.show(TEXT_ERROR_MAX_BET);
                    return
                }
                if (d > q.getCredit()) {
                    u.displayMsg(TEXT_NO_MONEY_FOR_PLAY);
                    return
                }
            } else d = q.getBetAnte();
            $(s_oMain).trigger("bet_placed", d);
            b === BET_ANTE ? (q.decreaseCredit(c), K += c, q.betAnte(c), u.enable(!0, !1, !1)) : (q.decreaseCredit(d), K += d, q.betPlay());
            u.refreshCredit(q.getCredit())
        }
    };
    this.setPairPlusBet = function(a) {
        b = !0;
        if (u.isResultPanelvisible()) u.disableBetFiches(), q.clearBet(), D = this.setPairPlusBet, this._onEndHand();
        else {
            a = s_oGameSettings.getFichesValues()[a];
            var c = q.getBetPlus() + a,
                c = parseFloat(c.toFixed(2));
            0 === q.getBetAnte() && q.getCredit() - a <= 3 * s_oGameSettings.getFichesValueAt(0) ?
                u.displayMsg(TEXT_NO_MONEY_FOR_ANTE) : 0 >= q.getCredit() ? u.displayMsg(TEXT_NO_MONEY) : (q.decreaseCredit(a), K += c, q.betPairPlus(a), u.refreshCredit(q.getCredit()))
        }
    };
    this._gameOver = function() {
        P.show()
    };
    this._calculateTotalWin = function() {
        v <= STRAIGHT ? (n = 2 * q.getBetAnte() + 2 * q.getBetAnte(), n += PAYOUT_ANTE[v] * q.getBetAnte()) : n = w === NO_HAND ? 2 * q.getBetAnte() + q.getBetAnte() : "player" === t ? 2 * q.getBetAnte() + 2 * q.getBetAnte() : "dealer" === t ? 0 : q.getBetAnte() + q.getBetAnte();
        n = parseFloat(n.toFixed(2));
        m = 0;
        0 < q.getBetPlus() && v <
            HIGH_CARD && (m = q.getBetPlus() * PAYOUT_PLUS[v], m = parseFloat(m.toFixed(2)))
    };
    this.onRebet = function() {
        u.isResultPanelvisible() && (D = this.rebet, this._onEndHand())
    };
    this.onDeal = function() {
        var a = q.getBetAnte() + q.getBetAnte();
        if (q.getBetAnte() < MIN_BET) O.show(TEXT_ERROR_MIN_BET), u.enableBetFiches(!1), u.enable(!1, !1, !1);
        else {
            r.removeAllChildren();
            if ((K < a ? WIN_OCCURRENCE + 1 : Math.floor(101 * Math.random())) > WIN_OCCURRENCE) {
                do {
                    E = this._generateRandPlayerCards();
                    C = this._generateRandDealerCards();
                    var b = N.evaluate(C);
                    a =
                        N.evaluate(E);
                    w = b.ret;
                    v = a.ret;
                    t = N.getWinnerComparingHands(a.sort_hand, b.sort_hand, v, w);
                    this._calculateTotalWin()
                } while (w === NO_HAND || "player" === t || "dealer_no_hand" === t)
            } else {
                do E = this._generateRandPlayerCards(), C = this._generateRandDealerCards(), b = N.evaluate(C), a = N.evaluate(E), w = b.ret, v = a.ret, t = N.getWinnerComparingHands(a.sort_hand, b.sort_hand, v, w), this._calculateTotalWin(); while ("dealer" === t || n + m > K)
            }
            q.setPrevBet();
            !1 !== DISABLE_SOUND_MOBILE && !1 !== s_bMobile || createjs.Sound.play("card");
            c = !1;
            this.changeState(STATE_GAME_DEALING)
        }
    };
    this.onFold = function() {
        c = !0;
        t = "dealer";
        l = 0;
        this._showNextDealerCard()
    };
    this.onPlay = function() {
        g !== STATE_GAME_DISTRIBUTE_FICHES && (this.setBet(u.getFicheSelected(), BET_PLAY), l = 0, this._showNextDealerCard())
    };
    this._showNextDealerCard = function() {
        I[l].showCard();
        l++
    };
    this._generateRandDealerCards = function() {
        for (var a = [], b = 0; b < CARD_TO_DEAL; b++) a.push({
            fotogram: A[h].fotogram,
            rank: A[h].rank,
            suit: A[h].suit
        }), h++, this._checkDeckLength();
        return a
    };
    this._generateRandPlayerCards = function() {
        for (var a = [], b = 0; b < CARD_TO_DEAL; b++) a.push({
            fotogram: A[h].fotogram,
            rank: A[h].rank,
            suit: A[h].suit
        }), h++, this._checkDeckLength();
        return a
    };
    this._checkDeckLength = function() {
        h >= A.length && (A = s_oGameSettings.getShuffledCardDeck(), h = 0)
    };
    this.clearBets = function() {
        if (g === STATE_GAME_WAITING_FOR_BET) {
            u.enable(!1, !1, !1);
            var a = q.getStartingBet();
            0 < a && (b = !1, q.clearBet(), q.increaseCredit(a), K -= a, u.refreshCredit(q.getCredit()), a = q.checkIfRebetIsPossible(), u.enableBetFiches(a))
        }
    };
    this.rebet = function() {
        this.clearBets();
        var a = q.rebet();
        K -= a;
        u.enable(!0, !1, !1);
        u.refreshCredit(q.getCredit());
        f = BET_TIME
    };
    this.onExit = function() {
        s_oGame.unload();
        $(s_oMain).trigger("save_score", [q.getCredit()]);
        $(s_oMain).trigger("end_session");
        $(s_oMain).trigger("share_event", q.getCredit());
        s_oMain.gotoMenu()
    };
    this.getState = function() {
        return g
    };
    this._updateDealing = function() {
        for (var a = 0; a < z.length; a++) z[a].update()
    };
    this._updateFiches = function() {
        q.updateFichesController()
    };
    this._updateShowWinner = function() {
        for (var a = 0; a < z.length; a++) z[a].update();
        f += s_iTimeElaps;
        f > TIME_END_HAND && (f = 0, a = q.checkIfRebetIsPossible(),
            this.reset(a), u.reset(), q.getCredit() < s_oGameSettings.getFichesValueAt(0) ? (this._gameOver(), this.changeState(-1)) : q.getCredit() < s_oGameSettings.getFichesValueAt(0) ? (this._gameOver(), this.changeState(-1)) : (this.changeState(STATE_GAME_WAITING_FOR_BET), D.call(this, u.getFicheSelected(), 0)))
    };
    this.update = function() {
        if (!1 !== d) switch (g) {
            case STATE_GAME_WAITING_FOR_BET:
                f += s_iTimeElaps;
                6E3 < f && (f = 0, y.isVisible() || 0 !== q.getBetAnte() || y.show(1));
                break;
            case STATE_GAME_DEALING:
                this._updateDealing();
                break;
            case STATE_GAME_DISTRIBUTE_FICHES:
                this._updateFiches();
                break;
            case STATE_GAME_SHOW_WINNER:
                this._updateShowWinner()
        }
    };
    s_oGame = this;
    TOTAL_MONEY = a.money;
    MIN_BET = a.min_bet;
    MAX_BET = a.max_bet;
    MULTIPLIERS = a.multiplier;
    BET_TIME = a.bet_time;
    BLACKJACK_PAYOUT = a.blackjack_payout;
    WIN_OCCURRENCE = a.win_occurrence;
    BET_OCCURRENCE = a.bet_occurrence;
    var K = a.game_cash;
    PAYOUT_ANTE = a.ante_payout;
    PAYOUT_PLUS = a.plus_payouts;
    TIME_END_HAND = a.time_show_hand;
    AD_SHOW_COUNTER = a.ad_show_counter;
    this._init()
}
var s_oGame, s_oTweenController;

function CInterface(a) {
    var d, c, b, f, e, g, h, k, n, m, l, w, v, p, t, D, z = null,
        I, A, G, F, E, C, M, L, J, B = null,
        H = null;
    this._init = function(a) {
        var r = s_oSpriteLibrary.getSprite("but_exit");
        b = CANVAS_WIDTH - r.width / 2 - 10;
        f = r.height / 2 + 10;
        n = new CGfxButton(b, f, r, s_oStage);
        n.addEventListener(ON_MOUSE_UP, this._onExit, this);
        if (!1 === DISABLE_SOUND_MOBILE || !1 === s_bMobile) e = n.getX() - r.width - 10, g = r.height / 2 + 10, z = new CToggle(e, g, s_oSpriteLibrary.getSprite("audio_icon"), s_bAudioActive, s_oStage), z.addEventListener(ON_MOUSE_UP, this._onAudioToggle,
            this);
        var r = window.document,
            x = r.documentElement;
        B = x.requestFullscreen || x.mozRequestFullScreen || x.webkitRequestFullScreen || x.msRequestFullscreen;
        H = r.exitFullscreen || r.mozCancelFullScreen || r.webkitExitFullscreen || r.msExitFullscreen;
        !1 === ENABLE_FULLSCREEN && (B = !1);
        B && !1 === inIframe() && (r = s_oSpriteLibrary.getSprite("but_fullscreen"), d = null === z ? n.getX() - r.width / 2 - 10 : e - r.width / 2 - 10, c = r.height / 2 + 10, J = new CToggle(d, c, r, s_bFullscreen, s_oStage), J.addEventListener(ON_MOUSE_UP, this._onFullscreenRelease, this));
        r = createBitmap(s_oSpriteLibrary.getSprite("display_bg"));
        r.x = 290;
        r.y = 6;
        s_oStage.addChild(r);
        r = s_oSpriteLibrary.getSprite("gui_bg");
        x = createBitmap(r);
        x.y = CANVAS_HEIGHT - r.height;
        s_oStage.addChild(x);
        r = s_oSpriteLibrary.getSprite("but_clear");
        m = new CGfxButton(830, CANVAS_HEIGHT - r.height / 2, r, s_oStage);
        m.addEventListener(ON_MOUSE_UP, this._onButClearRelease, this);
        r = s_oSpriteLibrary.getSprite("but_rebet");
        l = new CGfxButton(890, CANVAS_HEIGHT - r.height / 2, r, s_oStage);
        l.disable();
        l.addEventListener(ON_MOUSE_UP,
            this._onButRebetRelease, this);
        r = s_oSpriteLibrary.getSprite("but_generic");
        p = new CTextButton(1012, CANVAS_HEIGHT - r.height / 2, r, TEXT_DEAL, FONT_GAME_1, "#ffffff", 30, s_oStage);
        p.addEventListener(ON_MOUSE_UP, this._onButDealRelease, this);
        r = s_oSpriteLibrary.getSprite("but_generic");
        t = new CTextButton(1196, CANVAS_HEIGHT - r.height / 2, r, TEXT_PLAY, FONT_GAME_1, "#ffffff", 30, s_oStage);
        t.addEventListener(ON_MOUSE_UP, this._onButPlayRelease, this);
        r = s_oSpriteLibrary.getSprite("but_generic");
        D = new CTextButton(1380, CANVAS_HEIGHT -
            r.height / 2, r, TEXT_FOLD, FONT_GAME_1, "#ffffff", 30, s_oStage);
        D.addEventListener(ON_MOUSE_UP, this._onButFoldRelease, this);
        POS_BET[BET_PLUS] = {
            x: CANVAS_WIDTH / 2 - 200,
            y: 460
        };
        POS_BET[BET_ANTE] = {
            x: CANVAS_WIDTH / 2,
            y: 460
        };
        POS_BET[BET_PLAY] = {
            x: CANVAS_WIDTH / 2 + 200,
            y: 460
        };
        w = new CGfxButton(POS_BET[BET_PLUS].x, POS_BET[BET_PLUS].y, s_oSpriteLibrary.getSprite("bet_pair_plus"), s_oStage);
        w.addEventListener(ON_MOUSE_UP, this._onButPlusRelease, this);
        v = new CGfxButton(POS_BET[BET_ANTE].x, POS_BET[BET_ANTE].y, s_oSpriteLibrary.getSprite("bet_ante"),
            s_oStage);
        v.addEventListener(ON_MOUSE_UP, this._onButAnteRelease, this);
        var x = s_oSpriteLibrary.getSprite("bet_play"),
            y = createBitmap(x);
        y.x = POS_BET[BET_PLAY].x;
        y.y = POS_BET[BET_PLAY].y;
        y.regX = x.width / 2;
        y.regY = x.height / 2;
        s_oStage.addChild(y);
        F = new createjs.Text("", "24px " + FONT_GAME_2, "#ffde00");
        F.x = 412;
        F.y = 16;
        F.lineWidth = 150;
        F.textAlign = "left";
        F.lineHeight = 20;
        s_oStage.addChild(F);
        E = new createjs.Text("", "19px " + FONT_GAME_2, "#ffde00");
        E.x = 412;
        E.y = 66;
        F.lineWidth = 180;
        E.textAlign = "left";
        E.lineHeight = 18;
        s_oStage.addChild(E);
        A = new createjs.Text("", "21px " + FONT_GAME_1, "#fff");
        A.x = CANVAS_WIDTH / 2;
        A.y = 285;
        A.textAlign = "center";
        s_oStage.addChild(A);
        G = new createjs.Text("", "21px " + FONT_GAME_1, "#fff");
        G.x = CANVAS_WIDTH / 2;
        G.y = 550;
        G.textAlign = "center";
        s_oStage.addChild(G);
        x = new createjs.Text(TEXT_MONEY + ":", "30px " + FONT_GAME_2, "#ffde00");
        x.x = 320;
        x.y = CANVAS_HEIGHT - 84;
        x.textAlign = "left";
        s_oStage.addChild(x);
        I = new createjs.Text(TEXT_CURRENCY + a.toFixed(3), "30px " + FONT_GAME_2, "#ffde00");
        I.x = 410;
        I.y = CANVAS_HEIGHT - 84;
        I.textAlign = "left";
        s_oStage.addChild(I);
        a = [{
            x: 337,
            y: CANVAS_HEIGHT - 24
        }, {
            x: 417,
            y: CANVAS_HEIGHT - 24
        }, {
            x: 497,
            y: CANVAS_HEIGHT - 24
        }, {
            x: 577,
            y: CANVAS_HEIGHT - 24
        }, {
            x: 657,
            y: CANVAS_HEIGHT - 24
        }, {
            x: 737,
            y: CANVAS_HEIGHT - 24
        }];
        k = [];
        x = s_oGameSettings.getFichesValues();
        for (y = 0; y < NUM_FICHES; y++) r = s_oSpriteLibrary.getSprite("fiche_" + y), k[y] = new CGfxButton(a[y].x, a[y].y, r, s_oStage), k[y].addEventListenerWithParams(ON_MOUSE_UP, this._onFicheClicked, this, [x[y], y]);
        a = s_oSpriteLibrary.getSprite("fiche_highlight");
        C = createBitmap(a);
        C.regX = a.width /
            2;
        C.regY = a.height / 2;
        C.x = k[0].getX();
        C.y = k[0].getY();
        s_oStage.addChild(C);
        h = 0;
        FICHE_WIDTH = r.width;
        M = new CAnimText(CANVAS_WIDTH, CANVAS_HEIGHT, s_oStage);
        L = new CPaytablePanel(CANVAS_WIDTH - 303, 400, s_oStage);
        this.disableButtons();
        this.refreshButtonPos(s_iOffsetX, s_iOffsetY)
    };
    this.unload = function() {
        n.unload();
        n = null;
        !1 === DISABLE_SOUND_MOBILE && (z.unload(), z = null);
        B && !1 === inIframe() && J.unload();
        m.unload();
        p.unload();
        l.unload();
        s_oInterface = null
    };
    this.refreshButtonPos = function(a, h) {
        n.setPosition(b - a, h + f);
        !1 !==
            DISABLE_SOUND_MOBILE && !1 !== s_bMobile || z.setPosition(e - a, h + g);
        B && !1 === inIframe() && J.setPosition(d - a, c + h);
        L.refreshButtonPos(a, h)
    };
    this.reset = function() {
        this.disableButtons()
    };
    this.enableBetFiches = function(a) {
        for (var b = 0; b < NUM_FICHES; b++) k[b].enable();
        m.enable();
        v.enable();
        w.enable();
        a && l.enable()
    };
    this.disableBetFiches = function() {
        for (var a = 0; a < NUM_FICHES; a++) k[a].disable();
        m.disable();
        l.disable();
        v.disable();
        w.disable()
    };
    this.disableButtons = function() {
        p.disable();
        D.disable();
        t.disable()
    };
    this.enable =
        function(a, b, c) {
            a ? p.enable() : p.disable();
            b ? t.enable() : t.disable();
            c ? D.enable() : D.disable()
        };
    this.refreshCredit = function(a) {
        I.text = TEXT_CURRENCY + a.toFixed(3)
    };
    this.refreshCardValue = function(a, b) {
        A.text = "" + a;
        G.text = "" + b
    };
    this.displayMsg = function(a, b) {
        F.text = a;
        E.text = b
    };
    this.clearCardValueText = function() {
        A.text = "";
        G.text = "";
        M.hide()
    };
    this._onFicheClicked = function(a) {
        C.x = k[a[1]].getX();
        C.y = k[a[1]].getY();
        h = a[1]
    };
    this.showResultText = function(a) {
        M.show({
            x: -200,
            y: CANVAS_HEIGHT / 2 + 160
        }, {
            x: CANVAS_WIDTH / 2 - 450,
            y: CANVAS_HEIGHT / 2 + 160
        }, a)
    };
    this.showHandValue = function(a, b) {
        A.text = TEXT_EVALUATOR[a];
        G.text = TEXT_EVALUATOR[b]
    };
    this._onButClearRelease = function() {
        s_oGame.clearBets()
    };
    this._onButRebetRelease = function() {
        l.disable();
        s_oGame.onRebet()
    };
    this._onButPlusRelease = function() {
        s_oGame.setPairPlusBet(h)
    };
    this._onButAnteRelease = function() {
        s_oGame.setBet(h, BET_ANTE)
    };
    this._onButDealRelease = function() {
        this.disableBetFiches();
        this.disableButtons();
        s_oGame.onDeal()
    };
    this._onButPlayRelease = function() {
        this.disableBetFiches();
        this.disableButtons();
        s_oGame.onPlay()
    };
    this._onButFoldRelease = function() {
        this.disableBetFiches();
        this.disableButtons();
        s_oGame.onFold()
    };
    this._onExit = function() {
        new CAreYouSurePanel(s_oGame.onExit)
    };
    this._onAudioToggle = function() {
        createjs.Sound.setMute(s_bAudioActive);
        s_bAudioActive = !s_bAudioActive
    };
    this._onFullscreenRelease = function() {
        s_bFullscreen ? (H.call(window.document), s_bFullscreen = !1) : (B.call(window.document.documentElement), s_bFullscreen = !0);
        sizeHandler()
    };
    this.getFicheSelected = function() {
        return h
    };
    this.isResultPanelvisible = function() {
        return M.isVisible()
    };
    s_oInterface = this;
    this._init(a);
    return this
}
var s_oInterface = null;

function CTweenController() {
    this.tweenValue = function(a, d, c) {
        return a + c * (d - a)
    };
    this.easeLinear = function(a, d, c, b) {
        return c * a / b + d
    };
    this.easeInCubic = function(a, d, c, b) {
        b = (a /= b) * a * a;
        return d + c * b
    };
    this.easeBackInQuart = function(a, d, c, b) {
        b = (a /= b) * a;
        return d + c * (2 * b * b + 2 * b * a + -3 * b)
    };
    this.easeInBack = function(a, d, c, b) {
        return c * (a /= b) * a * (2.70158 * a - 1.70158) + d
    };
    this.easeOutCubic = function(a, d, c, b) {
        return c * ((a = a / b - 1) * a * a + 1) + d
    }
}

function CSeat() {
    var a, d, c, b, f, e, g, h, k, n, m, l;
    this._init = function() {
        n = new createjs.Container;
        n.x = CANVAS_WIDTH / 2 - 160;
        n.y = 586;
        s_oStage.addChild(n);
        l = [];
        for (var b = 0; 3 > b; b++) l[b] = new CFichesController;
        c = d = a = g = 0;
        this.reset();
        m = new CVector2;
        m.set(81, 50);
        k = new CVector2(m.getX(), m.getY())
    };
    this.unload = function() {
        s_oStage.removeChild(n)
    };
    this.addEventListener = function(a, b, c) {};
    this.reset = function() {
        for (var a = e = 0; a < l.length; a++) l[a].reset();
        h = [];
        for (a = 0; 3 > a; a++) h[a] = []
    };
    this.clearBet = function() {
        c = d = a = 0;
        h = [];
        for (var b = 0; b < l.length; b++) l[b].reset(), h[b] = []
    };
    this.resetBet = function() {
        c = d = a = 0
    };
    this.setCredit = function(a) {
        g = a
    };
    this.increaseCredit = function(a) {
        g += a
    };
    this.betAnte = function(b) {
        a += b;
        a = parseFloat(a.toFixed(2));
        l[BET_ANTE].createFichesPile(a, POS_BET[BET_ANTE].x, POS_BET[BET_ANTE].y)
    };
    this.betPlay = function() {
        d = a;
        d = parseFloat(d.toFixed(2));
        l[BET_PLAY].createFichesPile(d, POS_BET[BET_PLAY].x, POS_BET[BET_PLAY].y)
    };
    this.betPairPlus = function(a) {
        c += a;
        c = parseFloat(c.toFixed(2));
		
		document.getElementById("place_bid_charge").value = c;
        l[BET_PLUS].createFichesPile(c,
            POS_BET[BET_PLUS].x, POS_BET[BET_PLUS].y)
    };
    this.setPrevBet = function() {
        b = a;
        b = parseFloat(b.toFixed(2));
        f = c
    };
    this.decreaseCredit = function(a) {
        g -= a;
        g = parseFloat(g.toFixed(2))
    };
    this.refreshFiches = function(a, b, c, d, f) {
        h[f].push({
            value: a,
            index: b
        });
        l[f].refreshFiches(h[f], c, d)
    };
    this.initMovement = function(a, b, c) {
        l[a].initMovement(b, c)
    };
    this.newCardDealed = function() {
        e++
    };
    this.rebet = function() {
        d = 0;
        a = b;
        c = f;
        this.decreaseCredit(b + f);
        l[BET_ANTE].createFichesPile(b, POS_BET[BET_ANTE].x, POS_BET[BET_ANTE].y);
        0 < c && l[BET_PLUS].createFichesPile(f,
            POS_BET[BET_PLUS].x, POS_BET[BET_PLUS].y);
        return b + f
    };
    this.checkIfRebetIsPossible = function() {
        for (var a = 0, b = 0; b < l.length; b++) var c = parseFloat(l[b].getPrevBet().toFixed(2)),
            a = a + c;
        return a > g ? !1 : !0
    };
    this.updateFichesController = function() {
        for (var a = 0; a < l.length; a++) l[a].update()
    };
    this.getAttachCardOffset = function() {
        k.set(n.x + m.getX() + (CARD_WIDTH + 14) * e, n.y + m.getY());
        return k
    };
    this.getBetAnte = function() {
        return a
    };
    this.getBetPlay = function() {
        return d
    };
    this.getBetPlus = function() {
        return c
    };
    this.getCredit = function() {
        return g
    };
    this.getCardOffset = function() {
        return m
    };
    this.getPotentialWin = function(a) {
        return (void 0)[a]
    };
    this.getStartingBet = function() {
        for (var a = 0, b = 0; b < l.length; b++) a += l[b].getValue();
        return a
    };
    this._init()
}

function CFichesController() {
    var a, d, c, b, f, e, g, h, k, n;
    this._init = function() {
        h = new createjs.Container;
        s_oStage.addChild(h);
        f = new CVector2;
        f.set(h.x, h.y);
        k = new createjs.Container;
        s_oStage.addChild(k);
        n = new createjs.Text("", "28px " + FONT_GAME_1, "#fff");
        n.textAlign = "center";
        k.addChild(n);
        c = b = d = 0;
        a = !1
    };
    this.addEventListener = function(a, b, c) {};
    this.reset = function() {
        a = !1;
        c = 0;
        h.removeAllChildren();
        h.x = f.getX();
        h.y = f.getY();
        n.text = ""
    };
    this.setPrevValue = function(a) {
        b = a
    };
    this.refreshFiches = function(a, b, d) {
        a = a.sortOn("value",
            "index");
        for (var f = b, e = d + 10, g = c = 0, l = 0; l < a.length; l++) {
            var k = s_oSpriteLibrary.getSprite("fiche_" + a[l].index),
                k = createBitmap(k);
            k.scaleX = .7;
            k.scaleY = .7;
            h.addChild(k);
            k.x = f - 12;
            k.y = e;
            e -= 5;
            g++;
            9 < g && (g = 0, f += FICHE_WIDTH, e = d);
            c += a[l].value
        }
        playSound("chip", 1, 0);
        n.x = b;
        n.y = d + 35;
        n.text = c.toFixed(2) + TEXT_CURRENCY
    };
    this.createFichesPile = function(a, b, c) {
        this.reset();
        var d = s_oGameSettings.getFichesValues(),
            f = [];
        do {
            var e = d[d.length - 1];
            for (var h = d.length - 1; e > a;) h--, e = d[h];
            for (var h = Math.floor(a / e), g = 0; g < h; g++) f.push({
                value: e,
                index: s_oGameSettings.getIndexForFiches(e)
            });
            e = Math.floor(a / e) === a / e ? 0 : a % e;
            a = e.toFixed(2)
        } while (0 < e);
        this.refreshFiches(f, b, c)
    };
    this.initMovement = function(d, f) {
        b = c;
        e = new CVector2(h.x, h.y);
        g = new CVector2(d, f);
        n.text = "";
        a = !0
    };
    this.getValue = function() {
        return c
    };
    this.getPrevBet = function() {
        return b
    };
    this.update = function() {
        if (a)
            if (d += s_iTimeElaps, d > TIME_FICHES_MOV) d = 0, a = !1;
            else {
                var b = easeInOutCubic(d, 0, 1, TIME_FICHES_MOV),
                    c = new CVector2,
                    c = tweenVectors(e, g, b, c);
                h.x = c.getX();
                h.y = c.getY()
            }
    };
    this._init()
}

function CVector2(a, d) {
    var c, b;
    this._init = function(a, d) {
        c = a;
        b = d
    };
    this.add = function(a, d) {
        c += a;
        b += d
    };
    this.addV = function(a) {
        c += a.getX();
        b += a.getY()
    };
    this.scalarDivision = function(a) {
        c /= a;
        b /= a
    };
    this.subV = function(a) {
        c -= a.getX();
        b -= a.getY()
    };
    this.scalarProduct = function(a) {
        c *= a;
        b *= a
    };
    this.invert = function() {
        c *= -1;
        b *= -1
    };
    this.dotProduct = function(a) {
        return c * a.getX() + b * a.getY()
    };
    this.set = function(a, d) {
        c = a;
        b = d
    };
    this.setV = function(a) {
        c = a.getX();
        b = a.getY()
    };
    this.length = function() {
        return Math.sqrt(c * c + b * b)
    };
    this.length2 =
        function() {
            return c * c + b * b
        };
    this.normalize = function() {
        var a = this.length();
        0 < a && (c /= a, b /= a)
    };
    this.getNormalize = function(a) {
        this.length();
        a.set(c, b);
        a.normalize()
    };
    this.rot90CCW = function() {
        var a = c;
        c = -b;
        b = a
    };
    this.rot90CW = function() {
        var a = c;
        c = b;
        b = -a
    };
    this.getRotCCW = function(a) {
        a.set(c, b);
        a.rot90CCW()
    };
    this.getRotCW = function(a) {
        a.set(c, b);
        a.rot90CW()
    };
    this.ceil = function() {
        c = Math.ceil(c);
        b = Math.ceil(b)
    };
    this.round = function() {
        c = Math.round(c);
        b = Math.round(b)
    };
    this.toString = function() {
        return "Vector2: " + c + ", " +
            b
    };
    this.print = function() {
        trace("Vector2: " + c + ", " + b + "")
    };
    this.getX = function() {
        return c
    };
    this.getY = function() {
        return b
    };
    this._init(a, d)
}

function CGameSettings() {
    var a, d, c;
    this._init = function() {
        var b = -1;
        a = [];
        for (var d = 0; 52 > d; d++) {
            var e = (d + 1) % 13;
            1 === e ? (e = 14, b++) : 0 === e && (e = 13);
            a.push({
                fotogram: d,
                rank: e,
                suit: b
            })
        }
        c = [.1, 1, 5, 10, 25, 100]
    };
    this.getFichesValues = function() {
        return c
    };
    this.getFichesValueAt = function(a) {
        return c[a]
    };
    this.getIndexForFiches = function(a) {
        for (var b = 0, d = 0; d < c.length; d++) c[d] === a && (b = d);
        return b
    };
    this.generateFichesPile = function(a) {
        var b = [],
            d = c.length - 1,
            g = c[d];
        do {
            var h = a % g;
            h = CMath.roundDecimal(h, 1);
            a = Math.floor(a / g);
            for (var k =
                    0; k < a; k++) b.push(g);
            d--;
            g = c[d];
            a = h
        } while (0 < h && -1 < d);
        return b
    };
    this.timeToString = function(a) {
        a = Math.round(a / 1E3);
        var b = Math.floor(a / 60);
        a -= 60 * b;
        var c = "",
            c = 10 > b ? c + ("0" + b + ":") : c + (b + ":");
        return 10 > a ? c + ("0" + a) : c + a
    };
    this.getShuffledCardDeck = function() {
        for (var b = [], c = 0; c < a.length; c++) b[c] = a[c];
        for (d = []; 0 < b.length;) d.push(b.splice(Math.round(Math.random() * (b.length - 1)), 1)[0]);
        return d
    };
    this.getCardDeck = function() {
        return a
    };
    this._init()
}
var TYPE_LINEAR = 0,
    TYPE_OUT_CUBIC = 1,
    TYPE_IN_CUBIC = 2,
    TYPE_OUT_BACK = 3,
    TYPE_IN_BACK = 4;

function ease(a, d, c, b, f, e) {
    switch (a) {
        case TYPE_LINEAR:
            var g = easeLinear(d, c, b, f, e);
            break;
        case TYPE_IN_CUBIC:
            g = easeInCubic(d, c, b, f, e);
            break;
        case TYPE_OUT_CUBIC:
            g = easeOutCubic(d, c, b, f, e);
            break;
        case TYPE_IN_BACK:
            g = easeInBack(d, c, b, f, e);
            break;
        case TYPE_OUT_BACK:
            g = easeInBack(d, c, b, f, e)
    }
    return g
}

function easeOutBounce(a, d, c, b) {
    return (a /= b) < 1 / 2.75 ? 7.5625 * c * a * a + d : a < 2 / 2.75 ? c * (7.5625 * (a -= 1.5 / 2.75) * a + .75) + d : a < 2.5 / 2.75 ? c * (7.5625 * (a -= 2.25 / 2.75) * a + .9375) + d : c * (7.5625 * (a -= 2.625 / 2.75) * a + .984375) + d
}

function easeInBounce(a, d, c, b) {
    return c - easeOutBounce(b - a, 0, c, b) + d
}

function easeInOutBounce(a, d, c, b) {
    return a < b / 2 ? .5 * easeInBounce(2 * a, 0, c, b) + d : .5 * easeOutBounce(2 * a - b, 0, c, b) + .5 * c + d
}

function easeInCirc(a, d, c, b) {
    return -c * (Math.sqrt(1 - (a /= b) * a) - 1) + d
}

function easeOutCirc(a, d, c, b) {
    return c * Math.sqrt(1 - (a = a / b - 1) * a) + d
}

function easeInOutCirc(a, d, c, b) {
    return 1 > (a /= b / 2) ? -c / 2 * (Math.sqrt(1 - a * a) - 1) + d : c / 2 * (Math.sqrt(1 - (a -= 2) * a) + 1) + d
}

function easeInCubic(a, d, c, b, f) {
    return c * (a /= b) * a * a + d
}

function easeOutCubic(a, d, c, b, f) {
    return c * ((a = a / b - 1) * a * a + 1) + d
}

function easeInOutCubic(a, d, c, b, f) {
    return 1 > (a /= b / 2) ? c / 2 * a * a * a + d : c / 2 * ((a -= 2) * a * a + 2) + d
}

function easeInElastic(a, d, c, b, f, e, g) {
    if (0 == a) return d;
    if (1 == (a /= b)) return d + c;
    g || (g = .3 * b);
    !e || e < Math.abs(c) ? (e = c, f = g / 4) : f = g / (2 * Math.PI) * Math.asin(c / e);
    return -(e * Math.pow(2, 10 * --a) * Math.sin(2 * (a * b - f) * Math.PI / g)) + d
}

function easeOutElastic(a, d, c, b, f, e, g) {
    if (0 == a) return d;
    if (1 == (a /= b)) return d + c;
    g || (g = .3 * b);
    !e || e < Math.abs(c) ? (e = c, f = g / 4) : f = g / (2 * Math.PI) * Math.asin(c / e);
    return e * Math.pow(2, -10 * a) * Math.sin(2 * (a * b - f) * Math.PI / g) + c + d
}

function easeInOutElastic(a, d, c, b, f, e, g) {
    if (0 == a) return d;
    if (1 == (a /= b)) return d + c;
    g || (g = .3 * b);
    !e || e < Math.abs(c) ? (e = c, f = g / 4) : f = g / (2 * Math.PI) * Math.asin(c / e);
    return 1 > a ? -.5 * e * Math.pow(2, 10 * --a) * Math.sin(2 * (a * b - f) * Math.PI / g) + d : e * Math.pow(2, -10 * --a) * Math.sin(2 * (a * b - f) * Math.PI / g) * .5 + c + d
}

function easeInExpo(a, d, c, b) {
    return 0 == a ? d : c * Math.pow(2, 10 * (a / b - 1)) + d
}

function easeOutExpo(a, d, c, b) {
    return a == b ? d + c : c * (-Math.pow(2, -10 * a / b) + 1) + d
}

function easeInOutExpo(a, d, c, b) {
    return 0 == a ? d : a == b ? d + c : 1 > (a /= b / 2) ? c / 2 * Math.pow(2, 10 * (a - 1)) + d : c / 2 * (-Math.pow(2, -10 * --a) + 2) + d
}

function easeLinear(a, d, c, b) {
    return c * a / b + d
}

function easeInQuad(a, d, c, b) {
    return c * (a /= b) * a + d
}

function easeOutQuad(a, d, c, b) {
    return -c * (a /= b) * (a - 2) + d
}

function easeInOutQuad(a, d, c, b) {
    return 1 > (a /= b / 2) ? c / 2 * a * a + d : -c / 2 * (--a * (a - 2) - 1) + d
}

function easeInQuart(a, d, c, b) {
    return c * (a /= b) * a * a * a + d
}

function easeOutQuart(a, d, c, b) {
    return -c * ((a = a / b - 1) * a * a * a - 1) + d
}

function easeInOutQuart(a, d, c, b) {
    return 1 > (a /= b / 2) ? c / 2 * a * a * a * a + d : -c / 2 * ((a -= 2) * a * a * a - 2) + d
}

function easeInQuint(a, d, c, b) {
    return c * (a /= b) * a * a * a * a + d
}

function easeOutQuint(a, d, c, b) {
    return c * ((a = a / b - 1) * a * a * a * a + 1) + d
}

function easeInOutQuint(a, d, c, b) {
    return 1 > (a /= b / 2) ? c / 2 * a * a * a * a * a + d : c / 2 * ((a -= 2) * a * a * a * a + 2) + d
}

function easeInSine(a, d, c, b) {
    return -c * Math.cos(a / b * (Math.PI / 2)) + c + d
}

function easeOutSine(a, d, c, b) {
    return c * Math.sin(a / b * (Math.PI / 2)) + d
}

function easeInOutSine(a, d, c, b) {
    return -c / 2 * (Math.cos(Math.PI * a / b) - 1) + d
}

function easeInBack(a, d, c, b) {
    return c * (a /= b) * a * (2.70158 * a - 1.70158) + d
}

function easeOutBack(a, d, c, b) {
    return c * ((a = a / b - 1) * a * (2.70158 * a + 1.70158) + 1) + d
}

function CCard(a, d, c) {
    var b, f, e = -1,
        g, h, k, n, m, l, w, v, p, t;
    this._init = function(a, b, c) {
        t = c;
        c = {
            images: [s_oSpriteLibrary.getSprite("card_spritesheet")],
            frames: {
                width: CARD_WIDTH,
                height: CARD_HEIGHT,
                regX: CARD_WIDTH / 2,
                regY: CARD_HEIGHT / 2
            },
            animations: {
                card_1_1: [0],
                card_1_2: [1],
                card_1_3: [2],
                card_1_4: [3],
                card_1_5: [4],
                card_1_6: [5],
                card_1_7: [6],
                card_1_8: [7],
                card_1_9: [8],
                card_1_10: [9],
                card_1_J: [10],
                card_1_Q: [11],
                card_1_K: [12],
                card_2_1: [13],
                card_2_2: [14],
                card_2_3: [15],
                card_2_4: [16],
                card_2_5: [17],
                card_2_6: [18],
                card_2_7: [19],
                card_2_8: [20],
                card_2_9: [21],
                card_2_10: [22],
                card_2_J: [23],
                card_2_Q: [24],
                card_2_K: [25],
                card_3_1: [26],
                card_3_2: [27],
                card_3_3: [28],
                card_3_4: [29],
                card_3_5: [30],
                card_3_6: [31],
                card_3_7: [32],
                card_3_8: [33],
                card_3_9: [34],
                card_3_10: [35],
                card_3_J: [36],
                card_3_Q: [37],
                card_3_K: [38],
                card_4_1: [39],
                card_4_2: [40],
                card_4_3: [41],
                card_4_4: [42],
                card_4_5: [43],
                card_4_6: [44],
                card_4_7: [45],
                card_4_8: [46],
                card_4_9: [47],
                card_4_10: [48],
                card_4_J: [49],
                card_4_Q: [50],
                card_4_K: [51],
                back: [52]
            }
        };
        c = new createjs.SpriteSheet(c);
        p = createSprite(c,
            "back", CARD_WIDTH / 2, CARD_HEIGHT / 2, CARD_WIDTH, CARD_HEIGHT);
        p.x = a;
        p.y = b;
        p.rotation = 120;
        p.stop();
        t.addChild(p);
        w = [];
        v = []
    };
    this.unload = function() {
        l = m = null;
        t.removeChild(p)
    };
    this.addEventListener = function(a, b, c) {
        w[a] = b;
        v[a] = c
    };
    this.setInfo = function(a, c, d, p, v, t) {
        f = !1;
        n = 0;
        g = d;
        h = p;
        m = a;
        l = c;
        k = t;
        b = v;
        e = STATE_CARD_DEALING
    };
    this.initRemoving = function(a) {
        m = new CVector2(p.x, p.y);
        l = a;
        n = 0;
        e = STATE_CARD_REMOVING
    };
    this.setValue = function() {
        p.gotoAndStop(g);
        var a = this;
        createjs.Tween.get(p).to({
            scaleX: 1
        }, 100).call(function() {
            a.cardShown()
        })
    };
    this.showCard = function() {
        var a = this;
        createjs.Tween.get(p).to({
            scaleX: .1
        }, 100).call(function() {
            a.setValue()
        })
    };
    this.hideCard = function() {
        var a = this;
        createjs.Tween.get(p).to({
            scaleX: .1
        }, 100).call(function() {
            a.setBack()
        })
    };
    this.setBack = function() {
        p.gotoAndStop("back");
        var a = this;
        createjs.Tween.get(p).to({
            scaleX: 1
        }, 100).call(function() {
            a.cardHidden()
        })
    };
    this.cardShown = function() {
        w[ON_CARD_SHOWN] && w[ON_CARD_SHOWN].call(v[ON_CARD_SHOWN])
    };
    this.cardHidden = function() {
        f = !0
    };
    this.getValue = function() {
        return h
    };
    this.getFotogram = function() {
        return g
    };
    this._updateDealing = function() {
        n += s_iTimeElaps;
        if (n > TIME_CARD_DEALING) e = -1, n = 0, p.x = l.getX(), p.y = l.getY(), p.rotation = 360, w[ON_CARD_ANIMATION_ENDING] && w[ON_CARD_ANIMATION_ENDING].call(v[ON_CARD_ANIMATION_ENDING], this, b, k);
        else {
            this.visible = !0;
            var a = easeInOutCubic(n, 0, 1, TIME_CARD_DEALING),
                c = new CVector2,
                c = tweenVectors(m, l, a, c);
            p.x = c.getX();
            p.y = c.getY();
            p.rotation = 120 + 24E3 * a / 100
        }
    };
    this._updateRemoving = function() {
        n += s_iTimeElaps;
        if (n > TIME_CARD_REMOVE) n = 0, f = !1, e = -1,
            this.unload();
        else {
            var a = easeInOutCubic(n, 0, 1, TIME_CARD_REMOVE),
                b = new CVector2,
                b = tweenVectors(m, l, a, b);
            p.x = b.getX();
            p.y = b.getY();
            p.rotation = 4500 * a / 100
        }
    };
    this.update = function() {
        switch (e) {
            case STATE_CARD_DEALING:
                this._updateDealing();
                break;
            case STATE_CARD_REMOVING:
                !0 === f && this._updateRemoving()
        }
    };
    s_oCard = this;
    this._init(a, d, c)
}
var s_oCard;

function CGameOver() {
    var a, d, c, b;
    this._init = function() {
        b = new createjs.Container;
        s_oStage.addChild(b);
        b.on("click", function() {});
        var f = createBitmap(s_oSpriteLibrary.getSprite("msg_box"));
        b.addChild(f);
        a = new createjs.Text(TEXT_NO_MONEY, "32px " + FONT_GAME_1, "#fff");
        a.textAlign = "center";
        a.x = CANVAS_WIDTH / 2;
        a.y = 290;
        a.lineWidth = 300;
        a.shadow = new createjs.Shadow("#000000", 2, 2, 2);
        b.addChild(a);
        d = new CTextButton(CANVAS_WIDTH / 2 - 100, 450, s_oSpriteLibrary.getSprite("but_game_bg"), TEXT_RECHARGE, FONT_GAME_1, "#fff",
            14, b);
        d.addEventListener(ON_MOUSE_UP, this._onRecharge, this);
        c = new CTextButton(CANVAS_WIDTH / 2 + 100, 450, s_oSpriteLibrary.getSprite("but_game_bg"), TEXT_EXIT, FONT_GAME_1, "#fff", 14, b);
        c.addEventListener(ON_MOUSE_UP, this._onExit, this);
        this.hide()
    };
    this.unload = function() {
        d.unload();
        c.unload();
        b.off("click", function() {})
    };
    this.show = function() {
        b.visible = !0;
        $(s_oMain).trigger("end_session")
    };
    this.hide = function() {
        b.visible = !1
    };
    this._onRecharge = function() {
        $(s_oMain).trigger("recharge")
    };
    this._onExit = function() {
        s_oGame.onExit()
    };
    this._init()
}

function CMsgBox() {
    var a, d, c, b;
    this._init = function() {
        a = createBitmap(s_oSpriteLibrary.getSprite("msg_box"));
        c = new createjs.Text("", "34px " + FONT_GAME_1, "#000");
        c.x = CANVAS_WIDTH / 2 + 2;
        c.y = CANVAS_HEIGHT / 2 - 28;
        c.textAlign = "center";
        c.lineWidth = 400;
        c.textBaseline = "middle";
        d = new createjs.Text("", "34px " + FONT_GAME_1, "#ffffff");
        d.x = CANVAS_WIDTH / 2;
        d.y = CANVAS_HEIGHT / 2 - 30;
        d.textAlign = "center";
        d.lineWidth = 400;
        d.textBaseline = "middle";
        b = new createjs.Container;
        b.alpha = 0;
        b.visible = !1;
        b.addChild(a, c, d);
        s_oStage.addChild(b)
    };
    this.unload =
        function() {
            b.off("mousedown", this._onExit)
        };
    this._initListener = function() {
        b.on("mousedown", this._onExit)
    };
    this.show = function(a) {
        c.text = a;
        d.text = a;
        b.visible = !0;
        var e = this;
        createjs.Tween.get(b).to({
            alpha: 1
        }, 500).call(function() {
            e._initListener()
        });
        setTimeout(function() {
            e._onExit()
        }, 3E3)
    };
    this._onExit = function() {
        b.visible && (b.off("mousedown"), b.visible = !1)
    };
    this._init();
    return this
}

function CHandEvaluator() {
    var a, d;
    this.evaluate = function(c) {
        d = [];
        a = [];
        for (var b = 0; b < c.length; b++) d[b] = {
            rank: c[b].rank,
            suit: c[b].suit
        }, a[b] = {
            rank: c[b].rank,
            suit: c[b].suit
        };
        d.sort(this.compareRank);
        a.sort(this.compareRank);
        return {
            ret: this.rankHand(),
            sort_hand: a
        }
    };
    this.rankHand = function() {
        return this._checkForStraightFlush() ? STRAIGHT_FLUSH : this._checkForFlush() ? FLUSH : this._checkForStraight() ? STRAIGHT : this._checkForThreeOfAKind() ? THREE_OF_A_KIND : this._checkForOnePair() ? ONE_PAIR : this._checkHighCard() ?
            HIGH_CARD : NO_HAND
    };
    this._checkForStraightFlush = function() {
        return this._isStraight() && this._isFlush() ? !0 : !1
    };
    this._checkForFlush = function() {
        return this._isFlush() ? !0 : !1
    };
    this._checkForStraight = function() {
        return this._isStraight() ? !0 : !1
    };
    this._checkForThreeOfAKind = function() {
        return d[0].rank === d[1].rank && d[0].rank === d[2].rank ? !0 : !1
    };
    this._checkForOnePair = function() {
        for (var a = 0; 2 > a; a++)
            if (d[a].rank === d[a + 1].rank) return !0;
        return !1
    };
    this._checkHighCard = function() {
        for (var a = !1, b = 0; 3 > b; b++) d[b].rank > CARD_JACK &&
            (a = !0);
        return a ? !0 : !1
    };
    this._isFlush = function() {
        return d[0].suit === d[1].suit && d[0].suit === d[2].suit ? !0 : !1
    };
    this._isStraight = function() {
        var a = d[0].rank + 1 === d[1].rank;
        return a && d[0].rank === CARD_TWO && d[2].rank === CARD_ACE ? !0 : a && d[1].rank + 1 === d[2].rank ? !0 : !1
    };
    this.compareRank = function(a, b) {
        return a.rank < b.rank ? -1 : a.rank > b.rank ? 1 : 0
    };
    this.getWinnerComparingHands = function(a, b, d, e) {
        if (d === e) switch (d) {
            case STRAIGHT_FLUSH:
                return a[1].rank === b[1].rank ? a[0].suit > b[0].suit ? "dealer" : a[0].suit < b[0].suit ? "player" :
                    "standoff" : a[1].rank > b[1].rank ? "player" : "dealer";
            case FLUSH:
                return a[0].suit > b[0].suit ? "dealer" : a[0].suit < b[0].suit ? "player" : "standoff";
            case STRAIGHT:
                return a[2].rank > b[2].rank ? "player" : a[2].rank < b[2].rank ? "dealer" : "standoff";
            case THREE_OF_A_KIND:
                return a[2].rank > b[2].rank ? "player" : a[2].rank < b[2].rank ? "dealer" : "standoff";
            case ONE_PAIR:
                for (e = d = 0; e < a.length - 1; e++)
                    if (a[e].rank === a[e + 1].rank) {
                        d = a[e].rank;
                        break
                    }
                for (e = a = 0; e < b.length - 1; e++)
                    if (b[e].rank === b[e + 1].rank) {
                        a = b[e].rank;
                        break
                    }
                return d > a ? "player" :
                    d < a ? "dealer" : "standoff";
            case HIGH_CARD:
                for (d = 2; 0 <= d;) {
                    e = b[d].rank;
                    var c = a[d].rank;
                    if (c > e) return "player";
                    if (c < e) return "dealer";
                    d--
                }
                return "standoff";
            default:
                return "standoff"
        } else return e === NO_HAND ? "dealer_no_hand" : d > e ? "dealer" : "player"
    }
}

function CAnimText(a, d, c) {
    var b, f, e;
    this._init = function(a, b) {
        e = new createjs.Container;
        e.visible = !1;
        e.x = a;
        e.y = b;
        g.addChild(e);
        var c = s_oSpriteLibrary.getSprite("win_bg"),
            d = createBitmap(c);
        e.addChild(d);
        f = new createjs.Text("", "28px " + FONT_GAME_1, "#fff");
        f.x = c.width / 2;
        f.y = 0;
        f.alphabetic = "middle";
        f.textAlign = "center";
        f.lineWidth = c.width;
        e.addChild(f)
    };
    this.show = function(a, c, d) {
        b = a;
        f.text = d;
        e.x = a.x;
        e.y = a.y;
        e.visible = !0;
        createjs.Tween.get(e).to({
            x: c.x,
            y: c.y
        }, 1E3, createjs.Ease.elasticOut)
    };
    this.hide = function() {
        e.visible = !1;
        e.x = b.x;
        e.y = b.y
    };
    this.isVisible = function() {
        return e.visible
    };
    var g = c;
    this._init(a, d)
}

function CPaytablePanel(a, d, c) {
    var b, f, e, g, h;
    this._init = function(a, c) {
        b = a;
        f = c;
        h = new createjs.Container;
        h.x = b;
        h.y = f;
        k.addChild(h);
        var d = s_oSpriteLibrary.getSprite("paytable_ante_bg"),
            n = createBitmap(d);
        h.addChild(n);
        var m = new createjs.Text(TEXT_ANTE_BONUS, "24px " + FONT_GAME_1, "#ffde00");
        m.x = 10;
        m.y = 4;
        m.textAlign = "left";
        m.lineHeight = 20;
        h.addChild(m);
        for (var p = n = "", t = 0; t < PAYOUT_ANTE.length; t++) n += TEXT_EVALUATOR[t] + "\n", p += PAYOUT_ANTE[t] + ":1\n";
        e = new createjs.Text(n, "20px " + FONT_GAME_1, "#ffde00");
        e.x = 10;
        e.y = m.y + 30;
        e.textAlign = "left";
        e.lineHeight = 20;
        h.addChild(e);
        g = new createjs.Text(p, "20px " + FONT_GAME_1, "#ffde00");
        g.x = d.width - 10;
        g.y = m.y + 30;
        g.textAlign = "right";
        g.lineHeight = 20;
        h.addChild(g);
        n = createBitmap(s_oSpriteLibrary.getSprite("paytable_pair_plus_bg"));
        n.y = d.height + 10;
        h.addChild(n);
        m = new createjs.Text(TEXT_PAIR_PLUS, "24px " + FONT_GAME_1, "#ffde00");
        m.x = 10;
        m.y = n.y + 4;
        m.textAlign = "left";
        m.lineHeight = 20;
        h.addChild(m);
        p = n = "";
        for (t = 0; t < PAYOUT_PLUS.length; t++) n += TEXT_EVALUATOR[t] + "\n", p += PAYOUT_PLUS[t] +
            ":1\n";
        e = new createjs.Text(n, "20px " + FONT_GAME_1, "#ffde00");
        e.x = 10;
        e.y = m.y + 30;
        e.textAlign = "left";
        e.lineHeight = 20;
        h.addChild(e);
        g = new createjs.Text(p, "20px " + FONT_GAME_1, "#ffde00");
        g.x = d.width - 10;
        g.y = m.y + 30;
        g.textAlign = "right";
        g.lineHeight = 20;
        h.addChild(g)
    };
    this.refreshButtonPos = function(a, c) {
        h.x = b - a
    };
    var k = c;
    this._init(a, d)
}

function CHelpCursor(a, d, c, b) {
    var f, e;
    this._init = function(a, c, d) {
        f = a;
        e = createBitmap(d);
        e.visible = !1;
        e.x = a;
        e.y = c;
        b.addChild(e)
    };
    this.show = function(a) {
        0 > a && (e.scaleX *= -1);
        this._move(a, f + 30 * a, 600);
        e.visible = !0
    };
    this.hide = function() {
        createjs.Tween.removeTweens(e);
        e.x = f;
        e.visible = !1
    };
    this._move = function(a, b, c) {
        var d = 0 < a ? createjs.Ease.cubicIn : createjs.Ease.cubicOut;
        createjs.Tween.get(e).to({
            x: b
        }, c, d).call(function() {
            a *= -1;
            g._move(a, b + 15 * a, 400)
        })
    };
    this.isVisible = function() {
        return e.visible
    };
    var g = this;
    this._init(a,
        d, c)
}

function CCreditsPanel() {
    var a, d, c, b, f, e, g, h;
    this._init = function() {
        h = new createjs.Container;
        s_oStage.addChild(h);
        a = createBitmap(s_oSpriteLibrary.getSprite("msg_box"));
        h.addChild(a);
        f = new createjs.Shape;
        f.graphics.beginFill("#0f0f0f").drawRect(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT);
        f.alpha = .01;
        g = f.on("click", this._onLogoButRelease);
        h.addChild(f);
        var k = s_oSpriteLibrary.getSprite("but_exit");
        c = new CGfxButton(615, 270, k, h);
        c.addEventListener(ON_MOUSE_UP, this.unload, this);
        b = new createjs.Text(TEXT_CREDITS_DEVELOPED, "34px " +
            FONT_GAME_1, "#ffde00");
        b.textAlign = "center";
        b.textBaseline = "alphabetic";
        b.x = CANVAS_WIDTH / 2;
        b.y = CANVAS_HEIGHT / 2 - 54;
        h.addChild(b);
        k = s_oSpriteLibrary.getSprite("logo_ctl");
        d = createBitmap(k);
        d.regX = k.width / 2;
        d.regY = k.height / 2;
        d.x = CANVAS_WIDTH / 2;
        d.y = CANVAS_HEIGHT / 2;
        h.addChild(d);
        e = new createjs.Text("www.codethislab.com", "32px " + FONT_GAME_1, "#ffde00");
        e.textAlign = "center";
        e.textBaseline = "alphabetic";
        e.x = CANVAS_WIDTH / 2;
        e.y = CANVAS_HEIGHT / 2 + 70;
        h.addChild(e)
    };
    this.unload = function() {
        f.off("click", g);
        c.unload();
        c = null;
        s_oStage.removeChild(h)
    };
    this._onLogoButRelease = function() {
        window.open("http://www.codethislab.com/index.php?&l=en", "_blank")
    };
    this._init()
}

function CAreYouSurePanel(a) {
    var d, c, b, f;
    this._init = function() {
        b = new createjs.Shape;
        b.graphics.beginFill("black").drawRect(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT);
        b.alpha = 0;
        b.on("mousedown", function() {});
        s_oStage.addChild(b);
        (new createjs.Tween.get(b)).to({
            alpha: .7
        }, 500);
        f = new createjs.Container;
        s_oStage.addChild(f);
        var a = s_oSpriteLibrary.getSprite("msg_box"),
            a = createBitmap(a);
        a.x = -20;
        f.addChild(a);
        a = new createjs.Text(TEXT_ARE_SURE, " 44px " + FONT_GAME_1, "#ffde00");
        a.x = CANVAS_WIDTH / 2 - 30;
        a.y = 320;
        a.textAlign =
            "center";
        a.textBaseline = "middle";
        a.lineWidth = 500;
        f.addChild(a);
        d = new CGfxButton(CANVAS_WIDTH / 2 + 100, 420, s_oSpriteLibrary.getSprite("but_yes"), f);
        d.addEventListener(ON_MOUSE_UP, this._onButYes, this);
        c = new CGfxButton(CANVAS_WIDTH / 2 - 170, 420, s_oSpriteLibrary.getSprite("but_no"), f);
        c.addEventListener(ON_MOUSE_UP, this._onButNo, this)
    };
    this._onButYes = function() {
        e.unload();
        g()
    };
    this._onButNo = function() {
        e.unload()
    };
    this.unload = function() {
        c.unload();
        d.unload();
        s_oStage.removeChild(b);
        s_oStage.removeChild(f);
        b.off("mousedown",
            function() {})
    };
    var e = this;
    var g = a;
    this._init(a)
};




function makeid(length) {
   var result           = '';
   var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
   var charactersLength = characters.length;
   for ( var i = 0; i < length; i++ ) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
   }
   return result;
}