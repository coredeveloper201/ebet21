/*
  | Author: Atik bin hashmee
  | ----------------------------
  | this page has dependecy upon define js
*/

const api = (function($){
    "use strict"
     lgc("api--page");
    let _getUserslistsbyagents = function(agentid,getBack)
    {
        $.ajax({
            url:baseUrl+"/admin/assigned-user/lists/"+agentid,
            type:"GET",
            success:function(res)
            {
                getBack(res);
            }
        });
    }
     return {
         selectAgent : function()
         {
             return function(agentid)
             {
                _getUserslistsbyagents(agentid,res=>{
                    let tex = '';
                    res.usersList.forEach((user,index) => {
                        tex +=`<tr><td>${index}</td><td>${user.username}</td><td><a href="${baseUrl}/admin/assign-user/delete/${user.id}" class="btn btn-danger"><i class="fa fa-times"></i></a></td></tr>`;
                    });
                    $("#assigned_user_lists").html(tex);
                });
             }
         }
     }

})(jQuery);

let getUsers = api.selectAgent();
