<!DOCTYPE html>
<html>
<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
   @yield('game-head')
</head>
<body ondragstart="return false;" ondrop="return false;">

@yield('game-content')

@yield('game-script')
</body>
</html>
