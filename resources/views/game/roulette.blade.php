@extends('game.game_layout')

@section('game-head')
    <head>
        <title>Roulette</title>
        <link rel="stylesheet" href="{{ asset('game/roulette') }}/css/reset.css" type="text/css">
        <link rel="stylesheet" href="{{ asset('game/roulette') }}/css/main.css" type="text/css">
        <link rel="stylesheet" href="{{ asset('game/roulette') }}/css/orientation_utils.css" type="text/css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, minimal-ui" />
        <meta name="msapplication-tap-highlight" content="no"/>

        <script type="text/javascript" src="{{ asset('game/roulette') }}/js/jquery-3.2.1.min.js"></script>
        <script type="text/javascript" src="{{ asset('game/roulette') }}/js/createjs-2015.11.26.min.js"></script>
        <script type="text/javascript" src="{{ asset('game/roulette') }}/js/howler.min.js"></script>
        <script type="text/javascript" src="{{ asset('game/roulette') }}/js/main.js"></script>


    </head>
@endsection

@section('game-content')

    <div style="position: fixed; background-color: transparent; top: 0px; left: 0px; width: 100%; height: 100%"></div>
    <script>
        $(document).ready(function(){
            var oMain = new CMain({
                money: Number("{{ Auth::user()->available }}"),               //STARING CREDIT FOR THE USER
                min_bet: Number("{{ $creditLimits->casino_min_amount }}"),             //MINIMUM COIN FOR BET
                max_bet: Number("{{ $creditLimits->casino_max_amount }}"),             //MAXIMUM COIN FOR BET
                time_bet: 0,  //TIME TO WAIT FOR A BET IN MILLISECONDS. SET 0 IF YOU DON'T WANT BET LIMIT
                time_winner: 3000, //TIME FOR WINNER SHOWING IN MILLISECONDS
                win_occurrence: 30, //Win occurrence percentage (100 = always win).
                                    //SET THIS VALUE TO -1 IF YOU WANT WIN OCCURRENCE STRICTLY RELATED TO PLAYER BET ( SEE DOCUMENTATION)
                casino_cash:4000,    //The starting casino cash that is recharged by the money lost by the user
                fullscreen:true,     //SET THIS TO FALSE IF YOU DON'T WANT TO SHOW FULLSCREEN BUTTON
                check_orientation:true, //SET TO FALSE IF YOU DON'T WANT TO SHOW ORIENTATION ALERT ON MOBILE DEVICES
                show_credits:true,           //SET THIS VALUE TO FALSE IF YOU DON'T TO SHOW CREDITS BUTTON
                num_hand_before_ads:10  //NUMBER OF HANDS TO COMPLETE, BEFORE TRIGGERING SAVE_SCORE EVENT. USEFUL FOR INTER-LEVEL AD EVENTUALLY.
            });


            $(oMain).on("recharge", function(evt) {
                //alert("recharge");
            });

            $(oMain).on("start_session", function(evt) {
                if(getParamValue('ctl-arcade') === "true"){
                    parent.__ctlArcadeStartSession();
                }
                //...ADD YOUR CODE HERE EVENTUALLY
            });

            $(oMain).on("end_session", function(evt) {
                if(getParamValue('ctl-arcade') === "true"){
                    parent.__ctlArcadeEndSession();
                }
                //...ADD YOUR CODE HERE EVENTUALLY
            });

            $(oMain).on("bet_placed", function (evt, iTotBet) {
                document.getElementById("place_bid").value =  iTotBet;
            });

            $(oMain).on("save_score", function(evt,iMoney,iWin) {
                document.getElementById("win_bid").value =  iMoney;
                makebeteasy();
                if(getParamValue('ctl-arcade') === "true"){
                    parent.__ctlArcadeSaveScore({score:iMoney});
                }
                //...ADD YOUR CODE HERE EVENTUALLY
            });

            $(oMain).on("show_interlevel_ad", function(evt) {
                if(getParamValue('ctl-arcade') === "true"){
                    parent.__ctlArcadeShowInterlevelAD();
                }
                //...ADD YOUR CODE HERE EVENTUALLY
            });

            $(oMain).on("share_event", function(evt,iMoney) {
                if(getParamValue('ctl-arcade') === "true"){
                    parent.__ctlArcadeShareEvent({ img:"200x200.jpg",
                        title:TEXT_CONGRATULATIONS,
                        msg:TEXT_SHARE_1 + iMoney + TEXT_SHARE_2,
                        msg_share:TEXT_SHARE_3 + iMoney + TEXT_SHARE_4
                    });
                }
                //...ADD YOUR CODE HERE EVENTUALLY
            });

            if(isIOS()){
                setTimeout(function(){sizeHandler();},200);
            }else{
                sizeHandler();
            }
        });

    </script>

    <canvas id="canvas" class='ani_hack' width="750" height="600"> </canvas>
    <div data-orientation="landscape" class="orientation-msg-container"><p class="orientation-msg-text">Please rotate your device</p></div>
    <div id="block_game" style="position: fixed; background-color: transparent; top: 0px; left: 0px; width: 100%; height: 100%; display:none"></div>
@endsection

@section('game-script')
    <input type="hidden" id="place_bid"  value="0">
    <input type="hidden" id="win_bid"  value="0">

    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        function makebeteasy()
        {
            var place_bid = document.getElementById("place_bid").value ;
            var win_bid = document.getElementById("win_bid").value ;
            $.ajax({
                type: "POST",
                url: "{{ route('casino.store') }}",
                data: {place_bid:place_bid,win_bid:win_bid,game_id:"{{ $game_id }}"},
                success: function(data){
                }
            })
        }
    </script>
@endsection
