@extends('game.game_layout')

@section('game-head')
    <head>
        <title>Baccarat</title>
        <link rel="stylesheet" href="{{ asset('game/baccarat') }}/css/reset.css" type="text/css">
        <link rel="stylesheet" href="{{ asset('game/baccarat') }}/css/main.css" type="text/css">
        <link rel="stylesheet" href="{{ asset('game/baccarat') }}/css/orientation_utils.css" type="text/css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0,minimal-ui" />
        <meta name="msapplication-tap-highlight" content="no"/>

        <script type="text/javascript" src="{{ asset('game/baccarat') }}/js/jquery-2.0.3.min.js"></script>
        <script type="text/javascript" src="{{ asset('game/baccarat') }}/js/createjs-2013.12.12.min.js"></script>
        <script type="text/javascript" src="{{ asset('game/baccarat') }}/js/main.js"></script>

    </head>
@endsection

@section('game-content')

    <div style="position: fixed; background-color: transparent; top: 0px; left: 0px; width: 100%; height: 100%"></div>
    <script>
        $(document).ready(function(){
            var oMain = new CMain({
                win_occurrence: 40,          //WIN OCCURRENCE PERCENTAGE. VALUES BETWEEN 0-100
                bet_occurrence:[             //IF PLAYER MUST WIN CURRENT HAND AND THERE ARE MULTIPLE BETS:
                    //WARNING: DON'T SET ANY OF THESE VALUES AT 100.
                    20,          //OCCURRENCE FOR TIE BET
                    30,          //OCCURRENCE FOR BANKER BET
                    50           //OCCURRENCE FOR PLAYER BET
                ],
                min_bet: Number("{{ $creditLimits->casino_min_amount }}"),
                max_bet: Number("{{ $creditLimits->casino_max_amount }}"),             //MAXIMUM COIN FOR BET
                multiplier:[                 //MULTIPLIER FOR EACH BET
                    8,               //MULTIPLIER FOR TIE: PAYS 8 TO 1
                    1.95,            //MULTIPLIER FOR BANKER: PAYS 1.95 TO 1
                    2                //MULTIPLIER FOR PLAYER: PAYS 2 TO 1
                ],
                money: Number("{{ Auth::user()->available }}"),               //STARING CREDIT FOR THE USER
                game_cash: 1500,             //GAME CASH AVAILABLE WHEN GAME STARTS
                time_show_hand: 2500,        //TIME (IN MILLISECONDS) SHOWING LAST HAND
                fullscreen:true, //SET THIS TO FALSE IF YOU DON'T WANT TO SHOW FULLSCREEN BUTTON
                check_orientation:true,     //SET TO FALSE IF YOU DON'T WANT TO SHOW ORIENTATION ALERT ON MOBILE DEVICES
                //////////////////////////////////////////////////////////////////////////////////////////
                ad_show_counter: 10           //NUMBER OF HANDS PLAYED BEFORE AD SHOWN
                //
                //// THIS FUNCTIONALITY IS ACTIVATED ONLY WITH CTL ARCADE PLUGIN.///////////////////////////
                /////////////////// YOU CAN GET IT AT: /////////////////////////////////////////////////////////
                // http://codecanyon.net/item/ctl-arcade-wordpress-plugin/13856421 ///////////
            });


            $(oMain).on("recharge", function(evt) {
                alert("recharge");
            });


            $(oMain).on("bet_placed", function (evt, iTotBet) {
                alert('abc');
                document.getElementById("place_bid").value= iTotBet;


                //...ADD YOUR CODE HERE EVENTUALLY
            });



            $(oMain).on("start_session", function(evt) {
                if(getParamValue('ctl-arcade') === "true"){
                    parent.__ctlArcadeStartSession();
                }
                //...ADD YOUR CODE HERE EVENTUALLY
            });

            $(oMain).on("end_session", function(evt) {
                if(getParamValue('ctl-arcade') === "true"){
                    parent.__ctlArcadeEndSession();
                }
                //...ADD YOUR CODE HERE EVENTUALLY
            });

            $(oMain).on("save_score", function(evt,iMoney) {
                document.getElementById("win_bid").value =iMoney;
                makebeteasy();
                if(getParamValue('ctl-arcade') === "true"){
                    parent.__ctlArcadeSaveScore({score:iMoney});
                }
                //...ADD YOUR CODE HERE EVENTUALLY
            });

            $(oMain).on("show_interlevel_ad", function(evt) {
                if(getParamValue('ctl-arcade') === "true"){
                    parent.__ctlArcadeShowInterlevelAD();
                }
                //...ADD YOUR CODE HERE EVENTUALLY
            });

            $(oMain).on("share_event", function(evt, iScore) {
                if(getParamValue('ctl-arcade') === "true"){
                    parent.__ctlArcadeShareEvent({   img: TEXT_SHARE_IMAGE,
                        title: TEXT_SHARE_TITLE,
                        msg: TEXT_SHARE_MSG1 + iScore + TEXT_SHARE_MSG2,
                        msg_share: TEXT_SHARE_SHARE1 + iScore + TEXT_SHARE_SHARE1});
                }
            });

            if (isIOS()) {
                setTimeout(function () {
                    sizeHandler();
                }, 200);
            } else {
                sizeHandler();
            }
        });

    </script>

    <canvas id="canvas" class='ani_hack' width="1700" height="768"> </canvas>
    <div data-orientation="landscape" class="orientation-msg-container"><p class="orientation-msg-text">Please rotate your device</p></div>
    <div id="block_game" style="position: fixed; background-color: transparent; top: 0px; left: 0px; width: 100%; height: 100%; display:none"></div>
@endsection

@section('game-script')
    <input type="hidden" id="place_bid"  value="0">
    <input type="hidden" id="win_bid"  value="0">

    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        function makebeteasy()
        {
            var place_bid = document.getElementById("place_bid").value ;
            var win_bid = document.getElementById("win_bid").value ;
            $.ajax({
                type: "POST",
                url: "{{ route('casino.store') }}",
                data: {place_bid:place_bid,win_bid:win_bid,game_id:"{{ $game_id }}"},
                success: function(data){
                }
            })
        }
    </script>
@endsection
