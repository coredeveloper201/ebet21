@extends('game.game_layout')

@section('game-head')
    <head>
        <title>Horse racing</title>
        <link rel="stylesheet" href="{{ asset('game/horse_racing') }}/css/reset.css" type="text/css">
        <link rel="stylesheet" href="{{ asset('game/horse_racing') }}/css/main.css" type="text/css">
        <link rel="stylesheet" href="{{ asset('game/horse_racing') }}/css/orientation_utils.css" type="text/css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, minimal-ui" />
        <meta name="msapplication-tap-highlight" content="no"/>

        <script type="text/javascript" src="{{ asset('game/horse_racing') }}/js/jquery-3.1.1.min.js"></script>
        <script type="text/javascript" src="{{ asset('game/horse_racing') }}/js/createjs-2015.11.26.min.js"></script>
        <script type="text/javascript" src="{{ asset('game/horse_racing') }}/js/howler.min.js"></script>
        <script type="text/javascript" src="{{ asset('game/horse_racing') }}/js/main.js"></script>
    </head>
@endsection

@section('game-content')

    <div style="position: fixed; background-color: transparent; top: 0px; left: 0px; width: 100%; height: 100%"></div>
    <script>
        $(document).ready(function () {
            var oMain = new CMain({
                money: Number("{{ Auth::user()->available }}"),               //STARING CREDIT FOR THE USER
                min_bet: Number("{{ $creditLimits->casino_min_amount }}"),             //MINIMUM COIN FOR BET
                max_bet: Number("{{ $creditLimits->casino_max_amount }}"),             //MAXIMUM COIN FOR BET
                win_occurrence:40,   //WIN OCCURRENCE
                game_cash:100,       //GAME CASH. STARTING MONEY THAT THE GAME CAN DELIVER.
                chip_values:[1,5,10,25,50,100], //VALUE OF CHIPS
                show_credits:true, //SET THIS VALUE TO FALSE IF YOU DON'T TO SHOW CREDITS BUTTON
                fullscreen:true, //SET THIS TO FALSE IF YOU DON'T WANT TO SHOW FULLSCREEN BUTTON
                check_orientation:true,     //SET TO FALSE IF YOU DON'T WANT TO SHOW ORIENTATION ALERT ON MOBILE DEVICES
                num_levels_for_ads: 2 //NUMBER OF TURNS PLAYED BEFORE AD SHOWING //
                //////// THIS FEATURE  IS ACTIVATED ONLY WITH CTL ARCADE PLUGIN.///////////////////////////
                /////////////////// YOU CAN GET IT AT: /////////////////////////////////////////////////////////
                // http://codecanyon.net/item/ctl-arcade-wordpress-plugin/13856421///////////

            });

            $(oMain).on("start_session", function (evt) {
                console.log('start_session',evt);
                if (getParamValue('ctl-arcade') === "true") {

                    parent.__ctlArcadeStartSession();
                }
            });

            $(oMain).on("end_session", function (evt) {
                console.log('endsession',evt);
                if (getParamValue('ctl-arcade') === "true") {
                    parent.__ctlArcadeEndSession();
                }
            });

            $(oMain).on("bet_placed", function (evt, iTotBet) {
                console.log('bet_placed',evt, iTotBet);
                document.getElementById("place_bid").value =  iTotBet;
            });

            $(oMain).on("save_score", function (evt, iScore) {
                console.log('save_score',evt, iScore);
                document.getElementById("win_bid").value =  iScore;
                makebeteasy();
                if (getParamValue('ctl-arcade') === "true") {
                    parent.__ctlArcadeSaveScore({score: iScore});
                }
            });

            $(oMain).on("show_interlevel_ad", function (evt) {
                if (getParamValue('ctl-arcade') === "true") {
                    parent.__ctlArcadeShowInterlevelAD();
                }
            });

            $(oMain).on("share_event", function (evt, iScore) {
                if (getParamValue('ctl-arcade') === "true") {
                    parent.__ctlArcadeShareEvent({img: TEXT_SHARE_IMAGE,
                        title: TEXT_SHARE_TITLE,
                        msg: TEXT_SHARE_MSG1 + iScore
                            + TEXT_SHARE_MSG2,
                        msg_share: TEXT_SHARE_SHARE1
                            + iScore + TEXT_SHARE_SHARE1});
                }
            });

            if (isIOS()) {
                setTimeout(function () {
                    sizeHandler();
                }, 200);
            } else {
                sizeHandler();
            }
        });

    </script>

    <canvas id="canvas" class='ani_hack' width="1216" height="832"> </canvas>
    <div data-orientation="landscape" class="orientation-msg-container"><p class="orientation-msg-text">Please rotate your device</p></div>
    <div id="block_game" style="position: fixed; background-color: transparent; top: 0px; left: 0px; width: 100%; height: 100%; display:none"></div>
@endsection

@section('game-script')
    <input type="hidden" id="place_bid"  value="0">
    <input type="hidden" id="win_bid"  value="0">

    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        function makebeteasy()
        {
            var place_bid = document.getElementById("place_bid").value ;
            var win_bid = document.getElementById("win_bid").value ;
            $.ajax({
                type: "POST",
                url: "{{ route('casino.store') }}",
                data: {place_bid:place_bid,win_bid:win_bid,game_id:"{{ $game_id }}"},
                success: function(data){
                    console.log(data);
                }
            })
        }
    </script>
@endsection
