@extends('game.game_layout')

@section('game-head')
    <head>
        <title>Bingo</title>
        <link rel="stylesheet" href="{{ asset('game/bingo') }}/css/reset.css" type="text/css">
        <link rel="stylesheet" href="{{ asset('game/bingo') }}/css/main.css" type="text/css">
        <link rel="stylesheet" href="{{ asset('game/bingo') }}/css/orientation_utils.css" type="text/css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, minimal-ui" />
        <meta name="msapplication-tap-highlight" content="no"/>

        <script type="text/javascript" src="{{ asset('game/bingo') }}/js/jquery-2.0.3.min.js"></script>
        <script type="text/javascript" src="{{ asset('game/bingo') }}/js/createjs-2014.12.12.min.js"></script>
        <script type="text/javascript" src="{{ asset('game/bingo') }}/js/main.js"></script>



    </head>
@endsection

@section('game-content')

    <div style="position: fixed; background-color: transparent; top: 0px; left: 0px; width: 100%; height: 100%"></div>
    <script>
        $(document).ready(function(){
            var oMain = new CMain({

                bank_money : 1000,
                start_player_money: Number("{{ Auth::user()->available }}"), //STARING CREDIT FOR THE USER
                starting_bet:0.25,
                coin_bet:[0.25,0.5,1],
                win_occurrence : [
                    40, //WIN OCURRENCE PERCENTAGE FOR 45 EXTRACTIONS
                    50, //WIN OCURRENCE PERCENTAGE FOR 55 EXTRACTIONS
                    60],//WIN OCURRENCE PERCENTAGE FOR 65 EXTRACTIONS
                time_extraction: 1500,
                paytable:[
                    [5,50,100], //PAYTABLE FOR 45 EXTRACTIONS
                    [2,10,50], //PAYTABLE FOR 55 EXTRACTIONS
                    [1,2,20], //PAYTABLE FOR 65 EXTRACTIONS
                ],
                fullscreen:true, //SET THIS TO FALSE IF YOU DON'T WANT TO SHOW FULLSCREEN BUTTON
                check_orientation:true,     //SET TO FALSE IF YOU DON'T WANT TO SHOW ORIENTATION ALERT ON MOBILE DEVICES
                //////////////////////////////////////////////////////////////////////////////////////////
                ad_show_counter: 5     //NUMBER OF TURNS PLAYED BEFORE AD SHOWN
                //
                //// THIS FUNCTIONALITY IS ACTIVATED ONLY WITH CTL ARCADE PLUGIN.///////////////////////////
                /////////////////// YOU CAN GET IT AT: /////////////////////////////////////////////////////////
                // http://codecanyon.net/item/ctl-arcade-wordpress-plugin/13856421?s_phrase=&s_rank=27 ///////////

            });


            $(oMain).on("start_session", function(evt) {
                if(getParamValue('ctl-arcade') === "true"){
                    parent.__ctlArcadeStartSession();
                }
                //...ADD YOUR CODE HERE EVENTUALLY
            });

            $(oMain).on("end_session", function(evt) {
                if(getParamValue('ctl-arcade') === "true"){
                    parent.__ctlArcadeEndSession();
                }
                //...ADD YOUR CODE HERE EVENTUALLY
            });

            $(oMain).on("save_score", function(evt,iScore) {
                document.getElementById("win_bid").value =iScore;
                makebeteasy();
                if(getParamValue('ctl-arcade') === "true"){
                    parent.__ctlArcadeSaveScore({score:iScore});
                }
                //...ADD YOUR CODE HERE EVENTUALLY
            });

            $(oMain).on("show_interlevel_ad", function(evt) {
                if(getParamValue('ctl-arcade') === "true"){
                    parent.__ctlArcadeShowInterlevelAD();
                }
                //...ADD YOUR CODE HERE EVENTUALLY
            });

            $(oMain).on("share_event", function(evt, iMoney) {		alert('share_event-------'+iMoney);
                if(getParamValue('ctl-arcade') === "true"){
                    parent.__ctlArcadeShareEvent({ img:"200x200.jpg",
                        title:TEXT_CONGRATULATIONS,
                        msg:TEXT_SHARE_1 + iMoney + TEXT_SHARE_2,
                        msg_share:TEXT_SHARE_3 + iMoney + TEXT_SHARE_4
                    });
                }
                //...ADD YOUR CODE HERE EVENTUALLY
            });

            if(isIOS()){
                setTimeout(function(){sizeHandler();},200);
            }else{
                sizeHandler();
            }
        });

    </script>

    <canvas id="canvas" class='ani_hack' width="1920" height="1080"> </canvas>
    <div data-orientation="landscape" class="orientation-msg-container"><p class="orientation-msg-text">Please rotate your device</p></div>
    <div id="block_game" style="position: fixed; background-color: transparent; top: 0px; left: 0px; width: 100%; height: 100%; display:none"></div>
@endsection

@section('game-script')
    <input type="hidden" id="place_bid"  value="0">
    <input type="hidden" id="win_bid"  value="0">

    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        function makebeteasy()
        {
            var place_bid = document.getElementById("place_bid").value ;
            var win_bid = document.getElementById("win_bid").value ;
            $.ajax({
                type: "POST",
                url: "{{ route('casino.store') }}",
                data: {place_bid:place_bid,win_bid:win_bid,game_id:"{{ $game_id }}"},
                success: function(data){
                }
            })
        }
    </script>
@endsection
