@extends('layouts.frontend_layout')

@section('title')

@endsection

@section('content')

<!-- Upcoming Events area start -->
<section id="upcoming_events" class="display-inherit upcoming-margin-top upcoming-margin-bottom">
    <div class="container">
        <div class="col-12  padding-0">
            <div class="row">
                <div class="col-lg-12 col-xs-12  padding-0">
                    <div class="upcoming_title">
                        <h2> Pending </h2>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Upcoming Events area end -->


<!-- main body area start -->
<section id="body_warapper">
    <div class="container">
        <div class="row">
            <div class="col-12 padding-0 overflow-scroll">
                <table class="table table-bg-red">
                    <thead>
                    <tr>
                        <th class="table-hide-640">Date Time Accepted </th>
                        <th class="table-hide-640">Ticket #</th>
                        <th>Game</th>
                        <th>Wager Type</th>
                        <th>Risk</th>
                        <th>Win</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if (!empty($dateRecords))
                        @foreach ($dateRecords as $parlayRecords)
                            @foreach($parlayRecords as $val)
                                <tr class="tr-tap-board">
                                    <td data-th="Date Time Accepted " class="table-hide-640">
                                        {{ _getDate(strtotime($val->created_at)) }}<br/>{{ _getTime(strtotime($val->created_at)) }}
                                    </td>
                                    <td data-th="Ticket #" class="table-hide-640">
                                        {{ $val->ticket_id }}
                                    </td>
                                    <td data-th="Game">
                                        <div class="table-show-640 tr-tap-sticker">
                                            <div class="pull-left tr-tap-sticker-inline">{{ _getDate(strtotime($val->created_at)) }} &nbsp;&nbsp;{{ _getTime(strtotime($val->created_at)) }}</div>
                                            <div class="pull-right tr-tap-sticker-inline">{{ $val->ticket_id }}</div>
                                        </div>
                                        {{-- parlay bets pending result --}}
                                        @if(isset($val->detail))
                                            @foreach($val->detail as $detail)
                                                @php ($sportLeague = json_decode($detail->sport_league, true))
                                                @php( $extraInfo   = json_decode($detail->bet_extra_info))
                                                @php($type=json_decode($detail->type))
                                                {{ sportsName($sportLeague[0])."-".$sportLeague[1] }}
                                                <br>
                                                {{ isset($extraInfo->bet_on_team_name)?$extraInfo->bet_on_team_name:""}} VS {{isset($extraInfo->other_team_name)?$extraInfo->other_team_name:"" }}
                                                <br>
                                                @if (isset($extraInfo->event_date) &&  isset($extraInfo->event_time))
                                                {{ _getDate(strtotime($extraInfo->event_date)) }} ({{ _getTime(strtotime($extraInfo->event_time)) }})
                                                @else
                                                {{ _getDate(strtotime($val->created_at)) }} ({{ _getTime(strtotime($val->created_at)) }})
                                                @endif
                                                <br>
                                                <strong style="font-weight:bold;">
                                                    @if($type==1)
                                                        <strong style="font-weight: bold;"> Spread</strong>
                                                    @elseif($type==2)
                                                        <strong style="font-weight: bold;"> Money Line</strong>
                                                    @elseif($type==123)
                                                        <strong style="font-weight: bold;"> Money Line</strong>
                                                    @elseif($type==3)
                                                        <strong style="font-weight: bold;"> Total</strong>
                                                    @endif
                                                </strong>
                                                <strong style="font-weight: bold;">@if($type!==2)({{ $detail->points }})@endif</strong>
                                                <strong style="font-weight: bold;">{{ ((int)$detail->betting_condition)>0 ? "+".$detail->betting_condition:$detail->betting_condition }} </strong>
                                                    @if($type!=3 && $type!=123)
                                                        for <strong style="font-weight:bold">{{ isset($extraInfo->bet_on_team_name)?$extraInfo->bet_on_team_name:"" }}</strong>
                                                    @endif
                                                <br>
                                                <br>
                                            @endforeach
                                       {{-- Horse bet pending  --}}
                                        @elseif(isset($val->horsedetail))
                                            @foreach($val->horsedetail as $horsedetail)
                                                @php ($race_name =$horsedetail->race_name)
                                                @php ($horse_name=$horsedetail->horse_name)
                                                @php($tournament_name=$horsedetail->tournament_name)
                                                @php($race_date=$horsedetail->race_date)
                                                <strong style="font-weight:bold">{{$tournament_name}}</strong><span>_</span>
                                                <strong style="font-weight:bold">{{$race_name}}</strong><br>
                                                <strong>{{$race_date}}</strong><br>
                                                <strong style="font-weight:bold">{{$horsedetail->race_type}}</strong>
                                                <span>_</span>
                                                <strong style="font-weight:bold">{{$horsedetail->horse_number}}</strong>
                                                <strong style="font-weight:bold">{{$horsedetail->horse_name}}</strong>
                                            @endforeach
                                        @else
                                            @php ($sportLeague = json_decode(isset($val->sport_league)?$val->sport_league:'{}', true))
                                            @php ($extraInfo = json_decode(isset($val->bet_extra_info)?$val->bet_extra_info:'{}'))
                                            @if (isset($sportLeague[0]) && isset($sportLeague[1]))
                                            {{ sportsName($sportLeague[0])."-".$sportLeague[1] }}
                                            @endif

                                            <br>
                                            @if(!empty($extraInfo))
                                                @if (isset($extraInfo->bet_on_team_name) || isset($extraInfo->other_team_name))
                                                {{ $extraInfo->bet_on_team_name}}<b> VS </b> {{ $extraInfo->other_team_name }}<br>
                                                @endif
                                                @if (isset($extraInfo->event_date) &&  isset($extraInfo->event_time))
                                                {{ _getDate(strtotime($extraInfo->event_date)) }} ({{ _getTime(strtotime($extraInfo->event_time)) }})
                                                @else
                                                {{ _getDate(strtotime($val->created_at)) }} ({{ _getTime(strtotime($val->created_at)) }})
                                                @endif
                                                <span style="color:#e26c32;">
                                                    @if (isset($val->result))
                                                            @if($val->result==1)
                                                            Win
                                                        @elseif($val->result==2)
                                                            Loss
                                                        @endif
                                                    @endif

                                                </span>
                                            @endif
                                            <br>
                                            <?php
                                                $wager = '';
                                                $wager_type ='';
                                                $original_condition ='';
                                                if(isset($val->betting_wager_type))
                                                  $wager_type = $val->betting_wager_type;
                                                $show_for = true;
                                                include $_SERVER['DOCUMENT_ROOT']. '/../app/wagers.php';
                                            ?>
                                            <strong style="font-weight: bold;">
                                                {{$wager}}
                                                @if (!empty($original_condition))
                                                {{$original_condition}}
                                                @else
                                                  @if (isset($val->betting_condition_original))
                                                  {{$val->betting_condition_original }}
                                                  @endif
                                                @endif
                                            </strong>
                                            @if ($show_for)
                                            for <strong style="font-weight:bold;"> {{ isset($extraInfo->bet_on_team_name)?$extraInfo->bet_on_team_name:"" }} </strong>
                                            @endif
                                        @endif
                                    </td>
                                    <td data-th="Wager Type">
                                            <b style="margin-right:5px">{{ ($val->count>0)?$val->count.' Teams':'' }}</b>
                                        @if(($val->betting_type==="parlay")
                                        ||($val->betting_type==="tworound")
                                        ||($val->betting_type==="threeround")
                                        ||$val->betting_type==="fourround")
                                            <span class="__wager-type"> parlay </span>
                                            @else
                                                @if ($val->betting_type == "ifbet")
                                                IF Win
                                                @else
                                                <span class="__wager-type">{{$val->betting_type}}</span>
                                                @endif
                                            @endif
                                    </td>
                                    <td data-th="Risk">
                                        {{ $val->risk_amount }}
                                    </td>
                                    <td data-th="Win">
                                        {{ $val->parlay_win_amount }}
                                    </td>
                                </tr>
                            @endforeach
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </section>
    <!-- main body area start -->
@endsection
