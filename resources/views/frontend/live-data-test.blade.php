@extends('layouts.frontend_layout')

@section('title')

@endsection

@section('content')

<div ng-controller="realTimeBettingController3cvc" ng-app="realTimeBetting3cvc" id="realTimeBetting3cvc">
    <div class="popup-box d-none" id="confirm_passwordFootball">
        <div class="popup-box-body">
            <div class="close-icon">
                <span data-ng-click="passwordCloseIcon()">X</span>
            </div>
            <div>
                <h1>Enter Your Password</h1>
            </div>
            <div>
                <input type="password" class="form-control" id="bet_confirm_passwordFootball">
                <p id="mesageFootball" style="color: red"></p>
            </div>
            <div>
                <button class="btn" id="submit_confirm_passwordFootball">Submit</button>
            </div>
        </div>
    </div>
    <div class="popup-box d-none" id="bet_noticeFootball">
        <div class="popup-box-body">
            <div class="close-icon">
                <span data-ng-click="passwordCloseIcon()">X</span>
            </div>
            <div>
                <h4>Bets data has been changed, please review your bets</h4>
                <p>when review is done, hit the confirm button again</p>
            </div>
            <div>
                <button class="btn" id="Okaybutton" data-ng-click="passwordCloseIcon()">Okay</button>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-9 col-xl-9 col-lg-9 padding-0">
            <article id="myaccount" class="col span_12">
                <div class="Live-bet-event-content">
                    <div class="Live-bet-event-content-each" ng-repeat="league in bettingResult">
                        <div class="Live-bet-league-header">
                            <div class="_league_header_title">@{{league.even_name}}</div>
                            <div class="mobile-w-375">
                                <div class="_league_header_spread">Spread</div>
                                <div class="_league_header_mline">Money <span>Line</span></div>
                                <div class="_league_header_total">Total</div>
                            </div>
                        </div>
                        <div class="Live-bet-league-each" ng-repeat="item in league.results">
                            <div class="Live-bet-event-each">
                                <div class="_event_sections_mobile">
                                    @{{ item.time }}
                                    <br>
                                    @{{item.hr}}H
                                </div>
                                <div class="_league_header_title">
                                    <div class="_event_sections">
                                        @{{ item.time }}
                                        <br>
                                        @{{ item.hr }}
                                    </div>
                                    <div class="_event_titles">
                                        <ul>
                                            <li>
                                                <span class="spn-title">@{{item.home.name}}</span>
                                                <span class="spn-values">
                                                            <span ng-repeat="val in item.ss.split(',')">@{{ val.split('-')[0] }}</span>
                                                        </span>
                                            </li>
                                            <li>
                                                <span class="spn-title">@{{item.away.name}}</span>
                                                <span class="spn-values">
                                                            <span ng-repeat="val in item.ss.split(',')">@{{ val.split('-')[1] }}</span>
                                                        </span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="_league_header_spread">
                                    <ul>
                                        <li ng-if="item.spread_line.home_odd"
                                            ng-style="compareODD(item.id, 'spread_home')"
                                            id="spread_home@{{item.id}}"
                                            data-ng-click="addtocart(item.id, 'spread_home')">
                                                    <span class="mobile-w-100">
                                                        <span class="mobile-w-100" style="color: #929292">
                                                                (@{{item.spread_line.handicap}} )
                                                        </span>
                                                        <span class="mobile-w-100">
                                                            <i class="fa fa-arrow-down"
                                                               id="spread_home@{{item.id}}_decrease"
                                                               style="font-size:12px; color:red; display: none; margin-right: 5px"></i>
                                                            <i class="fa fa-arrow-up"
                                                               id="spread_home@{{item.id}}_increase"
                                                               style="font-size:12px; color:green; display: none; margin-right: 5px"></i>
                                                            <span ng-if="item.spread_line.home_odd > 0"
                                                                  style="margin-right: -2px">+ </span>
                                                            @{{ item.spread_line.home_odd }}
                                                        </span>
                                                    </span>
                                        </li>
                                        <li ng-if="!item.spread_line.home_odd" class="no-event">OTB</li>
                                        <li ng-if="item.spread_line.away_odd"
                                            ng-style="compareODD(item.id, 'spread_away')"
                                            id="spread_away@{{item.id}}"
                                            data-ng-click="addtocart(item.id, 'spread_away')">
                                                    <span class="mobile-w-100">
                                                        <span class="mobile-w-100" style="color: #929292">
                                                                (@{{ item.spread_line.handicap }})
                                                            </span>
                                                        <span class="mobile-w-100">
                                                            <i class="fa fa-arrow-down"
                                                               id="spread_away@{{item.id}}_decrease"
                                                               style="font-size:12px; color:red; display: none; margin-right: 5px"></i>
                                                            <i class="fa fa-arrow-up"
                                                               id="spread_away@{{item.id}}_increase"
                                                               style="font-size:12px; color:green; display: none; margin-right: 5px"></i>
                                                            <span ng-if="item.spread_line.away_odd > 0"
                                                                  style="margin-right: -2px">+ </span>
                                                            @{{item.spread_line.away_odd}}
                                                        </span>
                                                    </span>
                                        </li>
                                        <li ng-if="!item.spread_line.away_odd" class="no-event">OTB</li>
                                    </ul>
                                </div>
                                <div class="_league_header_mline">
                                    <ul>
                                        <li ng-if="item.money_line.home_odd"
                                            ng-style="compareODD(item.id, 'money_home')"
                                            id="money_home@{{item.id}}"
                                            data-ng-click="addtocart(item.id, 'money_home')">
                                                    <span class="mobile-w-100">
                                                        <i class="fa fa-arrow-down" id="money_home@{{item.id}}_decrease"
                                                           style="font-size:12px; color:red; display: none; margin-right: 5px"></i>
                                                        <i class="fa fa-arrow-up" id="money_home@{{item.id}}_increase"
                                                           style="font-size:12px; color:green; display: none; margin-right: 5px"></i>
                                                        <span ng-if="item.money_line.home_odd > 0"
                                                              style="margin-right: -2px">+ </span>
                                                        @{{ item.money_line.home_odd }}
                                                    </span>
                                        </li>
                                        <li class="no-event" ng-if="!item.money_line.home_odd">OTB</li>
                                        <li ng-if="item.money_line.away_odd"
                                            ng-style="compareODD(item.id, 'money_away')"
                                            id="money_away@{{item.id}}"
                                            data-ng-click="addtocart(item.id, 'money_away')">
                                                    <span class="mobile-w-100">
                                                        <i class="fa fa-arrow-down" id="money_away@{{item.id}}_decrease"
                                                           style="font-size:12px; color:red; display: none; margin-right: 5px"></i>
                                                        <i class="fa fa-arrow-up" id="money_away@{{item.id}}_increase"
                                                           style="font-size:12px; color:green; display: none; margin-right: 5px"></i>
                                                        <span ng-if="item.money_line.away_odd > 0"
                                                              style="margin-right: -2px">+</span>
                                                        @{{ item.money_line.away_odd }}
                                                    </span>
                                        </li>
                                        <li class="no-event" ng-if="!item.money_line.away_odd">OTB</li>
                                    </ul>
                                </div>
                                <div class="_league_header_total">
                                    <ul>
                                        <li ng-if="item.over.home_odd"
                                            ng-style="compareODD(item.id, 'total_home')"
                                            id="total_home@{{item.id}}"
                                            data-ng-click="addtocart(item.id, 'total_home')">
                                                    <span class="mobile-w-100">
                                                        <span class="mobile-w-100" style="color: #929292">
                                                                (Over @{{item.over_under.handicap}})
                                                        </span>
                                                        <span class="mobile-w-100">
                                                            <i class="fa fa-arrow-down"
                                                               id="total_home@{{item.id}}_decrease"
                                                               style="font-size:12px; color:red; display: none; margin-right: 5px"></i>
                                                            <i class="fa fa-arrow-up"
                                                               id="total_home@{{item.id}}_increase"
                                                               style="font-size:12px; color:green; display: none; margin-right: 5px"></i>
                                                            <span ng-if="item.over.home_odd > 0"
                                                                  style="margin-right: -2px">+</span>
                                                             @{{ item.over.home_odd }}
                                                        </span>
                                                    </span>

                                        </li>
                                        <li class="no-event" ng-if="!item.over.home_odd">OTB</li>
                                        <li ng-if="item.under.away_odd"
                                            ng-style="compareODD(item.id, 'total_away')"
                                            id="total_away@{{item.id}}"
                                            data-ng-click="addtocart(item.id, 'total_away')">
                                                    <span class="mobile-w-100">
                                                        <span class="mobile-w-100" style="color: #929292">
                                                                (Under @{{item.over_under.handicap}})
                                                        </span>
                                                        <span class="mobile-w-100">
                                                            <i class="fa fa-arrow-down"
                                                               id="total_away@{{item.id}}_decrease"
                                                               style="font-size:12px; color:red; display: none; margin-right: 5px"></i>
                                                            <i class="fa fa-arrow-up"
                                                               id="total_away@{{item.id}}_increase"
                                                               style="font-size:12px; color:green; display: none; margin-right: 5px"></i>
                                                            <span ng-if="item.under.away_odd > 0"
                                                                  style="margin-right: -2px">+</span> @{{ item.under.away_odd }}
                                                        </span>
                                                    </span>
                                        </li>
                                        <li class="mediumtd" ng-if="!item.under.away_odd">OTB</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </article>
            <div ng-if="loading" class="preloading">
                <img src="assets/img/live/spinner.gif" alt="loading..."/>
            </div>
        </div>

        <div class="col-md-3 col-xl-3 col-lg-3 padding-0">
            <div class="bg-overlay" id="popup-optin-6">
                <div class="subscribe-optin" style="margin-top: 8px;">
                    <a href="#" class="optin-close">&times;</a>
                    <div class="selip custom-slip">
                        <div class="slip_cart">
                            <h3> BET SLIP <span id="slip_count" >@{{ selectedData.length }}</span> </h3>
                        </div>
                        <!-- scroll bet slip  scroll_bet_slip-->
                        <div id="dev-slip-container">
                            <div  class="live_bet_slip_empty" ng-if="selectedData.length == 0">
                                <h3>Bet Slip is Empty</h3>

                                <p>Not sure where to start?</p>
                                <a href="#">Learn how to place a bet</a>
                            </div>

                            <div class="bet_slip_add straight_bet_count" ng-if="selectedData.length > 0" ng-repeat="item in selectedData">
                                <div class="add_title">
                                    <table class="bet_slip_title">
                                        <tr>
                                            <td>@{{ item.displayName }}</td>
                                            <td>
                                                <div>
                                                    <span style="float:right;font-size:15px;color:rgba(0,0,0,0.75);">
                                                        <i class="fa fa-arrow-down" id="betSlip_@{{item.id}}_@{{item.type}}_decrease" style="font-size:12px; color:red; display: none; margin-right: 5px"></i>
                                                        <i class="fa fa-arrow-up" id="betSlip_@{{item.id}}_@{{item.type}}_increase" style="font-size:12px; color:green; display: none; margin-right: 5px"></i>
                                                        <span ng-if="item.spread_handicap != null">( @{{item.spread_handicap}} )</span>
                                                        <span ng-if="item.score > 0">+</span>
                                                        @{{ item.score }}
                                                        <a class="closecard" data-ng-click="removeCardItem(item.id, item.type)" style="color: #fff !important;"> &times; </a>
                                                    </span>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="bet_slip_details">
                                        <p> @{{ item.wager }}  </p>
                                        <h3><strong> @{{ item.teamHomeName }} @ @{{ item.teamAwayName }} </strong></h3>
                                        <h3> @{{ item.dateTime | date: 'yyyy-MM-dd' }}</h3>
                                    </div>
                                    <table class="bet_slip_rat">
                                        <tr>
                                            <td>
                                                <input type="text" class="custom-bet-input risk_stake_slip form-control"
                                                       id="risk_@{{ item.type }}_@{{ item.id }}"
                                                       ng-model="item.risk"
                                                       ng-change="betChanges(item.id, item.type, 'risk')"
                                                       placeholder="Risk">
                                            </td>
                                            <td>
                                                <input type="text" class="custom-bet-input risk_win_slip form-control"
                                                       id="win_@{{ item.type }}_@{{ item.id }}"
                                                       ng-model="item.win" ng-change="betChanges(item.id, item.type, 'win')"
                                                       placeholder="Win">
                                            </td>
                                        </tr>
                                    </table>
                                    <p class="messgae hide"></p>
                                    <div class="confirm-success-alert" id="confirmSuccess" style="display: none">
                                        Confirm Success Message
                                    </div>
                                    <div class="confirm-fail-alert" ng-if="confirmFail">
                                        Your Balance doesn't enough
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="slip-message-box"><span id="cartMessage"></span></div>
                        <div class="bet_slip_details">
                            <div class="slip_price_table">
                                <div class="slip_checkbox" id="freePlayContainer">
                                    <input type="checkbox" name="freeplay" value="1" id="freeplay"> Free Play <br>
                                </div>
                                <table>
                                    <tr>
                                        <td>Total bets</td>
                                        <td><span>@{{ selectedData.length }}</span></td>
                                    </tr>
                                    <tr>
                                        <td>Total Stake</td>
                                        <td><span>@{{ totalStake }}</span></td>
                                    </tr>
                                    <tr>
                                        <td>Possible Winning</td>
                                        <td> <span>@{{ possibleWin }}</span> </td>
                                    </tr>
                                </table>
                                <p class="messgae hide" id="messagebox">

                                </p>
                            </div>
                            <div class="confirm">
                                <button type="button" class="confirm" id="confirmbutton"
                                        data-ng-click="confirmBet()">
                                    Confirm
                                </button>
                                <p style="text-align:center;"><a class="clearsection" id="clearBetSlip"
                                                                 data-ng-click="clearBetSlip()"> Clear all selection</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <a href="#popup-optin-6" class="popup-live">SLIP <span id="badge_count">@{{selectedData.length}}</span></a>
</div>
<script>
    console.groupCollapsed("live hockey");
    var realTimeBet = angular.module('realTimeBetting3cvc', []);
     realTimeBet.controller('realTimeBettingController3cvc', function ($scope, $http, $filter) {
         $scope.bettingResult = [];
         $scope.oldBettingData = [];
         $scope.confirmData = [];
         $scope.loading = true;
         $scope.selectedData = [];
         $scope.timePeriod = 20000;
         $scope.totalStake = 0;
         $scope.possibleWin = 0;
         $scope.confirmSuccess = false;
         $scope.confirmFail = false;
         let getApi;

         $scope.isChanged = false;
         $scope.slipOddTotal = 0;

         $scope.changeNow = false;


         $scope.timeFormate = function(timestap)
         {
             timestap = parseInt(timestap);
             let h = padZero(new Date(timestap).getHours());
             let m = padZero(new Date(timestap).getMinutes());
             return h+":"+m;
         }

         function padZero(n) {
             if (n < 10) return '0' + n;
             return n;
         }

         getBetData();

         function getApiResponse() {
             getApi = setInterval(function () {
                 getBetData();
             }, $scope.timePeriod);
         }

         function getBetData() {
             $http.get('{{url("/live-hockey")}}').then(function(response) {
                 if (response.data) {
                     $scope.oldBettingData = $scope.bettingResult;
                     $scope.bettingResult = response.data;
                     console.log(response.data);

                     betRefresh();
                     $scope.loading = false;
                     if ($scope.bettingResult.length < 5)
                     { clearInterval(getApi); $scope.timePeriod = 3000;
                        getApiResponse(); }
                     else if ($scope.bettingResult.length >= 5 &&  $scope.bettingResult.length < 10) {
                         clearInterval(getApi); $scope.timePeriod = 5000;
                         getApiResponse();
                        }
                     else if ($scope.bettingResult.length >= 10 && $scope.bettingResult.length < 25) {
                         clearInterval(getApi); $scope.timePeriod = 10000;
                         getApiResponse();
                        }
                     else if ($scope.bettingResult.length >= 25 && $scope.bettingResult.length < 35) {
                          clearInterval(getApi); $scope.timePeriod = 15000;
                          getApiResponse();
                        }
                     else if ($scope.bettingResult.length >= 35) {
                          clearInterval(getApi); $scope.timePeriod = 20000;
                          getApiResponse();
                        }
                 }
             });
         }

         // SET selected value to latest value.
         function betRefresh() {
             let newTotal = 0;
             $scope.selectedData.forEach((item) => {
                 let replaced = false;
                 $scope.bettingResult.forEach((rs) => {
                     rs.results.forEach(result=>{
                         if(item.id === result.id) {
                             switch (item.type) {
                                 case 'spread_home':
                                     if (result.spread_line.home_odd) {
                                         item.score = result.spread_line.home_odd; replaced = true;
                                     }
                                     break;
                                 case 'spread_away':
                                     if (result.spread_line.away_odd) {
                                         item.score = result.spread_line.away_odd; replaced = true;
                                     }
                                     break;
                                 case 'total_home':
                                     if ( result.over.home_odd) {
                                        item.score = result.over.home_odd; replaced = true;
                                     }
                                     break;
                                 case 'total_away':
                                     if (result.under.away_odd) {
                                        item.score = result.under.away_odd; replaced = true;

                                     }
                                     break;
                                 case 'money_home':
                                     if (result.money_line.home_odd) {
                                         item.score = result.money_line.home_odd; replaced = true;
                                     }
                                     break;
                                 case 'money_away':
                                     if (result.money_line.away_odd) {
                                        item.score = result.money_line.away_odd; replaced = true;
                                     }
                                     break;
                                 case 'draw_otot':
                                     if (result.draw.o_to_t) {
                                         item.score = result.draw.o_to_t; replaced = true;
                                     }
                                     break;
                                 case 'draw_goal':
                                     if (result.draw.goal.odd) {
                                         item.score = result.draw.goal.odd; replaced = true;
                                     }
                                     break;
                             }
                             if ($scope.changeNow) {
                                 newTotal += item.score;
                             }

                         }
                     });

                 });
                 if (!replaced) $scope.selectedData.splice($scope.selectedData.indexOf(item), 1);
                 calculateReport();
             });
             if ($scope.changeNow) {
                 if (newTotal !== $scope.slipOddTotal)
                     $scope.isChanged  = true;
             }

         }

         // Increase, Decrease Icon && Add active class
         $scope.compareODD = function(id, type) {

             let oldFirstVal = 0;
             let curFirstVal = 0;
             let selected = false;

             if ($scope.oldBettingData.length < 1) return;

             if (type === 'spread_home') {

                 $scope.selectedData.forEach((item) => {
                     if (item.id === id && item.type === type) {
                         $('#spread_home'+id).addClass('active');
                         selected = true;
                     }
                 });

                 if (!selected) $('#spread_home'+id).removeClass('active');

                 angular.forEach($scope.oldBettingData, function (ite) {
                     ite.results.forEach(item=>{
                         if (item.id === id) { oldFirstVal = item.spread_line.home_odd; }
                     });
                 });

                 angular.forEach($scope.bettingResult, function (ite) {
                     ite.results.forEach(item=> {
                         if (item.id === id) {
                             curFirstVal = item.spread_line.home_odd;
                         }
                     });
                 });
                 $("#betSlip_"+id+"_"+type+"_decrease").hide();
                 $("#betSlip_"+id+"_"+type+"_increase").hide();
                 if (oldFirstVal > curFirstVal) {
                     $('#spread_home'+id).css('color', 'red');
                     $('#spread_home'+id+'_decrease').show();
                     $("#betSlip_"+id+"_"+type+"_decrease").show();
                     $scope.betChanges(id,type);
                 }
                 else if (oldFirstVal < curFirstVal) {
                     $('#spread_home'+id).css('color', 'green');
                     $('#spread_home'+id+'_increase').show();
                     $("#betSlip_"+id+"_"+type+"_increase").show();
                     $scope.betChanges(id,type);
                 }
             }
             else if (type === 'spread_away') {
                 $scope.selectedData.forEach((item) => {
                     if (item.id === id && item.type === type) {
                         $('#spread_away'+id).addClass('active');
                         selected = true;
                     }
                 });

                 if (!selected) $('#spread_away'+id).removeClass('active');

                 angular.forEach($scope.oldBettingData, function (ite) {
                     ite.results.forEach(item=> {
                         if (item.id === id) {
                             oldFirstVal = item.spread_line.away_odd;
                         }
                     });
                 });

                 angular.forEach($scope.bettingResult, function (ite) {
                     ite.results.forEach(item=> {
                         if (item.id === id) {
                             curFirstVal = item.spread_line.away_odd;
                         }
                     });
                 });
                 $("#betSlip_"+id+"_"+type+"_decrease").hide();
                 $("#betSlip_"+id+"_"+type+"_increase").hide();
                 if (oldFirstVal > curFirstVal) {
                     $('#spread_away'+id).css('color', 'red');
                     $('#spread_away'+id+'_decrease').show();
                     $("#betSlip_"+id+"_"+type+"_decrease").show();
                     $scope.betChanges(id,type);
                 }
                 else if (oldFirstVal < curFirstVal) {
                     $('#spread_away'+id).css('color', 'green');
                     $('#spread_away'+id+'_increase').show();
                     $("#betSlip_"+id+"_"+type+"_increase").show();
                     $scope.betChanges(id,type);
                 }
             }
             else if (type === 'total_home') {
                 $scope.selectedData.forEach((item) => {
                     if (item.id === id && item.type === type) {
                         $('#total_home'+id).addClass('active');
                         selected = true;
                     }
                 });

                 if (!selected) $('#total_home'+id).removeClass('active');

                 angular.forEach($scope.oldBettingData, function (ite) {
                     ite.results.forEach(item=> {
                         if (item.id === id) {
                             oldFirstVal = item.over.home_odd;
                         }
                     });
                 });

                 angular.forEach($scope.bettingResult, function (ite) {
                     ite.results.forEach(item=> {
                         if (item.id === id) {
                             curFirstVal = item.over.home_odd;
                         }
                     });
                 });
                 $("#betSlip_"+id+"_"+type+"_decrease").hide();
                 $("#betSlip_"+id+"_"+type+"_increase").hide();
                 if (oldFirstVal > curFirstVal) {
                     $('#total_home'+id).css('color', 'red');
                     $('#total_home'+id+'_decrease').show();
                     $("#betSlip_"+id+"_"+type+"_decrease").show();
                     $scope.betChanges(id,type);
                 }
                 else if (oldFirstVal < curFirstVal) {
                     $('#total_home'+id).css('color', 'green');
                     $('#total_home'+id+'_increase').show();
                     $("#betSlip_"+id+"_"+type+"_increase").show();
                     $scope.betChanges(id,type);
                 }
             }
             else if (type === 'total_away') {
                 $scope.selectedData.forEach((item) => {
                     if (item.id === id && item.type === type) {
                         $('#total_away'+id).addClass('active');
                         selected = true;
                     }
                 });

                 if (!selected) $('#total_away'+id).removeClass('active');

                 angular.forEach($scope.oldBettingData, function (ite) {
                     ite.results.forEach(item=> {
                         if (item.id === id) {
                             oldFirstVal = item.under.away_odd;
                         }
                     });
                 });

                 angular.forEach($scope.bettingResult, function (ite) {
                     ite.results.forEach(item=> {
                         if (item.id === id) {
                             curFirstVal = item.under.away_odd;
                         }
                     });
                 });
                 $("#betSlip_"+id+"_"+type+"_decrease").hide();
                 $("#betSlip_"+id+"_"+type+"_increase").hide();
                 if (oldFirstVal > curFirstVal) {
                     $('#total_away'+id).css('color', 'red');
                     $('#total_away'+id+'_decrease').show();
                     $("#betSlip_"+id+"_"+type+"_decrease").show();
                     $scope.betChanges(id,type);
                 }
                 else if (oldFirstVal < curFirstVal) {
                     $('#total_away'+id).css('color', 'green');
                     $('#total_away'+id+'_increase').show();
                     $("#betSlip_"+id+"_"+type+"_increase").show();
                     $scope.betChanges(id,type);
                 }
             }
             else if (type === 'money_home') {
                 $scope.selectedData.forEach((item) => {
                     if (item.id === id && item.type === type) {
                         $('#money_home'+id).addClass('active');
                         selected = true;
                     }
                 });

                 if (!selected) $('#money_home'+id).removeClass('active');

                 angular.forEach($scope.oldBettingData, function (ite) {
                     ite.results.forEach(item=> {
                         if (item.id === id) {
                             oldFirstVal = item.money_line.home_odd;
                         }
                     });
                 });

                 angular.forEach($scope.bettingResult, function (ite) {
                     ite.results.forEach(item=> {
                         if (item.id === id) {
                             curFirstVal = item.money_line.home_odd;
                         }
                     });
                 });
                 $("#betSlip_"+id+"_"+type+"_decrease").hide();
                 $("#betSlip_"+id+"_"+type+"_increase").hide();
                 if (oldFirstVal > curFirstVal) {
                     $('#money_home'+id).css('color', 'red');
                     $('#money_home'+id+'_decrease').show();
                     $("#betSlip_"+id+"_"+type+"_decrease").show();
                     $scope.betChanges(id,type);
                 }
                 else if (oldFirstVal < curFirstVal) {
                     $('#money_home'+id).css('color', 'green');
                     $('#money_home'+id+'_increase').show();
                     $("#betSlip_"+id+"_"+type+"_increase").show();
                     $scope.betChanges(id,type);
                 }
             }
             else if (type === 'money_away') {
                 $scope.selectedData.forEach((item) => {
                     if (item.id === id && item.type === type) {
                         $('#money_away'+id).addClass('active');
                         selected = true;
                     }
                 });

                 if (!selected) $('#money_away'+id).removeClass('active');

                 angular.forEach($scope.oldBettingData, function (ite) {
                     ite.results.forEach(item=> {
                         if (item.id === id) {
                             oldFirstVal = item.money_line.away_odd;
                         }
                     });
                 });

                 angular.forEach($scope.bettingResult, function (ite) {
                     ite.results.forEach(item=> {
                         if (item.id === id) {
                             curFirstVal = item.money_line.away_odd;
                         }
                     });
                 });
                 $("#betSlip_"+id+"_"+type+"_decrease").hide();
                 $("#betSlip_"+id+"_"+type+"_increase").hide();
                 if (oldFirstVal > curFirstVal) {
                     $('#money_away'+id).css('color', 'red');
                     $('#money_away'+id+'_decrease').show();
                     $("#betSlip_"+id+"_"+type+"_decrease").show();
                     $scope.betChanges(id,type);
                 }
                 else if (oldFirstVal < curFirstVal) {
                     $('#money_away'+id).css('color', 'green');
                     $('#money_away'+id+'_increase').show();
                     $("#betSlip_"+id+"_"+type+"_increase").show();
                     $scope.betChanges(id,type);
                 }
             }
         };

         // Add To Cart ( BET Slip function )
         $scope.addtocart = function (id, type){
             let selected = false;
             let result = [];

             $scope.selectedData.forEach((item) => {
                 if (item.id === id && item.type === type) {
                     $scope.selectedData.splice($scope.selectedData.indexOf(item), 1);
                     $('#'+type+id).removeClass('active');
                     selected = true;
                 }
             });

             if (!selected) {

                 $scope.bettingResult.forEach((dt) => {
                     dt.results.forEach(data=>{
                         if (data.id === id) result = data;
                     })
                 });

                 let date = new Date(result.time*1000);

                 let betObject = {
                     id: id,
                     type: type,
                     win: 0,
                     risk: 0,
                     score: 0,
                     teamName: '',
                     displayName: '',
                     wager: '',
                     isHome: null,
                     isAway: null,
                     spread_handicap : null,
                     teamHomeName: result.home.name,
                     teamAwayName: result.away.name,
                     dateTime: date.toLocaleString(),
                     event_date: result.time,
                     bet_extra_info:{},
                 };
                 let slug;
                 switch (type) {
                     case 'spread_home':
                         betObject.isHome = 1;
                         betObject.teamName = result.home.name;
                         betObject.displayName = result.home.name;
                         betObject.wager = 'Spread';
                         betObject.score = result.spread_line.home_odd;
                         betObject.spread_handicap = result.spread_line.handicap;
                         slug = "home";
                         break;
                     case 'spread_away':
                         betObject.isAway = 1;
                         betObject.teamName = result.away.name;
                         betObject.displayName = result.away.name;
                         betObject.wager = 'Spread';
                         betObject.score = result.spread_line.away_odd;
                         betObject.spread_handicap = result.spread_line.handicap;
                         slug = "away";
                         break;
                     case 'total_home':
                         betObject.isHome = 1;
                         betObject.teamName = result.home.name;
                         betObject.displayName = 'Over '+result.over_under.handicap;
                         betObject.wager = 'Total';
                         betObject.score = result.over.home_odd;
                         slug = "home";
                         break;
                     case 'total_away':
                         betObject.isAway = 1;
                         betObject.teamName = result.away.name;
                         betObject.displayName = 'Under '+result.over_under.handicap;
                         betObject.wager = 'Total';
                         betObject.score = result.over.home_odd;
                         slug = "away";
                         break;
                     case 'money_home':
                         betObject.isHome = 1;
                         betObject.teamName = result.home.name;
                         betObject.displayName = result.home.name;
                         betObject.wager = 'Money Line';
                         betObject.score = result.money_line.home_odd;
                         slug = "home";
                         break;
                     case 'money_away':
                         betObject.isAway = 1;
                         betObject.teamName = result.away.name;
                         betObject.displayName = result.away.name;
                         betObject.wager = 'Money Line';
                         betObject.score = result.money_line.away_odd;
                         slug = "away";
                         break;
                 }
                 betObject.bet_extra_info = {
                     bet_on_team_name : betObject.teamName,
                     other_team_name  : (betObject.teamName === result.home.name) ? result.away.name : result.home.name,
                     betting_slug     : slug,
                     betting_wager    : betObject.wager
                 };
                 $scope.slipOddTotal += betObject.score;
                 $scope.selectedData.push(betObject);
                 $('#'+type+id).addClass('active');
             }
             calculateReport();
         };

         // Remove Card Item
         $scope.removeCardItem = function (id, type) {
             $scope.selectedData.forEach((item) => {
                 if (item.id === id && item.type === type) {
                     $scope.selectedData.splice($scope.selectedData.indexOf(item), 1);
                     $('#'+type+id).removeClass('active')
                 }
             });
             calculateReport();
         };
         $scope.clearBetSlip = function(){

             if ($scope.selectedData.length === 0)
             {
                 alert("You haven't selected a slip yet");
             }
             else
             {
                 $scope.selectedData.forEach(function (item) {
                     $('#'+item.type+item.id).removeClass('active');
                 });
                 $scope.selectedData = [];
                 calculateReport();
             }

         };

         $scope.betChanges = function (id, type, model) {
             $scope.selectedData.forEach((item) => {
                 if (item.id === id && item.type === type) {
                     if (model === 'risk') {
                         if (item.score >= 0) item.win = Math.round(item.score*item.risk)/100;
                         else item.win = Math.round((10000 / item.score)*item.risk)* (-1) / 100;
                     } else {
                         if (item.score >= 0) item.risk = Math.round((10000 / item.score)*item.win)/100;
                         else item.risk = Math.round(item.win *item.score)* (-1) / 100;
                     }
                 }
             });
             calculateReport();
         };

         function calculateReport() {
             $scope.possibleWin = 0;
             $scope.totalStake = 0;
             $scope.selectedData.forEach((item) => {
                 $scope.possibleWin += parseFloat(item.win);
                 $scope.totalStake += parseFloat(item.risk);
             })
         }

         $scope.confirmBet = function () {
             if ($scope.selectedData.length != 0)
             {
                 $scope.changeNow = true;
                 $("#confirm_passwordFootball").removeClass('d-none');

                 $("#submit_confirm_passwordFootball").off().click(function (e) {
                     let pass = $("#bet_confirm_passwordFootball").val();
                     if (pass.length === 0)
                     {
                         alert("password fields can't be empty")
                     }
                     else
                     {
                         $.ajax({
                            url: "{{url('/check-user-password')}}",
                            type: "POST",
                            data: {
                             "_token": $('meta[name="csrf-token"]').attr('content'),
                            pass: pass
                             },
                             success:function(response)
                             {
                                 if (response.status)
                                 {
                                     $("#confirm_passwordFootball").addClass('d-none');
                                     if (!$scope.isChanged)
                                     {

                                         $scope.selectedData.forEach((item) => {
                                             $scope.bettingResult.forEach((ad) => {
                                                 ad.results.forEach(apiData=>{
                                                     if (item.id === apiData.id) {
                                                         let carditem = {};
                                                         carditem.sport_league = JSON.stringify(['12', item.teamName]);
                                                         carditem.is_away            = item.isAway;
                                                         carditem.is_home            = item.isHome;
                                                         carditem.o_and_u            = '';
                                                         carditem.teamname           = item.teamName;
                                                         carditem.team_id            = item.teamName === apiData.home.name? apiData.home.id: apiData.away.id;
                                                         carditem.even_id            = item.id;
                                                         carditem.scores             = item.score;
                                                         carditem.event_date         = item.event_date;
                                                         carditem.sport_id = '12';
                                                         carditem.original_money     = item.spread_handicap !== null ? "("+item.spread_handicap+") "+item.score : item.score;
                                                         carditem.risk_stake_slip    = parseFloat(item.risk);
                                                         carditem.risk_win_slip      = parseFloat(item.win);
                                                         carditem.bet_type           = 'live';
                                                         carditem.bet_extra_info = JSON.stringify(item.bet_extra_info);
                                                         $scope.confirmData.push(carditem);
                                                     }
                                                 });

                                             });
                                         });
                                         $.ajax({
                                             url: " {{url('/save-live-data')}}",
                                            type: "POST",
                                            data: {
                                            "_token": $('meta[name="csrf-token"]').attr('content'),
                                                 items :  $scope.confirmData,
                                                 status: "oka"
                                             },
                                             success: function(resp){
                                                 //let resp = JSON.parse(res);

                                                 if (resp.status) {
                                                     $scope.isChanged = false;
                                                     $scope.slipOddTotal = 0;
                                                     $scope.changeNow = false;

                                                     $('#header_available_balance').html(resp.user.available_balance);
                                                     $('#header_pending_amount').html(resp.user.pending_amount);
                                                     $scope.selectedData.forEach((item) => {
                                                        // $scope.selectedData.splice($scope.selectedData.indexOf(item), 1);
                                                         $('#'+item.type+item.id).removeClass('active');
                                                     });

                                                     $scope.confirmData  = [];
                                                     $scope.selectedData = [];

                                                     $scope.totalStake = 0;
                                                     $scope.possibleWin = 0;

                                                     // $scope.confirmSuccess = true;
                                                     $("#confirmSuccess").show();
                                                     setTimeout(function () {
                                                         // $scope.confirmSuccess = false;
                                                         $("#confirmSuccess").hide();
                                                     }, 3000);
                                                 } else {
                                                     $scope.confirmFail = true;
                                                     setTimeout(function () {
                                                         $scope.confirmFail = false;
                                                     }, 5000);
                                                 }
                                             }
                                         });
                                     }
                                     else
                                     {
                                         $("#bet_noticeFootball").removeClass('d-none');
                                         $scope.isChanged = false;
                                         $scope.changeNow = false;
                                     }
                                 }
                                 else
                                 {
                                     $("#mesageFootball").text("password didn't match, try again !");
                                 }
                             }
                         })
                     }
                 });



             }
             else
             {
                 alert("No Item is selected");
             }
         }

         $scope.passwordCloseIcon =  function(){
             $(".popup-box").addClass("d-none");
         }
     });
     angular.bootstrap(document.getElementById("realTimeBetting3cvc"), ['realTimeBetting3cvc']);

     $(document).ready(function () {
         $(".abul").css('opacity',"1");
         $("#sidebar-content").css('opacity',"1");
     });

   console.groupEnd();

</script>
@endsection
