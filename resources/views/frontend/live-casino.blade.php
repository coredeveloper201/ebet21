@extends('layouts.frontend_layout')

@section('title')

@endsection

@section('content')

<!-- main body area start -->
<section id="body_warapper">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="rules_list">
                    <h2 style="text-align: center"> <span><i class="fas fa-exclamation-circle"></i></span> Feature Disabled</h2>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- main body area start -->
@endsection
