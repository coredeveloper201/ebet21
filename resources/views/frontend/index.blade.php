@extends('layouts.frontend_layout')

@section('title')

@endsection

@section('content')

<form action="{{url('straight_odds')}}" method="POST">
    @csrf
    <!-- Upcoming Events area start -->
    <div style="margin-top: 7px" class="container">
        <section class="col-12  padding-0" id="upcoming_events">
            @if(Session::has('notice'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <strong>Warning !</strong> {{ Session::get('notice')}}.
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif

            <div class="row">
                <div class="col-12 col-md-6 padding-0">
                    <div class="upcoming_title">
                        <h2>UPCOMING EVENTS <span class="ue2"> - {{_getDate(strtotime(now()))}}</span></h2>
                        <p>Straights <span class="dot"></span> Parlays <span class="dot"></span> Props <span class="dot"></span> Teasers <span class="dot"></span> If/Reverses <span class="dot"></span> Round Robins</p>

                    </div>
                </div>
                <div class="col-12 col-md-6  padding-0">
                    <div class="go_conintue">
                        <button class="btn btn-cont" name="continu_straight_bet" type="submit" >Continue</button>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <!-- Upcoming Events area end -->
    <section id="body_warapper">
        <div class="container">
            <div class="row">
                <!-- Game catagory list start -->
                <div class="col-lg-4 col-xs-12 padding-0-index padding-0-index-left">

                    @if(!empty($data['first_column']))
                        @foreach ($data['first_column'] as $sport_key => $item)

                            @if (getWagerDisableCheck($sport_key) != null)
                                <div class="single_game_catagory">
                                        <div class="game_name">
                                            <img src="{{ asset('assets').$data['images'][$sport_key] }}" alt="">
                                            <h3> {{ $item['game_name'] }} </h3>
                                            <span class="collapse-icon" data-toggle="collapse" data-target="#game-list{{$sport_key}}" onclick="plugin.changeIcon(this)">
                                                <i class="fa fa-minus clp-target font-14"></i>
                                                <i class="fa fa-plus clp-target font-14" style="display: none;"></i>
                                            </span>
                                        </div>

                                        @php
                                        $sports_leagueF = [];
                                        $u_sport_leagueF = [];
                                        $sport_leagueF = [];

                                        if (!empty($item['game_data'])) {
                                            $sports_leagueF = $item['game_data'];
                                            $u_sport_leagueF = array_map("unserialize", array_unique(array_map("serialize", $sports_leagueF)));
                                            asort($u_sport_leagueF);
                                        $sport_leagueF = _usaSort($u_sport_leagueF);
                                        }
                                        @endphp

                                        @if(!empty($sport_leagueF))
                                            <div id="game-list{{$sport_key}}" class="collapse">
                                                @foreach ($sport_leagueF as $key => $val)
                                                    <div class="game_play_list">
                                                        @if (getWagerDisableCheck($sport_key.'_'.$key) != null)
                                                        <div class="checkbox myboxs">
                                                            <label>
                                                                <input type="checkbox" name="league_ids[{{$sport_key}}][{{$key}}][]" style="border:0;">
                                                                <span class="checkbox-material"><span class="check"></span></span> {{$val}}
                                                            </label>
                                                        </div>
                                                        @endif

                                                    </div>
                                                @endforeach
                                            </div>
                                        @else
                                            <div class="text-center">
                                                <span class="no_event">No Events</span>
                                            </div>
                                        @endif
                                </div>
                            @endif
                        @endforeach
                    @endif
                </div>
                <!-- Game catagory list end -->

            <!-- Game catagory list start -->
                <div class="col-lg-4 col-xs-12 padding-0-index">
                    @if(!empty($data['second_column']))

                        @foreach ($data['second_column'] as $sport_key => $itemSecond)
                            @if (getWagerDisableCheck($sport_key)!=null)
                                <div class="single_game_catagory">
                                    <div class="game_name">
                                        <img src="{{ asset('assets').$data['images'][$sport_key] }}" alt="">
                                        <h3>{{ $itemSecond['game_name'] }}</h3>
                                        <span class="collapse-icon" data-toggle="collapse" data-target="#game-list{{$sport_key}}" onclick="plugin.changeIcon(this)">
                                            <i class="fa fa-minus clp-target font-14"></i>
                                            <i class="fa fa-plus clp-target font-14" style="display: none;"></i>
                                        </span>
                                    </div>

                                    @php
                                        $sports_leagueD = [];
                                        $u_sport_leagueD = [];
                                        $sport_leagueS = [];

                                        if (!empty($itemSecond['game_data'])) {
                                            $sports_leagueD = $itemSecond['game_data'];
                                            $u_sport_leagueD = array_map("unserialize", array_unique(array_map("serialize", $sports_leagueD)));
                                            asort($u_sport_leagueD);
                                        $sport_leagueS = _usaSort($u_sport_leagueD);
                                        }
                                    @endphp
                                    @if(!empty($sport_leagueS))
                                        <div id="game-list{{$sport_key}}" class="collapse">
                                            @foreach ($sport_leagueS as $key => $val)
                                                @if (getWagerDisableCheck($sport_key.'_'.$key)!=null)
                                                    <div class="game_play_list">
                                                        <div class="checkbox myboxs">
                                                            <label>
                                                                <input type="checkbox" name="league_ids[{{$sport_key}}][{{$key}}][]" style="border:0;">
                                                                <span class="checkbox-material"><span class="check"></span></span> {{$val}}
                                                            </label>
                                                        </div>
                                                    </div>
                                                @endif
                                            @endforeach
                                        </div>
                                    @else
                                        <div class="text-center">
                                            <span class="no_event">No Events</span>
                                        </div>
                                    @endif
                                </div>
                            @endif
                        @endforeach
                    @endif
                </div>


                <!-- Game catagory list start -->
                  <!-- Game catagory list start -->
                  <div class="col-lg-4 col-xs-12 padding-0-index padding-0-index-right">
                        @if(!empty($data['third_column']))
                            @foreach ($data['third_column'] as $sport_key => $third_column)
                                @if (getWagerDisableCheck($sport_key)!=null)
                                    <div class="single_game_catagory">
                                            <div class="game_name">
                                                <img src="{{ asset('assets').$data['images'][$sport_key] }}" alt="">
                                                <h3>{{ $third_column['game_name'] }}</h3>
                                                <span class="collapse-icon" data-toggle="collapse" data-target="#game-list{{$sport_key}}" onclick="plugin.changeIcon(this)">
                                                    <i class="fa fa-minus clp-target font-14"></i>
                                                    <i class="fa fa-plus clp-target font-14" style="display: none;"></i>
                                                </span>
                                            </div>
                                            @php
                                                $sports_leagueT = [];
                                                $u_sport_leagueT = [];
                                                $sport_league = [];
                                                if (!empty($third_column['game_data'])) {
                                                    if ($sport_key == 18 || $sport_key == 1) {
                                                        $sports_leagueT = $third_column['game_data'];
                                                    }
                                                    else {
                                                        if (isset($third_column['game_data']['results'])) {
                                                            foreach ($third_column['game_data']['results'] as $k => $v) {
                                                            $sports_leagueT[$v['Id']] = $v['LeagueName'];
                                                            }
                                                        }
                                                    }

                                                    $u_sport_leagueT = array_map("unserialize", array_unique(array_map("serialize", $sports_leagueT)));
                                                    asort($u_sport_leagueT);
                                                    $sport_league = _usaSort($u_sport_leagueT);
                                                }
                                            @endphp
                                            @if(!empty($sport_league))
                                                <div id="game-list{{$sport_key}}" class="collapse">
                                                    @foreach ($sport_league as $key => $val)
                                                        <div class="game_play_list">
                                                            @if (getWagerDisableCheck($sport_key.'_'.$key)!=null)
                                                                <div class="checkbox myboxs">
                                                                    <label>
                                                                        <input type="checkbox" name="league_ids[{{$sport_key}}][{{$key}}][]" style="border:0;">
                                                                        <span class="checkbox-material"><span class="check"></span></span> {{$val}}
                                                                    </label>
                                                                </div>
                                                            @endif
                                                        </div>
                                                    @endforeach
                                                </div>
                                            @else
                                                <div class="text-center">
                                                    <span class="no_event">No Events</span>
                                                </div>
                                            @endif
                                    </div>
                                @endif
                            @endforeach
                        @endif
                    </div>
            </div>
        </div>
    </section>
</form>

@endsection
