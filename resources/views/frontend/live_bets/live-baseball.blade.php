<div ng-controller="realTimeBettingController5e">
        <div class="popup-box d-none" id="confirm_passwordBaseball">
            <div class="popup-box-body">
                <div class="close-icon"><span data-ng-click="passwordCloseIcon()">X</span></div>
                <div><h1>Enter Your Password</h1> </div>
                <div>
                    <input type="password" class="form-control" id="bet_confirm_passwordBaseball">
                    <p id="mesage_Baseball" style="color: red"></p>
                </div>
                <div> <button class="btn" id="submit_confirm_passwordBaseball">Submit</button></div>
            </div>
        </div>
        <div class="popup-box d-none" id="bet_notice_Baseball">
            <div class="popup-box-body">
                <div class="close-icon"> <span data-ng-click="passwordCloseIcon()">X</span> </div>
                <div>
                    <h4>Bets data has been changed, please review your bets</h4>
                    <p>when review is done, hit the confirm button again</p>
                </div>
                <div><button class="btn" id="Okaybutton" data-ng-click="passwordCloseIcon()">Okay</button> </div>
            </div>
        </div>
        <div class="row">
           <div class="col-md-9 col-lg-9 col-xl-9 padding-0">
              <article id="myaccount" class="col span_12">
                <div class="Live-bet-event-content">
                    <div class="Live-bet-event-content-each" ng-repeat="league in bettingResult">
                        <div class="Live-bet-league-header">
                            <div class="_league_header_title">@{{league.even_name}}</div>
                            <div class="mobile-w-375">
                                <div class="_league_header_spread">Run <span>Line</span></div>
                                <div class="_league_header_mline">Money <span>Line</span> </div>
                                <div class="_league_header_total">Total</div>
                            </div>
                        </div>
                        <div class="Live-bet-league-each" ng-repeat="item in league.results track by $index">
                            <div class="Live-bet-event-each">
                                <div class="_event_sections_mobile">
                                    Inning @{{ item.no_of_innings }}
                                </div>
                                <div class="_league_header_title">
                                    <div class="_event_sections">
                                        Inning  @{{ item.no_of_innings }}
                                    </div>
                                    <div class="_event_titles">
                                        <ul>
                                            <li>
                                                <span class="spn-title">@{{item.home.name}}</span>
                                                <span class="spn-values no-letter-spacing">
                                                    <span ng-repeat="val in item.ss.home track by $index">@{{ val }}</span>
                                                </span>
                                            </li>
                                            <li>
                                                <span class="spn-title">@{{item.away.name}}</span>
                                                <span class="spn-values no-letter-spacing">
                                                    <span ng-repeat="val in item.ss.away track by $index">@{{ val }}</span>
                                                </span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="_league_header_spread">
                                    <ul>
                                    <li ng-if="item.odd.first" ng-style="compareODD(item.id, 'odd_first')"
                                        id="odd_first@{{item.id}}" data-ng-click="addtocart(item.id, 'odd_first')">
                                        <i class="fa fa-arrow-down" id="odd_first@{{item.id}}_decrease" style="font-size:12px; color:red; display: none; margin-right: 5px"></i>
                                        <i class="fa fa-arrow-up" id="odd_first@{{item.id}}_increase" style="font-size:12px; color:green; display: none; margin-right: 5px"></i>
                                        <span class="mobile-w-100">
                                        <span class="mobile-w-100" style="color: #929292">
                                            <span ng-if="item.odd.first_val">@{{ item.odd.first_val }}</span>
                                        </span>
                                        <span class="mobile-w-100">
                                        <span>(</span><span ng-if="item.odd.first > 0" style="margin-right: -2px">+</span>
                                        @{{ item.odd.first }})
                                        </span>
                                        </span>
                                    </li>
                                    <li ng-if="!item.odd.first" class="no-event">OTB</li>
                                    <li ng-if="item.odd.second"
                                        ng-style="compareODD(item.id, 'odd_second')"
                                        id="odd_second@{{item.id}}"
                                        data-ng-click="addtocart(item.id, 'odd_second')">
                                        <i class="fa fa-arrow-down" id="odd_second@{{item.id}}_decrease" style="font-size:12px; color:red; display: none; margin-right: 5px"></i>
                                        <i class="fa fa-arrow-up" id="odd_second@{{item.id}}_increase" style="font-size:12px; color:green; display: none; margin-right: 5px"></i>
                                        <span class="mobile-w-100">
                                        <span class="mobile-w-100" style="color: #929292">

                                        @{{ item.odd.first_val.includes("-")
                                        ?  item.odd.second_val.replace("-","+")
                                        : item.odd.second_val.replace("+","-") }}</span>
                                        <span class="mobile-w-100">

                                        <span>(</span><span ng-if="item.odd.second > 0" style="margin-right: -2px">+</span>
                                        @{{item.odd.second}})
                                        </span>
                                        </span>
                                    </li>
                                    <li ng-if="!item.odd.second" class="no-event">OTB</li>
                                    </ul>
                                </div>
                                <div class="_league_header_mline">
                                    <ul>
                                    <li  ng-if="item.match.over.odd"
                                        ng-style="compareODD(item.id, 'match_over')"
                                        id="match_over@{{item.id}}" data-ng-click="addtocart(item.id, 'match_over')">
                                        <span class="mobile-w-100">
                                            <i class="fa fa-arrow-down" id="match_over@{{item.id}}_decrease" style="font-size:12px; color:red; display: none; margin-right: 5px"></i>
                                            <i class="fa fa-arrow-up" id="match_over@{{item.id}}_increase" style="font-size:12px; color:green; display: none; margin-right: 5px"></i>
                                            <span ng-if="item.match.over.odd > 0" style="margin-right: -2px">+</span> @{{ item.match.over.odd }}
                                        </span>
                                    </li>
                                    <li class="no-event" ng-if="!item.match.over.odd">OTB</li>
                                    <li ng-if="item.match.under.odd"
                                        ng-style="compareODD(item.id, 'match_under')"
                                        id="match_under@{{item.id}}" data-ng-click="addtocart(item.id, 'match_under')">
                                        <span class="mobile-w-100">
                                            <i class="fa fa-arrow-down" id="match_under@{{item.id}}_decrease" style="font-size:12px; color:red; display: none; margin-right: 5px"></i>
                                            <i class="fa fa-arrow-up" id="match_under@{{item.id}}_increase" style="font-size:12px; color:green; display: none; margin-right: 5px"></i>
                                            <span ng-if="item.match.under.odd > 0" style="margin-right: -2px">+</span> @{{ item.match.under.odd }}
                                        </span>
                                    </li>
                                    <li class="no-event" ng-if="!item.match.under.odd">OTB</li>
                                    </ul>
                                </div>
                                <div class="_league_header_total">
                                    <ul>
                                        <li ng-if="item.next.first"
                                            ng-style="compareODD(item.id, 'next_first')"
                                            id="next_first@{{item.id}}" data-ng-click="addtocart(item.id, 'next_first')">
                                            <i class="fa fa-arrow-down" id="next_first@{{item.id}}_decrease" style="font-size:12px; color:red; display: none; margin-right: 5px"></i>
                                            <i class="fa fa-arrow-up" id="next_first@{{item.id}}_increase" style="font-size:12px; color:green; display: none; margin-right: 5px"></i>
                                            <span class="mobile-w-100">
                                                <span class="mobile-w-100" style="color: #929292">
                                                    @{{ item.next.first_val }}
                                                </span>
                                                <span class="mobile-w-100">

                                                    <span>(</span><span ng-if="item.next.first > 0" style="margin-right: -2px">+</span>
                                                    @{{ item.next.first }})
                                                </span>
                                            </span>
                                        </li>
                                        <li class="no-event" ng-if="!item.next.first">OTB</li>
                                        <li ng-if="item.next.second"
                                            ng-style="compareODD(item.id, 'next_second')"
                                            id="next_second@{{item.id}}" data-ng-click="addtocart(item.id, 'next_second')">
                                            <i class="fa fa-arrow-down" id="next_second@{{item.id}}_decrease" style="font-size:12px; color:red; display: none; margin-right: 5px"></i>
                                            <i class="fa fa-arrow-up" id="next_second@{{item.id}}_increase" style="font-size:12px; color:green; display: none; margin-right: 5px"></i>
                                            <span class="mobile-w-100">
                                                <span class="mobile-w-100" style="color: #929292">
                                                    @{{ item.next.second_val }}
                                                </span>
                                                <span class="mobile-w-100">
                                                    <span>(</span><span ng-if="item.next.second > 0" style="margin-right: -2px">+</span> @{{ item.next.second }})
                                                </span>
                                            </span>
                                        </li>
                                        <li class="mediumtd" ng-if="!item.next.second">OTB</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
              </article>
              <div ng-if="loading" class="preloading">
                 <img src="assets/img/live/spinner.gif" alt="loading..."/>
              </div>
           </div>
           <div class="col-md-3 col-lg-3 col-xl-3 padding-0">
              <div  class="bg-overlay" id="popup-optin-baseball">
                 <div class="subscribe-optin" style="margin-top: 8px;">
                    <a href="#" class="optin-close">&times;</a>
                    @php
                    $slip = [
                        'name' => 'baseball'
                     ];
                @endphp
                @include('frontend.live_bets.live-slip',$slip)
                 </div>
              </div>
              <a href="#popup-optin-baseball" class="popup-live">SLIP <span id="badge_count">@{{selectedData.length}}</span></a>
           </div>
        </div>
     </div>
