<div ng-controller="realTimeBettingController7g">
    <div class="popup-box d-none" id="confirm_passwordTennis">
       <div class="popup-box-body">
          <div class="close-icon">
             <span data-ng-click="passwordCloseIcon()">X</span>
          </div>
          <div>
             <h1>Enter Your Password</h1>
          </div>
          <div>
             <input type="password" class="form-control" id="bet_confirm_passwordTennis">
             <p id="mesageTennis" style="color: red"></p>
          </div>
          <div>
             <button class="btn" id="submit_confirm_passwordTennis" data-ng-click="ConfirmPassBetSlip()">Submit</button>
          </div>
       </div>
    </div>
    <div class="popup-box d-none" id="bet_notice_tennis">
       <div class="popup-box-body">
          <div class="close-icon">
             <span data-ng-click="passwordCloseIcon()">X</span>
          </div>
          <div>
             <h4>Bets data has been changed, please review your bets</h4>
             <p>when review is done, hit the confirm button again</p>
          </div>
          <div>
             <button class="btn" id="Okaybutton" data-ng-click="passwordCloseIcon()">Okay</button>
          </div>
       </div>
    </div>
    <div class="row">
       <div class="col-lg-9 col-xl-9 col-md-9 col-sm-12 col-xs-12 padding-0">
          <article id="myaccount">
             <div class="Live-bet-event-content">
                <div class="Live-bet-event-content-each" ng-repeat="league in bettingResult">
                   <div class="Live-bet-league-header">
                      <div class="_league_header_title">@{{league.even_name}}</div>
                      <div class="_league_header_spread">Winner</div>
                      <div class="_league_header_mline">Current SET</div>
                      <div class="_league_header_mline">Next SET</div>
                   </div>
                   <div class="Live-bet-league-each" ng-repeat="item in league.results">
                      <div class="Live-bet-event-each">
                         <div class="_event_sections_mobile">@{{ item.period}}</div>
                         <div class="_league_header_title">
                            <div class="_event_sections">@{{ item.period}}</div>
                            <div class="_event_titles">
                               <ul>
                                  <li>
                                     <span class="spn-title">@{{item.home.name}}</span>
                                     <span class="spn-values">
                                     <span ng-repeat="val in item.ss.home track by $index">@{{ val }}</span>
                                     </span>
                                  </li>
                                  <li>
                                     <span class="spn-title">@{{item.away.name}}</span>
                                     <span class="spn-values">
                                     <span ng-repeat="val in item.ss.away track by $index">@{{ val }}</span>
                                     </span>
                                  </li>
                               </ul>
                            </div>
                         </div>
                         <div class="_league_header_spread">
                            <ul>
                               <li ng-if="item.winner_set.home_odd" ng-style="compareODD(item.id, 'odd_first')"
                                  id="odd_first@{{item.id}}" data-ng-click="addtocart(item.id, 'odd_first')">
                                  <span class="mobile-w-100">
                                  <span class="mobile-w-100" style="padding-right: 5px; color: #929292"></span>
                                  <span class="mobile-w-100">
                                  <i class="fa fa-arrow-down" id="first_@{{item.id}}_decrease" style="font-size:12px; color:red; display: none; margin-right: 5px"></i>
                                  <i class="fa fa-arrow-up" id="first_@{{item.id}}_increase" style="font-size:12px; color:green; display: none; margin-right: 5px"></i>
                                  <span ng-if="item.winner_set.home_odd > 0" style="margin-right: -2px">+ </span>
                                  @{{ item.winner_set.home_odd}}
                                  </span>
                                  </span>
                               </li>
                               <li ng-if="!item.winner_set.home_odd" class="no-event">OTB</li>
                               <li ng-if="item.winner_set.away_odd"
                                  ng-style="compareODD(item.id, 'odd_second')"
                                  id="odd_second@{{item.id}}"
                                  data-ng-click="addtocart(item.id, 'odd_second')">
                                  <span class="mobile-w-100">
                                  <span class="mobile-w-100" style="padding-right: 5px; color: #929292"></span>
                                  <span class="mobile-w-100">
                                  <i class="fa fa-arrow-down" id="second_@{{item.id}}_decrease" style="font-size:12px; color:red; display: none; margin-right: 5px"></i>
                                  <i class="fa fa-arrow-up" id="second_@{{item.id}}_increase" style="font-size:12px; color:green; display: none; margin-right: 5px"></i>
                                  <span ng-if="item.winner_set.away_odd > 0" style="margin-right: -2px">+ </span> @{{ item.winner_set.away_odd }}
                                  </span>
                                  </span>
                               </li>
                               <li ng-if="!item.winner_set.away_odd" class="no-event">OTB</li>
                            </ul>
                         </div>
                         <div class="_league_header_mline">
                            <ul>
                               <li  ng-if="item.current_set.home_odd" ng-style="compareODD(item.id, 'cur_first')" id="cur_first@{{item.id}}" data-ng-click="addtocart(item.id, 'cur_first')">
                                  <span class="mobile-w-100">
                                  <i class="fa fa-arrow-down" id="third_@{{item.id}}_decrease" style="font-size:12px; color:red; display: none; margin-right: 5px"></i>
                                  <i class="fa fa-arrow-up" id="third_@{{item.id}}_increase"style="font-size:12px; color:green; display: none; margin-right: 5px"></i>
                                  <span ng-if="item.current_set.home_odd > 0" style="margin-right: -2px">+ </span>
                                  @{{ item.current_set.home_odd }}
                                  </span>
                               </li>
                               <li class="no-event" ng-if="!item.current_set.home_odd">OTB</li>
                               <li ng-if="item.current_set.away_odd"
                                  ng-style="compareODD(item.id, 'cur_second')"
                                  id="cur_second@{{item.id}}" data-ng-click="addtocart(item.id, 'cur_second')">
                                  <span class="mobile-w-100">
                                  <i class="fa fa-arrow-down" id="fourth_@{{item.id}}_decrease" style="font-size:12px; color:red; display: none; margin-right: 5px"></i>
                                  <i class="fa fa-arrow-up" id="fourth_@{{item.id}}_increase" style="font-size:12px; color:green; display: none; margin-right: 5px"></i>
                                  <span ng-if="item.current_set.away_odd > 0" style="margin-right: -2px">+ </span>
                                  @{{ item.current_set.away_odd }}
                                  </span>
                               </li>
                               <li class="no-event" ng-if="!item.current_set.away_odd">OTB</li>
                            </ul>
                         </div>
                         <div class="_league_header_total">
                            <ul>
                               <li ng-if="item.current_set_two.home_odd"
                                  ng-style="compareODD(item.id, 'next_set_home')"
                                  id="next_set_home@{{item.id}}" data-ng-click="addtocart(item.id, 'next_set_home')"
                                  >
                                  <span class="mobile-w-100">
                                  <i class="fa fa-arrow-down" id="fifth@{{item.id}}_decrease" style="font-size:12px; color:red; display: none; margin-right: 5px"></i>
                                  <i class="fa fa-arrow-up" id="fifth@{{item.id}}_increase"   style="font-size:12px; color:green; display: none; margin-right: 5px"></i>
                                  <span ng-if="item.current_set_two.home_odd > 0" style="margin-right: -2px">+ </span>
                                  @{{ item.current_set_two.home_odd }}
                                  </span>
                               </li>
                               <li class="no-event" ng-if="!item.current_set_two.home_odd">OTB</li>
                               <li
                                  ng-if="item.current_set_two.away_odd"
                                  ng-style="compareODD(item.id, 'next_set_away')"
                                  id="next_set_away@{{item.id}}" data-ng-click="addtocart(item.id, 'next_set_away')"
                                  >
                                  <span class="mobile-w-100">
                                  <i class="fa fa-arrow-down" id="sixth@{{item.id}}_decrease" style="font-size:12px; color:red; display: none; margin-right: 5px"></i>
                                  <i class="fa fa-arrow-up" id="sixth@{{item.id}}_increase"  style="font-size:12px; color:green; display: none; margin-right: 5px"></i>
                                  <span ng-if="item.current_set_two.away_odd > 0" style="margin-right: -2px">+ </span>
                                  @{{ item.current_set_two.away_odd }}
                                  </span>
                               </li>
                               <li class="no-event" ng-if="!item.current_set_two.away_odd">OTB</li>
                            </ul>
                         </div>
                      </div>
                   </div>
                </div>
             </div>
          </article>
          <div ng-if="loading" class="preloading">
             <img src="assets/img/live/spinner.gif" alt="loading..."/>
          </div>
       </div>
       <div class="col-lg-3 col-xl-3 col-md-3 padding-0">
          <div class="bg-overlay" id="popup-optin-1">
             <div class="subscribe-optin" style="margin-top: 8px;">
                <a href="#" class="optin-close">&times;</a>
                @php
                $slip = [
                'name' => 'tennis'
                ];
                @endphp
                @include('frontend.live_bets.live-slip',$slip)
             </div>
          </div>
          <a href="#popup-optin-1" class="popup-live">SLIP <span id="badge_count" >@{{selectedData.length}}</span></a>
       </div>
    </div>
 </div>
