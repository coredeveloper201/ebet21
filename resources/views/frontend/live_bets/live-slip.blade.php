<div class="selip custom-slip __ng_loaded">
        <div class="slip_cart">
            <h3> BET SLIP <span id="slip_count" >@{{ selectedData.length }}</span></h3>
        </div>
        <div class="live_bet_slip_empty"  ng-if="selectedData.length == 0">
            <h3>Bet Slip is Empty</h3>
            <p>Not sure where to start?</p>
            <a href="#">Learn how to place a bet</a>
        </div>
        <div id="dev-slip-container">
            <div class="bet_slip_add straight_bet_count" ng-if="selectedData.length > 0" ng-repeat="item in selectedData  track by $index">
                <div class="add_title">
                    <table class="bet_slip_title">
                        <tr>
                            <td><span class="tname1">@{{ item.displayName }}</span></td>
                            <td>
                                <div>
                                    <span style="float:right;color:rgba(0,0,0,0.75);width:120px">
                                        <i class="fa fa-arrow-down" id="betSlip_@{{item.id}}_@{{item.type}}_decrease" style="font-size:12px; color:red; display: none; margin-right: 5px"></i>
                                        <i class="fa fa-arrow-up" id="betSlip_@{{item.id}}_@{{item.type}}_increase"style="font-size:12px; color:green; display: none; margin-right: 5px"></i>
                                        <span ng-if="item.spread_handicap != null">@{{item.spread_handicap}}</span>
                                        <span>(</span><span ng-if="item.score > 0">+</span>@{{ item.score }})
                                        <a class="closecard" data-ng-click="removeCardItem(item.id, item.type)" style="color: #fff !important;"> &times; </a>
                                    </span>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <div class="bet_slip_details">
                        <span class="sd33">@{{ item.wager }}</span><br>
                        <span class="sd34">@{{ item.teamHomeName }} @ @{{ item.teamAwayName }}</span>
                    </div>
                    <table class="bet_slip_rat">
                        <tr>
                            <td>
                                <input type="text" class="custom-bet-input risk_stake_slip form-control"
                                    id="{{$slip['name']}}_risk_@{{ item.type }}_@{{ item.id }}"
                                    ng-model="item.risk"
                                    ng-change="betChange(item.id, item.type, '{{$slip['name']}}_risk_')"
                                    placeholder="Risk">
                            </td>
                            <td>
                                <input type="text" class="custom-bet-input risk_win_slip form-control"
                                    id="{{$slip['name']}}_win_@{{ item.type }}_@{{ item.id }}"
                                    ng-model="item.win" ng-change="betChange(item.id, item.type, '{{$slip['name']}}_win_')"
                                    placeholder="Win">
                            </td>
                        </tr>
                    </table>

                </div>
            </div>
        </div>
        <div class="confirm-success-alert" ng-if="confirmSuccess">
                Confirm Success Message
            </div>
            <div class="confirm-fail-alert" ng-if="confirmFail">
                Your Balance doesn't enough
            </div>
        <div class="bet_slip_details">
            <div class="slip_price_table">
                @if (Auth::user()->freePlay > 0)
                <div class="slip_checkbox" id="freePlayContainer">
                    <input type="checkbox" name="freeplay" value="1" id="freeplay"> Free Play <br>
                </div>
                @endif
                <table>
                    <tr>
                        <td>Total bets</td>
                        <td><span>@{{ selectedData.length }}</span></td>
                    </tr>
                    <tr>
                        <td>Total Stake</td>
                        <td><span>@{{ totalStake }}</span></td>
                    </tr>
                    <tr>
                        <td>Possible Winning</td>
                        <td> <span>@{{ possibleWin }}</span> </td>
                    </tr>
                </table>
                <p class="messgae hide" id="messagebox"></p>
            </div>
            <div class="confirm">
                <button type="button" class="confirm" id="confirmbutton" data-ng-click="confirmBet()"> Confirm </button>
                <p style="text-align:center;"><a class="clearsection" id="clearBetSlip" data-ng-click="clearBetSlip()"> Clear all selection</a></p>
            </div>
        </div>
    </div>
