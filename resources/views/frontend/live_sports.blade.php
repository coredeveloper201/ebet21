@extends('layouts.frontend_layout')
@section('page_css')
@endsection

@section('title')

@endsection

@section('content')

    <section id="body_warapper" ng-app="live_bets" ng-controller='live_bet_controller'>
        <div class="container container-responsive">
           <div class="row">
                 {{-- <div class="col-md-12 col-lg-12"
                ng-if="dataNotAvailable">
                        <div class="card">
                            <div class="card-body data-not-available">
                                    <div>
                                         <img src="assets/img/live/spinner.gif" alt="loading..."/>
                                      </div>
                                      <div class="unavaiable-text">
                                            <h4>Data is currently unavailable, please wait till it comes back, we are sorry for the inconvenience</h4>
                                            <p>--Ebet21 Team</p>
                                      </div>
                            </div>
                        </div>
                </div> --}}
                <div class="col-md-12 col-xl-12 col-lg-12 padding-0 ">
                    <div class="tab-style-5 __ng_loaded">
                        <div class="tab">
                            <div id="navbar-live-bet" class="scroll-me-mobile">
                                <ul class="tabs active">
                                        <li ng-if="sports_avaiable.tennis" >
                                            <a style="cursor: pointer" title="tennis" class="tab-radious">
                                            <span class="img-sec">
                                                <img src="assets/img/live/tennis.png">
                                            </span>
                                                <span class="text-sec"> Tennis </span>
                                            </a>
                                        </li>
                                        <li ng-if="sports_avaiable.hockey">
                                            <a style="cursor: pointer" title="hockey" class="tab-radious">
                                            <span class="img-sec">
                                                 <img src="assets/img/live/Hockey.png">
                                            </span>
                                                <span class="text-sec"> Hockey </span>
                                            </a>
                                        </li>
                                        <li ng-if="sports_avaiable.baseball">
                                            <a style="cursor: pointer" title="baseball" class="tab-radious">
                                            <span class="img-sec">
                                                <img src="assets/img/live/baseball.png">
                                            </span>
                                                <span class="text-sec"> Baseball </span>
                                            </a>
                                        </li>
                                        <li ng-if="sports_avaiable.basket">
                                            <a style="cursor: pointer" title="basket" class="tab-radious">
                                            <span class="img-sec">
                                                 <img src="assets/img/live/basketball.png">
                                            </span>
                                                <span class="text-sec"> BasketBall </span>
                                            </a>
                                        </li>
                                       {{--  <li ng-if="sports_avaiable.volleyball">
                                            <a style="cursor: pointer" title="volleyball" class="tab-radious">
                                            <span class="img-sec">
                                                 <img src="assets/img/live/volleyball.png">
                                            </span>
                                                <span class="text-sec"> Vollyball </span>
                                            </a>
                                        </li> --}}
                                        <li ng-if="sports_avaiable.football">
                                            <a style="cursor: pointer" title="football" class="tab-radious">
                                            <span class="img-sec">
                                                 <img src="assets/img/live/football.png">
                                            </span>
                                                <span class="text-sec"> Football </span>
                                            </a>
                                        </li>
                                      {{--   <li ng-if="sports_avaiable.motorsport">
                                            <a style="cursor: pointer" title="motorsport" class="tab-radious">
                                            <span class="img-sec">
                                                 <img src="assets/img/live/motorcycle.png">
                                            </span>

                                                <span class="text-sec"> MotorSport </span>
                                            </a>
                                        </li>
                                        <li ng-if="sports_avaiable.cycling">
                                            <a style="cursor: pointer" title="cycling" class="tab-radious">
                                            <span class="img-sec">
                                                <img src="assets/img/live/basketball.png">
                                            </span>
                                                <span class="text-sec"> Cycling </span>
                                            </a>
                                        </li> --}}
                                        <li ng-if="sports_avaiable.soccer">
                                            <a  style="cursor: pointer" title="soccer" class="tab-radious">
                                            <span class="img-sec">
                                                <img src="assets/img/live/basketball.png">
                                            </span>
                                                <span class="text-sec"> Soccer </span>
                                            </a>
                                        </li>
                                </ul>
                            </div>

                            <div class="tab_content sportMenuListTemplate">
                                    <div class="tabs_item" ng-if="sports_avaiable.tennis">
                                        <section id="upcoming_events">
                                            <div class="row">
                                                <div class="col-lg-12 col-xs-12 col-md-12 text-center">
                                                    <div class="upcoming_title">
                                                        <h2>Tennis</h2>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                        @include('frontend.live_bets.live-tennis')
                                    </div>
                                    <div class="tabs_item" ng-if="sports_avaiable.hockey">
                                        <section id="upcoming_events">
                                            <div class="row">
                                                <div class="col-lg-12 col-xs-12 col-md-12 text-center">
                                                    <div class="upcoming_title">
                                                        <h2>Hockey</h2>
                                                    </div>
                                                </div>
                                            </div>

                                        </section>
                                        @include('frontend.live_bets.live-hockey')
                                    </div>
                                    <div class="tabs_item" ng-if="sports_avaiable.baseball">
                                        <section id="upcoming_events">
                                            <div class="row">
                                                <div class="col-lg-12 col-xs-12 text-center">
                                                    <div class="upcoming_title">
                                                        <h2>Baseball</h2>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                        @include('frontend.live_bets.live-baseball')
                                    </div>
                                    <div class="tabs_item" ng-if="sports_avaiable.basket">
                                        <section id="upcoming_events">
                                            <div class="row">
                                                <div class="col-lg-12 col-xs-12 text-center">
                                                    <div class="upcoming_title">
                                                        <h2> Basketball </h2>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                        @include('frontend.live_bets.live-basketball')
                                    </div>
                                 {{--    <div class="tabs_item" ng-if="sports_avaiable.volleyball">
                                        <section id="upcoming_events">
                                            <div class="row">
                                                <div class="col-lg-12 col-xs-12 text-center">
                                                    <div class="upcoming_title">
                                                        <h2> VolleyBall </h2>
                                                    </div>
                                                </div>
                                            </div>

                                        </section>
                                        @include('frontend.live_bets.live-vollyball')
                                    </div> --}}
                                    <div class="tabs_item" ng-if="sports_avaiable.football">
                                        <section id="upcoming_events">
                                            <div class="row">
                                                <div class="col-lg-12 col-xs-12 text-center">
                                                    <div class="upcoming_title">
                                                        <h2> Football </h2>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                        @include('frontend.live_bets.liveFootball')
                                    </div>
                                  {{--   <div class="tabs_item" ng-if="sports_avaiable.motorsport">
                                        <section id="upcoming_events">
                                            <div class="row">
                                                <div class="col-lg-12 col-xs-12 text-center">
                                                    <div class="upcoming_title">
                                                        <h2> Motor Sport </h2>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                        @include('frontend.live_bets.live-motor-sport')
                                    </div>
                                    <div class="tabs_item" ng-if="sports_avaiable.cycling">
                                        <section id="upcoming_events">
                                            <div class="row">
                                                <div class="col-lg-12 col-xs-12 text-center">
                                                    <div class="upcoming_title">
                                                        <h2> Cycling </h2>
                                                    </div>
                                                </div>
                                            </div>

                                        </section>
                                        @include('frontend.live_bets.live-cycling')
                                    </div> --}}
                                    <div class="tabs_item" ng-if="sports_avaiable.soccer">
                                        <section id="upcoming_events">
                                            <div class="row">
                                                <div class="col-lg-12 col-xs-12 text-center">
                                                    <div class="upcoming_title">
                                                        <h2> Soccer League </h2>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                            @include('frontend.live_bets.liveSoccer')
                                    </div>
                            </div> <!-- / tab_content -->
                        </div> <!-- / tab -->
                    </div> <!-- / tab -->
                    <div ng-if="loading" class="preloading">
                        <img src="assets/img/live/spinner.gif" alt="loading..."/>
                    </div>
                </div>

            </div>
        </div>
    </section>
@endsection

@section('page_js')
    <script>
      const liveBet = angular.module('live_bets',[]);
      liveBet.controller('live_bet_controller',function ($scope, $http, $filter){
        $scope.sports_avaiable = {
                tennis:true,
                basket:true,
                baseball:true,
                hockey:true,
                football:true,
                cycling:true,
                motorsport:true,
                volleyball:true,
                soccer:true,
        };
        $scope.dataNotAvailable = false;
        $scope.tab_contents = {
                tennis:false,
                basket:false,
                baseball:false,
                hockey:false,
                football:false,
                cycling:false,
                motorsport:false,
                volleyball:false,
                soccer:false,
        };

        $scope.loading = true;


        function _activateGame(gameName) {
            for(let item in $scope.tab_contents)
            {
                 if(item === gameName)
                 {
                    $scope.tab_contents[item] = true;
                    $scope.$broadcast ('activeGame',$scope.tab_contents);
                 } else {
                    $scope.tab_contents[item] = false;
                 }
            }
        }

        $('.tab ul.tabs').find('> li:eq(0)').addClass('current');
            $('#body_warapper').on('click','.tab ul.tabs li a',function (g) {
                var tab = $(this).closest('.tab'),
                    index = $(this).closest('li').index();
                tab.find('ul.tabs > li').removeClass('current');
                $(this).closest('li').addClass('current');
                _activateGame($(this).attr('title'));
                tab.find('.tab_content').find('div.tabs_item').not('div.tabs_item:eq(' + index + ')').slideUp();
                tab.find('.tab_content').find('div.tabs_item:eq(' + index + ')').slideDown();
                g.preventDefault();
            });

        getAvailableSport();
        function getAvailableSport()
        {
            $http.get('{{url("/live-sports-menu")}}').then(function(response) {
                if (response.data) {
                    let sports = response.data;
                    if(sports.hockey)
                    {
                        $scope.sports_avaiable.hockey = true;
                    }
                    else
                    {
                        $scope.sports_avaiable.hockey = false;
                    }
                    if(sports.basket)
                    {
                        $scope.sports_avaiable.basket = true;
                    }
                    else
                    {
                        $scope.sports_avaiable.basket = false;
                    }
                    if(sports.baseball)
                    {
                        $scope.sports_avaiable.baseball = true;
                    }
                    else
                    {
                        $scope.sports_avaiable.baseball = false;
                    }
                    if(sports.soccer)
                    {
                        $scope.sports_avaiable.soccer = true;
                    }
                    else
                    {
                        $scope.sports_avaiable.soccer = false;
                    }
                    if(sports.tennis)
                    {
                        $scope.sports_avaiable.tennis = true;
                    }
                    else
                    {
                        $scope.sports_avaiable.tennis = false;
                    }
                    if(sports.football)
                    {
                        $scope.sports_avaiable.football = true;
                    }
                    else
                    {
                        $scope.sports_avaiable.football = false;
                    }
                    if(!sports.hockey
                    && !sports.basket
                    && !sports.baseball
                    && !sports.soccer
                    && !sports.tennis
                    && !sports.football
                    )
                    {
                        $scope.dataNotAvailable = true;
                    }
                    else {
                        $scope.dataNotAvailable = false;
                        $scope.loading = false;
                    }

                }
            });
        }

      });

        $(window).scroll(function (e) {
            if (window.scrollY > 100) {
                $('.side-bar-demo-content').css({top: (window.scrollY - 100) + 'px', position: 'absolute', right: '0'});
                $('.card-body').addClass('sticky');
            } else {
                $('.side-bar-demo-content').css({top: '0', position: 'absolute', right: '0'});
                $('.card-body').removeClass('sticky');
            }
        });
    </script>
    <script type="text/javascript" src="{{ asset('assets') }}/js/live/baseball.js"></script>
    <script type="text/javascript" src="{{ asset('assets') }}/js/live/basket.js"></script>
    <script type="text/javascript" src="{{ asset('assets') }}/js/live/cycling.js"></script>
    <script type="text/javascript" src="{{ asset('assets') }}/js/live/football.js"></script>
    <script type="text/javascript" src="{{ asset('assets') }}/js/live/hockey.js"></script>
    <script type="text/javascript" src="{{ asset('assets') }}/js/live/motorsport.js"></script>
    <script type="text/javascript" src="{{ asset('assets') }}/js/live/soccer_league.js"></script>
    <script type="text/javascript" src="{{ asset('assets') }}/js/live/soccer.js"></script>
    <script type="text/javascript" src="{{ asset('assets') }}/js/live/tennis.js"></script>
    <script type="text/javascript" src="{{ asset('assets') }}/js/live/volleyball.js"></script>
@endsection
