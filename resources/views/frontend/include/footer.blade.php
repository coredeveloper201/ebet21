<section id="mobile-tab-view" class="d-xl-none">
    <div class="mobile-container">
        <div class="row">
            <div class="col-sm-3 col-4">
                <p class="text-right-bottom-live"><a href="{{ url('/live-sports') }}"><i class="fa fa-globe"></i> <span>Live</span></a> </p>
            </div>
            <div class="col-sm-3 col-3">
                <p><a href="{{ url('/casino-games') }}">Casino</a></p>
            </div>
            <div class="col-sm-3 col-2">
                <p>...</p>
            </div>
            <div class="col-sm-3 col-3" id="button-container">
            </div>
        </div>
    </div>
</section>

<section id="footer_area">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="copy_right">
                    <p> &copy;  2019 All Rights Reserved. </p>
                </div>
                
            </div>
        </div>
    </div>
</section>
