<section id="header_top_area">
    <div class="container">
        <div class="row">
            <!-- Wagers & Support area -->
            <div class="#logo.col-lg-3">
                <div class="header_logo_here">
                    <img src="{{ asset('assets') }}/img/elogo.png" alt="">
                </div>
            </div>
            <!-- Wagers & Support area -->


            <!-- Logo here -->
            <div class="col-lge-4 __user_profile_container">
                <div class="user_profile_details">
                         <span>Balance:{{ number_format(Auth::user()->coins,2)  }} </span>
                         <span>Available: {{ number_format( Auth::user()->available,2) }}</span>
                         <span>Pending: {{ number_format( Auth::user()->pending,2) }}</span>
                            @if(Auth::user()->free_pay_balance>0)
                                <br><span>Free Balance:</span> <span>{{ Auth::user()->free_pay_balance }} </span><span style="padding-left: 8px;">Free Pending:</span> <span id="header_free_pending_amount">0.00</span>
                            @endif
                </div>
            </div>
            <!-- Logo here -->


            <!-- User details -->
            <div class="col-lge-5 __hide_mobile">
                <div class="user_icon1">
                <img src="{{ asset('assets') }}/img/userico.jpg" height="11px" width="11px" alt="">
                <span> {{ Auth::user()->name }} </span>
                </div>
                <br>
                    <a style="margin-top: 12px; font-size: 12px; font-weight: 500; display: inline-block;" href="{{ route('logout') }}"
                       onclick="event.preventDefault();document.getElementById('logout-form').submit();" class="btn-log btn-info btn-sm">
                        <span class="glyphicon glyphicon-log-out"></span> Log out
                    </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
                </div>

            <!-- User details -->

        </div>
    </div>
</section>
