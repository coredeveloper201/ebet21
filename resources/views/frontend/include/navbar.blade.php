<section id="navigation_area">
    <div class="container">
        <div class="row">
            <div class="col-12">

                <nav>
                    <ul id="nabvar" class="nabvar justify-content-center">
                        <li><a href="{{url('/straight-bet')}}"> Sports</a></li>
                        <li><a href="{{ url('/live-sports') }}">LIVE</a></li>
                        <li><a href="{{ url('/casino-games') }}">Casino</a></li>
                        <li><a href="{{ url('/live-casino') }}">Live Casino</a></li>
                        <li><a href="{{ url('/horses') }}">Horses</a></li>
                        <li><a href="{{ url('/account') }}">Account</a></li>
                        <li><a href="{{url('/pending')}}">Pending</a></li>
                        <li><a href="{{ url('/guide') }}">Guide</a></li>
                        <li><a href="{{ url("/rules") }}">Rules</a></li>
                    </ul>
                </nav>

            </div>
        </div>
    </div>
</section>

<div class="mobile-menu">
    <nav>
        <div class="mobile-menu-logo">
            <img src="/assets/img/elogo.png" alt="">
        </div>
        <div class="user_icon1 __show_in_mobile">
            <img src="{{ asset('assets') }}/img/userico.jpg" height="11" width="11" alt="">
            <span> {{ Auth::user()->name }} </span>
        </div>
        <a href="#0" class="js-menu-open menu-open">
            <i class="material-icons fas fa-bars"></i>
        </a>
    </nav>
    <div class="js-side-nav-container side-nav-container">
        <div class="js-side-nav side-nav">
            <a href="#0" class="js-menu-close menu-close">
                <i class="material-icons"></i>
            </a>
            <div class="profile-file">
                <div class="mobile-menu-logo">
                    <img style="width: 120px" src="/assets/img/elogo.png" alt="">
                </div>
                <div class="user_name-mobile padding-top">
                    <p> test </p>
                </div>
                <div class="profile-images">
                </div>
            </div>

            <ul id="nabvar" class="nabvar justify-content-center">
                <li><a href="{{url('/straight-bet')}}"> Sports</a></li>
                <li><a href="{{ url('/live-sports') }}">LIVE</a></li>
                <li><a href="{{ url('/casino-games') }}">Casino</a></li>
                <li><a href="{{ url('/live-casino') }}">Live Casino</a></li>
                <li><a href="{{ url('/horses') }}">Horses</a></li>
                <li><a href="{{ url('/account') }}">Account</a></li>
                <li><a href="{{url('/pending')}}">Pending</a></li>
                <li><a href="{{ url('/guide') }}">Guide</a></li>
                <li><a href="{{ url("/rules") }}">Rules</a></li>
                <li>
                    <a href="{{ route('logout') }}"
                        onclick="event.preventDefault();document.getElementById('logout-form-mobile').submit();">Log
                        Out</a>
                    <form id="logout-form-mobile" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </li>
            </ul>
        </div>
    </div>
</div>
