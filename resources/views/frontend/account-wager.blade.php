@extends('layouts.frontend_layout')

@section('title')
    Wager: {{ _getDate(strtotime($start_week)) }} to {{ _getDate(strtotime($end_week)) }}
@endsection

@section('content')
	<!-- Upcoming Events area start -->
		<div class="container container-responsive" style="background:grey">
            <div class="row">
                <div class="col-lg-12 col-xs-12">
                    <div class="upcoming_title">
                        <h2> Wager:
                            @if($date!='')
                                {{ _getDate(strtotime($date)) }}
                            @else
                                {{ _getDate(strtotime($start_week)) }} to {{ _getDate(strtotime($end_week)) }}
                            @endif
                        </h2>
                    </div>
                </div>
            </div>
		</div>
	<!-- Upcoming Events area end -->
    @include('frontend.accounts.wager-history')
@endsection
