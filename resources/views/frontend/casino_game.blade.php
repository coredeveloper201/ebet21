@extends('layouts.frontend_layout')

@section('title')

@endsection

@section('content')

    <!-- Upcoming Events area start -->
    <section id="upcoming_events" class="upcoming-margin-top">
        <div class="container">
            <div class="col-12 padding-0">
                <div class="row">
                    <div class="col-lg-12 col-xs-12 padding-0">
                        <div class="upcoming_title">
                            <h2> Casino Games </h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Upcoming Events area end -->

    <!-- main body area start -->
    <section id="body_warapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-xs-12 padding-0">
                    <div class="tab-style-5 tab-style-6">
                        <div class="tab">
                            <div class="scroll-me-mobile-casino">
                                <ul id="navbar-casino" class="tabs casiontab" style="min-width: 0">

                                    <li>
                                        <a href="" style="background: #666; box-shadow: 0 0 16px 3px rgba(43,43,43,.3);" class="">
											<span class="img-sec">
												<img src="assets/img/Casinio/i_blackjack.png">
											</span>
                                            <span class="casino_header"> Blackjack Games </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="" style="background: #666; box-shadow: 0 0 16px 3px rgba(43,43,43,.3);" class="">
											<span class="img-sec">
												<img src="assets/img/Casinio/i_casino.png">
											</span>
                                            <span class="casino_header"> Table Games </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="" style="background: #666; box-shadow: 0 0 16px 3px rgba(43,43,43,.3);" class="">
											<span class="img-sec">
												<img src="assets/img/Casinio/i_slots.png">
											</span>
                                            <span class="casino_header"> Slot Games </span>
                                        </a>
                                    </li>

                                    <li>
                                        <a href="" style="background: #666; box-shadow: 0 0 16px 3px rgba(43,43,43,.3);" class="">
											<span class="img-sec">
												<img src="assets/img/Casinio/i_keno.png">
											</span>
                                            <span class="casino_header"> Keno & Zip Tables </span>
                                        </a>
                                    </li>

                                </ul>
                            </div>
                            <div class="tab_content">

                                <div class="tabs_item">
                                    <div class="game_names">
                                        <span class="casino_header2">Blackjack Games</span>
                                    </div>
                                    <div class="container padding-0">
                                        <div class="row">

                                            <div class="single_casnio_game">
                                                <a href="{{ url('casino-games/jacks-or-better') }}">
                                                    <img src="assets/img/Casinio/jacks_or_better.png" alt=""/>
                                                    <h2>Jacks or Better </h2>
                                                </a>
                                            </div>

                                            <div class="single_casnio_game">
                                                <a href="{{ url('casino-games/blackjack-21') }}">
                                                    <img src="assets/img/Casinio/blackjack_21.png" alt=""/>
                                                    <h2>Black Jack 21 </h2>
                                                </a>
                                            </div>

                                        </div>
                                    </div>
                                </div>


                                <div class="tabs_item">
                                    <div class="game_names">
                                        <span class="casino_header2"> Table Games </span>
                                    </div>
                                    <div class="container padding-0">
                                        <div class="row">

                                            <div class="single_casnio_game">
                                                <a href="{{ url('casino-games/caribbean-stud-poker') }}">
                                                    <img src="assets/img/Casinio/carribean_stud.png" alt=""/>
                                                    <h2>Caribbean Stud Poker </h2>
                                                </a>

                                            </div>
                                            <div class="single_casnio_game">
                                                <a href="{{ url('casino-games/pai-gow-poker') }}">
                                                    <img src="assets/img/Casinio/pai_gow_poker.png" alt=""/>
                                                    <h2> Pai Gow Poker </h2>
                                                </a>
                                            </div>
                                            <div class="single_casnio_game">
                                                <a href="{{ url('casino-games/3card-poker') }}">
                                                    <img src="assets/img/Casinio/3card_pocker.png" alt=""/>
                                                    <h2> 3Card Poker </h2>
                                                </a>
                                            </div>
                                            <div class="single_casnio_game">
                                                <a href="{{ url('casino-games/baccarat') }}">
                                                    <img src="assets/img/Casinio/baccarat.png" alt=""/>
                                                    <h2> Baccarat </h2>
                                                </a>
                                            </div>
                                            <div class="single_casnio_game">
                                                <a href="{{ url('casino-games/craps') }}">
                                                    <img src="assets/img/Casinio/craps.png" alt=""/>
                                                    <h2> Craps </h2>
                                                </a>
                                            </div>
                                            <div class="single_casnio_game">
                                                <a href="{{ url('casino-games/red-dog') }}">
                                                    <img src="assets/img/Casinio/red_dog.png" alt=""/>
                                                    <h2> Red Dog </h2>
                                                </a>
                                            </div>
                                            <div class="single_casnio_game">
                                                <a href="{{ url('casino-games/roulette') }}">
                                                    <img src="assets/img/Casinio/roulette.png" alt=""/>
                                                    <h2> Roulette </h2>
                                                </a>
                                            </div>
                                            <div class="single_casnio_game">
                                                <a href="{{ url('casino-games/joker-poker') }}">
                                                    <img src="assets/img/Casinio/joker_poker.png" alt=""/>
                                                    <h2> Joker Poker </h2>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="tabs_item">
                                    <div class="game_names">
                                        <span class="casino_header2"> Slot Games </span>
                                    </div>
                                    <div class="container padding-0">
                                        <div class="row">

                                            <div class="single_casnio_game">
                                                <a href="{{ url('casino-games/slot-ramses') }}">
                                                    <img src="assets/img/Casinio/slot_rames.png" alt=""/>
                                                    <h2> Slot Ramses</h2>
                                                </a>
                                            </div>
                                            <div class="single_casnio_game">
                                                <a href="{{ url('casino-games/slot-mr-chicken') }}">
                                                    <img src="assets/img/Casinio/slot_mr_chicken.png" alt=""/>
                                                    <h2> Slot Mr Chicken </h2>
                                                </a>
                                            </div>
                                            <div class="single_casnio_game">
                                                <a href="{{ url('casino-games/slot-arabian') }}">
                                                    <img src="assets/img/Casinio/slot_arabian.png" alt=""/>
                                                    <h2> Slot Arabian </h2>
                                                </a>
                                            </div>
                                            <div class="single_casnio_game">
                                                <a href="{{ url('casino-games/slot-3d-soccer') }}">
                                                    <img src="assets/img/Casinio/slot_3d_soccer.png" alt=""/>
                                                    <h2> 3D Soccer Slot </h2>
                                                </a>
                                            </div>
                                            <div class="single_casnio_game">
                                                <a href="{{ url('casino-games/slot-space-adventure') }}">
                                                    <img src="assets/img/Casinio/slot_space_adventure.png" alt=""/>
                                                    <h2> Slot Space Adventure </h2>
                                                </a>
                                            </div>
                                            <div class="single_casnio_game">
                                                <a href="{{ url('casino-games/slot-christmas') }}">
                                                    <img src="assets/img/Casinio/slot_christmas.png" alt=""/>
                                                    <h2> Slot Christmas </h2>
                                                </a>
                                            </div>
                                            <div class="single_casnio_game">
                                                <a href="{{ url('casino-games/pai-gow-poker') }}">
                                                    <img src="assets/img/Casinio/pai_gow_poker.png" alt=""/>
                                                    <h2> Pai Gow Poker </h2>
                                                </a>
                                            </div>

                                        </div>
                                    </div>
                                </div>


                                <div class="tabs_item">
                                    <div class="game_names">
                                        <span class="casino_header2"> Keno & Zip Tables</span>
                                    </div>
                                    <div class="container padding-0">
                                        <div class="row">

                                            <div class="single_casnio_game">
                                                <a href="{{ url('casino-games/pai-gow-poker') }}">
                                                    <img src="assets/img/Casinio/pai_gow_poker.png" alt=""/>
                                                    <h2> Pai Gow Poker </h2>
                                                </a>
                                            </div>
                                            <div class="single_casnio_game">
                                                <a href="{{ url('casino-games/horse-racing') }}">
                                                    <img src="assets/img/Casinio/horse_racing.png" alt=""/>
                                                    <h2> Horse Racing </h2>
                                                </a>
                                            </div>
                                            <div class="single_casnio_game">
                                                <a href="{{ url('casino-games/bingo') }}">
                                                    <img src="assets/img/Casinio/bingo.png" alt=""/>
                                                    <h2> Bingo </h2>
                                                </a>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                            </div><!-- / tab_content -->
                        </div> <!-- / tab -->
                    </div> <!-- / tab -->
                </div>
            </div>
        </div>
    </section>
    <script>

    </script>
    <!-- main body area start -->
@endsection
@section('page_js')
<script>
   $('.tab ul.tabs').addClass('active').find('> li:eq(0)').addClass('current');
    $('#body_warapper').on('click','.tab ul.tabs li a',function (g) {
        var tab = $(this).closest('.tab'),
            index = $(this).closest('li').index();

        tab.find('ul.tabs > li').removeClass('current');
        $(this).closest('li').addClass('current');

        tab.find('.tab_content').find('div.tabs_item').not('div.tabs_item:eq(' + index + ')').slideUp();
        tab.find('.tab_content').find('div.tabs_item:eq(' + index + ')').slideDown();

        g.preventDefault();
    });
</script>
@endsection
