@extends('layouts.frontend_layout')

@section('title')
    Account
@endsection

@section('content')
	<!-- Upcoming Events area start -->
	<section id="upcoming_events" class="upcoming-margin-top ">
		<div class="container">
            <div class="col-12  padding-0">
                <div class="row">
                    <div class="col-lg-12 col-xs-12 padding-0">
                        <div class="upcoming_title ">
                            <h2> Account </h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Upcoming Events area end -->

	<!-- main body area start -->
	<section id="body_warapper" class="acc-body-margin-top">
		<div class="container">
			<div class="row">

				<div class="col-12  padding-0">
                    <!-- section title markup start here -->
                    <div class="tab-style-3 section-title-area section-padding">
                        <div class="tab">
                            <ul class="tabs acc-tab-hide-767">
                                <li><a href="#" id="test">Summary</a></li>
                                <li><a href="#">Daily Figure</a></li>
                                <li><a href="#">Wager History</a></li>
                                <li><a href="#">IP Log</a></li>
                            </ul> <!-- / tabs -->

                            <ul class="tabs responsive-tab acc-tab-show-767">
                                <li><a class="acc-tabs-head" href="#">Summary</a></li>
                                <li><a class="acc-tabs-head" href="#">Daily Figure</a></li>
                                {{--<li><a href="#">  Account History </a></li>--}}
                                <li><a class="acc-tabs-head" href="#">Wager History</a></li>
                                <li><a class="acc-tabs-head" href="#">IP Log</a></li>
                            </ul> <!-- / tabs -->

                            <div class="tab_content tab-content-responsive">
                                <div class="tabs_item">
                                    <div class="container container-responsive">
                                        <div class="row">

                                            <div class="col-xl-12 padding-0">
                                                <div class="video_details">

                                                    <div class="current_details">

                                                        <div class="single_current_details">
                                                            <h3>Current</h3>
                                                            <h4>$ {{ number_format(Auth::user()->coins) }}</h4>
                                                        </div>

                                                        <div class="single_current_details">
                                                            <h3>Available</h3>
                                                            <h4>$ {{ number_format(Auth::user()->available) }}</h4>
                                                        </div>

                                                        <div class="single_current_details">
                                                            <h3>Pending</h3>
                                                            <h4 id="header_pending_amount">$ {{ number_format(Auth::user()->pending) }}</h4>
                                                        </div>

                                                        <div class="single_current_details">
                                                            <h3>Free Available</h3>
                                                            <h4>$ {{ number_format(Auth::user()->free_pay_balance) }}</h4>
                                                        </div>

                                                        <div class="single_current_details">
                                                            <h3>Free Pending</h3>
                                                            <h4>$ {{ number_format(Auth::user()->free_pay_pending) }}</h4>
                                                        </div>

                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <div class="tabs_item">
                                    <div class="container container-responsive">
                                        <div class="row">
                                            <div class="col-xl-12  padding-0">
                                                <div class="video_details">
                                                    <table class="table table-account table-bg-red">
                                                        <thead>
                                                        <tr class="table-hide-640">
                                                            <th>Week Starting</th>
                                                            <?php
                                                            for ($ijk=0; $ijk<1; $ijk++) {
                                                                $previous_week = strtotime("-$ijk week +$ijk day");
                                                                $start_week = strtotime("last sunday midnight",$previous_week);
                                                                $end_week = strtotime("next saturday",$start_week);
                                                                $start_week  = date("Y-m-d",$start_week);
                                                                $end_week  = date("Y-m-d",$end_week);
                                                                for ($iOms =	strtotime($start_week); $iOms<=	strtotime($end_week); $iOms+=86400) {
                                                                    echo '<th>'.date("D", $iOms).'</th>';
                                                                }
                                                            }
                                                            ?>
                                                            <th>Weekly</th>
                                                            <th>Payments</th>
                                                            <th>Pending</th>
                                                            <th>Casino</th>
                                                            <th>Balance</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @foreach($dataArray as $val)
                                                            <tr class="account-href">
                                                                <td class="table-hide-640" data-th="Week Starting">{{ $val['start_week'] }}</td>
                                                                @php ($iTotalFinalAamou = 0)
                                                                @foreach($val['date_val'] as $v)
                                                                    <td class="table-hide-640" data-th="Sun"><a href="{{ url('account/wager') }}?date={{ $v->date }}">{{ _preccedSign($v->value) }}</a></td>
                                                                    @php ($iTotalFinalAamou += $v->value)
                                                                @endforeach
                                                                <td class="table-hide-640" data-th="Weekly"><a href="{{ url('account/wager') }}?start_week={{ $val['start_week_date'] }}&end_week={{ $val['end_week_date'] }}">{{ _preccedSign($iTotalFinalAamou) }}</a></td>
                                                                <td class="table-hide-640" data-th="Pending">{{ ($val['payment']>0)?'+'.$val['payment']:$val['payment'] }}</td>
                                                                <td class="table-hide-640" data-th="Pending">{{ $val['pending']  }}</td>
                                                                <td class="table-hide-640" data-th="casino">{{  ($val['casino']>0)?'+'.$val['casino']:$val['casino'] }}</td>
                                                                <td class="table-hide-640" data-th="Balance">{{ number_format($val['balance'],2)  }}</td>
                                                                <td class="table-show-640">
                                                                    <div class="text-left mb-2">
                                                                        <span class="font-weight-bold mini-sudo-table-acc">Week Starting:</span> <span>{{ $val['start_week'] }}</span>
                                                                    </div>

                                                                    <?php
                                                                    for ($ijk=0; $ijk<1; $ijk++) {
                                                                        $previous_week = strtotime("-$ijk week +$ijk day");
                                                                        $start_week = strtotime("last sunday midnight",$previous_week);
                                                                        $end_week = strtotime("next saturday",$start_week);
                                                                        $start_week  = date("Y-m-d",$start_week);
                                                                        $end_week  = date("Y-m-d",$end_week);
                                                                        for ($iOms =	strtotime($start_week); $iOms<=	strtotime($end_week); $iOms+=86400) {
                                                                            echo '<div class="text-left mb-2"><span class="font-weight-bold mini-sudo-table-acc">'.date("D", $iOms).':</span> <a href="'.url('account/wager').'?date='.$val['date_val'][$ijk]->date.'">'._preccedSign($val['date_val'][$ijk]->value).'</a></div>';
                                                                        }
                                                                    }
                                                                    ?>
                                                                </td>
                                                                <td class="table-show-640">
                                                                    <div class="text-left mb-2">
                                                                        <span class="font-weight-bold mini-sudo-table-acc">Weekly:</span> <span><a href="{{ url('account/wager') }}?start_week={{ $val['start_week_date'] }}&end_week={{ $val['end_week_date'] }}">{{ _preccedSign($iTotalFinalAamou) }}</a></span>
                                                                    </div>
                                                                    <div class="text-left mb-2">
                                                                        <span class="font-weight-bold mini-sudo-table-acc">Payments:</span> <span>{{ ($val['payment']>0)?'+'.$val['payment']:$val['payment'] }}</span>
                                                                    </div>
                                                                    <div class="text-left mb-2">
                                                                        <span class="font-weight-bold mini-sudo-table-acc">Pending:</span> <span>{{ $val['pending'] }}</span>
                                                                    </div>
                                                                    <div class="text-left mb-2">
                                                                        <span class="font-weight-bold mini-sudo-table-acc">Balance:</span> <span>{{ number_format($val['balance'],2) }}</span>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="tabs_item">
                                    @include('frontend.accounts.wager-history')
                                </div>

                                <div class="tabs_item">
                                    <div class="container container-responsive">
                                        <div class="row">
                                            <div class="col-xl-12 padding-0">
                                                <div class="video_details">
                                                    <table class="table table-bg-red">
                                                        <thead>
                                                        <tr>
                                                            <th>#SL</th>
                                                            <th>Date Time</th>
                                                            <th>IP Address</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @if(!empty($loginLogs))
                                                            @php($sl=0)
                                                            @foreach($loginLogs as $log)
                                                                @php($sl++)
                                                                <tr class="">
                                                                    <td>{{$sl}}</td>
                                                                    <td data-th="Date Time">
                                                                        {{ _getDate(strtotime($log->created_at)) }} {{ _getTime(strtotime($log->created_at)) }}
                                                                    </td>
                                                                    <td data-th="IP Address">
                                                                        {{ $log->ip_address }}
                                                                    </td>
                                                                </tr>
                                                            @endforeach
                                                        @endif
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- main body area start -->
@endsection

@section('page_js')
<script>
   $('.tab ul.tabs').addClass('active').find('> li:eq(0)').addClass('current');
    $('#body_warapper').on('click','.tab ul.tabs li a',function (g) {
        var tab = $(this).closest('.tab'),
            index = $(this).closest('li').index();

        tab.find('ul.tabs > li').removeClass('current');
        $(this).closest('li').addClass('current');

        tab.find('.tab_content').find('div.tabs_item').not('div.tabs_item:eq(' + index + ')').slideUp();
        tab.find('.tab_content').find('div.tabs_item:eq(' + index + ')').slideDown();

        g.preventDefault();
    });
</script>
@endsection
