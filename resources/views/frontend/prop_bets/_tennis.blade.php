@extends('layouts.frontend_layout')
@section('title')
@endsection
@section('content')
<!-- main body area start -->
<section id="body_warapper">
    <div class="container">
        <div class="row">
            <div class="col-md-9 col-sm-12 col-xs-12 col-lg-9 col-xl-9">
                @php
                    $prop_data = json_decode(json_encode($prop_bet_details['prop_data']), true);
                    $event_data = $prop_bet_details['event_info'];
                    $event_time = strtotime($event_data->e_date.' '.$event_data->e_time);
                    $event_data->date = _getDate($event_time);
                    $event_data->time = _getTime($event_time);
                    $home_name = $event_data->home_name;
                    $away_name = $event_data->away_name;
                @endphp
                <!-- Upcoming Events area start -->
                <div id="upcoming_events">
                    <div class="row">
                        <div class="col-lg-6 col-xs-6">
                            <div class="upcoming_title">
                                <h2> {{ $away_name }} @ {{$home_name}}</h2>
                            </div>
                        </div>
                        <div class="col-lg-6 col-xs-12">
                            <div class="go_conintue -mobile-devices-previous-button">
                                <a href="{{ url('/straight_odds') }}">Previous</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- 1st half -->
                @if (!empty($prop_data['first_set']))
                    @php
                        $first_set = $prop_data['first_set'];
                    @endphp
                    <h3 style="width: 100%" class="tabletitle tabletitle-mobile">1st Set</h3>
                    <div class="football_first_half">
                        <div class="data-table">
                            <div class="header">
                                <div>Team</div>
                                <div>Spread</div>
                                <div>Money Line</div>
                                <div>Total</div>
                            </div>
                            <div class="body-content">
                                <div class="each-row">
                                    <div>{{ $home_name }}</div>
                                    <div>
                                        @if (!isset($first_set['spread']) || empty($first_set['spread']['home']['odd']))
                                                <a>OTB</a>
                                        @else
                                            @php
                                                $first_set['spread']['home']['odd'] = _toAmericanDecimal($first_set['spread']['home']['odd']);
                                                $first_set['spread']['home']['team_name'] = "1st Set";
                                                $first_set['spread']['home']['sub_type_name'] ="Spread";
                                                $unique_id = $event_data->id."1st-set-7jk799-00-".$first_set['spread']['home']['sub_type_name'];
                                                $first_set['spread']['home']['Unique_id'] = $unique_id;
                                                $first_set['spread']['home']['betting_wager'] = "Spread";
                                                $first_set['spread']['home']['row_line'] = 'home';
                                                $first_set['spread']['home']['handicap'] = isset($first_set['spread']['home']['handicap'])?$first_set['spread']['home']['handicap']:"";
                                            @endphp
                                            <span id="{{$unique_id}}"
                                                onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($first_set['spread']['home'])}}','prop_bet')">
                                                {{ $first_set['spread']['home']['handicap']}} ({{_preccedSign($first_set['spread']['home']['odd'])}})
                                            </span>
                                        @endif
                                    </div>
                                    <div>
                                        @if (!isset($first_set['money']) || empty($first_set['money']['home']))
                                            <a>OTB</a>
                                        @else
                                            @php
                                                $first_half_h = [];
                                                $first_half_h['money']['home']['odd'] = _toAmericanDecimal($first_set['money']['home']);
                                                $first_half_h['money']['home']['team_name'] = "1st Set";
                                                $first_half_h['money']['home']['sub_type_name'] ="Money Line";
                                                $unique_id = $event_data->id."1st-set-99977-00-money-line";
                                                $first_half_h['money']['home']['Unique_id'] = $unique_id;
                                                $first_half_h['money']['home']['betting_wager'] = "Money Line";
                                                $first_half_h['money']['home']['row_line'] = 'home';
                                            @endphp
                                            <span  id="{{$unique_id}}"
                                                    onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($first_half_h['money']['home'])}}','prop_bet')">
                                                    {{ _preccedSign($first_half_h['money']['home']['odd']) }}
                                            </span>
                                        @endif
                                    </div>
                                    <div>
                                        @if (!isset($first_set['total']) || empty($first_set['total']['home']['odd']))
                                            <a>OTB</a>
                                        @else
                                            @php
                                                $first_set['total']['home']['odd'] =
                                                _toAmericanDecimal($first_set['total']['home']['odd']);
                                                $first_set['total']['home']['team_name'] = $first_set['total']['home']['under_over'];
                                                $first_set['total']['home']['sub_type_name'] ="1st Set";
                                                $unique_id = $event_data->id."1st-set-99977-03-total-home";
                                                $first_set['total']['home']['Unique_id'] = $unique_id;
                                                $first_set['total']['home']['betting_wager'] = "Total";
                                                $first_set['total']['home']['row_line'] = 'home';
                                            @endphp
                                            <span id="{{$unique_id}}"
                                                onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($first_set['total']['home'])}}','prop_bet')">
                                                {{$first_set['total']['home']['under_over']}} ({{ _preccedSign($first_set['total']['home']['odd']) }})
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="each-row">
                                    <div>
                                        {{ $away_name  }}
                                    </div>
                                    <div>
                                        @if (!isset($first_set['spread']) || empty($first_set['spread']['away']['odd']))
                                            <a >OTB</a>
                                        @else
                                            @php
                                                $first_set['spread']['away']['odd'] = _toAmericanDecimal($first_set['spread']['away']['odd']);
                                                $first_set['spread']['away']['team_name'] = "1st Set";
                                                $first_set['spread']['away']['sub_type_name'] ="Spread";
                                                $unique_id = $event_data->id."1st-half-78899-11-".$first_set['spread']['away']['sub_type_name'];
                                                $first_set['spread']['away']['Unique_id'] = $unique_id;
                                                $first_set['spread']['away']['betting_wager'] = "Spread";
                                                $first_set['spread']['away']['row_line'] = 'away';
                                                $first_set['spread']['away']['handicap'] = isset($first_set['spread']['away']['handicap'])?$first_set['spread']['away']['handicap']:"";
                                            @endphp
                                            <span id="{{$unique_id}}"
                                                onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($first_set['spread']['away'])}}','prop_bet')">
                                                @php
                                                $home_hendicap =    $first_set['spread']['home']['handicap'];
                                                $away_handicap =   $first_set['spread']['away']['handicap'];
                                                      if ($home_hendicap<0) {
                                                     if($away_handicap < 0)
                                                     {
                                                         $away_handicap = (-1 *  $away_handicap);
                                                         $away_handicap = '+'.$away_handicap;
                                                     }
                                                 }
                                                 else {
                                                     $away_handicap = (-1 *  $away_handicap);
                                                 }
                                                 @endphp
                                                {{$away_handicap}} ({{ _preccedSign($first_set['spread']['away']['odd'])}})
                                            </span>
                                        @endif
                                    </div>
                                    <div>
                                        @if (!isset($first_set['money']) || empty($first_set['money']['away']))
                                            <a>OTB</a>
                                        @else
                                                @php
                                                    $first_half_a = [];
                                                    $first_half_a['money']['away']['odd'] =
                                                    _toAmericanDecimal($first_set['money']['away']);
                                                    $first_half_a['money']['away']['team_name'] = "1st Set";
                                                    $first_half_a['money']['away']['sub_type_name'] ="Money Line";
                                                    $unique_id = $event_data->id."1st-half-96677-11-money-line";
                                                    $first_half_a['money']['away']['Unique_id'] = $unique_id;
                                                    $first_half_a['money']['away']['betting_wager'] = "Money Line";
                                                    $first_half_a['money']['away']['row_line'] = 'away';
                                                @endphp
                                                <span id="{{$unique_id}}"
                                                    onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($first_half_a['money']['away'])}}','prop_bet')">
                                                    {{_preccedSign($first_half_a['money']['away']['odd'])}}
                                                </span>
                                        @endif
                                    </div>
                                    <div>
                                        @if (!isset($first_set['total']) || empty($first_set['total']['away']['odd']))
                                            <a>OTB</a>
                                        @else
                                            @php
                                                $first_set['total']['away']['odd'] =
                                                _toAmericanDecimal($first_set['total']['away']['odd']);
                                                $first_set['total']['away']['team_name'] = $first_set['total']['away']['under_over'];
                                                $first_set['total']['away']['sub_type_name'] ="1st Set";
                                                $unique_id = $event_data->id."1st-set-92277-11-under-away";
                                                $first_set['total']['away']['Unique_id'] = $unique_id;
                                                $first_set['total']['away']['betting_wager'] = "Total";
                                                $first_set['total']['away']['row_line'] = 'away';
                                            @endphp
                                            <span id="{{$unique_id}}"
                                                onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($first_set['total']['away'])}}','prop_bet')">
                                                {{$first_set['total']['away']['under_over'] }} ({{ _preccedSign($first_set['total']['away']['odd'])}})
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif

                @if (!empty($prop_data['set_betting']))
                    @php
                        $set_betting = $prop_data['set_betting'];
                        $k = 0;
                    @endphp
                    <div class="highest_scores_half">
                        <h3 style="width: 100%" class="tabletitle tabletitle-mobile">Set Betting</h3>
                        <div class="data-table">
                            <div class="header">
                            <div> {{$home_name}} : {{$away_name}}</div>
                                <div> Odds </div>
                            </div>
                            <div class="body-content">
                                @foreach ($set_betting as $key=> $hsq)
                                <div class="each-row">
                                    <div> {{$key}}</div>
                                    <div>
                                        @php
                                            $k ++;
                                            $higest_score_qtr = [];
                                            $higest_score_qtr['odd'] =_toAmericanDecimal($hsq);
                                            $higest_score_qtr['team_name'] = $key;
                                            $higest_score_qtr['sub_type_name'] = "Set Betting";
                                            $unique_id = $event_data->id."set-betting-9ikikikkokre77-14-".$k;
                                            $higest_score_qtr['Unique_id'] = $unique_id;
                                            $higest_score_qtr['betting_wager'] = $key;
                                            $higest_score_qtr['row_line'] = 'away';
                                        @endphp
                                        <span id="{{$unique_id}}"
                                            onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($higest_score_qtr)}}','prop_bet')">
                                            {{_preccedSign($higest_score_qtr['odd'])}}
                                        </span>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                @endif

                @if (!empty($prop_data['tie_break']))
                    @php
                        $tie_break = $prop_data['tie_break'];
                    @endphp
                    <div class="odd_even_vollyball">
                        <h3 style="width: 100%" class="tabletitle tabletitle-mobile">Tie Break</h3>
                        <div class="data-table">
                            <div class="header">
                                <div> Yes </div>
                                <div> No </div>
                            </div>
                            <div class="body-content">
                                <div class="each-row">
                                    <div>
                                        @php
                                            $tie_break_yes = [];
                                            $tie_break_yes['odd'] = _toAmericanDecimal($tie_break["Yes"]);
                                            $tie_break_yes['team_name'] = "Yes";
                                            $tie_break_yes['sub_type_name'] = "Tie Break";
                                            $unique_id = $event_data->id."tie-break-yes-775gfxu-01-yes";
                                            $tie_break_yes['Unique_id'] = $unique_id;
                                            $tie_break_yes['betting_wager'] = "Yes";
                                            $tie_break_yes['row_line'] = 'home';
                                        @endphp
                                        <span  id="{{$unique_id}}"
                                                onclick="Betslip.addToCart({{json_encode($event_data) }},'{{ json_encode($tie_break_yes)}}','prop_bet')">
                                                {{_preccedSign($tie_break_yes['odd'])}}
                                        </span>
                                    </div>
                                    <div>
                                        @php
                                             $tie_break_no = [];
                                            $tie_break_no['odd'] = _toAmericanDecimal($tie_break["No"]);
                                            $tie_break_no['team_name'] = "No";
                                            $tie_break_no['sub_type_name'] = "Tie Break";
                                            $unique_id = $event_data->id."tie-break-no-755ohjku-01-no";
                                            $tie_break_no['Unique_id'] = $unique_id;
                                            $tie_break_no['betting_wager'] = "No";
                                            $tie_break_no['row_line'] = 'away';
                                        @endphp
                                        <span  id="{{$unique_id}}"
                                                onclick="Betslip.addToCart({{json_encode($event_data) }},'{{ json_encode($tie_break_no)}}','prop_bet')">
                                                {{_preccedSign($tie_break_no['odd'])}}
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif


                @if (!empty($prop_data['set_match']))
                    @php
                        $set_match = $prop_data['set_match'];
                        $s = 0;
                    @endphp
                    <div class="highest_scores_half">
                        <h3 style="width: 100%" class="tabletitle tabletitle-mobile">Set Match</h3>
                        <div class="data-table">
                            <div class="header">
                                <div> Set </div>
                                <div> Odds </div>
                            </div>
                            <div class="body-content">
                                @foreach ($set_match as $key=> $hsq)
                                <div class="each-row">
                                    <div> {{$key}}</div>
                                    <div>
                                        @php
                                          $s ++;
                                            $higest_score_qtr = [];
                                            $higest_score_qtr['odd'] =_toAmericanDecimal($hsq);
                                            $higest_score_qtr['team_name'] = $key;
                                            $higest_score_qtr['sub_type_name'] ="Set Match";
                                            $unique_id = $event_data->id."set-match-9ikikilkigftkre77-14-".$s;
                                            $higest_score_qtr['Unique_id'] = $unique_id;
                                            $higest_score_qtr['betting_wager'] = $key;
                                            $higest_score_qtr['row_line'] = 'away';
                                        @endphp
                                        <span id="{{$unique_id}}"
                                            onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($higest_score_qtr)}}','prop_bet')">
                                            {{_preccedSign($higest_score_qtr['odd'])}}
                                        </span>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                @endif

                </div>

            <div class="col-3 mobilepostion">
                @include('frontend.bet_slip.slip')
            </div>
        </div>
    </div>
</section>
<!-- main body area start -->
<div class="modal" id="passwordModal">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <h4 class="modal-title">Enter Your Password</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <input type="password" class="form-control" id="bet_confirm_password">
            <p id="passwordMessage" style="color: red"></p>
         </div>
         <div class="modal-footer">
            <button class="btn btn-danger" id="submit_confirm_password">Submit</button>
         </div>
      </div>
   </div>
</div>
<div class="modal" id="betNotice">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <h4 class="modal-title">Bets data changed</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            Bets data has been changed, please review your bets
         </div>
         <div class="modal-footer">
            <button class="btn btn-danger" data-dismiss="modal">Okay</button>
         </div>
      </div>
   </div>
</div>
@endsection
