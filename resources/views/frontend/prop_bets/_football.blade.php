@extends('layouts.frontend_layout')
@section('title')
@endsection
@section('content')
<!-- main body area start -->
<section id="body_warapper">
    <div class="container">
        <div class="row">
            <div class="col-md-9 col-sm-12 col-xs-12 col-lg-9 col-xl-9">
                @php
                    $prop_data = json_decode(json_encode($prop_bet_details['prop_data']), true);
                    $event_data = $prop_bet_details['event_info'];
                    $event_time = strtotime($event_data->e_date.' '.$event_data->e_time);
                    $event_data->date = _getDate($event_time);
                    $event_data->time = _getTime($event_time);
                    $home_name = $event_data->home_name;
                    $away_name = $event_data->away_name;
                @endphp
                <!-- Upcoming Events area start -->
                <div id="upcoming_events">
                    <div class="row">
                        <div class="col-lg-6 col-xs-6">
                            <div class="upcoming_title">
                                <h2> {{ $away_name }} @ {{$home_name}}</h2>
                            </div>
                        </div>
                        <div class="col-lg-6 col-xs-12">
                            <div class="go_conintue -mobile-devices-previous-button">
                                <a href="{{ url('/straight_odds') }}">Previous</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- 1st half -->
                @if (!empty($prop_data['first_half']))
                    @php
                        $first_half = $prop_data['first_half'];
                    @endphp
                    <h3 style="width: 100%" class="tabletitle tabletitle-mobile">1st half</h3>
                    <div class="football_first_half">
                        <div class="data-table">
                            <div class="header">
                                <div>Team</div>
                                <div>Spread</div>
                                <div>Money Line</div>
                                <div>Total</div>
                            </div>
                            <div class="body-content">
                                <div class="each-row">
                                    <div>{{ $home_name }}</div>
                                    <div>
                                        @if (!isset($first_half['spread']) || empty($first_half['spread']['home']['odd']))
                                                <a>OTB</a>
                                        @else
                                            @php
                                                $first_half['spread']['home']['odd'] = _toAmericanDecimal($first_half['spread']['home']['odd']);
                                                $first_half['spread']['home']['team_name'] = "1st half";
                                                $first_half['spread']['home']['sub_type_name'] ="Spread";
                                                $unique_id = $event_data->id."1st-half-77799-00-".$first_half['spread']['home']['sub_type_name'];
                                                $first_half['spread']['home']['Unique_id'] = $unique_id;
                                                $first_half['spread']['home']['betting_wager'] = "Spread";
                                                $first_half['spread']['home']['row_line'] = 'home';
                                                $first_half['spread']['home']['handicap'] = isset($first_half['spread']['home']['handicap'])?$first_half['spread']['home']['handicap']:"";
                                            @endphp
                                            <span id="{{$unique_id}}"
                                                onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($first_half['spread']['home'])}}','prop_bet')">
                                                {{ $first_half['spread']['home']['handicap']}} ({{_preccedSign($first_half['spread']['home']['odd'])}})
                                            </span>
                                        @endif
                                    </div>
                                    <div>
                                        @if (!isset($first_half['money']) || empty($first_half['money']['home']))
                                            <a>OTB</a>
                                        @else
                                            @php
                                                $first_half_h = [];
                                                $first_half_h['money']['home']['odd'] = _toAmericanDecimal($first_half['money']['home']);
                                                $first_half_h['money']['home']['team_name'] = "1st half";
                                                $first_half_h['money']['home']['sub_type_name'] ="Money Line";
                                                $unique_id = $event_data->id."1st-half-99977-00-money-line";
                                                $first_half_h['money']['home']['Unique_id'] = $unique_id;
                                                $first_half_h['money']['home']['betting_wager'] = "Money Line";
                                                $first_half_h['money']['home']['row_line'] = 'home';
                                            @endphp
                                            <span  id="{{$unique_id}}"
                                                    onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($first_half_h['money']['home'])}}','prop_bet')">
                                                    {{ _preccedSign($first_half_h['money']['home']['odd']) }}
                                            </span>
                                        @endif
                                    </div>
                                    <div>
                                        @if (!isset($first_half['total']) || empty($first_half['total']['home']['odd']))
                                            <a>OTB</a>
                                        @else
                                            @php
                                                $first_half['total']['home']['odd'] =
                                                _toAmericanDecimal($first_half['total']['home']['odd']);
                                                $first_half['total']['home']['team_name'] = $first_half['total']['home']['under_over'];
                                                $first_half['total']['home']['sub_type_name'] ="1st half";
                                                $unique_id = $event_data->id."1st-half-99977-03-total-home";
                                                $first_half['total']['home']['Unique_id'] = $unique_id;
                                                $first_half['total']['home']['betting_wager'] = "Total";
                                                $first_half['total']['home']['row_line'] = 'home';
                                            @endphp
                                            <span id="{{$unique_id}}"
                                                onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($first_half['total']['home'])}}','prop_bet')">
                                                {{$first_half['total']['home']['under_over']}} ({{ _preccedSign($first_half['total']['home']['odd']) }})
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="each-row">
                                    <div>
                                        {{ $away_name  }}
                                    </div>
                                    <div>
                                        @if (!isset($first_half['spread']) || empty($first_half['spread']['away']['odd']))
                                            <a >OTB</a>
                                        @else
                                            @php
                                                $first_half['spread']['away']['odd'] = _toAmericanDecimal($first_half['spread']['away']['odd']);
                                                $first_half['spread']['away']['team_name'] = "1st half";
                                                $first_half['spread']['away']['sub_type_name'] ="Spread";
                                                $unique_id = $event_data->id."1st-half-78899-11-".$first_half['spread']['away']['sub_type_name'];
                                                $first_half['spread']['away']['Unique_id'] = $unique_id;
                                                $first_half['spread']['away']['betting_wager'] = "Spread";
                                                $first_half['spread']['away']['row_line'] = 'away';
                                                $first_half['spread']['away']['handicap'] = isset($first_half['spread']['away']['handicap'])?$first_half['spread']['away']['handicap']:"";
                                            @endphp
                                            <span id="{{$unique_id}}"
                                                onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($first_half['spread']['away'])}}','prop_bet')">
                                                @php
                                                $home_hendicap =    $first_half['spread']['home']['handicap'];
                                                $away_handicap =   $first_half['spread']['away']['handicap'];
                                                      if ($home_hendicap<0) {
                                                     if($away_handicap < 0)
                                                     {
                                                         $away_handicap = (-1 *  $away_handicap);
                                                         $away_handicap = '+'.$away_handicap;
                                                     }
                                                 }
                                                 else {
                                                     $away_handicap = (-1 *  $away_handicap);
                                                 }
                                                 @endphp
                                                {{$away_handicap}} ({{ _preccedSign($first_half['spread']['away']['odd'])}})
                                            </span>
                                        @endif
                                    </div>
                                    <div>
                                        @if (!isset($first_half['money']) || empty($first_half['money']['away']))
                                            <a>OTB</a>
                                        @else
                                                @php
                                                    $first_half_a = [];
                                                    $first_half_a['money']['away']['odd'] =
                                                    _toAmericanDecimal($first_half['money']['away']);
                                                    $first_half_a['money']['away']['team_name'] = "1st half";
                                                    $first_half_a['money']['away']['sub_type_name'] ="Money Line";
                                                    $unique_id = $event_data->id."1st-half-96677-11-money-line";
                                                    $first_half_a['money']['away']['Unique_id'] = $unique_id;
                                                    $first_half_a['money']['away']['betting_wager'] = "Money Line";
                                                    $first_half_a['money']['away']['row_line'] = 'away';
                                                @endphp
                                                <span id="{{$unique_id}}"
                                                    onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($first_half_a['money']['away'])}}','prop_bet')">
                                                    {{_preccedSign($first_half_a['money']['away']['odd'])}}
                                                </span>
                                        @endif
                                    </div>
                                    <div>
                                        @if (!isset($first_half['total']) || empty($first_half['total']['away']['odd']))
                                            <a>OTB</a>
                                        @else
                                            @php
                                                $first_half['total']['away']['odd'] =
                                                _toAmericanDecimal($first_half['total']['away']['odd']);
                                                $first_half['total']['away']['team_name'] =$first_half['total']['away']['under_over'];
                                                $first_half['total']['away']['sub_type_name'] ="1st half";
                                                $unique_id = $event_data->id."1st-half-92277-11-under";
                                                $first_half['total']['away']['Unique_id'] = $unique_id;
                                                $first_half['total']['away']['betting_wager'] = "Total";
                                                $first_half['total']['away']['row_line'] = 'away';
                                            @endphp
                                            <span id="{{$unique_id}}"
                                                onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($first_half['total']['away'])}}','prop_bet')">
                                                {{$first_half['total']['away']['under_over'] }} ({{ _preccedSign($first_half['total']['away']['odd'])}})
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif

                @if (!empty($prop_data['second_half']))
                    <!-- 2nd half -->
                    @php
                    $second_half = $prop_data['second_half'];
                    @endphp
                    <h3 style="width: 100%" class="tabletitle tabletitle-mobile">2nd half</h3>
                    <div class="football_second_half_prop">
                        <div class="data-table">
                            <div class="header">
                                <div>Team</div>
                                <div>Spread</div>
                                <div>Money Line</div>
                                <div>Total</div>
                            </div>
                            <div class="body-content">
                                <div class="each-row">
                                    <div>{{ $home_name }}</div>
                                    <div>
                                        @if ( !isset($second_half['spread']) || empty($second_half['spread']['home']['odd']))
                                            <a>OTB</a>
                                        @else
                                            @php
                                                $second_half['spread']['home']['odd'] =
                                                _toAmericanDecimal($second_half['spread']['home']['odd']);
                                                $second_half['spread']['home']['team_name'] = "2nd half";
                                                $second_half['spread']['home']['sub_type_name'] ="Spread";
                                                $unique_id = $event_data->id."2st-half-22577-00-".$second_half['spread']['home']['sub_type_name'];
                                                $second_half['spread']['home']['Unique_id'] = $unique_id;
                                                $second_half['spread']['home']['betting_wager'] = "Spread";
                                                $second_half['spread']['home']['row_line'] = 'home';
                                                $second_half['spread']['home']['handicap'] = isset($second_half['spread']['home']['handicap'])?$second_half['spread']['home']['handicap']:"";
                                            @endphp
                                            <span id="{{$unique_id}}"
                                                onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($second_half['spread']['home'])}}','prop_bet')">
                                                {{ $second_half['spread']['home']['handicap']}} ({{ _preccedSign($second_half['spread']['home']['odd'])}})
                                            </span>
                                        @endif
                                    </div>
                                    <div>
                                        @if ( !isset($second_half['money']) || empty($second_half['money']['home']))
                                            <a>OTB</a>
                                        @else
                                                @php
                                                    $second_half_h = [];
                                                    $second_half_h['money']['home']['odd'] =  _toAmericanDecimal($second_half['money']['home']);
                                                    $second_half_h['money']['home']['team_name'] = "2nd half";
                                                    $second_half_h['money']['home']['sub_type_name'] ="Money Line";
                                                    $unique_id = $event_data->id."2st-half-33655-00-money-line";
                                                    $second_half_h['money']['home']['Unique_id'] = $unique_id;
                                                    $second_half_h['money']['home']['betting_wager'] = "Money Line";
                                                    $second_half_h['money']['home']['row_line'] = 'home';
                                                @endphp
                                                <span id="{{$unique_id}}"
                                                    onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($second_half_h['money']['home'])}}','prop_bet')">
                                                    {{ _preccedSign($second_half_h['money']['home']['odd']) }}
                                                </span>
                                        @endif
                                    </div>
                                    <div>
                                        @if ( !isset($second_half['total']['home']) || empty($second_half['total']['home']['odd']))
                                            <a>OTB</a>
                                        @else
                                            @php
                                                $second_half['total']['home']['odd'] =  _toAmericanDecimal($second_half['total']['home']['odd']);
                                                $second_half['total']['home']['team_name'] = $second_half['total']['home']['under_over'];
                                                $second_half['total']['home']['sub_type_name'] ="2nd half";
                                                $unique_id = $event_data->id."2st-half-33299-00-total";
                                                $second_half['total']['home']['Unique_id'] = $unique_id;
                                                $second_half['total']['home']['betting_wager'] = "Total";
                                                $second_half['total']['home']['row_line'] = 'home';
                                            @endphp
                                            <span id="{{$unique_id}}"
                                                    onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($second_half['total']['home'])}}','prop_bet')">
                                                    {{$second_half['total']['home']['under_over'] }} ({{_preccedSign($second_half['total']['home']['odd'])}})
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="each-row">
                                    <div> {{ $away_name  }} </div>
                                    <div>
                                        @if ( !isset($second_half['spread']['away']) || empty($second_half['spread']['away']['odd']))
                                            <a> OTB</a>
                                        @else
                                            @php
                                                $second_half['spread']['away']['odd'] =
                                                _toAmericanDecimal($second_half['spread']['away']['odd']);
                                                $second_half['spread']['away']['team_name'] = "2nd half";
                                                $second_half['spread']['away']['sub_type_name'] ="Spread";
                                                $unique_id = $event_data->id."2st-half-22500-01-".$second_half['spread']['away']['sub_type_name'];
                                                $second_half['spread']['away']['Unique_id'] = $unique_id;
                                                $second_half['spread']['away']['betting_wager'] = "Spread";
                                                $second_half['spread']['away']['row_line'] = 'away';
                                                $second_half['spread']['away']['handicap'] = isset($second_half['spread']['away']['handicap'])?$second_half['spread']['away']['handicap']:"";
                                            @endphp
                                            <span id="{{$unique_id}}"
                                                onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($second_half['spread']['away'])}}','prop_bet')">
                                                @php
                                                $home_hendicap =    $second_half['spread']['home']['handicap'];
                                                $away_handicap =    $second_half['spread']['away']['handicap'];
                                                      if ($home_hendicap<0) {
                                                     if($away_handicap < 0)
                                                     {
                                                         $away_handicap = (-1 *  $away_handicap);
                                                         $away_handicap = '+'.$away_handicap;
                                                     }
                                                 }
                                                 else {
                                                     $away_handicap = (-1 *  $away_handicap);
                                                 }
                                                 @endphp
                                                {{$away_handicap}} ({{ _preccedSign($second_half['spread']['away']['odd'])}})
                                            </span>
                                        @endif
                                    </div>
                                    <div>
                                        @if (!isset($second_half['money']['away']) || empty($second_half['money']['away']))
                                            <a>OTB</a>
                                        @else
                                                @php
                                                    $second_half_a = [];
                                                    $second_half_a['money']['away']['odd'] =  _toAmericanDecimal($second_half['money']['away']);
                                                    $second_half_a['money']['away']['team_name'] = "2nd half";
                                                    $second_half_a['money']['away']['sub_type_name'] ="Money Line";
                                                    $unique_id = $event_data->id."2st-half-33600-01-money-line";
                                                    $second_half_a['money']['away']['Unique_id'] = $unique_id;
                                                    $second_half_a['money']['away']['betting_wager'] = "Money Line";
                                                    $second_half_a['money']['away']['row_line'] = 'away';
                                                @endphp
                                                <span id="{{$unique_id}}"
                                                    onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($second_half_a['money']['away'])}}','prop_bet')">
                                                    {{_preccedSign($second_half_a['money']['away']['odd'])}}
                                                </span>
                                        @endif
                                    </div>
                                    <div>
                                        @if (!isset($second_half['total']['away']) || empty($second_half['total']['away']['odd']))
                                            <a>OTB</a>
                                        @else
                                            @php
                                                $second_half['total']['away']['odd'] =  _toAmericanDecimal($second_half['total']['away']['odd']);
                                                $second_half['total']['away']['team_name'] = $second_half['total']['away']['under_over'];
                                                $second_half['total']['away']['sub_type_name'] ="2nd half";
                                                $unique_id = $event_data->id."2st-half-33200-01-total";
                                                $second_half['total']['away']['Unique_id'] = $unique_id;
                                                $second_half['total']['away']['betting_wager'] = "Total";
                                                $second_half['total']['away']['row_line'] = 'away';
                                            @endphp
                                            <span id="{{$unique_id}}"
                                                onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($second_half['total']['away'])}}','prop_bet')">
                                                {{ $second_half['total']['away']['under_over']}} ({{_preccedSign($second_half['total']['away']['odd'])}})
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif

                @if (!empty($prop_data['first_qtr']))
                    @php
                        $first_qtr = $prop_data['first_qtr'];
                    @endphp
                    <!-- 1st Quarter -->
                    <h3 style="width: 100%" class="tabletitle tabletitle-mobile">1st Quarter</h3>
                    <div class="football_first_qtr_prop_bet">
                        <div class="data-table">
                            <div class="header">
                                <div>Team</div>
                                <div>Spread</div>
                                <div>Money Line</div>
                                <div>Total</div>
                            </div>
                            <div class="body-content">
                                <div class="each-row">
                                    <div>{{ $home_name }}</div>
                                    <div>
                                        @if (!isset( $first_qtr['spread']['home'] ) || empty($first_qtr['spread']['home']['odd']))
                                            <a> OTB </a>
                                        @else
                                            @php
                                                $first_qtr['spread']['home']['odd'] =  _toAmericanDecimal($first_qtr['spread']['home']['odd']);
                                                $first_qtr['spread']['home']['team_name'] = "1st Quarter table";
                                                $first_qtr['spread']['home']['sub_type_name'] ="Spread";
                                                $unique_id = $event_data->id."1st-quarter-66455-00-spread";
                                                $first_qtr['spread']['home']['Unique_id'] = $unique_id;
                                                $first_qtr['spread']['home']['betting_wager'] = "Spread";
                                                $first_qtr['spread']['home']['row_line'] = 'home';
                                                $first_qtr['spread']['home']['handicap'] = isset($first_qtr['spread']['home']['handicap'])?$first_qtr['spread']['home']['handicap']:"";
                                            @endphp
                                            <span id="{{$unique_id}}"
                                                onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($first_qtr['spread']['home'])}}','prop_bet')">
                                                {{ $first_qtr['spread']['home']['handicap']}} ({{ _preccedSign($first_qtr['spread']['home']['odd'])}})
                                            </span>
                                        @endif
                                    </div>
                                    <div>
                                        @if (!isset( $first_qtr['money']['home'] ) ||  empty($first_qtr['money']['home']))
                                            <a > OTB</a>
                                        @else
                                                @php
                                                    $first_qtr_h = [];
                                                    $first_qtr_h['money']['home']['odd'] =  _toAmericanDecimal($first_qtr['money']['home']);
                                                    $first_qtr_h['money']['home']['team_name'] = "1st Quarter table";
                                                    $first_qtr_h['money']['home']['sub_type_name'] ="Money Line";
                                                    $unique_id = $event_data->id."1st-quarter-66577-00-money-line";
                                                    $first_qtr_h['money']['home']['Unique_id'] = $unique_id;
                                                    $first_qtr_h['money']['home']['betting_wager'] = "Money Line";
                                                    $first_qtr_h['money']['home']['row_line'] = 'home';
                                                @endphp
                                                <span id="{{$unique_id}}"
                                                    onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($first_qtr_h['money']['home'])}}','prop_bet')">
                                                {{_preccedSign($first_qtr_h['money']['home']['odd'])}}
                                                </span>
                                        @endif
                                    </div>
                                    <div>
                                        @if (!isset( $first_qtr['total']['home'] ) || empty($first_qtr['total']['home']['odd']))
                                            <a> OTB </a>
                                        @else
                                            @php
                                                $first_qtr['total']['home']['odd'] =  _toAmericanDecimal($first_qtr['total']['home']['odd']);
                                                $first_qtr['total']['home']['team_name'] = $first_qtr['total']['home']['under_over'];
                                                $first_qtr['total']['home']['sub_type_name'] ="1st Quarter table";
                                                $unique_id = $event_data->id."1st-quarter-66563-00-total";
                                                $first_qtr['total']['home']['Unique_id'] = $unique_id;
                                                $first_qtr['total']['home']['betting_wager'] = "Total";
                                                $first_qtr['total']['home']['row_line'] = 'home';
                                            @endphp
                                            <span id="{{$unique_id}}"
                                                onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($first_qtr['total']['home'])}}','prop_bet')">
                                                {{$first_qtr['total']['home']['under_over']}} ({{_preccedSign($first_qtr['total']['home']['odd'])}})
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="each-row">
                                    <div>
                                        {{ $away_name  }}
                                    </div>
                                    <div>
                                        @if (!isset( $first_qtr['spread']['away'] ) || empty($first_qtr['spread']['away']['odd']))
                                            <a>OTB </a>
                                        @else
                                            @php
                                                $first_qtr['spread']['away']['odd'] =  _toAmericanDecimal($first_qtr['spread']['away']['odd']);
                                                $first_qtr['spread']['away']['team_name'] = "1st Quarter table";
                                                $first_qtr['spread']['away']['sub_type_name'] ="Spread";
                                                $unique_id = $event_data->id."1st-quarter-66411-00-spread";
                                                $first_qtr['spread']['away']['Unique_id'] = $unique_id;
                                                $first_qtr['spread']['away']['betting_wager'] = "Spread";
                                                $first_qtr['spread']['away']['row_line'] = 'away';
                                                $first_qtr['spread']['away']['handicap'] = isset($first_qtr['spread']['away']['handicap'])?$first_qtr['spread']['away']['handicap']:"";
                                            @endphp
                                            <span id="{{$unique_id}}"
                                                onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($first_qtr['spread']['away'])}}','prop_bet')">
                                                @php
                                                $home_hendicap =     $first_qtr['spread']['home']['handicap'];
                                                $away_handicap =     $first_qtr['spread']['away']['handicap'];
                                                      if ($home_hendicap<0) {
                                                     if($away_handicap < 0)
                                                     {
                                                         $away_handicap = (-1 *  $away_handicap);
                                                         $away_handicap = '+'.$away_handicap;
                                                     }
                                                 }
                                                 else {
                                                     $away_handicap = (-1 *  $away_handicap);
                                                 }
                                                 @endphp
                                                {{$away_handicap}} ({{_preccedSign($first_qtr['spread']['away']['odd'])}})
                                            </span>
                                        @endif
                                    </div>
                                    <div>
                                        @if (!isset( $first_qtr['money']['away'] ) || empty($first_qtr['money']['away']))
                                            <a> OTB</a>
                                        @else
                                                @php
                                                    $first_qtr_a = [];
                                                    $first_qtr_a['money']['away']['odd'] =  _toAmericanDecimal($first_qtr['money']['away']);
                                                    $first_qtr_a['money']['away']['team_name'] = "1st Quarter table";
                                                    $first_qtr_a['money']['away']['sub_type_name'] ="Money Line";
                                                    $unique_id = $event_data->id."1st-quarter-66599-01-money-line";
                                                    $first_qtr_a['money']['away']['Unique_id'] = $unique_id;
                                                    $first_qtr_a['money']['away']['betting_wager'] = "Money Line";
                                                    $first_qtr_a['money']['away']['row_line'] = 'away';
                                                @endphp
                                                <span id="{{$unique_id}}"
                                                    onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($first_qtr_a['money']['away'])}}','prop_bet')">
                                                    {{_preccedSign($first_qtr_a['money']['away']['odd'])}}
                                                </span>
                                        @endif
                                    </div>
                                    <div>
                                        @if (!isset($first_qtr['total']['away'] ) || empty($first_qtr['total']['away']['odd']))
                                            <a> OTB </a>
                                        @else
                                            @php
                                                $first_qtr['total']['away']['odd'] =  _toAmericanDecimal($first_qtr['total']['away']['odd']);
                                                $first_qtr['total']['away']['team_name'] = $first_qtr['total']['away']['under_over'];
                                                $first_qtr['total']['away']['sub_type_name'] ="1st Quarter table";
                                                $unique_id = $event_data->id."1st-quarter-66536-01-total";
                                                $first_qtr['total']['away']['Unique_id'] = $unique_id;
                                                $first_qtr['total']['away']['betting_wager'] = "Total";
                                                $first_qtr['total']['away']['row_line'] = 'away';
                                            @endphp
                                            <span id="{{$unique_id}}"
                                                onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($first_qtr['total']['away'])}}','prop_bet')">
                                                {{$first_qtr['total']['away']['under_over'] }} ({{ _preccedSign($first_qtr['total']['away']['odd'])}})
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif

                @if (!empty($prop_data['overtime']))
                    @php
                        $overtime = $prop_data['overtime'];
                        $overtime_home = [];
                        $overtime_away = [];
                    @endphp
                    <div class="away_total_goals">
                        <h3 style="width: 100%" class="tabletitle tabletitle-mobile">Over Time</h3>
                        <div class="data-table">
                            <div class="header">
                                <div> Yes </div>
                                <div> No </div>
                            </div>
                            <div class="body-content">
                                <div class="each-row">
                                    <div>
                                        @php
                                            $overtime_home['home']['odd'] =  _toAmericanDecimal($overtime['home']);
                                            $overtime_home['home']['team_name'] = "Over Time";
                                            $overtime_home['home']['sub_type_name'] ="Yes";
                                            $unique_id = $event_data->id."over-time-66ig6-00-yes";
                                            $overtime_home['home']['Unique_id'] = $unique_id;
                                            $overtime_home['home']['betting_wager'] = "Yes";
                                            $overtime_home['home']['row_line'] = 'home';
                                            $overtime_home['home']['handicap'] = 'Yes';
                                        @endphp
                                        <span id="{{$unique_id}}"
                                            onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($overtime_home['home'])}}','prop_bet')">
                                            {{_preccedSign($overtime_home['home']['odd'])}}
                                        </span>
                                    </div>
                                    <div>
                                        @php
                                            $overtime_away['away']['odd'] =  _toAmericanDecimal($overtime['away']);
                                            $overtime_away['away']['team_name'] = "Over Time";
                                            $overtime_away['away']['sub_type_name'] ="No";
                                            $unique_id = $event_data->id."over-time-66ii6-01-no";
                                            $overtime_away['away']['Unique_id'] = $unique_id;
                                            $overtime_away['away']['betting_wager'] = "No";
                                            $overtime_away['away']['row_line'] = 'away';
                                            $overtime_away['away']['handicap']  = 'No';
                                        @endphp
                                        <span id="{{$unique_id}}"
                                            onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($overtime_away['away'])}}','prop_bet')">
                                            {{_preccedSign($overtime_away['away']['odd'])}}
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif

                @if (!empty($prop_data['winning_margin']))
                    @php
                        $winning_margin = $prop_data['winning_margin'];
                    @endphp
                    <div class="basketball_winning_margin">
                        <h3 style="width: 100%" class="tabletitle tabletitle-mobile">Winning Margin</h3>
                        <div class="data-table">
                            <div class="header">
                                <div> Range </div>
                            <div> {{$home_name}} </div>
                                <div> {{$away_name}} </div>
                            </div>
                            <div class="body-content">
                                @php
                                    $winning_ranges = array_keys($winning_margin['odds']);
                                @endphp
                                @foreach($winning_margin['odds'] as $range => $value)
                                    <div class="each-row">
                                        <div> {{$range}}</div>
                                        <div>
                                            @php
                                                $home_winning_margin = [];
                                                $home_winning_margin['odd'] =_toAmericanDecimal($value['home']);
                                                $home_winning_margin['team_name'] = "Winning Margin";
                                                $home_winning_margin['sub_type_name'] = 'home';
                                                $unique_id = $event_data->id."1st-half-9skkf77-".str_replace('+','-',$range)."-";
                                                $home_winning_margin['Unique_id'] = $unique_id;
                                                $home_winning_margin['betting_wager'] = 'winning_margin_home';
                                                $home_winning_margin['row_line'] = $winning_ranges;
                                                $home_winning_margin['handicap'] = $range;
                                            @endphp
                                            <span id="{{$unique_id}}"
                                                onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($home_winning_margin)}}','prop_bet')">
                                                {{_preccedSign($home_winning_margin['odd'])}}
                                            </span>
                                        </div>
                                        <div>
                                            @php
                                                $away_winning_margin = [];
                                                $away_winning_margin['odd'] =_toAmericanDecimal($value['away']);
                                                $away_winning_margin['team_name'] = "Winning Margin";
                                                $away_winning_margin['sub_type_name'] = 'away';
                                                $unique_id = $event_data->id."1st-half-9soof77-".str_replace('+','-',$range)."-";
                                                $away_winning_margin['Unique_id'] = $unique_id;
                                                $away_winning_margin['betting_wager'] = 'winning_margin_away';
                                                $away_winning_margin['row_line'] = $winning_ranges;
                                                $away_winning_margin['handicap'] = $range;
                                            @endphp
                                            <span id="{{$unique_id}}"
                                                onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($away_winning_margin)}}','prop_bet')">
                                                {{_preccedSign($away_winning_margin['odd'])}}
                                            </span>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                @endif

                @if (!empty($prop_data['highest_scoring_qtr']))
                    @php
                        $highest_scor_qtr = $prop_data['highest_scoring_qtr'];
                    @endphp
                    <div class="highest_scores_half">
                        <h3 style="width: 100%" class="tabletitle tabletitle-mobile">Highest Scoring QUARTER</h3>
                        <div class="data-table">
                            <div class="header">
                                <div> Quarter </div>
                                <div> Odds </div>
                            </div>
                            <div class="body-content">
                                @foreach ($highest_scor_qtr['odds'] as $i=> $hsq)
                                <div class="each-row">
                                    <div> {{$hsq['name']}}</div>
                                    <div>
                                        @php
                                            $higest_score_qtr = [];
                                            $higest_score_qtr['odd'] =_toAmericanDecimal($hsq['value']);
                                            $higest_score_qtr['team_name'] = $hsq['name'];
                                            $higest_score_qtr['sub_type_name'] = "Highest Scoring Quarter";
                                            $unique_id = $event_data->id."1st-half-9ikikiokre77-".$i."-";
                                            $higest_score_qtr['Unique_id'] = $unique_id;
                                            $higest_score_qtr['betting_wager'] = $hsq['name'];
                                            $higest_score_qtr['row_line'] = 'away';
                                        @endphp
                                        <span id="{{$unique_id}}"
                                            onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($higest_score_qtr)}}','prop_bet')">
                                            {{_preccedSign($higest_score_qtr['odd'])}}
                                        </span>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                @endif

                @if (!empty($prop_data['team_total_home']))
                    @php
                    $teamTotal = $prop_data['team_total_home'];
                    @endphp
                    <div class="away_total_goals">
                        <h3 style="width: 100%" class="tabletitle tabletitle-mobile">Team Total - {{$home_name}}</h3>
                        <div class="data-table">
                            <div class="header">
                                <div> Over </div>
                                <div> Under </div>
                            </div>
                            <div class="body-content">
                                <div class="each-row">
                                    <div>
                                        @php
                                            $teamTotal['total']['home']['odd'] =  _toAmericanDecimal($teamTotal['total']['home']['odd']);
                                            $teamTotal['total']['home']['team_name'] = "Team Total - Home";
                                            $teamTotal['total']['home']['sub_type_name'] ="Over";
                                            $unique_id = $event_data->id."team-total-home-6jkh56-01-over";
                                            $teamTotal['total']['home']['Unique_id'] = $unique_id;
                                            $teamTotal['total']['home']['betting_wager'] = "Over";
                                            $teamTotal['total']['home']['row_line'] = 'home';
                                            $teamTotal['total']['home']['handicap'] =  $teamTotal['total']['home']['under_over'];
                                        @endphp
                                        <span id="{{$unique_id}}"
                                             onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($teamTotal['total']['home'])}}','prop_bet')">
                                            {{$teamTotal['total']['home']['handicap']}} ({{_preccedSign($teamTotal['total']['home']['odd'])}})
                                        </span>
                                    </div>
                                    <div>
                                        @php
                                            $teamTotal['total']['away']['odd'] =  _toAmericanDecimal($teamTotal['total']['away']['odd']);
                                            $teamTotal['total']['away']['team_name'] = "Team Total - Home";
                                            $teamTotal['total']['away']['sub_type_name'] ="Under";
                                            $unique_id = $event_data->id."team-total-6jkh896-011-under";
                                            $teamTotal['total']['away']['Unique_id'] = $unique_id;
                                            $teamTotal['total']['away']['betting_wager'] = "Under";
                                            $teamTotal['total']['away']['row_line'] = 'home';
                                            $teamTotal['total']['away']['handicap'] =  $teamTotal['total']['away']['under_over'];
                                        @endphp
                                        <span id="{{$unique_id}}"
                                            onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($teamTotal['total']['away'])}}','prop_bet')">
                                            {{$teamTotal['total']['away']['handicap']}} ({{_preccedSign($teamTotal['total']['away']['odd'])}})
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif

                @if (!empty($prop_data['team_total_away']))
                    @php
                    $teamTotalaway = $prop_data['team_total_away'];
                    @endphp
                    <div class="away_total_goals">
                        <h3 style="width: 100%" class="tabletitle tabletitle-mobile">Team Total - {{$away_name}}</h3>
                        <div class="data-table">
                            <div class="header">
                                <div> Over </div>
                                <div> Under </div>
                            </div>
                            <div class="body-content">
                                <div class="each-row">
                                    <div>
                                        @php
                                            $teamTotalaway['total']['home']['odd'] =  _toAmericanDecimal($teamTotalaway['total']['home']['odd']);
                                            $teamTotalaway['total']['home']['team_name'] = "Team Total - Away";
                                            $teamTotalaway['total']['home']['sub_type_name'] ="Over";
                                            $unique_id = $event_data->id."team-total-away-6jkll56-01-over";
                                            $teamTotalaway['total']['home']['Unique_id'] = $unique_id;
                                            $teamTotalaway['total']['home']['betting_wager'] = "Over";
                                            $teamTotalaway['total']['home']['row_line'] = 'away';
                                            $teamTotalaway['total']['home']['handicap'] =  $teamTotalaway['total']['home']['under_over'];
                                        @endphp
                                        <span id="{{$unique_id}}"
                                            onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($teamTotalaway['total']['home'])}}','prop_bet')">
                                            {{$teamTotalaway['total']['home']['handicap']}} ({{_preccedSign($teamTotalaway['total']['home']['odd'])}})
                                        </span>
                                    </div>
                                    <div>
                                        @php
                                            $teamTotalaway['total']['away']['odd'] =  _toAmericanDecimal($teamTotalaway['total']['away']['odd']);
                                            $teamTotalaway['total']['away']['team_name'] = "Team Total - Away";
                                            $teamTotalaway['total']['away']['sub_type_name'] ="Under";
                                            $unique_id = $event_data->id."team-total-away-6jkuiu96-011-under";
                                            $teamTotalaway['total']['away']['Unique_id'] = $unique_id;
                                            $teamTotalaway['total']['away']['betting_wager'] = "Under";
                                            $teamTotalaway['total']['away']['row_line'] = 'away';
                                            $teamTotalaway['total']['away']['handicap'] =  $teamTotalaway['total']['away']['under_over'];
                                        @endphp
                                        <span id="{{$unique_id}}"
                                            onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($teamTotalaway['total']['away'])}}','prop_bet')">
                                            {{$teamTotalaway['total']['away']['handicap']}} ({{_preccedSign($teamTotalaway['total']['away']['odd'])}})
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif


                @if (!empty($prop_data['odd_even']))
                    @php
                        $odd_even = $prop_data['odd_even'];
                    @endphp
                    <div class="odd_even_vollyball">
                        <h3 style="width: 100%" class="tabletitle tabletitle-mobile">Odd Even</h3>
                        <div class="data-table">
                            <div class="header">
                                <div> Odd </div>
                                <div> Even </div>
                            </div>
                            <div class="body-content">
                                <div class="each-row">
                                    <div>
                                        @php
                                            $odd_even['odd']['odd'] = _toAmericanDecimal($odd_even["Odd"]);
                                            $odd_even['odd']['team_name'] = "Odd";
                                            $odd_even['odd']['sub_type_name'] = "Odd/Even";
                                            $unique_id = $event_data->id."odd-even-775gfxu-01-odd";
                                            $odd_even['odd']['Unique_id'] = $unique_id;
                                            $odd_even['odd']['betting_wager'] = "Odd";
                                            $odd_even['odd']['row_line'] = 'home';
                                        @endphp
                                        <span  id="{{$unique_id}}"
                                                onclick="Betslip.addToCart({{json_encode($event_data) }},'{{ json_encode($odd_even['odd'])}}','prop_bet')">
                                                {{_preccedSign($odd_even['odd']['odd'])}}
                                        </span>
                                    </div>
                                    <div>
                                        @php
                                            $odd_even['even']['odd'] = _toAmericanDecimal($odd_even["Odd"]);
                                            $odd_even['even']['team_name'] = "Even";
                                            $odd_even['even']['sub_type_name'] = "Odd/Even";
                                            $unique_id = $event_data->id."odd-even-755ohxu-01-even";
                                            $odd_even['even']['Unique_id'] = $unique_id;
                                            $odd_even['even']['betting_wager'] = "Even";
                                            $odd_even['even']['row_line'] = 'away';
                                        @endphp
                                        <span  id="{{$unique_id}}"
                                                onclick="Betslip.addToCart({{json_encode($event_data) }},'{{ json_encode($odd_even['even'])}}','prop_bet')">
                                                {{_preccedSign($odd_even['even']['odd'])}}
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif


                </div>

            <div class="col-3 mobilepostion">
                @include('frontend.bet_slip.slip')
            </div>
        </div>
    </div>
</section>
<!-- main body area start -->
<div class="modal" id="passwordModal">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <h4 class="modal-title">Enter Your Password</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <input type="password" class="form-control" id="bet_confirm_password">
            <p id="passwordMessage" style="color: red"></p>
         </div>
         <div class="modal-footer">
            <button class="btn btn-danger" id="submit_confirm_password">Submit</button>
         </div>
      </div>
   </div>
</div>
<div class="modal" id="betNotice">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <h4 class="modal-title">Bets data changed</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            Bets data has been changed, please review your bets
         </div>
         <div class="modal-footer">
            <button class="btn btn-danger" data-dismiss="modal">Okay</button>
         </div>
      </div>
   </div>
</div>
@endsection
