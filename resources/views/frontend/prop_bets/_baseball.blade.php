@extends('layouts.frontend_layout')
@section('title')
@endsection
@section('content')
<!-- main body area start -->
<section id="body_warapper">
    <div class="container">
        <div class="row">
            <div class="col-md-9 col-sm-12 col-xs-12 col-lg-9 col-xl-9">
                @php
                    $prop_data = json_decode(json_encode($prop_bet_details['prop_data']), true);
                    $event_data = $prop_bet_details['event_info'];
                    $event_time = strtotime($event_data->e_date.' '.$event_data->e_time);
                    $event_data->date = _getDate($event_time);
                    $event_data->time = _getTime($event_time);
                    $home_name = $event_data->home_name;
                    $away_name = $event_data->away_name;
                @endphp
                <!-- Upcoming Events area start -->
                <div id="upcoming_events">
                    <div class="row">
                        <div class="col-lg-6 col-xs-6">
                            <div class="upcoming_title">
                                <h2> {{ $away_name }} @ {{$home_name}}</h2>
                            </div>
                        </div>
                        <div class="col-lg-6 col-xs-12">
                            <div class="go_conintue -mobile-devices-previous-button">
                                <a href="{{ url('/straight_odds') }}">Previous</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- First five innings -->
                @if (!empty($prop_data['first_five_innings']))
                    @php
                        $first_five_innings = $prop_data['first_five_innings'];
                    @endphp
                    <h3 style="width: 100%" class="tabletitle tabletitle-mobile">First Five Innings</h3>
                    <div class="football_first_half">
                        <div class="data-table">
                            <div class="header">
                                <div>Team</div>
                                <div>Spread</div>
                                <div>Money Line</div>
                                <div>Total</div>
                            </div>
                            <div class="body-content">
                                <div class="each-row">
                                    <div>{{ $home_name }}</div>
                                    <div>
                                        @if (!isset($first_five_innings['spread']) || empty($first_five_innings['spread']['home']['odd']))
                                                <a>OTB</a>
                                        @else
                                            @php
                                                $first_five_innings['spread']['home']['odd'] = _toAmericanDecimal($first_five_innings['spread']['home']['odd']);
                                                $first_five_innings['spread']['home']['team_name'] = "First five innings";
                                                $first_five_innings['spread']['home']['sub_type_name'] ="Spread";
                                                $unique_id = $event_data->id."1st-half-77799-00-".$first_five_innings['spread']['home']['sub_type_name'];
                                                $first_five_innings['spread']['home']['Unique_id'] = $unique_id;
                                                $first_five_innings['spread']['home']['betting_wager'] = "Spread";
                                                $first_five_innings['spread']['home']['row_line'] = 'home';
                                                $first_five_innings['spread']['home']['handicap'] = isset($first_five_innings['spread']['home']['handicap'])?$first_five_innings['spread']['home']['handicap']:"";
                                            @endphp
                                            <span id="{{$unique_id}}"
                                                onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($first_five_innings['spread']['home'])}}','prop_bet')">
                                                {{ $first_five_innings['spread']['home']['handicap']}} ({{_preccedSign($first_five_innings['spread']['home']['odd'])}})
                                            </span>
                                        @endif
                                    </div>
                                    <div>
                                        @if (!isset($first_five_innings['money']) || empty($first_five_innings['money']['home']))
                                            <a>OTB</a>
                                        @else
                                            @php
                                                $first_half_h = [];
                                                $first_half_h['money']['home']['odd'] = _toAmericanDecimal($first_five_innings['money']['home']);
                                                $first_half_h['money']['home']['team_name'] = "First five innings";
                                                $first_half_h['money']['home']['sub_type_name'] ="Money Line";
                                                $unique_id = $event_data->id."1st-half-99977-00-money-line";
                                                $first_half_h['money']['home']['Unique_id'] = $unique_id;
                                                $first_half_h['money']['home']['betting_wager'] = "Money Line";
                                                $first_half_h['money']['home']['row_line'] = 'home';
                                            @endphp
                                            <span  id="{{$unique_id}}"
                                                    onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($first_half_h['money']['home'])}}','prop_bet')">
                                                    {{ _preccedSign($first_half_h['money']['home']['odd']) }}
                                            </span>
                                        @endif
                                    </div>
                                    <div>
                                        @if (!isset($first_five_innings['total']) || empty($first_five_innings['total']['home']['odd']))
                                            <a>OTB</a>
                                        @else
                                            @php
                                                $first_five_innings['total']['home']['odd'] =
                                                _toAmericanDecimal($first_five_innings['total']['home']['odd']);
                                                $first_five_innings['total']['home']['team_name'] = $first_five_innings['total']['home']['under_over'];
                                                $first_five_innings['total']['home']['sub_type_name'] ="First five innings";
                                                $unique_id = $event_data->id."first-five-innings-99977-03-total-home";
                                                $first_five_innings['total']['home']['Unique_id'] = $unique_id;
                                                $first_five_innings['total']['home']['betting_wager'] = "Total";
                                                $first_five_innings['total']['home']['row_line'] = 'home';
                                            @endphp
                                            <span id="{{$unique_id}}"
                                                onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($first_five_innings['total']['home'])}}','prop_bet')">
                                                {{$first_five_innings['total']['home']['under_over']}} ({{ _preccedSign($first_five_innings['total']['home']['odd'])}})
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="each-row">
                                    <div>
                                        {{ $away_name  }}
                                    </div>
                                    <div>
                                        @if (!isset($first_five_innings['spread']) || empty($first_five_innings['spread']['away']['odd']))
                                            <a >OTB</a>
                                        @else
                                            @php
                                                $first_five_innings['spread']['away']['odd'] = _toAmericanDecimal($first_five_innings['spread']['away']['odd']);
                                                $first_five_innings['spread']['away']['team_name'] = "First five innings";
                                                $first_five_innings['spread']['away']['sub_type_name'] ="Spread";
                                                $unique_id = $event_data->id."1st-half-78899-11-".$first_five_innings['spread']['away']['sub_type_name'];
                                                $first_five_innings['spread']['away']['Unique_id'] = $unique_id;
                                                $first_five_innings['spread']['away']['betting_wager'] = "Spread";
                                                $first_five_innings['spread']['away']['row_line'] = 'away';
                                                $first_five_innings['spread']['away']['handicap'] = isset($first_five_innings['spread']['away']['handicap'])?$first_five_innings['spread']['away']['handicap']:"";
                                            @endphp
                                            <span id="{{$unique_id}}"
                                                onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($first_five_innings['spread']['away'])}}','prop_bet')">
                                                @php
                                                $home_hendicap =    $first_five_innings['spread']['home']['handicap'];
                                                $away_handicap =   $first_five_innings['spread']['away']['handicap'];
                                                      if ($home_hendicap<0) {
                                                     if($away_handicap < 0)
                                                     {
                                                         $away_handicap = (-1 *  $away_handicap);
                                                         $away_handicap = '+'.$away_handicap;
                                                     }
                                                 }
                                                 else {
                                                     $away_handicap = (-1 *  $away_handicap);
                                                 }
                                                 @endphp
                                                {{$away_handicap}} ({{ _preccedSign($first_five_innings['spread']['away']['odd'])}})
                                            </span>
                                        @endif
                                    </div>
                                    <div>
                                        @if (!isset($first_five_innings['money']) || empty($first_five_innings['money']['away']))
                                            <a>OTB</a>
                                        @else
                                                @php
                                                    $first_half_a = [];
                                                    $first_half_a['money']['away']['odd'] =
                                                    _toAmericanDecimal($first_five_innings['money']['away']);
                                                    $first_half_a['money']['away']['team_name'] = "First five innings";
                                                    $first_half_a['money']['away']['sub_type_name'] ="Money Line";
                                                    $unique_id = $event_data->id."1st-half-96677-11-money-line";
                                                    $first_half_a['money']['away']['Unique_id'] = $unique_id;
                                                    $first_half_a['money']['away']['betting_wager'] = "Money Line";
                                                    $first_half_a['money']['away']['row_line'] = 'away';
                                                @endphp
                                                <span id="{{$unique_id}}"
                                                    onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($first_half_a['money']['away'])}}','prop_bet')">
                                                    {{_preccedSign($first_half_a['money']['away']['odd'])}}
                                                </span>
                                        @endif
                                    </div>
                                    <div>
                                        @if (!isset($first_five_innings['total']) || empty($first_five_innings['total']['away']['odd']))
                                            <a>OTB</a>
                                        @else
                                            @php
                                                $first_five_innings['total']['away']['odd'] =
                                                _toAmericanDecimal($first_five_innings['total']['away']['odd']);
                                                $first_five_innings['total']['away']['team_name'] = $first_five_innings['total']['away']['under_over'];
                                                $first_five_innings['total']['away']['sub_type_name'] ="First five innings";
                                                $unique_id = $event_data->id."first-five-innings-92277-11-under";
                                                $first_five_innings['total']['away']['Unique_id'] = $unique_id;
                                                $first_five_innings['total']['away']['betting_wager'] = "Total";
                                                $first_five_innings['total']['away']['row_line'] = 'away';
                                            @endphp
                                            <span id="{{$unique_id}}"
                                                onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($first_five_innings['total']['away'])}}','prop_bet')">
                                                {{$first_five_innings['total']['away']['under_over'] }} ({{ _preccedSign($first_five_innings['total']['away']['odd'])}})
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif

                @if (!empty($prop_data['first_innings']))
                    <!-- 2nd half -->
                    @php
                    $first_innings = $prop_data['first_innings'];
                    @endphp
                    <h3 style="width: 100%" class="tabletitle tabletitle-mobile">First Inning</h3>
                    <div class="football_second_half_prop">
                        <div class="data-table">
                            <div class="header">
                                <div>Team</div>
                                <div>Spread</div>
                                <div>Money Line</div>
                                <div>Total</div>
                            </div>
                            <div class="body-content">
                                <div class="each-row">
                                    <div>{{ $home_name }}</div>
                                    <div>
                                        @if ( !isset($first_innings['spread']) || empty($first_innings['spread']['home']['odd']))
                                            <a>OTB</a>
                                        @else
                                            @php
                                                $first_innings['spread']['home']['odd'] =
                                                _toAmericanDecimal($first_innings['spread']['home']['odd']);
                                                $first_innings['spread']['home']['team_name'] = "First Inning";
                                                $first_innings['spread']['home']['sub_type_name'] ="Spread";
                                                $unique_id = $event_data->id."2st-half-22577-00-".$first_innings['spread']['home']['sub_type_name'];
                                                $first_innings['spread']['home']['Unique_id'] = $unique_id;
                                                $first_innings['spread']['home']['betting_wager'] = "Spread";
                                                $first_innings['spread']['home']['row_line'] = 'home';
                                                $first_innings['spread']['home']['handicap'] = isset($first_innings['spread']['home']['handicap'])?$first_innings['spread']['home']['handicap']:"";
                                            @endphp
                                            <span id="{{$unique_id}}"
                                                onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($first_innings['spread']['home'])}}','prop_bet')">
                                                {{ $first_innings['spread']['home']['handicap']}} ({{ _preccedSign($first_innings['spread']['home']['odd'])}})
                                            </span>
                                        @endif
                                    </div>
                                    <div>
                                        @if ( !isset($first_innings['money']) || empty($first_innings['money']['home']))
                                            <a>OTB</a>
                                        @else
                                                @php
                                                    $second_half_h = [];
                                                    $second_half_h['money']['home']['odd'] =  _toAmericanDecimal($first_innings['money']['home']);
                                                    $second_half_h['money']['home']['team_name'] = "First Inning";
                                                    $second_half_h['money']['home']['sub_type_name'] ="Money Line";
                                                    $unique_id = $event_data->id."2st-half-33655-00-money-line";
                                                    $second_half_h['money']['home']['Unique_id'] = $unique_id;
                                                    $second_half_h['money']['home']['betting_wager'] = "Money Line";
                                                    $second_half_h['money']['home']['row_line'] = 'home';
                                                @endphp
                                                <span id="{{$unique_id}}"
                                                    onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($second_half_h['money']['home'])}}','prop_bet')">
                                                    {{ _preccedSign($second_half_h['money']['home']['odd']) }}
                                                </span>
                                        @endif
                                    </div>
                                    <div>
                                        @if ( !isset($first_innings['total']['home']) || empty($first_innings['total']['home']['odd']))
                                            <a>OTB</a>
                                        @else
                                            @php
                                                $first_innings['total']['home']['odd'] =  _toAmericanDecimal($first_innings['total']['home']['odd']);
                                                $first_innings['total']['home']['team_name'] = $first_innings['total']['home']['under_over'];
                                                $first_innings['total']['home']['sub_type_name'] ="First Inning";
                                                $unique_id = $event_data->id."first-innings-33299-00-total-over";
                                                $first_innings['total']['home']['Unique_id'] = $unique_id;
                                                $first_innings['total']['home']['betting_wager'] = "Total";
                                                $first_innings['total']['home']['row_line'] = 'home';
                                            @endphp
                                            <span id="{{$unique_id}}"
                                                    onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($first_innings['total']['home'])}}','prop_bet')">
                                                    {{$first_innings['total']['home']['under_over'] }} ({{_preccedSign($first_innings['total']['home']['odd'])}})
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="each-row">
                                    <div> {{ $away_name  }} </div>
                                    <div>
                                        @if ( !isset($first_innings['spread']['away']) || empty($first_innings['spread']['away']['odd']))
                                            <a> OTB</a>
                                        @else
                                            @php
                                                $first_innings['spread']['away']['odd'] =
                                                _toAmericanDecimal($first_innings['spread']['away']['odd']);
                                                $first_innings['spread']['away']['team_name'] = "First Inning";
                                                $first_innings['spread']['away']['sub_type_name'] ="Spread";
                                                $unique_id = $event_data->id."2st-half-22500-01-".$first_innings['spread']['away']['sub_type_name'];
                                                $first_innings['spread']['away']['Unique_id'] = $unique_id;
                                                $first_innings['spread']['away']['betting_wager'] = "Spread";
                                                $first_innings['spread']['away']['row_line'] = 'away';
                                                $first_innings['spread']['away']['handicap'] = isset($first_innings['spread']['away']['handicap'])?$first_innings['spread']['away']['handicap']:"";
                                            @endphp
                                            <span id="{{$unique_id}}"
                                                onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($first_innings['spread']['away'])}}','prop_bet')">
                                                @php
                                                $home_hendicap =    $first_innings['spread']['home']['handicap'];
                                                $away_handicap =    $first_innings['spread']['away']['handicap'];
                                                      if ($home_hendicap<0) {
                                                     if($away_handicap < 0)
                                                     {
                                                         $away_handicap = (-1 *  $away_handicap);
                                                         $away_handicap = '+'.$away_handicap;
                                                     }
                                                 }
                                                 else {
                                                     $away_handicap = (-1 *  $away_handicap);
                                                 }
                                                 @endphp
                                                {{$away_handicap}} ({{ _preccedSign($first_innings['spread']['away']['odd'])}})
                                            </span>
                                        @endif
                                    </div>
                                    <div>
                                        @if (!isset($first_innings['money']['away']) || empty($first_innings['money']['away']))
                                            <a>OTB</a>
                                        @else
                                                @php
                                                    $second_half_a = [];
                                                    $second_half_a['money']['away']['odd'] =  _toAmericanDecimal($first_innings['money']['away']);
                                                    $second_half_a['money']['away']['team_name'] = "First Inning";
                                                    $second_half_a['money']['away']['sub_type_name'] ="Money Line";
                                                    $unique_id = $event_data->id."2st-half-33600-01-money-line";
                                                    $second_half_a['money']['away']['Unique_id'] = $unique_id;
                                                    $second_half_a['money']['away']['betting_wager'] = "Money Line";
                                                    $second_half_a['money']['away']['row_line'] = 'away';
                                                @endphp
                                                <span id="{{$unique_id}}"
                                                    onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($second_half_a['money']['away'])}}','prop_bet')">
                                                    {{_preccedSign($second_half_a['money']['away']['odd'])}}
                                                </span>
                                        @endif
                                    </div>
                                    <div>
                                        @if (!isset($first_innings['total']['away']) || empty($first_innings['total']['away']['odd']))
                                            <a>OTB</a>
                                        @else
                                            @php
                                                $first_innings['total']['away']['odd'] =  _toAmericanDecimal($first_innings['total']['away']['odd']);
                                                $first_innings['total']['away']['team_name'] = $first_innings['total']['away']['under_over'];
                                                $first_innings['total']['away']['sub_type_name'] ="First Inning";
                                                $unique_id = $event_data->id."first-innings-33200-01-total-under";
                                                $first_innings['total']['away']['Unique_id'] = $unique_id;
                                                $first_innings['total']['away']['betting_wager'] = "Total";
                                                $first_innings['total']['away']['row_line'] = 'away';
                                            @endphp
                                            <span id="{{$unique_id}}"
                                                onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($first_innings['total']['away'])}}','prop_bet')">
                                                {{ $first_innings['total']['away']['under_over']}} ({{_preccedSign($first_innings['total']['away']['odd'])}})
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif

                @if (!empty($prop_data['team_total_home']))
                    @php
                    $teamTotal = $prop_data['team_total_home'];
                    @endphp
                    <div class="away_total_goals">
                        <h3 style="width: 100%" class="tabletitle tabletitle-mobile">Team Total - {{$home_name}}</h3>
                        <div class="data-table">
                            <div class="header">
                                <div> Over </div>
                                <div> Under </div>
                            </div>
                            <div class="body-content">
                                <div class="each-row">
                                    <div>
                                        @php
                                            $teamTotal['total']['home']['odd'] =  _toAmericanDecimal($teamTotal['total']['home']['odd']);
                                            $teamTotal['total']['home']['team_name'] = "Team Total - Home";
                                            $teamTotal['total']['home']['sub_type_name'] ="Over";
                                            $unique_id = $event_data->id."team-total-home-6jkh56-01-over";
                                            $teamTotal['total']['home']['Unique_id'] = $unique_id;
                                            $teamTotal['total']['home']['betting_wager'] = "Over";
                                            $teamTotal['total']['home']['row_line'] = 'home';
                                            $teamTotal['total']['home']['handicap'] =  $teamTotal['total']['home']['under_over'];
                                        @endphp
                                        <span id="{{$unique_id}}"
                                             onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($teamTotal['total']['home'])}}','prop_bet')">
                                            {{$teamTotal['total']['home']['handicap']}} ({{_preccedSign($teamTotal['total']['home']['odd'])}})
                                        </span>
                                    </div>
                                    <div>
                                        @php
                                            $teamTotal['total']['away']['odd'] =  _toAmericanDecimal($teamTotal['total']['away']['odd']);
                                            $teamTotal['total']['away']['team_name'] = "Team Total - Home";
                                            $teamTotal['total']['away']['sub_type_name'] ="Under";
                                            $unique_id = $event_data->id."team-total-6jkh896-011-under";
                                            $teamTotal['total']['away']['Unique_id'] = $unique_id;
                                            $teamTotal['total']['away']['betting_wager'] = "Under";
                                            $teamTotal['total']['away']['row_line'] = 'home';
                                            $teamTotal['total']['away']['handicap'] =  $teamTotal['total']['away']['under_over'];
                                        @endphp
                                        <span id="{{$unique_id}}"
                                            onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($teamTotal['total']['away'])}}','prop_bet')">
                                            {{$teamTotal['total']['away']['handicap']}} ({{_preccedSign($teamTotal['total']['away']['odd'])}})
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif

                @if (!empty($prop_data['team_total_away']))
                    @php
                    $teamTotalaway = $prop_data['team_total_away'];
                    @endphp
                    <div class="away_total_goals">
                        <h3 style="width: 100%" class="tabletitle tabletitle-mobile">Team Total - {{$away_name}}</h3>
                        <div class="data-table">
                            <div class="header">
                                <div> Over </div>
                                <div> Under </div>
                            </div>
                            <div class="body-content">
                                <div class="each-row">
                                    <div>
                                        @php
                                            $teamTotalaway['total']['home']['odd'] =  _toAmericanDecimal($teamTotalaway['total']['home']['odd']);
                                            $teamTotalaway['total']['home']['team_name'] = "Team Total - Away";
                                            $teamTotalaway['total']['home']['sub_type_name'] ="Over";
                                            $unique_id = $event_data->id."team-total-away-6jkll56-01-over";
                                            $teamTotalaway['total']['home']['Unique_id'] = $unique_id;
                                            $teamTotalaway['total']['home']['betting_wager'] = "Over";
                                            $teamTotalaway['total']['home']['row_line'] = 'away';
                                            $teamTotalaway['total']['home']['handicap'] =  $teamTotalaway['total']['home']['under_over'];
                                        @endphp
                                        <span id="{{$unique_id}}"
                                            onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($teamTotalaway['total']['home'])}}','prop_bet')">
                                            {{$teamTotalaway['total']['home']['handicap']}} ({{_preccedSign($teamTotalaway['total']['home']['odd'])}})
                                        </span>
                                    </div>
                                    <div>
                                        @php
                                            $teamTotalaway['total']['away']['odd'] =  _toAmericanDecimal($teamTotalaway['total']['away']['odd']);
                                            $teamTotalaway['total']['away']['team_name'] = "Team Total - Away";
                                            $teamTotalaway['total']['away']['sub_type_name'] ="Under";
                                            $unique_id = $event_data->id."team-total-away-6jkuiu96-011-under";
                                            $teamTotalaway['total']['away']['Unique_id'] = $unique_id;
                                            $teamTotalaway['total']['away']['betting_wager'] = "Under";
                                            $teamTotalaway['total']['away']['row_line'] = 'away';
                                            $teamTotalaway['total']['away']['handicap'] =  $teamTotalaway['total']['away']['under_over'];
                                        @endphp
                                        <span id="{{$unique_id}}"
                                            onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($teamTotalaway['total']['away'])}}','prop_bet')">
                                            {{$teamTotalaway['total']['away']['handicap']}} ({{_preccedSign($teamTotalaway['total']['away']['odd'])}})
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif


                @if (!empty($prop_data['odd_even']))
                @php
                    $odd_even = $prop_data['odd_even'];
                @endphp
                <div class="odd_even_vollyball">
                    <h3 style="width: 100%" class="tabletitle tabletitle-mobile">Odd Even</h3>
                    <div class="data-table">
                        <div class="header">
                            <div> Odd </div>
                            <div> Even </div>
                        </div>
                        <div class="body-content">
                            <div class="each-row">
                                <div>
                                    @php
                                        $odd_even['odd']['odd'] = _toAmericanDecimal($odd_even["Odd"]);
                                        $odd_even['odd']['team_name'] = "Odd";
                                        $odd_even['odd']['sub_type_name'] = "Odd/Even";
                                        $unique_id = $event_data->id."odd-even-775gfxu-01-odd";
                                        $odd_even['odd']['Unique_id'] = $unique_id;
                                        $odd_even['odd']['betting_wager'] = "Odd";
                                        $odd_even['odd']['row_line'] = 'home';
                                    @endphp
                                    <span  id="{{$unique_id}}"
                                            onclick="Betslip.addToCart({{json_encode($event_data) }},'{{ json_encode($odd_even['odd'])}}','prop_bet')">
                                            {{_preccedSign($odd_even['odd']['odd'])}}
                                    </span>
                                </div>
                                <div>
                                    @php
                                        $odd_even['even']['odd'] = _toAmericanDecimal($odd_even["Odd"]);
                                        $odd_even['even']['team_name'] = "Even";
                                        $odd_even['even']['sub_type_name'] = "Odd/Even";
                                        $unique_id = $event_data->id."odd-even-755ohxu-01-even";
                                        $odd_even['even']['Unique_id'] = $unique_id;
                                        $odd_even['even']['betting_wager'] = "Even";
                                        $odd_even['even']['row_line'] = 'away';
                                    @endphp
                                    <span  id="{{$unique_id}}"
                                            onclick="Betslip.addToCart({{json_encode($event_data) }},'{{ json_encode($odd_even['even'])}}','prop_bet')">
                                            {{_preccedSign($odd_even['even']['odd'])}}
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif


                </div>

            <div class="col-3 mobilepostion">
                @include('frontend.bet_slip.slip')
            </div>
        </div>
    </div>
</section>
<!-- main body area start -->
<div class="modal" id="passwordModal">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <h4 class="modal-title">Enter Your Password</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <input type="password" class="form-control" id="bet_confirm_password">
            <p id="passwordMessage" style="color: red"></p>
         </div>
         <div class="modal-footer">
            <button class="btn btn-danger" id="submit_confirm_password">Submit</button>
         </div>
      </div>
   </div>
</div>
<div class="modal" id="betNotice">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <h4 class="modal-title">Bets data changed</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            Bets data has been changed, please review your bets
         </div>
         <div class="modal-footer">
            <button class="btn btn-danger" data-dismiss="modal">Okay</button>
         </div>
      </div>
   </div>
</div>
@endsection
