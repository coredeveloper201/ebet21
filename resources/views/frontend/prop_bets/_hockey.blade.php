@extends('layouts.frontend_layout')

@section('title')

@endsection

@section('content')

<!-- main body area start -->
<section id="body_warapper">
    <div class="container">
        <div class="row">
            <div class="col-md-9 col-sm-12 col-xs-12 col-lg-9 col-xl-9">
                @php
                    $prop_data = json_decode(json_encode($prop_bet_details['prop_data']), true);
                    $event_data = $prop_bet_details['event_info'];
                    $event_time = strtotime($event_data->e_date.' '.$event_data->e_time);
                    $event_data->date = _getDate($event_time);
                    $event_data->time = _getTime($event_time);
                    $home_name = $event_data->home_name;
                    $away_name = $event_data->away_name;
                @endphp
                <!-- Upcoming Events area start -->
                <div id="upcoming_events">
                    <div class="row">
                        <div class="col-lg-6 col-xs-6">
                            <div class="upcoming_title">
                                <h2> {{ $away_name }} @ {{$home_name}}</h2>
                            </div>
                        </div>
                        <div class="col-lg-6 col-xs-12">
                            <div class="go_conintue -mobile-devices-previous-button">
                                <a href="{{ url('/straight_odds') }}">Previous</a>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- 1st Period -->
                @if (!empty($prop_data['first_period']))
                    @php
                        $first_period = $prop_data['first_period'];
                    @endphp
                    <div class="first_period">
                        <h3 style="width: 100%" class="tabletitle tabletitle-mobile">First Period</h3>
                        <div class="data-table">
                            <div class="header">
                                <div>Team</div>
                                <div>Spread</div>
                                <div>Money Line</div>
                                <div>Total</div>
                            </div>
                            <div class="body-content">
                                <div class="each-row">
                                    <div class="player_name">{{ $home_name }}</div>
                                    <div>
                                        @if (!isset($first_period['spread']) || empty($first_period['spread']['home']['odd']))
                                            <a>OTB</a>
                                        @else
                                            @php
                                                $first_period['spread']['home']['odd'] = _toAmericanDecimal($first_period['spread']['home']['odd']);
                                                $first_period['spread']['home']['team_name'] = "First Period";
                                                $first_period['spread']['home']['sub_type_name'] ="Spread";
                                                $unique_id = $event_data->id."first-period-77jji99-00-spread";
                                                $first_period['spread']['home']['Unique_id'] = $unique_id;
                                                $first_period['spread']['home']['betting_wager'] = "Spread";
                                                $first_period['spread']['home']['row_line'] = 'home';
                                                $first_period['spread']['home']['handicap'] = $first_period['spread']['home']['handicap'];
                                            @endphp
                                            <span id="{{$unique_id}}"
                                                onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($first_period['spread']['home'])}}','prop_bet')">
                                                {{ $first_period['spread']['home']['handicap'] }} ({{_preccedSign($first_period['spread']['home']['odd'])}})
                                            </span>
                                        @endif
                                    </div>
                                    <div>
                                        @if (!isset($first_period['money']) || empty($first_period['money']['home']))
                                            <a>OTB</a>
                                        @else
                                            @php
                                                $first_period_home = [];
                                                $first_period_home['odd'] = _toAmericanDecimal($first_period['money']['home']);
                                                $first_period_home['team_name'] = "First Period";
                                                $first_period_home['sub_type_name'] ="Money Line";
                                                $unique_id = $event_data->id."1st-half-99977-00-money-line";
                                                $first_period_home['Unique_id'] = $unique_id;
                                                $first_period_home['betting_wager'] = "Money Line";
                                                $first_period_home['row_line'] = 'home';
                                            @endphp
                                            <span id="{{$unique_id}}"
                                                  onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($first_period_home)}}','prop_bet')">
                                                  {{ _preccedSign($first_period_home['odd']) }}
                                            </span>
                                        @endif
                                    </div>
                                    <div>
                                        @if (!isset($first_period['total']) || empty($first_period['total']['home']['odd']))
                                            <a>OTB</a>
                                        @else
                                            @php
                                                $first_period['total']['home']['odd'] =
                                                _toAmericanDecimal($first_period['total']['home']['odd']);
                                                $first_period['total']['home']['team_name'] = $first_period['total']['home']['under_over'];
                                                $first_period['total']['home']['sub_type_name'] ="First Period";
                                                $unique_id = $event_data->id."1st-period-9kkll977-00-total-over-home";
                                                $first_period['total']['home']['Unique_id'] = $unique_id;
                                                $first_period['total']['home']['betting_wager'] = "Total";
                                                $first_period['total']['home']['row_line'] = 'home';
                                            @endphp
                                            <span id="{{$unique_id}}"
                                                onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($first_period['total']['home'])}}','prop_bet')">
                                                {{$first_period['total']['home']['under_over']}} ({{ _preccedSign($first_period['total']['home']['odd']) }})
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="each-row">
                                    <div class="player_name">{{ $away_name}}</div>
                                    <div>
                                        @if (!isset($first_period['spread']) || empty($first_period['spread']['away']['odd']))
                                            <a>OTB</a>
                                        @else
                                            @php
                                                $first_period['spread']['away']['odd'] = _toAmericanDecimal($first_period['spread']['away']['odd']);
                                                $first_period['spread']['away']['team_name'] = "First Period";
                                                $first_period['spread']['away']['sub_type_name'] ="Spread";
                                                $unique_id = $event_data->id."1st-half-78899-11-".$first_period['spread']['away']['sub_type_name'];
                                                $first_period['spread']['away']['Unique_id'] = $unique_id;
                                                $first_period['spread']['away']['betting_wager'] = "Spread";
                                                $first_period['spread']['away']['row_line'] = 'away';
                                                $first_period['spread']['away']['handicap'] = $first_period['spread']['away']['handicap'];
                                            @endphp
                                            <span id="{{$unique_id}}"
                                                onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($first_period['spread']['away'])}}','prop_bet')">
                                                {{$first_period['spread']['away']['handicap'] }} ({{ _preccedSign($first_period['spread']['away']['odd'])}})
                                            </span>
                                        @endif
                                    </div>
                                    <div>
                                        @if (!isset($first_period['money']) || empty($first_period['money']['away']))
                                            <a >OTB</a>
                                        @else
                                            @php
                                                $first_period_away = [];
                                                $first_period_away['odd'] =_toAmericanDecimal($first_period['money']['away']);
                                                $first_period_away['team_name'] = "First Period";
                                                $first_period_away['sub_type_name'] ="Money Line";
                                                $unique_id = $event_data->id."1st-half-96677-11-money-line";
                                                $first_period_away['Unique_id'] = $unique_id;
                                                $first_period_away['betting_wager'] = "Money Line";
                                                $first_period_away['row_line'] = 'away';
                                            @endphp
                                            <span id="{{$unique_id}}"
                                                onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($first_period_away)}}','prop_bet')">
                                                {{_preccedSign($first_period_away['odd'])}}
                                            </span>
                                        @endif
                                    </div>
                                    <div>
                                        @if (!isset($first_period['total']) || empty($first_period['total']['away']['odd']))
                                            <a >OTB</a>
                                        @else
                                            @php
                                                $first_period['total']['away']['odd'] =
                                                _toAmericanDecimal($first_period['total']['away']['odd']);
                                                $first_period['total']['away']['team_name'] = $first_period['total']['away']['under_over'];
                                                $first_period['total']['away']['sub_type_name'] ="First Period";
                                                $unique_id = $event_data->id."first-period-92277-11-total-under";
                                                $first_period['total']['away']['Unique_id'] = $unique_id;
                                                $first_period['total']['away']['betting_wager'] = "Total";
                                                $first_period['total']['away']['row_line'] = 'away';
                                            @endphp
                                            <span id="{{$unique_id}}"
                                                onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($first_period['total']['away'])}}','prop_bet')">
                                                {{  $first_period['total']['away']['under_over'] }} ({{ _preccedSign($first_period['total']['away']['odd'])}})
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif

                @if (!empty($prop_data['highest_scoring_half']))
                    @php
                        $highest_scor_half = $prop_data['highest_scoring_half'];
                    @endphp
                    <div class="highest_scores_half">
                        <h3 style="width: 100%" class="tabletitle tabletitle-mobile">HIGHEST SCORING PERIOD</h3>
                        <div class="data-table">
                            <div class="header">
                                <div> Half </div>
                                <div> Odds </div>
                            </div>
                            <div class="body-content">
                                @foreach ($highest_scor_half as $i=> $hsh)
                                    <div class="each-row">
                                        <div> {{$hsh['name']}}</div>
                                        <div>
                                            @php
                                                $higest_score_half = [];
                                                $higest_score_half['odd'] =
                                                _toAmericanDecimal($hsh['value']);
                                                $higest_score_half['team_name'] = $hsh['name'];
                                                $higest_score_half['sub_type_name'] = "Highest Scoring Period";
                                                $unique_id = $event_data->id."higest-score-half-hockey-92277-".$i."-";
                                                $higest_score_half['Unique_id'] = $unique_id;
                                                $higest_score_half['betting_wager'] = $hsh['name'];
                                                $higest_score_half['row_line'] = 'away';
                                            @endphp
                                            <span id="{{$unique_id}}"
                                                  onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($higest_score_half)}}','prop_bet')">
                                                  {{ _preccedSign( $higest_score_half['odd']) }}
                                            </span>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                @endif
                {{-- acitve team total - home --}}
                @if (!empty($prop_data['home_total_goals']))
                    @php
                         $home_total_goals = $prop_data['home_total_goals'];
                    @endphp
                    <div class="home_total_goals">
                        <h3 style="width: 100%" class="tabletitle tabletitle-mobile">Team Total - {{$home_name}}</h3>
                        <div class="data-table">
                            <div class="header">
                                <div> Over </div>
                                <div> Under </div>
                            </div>
                            <div class="body-content">
                                @foreach ($home_total_goals['home']['Over'] as $i => $v)
                                    <div class="each-row">
                                        <div>
                                            @php
                                                $home_goals_total_over = [];
                                                $home_goals_total_over['odd'] = _toAmericanDecimal($home_total_goals['home']['Over'][$i]);
                                                $home_goals_total_over['team_name'] = "Team Total - Home";
                                                $home_goals_total_over['sub_type_name'] = 'Over';
                                                $unique_id = $event_data->id."1st-half-9gdh277-".$i."-";
                                                $home_goals_total_over['Unique_id'] = $unique_id;
                                                $home_goals_total_over['betting_wager'] = "Over";
                                                $home_goals_total_over['row_line'] = 'home';
                                                $home_goals_total_over['handicap'] = 'Over '.$home_total_goals['home']['wager'][$i];
                                            @endphp
                                            <span id="{{$unique_id}}"
                                                onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($home_goals_total_over)}}','prop_bet')">
                                                {{$home_goals_total_over['handicap']}} ({{_preccedSign($home_goals_total_over['odd'])}})
                                            </span>
                                        </div>
                                        <div>
                                            @php
                                                $home_goals_total_under = [];
                                                $home_goals_total_under['odd'] = _toAmericanDecimal($home_total_goals['home']['Under'][$i]);
                                                $home_goals_total_under['team_name'] =  "Team Total - Home";
                                                $home_goals_total_under['sub_type_name'] = 'Under';
                                                $unique_id = $event_data->id."1st-half-9gdhkl77-".$i."-";
                                                $home_goals_total_under['Unique_id'] = $unique_id;
                                                $home_goals_total_under['betting_wager'] = 'Under';
                                                $home_goals_total_under['row_line'] = 'home';
                                                $home_goals_total_under['handicap'] = 'Under '.$home_total_goals['home']['wager'][$i];
                                            @endphp
                                            <span id="{{$unique_id}}"
                                            onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($home_goals_total_under)}}','prop_bet')">
                                               {{$home_goals_total_under['handicap']}} ({{_preccedSign($home_goals_total_under['odd'])}})
                                            </span>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                @endif


                {{-- acitve team total - away --}}
                @if (!empty($prop_data['away_total_goals']))
                    @php
                        $away_total_goals = $prop_data['away_total_goals'];
                    @endphp
                    <div class="away_total_goals">
                        <h3 style="width: 100%" class="tabletitle tabletitle-mobile">Team Total - {{$away_name}}</h3>
                        <div class="data-table">
                            <div class="header">
                                <div> Over </div>
                                <div> Under </div>
                            </div>
                            <div class="body-content">
                                @foreach ($away_total_goals['away']['Over'] as $i => $v)
                                    <div class="each-row">
                                        <div>
                                            @php
                                                $away_goals_total_over = [];
                                                $away_goals_total_over['odd'] = _toAmericanDecimal($away_total_goals['away']['Over'][$i]);
                                                $away_goals_total_over['team_name'] = "Team Total - Away";
                                                $away_goals_total_over['sub_type_name'] = 'Over';
                                                $unique_id = $event_data->id."1st-half-9gdv277-".$i."-";
                                                $away_goals_total_over['Unique_id'] = $unique_id;
                                                $away_goals_total_over['betting_wager'] = 'Over';
                                                $away_goals_total_over['row_line'] = 'away';
                                                $away_goals_total_over['handicap'] = 'Over '.$away_total_goals['away']['wager'][$i];
                                            @endphp
                                            <span id="{{$unique_id}}"
                                                onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($away_goals_total_over)}}','prop_bet')">
                                                {{$away_goals_total_over['handicap']}} ({{ _preccedSign($away_goals_total_over['odd']) }})
                                            </span>
                                        </div>
                                        <div>
                                            @php
                                                $away_goals_total_under = [];
                                                $away_goals_total_under['odd'] = _toAmericanDecimal($away_total_goals['away']['Under'][$i]);
                                                $away_goals_total_under['team_name'] = "Team Total - Away";
                                                $away_goals_total_under['sub_type_name'] = 'Under';
                                                $unique_id = $event_data->id."1st-half-9gdiokl77-".$i."-";
                                                $away_goals_total_under['Unique_id'] = $unique_id;
                                                $away_goals_total_under['betting_wager'] = 'Under';
                                                $away_goals_total_under['row_line'] = 'away';
                                                $away_goals_total_under['handicap'] = 'Under '.$away_total_goals['away']['wager'][$i];
                                            @endphp
                                            <span id="{{$unique_id}}"
                                                onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($away_goals_total_under)}}','prop_bet')">
                                                {{ $away_goals_total_under['handicap']}} ({{_preccedSign($away_goals_total_under['odd'])}})
                                            </span>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                @endif

                    {{-- inactive --}}
                @if (!empty($prop_data['total_home']))
                    @php
                        $home_total = $prop_data['total_home'];
                    @endphp
                    <div class="home_total_goals">
                        <h3 style="width: 100%" class="tabletitle tabletitle-mobile">Home Total</h3>
                        <div class="data-table">
                            <div class="header">
                                <div> Over </div>
                                <div> Under </div>
                            </div>
                            <div class="body-content">
                                @foreach ($home_total['home']['Over'] as $i => $v)
                                    <div class="each-row">
                                        <div>
                                            @php
                                                $home_total_over = [];
                                                $home_total_over['odd'] = _toAmericanDecimal($home_total['home']['Over'][$i]);
                                                $home_total_over['team_name'] = "Home Total";
                                                $home_total_over['sub_type_name'] = 'over '.$home_total['home']['wager'][$i];
                                                $unique_id = $event_data->id."1st-half-934dh277-".$i."-";
                                                $home_total_over['Unique_id'] = $unique_id;
                                                $home_total_over['betting_wager'] = 'over';
                                                $home_total_over['row_line'] = 'home';
                                            @endphp
                                            <span id="{{$unique_id}}"
                                                onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($home_total_over)}}','prop_bet')">
                                                Over {{$home_total['home']['wager'][$i]}} ({{_preccedSign($home_total_over['odd'])}})
                                            </span>
                                        </div>
                                        <div>
                                            @php
                                                $home_total_under = [];
                                                $home_total_under['odd'] = _toAmericanDecimal($home_total['home']['Under'][$i]);
                                                $home_total_under['team_name'] = "Home Total";
                                                $home_total_under['sub_type_name'] = 'under '.$home_total['home']['wager'][$i];
                                                $unique_id = $event_data->id."1st-half-9ghhkl77-".$i."-";
                                                $home_total_under['Unique_id'] = $unique_id;
                                                $home_total_under['betting_wager'] = 'under';
                                                $home_total_under['row_line'] = 'away';
                                            @endphp
                                            <span id="{{$unique_id}}"
                                            onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($home_total_under)}}','prop_bet')">
                                            Under {{$home_total['home']['wager'][$i]}} ({{_preccedSign($home_total_under['odd'])}})
                                            </span>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                @endif
                    {{-- inactive --}}
                @if (!empty($prop_data['total_away']))
                    @php
                        $away_total = $prop_data['total_away'];
                    @endphp
                    <div class="away_total_goals">
                        <h3 style="width: 100%" class="tabletitle tabletitle-mobile">Away Total</h3>
                        <div class="data-table">
                            <div class="header">
                                <div> Over </div>
                                <div> Under </div>
                            </div>
                            <div class="body-content">
                                @foreach ($away_total['away']['Over'] as $i => $v)
                                    <div class="each-row">
                                        <div>
                                            @php
                                                $away_total_over = [];
                                                $away_total_over['odd'] = _toAmericanDecimal($away_total['away']['Over'][$i]);
                                                $away_total_over['team_name'] = "Away Total Goals";
                                                $away_total_over['sub_type_name'] = 'over '.$away_total['away']['wager'][$i];
                                                $unique_id = $event_data->id."1st-half-9cfbv277-".$i."-";
                                                $away_total_over['Unique_id'] = $unique_id;
                                                $away_total_over['betting_wager'] = 'over';
                                                $away_total_over['row_line'] = 'home';
                                            @endphp
                                            <span id="{{$unique_id}}"
                                                onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($away_total_over)}}','prop_bet')">
                                                Over {{$away_total['away']['wager'][$i]}} ({{ _preccedSign($away_total_over['odd'])}})
                                            </span>
                                        </div>
                                        <div>
                                            @php
                                                $away_total_under = [];
                                                $away_total_under['odd'] = _toAmericanDecimal($away_total['away']['Under'][$i]);
                                                $away_total_under['team_name'] = "Home Total Goals";
                                                $away_total_under['sub_type_name'] = 'under '.$away_total['away']['wager'][$i];
                                                $unique_id = $event_data->id."1st-half-9gdyukl77-".$i."-";
                                                $away_total_under['Unique_id'] = $unique_id;
                                                $away_total_under['betting_wager'] = 'under';
                                                $away_total_under['row_line'] = 'away';
                                            @endphp
                                            <span id="{{$unique_id}}"
                                                onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($away_total_under)}}','prop_bet')">
                                                Under {{$away_total['away']['wager'][$i]}} ({{_preccedSign($away_total_under['odd'])}})
                                            </span>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                @endif

                    {{-- formatting is done --}}
                @if (!empty($prop_data['both_teams_to_score']))
                    @php
                        $both_teams_to_score = $prop_data['both_teams_to_score'];
                    @endphp
                    <div class="odd_even_vollyball">
                        <h3 style="width: 100%" class="tabletitle tabletitle-mobile">Both Team To Score</h3>
                        <div class="data-table">
                            <div class="header">
                                <div> Yes </div>
                                <div> No </div>
                            </div>
                            <div class="body-content">
                                <div class="each-row">
                                    <div>
                                        @php
                                            $b_t_t_s = [];
                                            $b_t_t_s['odd'] = _toAmericanDecimal($both_teams_to_score["Yes"]);
                                            $b_t_t_s['team_name'] = "Yes";
                                            $b_t_t_s['sub_type_name'] = "Both Team To Score";
                                            $unique_id = $event_data->id."both-team-to-Score-77bsgaxu-01-yes";
                                            $b_t_t_s['Unique_id'] = $unique_id;
                                            $b_t_t_s['betting_wager'] = "Yes";
                                            $b_t_t_s['row_line'] = 'home';
                                        @endphp
                                        <span  id="{{$unique_id}}"
                                                onclick="Betslip.addToCart({{json_encode($event_data) }},'{{ json_encode($b_t_t_s)}}','prop_bet')">
                                                {{_preccedSign($b_t_t_s['odd'])}}
                                        </span>
                                    </div>
                                    <div>
                                        @php
                                            $b_t_t_n = [];
                                            $b_t_t_n['odd'] = _toAmericanDecimal($both_teams_to_score["No"]);
                                            $b_t_t_n['team_name'] = "No";
                                            $b_t_t_n['sub_type_name'] = "Both Team To Score";
                                            $unique_id = $event_data->id."both-team-to-Score-77bfffaxu-02-no";
                                            $b_t_t_n['Unique_id'] = $unique_id;
                                            $b_t_t_n['betting_wager'] = "No";
                                            $b_t_t_n['row_line'] = 'away';
                                        @endphp
                                        <span  id="{{$unique_id}}"
                                                onclick="Betslip.addToCart({{json_encode($event_data) }},'{{ json_encode($b_t_t_n)}}','prop_bet')">
                                                {{_preccedSign($b_t_t_n['odd'])}}
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif


                    {{-- inactive --}}
                @if (!empty($prop_data['team_to_score_first']))
                    @php
                        $team_to_score_first = $prop_data['team_to_score_first'];
                    @endphp
                    <div class="team_to_score_first">
                        <h3 style="width: 100%" class="tabletitle tabletitle-mobile">Team To Score First</h3>
                        <div class="data-table">
                            <div class="header">
                              {{--   @foreach ($team_to_score_first as $team_to_e_first)
                                <div> <strong>{{ $team_to_e_first['name']=="Home"?$home_name:$team_to_e_first['name']=="Away"?$away_name:$team_to_e_first['name'] }}</strong> </div>
                                @endforeach --}}
                              <div> {{$home_name}} </div>
                              <div> Draw </div>
                              <div> {{$away_name}} </div>
                            </div>
                            <div class="body-content">
                                    <div class="each-row">
                                        @foreach ($team_to_score_first as $i => $v)
                                        <div>
                                            @php
                                                $t_t_s_f = [];
                                                $t_t_s_f['odd'] = _toAmericanDecimal($v['value']);
                                                $t_t_s_f['team_name'] = "Team To Score First";
                                                $t_t_s_f['sub_type_name'] = $v['name'];
                                                $unique_id = $event_data->id."team-to-score-first-9gdv2kkll77-".$i."-".$v['name'];
                                                $t_t_s_f['Unique_id'] = $unique_id;
                                                $t_t_s_f['betting_wager'] = $v['name'];
                                                $t_t_s_f['row_line'] = 'home'
                                            @endphp
                                            <span id="{{$unique_id}}"
                                                onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($t_t_s_f)}}','prop_bet')">
                                                {{ _preccedSign($t_t_s_f['odd']) }}
                                            </span>
                                        </div>
                                    @endforeach
                                    </div>
                            </div>
                        </div>
                    </div>
                @endif
                    {{-- inactive --}}
                @if (!empty($prop_data['team_to_score_last']))
                    @php
                        $team_to_score_last = $prop_data['team_to_score_last'];
                    @endphp
                    <div class="team_to_score_first">
                        <h3 style="width: 100%" class="tabletitle tabletitle-mobile">Team To Score Last</h3>
                        <div class="data-table">
                            <div class="header">
                                {{-- @foreach ($team_to_score_last as $team_to_e_last)
                                <div> <strong>{{ $team_to_e_last['name'] }}</strong> </div>
                                @endforeach --}}
                                <div> {{$home_name}}</div>
                              <div> Draw</div>
                              <div> {{$away_name}} </div>
                            </div>
                            <div class="body-content">
                                    <div class="each-row">
                                        @foreach ($team_to_score_first as $i => $v)
                                        <div>
                                            @php
                                                $t_t_s_l = [];
                                                $t_t_s_l['odd'] = _toAmericanDecimal($v['value']);
                                                $t_t_s_l['team_name'] = "Team To Score Last";
                                                $t_t_s_l['sub_type_name'] = $v['name'];
                                                $unique_id = $event_data->id."team-to-score-last-9glioh2kkll77-".$i."-";
                                                $t_t_s_l['Unique_id'] = $unique_id;
                                                $t_t_s_l['betting_wager'] = $v['name'];
                                                $t_t_s_l['row_line'] = 'home'
                                            @endphp
                                            <span id="{{$unique_id}}"
                                                onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($t_t_s_l)}}','prop_bet')">
                                                {{ _preccedSign($t_t_s_l['odd']) }}
                                            </span>
                                        </div>
                                    @endforeach
                                    </div>
                            </div>
                        </div>
                    </div>
                @endif


                {{-- acitve odd/even --}}
                @if (!empty($prop_data['odd_even_ot']))
                    @php
                        $odd_even = $prop_data['odd_even_ot'];
                    @endphp
                    <div class="odd_even_vollyball">
                        <h3 style="width: 100%" class="tabletitle tabletitle-mobile">Odd/Even</h3>
                        <div class="data-table">
                            <div class="header">
                                <div> Odd  </div>
                                <div> Even </div>
                            </div>
                            <div class="body-content">
                                <div class="each-row">
                                    <div>
                                        @php
                                            $odd_even['odd']['odd'] = _toAmericanDecimal($odd_even["Odd"]);
                                            $odd_even['odd']['team_name'] = "Odd";
                                            $odd_even['odd']['sub_type_name'] = "Odd/Even";
                                            $unique_id = $event_data->id."odd-even-775gfxu-01-odd";
                                            $odd_even['odd']['Unique_id'] = $unique_id;
                                            $odd_even['odd']['betting_wager'] = "Odd";
                                            $odd_even['odd']['row_line'] = 'home';
                                        @endphp
                                        <span  id="{{$unique_id}}"
                                                onclick="Betslip.addToCart({{json_encode($event_data) }},'{{ json_encode($odd_even['odd'])}}','prop_bet')">
                                                {{_preccedSign($odd_even['odd']['odd'])}}
                                        </span>
                                    </div>
                                    <div>
                                        @php
                                            $odd_even['even']['odd'] = _toAmericanDecimal($odd_even["Odd"]);
                                            $odd_even['even']['team_name'] = "Even";
                                            $odd_even['even']['sub_type_name'] = "Odd/Even";
                                            $unique_id = $event_data->id."odd-even-755ohxu-01-even";
                                            $odd_even['even']['Unique_id'] = $unique_id;
                                            $odd_even['even']['betting_wager'] = "Even";
                                            $odd_even['even']['row_line'] = 'away';
                                        @endphp
                                        <span  id="{{$unique_id}}"
                                                onclick="Betslip.addToCart({{json_encode($event_data) }},'{{ json_encode($odd_even['even'])}}','prop_bet')">
                                                {{_preccedSign($odd_even['even']['odd'])}}
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
                    {{-- inactive --}}
                @if (!empty($prop_data['odd_even']))
                    @php
                        $odd_even = $prop_data['odd_even'];
                    @endphp
                    <div class="odd_even_vollyball">
                        <h3 style="width: 100%" class="tabletitle tabletitle-mobile">Odd/Even</h3>
                        <div class="data-table">
                            <div class="header">
                                <div> Odd </div>
                                <div> Even </div>
                            </div>
                            <div class="body-content">
                                <div class="each-row">
                                    <div>
                                        @php
                                            $odd_even['odd']['odd'] = _toAmericanDecimal($odd_even["Odd"]);
                                            $odd_even['odd']['team_name'] = "Odd Even";
                                            $odd_even['odd']['sub_type_name'] = "Odd";
                                            $unique_id = $event_data->id."odd-even-775gfxu-01-odd";
                                            $odd_even['odd']['Unique_id'] = $unique_id;
                                            $odd_even['odd']['betting_wager'] = "Odd";
                                            $odd_even['odd']['row_line'] = 'home';
                                        @endphp
                                        <span  id="{{$unique_id}}"
                                                onclick="Betslip.addToCart({{json_encode($event_data) }},'{{ json_encode($odd_even['odd'])}}','prop_bet')">
                                                {{_preccedSign($odd_even['odd']['odd'])}}
                                        </span>
                                    </div>
                                    <div>
                                        @php
                                            $odd_even['even']['odd'] = _toAmericanDecimal($odd_even["Odd"]);
                                            $odd_even['even']['team_name'] = "Odd Even";
                                            $odd_even['even']['sub_type_name'] = "Even";
                                            $unique_id = $event_data->id."odd-even-755ohxu-01-even";
                                            $odd_even['even']['Unique_id'] = $unique_id;
                                            $odd_even['even']['betting_wager'] = "Even";
                                            $odd_even['even']['row_line'] = 'away';
                                        @endphp
                                        <span  id="{{$unique_id}}"
                                                onclick="Betslip.addToCart({{json_encode($event_data) }},'{{ json_encode($odd_even['even'])}}','prop_bet')">
                                                {{_preccedSign($odd_even['even']['odd'])}}
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif

                    {{-- formatting is done --}}
                @if (!empty($prop_data['home_team_will_score_a_goal']))
                    @php
                        $home_team_will_score_a_goal = $prop_data['home_team_will_score_a_goal'];
                    @endphp
                    <div class="odd_even_vollyball">
                        <h3 style="width: 100%" class="tabletitle tabletitle-mobile">{{$home_name}} will score a Goal</h3>
                        <div class="data-table">
                            <div class="header">
                                <div> Yes </div>
                                <div> No </div>
                            </div>
                            <div class="body-content">
                                <div class="each-row">
                                    <div>
                                        @php
                                            $h_t_w_sc_a_g = [];
                                            $h_t_w_sc_a_g['odd'] = _toAmericanDecimal($home_team_will_score_a_goal["Yes"]);
                                            $h_t_w_sc_a_g['team_name'] = "Home team will score a Goal";
                                            $h_t_w_sc_a_g['sub_type_name'] = "Yes";
                                            $unique_id = $event_data->id."h_t_w_sc_a_g-775gllfxu-01-yes";
                                            $h_t_w_sc_a_g['Unique_id'] = $unique_id;
                                            $h_t_w_sc_a_g['betting_wager'] = "Yes";
                                            $h_t_w_sc_a_g['row_line'] = 'home';
                                            $h_t_w_sc_a_g['handicap'] = 'Yes';
                                        @endphp
                                        <span  id="{{$unique_id}}"
                                                onclick="Betslip.addToCart({{json_encode($event_data) }},'{{ json_encode($h_t_w_sc_a_g)}}','prop_bet')">
                                                {{_preccedSign($h_t_w_sc_a_g['odd'])}}
                                        </span>
                                    </div>
                                    <div>
                                        @php
                                            $h_t_w_sc_a_g_n = [];
                                            $h_t_w_sc_a_g_n['odd'] = _toAmericanDecimal($home_team_will_score_a_goal["No"]);
                                            $h_t_w_sc_a_g_n['team_name'] = "Home team will score a Goal";
                                            $h_t_w_sc_a_g_n['sub_type_name'] = "No";
                                            $unique_id = $event_data->id."h_t_w_sc_a_g_n-775gljjhhxu-02-no";
                                            $h_t_w_sc_a_g_n['Unique_id'] = $unique_id;
                                            $h_t_w_sc_a_g_n['betting_wager'] = "No";
                                            $h_t_w_sc_a_g_n['row_line'] = 'home';
                                            $h_t_w_sc_a_g_n['handicap'] = 'No';
                                        @endphp
                                        <span  id="{{$unique_id}}"
                                                onclick="Betslip.addToCart({{json_encode($event_data) }},'{{ json_encode($h_t_w_sc_a_g_n)}}','prop_bet')">
                                                {{_preccedSign($h_t_w_sc_a_g_n['odd'])}}
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif

                    {{-- formatting is done --}}
                @if (!empty($prop_data['away_team_will_score_a_goal']))
                    @php
                        $away_team_will_score_a_goal = $prop_data['away_team_will_score_a_goal'];
                    @endphp
                    <div class="odd_even_vollyball">
                        <h3 style="width: 100%" class="tabletitle tabletitle-mobile">{{$away_name}} will score a Goal</h3>
                        <div class="data-table">
                            <div class="header">
                                <div> Yes </div>
                                <div> No </div>
                            </div>
                            <div class="body-content">
                                <div class="each-row">
                                    <div>
                                        @php
                                            $h_t_w_sc_a_g = [];
                                            $h_t_w_sc_a_g['odd'] = _toAmericanDecimal($away_team_will_score_a_goal["Yes"]);
                                            $h_t_w_sc_a_g['team_name'] = "Away team will score a Goal";
                                            $h_t_w_sc_a_g['sub_type_name'] = "Yes";
                                            $unique_id = $event_data->id."a_t_w_sc_a_g-775gllfxu-01-yes";
                                            $h_t_w_sc_a_g['Unique_id'] = $unique_id;
                                            $h_t_w_sc_a_g['betting_wager'] = "Yes";
                                            $h_t_w_sc_a_g['row_line'] = 'away';
                                            $h_t_w_sc_a_g['handicap'] = 'Yes';
                                        @endphp
                                        <span  id="{{$unique_id}}"
                                                onclick="Betslip.addToCart({{json_encode($event_data) }},'{{ json_encode($h_t_w_sc_a_g)}}','prop_bet')">
                                                {{_preccedSign($h_t_w_sc_a_g['odd'])}}
                                        </span>
                                    </div>
                                    <div>
                                        @php
                                            $h_t_w_sc_a_g_n = [];
                                            $h_t_w_sc_a_g_n['odd'] = _toAmericanDecimal($away_team_will_score_a_goal["No"]);
                                            $h_t_w_sc_a_g_n['team_name'] = "Away team will score a Goal";
                                            $h_t_w_sc_a_g_n['sub_type_name'] = "No";
                                            $unique_id = $event_data->id."a_t_w_sc_a_g_n-775gljjhhxu-02-no";
                                            $h_t_w_sc_a_g_n['Unique_id'] = $unique_id;
                                            $h_t_w_sc_a_g_n['betting_wager'] = "No";
                                            $h_t_w_sc_a_g_n['row_line'] = 'away';
                                            $h_t_w_sc_a_g_n['handicap'] = 'No';
                                        @endphp
                                        <span  id="{{$unique_id}}"
                                                onclick="Betslip.addToCart({{json_encode($event_data) }},'{{ json_encode($h_t_w_sc_a_g_n)}}','prop_bet')">
                                                {{_preccedSign($h_t_w_sc_a_g_n['odd'])}}
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            </div>

            <div class="col-3 mobilepostion">
                @include('frontend.bet_slip.slip')
            </div>
        </div>
    </div>
</section>
<!-- main body area start -->

<div class="modal" id="passwordModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Enter Your Password</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <input type="password" class="form-control" id="bet_confirm_password">
                <p id="passwordMessage" style="color: red"></p>
            </div>
            <div class="modal-footer">
                <button class="btn btn-danger" id="submit_confirm_password">Submit</button>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="betNotice">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Bets data changed</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                Bets data has been changed, please review your bets
            </div>
            <div class="modal-footer">
                <button class="btn btn-danger" data-dismiss="modal">Okay</button>
            </div>
        </div>
    </div>
</div>
@endsection
