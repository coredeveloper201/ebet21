@extends('layouts.frontend_layout')
@section('title')
@endsection
@section('content')
<!-- main body area start -->
<section id="body_warapper">
    <div class="container">
        <div class="row">
            <div class="col-md-9 col-sm-12 col-xs-12 col-lg-9 col-xl-9">
                @php
                    $prop_data = json_decode(json_encode($prop_bet_details['prop_data']), true);
                    $event_data = $prop_bet_details['event_info'];
                    $event_time = strtotime($event_data->e_date.' '.$event_data->e_time);
                    $event_data->date = _getDate($event_time);
                    $event_data->time = _getTime($event_time);
                    $home_name = $event_data->home_name;
                    $away_name = $event_data->away_name;
                @endphp
                <!-- Upcoming Events area start -->
                <div id="upcoming_events">
                    <div class="row">
                        <div class="col-lg-6 col-xs-6">
                            <div class="upcoming_title">
                            <h2> {{ $away_name }} @ {{$home_name}}</h2>
                            </div>
                        </div>
                        <div class="col-lg-6 col-xs-12">
                            <div class="go_conintue -mobile-devices-previous-button">
                            <a href="{{ url('/straight_odds') }}">Previous</a>
                            </div>
                        </div>
                    </div>
                </div>

                @if (!empty($prop_data['first_half_goal']))
                    @php
                        $first_half_goal = $prop_data['first_half_goal'];
                    @endphp
                    <div class="home_total_goals">
                        <h3 style="width: 100%" class="tabletitle tabletitle-mobile">First Half Goals</h3>
                        <div class="data-table">
                            <div class="header">
                                <div> Over </div>
                                <div> Under </div>
                            </div>
                            <div class="body-content">
                                @foreach ($first_half_goal['home']['Over'] as $i => $v)
                                    <div class="each-row">
                                        <div>
                                            @php
                                                $home_goals_total_over = [];
                                                $home_goals_total_over['odd'] = _toAmericanDecimal($first_half_goal['home']['Over'][$i]);
                                                $tteam_name = "Over ".$first_half_goal['home']['wager'][$i];
                                                $home_goals_total_over['team_name'] = $tteam_name;
                                                $home_goals_total_over['sub_type_name'] = 'First Half Goals';
                                                $unique_id = $event_data->id."1st-half-9gd9iih277-".$i."-";
                                                $home_goals_total_over['Unique_id'] = $unique_id;
                                                $home_goals_total_over['betting_wager'] = 'over';
                                                $home_goals_total_over['row_line'] = 'home';
                                            @endphp
                                            <span id="{{$unique_id}}"
                                                onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($home_goals_total_over)}}','prop_bet')">
                                                {{$tteam_name}} ({{_preccedSign($home_goals_total_over['odd'])}})</span>
                                        </div>
                                        <div>
                                            @php
                                                $home_goals_total_under = [];
                                                $home_goals_total_under['odd'] = _toAmericanDecimal($first_half_goal['home']['Under'][$i]);
                                                $tteamName = "Under ".$first_half_goal['home']['wager'][$i];
                                                $home_goals_total_under['team_name'] = $tteamName;
                                                $home_goals_total_under['sub_type_name'] = 'First Half Goals';
                                                $unique_id = $event_data->id."1st-half-9giiihkl76j-".$i."-";
                                                $home_goals_total_under['Unique_id'] = $unique_id;
                                                $home_goals_total_under['betting_wager'] = 'under';
                                                $home_goals_total_under['row_line'] = 'away';
                                            @endphp
                                            <span id="{{$unique_id}}"
                                                onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($home_goals_total_under)}}','prop_bet')">
                                                {{ $tteamName }} ({{_preccedSign($home_goals_total_under['odd'])}})
                                            </span>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                @endif


                @if (!empty($prop_data['yes_no']))
                    @php
                        $yes_no = $prop_data['yes_no'];
                    @endphp
                    <div class="odd_even_vollyball">
                        <h3 style="width: 100%" class="tabletitle tabletitle-mobile">Both Team To Score</h3>
                        <div class="data-table">
                            <div class="header">
                                <div> Yes </div>
                                <div> No </div>
                            </div>
                            <div class="body-content">
                                <div class="each-row">
                                    <div>
                                        @php
                                        $yyes_no  = [];
                                            $yyes_no['odd'] = _toAmericanDecimal($yes_no["Yes"]);
                                            $yyes_no['team_name'] = "Yes";
                                            $yyes_no['sub_type_name'] = "Both Team To Score";
                                            $unique_id = $event_data->id."yes-no-77kj6gfxu-01-yes";
                                            $yyes_no['Unique_id'] = $unique_id;
                                            $yyes_no['betting_wager'] = "Yes";
                                            $yyes_no['row_line'] = 'home';
                                        @endphp
                                        <span  id="{{$unique_id}}"
                                                onclick="Betslip.addToCart({{json_encode($event_data) }},'{{ json_encode($yyes_no)}}','prop_bet')">
                                                {{_preccedSign($yyes_no['odd'])}}
                                        </span>
                                    </div>
                                    <div>
                                        @php
                                            $yess_no =[];
                                            $yess_no['odd'] = _toAmericanDecimal($yes_no["No"]);
                                            $yess_no['team_name'] = "No";
                                            $yess_no['sub_type_name'] = "Both Team To Score";
                                            $unique_id = $event_data->id."yes-no-77kjtftxu-01-no";
                                            $yess_no['Unique_id'] = $unique_id;
                                            $yess_no['betting_wager'] = "no";
                                            $yess_no['row_line'] = 'home';
                                        @endphp
                                        <span  id="{{$unique_id}}"
                                                onclick="Betslip.addToCart({{json_encode($event_data) }},'{{ json_encode($yess_no)}}','prop_bet')">
                                                {{_preccedSign($yess_no['odd'])}}
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif


                @if (!empty($prop_data['correct_score']))
                    @php
                        $correct_score = $prop_data['correct_score'];
                    @endphp
                    <div class="highest_scores_half">
                        <h3 style="width: 100%" class="tabletitle tabletitle-mobile">Correct Score</h3>
                        <div class="data-table">
                            <div class="header">
                                <div> {{$home_name}} : {{$away_name}} </div>
                                <div> Odds </div>
                            </div>
                            <div class="body-content">
                                @php
                                    $i = 0 ;
                                @endphp
                                @foreach ($correct_score as $name => $hsq)
                                @php
                                    $i ++;
                                @endphp
                                <div class="each-row">
                                    <div> {{$name}}</div>
                                    <div>
                                        @php
                                            $correct_scr = [];
                                            $correct_scr['odd'] =_toAmericanDecimal($hsq);
                                            $correct_scr['team_name'] = $name;
                                            $correct_scr['sub_type_name'] = "Correct Score";
                                            $unique_id = $event_data->id."correct-scr-9siie77-".$i."-";
                                            $correct_scr['Unique_id'] = $unique_id;
                                            $correct_scr['betting_wager'] = str_replace(':','-',$name);
                                            $correct_scr['row_line'] = 'home';
                                        @endphp
                                        <span id="{{$unique_id}}"
                                            onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($correct_scr)}}','prop_bet')">
                                            {{_preccedSign($correct_scr['odd'])}}
                                        </span>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                @endif

                @if (!empty($prop_data['highest_scoring_half']))
                    @php
                        $highest_scor_half = $prop_data['highest_scoring_half'];
                    @endphp
                    <div class="highest_scores_half">
                        <h3 style="width: 100%" class="tabletitle tabletitle-mobile">Highest Scoring Half</h3>
                        <div class="data-table">
                            <div class="header">
                                <div> Half </div>
                                <div> Odds </div>
                            </div>
                            <div class="body-content">
                                @foreach ($highest_scor_half as $i => $hsh)
                                    <div class="each-row">
                                        <div> {{$hsh['name']}}</div>
                                        <div>
                                            @php
                                                $higest_score_half = [];
                                                $higest_score_half['odd'] =_toAmericanDecimal($hsh['value']);
                                                $higest_score_half['team_name'] = $hsh['name'];
                                                $higest_score_half['sub_type_name'] = "Highest Score Half";
                                                $unique_id = $event_data->id."higest-score-half-soccer-9ssfddf77-".$i."-";
                                                $higest_score_half['Unique_id'] = $unique_id;
                                                $higest_score_half['betting_wager'] = $hsh['name'];
                                                $higest_score_half['row_line'] = 'away';
                                            @endphp
                                            <span id="{{$unique_id}}"
                                                    onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($higest_score_half)}}','prop_bet')">
                                                    {{_preccedSign($higest_score_half['odd'])}}
                                            </span>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                @endif


                @if (!empty($prop_data['first_half_winner']))
                    @php
                        $first_half_winner = $prop_data['first_half_winner'];
                    @endphp
                    <div class="odd_even_vollyball">
                        <h3 style="width: 100%" class="tabletitle tabletitle-mobile">First Half Winner</h3>
                        <div class="data-table">
                            <div class="header">
                                <div> {{$home_name}} </div>
                                <div> {{$away_name}} </div>
                            </div>
                            <div class="body-content">
                                <div class="each-row">
                                    <div>
                                        @php
                                            $f_s_w_h  = [];
                                            $f_s_w_h['odd'] = _toAmericanDecimal($first_half_winner["money"]['home']);
                                            $f_s_w_h['team_name'] = $home_name;
                                            $f_s_w_h['sub_type_name'] = "First Half Winner";
                                            $unique_id = $event_data->id."first-half-winner-dd7kj6gfxu-01-home";
                                            $f_s_w_h['Unique_id'] = $unique_id;
                                            $f_s_w_h['betting_wager'] = "home";
                                            $f_s_w_h['row_line'] = 'home';
                                        @endphp
                                        <span  id="{{$unique_id}}"
                                                onclick="Betslip.addToCart({{json_encode($event_data) }},'{{ json_encode($f_s_w_h)}}','prop_bet')">
                                                {{_preccedSign($f_s_w_h['odd'])}}
                                        </span>
                                    </div>
                                    <div>
                                        @php
                                            $f_s_w_a  = [];
                                            $f_s_w_a['odd'] = _toAmericanDecimal($first_half_winner["money"]['away']);
                                            $f_s_w_a['team_name'] = $away_name;
                                            $f_s_w_a['sub_type_name'] =  "First Half Winner";
                                            $unique_id = $event_data->id."first-half-winner-dd7kjjkfxu-01-away";
                                            $f_s_w_a['Unique_id'] = $unique_id;
                                            $f_s_w_a['betting_wager'] = "away";
                                            $f_s_w_a['row_line'] = 'away';
                                        @endphp
                                       <span  id="{{$unique_id}}"
                                       onclick="Betslip.addToCart({{json_encode($event_data) }},'{{ json_encode($f_s_w_a)}}','prop_bet')">
                                       {{_preccedSign($f_s_w_a['odd'])}}
                                         </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif


                @if (!empty($prop_data['home_total_goals']))
                    @php
                        $home_total_goals = $prop_data['home_total_goals'];
                    @endphp
                    <div class="home_total_goals">
                        <h3 style="width: 100%" class="tabletitle tabletitle-mobile">Total Goals - {{$home_name}} </h3>
                        <div class="data-table">
                            <div class="header">
                                <div> Over </div>
                                <div> Under </div>
                            </div>
                            <div class="body-content">
                                @foreach ($home_total_goals['home']['Over'] as $i => $v)
                                    <div class="each-row">
                                        <div>
                                            @php
                                                $home_goals_total_over = [];
                                                $home_goals_total_over['odd'] = _toAmericanDecimal($home_total_goals['home']['Over'][$i]);
                                                $home_goals_total_over['team_name'] = "Home Total Goals";
                                                $home_goals_total_over['sub_type_name'] = 'over '.$home_total_goals['home']['wager'][$i];
                                                $unique_id = $event_data->id."1st-half-9gdh277-".$i."-";
                                                $home_goals_total_over['Unique_id'] = $unique_id;
                                                $home_goals_total_over['betting_wager'] = 'over';
                                                $home_goals_total_over['row_line'] = 'home';
                                                $home_goals_total_over['handicap'] ='Over '.$home_total_goals['home']['wager'][$i];
                                            @endphp
                                            <span id="{{$unique_id}}"
                                                onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($home_goals_total_over)}}','prop_bet')">
                                                {{$home_goals_total_over['handicap']}} ({{_preccedSign($home_goals_total_over['odd'])}})</span>
                                        </div>
                                        <div>
                                            @php
                                                $home_goals_total_under = [];
                                                $home_goals_total_under['odd'] = _toAmericanDecimal($home_total_goals['home']['Under'][$i]);
                                                $home_goals_total_under['team_name'] = "Home Total Goals";
                                                $home_goals_total_under['sub_type_name'] = 'under '.$home_total_goals['home']['wager'][$i];
                                                $unique_id = $event_data->id."1st-half-9gdhkl76j-".$i."-";
                                                $home_goals_total_under['Unique_id'] = $unique_id;
                                                $home_goals_total_under['betting_wager'] = 'under';
                                                $home_goals_total_under['row_line'] = 'away';
                                                $home_goals_total_under['handicap'] ='Under '.$home_total_goals['home']['wager'][$i];
                                            @endphp
                                            <span id="{{$unique_id}}"
                                                onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($home_goals_total_under)}}','prop_bet')">
                                                {{$home_goals_total_under['handicap']}} ({{_preccedSign($home_goals_total_under['odd'])}})
                                            </span>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                @endif

                @if (!empty($prop_data['away_total_goals']))
                    @php
                        $away_total_goals = $prop_data['away_total_goals'];
                    @endphp
                    <div class="away_total_goals">
                        <h3 style="width: 100%" class="tabletitle tabletitle-mobile">Total Goals - {{$away_name}}</h3>
                        <div class="data-table">
                            <div class="header">
                                <div> Over </div>
                                <div> Under </div>
                            </div>
                            <div class="body-content">
                                @foreach ($away_total_goals['away']['Over'] as $i => $v)
                                    <div class="each-row">
                                        <div>
                                            @php
                                                $away_goals_total_over = [];
                                                $away_goals_total_over['odd'] = _toAmericanDecimal($away_total_goals['away']['Over'][$i]);
                                                $away_goals_total_over['team_name'] = "Away Total Goals";
                                                $away_goals_total_over['sub_type_name'] = 'over '.$away_total_goals['away']['wager'][$i];
                                                $unique_id = $event_data->id."1st-half-9gdv277-".$i."-";
                                                $away_goals_total_over['Unique_id'] = $unique_id;
                                                $away_goals_total_over['betting_wager'] = 'over';
                                                $away_goals_total_over['row_line'] = 'away';
                                                $away_goals_total_over['handicap'] = 'Over '.$away_total_goals['away']['wager'][$i];
                                            @endphp
                                            <span id="{{$unique_id}}"
                                                onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($away_goals_total_over)}}','prop_bet')">
                                                {{$away_goals_total_over['handicap']}} ({{ _preccedSign($away_goals_total_over['odd'])}})
                                            </span>
                                        </div>
                                        <div>
                                            @php
                                                $away_goals_total_under = [];
                                                $away_goals_total_under['odd'] = _toAmericanDecimal($away_total_goals['away']['Under'][$i]);
                                                $away_goals_total_under['team_name'] = "Away Total Goals";
                                                $away_goals_total_under['sub_type_name'] = 'under '.$away_total_goals['away']['wager'][$i];
                                                $unique_id = $event_data->id."1st-half-9gdiiil77-".$i."-";
                                                $away_goals_total_under['Unique_id'] = $unique_id;
                                                $away_goals_total_under['betting_wager'] = 'under';
                                                $away_goals_total_under['row_line'] = 'away';
                                                $away_goals_total_under['handicap'] ='Under '.$away_total_goals['away']['wager'][$i];
                                            @endphp
                                            <span id="{{$unique_id}}"
                                                onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($away_goals_total_under)}}','prop_bet')">
                                                {{$away_goals_total_under['handicap']}} ({{_preccedSign($away_goals_total_under['odd'])}})
                                            </span>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                @endif

                @if (!empty($prop_data['odd_even']))
                    @php
                        $odd_even = $prop_data['odd_even'];
                    @endphp
                    <div class="odd_even_vollyball">
                        <h3 style="width: 100%" class="tabletitle tabletitle-mobile">Odd Even</h3>
                        <div class="data-table">
                            <div class="header">
                                <div> Odd </div>
                                <div> Even </div>
                            </div>
                            <div class="body-content">
                                <div class="each-row">
                                    <div>
                                        @php
                                            $odd_even['odd']['odd'] = _toAmericanDecimal($odd_even["Odd"]);
                                            $odd_even['odd']['team_name'] = "Odd";
                                            $odd_even['odd']['sub_type_name'] = "Odd/Even";
                                            $unique_id = $event_data->id."odd-even-775gfxu-01-odd";
                                            $odd_even['odd']['Unique_id'] = $unique_id;
                                            $odd_even['odd']['betting_wager'] = "Odd";
                                            $odd_even['odd']['row_line'] = 'home';
                                        @endphp
                                        <span  id="{{$unique_id}}"
                                                onclick="Betslip.addToCart({{json_encode($event_data) }},'{{ json_encode($odd_even['odd'])}}','prop_bet')">
                                                {{_preccedSign($odd_even['odd']['odd'])}}
                                        </span>
                                    </div>
                                    <div>
                                        @php
                                            $odd_even['even']['odd'] = _toAmericanDecimal($odd_even["Odd"]);
                                            $odd_even['even']['team_name'] = "Even";
                                            $odd_even['even']['sub_type_name'] = "Odd/Even";
                                            $unique_id = $event_data->id."odd-even-755ohxu-01-even";
                                            $odd_even['even']['Unique_id'] = $unique_id;
                                            $odd_even['even']['betting_wager'] = "Even";
                                            $odd_even['even']['row_line'] = 'away';
                                        @endphp
                                        <span  id="{{$unique_id}}"
                                                onclick="Betslip.addToCart({{json_encode($event_data) }},'{{ json_encode($odd_even['even'])}}','prop_bet')">
                                                {{_preccedSign($odd_even['even']['odd'])}}
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif

                @if (!empty($prop_data['first_half_double_chance']))
                    @php
                        $first_half_double_chance = $prop_data['first_half_double_chance'];
                    @endphp
                    <div class="highest_scores_half">
                        <h3 style="width: 100%" class="tabletitle tabletitle-mobile">First Half Double Chance</h3>
                        <div class="data-table">
                            <div class="header">
                                <div> Name </div>
                                <div> Odds </div>
                            </div>
                            <div class="body-content">
                                @php
                                    $j=0;
                                @endphp
                                @foreach ($first_half_double_chance as $name => $hsh)
                                @php
                                    $j++;
                                @endphp
                                    <div class="each-row">
                                        <div> {{$name}}</div>
                                        <div>
                                            @php
                                                $first_half_double_chanceee = [];
                                                $first_half_double_chanceee['odd'] =_toAmericanDecimal($hsh);
                                                $first_half_double_chanceee['team_name'] =$name;
                                                $first_half_double_chanceee['sub_type_name'] ="First Half Double Chance";
                                                $unique_id = $event_data->id."1st-half-9ssfiidf77-".$j."-";
                                                $first_half_double_chanceee['Unique_id'] = $unique_id;
                                                $first_half_double_chanceee['betting_wager'] = $name;
                                                $first_half_double_chanceee['row_line'] = 'away';
                                            @endphp
                                            <span id="{{$unique_id}}"
                                                    onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($first_half_double_chanceee)}}','prop_bet')">
                                                    {{_preccedSign($first_half_double_chanceee['odd'])}}
                                            </span>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                @endif

                @if (!empty($prop_data['exact_goal_numbers']))
                    @php
                        $exact_goal_numbers = $prop_data['exact_goal_numbers'];
                    @endphp
                    <div class="highest_scores_half">
                        <h3 style="width: 100%" class="tabletitle tabletitle-mobile">exact number of goals</h3>
                        <div class="data-table">
                            <div class="header">
                                <div> Goals </div>
                                <div> Odds </div>
                            </div>
                            <div class="body-content">
                                @php
                                    $j=0;
                                @endphp
                                @foreach ($exact_goal_numbers as $name => $hsh)
                                @php
                                    $j++;
                                @endphp
                                    <div class="each-row">
                                        <div> {{$name}}</div>
                                        <div>
                                            @php
                                                $exact_goals_number = [];
                                                $exact_goals_number['odd'] =_toAmericanDecimal($hsh);
                                                $exact_goals_number['team_name'] =$name;
                                                $exact_goals_number['sub_type_name'] = "Exact Goal Numbers";
                                                $unique_id = $event_data->id."exact_goal_numbers-9ssfkldf77-".$j."-";
                                                $exact_goals_number['Unique_id'] = $unique_id;
                                                $exact_goals_number['betting_wager'] = $name;
                                                $exact_goals_number['row_line'] = 'away';
                                            @endphp
                                            <span id="{{$unique_id}}"
                                                    onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($exact_goals_number)}}','prop_bet')">
                                                    {{_preccedSign($exact_goals_number['odd'])}}
                                            </span>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                @endif
            </div>
            <div class="col-3 mobilepostion">
                @include('frontend.bet_slip.slip')
            </div>
        </div>
    </div>
</section>
<!-- main body area start -->
<div class="modal" id="passwordModal">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <h4 class="modal-title">Enter Your Password</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <input type="password" class="form-control" id="bet_confirm_password">
            <p id="passwordMessage" style="color: red"></p>
         </div>
         <div class="modal-footer">
            <button class="btn btn-danger" id="submit_confirm_password">Submit</button>
         </div>
      </div>
   </div>
</div>
<div class="modal" id="betNotice">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <h4 class="modal-title">Bets data changed</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            Bets data has been changed, please review your bets
         </div>
         <div class="modal-footer">
            <button class="btn btn-danger" data-dismiss="modal">Okay</button>
         </div>
      </div>
   </div>
</div>
@endsection
