@extends('layouts.frontend_layout')
@section('title')
@endsection
@section('content')
<!-- main body area start -->
<section id="body_warapper">
    <div class="container">
        <div class="row">
            <div class="col-md-9 col-sm-12 col-xs-12 col-lg-9 col-xl-9">
                @php
                    $prop_data = json_decode(json_encode($prop_bet_details['prop_data']), true);
                    $event_data = $prop_bet_details['event_info'];
                    $event_time = strtotime($event_data->e_date.' '.$event_data->e_time);
                    $event_data->date = _getDate($event_time);
                    $event_data->time = _getTime($event_time);
                    $home_name = $event_data->home_name;
                    $away_name = $event_data->away_name;
                @endphp
                <!-- Upcoming Events area start -->
                <div id="upcoming_events">
                    <div class="row">
                        <div class="col-lg-6 col-xs-6">
                            <div class="upcoming_title">
                                <h2> {{ $away_name }} @ {{$home_name}}</h2>
                            </div>
                        </div>
                        <div class="col-lg-6 col-xs-12">
                            <div class="go_conintue -mobile-devices-previous-button">
                                <a href="{{ url('/straight_odds') }}">Previous</a>
                            </div>
                        </div>
                    </div>
                </div>
                @if (!empty($prop_data['first_set']))
                    @php
                        $first_half = $prop_data['first_set'];
                    @endphp
                    <h3 style="width: 100%" class="tabletitle tabletitle-mobile">1st set</h3>
                    <div class="volleyball_first_set">
                        <div class="data-table">
                            <div class="header">
                                <div>Player Name</div>
                                <div>Spread</div>
                                <div>Money Line</div>
                                <div>Total</div>
                            </div>
                            <div class="body-content">
                                <div class="each-row">
                                    <div>{{ $home_name }}</div>
                                    <div>
                                        @if (!isset($first_half['spread']) || empty($first_half['spread']['home']['odd']))
                                            <a>OTB</a>
                                        @else
                                            @php
                                                $first_half['spread']['home']['odd'] = _toAmericanDecimal($first_half['spread']['home']['odd']);
                                                $first_half['spread']['home']['team_name'] =  "1st Set";
                                                $first_half['spread']['home']['sub_type_name'] ="Spread";
                                                $unique_id = $event_data->id."1st-Set-77799-00-".$first_half['spread']['home']['sub_type_name'];
                                                $first_half['spread']['home']['Unique_id'] = $unique_id;
                                                $first_half['spread']['home']['betting_wager'] = "Spread";
                                                $first_half['spread']['home']['row_line'] = 'home';
                                                $first_half['spread']['home']['handicap'] = isset($first_half['spread']['home']['handicap'])?$first_half['spread']['home']['handicap']:"";
                                            @endphp
                                            <span id="{{$unique_id}}"
                                                onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($first_half['spread']['home'])}}','prop_bet')">
                                                ({{ $first_half['spread']['home']['handicap'] }})
                                                {{_preccedSign($first_half['spread']['home']['odd'])}}
                                            </span>
                                        @endif
                                    </div>
                                    <div>

                                        @if (!isset($first_half['money']) || empty($first_half['money']['home']))
                                            <a>OTB</a>
                                        @else
                                            @php
                                                $frst_hlf_money = [];
                                                $frst_hlf_money['money']['home']['odd'] = _toAmericanDecimal($first_half['money']['home']);
                                                $frst_hlf_money['money']['home']['team_name'] = "1st Set";
                                                $frst_hlf_money['money']['home']['sub_type_name'] ="Money Line";
                                                $unique_id = $event_data->id."1st-half-99977-00-money-line";
                                                $frst_hlf_money['money']['home']['Unique_id'] = $unique_id;
                                                $frst_hlf_money['money']['home']['betting_wager'] = "Money Line";
                                                $frst_hlf_money['money']['home']['row_line'] = 'home';
                                            @endphp
                                            <span id="{{$unique_id}}"
                                                    onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($frst_hlf_money['money']['home'])}}','prop_bet')">
                                                {{ $frst_hlf_money['money']['home']['odd'] }}
                                            </span>
                                        @endif
                                    </div>
                                    <div>
                                        @if (!isset($first_half['total']) || empty($first_half['total']['home']['odd']))
                                            <a>OTB</a>
                                        @else
                                            @php
                                                $first_half['total']['home']['odd'] =
                                                _toAmericanDecimal($first_half['total']['home']['odd']);
                                                $first_half['total']['home']['team_name'] =  $first_half['total']['home']['under_over'];
                                                $first_half['total']['home']['sub_type_name'] = "1st Set";
                                                $unique_id = $event_data->id."1st-set-999jkh77-00-total-line";
                                                $first_half['total']['home']['Unique_id'] = $unique_id;
                                                $first_half['total']['home']['betting_wager'] = "Total";
                                                $first_half['total']['home']['row_line'] = 'home';
                                            @endphp
                                            <span id="{{$unique_id}}"
                                                onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($first_half['total']['home'])}}','prop_bet')">
                                                ({{  $first_half['total']['home']['under_over'] }})
                                                {{ $first_half['total']['home']['odd'] }}
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="each-row">
                                    <div>{{ $away_name}}</div>
                                    <div>
                                        @if (!isset($first_half['spread']) || empty($first_half['spread']['away']['odd']))
                                            <a>OTB</a>
                                        @else
                                            @php
                                                $first_half['spread']['away']['odd'] = _toAmericanDecimal($first_half['spread']['away']['odd']);
                                                $first_half['spread']['away']['team_name'] = "1st Set";
                                                $first_half['spread']['away']['sub_type_name'] ="Spread";
                                                $unique_id = $event_data->id."1st-half-78899-11-".$first_half['spread']['away']['sub_type_name'];
                                                $first_half['spread']['away']['Unique_id'] = $unique_id;
                                                $first_half['spread']['away']['betting_wager'] = "Spread";
                                                $first_half['spread']['away']['row_line'] = 'away';
                                                $first_half['spread']['away']['handicap'] = isset($first_half['spread']['away']['handicap'])?$first_half['spread']['away']['handicap']:"";
                                            @endphp
                                            <span id="{{$unique_id}}"
                                                onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($first_half['spread']['away'])}}','prop_bet')">
                                                @php
                                                $home_hendicap =    $first_half['spread']['home']['handicap'];
                                                $away_handicap =    $first_half['spread']['away']['handicap'];
                                                      if ($home_hendicap<0) {
                                                     if($away_handicap < 0)
                                                     {
                                                         $away_handicap = (-1 *  $away_handicap);
                                                         $away_handicap = '+'.$away_handicap;
                                                     }
                                                 }
                                                 else {
                                                     $away_handicap = (-1 *  $away_handicap);
                                                 }
                                                 @endphp
                                                ( {{$away_handicap}})
                                                {{ _preccedSign($first_half['spread']['away']['odd'])}}
                                            </span>
                                        @endif
                                    </div>
                                    <div>
                                        @if (!isset($first_half['money']) || empty($first_half['money']['away']))
                                            <a >OTB</a>
                                        @else
                                                @php
                                                    $first_half_money_away = [];
                                                    $first_half_money_away['money']['away']['odd'] =
                                                    _toAmericanDecimal($first_half['money']['away']);
                                                    $first_half_money_away['money']['away']['team_name'] = "1st Set";
                                                    $first_half_money_away['money']['away']['sub_type_name'] ="Money Line";
                                                    $unique_id = $event_data->id."1st-half-96677-11-money-line";
                                                    $first_half_money_away['money']['away']['Unique_id'] = $unique_id;
                                                    $first_half_money_away['money']['away']['betting_wager'] = "Money Line";
                                                    $first_half_money_away['money']['away']['row_line'] = 'away';
                                                @endphp
                                                <span id="{{$unique_id}}"
                                                    onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($first_half_money_away['money']['away'])}}','prop_bet')">
                                                    {{_preccedSign($first_half_money_away['money']['away']['odd'])}}
                                                </span>
                                        @endif
                                    </div>
                                    <div>
                                        @if (!isset($first_half['total']) || empty($first_half['total']['away']['odd']))
                                            <a>OTB</a>
                                        @else
                                            @php
                                                $first_half['total']['away']['odd'] =
                                                _toAmericanDecimal($first_half['total']['away']['odd']);
                                                $first_half['total']['away']['team_name'] =  $first_half['total']['away']['under_over'];
                                                $first_half['total']['away']['sub_type_name'] ="1st Set";
                                                $unique_id = $event_data->id."1st-set-92dfdf277-11-total-away";
                                                $first_half['total']['away']['Unique_id'] = $unique_id;
                                                $first_half['total']['away']['betting_wager'] = "Total";
                                                $first_half['total']['away']['row_line'] = 'away';
                                            @endphp
                                            <span id="{{$unique_id}}"
                                                onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($first_half['total']['away'])}}','prop_bet')">
                                                ({{$first_half['total']['away']['under_over'] }}){{ _preccedSign($first_half['total']['away']['odd'])}}
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif

                @if (!empty($prop_data['team_total']))
                    @if (!empty($prop_data['team_total']['home']))
                        @php
                            $teamTotal = $prop_data['team_total']['home'];
                        @endphp
                        <div class="away_total_goals">
                            <h3 style="width: 100%" class="tabletitle tabletitle-mobile">Team Total - {{$home_name}}</h3>
                            <div class="data-table">
                                <div class="header">
                                    <div> Over </div>
                                    <div> Under </div>
                                </div>
                                <div class="body-content">
                                    <div class="each-row">
                                        <div>
                                            @php
                                                $team_total_over = [];
                                                $team_total_over['total']['home']['odd'] =  _toAmericanDecimal($teamTotal['Over']);
                                                $team_total_over['total']['home']['team_name'] = "Team Total - Home";
                                                $team_total_over['total']['home']['sub_type_name'] ="Over";
                                                $unique_id = $event_data->id."team-total-home-6jkh56-01-over";
                                                $team_total_over['total']['home']['Unique_id'] = $unique_id;
                                                $team_total_over['total']['home']['betting_wager'] = "Over";
                                                $team_total_over['total']['home']['row_line'] = 'home';
                                                $team_total_over['total']['home']['handicap'] = "Over ".$teamTotal['wager'];
                                            @endphp
                                            <span id="{{$unique_id}}"
                                                onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($team_total_over['total']['home'])}}','prop_bet')">
                                                {{$team_total_over['total']['home']['handicap']}} ({{_preccedSign($team_total_over['total']['home']['odd'])}})
                                            </span>
                                        </div>
                                        <div>
                                            @php
                                                $team_total_under = [];
                                                $team_total_under['total']['away']['odd'] =  _toAmericanDecimal($teamTotal['Under']);
                                                $team_total_under['total']['away']['team_name'] = "Team Total - Home";
                                                $team_total_under['total']['away']['sub_type_name'] ="Under";
                                                $unique_id = $event_data->id."team-total-6jkh896-011-under";
                                                $team_total_under['total']['away']['Unique_id'] = $unique_id;
                                                $team_total_under['total']['away']['betting_wager'] = "Under";
                                                $team_total_under['total']['away']['row_line'] = 'home';
                                                $team_total_under['total']['away']['handicap'] = "Under ". $teamTotal['wager'];
                                            @endphp
                                            <span id="{{$unique_id}}"
                                                onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($team_total_under['total']['away'])}}','prop_bet')">
                                                {{$team_total_under['total']['away']['handicap']}} ({{_preccedSign($team_total_under['total']['away']['odd'])}})
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif

                    @if (!empty($prop_data['team_total']['away']))
                        @php
                            $teamTotalaway = $prop_data['team_total']['away'];
                        @endphp
                        <div class="away_total_goals">
                            <h3 style="width: 100%" class="tabletitle tabletitle-mobile">Team Total - {{$away_name}}</h3>
                            <div class="data-table">
                                <div class="header">
                                    <div> Over </div>
                                    <div> Under </div>
                                </div>
                                <div class="body-content">
                                    <div class="each-row">
                                        <div>
                                            @php
                                                $team_total_away_over = [];
                                                $team_total_away_over['total']['home']['odd'] =  _toAmericanDecimal($teamTotalaway['Over']);
                                                $team_total_away_over['total']['home']['team_name'] = "Team Total - Away";
                                                $team_total_away_over['total']['home']['sub_type_name'] ="Over";
                                                $unique_id = $event_data->id."team-total-away-6jkfsdfll56-01-over";
                                                $team_total_away_over['total']['home']['Unique_id'] = $unique_id;
                                                $team_total_away_over['total']['home']['betting_wager'] = "Over";
                                                $team_total_away_over['total']['home']['row_line'] = 'away';
                                                $team_total_away_over['total']['home']['handicap'] =  "Over ".$teamTotalaway['wager'];
                                            @endphp
                                            <span id="{{$unique_id}}"
                                                onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($team_total_away_over['total']['home'])}}','prop_bet')">
                                                {{$team_total_away_over['total']['home']['handicap']}} ({{_preccedSign($team_total_away_over['total']['home']['odd'])}})
                                            </span>
                                        </div>
                                        <div>
                                            @php
                                                $team_total_away_under = [];
                                                $team_total_away_under['total']['away']['odd'] =  _toAmericanDecimal($teamTotalaway['Under']);
                                                $team_total_away_under['total']['away']['team_name'] = "Team Total - Away";
                                                $team_total_away_under['total']['away']['sub_type_name'] ="Under";
                                                $unique_id = $event_data->id."team-total-away-6jkuiu96-011-under";
                                                $team_total_away_under['total']['away']['Unique_id'] = $unique_id;
                                                $team_total_away_under['total']['away']['betting_wager'] = "Under";
                                                $team_total_away_under['total']['away']['row_line'] = 'away';
                                                $team_total_away_under['total']['away']['handicap'] = "Under ".$teamTotalaway['wager'];
                                            @endphp
                                            <span id="{{$unique_id}}"
                                                onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($team_total_away_under['total']['away'])}}','prop_bet')">
                                                {{$team_total_away_under['total']['away']['handicap']}} ({{_preccedSign($team_total_away_under['total']['away']['odd'])}})
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                @endif
                @if (!empty($prop_data['odd_even']))
                @php
                    $odd_even = $prop_data['odd_even'];
                @endphp
                <div class="odd_even_vollyball">
                    <h3 style="width: 100%" class="tabletitle tabletitle-mobile">Odd Even</h3>
                    <div class="data-table">
                        <div class="header">
                            <div> Odd </div>
                            <div> Even </div>
                        </div>
                        <div class="body-content">
                            <div class="each-row">
                                <div>
                                    @php
                                        $odd_even['odd']['odd'] = _toAmericanDecimal($odd_even["Odd"]);
                                        $odd_even['odd']['team_name'] = "Odd";
                                        $odd_even['odd']['sub_type_name'] = "Odd/Even";
                                        $unique_id = $event_data->id."odd-even-775gfxu-01-odd";
                                        $odd_even['odd']['Unique_id'] = $unique_id;
                                        $odd_even['odd']['betting_wager'] = "Odd";
                                        $odd_even['odd']['row_line'] = 'home';
                                    @endphp
                                    <span  id="{{$unique_id}}"
                                            onclick="Betslip.addToCart({{json_encode($event_data) }},'{{ json_encode($odd_even['odd'])}}','prop_bet')">
                                            {{_preccedSign($odd_even['odd']['odd'])}}
                                    </span>
                                </div>
                                <div>
                                    @php
                                        $odd_even['even']['odd'] = _toAmericanDecimal($odd_even["Odd"]);
                                        $odd_even['even']['team_name'] = "Even";
                                        $odd_even['even']['sub_type_name'] = "Odd/Even";
                                        $unique_id = $event_data->id."odd-even-755ohxu-01-even";
                                        $odd_even['even']['Unique_id'] = $unique_id;
                                        $odd_even['even']['betting_wager'] = "Even";
                                        $odd_even['even']['row_line'] = 'away';
                                    @endphp
                                    <span  id="{{$unique_id}}"
                                            onclick="Betslip.addToCart({{json_encode($event_data) }},'{{ json_encode($odd_even['even'])}}','prop_bet')">
                                            {{_preccedSign($odd_even['even']['odd'])}}
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
            </div>
            <div class="col-3 mobilepostion">
                @include('frontend.bet_slip.slip')
            </div>
        </div>
    </div>
</section>
<!-- main body area start -->
<div class="modal" id="passwordModal">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <h4 class="modal-title">Enter Your Password</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <input type="password" class="form-control" id="bet_confirm_password">
            <p id="passwordMessage" style="color: red"></p>
         </div>
         <div class="modal-footer">
            <button class="btn btn-danger" id="submit_confirm_password">Submit</button>
         </div>
      </div>
   </div>
</div>
<div class="modal" id="betNotice">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <h4 class="modal-title">Bets data changed</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            Bets data has been changed, please review your bets
         </div>
         <div class="modal-footer">
            <button class="btn btn-danger" data-dismiss="modal">Okay</button>
         </div>
      </div>
   </div>
</div>
@endsection
