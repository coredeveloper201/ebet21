<div class="bg-overlay" id="popup-optin-1">
    <div class="subscribe-optin">
        <a href="#" class="optin-close">&times;</a>
        <div class="selip">
            <div class="slip_cart">
                <h3> BET SLIP <span id="slip_count"  class="__slip_count">0</span> </h3>
            </div>
            <!-- scroll bet slip  scroll_bet_slip-->
            <div id="dev-slip-container">
                <div id="dev-straightbet-slip-container">
                </div>
                <div id="dev-parlaybet-slip-container">
                </div>
                <div id="dev-roundrobinbet-slip-container">
                </div>
                <div id="dev-teaserbet-slip-container">
                </div>
                <div id="dev-ifbet-slip-container">
                </div>
                <div id="dev-reversebet-slip-container">
                </div>
            </div>
            <div class="bet_slip_empty" style="display:none">
                <h3>Bet Slip is Empty</h3>
                <p>Not sure where to start?</p>
                <a href="#">Learn how to place a bet</a>
            </div>
            <div class="slip-message-box"><span id="cartMessage"></span></div>
            <div class="bet_slip_details">
                <div class="slip_price_table">
                    @if (Auth::user()->freePlay > 0)
                        <div class="slip_checkbox" id="freePlayContainer">
                            <input type="checkbox" name="freeplay" value="1" id="freeplay"> Free Play <br>
                        </div>
                    @endif

                    <table>
                        <tr>
                            <td>Total bets</td>
                            <td><span id="total_bets">0</span></td>
                        </tr>
                        <tr>
                            <td>Total Stake</td>
                            <td><span id="total_stake">0.00</span></td>
                        </tr>
                        <tr>
                            <td>Possible Winning</td>
                            <td> <span id="possible_winning">0.00</span> </td>
                        </tr>
                    </table>
                </div>
                <div class="confirm">
                    <button class="confirm" onclick="Betslip.saveData()">Confirm</button>
                    <a onclick="Betslip.clearBetSlip()"> Clear all selection </a>
                </div>
            </div>
        </div>
        <script id="bet-card-slip" type="text/x-handlebars-template">
            <!-- bet slip add -->
            <div class="bet_slip_add straight_bet_count" id="card_@{{slip_item.slip_item}}">
                <div class="add_title">
                    <table class="bet_slip_title">
                        <tr>
                            <td><span class="tname1">@{{slip_item.teamName}}</span></td>
                            <td>
                                <div>
                                    @{{#if  slip_item.handicap}}
                                    <span class="handcap">@{{slip_item.handicap}}</span>
                                    @{{/if }}
                                    (<span class="odd_sign">@{{slip_item.odd_sign}}</span><span class="odd">@{{slip_item.odd}}</span>)
                                    <a onclick="Betslip.remove_Slip_Item('@{{slip_item.slip_item}}')">
                                        <i class="far fa-times-circle"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <div class="bet_slip_details">
                        <span class="sd33"> @{{slip_item.subType}} </span><br>
                        <span class="sd34">@{{slip_item.away_name}} @ @{{slip_item.home_name}}</span><br>
                        <span class="sd35">@{{slip_item.date}}, @{{slip_item.time}}</span>
                        <br>
                        @{{#ifCond slip_item.sport_id '===' 12}}
                        @{{#ifCond slip_item.subType '!==' "Money Line" }}
                        @{{#ifCond slip_item.bet_type '!==' "Props Bet" }}
                        <span class="buying_points">Buy Points Option</span>
                        <input type="hidden" value="@{{ slip_item.handicap }}">
                        <select class="form-control inputstl" style="margin-top:5px;margin-bottom:10px;height:30px;font-size:12px;padding:0 0 0 2px" onchange="Betslip.Buyingpoint($(this))">
                            <option id="header">Buy Points!</option>
                            <option id="point_0.5">½  (@{{first_changed_odd}}) </option>
                            <option id="point_1">1  (@{{second_changed_odd}})</option>
                            <option id="point_1.5">1½  (@{{third_changed_odd}})</option>
                        </select>
                        @{{/ifCond}}
                        @{{/ifCond}}
                        @{{/ifCond}}
                        @{{#ifCond slip_item.sport_id '===' 18}}
                        @{{#ifCond slip_item.subType '!==' "Money Line" }}
                        @{{#ifCond slip_item.bet_type '!==' "Props Bet" }}
                        <span class="buying_points">Buy Points Option</span>
                        <input type="hidden" value="@{{ slip_item.handicap }}">
                        <select class="form-control inputstl" style="margin-top:5px;margin-bottom:10px;height:30px;font-size:12px;padding:0 0 0 2px" onchange="Betslip.Buyingpoint($(this))">
                            <option id="header">Buy Points!</option>
                            <option id="point_0.5">½  (@{{first_changed_odd}})</option>
                            <option id="point_1">1  (@{{ second_changed_odd }})</option>
                            <option id="point_1.5">1½  (@{{third_changed_odd}})</option>
                        </select>
                        @{{/ifCond}}
                        @{{/ifCond}}
                        @{{/ifCond}}
                    </div>
                    <table class="bet_slip_rat">
                        <tr>
                            <td>
                                <input
                                        type="number"
                                        placeholder="Risk"
                                        id="input_risk_@{{slip_item.slip_item}}"
                                        class="risk_stake_slip form-control"
                                        data-id="@{{slip_item.slip_item}}"
                                        onkeyup="Betslip.riskCal(@{{slip_item.odd}},this)"
                                        value="@{{slip_item.risk_amount}}">
                            </td>
                            <td>
                                <input
                                        type="number"
                                        placeholder="Win"
                                        id="input_win_@{{slip_item.slip_item}}"
                                        class="risk_win_slip form-control"
                                        data-id="@{{slip_item.slip_item}}"
                                        onkeyup="Betslip.winCal(@{{slip_item.odd}},this)"
                                        value="@{{slip_item.win_amount}}">
                            </td>
                        </tr>
                    </table>
                    <div class="user-min-limit d-none" id="limit_check_@{{slip_item.slip_item}}"><p>You can't bet less than 10</p></div>
                </div>
            </div>
        </script>
        <script id="parlaybet-card-slip" type="text/x-handlebars-template">
            <!-- bet slip add -->
            <div class="bet_slip_add" id="card_Parlay">
                <div class="add_title">
                    <div  class="otherbet_slip_title row" style="margin:0">
                        <div class="col-sm-10" style="text-align:left;padding-left:5px"><b><span class="font-size:24px">Parlay</span></b></div>
                        <div class="col-sm-2" style="text-align:right;padding-right:0" ><i class="fa fa-plus" id="toggle" onclick="Betslip.toggleParlay(this)"></i></div>
                    </div>
                    <div id="parlay-content" style="display:none">
                        <div class="otherbet_slip_details"  style="font-size:24px;font-weight:bold">
                            <p><b>Parlay</b>(@{{team_count}} picks)</p>
                        </div>
                        <table class="bet_slip_rat">
                            <tr>
                                <td>
                                    <input
                                            type="number"
                                            placeholder="Risk"
                                            id="parlay_risk"
                                            class="parlayrisk_stake_slip form-control"
                                            onkeyup="Betslip.riskparlayCal(this)"
                                            value="@{{parlayrisk}}">
                                </td>
                                <td>
                                    <input
                                            type="number"
                                            placeholder="Win"
                                            id="parlay_win"
                                            class="parlayrisk_win_slip form-control"
                                            onkeyup="Betslip.winparlayCal(this)"
                                            value="@{{parlaywin}}">
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="user-min-limit d-none" id="limit_check_Parlay"><p>You can't bet less than 10</p></div>
                </div>
            </div>


        </script>
        <script id="RoundRobinbet-card-slip" type="text/x-handlebars-template">
            <!-- bet slip add -->
            <div class="bet_slip_add" id="card_RoundRobin">
                <div class="add_title">
                    <div  class="otherbet_slip_title row" style="margin:0">
                        <div class="col-sm-10" style="text-align:left;padding-left:5px"><b><span class="font-size:24px">Round Robin</span></b></div>
                        <div class="col-sm-2" style="text-align:right;padding-right:0" ><i class="fa fa-plus" id="RoundRobintoggle" onclick="Betslip.toggleRoundRobin(this)"></i></div>
                    </div>
                    <div id="twoteamparlay-content" style="display:none">
                        <div class="otherbet_slip_details"  style="font-size:24px;font-weight:bold">
                            <p><b>2 Team Parlay</b>(@{{team_count}} bets)</p>
                        </div>
                        <table class="bet_slip_rat">
                            <tr>
                                <td>
                                    <input
                                            type="number"
                                            placeholder="Risk"
                                            id="twoteamparlay_risk"
                                            class="parlayrisk_stake_slip form-control"
                                            onkeyup="Betslip.twoparlayriskCal(this)"
                                            value="@{{tworoundrobinrisk}}">
                                </td>
                                <td>
                                    <input
                                            type="number"
                                            placeholder="Win"
                                            id="twoteamparlay_win"
                                            class="parlayrisk_win_slip form-control"
                                            onkeyup="Betslip.twoparlaywinCal(this)"
                                            value="@{{tworoundrobinwin}}">
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div id="threeteamparlay-content" style="display:none">
                    </div>
                    <div id="fourteamparlay-content" style="display:none">
                    </div>
                    <div class="user-min-limit d-none" id="limit_check_RoundRobin"><p>You can't bet less than 10</p></div>
                </div>
            </div>


        </script>
        <script id="ThreeRoundRobinbet-card-slip" type="text/x-handlebars-template">

                <div class="otherbet_slip_details"  style="font-size:24px;font-weight:bold;border-top:none !important;">
                    <p><b>3 Team Parlay</b>(@{{team_count}} bets)</p>
                </div>
                <table class="bet_slip_rat">
                    <tr>
                        <td>
                            <input
                                    type="number"
                                    placeholder="Risk"
                                    id="threeteamparlay_risk"
                                    class="parlayrisk_stake_slip form-control"
                                    onkeyup="Betslip.threeparlayriskCal(this)"
                                    value="@{{threeroundrobinrisk}}" >
                        </td>
                        <td>
                            <input
                                    type="number"
                                    placeholder="Win"
                                    id="threeteamparlay_win"
                                    class="parlayrisk_win_slip form-control"
                                    onkeyup="Betslip.threeparlaywinCal(this)"
                                    value="@{{threeroundrobinwin}}">
                        </td>
                    </tr>
                </table>

        </script>
        <script id="FourRoundRobinbet-card-slip" type="text/x-handlebars-template">
            <div class="otherbet_slip_details"  style="font-size:24px;font-weight:bold;border-top:none !important;">
                <p><b>4 Team Parlay</b>(@{{team_count}} bets)</p>
            </div>
            <table class="bet_slip_rat">
                <tr>
                    <td>
                        <input
                                type="number"
                                placeholder="Risk"
                                id="fourteamparlay_risk"
                                class="parlayrisk_stake_slip form-control"
                                onkeyup="Betslip.fourparlayriskCal(this)"
                                value="@{{fourroundrobinrisk}}" >
                    </td>
                    <td>
                        <input
                                type="number"
                                placeholder="Win"
                                id="fourteamparlay_win"
                                class="parlayrisk_win_slip form-control"
                                onkeyup="Betslip.fourparlaywinCal(this)"
                                value="@{{fourroundrobinwin}}">
                    </td>
                </tr>
            </table>
          </script>
        <script id="teaserbet-card-slip" type="text/x-handlebars-template">
            <div class="bet_slip_add" id="card_Teaser">
                <div class="add_title">
                    <div  class="otherbet_slip_title row" style="margin:0">
                        <div class="col-sm-10" style="text-align:left;padding-left:5px"><b><span class="font-size:24px">Teaser</span></b></div>
                        <div class="col-sm-2" style="text-align:right;padding-right:0" ><i class="fa fa-plus" id="Teasertoggle" onclick="Betslip.toggleTeaser(this)"></i></div>
                    </div>
                    <div id="Teaser-content" style="display:none">
                        <div class="otherbet_slip_details row" >
                            <div class="col-sm-8" ><p><b>Teaser</b>(@{{team_count}} picks)</p></div>
                            <div class="col-sm-4" id="odd" style="text-align:right"><p></p></div>
                        </div>
                        <div id="select" style="margin-bottom:10px">
                            <select class="form-control inputstl" id="points" onchange="Betslip.teaserselected($(this))">
                                @{{#each bettype}}
                                <option>@{{ this}}</option>
                                @{{/each}}
                            </select>
                        </div>
                        <div class="Teaserbet_info teaserbet-content" style="margin-right:0;margin-bottom:10px;">
                                {{--@{{#each obj}}--}}
                                {{--<div class="row" style="margin:5px">--}}
                                    {{--<div class="col-sm-8" style="padding:0"><span class="teaser_info" style="margin-bottom:5px">@{{ this }}</span></div>--}}
                                    {{--<div class="col-sm-4" style="text-align:right"><span class="points"></span></div></div>--}}
                                {{--@{{/each}}--}}
                            <div>
                            </div>
                        </div>
                        <table class="bet_slip_rat">
                            <tr>
                                <td>
                                    <input
                                            type="number"
                                            placeholder="Risk"
                                            id="teaser_risk"
                                            class="parlayrisk_stake_slip form-control"
                                            onkeyup="Betslip.teaserriskCal(this)"
                                            value="@{{teaserrisk}}">
                                </td>
                                <td>
                                    <input
                                            type="number"
                                            placeholder="Win"
                                            id="teaser_win"
                                            class="parlayrisk_win_slip form-control"
                                            onkeyup="Betslip.teaserwinCal(this)"
                                            value="@{{teaserwin}}">
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="user-min-limit d-none" id="limit_check_Teaser"><p>You can't bet less than 10</p></div>
                </div>
            </div>
        </script>
        <script id="ifbet-card-slip" type="text/x-handlebars-template">
            <div class="bet_slip_add" id="card_ifbet">
                <div class="add_title">
                    <div  class="otherbet_slip_title row" style="margin:0">
                        <div class="col-sm-10" style="text-align:left;padding-left:5px"><b><span class="font-size:24px">IfBet</span></b></div>
                        <div class="col-sm-2" style="text-align:right;padding-right:0" ><i class="fa fa-plus" id="IfBettoggle" onclick="Betslip.toggleIfBet(this)"></i></div>
                    </div>
                    <div id="IfBet-content" style="display:none">
                        <div class="otherbet_slip_details"  style="font-size:24px;font-weight:bold;display:block">
                            <p><b>IfBet</b>(@{{team_count}} picks)</p>
                        </div>
                        <table class="bet_slip_rat">
                            <tr>
                                <td>
                                    <input
                                            type="number"
                                            placeholder="Risk"
                                            id="ifbet_risk"
                                            class="parlayrisk_stake_slip form-control"
                                            onkeyup="Betslip.IfBetriskCal(this)"
                                            value="@{{ifbetrisk}}">
                                </td>
                                <td>
                                    <input
                                            type="number"
                                            placeholder="Win"
                                            id="ifbet_win"
                                            class="parlayrisk_win_slip form-control"
                                            onkeyup="Betslip.IfBetwinCal(this)"
                                            value="@{{ifbetwin}}">
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="user-min-limit d-none" id="limit_check_ifbet"><p>You can't bet less than 10</p></div>
                </div>
            </div>
        </script>
        <script id="reversebet-card-slip" type="text/x-handlebars-template">
            <div class="bet_slip_add" id="card_reversebet">
                <div class="add_title">
                    <div  class="otherbet_slip_title row" style="margin:0">
                        <div class="col-sm-10" style="text-align:left;padding-left:5px"><b><span class="font-size:24px">ReverseBet</span></b></div>
                        <div class="col-sm-2" style="text-align:right;padding-right:0" ><i class="fa fa-plus" id="ReverseBettoggle" onclick="Betslip.toggleReverseBet(this)"></i></div>
                    </div>
                    <div id="ReverseBet-content" style="display:none">
                        <div class="otherbet_slip_details"  style="font-size:24px;font-weight:bold;display:block">
                            <p><b>ReverseBet</b>(@{{team_count}} picks)</p>
                        </div>
                        <table class="bet_slip_rat">
                            <tr>
                                <td>
                                    <input
                                            type="number"
                                            placeholder="Risk"
                                            id="reversebet_risk"
                                            class="parlayrisk_stake_slip form-control"
                                            onkeyup="Betslip.ReverseBetriskCal(this)"
                                            value="@{{reversebetrisk}}">
                                </td>
                                <td>
                                    <input
                                            type="number"
                                            placeholder="Win"
                                            id="reversebet_win"
                                            class="parlayrisk_win_slip form-control"
                                            onkeyup="Betslip.ReverseBetwinCal(this)"
                                            value="@{{reversebetwin}}">
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="user-min-limit d-none" id="limit_check_reversebet"><p>You can't bet less than 10</p></div>
                </div>
            </div>
        </script>
    </div>
</div>

<a href="#popup-optin-1" class="popup-live">SLIP <span id="slip_count" class="__slip_count">0</span></a>
