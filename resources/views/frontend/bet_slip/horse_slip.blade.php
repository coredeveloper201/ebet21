<div class="bg-overlay" id="popup-optin-1">
    <div class="subscribe-optin">
        <a href="#" class="optin-close">&times;</a>
        <div class="selip">
            <div class="slip_cart">
                <h3> BET SLIP <span id="horse_slip_count"  class="horse_slip_count">0</span> </h3>
            </div>
            <!-- scroll bet slip  scroll_bet_slip-->
            <div id="dev-slip-container">
                <div id="dev-horsebet-slip-container" >
                </div>
            </div>
            <div class="horse_bet_slip_empty" style="display:block">
                <h3>Bet Slip is Empty</h3>
                <p>Not sure where to start?</p>
                <a href="#">Learn how to place a bet</a>
            </div>
            <div class="slip-message-box"><span id="cartMessage"></span></div>
            <div class="bet_slip_details">
                <div class="slip_price_table">
                    <table>
                        <tr>
                            <td>Total bets</td>
                            <td><span id="horse_total_bets">0</span></td>
                        </tr>
                        <tr>
                            <td>Total Stake</td>
                            <td><span id="horse_total_stake">0.00</span></td>
                        </tr>
                    </table>
                </div>
                <div class="confirm">
                    <button class="confirm" onclick="HorseBetslip.saveHorseBetData()">Confirm</button>
                    <a onclick="HorseBetslip.clearBetSlip()"> Clear all selection </a>
                </div>
            </div>
        </div>
        <script id="horse-bet-card-slip" type="text/x-handlebars-template">
            <!-- bet slip add -->
            <div class="horse_bet_slip_add horse_bet_count" id="card_@{{ horse_slip_item.unique_id}}@{{horse_slip_item.random_id }}">
                <div class="add_title">
                    <table class="horse_bet_slip_title" style="margin-bottom:10px">
                        <tr>
                            <td style="padding-left:10px"><span class="tname1">@{{horse_slip_item.horse_num}} @{{horse_slip_item.horse_name}}</span>
                                <span class="sd33">@{{ horse_slip_item.Race_type }}</span>
                                <span class="sd34">@{{ horse_slip_item.tournament_name }}-@{{ horse_slip_item.Race_name }}</span>
                                <span class="sd35">@{{ horse_slip_item.Race_date }}</span>
                            </td>
                            <td style="text-align:right;color:red">
                                    <a onclick="HorseBetslip.removeHorseCart('@{{horse_slip_item.unique_id }}','@{{horse_slip_item.created_at }}',this)">
                                        <i class="far fa-times-circle"></i>
                                    </a>
                            </td>
                        </tr>
                    </table>
                    <table class="bet_slip_rat">
                        <tr>
                            <td>
                                <input
                                        type="number"
                                        placeholder="Risk"
                                        id="@{{ horse_slip_item.unique_id }},@{{ horse_slip_item.created_at}}"
                                        class="risk_stake_slip form-control"
                                        data-id=""
                                        onkeyup="HorseBetslip.InputRisk(this)"
                                        value="">
                            </td>
                        </tr>
                    </table>
                    <div class="user-min-limit d-none" id="limit_check_@{{ horse_slip_item.unique_id}}@{{ horse_slip_item.random_id}}"><p>You can't bet less than 10</p></div>
                </div>
            </div>
        </script>
    </div>
</div>

<a href="#popup-optin-1" class="popup-live">SLIP <span id="slip_count" class="__slip_count">0</span></a>
