<!DOCTYPE html>
<html>

<meta http-equiv="content-type" content="text/html;charset=utf-8" />

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@yield('title')</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('admin/plugins') }}/fontawesome-free/css/all.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{ asset('admin/dist') }}/css/ionicons.min.css">
    <!-- Tempusdominus Bbootstrap 4 -->
    <link rel="stylesheet" href="{{ asset('admin/plugins') }}/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{ asset('admin/plugins') }}/icheck-bootstrap/icheck-bootstrap.min.css">
    <!-- JQVMap -->
    <link rel="stylesheet" href="{{ asset('admin/plugins') }}/jqvmap/jqvmap.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('admin/dist') }}/css/adminlte.min.css">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="{{ asset('admin/plugins') }}/overlayScrollbars/css/OverlayScrollbars.min.css">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="{{ asset('admin/plugins') }}/daterangepicker/daterangepicker.css">
    <!-- summernote -->
    <link rel="stylesheet" href="{{ asset('admin/plugins') }}/summernote/summernote-bs4.css">
    <link rel="stylesheet" href="{{ asset('admin/plugins') }}/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">
    <link rel="stylesheet" href="{{ asset('admin/plugins') }}/toastr/toastr.min.css">
    <link rel="stylesheet" href="{{ asset('admin/plugins') }}/datatables-bs4/css/dataTables.bootstrap4.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <!--  style css -->
    <link rel="stylesheet" href="{{ asset('admin/minified') }}/css/main-mini.css">
    <!-- page css -->
    @yield('page_css')
    <script>
        var baseUrl = "{{url('/')}}";
    </script>
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

    <!-- Header Navbar -->
    @include('admin.include.header')
    <!-- / Header navbar -->

    <!-- Main Sidebar Container -->
    @include('admin.include.navbar')
    <!-- / Main Sidebar Container -->

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="card card-body mb-xl-0 mb-lg-0 breadcum">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">@yield('body-title')</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ url('/home') }}">Home</a></li>
                            @yield('breadcrumb')
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div>
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->


        <!-- Main content -->
        @yield('content')
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- footer  -->
    @include('admin.include.footer')
    <!-- / footer  -->

</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="{{ asset('admin/plugins') }}/jquery/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{ asset('admin/plugins') }}/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="{{ asset('admin/plugins') }}/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- ChartJS -->
<script src="{{ asset('admin/plugins') }}/chart.js/Chart.min.js"></script>
<!-- Sparkline -->
<script src="{{ asset('admin/plugins') }}/sparklines/sparkline.js"></script>
<!-- JQVMap -->
<script src="{{ asset('admin/plugins') }}/jqvmap/jquery.vmap.min.js"></script>
<script src="{{ asset('admin/plugins') }}/jqvmap/maps/jquery.vmap.usa.js"></script>
<!-- jQuery Knob Chart -->
<script src="{{ asset('admin/plugins') }}/jquery-knob/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="{{ asset('admin/plugins') }}/moment/moment.min.js"></script>
<script src="{{ asset('admin/plugins') }}/daterangepicker/daterangepicker.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="{{ asset('admin/plugins') }}/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Summernote -->
<script src="{{ asset('admin/plugins') }}/summernote/summernote-bs4.min.js"></script>
<!-- overlayScrollbars -->
<script src="{{ asset('admin/plugins') }}/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="{{ asset('admin/dist') }}/js/adminlte.js"></script>

<!-- AdminLTE for demo purposes -->
<script src="{{ asset('admin/dist') }}/js/demo.js"></script>

<script src="{{ asset('admin/plugins') }}/sweetalert2/sweetalert2.min.js"></script>
<script src="{{ asset('admin/plugins') }}/toastr/toastr.min.js"></script>

<!-- DataTables -->
<script src="{{ asset('admin/plugins') }}/datatables/jquery.dataTables.js"></script>
<script src="{{ asset('admin/plugins') }}/datatables-bs4/js/dataTables.bootstrap4.js"></script>

<!-- custom minified.js App -->
<script src="{{ asset('admin/minified') }}/js/main-mini.js"></script>
<script>
    $(function () {
        $("#example1").DataTable();
        $(".myTbl").DataTable();
    });
</script>

<!-- page js -->
@yield('page_js')

@if (session('success'))
    <script>
        $(function() {
            const Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 5000
            });
            toastr.success("{{ session('success') }}");
        });
    </script>
@elseif(session('error'))
    <script>
        $(function() {
            const Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 5000
            });
            toastr.error("{{ session('error') }}")
        });
    </script>

@elseif(session('warning'))
    <script>
        $(function() {
            const Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 5000
            });
            toastr.warning("{{ session('warning') }}")
        });
    </script>
@endif

@if(Auth::check())
    @if(Auth::user()->role != 'admin')
        <script>
            function checkMaintain() {
                $.ajax({
                    url: "{{ route('maintenance.check') }}",
                    type: 'get',
                    dataType: 'json',
                    success: function (res) {
                        if (res.success) {
                            window.location.reload();
                        }
                    }
                });
            }
            $(document).ready(function () {
                setTimeout(function () {
                    checkMaintain();
                }, 100);

                setInterval(function () {
                    checkMaintain();
                }, 60000);
            });
        </script>
    @endif
@endif

</body>

</html>
