<!DOCTYPE HTML>
<html lang="en-US">

<head>
    <meta charset="UTF-8">
    <title>
        @hasSection('title')
            @yield('title')
        @else
            {{ env('APP_NAME') }}
        @endif
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Font name -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,700,900&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,700,900&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900&display=swap" rel="stylesheet">


    <script src="{{ asset('assets') }}/js/handlebars.js"></script>
    {{-- <script src="{{ asset('assets') }}/js/jquery-3.3.1.min.js"></script> --}}
    <script src="{{ asset('assets') }}/js/jquery3.4.1.min.js"></script>
   {{--  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> --}}
    <script src="{{ asset('assets') }}/js/angular-1.7.8.min.js"></script>
    <script src="{{ asset('assets') }}/js/idle-timer.js"></script>
    <link rel="stylesheet" href="{{ asset('front_end') }}/css/main.css" />
    <link rel="stylesheet" href="{{ asset('front_end') }}/css/main.css" />
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <script>
        var CSRF = '{{ csrf_token() }}';
    </script>
    @yield('page_css')
</head>

<body>

<div id="bichi" class="body-overlay hide-idle-timer">
    <div class="idle-timer-container">
        <div class="idle-timer">
            <h1>We noticed you have been inactive for the past 5 minuets, you are going to be logged out in</h1>
            <div> <span  id="sessionSecondsRemaining">120</span> seconds.</p></div>
            <div style="display: flex">
                <button id="extendSession" type="button" class="come-back-button">Stay Online</button>

                <button id="logoutSession" type="button" class="go-log-out-button">Logout</button>
            </div>

        </div>
    </div>
</div>

    <!-- Header topbar area start -->
    @include('frontend.include.header')
    <!-- Header topbar area end -->

    <!-- Navigation area start -->
    @include('frontend.include.navbar')
    <!-- Navigation area end -->

    <!-- main body area start -->
    @yield('content')
    <!-- main body area start -->

    <!-- Footer area start -->
    @include('frontend.include.footer')
    <!-- Footer area end -->
    {{--Onclick-top--}}
    <div id="logo-scroll" class="top-click" style="display: none;">
        <i class="fa fa-chevron-up"></i>
    </div>
    {{--Onclick-top--}}
    <!-- main js file -->

    <script type="text/javascript" src="{{ asset('assets') }}/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="{{ asset('front_end') }}/js/main.js"></script>
    @if(Auth::check())
        @php
                $straight_min = (!empty(Auth::user()->creditLimits->straight_min_amount))?Auth::user()->creditLimits->straight_min_amount:0;
                $straight_max = (!empty(Auth::user()->creditLimits->straight_max_amount))?Auth::user()->creditLimits->straight_max_amount:0;
                $credit_limit = (!empty(Auth::user()->credit_limit))?Auth::user()->credit_limit:0;

        @endphp
    @if(Auth::user()->role != 'admin')
    <script>
        function checkMaintain() {
                $.ajax({
                    url: "{{ route('maintenance.check') }}",
                    type: 'get',
                    dataType: 'json',
                    success: function (res) {
                        if (res.success) {
                            window.location.reload();
                        }
                    }
                });
            }

           /*  $(document).ready(function () {
                setTimeout(function () {
                    checkMaintain();
                }, 100);

                setInterval(function () {
                    checkMaintain();
                }, 60000);
            }); */
    </script>
    @endif
        <script>
            (function () {
                const userObject = {
                        straight_min : <?=$straight_min?>,
                        straight_max : <?=$straight_max?>,
                        credit_limit : <?=$credit_limit?>
                };
                localStorage.setItem("user_limits",JSON.stringify(userObject));
            })();
        </script>
    @endif
    @yield('page_js')
</body>

</html>
