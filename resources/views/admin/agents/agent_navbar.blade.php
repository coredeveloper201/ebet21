<ul class="nav nav-tabs" id="custom-tabs-two-tab" role="tablist">
    <li class="nav-item">
        <a href="{{ url('admin/agent/'.$data->id) }}/edit" class="nav-link {{ Request::is('admin/agent/*/edit') ? 'active':'' }}">Edit Profile</a>
    </li>
    <li class="nav-item">
        <a href="{{ url('admin/agentassign/transaction/'.$data->id) }}" class="nav-link {{ Request::is('admin/agentassign/transaction*') ? 'active':'' }}">Transaction</a>
    </li>
    <li class="nav-item">
        <a href="{{ url('admin/agentassign/internet-log/'.$data->id) }}" class="nav-link {{ Request::is('admin/agentassign/internet-log*') ? 'active':'' }}">Internet Log</a>
    </li>
    <li class="nav-item">
        <a href="{{ url('admin/agentassign/distribution/'.$data->id) }}" class="nav-link {{ Request::is('admin/agentassign/distribution*') ? 'active':'' }}"> Distribution</a>
    </li>
    <li class="nav-item">
        <a href="{{ url('admin/agentassign/delete-account/'.$data->id) }}"  class="nav-link {{ Request::is('admin/agentassign/delete-account*') ? 'active':'' }}"> Delete Account</a>
    </li>
</ul>
