@extends('layouts.admin_layout')


@section('title')
    Admin / Weekly Figures
@endsection

@section('body-title')
    Weekly Figures
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item active">Weekly Figures</li>
@endsection

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="card">
                <div class="card-header">
                    <form action="" id="sumbmitofmr" method="get">
                        <table>
                            <tbody>
                            <tr>
                                <td>
                                    <select class="form-control" name="weekdays">
                                        <option>Current Week</option>
                                        <option value="2019-10-15,2019-10-21">10/15/2019</option>
                                        <option value="2019-10-08,2019-10-14">10/08/2019</option>
                                        <option value="2019-10-01,2019-10-07">10/01/2019</option>
                                        <option value="2019-09-24,2019-09-30">09/24/2019</option>
                                        <option value="2019-09-17,2019-09-23">09/17/2019</option>
                                        <option value="2019-09-17,2019-09-23">09/17/2019</option>
                                    </select>
                                </td>
                                <td><a href="" class="btn btn-default">Current Week</a></td>
                                <td><a href="" class="btn btn-primary">Last Week</a> </td>
                            </tr>
                            </tbody>
                        </table>
                    </form>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="example1" data-page-length='50' class="table table-bordered table-striped">
                            <thead>
                            <tr role="row">
                                <th>S No.</th>
                                <th>Name</th>
                                <th>Tue</th>
                                <th>Wed</th>
                                <th>Thu</th>
                                <th>Fri</th>
                                <th>Sat</th>
                                <th>Sun</th>
                                <th>Mon</th>
                                <th>Weekly</th>
                                <th>Pending</th>
                                <th>Payments</th>
                                <th>Balance</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>1</td>
                                <td>test</td>
                                <td>0</td>
                                <td>290</td>
                                <td>0</td>
                                <td>0</td>
                                <td>0</td>
                                <td>0</td>
                                <td>0</td>
                                <td>290</td>
                                <td>137.00</td>
                                <td>247</td>
                                <td>6213.35</td>
                            </tr>
                            </tbody>

                        </table>
                    </div>
                </div>
                <!-- /.card-body -->
            </div>
        </div><!-- /.container-fluid -->
    </section>


    <!-- /.modal -->
@endsection

@section('page_js')

@endsection



