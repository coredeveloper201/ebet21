@extends('layouts.admin_layout')


@section('title')
Admin / Delete Wagers
@endsection

@section('body-title')
Delete Wagers
@endsection

@section('breadcrumb')
<li class="breadcrumb-item active">Delete Wagers</li>
@endsection

@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="card">
            <!-- /.card-header -->
            <div class="card-body">
                <div class="table-responsive">
                    <table id="example1" data-page-length='50' class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>S No.</th>
                                <th>Username</th>
                                <th>Description</th>
                                <th>Risk/Win</th>
                                <th>Delete By</th>
                                <th>Delete Date</th>
                            </tr>
                        </thead>
                        <tbody>
                                @foreach ($wagers as $wager)
                                <tr>
                                <td>{{$loop->iteration}}</td>
                                      <td><a>{{ $wager->username }}</a></td>
                                      <td>@php ($sportLeague = json_decode($wager->sport_league, true))
                                              @php ($extraInfo = json_decode($wager->bet_extra_info))
                                              {{ sportsName($sportLeague[0])."-".$sportLeague[1] }}<br>
                                              @if(!empty($extraInfo))
                                                  {{ $extraInfo->bet_on_team_name." VS ". $extraInfo->other_team_name }}<br>
                                                  {{ _getDate(strtotime($wager->created_at)) }} ({{ _getTime(strtotime($wager->created_at)) }})
                                                  <span style="color:#e26c32;">
                                                      @if($wager->result==1)
                                                          Win
                                                      @elseif($wager->result==2)
                                                          Loss
                                                      @endif
                                                  </span>
                                                  <br>
                                                  <strong style="font-weight: bold;">{{ $extraInfo->betting_wager.' '.$wager->betting_condition_original }}</strong>
                                                  for <strong style="font-weight:bold;"> {{ $extraInfo->bet_on_team_name }} </strong>
                                              @endif</b></td>
                                      <td> {{ $wager->risk_amount }} / {{ $wager->betting_win }}</td>
                                      <td>  </td>
                                      <td>{{ _getDate(strtotime($wager->deleted_at)) }} {{ _getTime(strtotime($wager->deleted_at)) }}</td>
                                  </tr>
                              @endforeach
                        </tbody>

                    </table>
                </div>
            </div>
            <!-- /.card-body -->
        </div>
    </div><!-- /.container-fluid -->
</section>


<!-- /.modal -->
@endsection

@section('page_js')

@endsection
