@extends('layouts.admin_layout')

@section('title')
    Admin Info
@endsection

@section('body-title')
    Admin Info
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('admin-manage.index') }}">Admin Manage</a></li>
    <li class="breadcrumb-item active">Admin Info</li>
@endsection

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-sm-12 col-lg-12">
                    <div class="card card-primary card-outline card-outline-tabs">
                        <div class="card-header p-0 border-bottom-0">
                            <ul class="nav nav-tabs" id="custom-tabs-three-tab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="custom-tabs-three-home-tab" data-toggle="pill" href="#custom-tabs-three-home" role="tab" aria-controls="custom-tabs-three-home" aria-selected="true">Betting</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="custom-tabs-three-profile-tab" data-toggle="pill" href="#custom-tabs-three-profile" role="tab" aria-controls="custom-tabs-three-profile" aria-selected="false">Coin</a>
                                </li>
                            </ul>
                        </div>
                        <div class="card-body">
                            <div class="tab-content" id="custom-tabs-three-tabContent">
                                <div class="tab-pane fade show active" id="custom-tabs-three-home" role="tabpanel" aria-labelledby="custom-tabs-three-home-tab">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="card-primary card-outline">
                                                <div class="box">
                                                    <div class="box-header mt-4">
                                                        <h3 class="box-title">Now Betting</h3>
                                                    </div><!-- /.box-header -->
                                                    <div class="box-body no-padding">
                                                        <table class="table table-striped">
                                                            <tbody><tr>
                                                                <th>Game ID</th>
                                                                <th>Game Title</th>
                                                                <th style="width: 40px">COIN</th>
                                                            </tr>
                                                            <tr>
                                                                <td>--</td>
                                                                <td>--</td>
                                                                <td>--</td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div><!-- /.box-body -->
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="card-primary card-outline">
                                                <div class="card">
                                                    <div class="card-header">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <h3 class="card-title">Closed Games</h3>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="card-tools" style="float: right">
                                                                    <div class="btn-group pull-right">
                                                                        <button type="button" id="all-closed-bets-button" class="btn btn-sm btn-default active">All (0)</button>
                                                                        <button type="button" id="all-win-bets-button" class="btn btn-sm btn-default">Won (0)</button>
                                                                        <button type="button" id="all-lose-bets-button" class="btn btn-sm btn-default">Lost (0)</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>


                                                    </div><!-- /.box-header -->
                                                    <div class="card-body no-padding" id="all-closed-bets">
                                                        <table class="table table-striped">
                                                            <tbody><tr>
                                                                <th>Game ID</th>
                                                                <th>Game Title</th>
                                                                <th>COIN</th>
                                                                <th style="width: 40px">Result</th>
                                                            </tr>
                                                            <tr>
                                                                <td>--</td>
                                                                <td>--</td>
                                                                <td>--</td>
                                                                <td>--</td>
                                                            </tr>
                                                            </tbody></table>
                                                    </div><!-- /.box-body -->

                                                    <div class="card-body no-padding" id="all-win-bets" style="display:none">
                                                        <table class="table table-striped">
                                                            <tbody><tr>
                                                                <th>Game ID</th>
                                                                <th>Game Title</th>
                                                                <th>COIN</th>
                                                                <th style="width: 40px">Result</th>
                                                            </tr>
                                                            <tr>
                                                                <td>--</td>
                                                                <td>--</td>
                                                                <td>--</td>
                                                                <td>--</td>
                                                            </tr>
                                                            </tbody></table>
                                                    </div><!-- /.box-body -->

                                                    <div class="card-body no-padding" id="all-lose-bets" style="display:none">
                                                        <table class="table table-striped">
                                                            <tbody><tr>
                                                                <th>Game ID</th>
                                                                <th>Game Title</th>
                                                                <th>COIN</th>
                                                                <th style="width: 40px">Result</th>
                                                            </tr>
                                                            <tr>
                                                                <td>--</td>
                                                                <td>--</td>
                                                                <td>--</td>
                                                                <td>--</td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div><!-- /.box-body -->

                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="card-primary card-outline">
                                                <div class="box">
                                                    <div class="card-header">
                                                        <h3 class="card-title-title">Liked Games</h3>
                                                    </div><!-- /.box-header -->
                                                    <div class="card-body no-padding">
                                                        <table class="table table-striped">
                                                            <tbody><tr>
                                                                <th>Game ID</th>
                                                                <th>Game Title</th>
                                                            </tr>
                                                            <tr>
                                                                <td>--</td>
                                                                <td>--</td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div><!-- /.box-body -->
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="card-primary card-outline">
                                                <div class="card">
                                                    <div class="card-header">
                                                        <h3 class="card-title">Bookmarked Games</h3>
                                                    </div><!-- /.box-header -->
                                                    <div class="box-body no-padding">
                                                        <table class="table table-striped">
                                                            <tbody><tr>
                                                                <th>Game ID</th>
                                                                <th>Game Title</th>
                                                            </tr>
                                                            <tr>
                                                                <td>--</td>
                                                                <td>--</td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div><!-- /.box-body -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="tab-pane fade" id="custom-tabs-three-profile" role="tabpanel" aria-labelledby="custom-tabs-three-profile-tab">
                                    <div class="table-responsive">
                                        <table data-page-length='50' class="table myTbl table-bordered table-striped">
                                            <thead>
                                            <tr>
                                                <th>Date</th>
                                                <th>Transaction ID</th>
                                                <th>IN / OUT</th>
                                                <th>Status</th>
                                                <th>User Name</th>
                                                <th>Amount</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td>--</td>
                                                <td>--</td>
                                                <td>--</td>
                                                <td>--</td>
                                                <td>--</td>
                                                <td>--</td>
                                            </tr>

                                            </tbody>

                                        </table>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <!-- /.card -->
                    </div>
                </div>

            </div>

        </div><!-- /.container-fluid -->
    </section>

    <!-- /.modal -->
@endsection

@section('page_js')

@endsection



