@extends('layouts.admin_layout')


@section('title')
    Admin / {{ $data->name }} / Edit
@endsection

@section('body-title')
    User Edit
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('user.index') }}">User</a></li>
    <li class="breadcrumb-item active">Edit</li>
@endsection

@section('content')
    <section class="content">
        <div class="container-fluid">
            <table style="width:100%; background:#fff; padding:20px; height:50px;">
                <tbody>
                <tr>
                    <td style="text-align:center"><b> {{ $data->username }}</b></td>
                    <td style="text-align:center"><b>Balance : {{ $data->available_balance }} </b></td>
                    <td style="text-align:center"><b>Free Balance : {{ $data->free_pay_balance }} </b></td>
                    <td style="text-align:center"><b>Agent : admin</b></td>
                    <td style="text-align:center"><b>Pending : 00.00</b></td>
                </tr>
                </tbody>
            </table>

            <div class="row mt-5" style="margin-bottom: -16px;">
                <div class="col-12 col-sm-12 col-lg-12">
                    <div class="card card-primary card-outline">
                        <div class="card-header p-0 pt-1 border-bottom-0">
                            @include('admin.users.user_navbar')
                        </div>
                        <!-- /.card -->
                    </div>
                </div>

            </div>

            <div class="tab-content" id="custom-tabs-one-tabContent">
                <div class="tab-pane fade show active" id="custom-tabs-one-home" role="tabpanel" aria-labelledby="custom-tabs-one-home-tab">
                    <div class="card">
                        <div class="card-body">
                            <form class=""  method="post" action="{!! route('user.update', $data->id)!!}">
                                @method('PATCH')
                                @csrf

                                <div class="form-group">
                                    <label for="">User Name</label>
                                    <input type="text" class="form-control" id="username" name="username" value="{{ $data->username }}" placeholder="Enter username">
                                    @if ($errors->has('username'))
                                        <small class="text-danger">{{ $errors->first('username') }}</small>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label for="">User Full Name</label>
                                    <input type="text" class="form-control" id="name" name="name" value="{{ $data->name }}" placeholder="Enter User Full Name">
                                    @if ($errors->has('name'))
                                        <small class="text-danger">{{ $errors->first('name') }}</small>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label for="">User Email</label>
                                    <input type="email" class="form-control" id="email" name="email" value="{{ $data->email }}" placeholder="Enter email">
                                    @if ($errors->has('email'))
                                        <small class="text-danger">{{ $errors->first('email') }}</small>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label for="">User Referred By</label>
                                    <input type="text" class="form-control" id="referred_by" name="referred_by" value="{{ $data->referred_by }}" placeholder="Enter Referred By">
                                    @if ($errors->has('referred_by'))
                                        <small class="text-danger">{{ $errors->first('referred_by') }}</small>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label for="">Account Status</label>
                                        </div>
                                        <div class="col-md-1">
                                            <div class="custom-control custom-radio">
                                                <input class="custom-control-input" type="radio" {{ $data->status == 1 ? 'checked':'' }} value="1" id="open" name="account_status">
                                                <label for="open" class="custom-control-label">Open</label>
                                            </div>
                                        </div>
                                        <div class="col-md-1">
                                            <div class="custom-control custom-radio">
                                                <input class="custom-control-input" type="radio" {{ $data->status == 0 ? 'checked':'' }} value="0" id="close" name="account_status">
                                                <label for="close" class="custom-control-label">Close</label>
                                            </div>
                                        </div>
                                    </div>
                                    @if ($errors->has('account_status'))
                                        <small class="text-danger">{{ $errors->first('account_status') }}</small>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label for="">Casino Status</label>
                                        </div>
                                        <div class="col-md-1">
                                            <div class="custom-control custom-radio">
                                                <input class="custom-control-input" {{ $data->is_casino == 1 ? 'checked':'' }} value="1" type="radio" id="enable" name="casino_status">
                                                <label for="enable" class="custom-control-label">Enable </label>
                                            </div>
                                        </div>
                                        <div class="col-md-1">
                                            <div class="custom-control custom-radio">
                                                <input class="custom-control-input" {{ $data->is_casino == 0 ? 'checked':'' }} value="0" type="radio" id="disable" name="casino_status">
                                                <label for="disable" class="custom-control-label">Disable</label>
                                            </div>
                                        </div>
                                    </div>
                                    @if ($errors->has('casino_status'))
                                        <small class="text-danger">{{ $errors->first('casino_status') }}</small>
                                    @endif
                                </div>

                                <!-- /.card-body -->

                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary">Update</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <div class="card-header">
                                <h3 class="card-title">Reset Password</h3>
                            </div>
                            <!-- /.card-header -->
                            <!-- form start -->
                            <form role="form" action="{{ url('admin/reset-password', $data->id) }}" method="post">
                                @csrf
                                <div class="card-body">
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Enter Password</label>
                                        <input type="password" id="password" name="password" value="{{ old('password') }}" class="form-control" placeholder="Password" required>
                                        @if ($errors->has('password'))
                                            <small class="text-danger">{{ $errors->first('password') }}</small>
                                        @endif
                                    </div>

                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Re-enter Password</label>
                                        <input type="password" id="password_confirmation" value="{{ old('password_confirmation') }}" name="password_confirmation" class="form-control" placeholder="Retype password" required>
                                    </div>

                                </div>
                                <!-- /.card-body -->

                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>


                </div>

                <div class="tab-pane fade" id="custom-tabs-one-transaction" role="tabpanel" aria-labelledby="custom-tabs-one-transaction-tab">
                    <div class="card">
                        <div class="card-body">
                            <form class="" >

                                <div class="form-group">
                                    <label for="">Transaction</label>
                                    <select class="form-control">
                                        <option>Withdrawal</option>
                                        <option>Deposit</option>
                                        <option>Free Pay Deposit</option>
                                        <option>Free Pay Withdrawal</option>
                                    </select>
                                    @if ($errors->has('transaction'))
                                        <small class="text-danger">{{ $errors->first('transaction') }}</small>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label for="">Amount</label>
                                    <input type="text" class="form-control" id="amount" name="amount">
                                    @if ($errors->has('amount'))
                                        <small class="text-danger">{{ $errors->first('amount') }}</small>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label for="">Brief Description</label>
                                    <input type="text" class="form-control" id="brief_description" name="brief_description" >
                                    @if ($errors->has('brief_description'))
                                        <small class="text-danger">{{ $errors->first('brief_description') }}</small>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label for="">Private Note <small class="text-info">(Customer do not see this)</small></label>
                                    <textarea type="text" class="form-control" id="referred_by" name="private_note"></textarea>
                                    @if ($errors->has('private_note'))
                                        <small class="text-danger">{{ $errors->first('private_note') }}</small>
                                    @endif
                                </div>

                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="example1" data-page-length='50' class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>S No.</th>
                                        <th>Date</th>
                                        <th>Amount</th>
                                        <th>Description</th>
                                        <th>Type</th>
                                        <th>Source</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>10/10/2019 21:43:26</td>
                                        <td>50.00</td>
                                        <td>For Betting</td>
                                        <td>Withdrawal</td>
                                        <td>For Betting</td>
                                    </tr>

                                    <tr>
                                        <td>2</td>
                                        <td>10/10/2019 21:43:26</td>
                                        <td>50.00</td>
                                        <td>For Betting</td>
                                        <td>Withdrawal</td>
                                        <td>For Betting</td>
                                    </tr>

                                    </tbody>

                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class=" card tab-pane fade" id="custom-tabs-one-limits" role="tabpanel" aria-labelledby="custom-tabs-one-limits-tab">
                    <form role="form">
                        <div class="row pt-5">
                            <div class="col-md-4">
                                <div class="card-secondary">
                                    <div class="card-header">
                                        <h3 class="card-title">Limit</h3>
                                    </div>
                                    <!-- /.card-header -->
                                    <!-- form start -->

                                    <div class="card-body">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Credit Limit</label>
                                            <input type="number" name="credit_limit" class="form-control" id="credit_limit" placeholder="Credit Limit">
                                        </div>
                                    </div>
                                    <!-- /.card-body -->

                                    <div class="card-footer mt-5">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="card-secondary">
                                    <div class="card-header">
                                        <h3 class="card-title">General Limit</h3>
                                    </div>
                                    <!-- /.card-header -->
                                    <!-- form start -->


                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th  width="20%"></th>
                                            <th  width="40%">Min</th>
                                            <th  width="40%">Max</th>
                                        </tr>
                                        </thead>

                                        <tbody>
                                        <tr>
                                            <td>All</td>
                                            <td><input type="number" name=""></td>
                                            <td><input type="number" name=""></td>
                                        </tr>

                                        <tr>
                                            <td>Straight</td>
                                            <td><input type="number" name=""></td>
                                            <td><input type="number" name=""></td>
                                        </tr>

                                        <tr>
                                            <td>Casino</td>
                                            <td><input type="number" name=""></td>
                                            <td><input type="number" name=""></td>
                                        </tr>
                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        </div>
                    </form>
                </div>

                <div class="tab-pane fade" id="custom-tabs-one-pending" role="tabpanel" aria-labelledby="custom-tabs-one-pending-tab">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table data-page-length='50' class="table myTbl table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Description</th>
                                        <th>Risk/Win</th>
                                        <th>Date</th>
                                        <th>Delete</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td> Miami Florida <b>((-2.0) -110)</b></td>
                                        <td>50.00 / 45.45</td>
                                        <td>10/10/2019 21:43:26 </td>
                                        <td>
                                            <a title="Delete This Bet" href="#" class="btn btn-sm btn-danger"> <i class="fas fa-trash"></i> </a>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td> Miami Florida <b>((-2.0) -110)</b></td>
                                        <td>50.00 / 45.45</td>
                                        <td>10/10/2019 21:43:26 </td>
                                        <td>
                                            <a title="Delete This Bet" href="#" class="btn btn-sm btn-danger"> <i class="fas fa-trash"></i> </a>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td> Miami Florida <b>((-2.0) -110)</b></td>
                                        <td>50.00 / 45.45</td>
                                        <td>10/10/2019 21:43:26 </td>
                                        <td>
                                            <a title="Delete This Bet" href="#" class="btn btn-sm btn-danger"> <i class="fas fa-trash"></i> </a>
                                        </td>
                                    </tr>

                                    </tbody>

                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="tab-pane fade" id="custom-tabs-one-wager" role="tabpanel" aria-labelledby="custom-tabs-one-wager-tab">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table data-page-length='50' class="table myTbl table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Description</th>
                                        <th>Risk/Win</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td> Miami Florida <b>((-2.0) -110)</b></td>
                                        <td>50.00 / 45.45</td>
                                    </tr>

                                    <tr>
                                        <td> Miami Florida <b>((-2.0) -110)</b></td>
                                        <td>50.00 / 45.45</td>
                                    </tr>

                                    <tr>
                                        <td> Miami Florida <b>((-2.0) -110)</b></td>
                                        <td>50.00 / 45.45</td>
                                    </tr>

                                    </tbody>

                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="tab-pane fade" id="custom-tabs-one-notification" role="tabpanel" aria-labelledby="custom-tabs-one-notification-tab">
                    <div class="card">
                        <div class="card-body">
                            <form class="" >
                                <div class="form-group">
                                    <label for="">Enter Your Message</label>
                                    <textarea type="text" class="form-control" id="message" name="message"></textarea>
                                    @if ($errors->has('message'))
                                        <small class="text-danger">{{ $errors->first('message') }}</small>
                                    @endif
                                </div>
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table data-page-length='50' class="table myTbl table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Date</th>
                                        <th>Message</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>10/10/2019 21:43:26</td>
                                        <td>Lorem ipsum dolor sit amet, consectetur</td>
                                    </tr>

                                    <tr>
                                        <td>10/10/2019 21:43:26</td>
                                        <td>Lorem ipsum dolor sit amet, consectetur</td>
                                    </tr>

                                    </tbody>

                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="tab-pane fade" id="custom-tabs-one-internet" role="tabpanel" aria-labelledby="custom-tabs-one-internet-tab">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table data-page-length='50' class="table myTbl table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Date</th>
                                        <th>IP Address</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>10/10/2019 21:43:26</td>
                                        <td>103.60.175.94</td>
                                    </tr>

                                    <tr>
                                        <td>10/10/2019 21:43:26</td>
                                        <td>103.60.175.94</td>
                                    </tr>

                                    </tbody>

                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="tab-pane fade" id="custom-tabs-one-delete" role="tabpanel" aria-labelledby="custom-tabs-one-delete-tab">
                    <div class="card">
                        <div class="card-secondary">
                            <div class="card-header">
                                <h3 class="card-title">Delete Account</h3>
                            </div>

                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Delete Accout</button>
                            </div>
                        </div>
                    </div>

                </div>

            </div>

        </div><!-- /.container-fluid -->
    </section>

    <!-- /.modal -->
@endsection

@section('page_js')
<script>
    // $(function() {
    //
    //     $('a[data-toggle="tab"]').click(function (e) {
    //         e.preventDefault();
    //         $(this).tab('show');
    //     });
    //
    //     $('a[data-toggle="tab"]').on("shown.bs.tab", function (e) {
    //         var id = $(e.target).attr("href");
    //         localStorage.setItem('selectedTab', id)
    //     });
    //
    //     var selectedTab = localStorage.getItem('selectedTab');
    //     if (selectedTab != null) {
    //         $('a[data-toggle="tab"][href="' + selectedTab + '"]').tab('show');
    //
    //     }
    // });
</script>
@endsection



