@extends('layouts.admin_layout')


@section('title')
    Admin / Pending
@endsection

@section('body-title')
    Deleted wagers
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item active">Deleted wagers</li>
@endsection

@section('content')
    <section class="content">
        <div class="container-fluid">

            <div class="row mt-5">
                <div class="col-12 col-sm-12 col-lg-12">
                    <div class="card card-primary card-outline">
                        <div class="card-header p-0 pt-1 border-bottom-0">
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table data-page-length='50' class="table myTbl table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Description</th>
                                        <th>Risk/Win</th>
                                        <th>Date</th>
                                        <th>Delete</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($deleted_lists as $pending)
                                            <tr>
                                                <td> {{ sportsName($pending->sport_name) }}<b>( {{ $pending->betting_condition_original }} )</b></td>
                                                <td> {{ $pending->risk_amount }}  / {{ $pending->betting_amount }} </td>
                                                <td>  {{ $pending->created_at }} </td>
                                                <td>
                                                    <a title="Delete This Bet"  href="#" class="btn btn-sm btn-danger"> restore </a>
                                                </td>
                                            </tr>
                                        @endforeach






                                    </tbody>

                                </table>
                            </div>
                        </div>

                        <!-- /.card -->
                    </div>

                </div>

            </div>

        </div><!-- /.container-fluid -->
    </section>
@endsection
@section('page_js')

@endsection





