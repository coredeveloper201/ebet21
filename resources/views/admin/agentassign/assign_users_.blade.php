@extends('layouts.admin_layout')


@section('title')
    Admin / Assign User
@endsection

@section('body-title')
    Assign User
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item active">Assign User</li>
@endsection

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row admin-agent-lists">
                <div class="col-md-2">
                        <div class="card">
                                <div class="card-header">
                                    <h3 class="card-title">Assignts</h3>
                                </div>
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table data-page-length='50' class="table table-bordered">
                                            <tbody>
                                            @foreach($data as $key => $anotheragent)
                                            <tr>
                                                <td> <a onclick="getUsers({{$anotheragent->id}})">{{ $anotheragent->username }}</a> </td>
                                            </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                </div>
                <div class="col-md-6">
                        <div class="card">
                                <div class="card-header">
                                    <h3 class="card-title"> User Lists</h3>
                                </div>
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table data-page-length='10' class="table myTbl  table-bordered table-striped">
                                            <thead>
                                            <tr>
                                                <th width="15%">S.No</th>
                                                <th>Agent</th>
                                                <th width="10%">Action</th>
                                            </tr>
                                            </thead>
                                            <tbody id="assigned_user_lists">
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                        </div>
                </div>
                <div class="col-md-4">
                    <form action="{{ route('assign-user.store') }}" method="post">
                        @csrf
                        <div class="card">
                                <div class="card-header">
                                    <div class="row">
                                        <div class="col-md-12">
                                                <h1 class="card-title"> Unassigned User Lists</h1>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <select id="my-select" class="form-control" name="agent_id">
                                                    <option value="">Select Agent</option>
                                                        @foreach($data as $key => $agent)
                                                         <option value="{{$agent->id}}">{{ $agent->username }}</option>
                                                        @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <button type="submit" class="btn btn-success">Assign</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table data-page-length='10' class="table myTbl table-bordered table-striped">
                                            <thead>
                                            <tr>
                                                <th width="15%">S.No</th>
                                                <th>Agent</th>
                                                <th width="10%">Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($unAssignedUsers as $key => $user)
                                            <tr>
                                                <td>{{ ++$key }}</td>
                                                <td>{{ $user->username }}</td>
                                                <td> <input type="checkbox"
                                                    name="user[]"
                                                    id="user{{ $user->id }}"
                                                    value="{{ $user->id }}"
                                                    >
                                                </td>
                                            </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </form>
                     </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
@endsection
@section('page_js')
@endsection





