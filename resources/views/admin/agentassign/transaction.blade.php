@extends('layouts.admin_layout')


@section('title')
    Admin / Transaction
@endsection

@section('body-title')
    Transaction
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ url('admin/user', $data->id) }}/edit">User</a></li>
    <li class="breadcrumb-item active">Transaction</li>
@endsection

@section('content')
    <section class="content">
        <div class="container-fluid">
            <table style="width:100%; background:#fff; padding:20px; height:50px;">
                <tbody>
                @if($data->role == 'user')
                    <tr>
                        <td style="text-align:center"><b> {{ $data->username }}</b></td>
                        <td style="text-align:center"><b>Balance : {{ $data->available_balance }} </b></td>
                        <td style="text-align:center"><b>Free Balance : {{ $data->free_pay_balance }} </b></td>
                        <td style="text-align:center"><b>Agent : admin</b></td>
                        <td style="text-align:center"><b>Pending : 00.00</b></td>

                    </tr>
                @else
                    <tr>
                        <td width="50%" style="text-align:center"><b>Agent Name {{ $data->username }}</b></td>
                        <td width="50%" style="text-align:center"><b>Agent Total : {{ $data->agent_total }} </b></td>
                    </tr>
                @endif
                </tbody>
            </table>

            <div class="row mt-5">
                <div class="col-12 col-sm-12 col-lg-12">
                    <div class="card card-primary card-outline">
                        <div class="card-header p-0 pt-1 border-bottom-0">
                            @if($data->role == 'user')
                                @include('admin.users.user_navbar')
                            @else
                                @include('admin.agents.agent_navbar')
                            @endif
                        </div>
                        <div class="card-body">
                            <form class="" method="post" action="{{ route('transaction.store') }}">
                                @csrf
                                <input type="hidden" value="{{ $data->id }}" name="user_id">
                                <div class="form-group">
                                    <label for="">Transaction<span class="text-danger">*</span></label>
                                    <select class="form-control" name="type">
                                        <option value="1">Withdrawal</option>
                                        <option value="2">Deposit</option>
                                        @if($data->role == 'user')
                                        <option value="3">Free Pay Deposit</option>
                                        <option value="4">Free Pay Withdrawal</option>
                                        @endif
                                    </select>
                                    @if ($errors->has('transaction'))
                                        <small class="text-danger">{{ $errors->first('transaction') }}</small>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label for="">Amount<span class="text-danger">*</span></label>
                                    <input type="number" min="0" step="0.01" class="form-control" id="amount" name="amount">
                                    @if ($errors->has('amount'))
                                        <small class="text-danger">{{ $errors->first('amount') }}</small>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label for="">Brief Description</label>
                                    <input type="text" class="form-control" id="brief_description" name="brief_description" >
                                    @if ($errors->has('brief_description'))
                                        <small class="text-danger">{{ $errors->first('brief_description') }}</small>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label for="">Private Note <small class="text-info">(Customer do not see this)</small></label>
                                    <textarea type="text" class="form-control" id="referred_by" name="private_note"></textarea>
                                    @if ($errors->has('private_note'))
                                        <small class="text-danger">{{ $errors->first('private_note') }}</small>
                                    @endif
                                </div>

                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </form>
                        </div>

                        <!-- /.card -->
                    </div>

                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="example1" data-page-length='50' class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>S No.</th>
                                        <th>Date</th>
                                        <th>Amount</th>
                                        <th>Description</th>
                                        <th>Type</th>
                                        <th>Source</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($transactions as $key => $transaction)
                                    <tr>
                                        <td>{{ ++$key }}</td>
                                        <td>{{ _getDate(strtotime($transaction->created_at)).' '. _getTime(strtotime($transaction->created_at)) }}</td>
                                        <td>{{ $transaction->amount }}</td>
                                        <td>{{ $transaction->description }}</td>
                                        <td>
                                            @if($transaction->type == 1)
                                                Withdrawal
                                            @elseif($transaction->type == 2)
                                                Deposit
                                            @elseif($transaction->type == 3)
                                                Free Pay Deposit
                                            @else
                                                Free Pay Withdrawal
                                            @endif
                                        </td>
                                        <td>
                                            @if(getUserById($transaction->user_id)->role == 'user')
                                                @if($transaction->source == 0)
                                                    For Betting
                                                @elseif($transaction->source == 1)
                                                    Done By Agent (<b>{{ getUserById($transaction->assign_id)->username }}</b>)
                                                @elseif($transaction->type == 2)
                                                    Wager Deleted By Agent (<b>{{ getUserById($transaction->assign_id)->username }}</b>)
                                                @endif
                                            @else
                                                @if($transaction->source == 1)
                                                    Done By Admin (<b>{{ getUserById($transaction->assign_id)->username }}</b>)
                                                @else
                                                    Wager Deleted By Admin (<b>{{ getUserById($transaction->assign_id)->username }}</b>)
                                                @endif
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach

                                    </tbody>

                                </table>
                            </div>
                        </div>
                    </div>

                </div>

            </div>

        </div><!-- /.container-fluid -->
    </section>
@endsection
@section('page_js')

@endsection





