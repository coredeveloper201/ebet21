@extends('layouts.admin_layout')


@section('title')
    Admin / Pending
@endsection

@section('body-title')
    Pending
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item active">Pending</li>
@endsection

@section('content')
    <section class="content">
        <div class="container-fluid">
            <table style="width:100%; background:#fff; padding:20px; height:50px;">
                <tbody>
                @if($data->role == 'user')
                    <tr>
                        <td style="text-align:center"><b> {{ $data->username }}</b></td>
                        <td style="text-align:center"><b>Balance : {{ $data->available_balance }} </b></td>
                        <td style="text-align:center"><b>Free Balance : {{ $data->free_pay_balance }} </b></td>
                        <td style="text-align:center"><b>Agent : admin</b></td>
                        <td style="text-align:center"><b>Pending : 00.00</b></td>

                    </tr>
                @else
                    <tr>
                        <td width="50%" style="text-align:center"><b>Agent Name {{ $data->username }}</b></td>
                        <td width="50%" style="text-align:center"><b>Agent Total : {{ $data->agent_total }} </b></td>
                    </tr>
                @endif
                </tbody>
            </table>

            <div class="row mt-5">
                <div class="col-12 col-sm-12 col-lg-12">
                    <div class="card card-primary card-outline">
                        <div class="card-header p-0 pt-1 border-bottom-0">
                            @if($data->role == 'user')
                                @include('admin.users.user_navbar')
                            @else
                                @include('admin.agents.agent_navbar')
                            @endif
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table data-page-length='50' class="table myTbl table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Description</th>
                                        <th>Risk/Win</th>
                                        <th>Date</th>
                                        <th>Delete</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($pendings as $pending)
                                            <tr>
                                                <td> @php ($sportLeague = json_decode($pending->sport_league, true))
                                                        @php ($extraInfo = json_decode($pending->bet_extra_info))
                                                        {{ sportsName($sportLeague[0])."-".$sportLeague[1] }}<br>
                                                        @if(!empty($extraInfo))
                                                            {{ $extraInfo->bet_on_team_name." VS ". $extraInfo->other_team_name }}<br>
                                                            {{ _getDate(strtotime($pending->created_at)) }} ({{ _getTime(strtotime($pending->created_at)) }})
                                                            <span style="color:#e26c32;">
                                                                @if($pending->result==1)
                                                                    Win
                                                                @elseif($pending->result==2)
                                                                    Loss
                                                                @endif
                                                            </span>
                                                            <br>
                                                            <strong style="font-weight: bold;">{{ $extraInfo->betting_wager.' '.$pending->betting_condition_original }}</strong>
                                                            for <strong style="font-weight:bold;"> {{ $extraInfo->bet_on_team_name }} </strong>
                                                        @endif</b>
                                                    </td>
                                                <td> {{ $pending->risk_amount }}  / {{ $pending->betting_win }} </td>
                                                <td>  {{ $pending->created_at }} </td>
                                                <td>
                                                    <a title="Delete This Bet" onclick="return confirm('Are you sure?')" href="{{url('admin/agentassign/pending/delete/'.$pending->id)}}" class="btn btn-sm btn-danger"> <i class="fas fa-trash"></i> </a>
                                                </td>
                                            </tr>
                                        @endforeach






                                    </tbody>

                                </table>
                            </div>
                        </div>

                        <!-- /.card -->
                    </div>

                </div>

            </div>

        </div><!-- /.container-fluid -->
    </section>
@endsection
@section('page_js')

@endsection





