@extends('layouts.admin_layout')


@section('title')
    Admin / Transaction
@endsection

@section('body-title')
    Limits
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ url('admin/user', $data->id) }}/edit">User</a></li>
    <li class="breadcrumb-item active">Limits</li>
@endsection

@section('content')
    <section class="content">
        <div class="container-fluid">
            <table style="width:100%; background:#fff; padding:20px; height:50px;">
                <tbody>
                @if($data->role == 'user')
                    <tr>
                        <td style="text-align:center"><b> {{ $data->username }}</b></td>
                        <td style="text-align:center"><b>Balance : {{ $data->available_balance }} </b></td>
                        <td style="text-align:center"><b>Free Balance : {{ $data->free_pay_balance }} </b></td>
                        <td style="text-align:center"><b>Agent : admin</b></td>
                        <td style="text-align:center"><b>Pending : 00.00</b></td>
                    </tr>
                @else
                    <tr>
                        <td width="50%" style="text-align:center"><b>Agent Name {{ $data->username }}</b></td>
                        <td width="50%" style="text-align:center"><b>Agent Total : {{ $data->agent_total }} </b></td>
                    </tr>
                @endif
                </tbody>
            </table>

            <div class="row mt-5">
                <div class="col-12 col-sm-12 col-lg-12">
                    <div class="card card-primary card-outline">
                        <div class="card-header p-0 pt-1 border-bottom-0">
                            @if($data->role == 'user')
                                @include('admin.users.user_navbar')
                            @else
                                @include('admin.agents.agent_navbar')
                            @endif
                        </div>
                        <form role="form" method="post" action="{{ route('limit.store') }}">
                            @csrf
                            <div class="row pt-3">
                                <div class="col-md-4">
                                    <div class="card-secondary">
                                        <div class="card-header">
                                            <h3 class="card-title">Limit</h3>
                                        </div>
                                        <!-- /.card-header -->
                                        <!-- form start -->

                                        <div class="card-body">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Credit Limit</label>
                                                <input type="hidden" name="user_id" value="{{ old('user_id', !empty($data->id) ? $data->id:'') }}">
                                                <input type="number" name="credit_limit" value="{{ old('credit_limit', !empty($data->credit_limit) ? $data->credit_limit:'') }}" class="form-control" id="credit_limit" placeholder="Credit Limit">
                                            </div>
                                        </div>
                                        <!-- /.card-body -->

                                        <div class="card-footer mt-5">
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="card-secondary">
                                        <div class="card-header">
                                            <h3 class="card-title">General Limit</h3>
                                        </div>
                                        <!-- /.card-header -->
                                        <!-- form start -->


                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <th  width="20%"></th>
                                                <th  width="40%">Min</th>
                                                <th  width="40%">Max</th>
                                            </tr>
                                            </thead>

                                            <tbody>
                                            <tr>
                                                <td>All</td>
                                                <td><input type="number" name="all_min_amount" value="{{ old('all_min_amount', !empty($limit->all_min_amount) ? $limit->all_min_amount:'') }}"></td>
                                                <td><input type="number" name="all_max_amount" value="{{ old('all_max_amount', !empty($limit->all_max_amount) ? $limit->all_max_amount:'') }}"></td>
                                            </tr>

                                            <tr>
                                                <td>Straight</td>
                                                <td><input type="number" name="straight_min_amount" value="{{ old('straight_min_amount', !empty($limit->straight_min_amount) ? $limit->straight_min_amount:'') }}"></td>
                                                <td><input type="number" name="straight_max_amount" value="{{ old('straight_max_amount', !empty($limit->straight_max_amount) ? $limit->straight_max_amount:'') }}"></td>
                                            </tr>

                                            <tr>
                                                <td>Casino</td>
                                                <td><input type="number" name="casino_min_amount" value="{{ old('casino_min_amount', !empty($limit->casino_min_amount) ? $limit->casino_min_amount:'') }}"></td>
                                                <td><input type="number" name="casino_max_amount" value="{{ old('casino_max_amount', !empty($limit->casino_max_amount) ? $limit->casino_max_amount:'') }}"></td>
                                            </tr>
                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                            </div>
                        </form>

                        <!-- /.card -->
                    </div>


                </div>

            </div>

        </div><!-- /.container-fluid -->
    </section>
@endsection
@section('page_js')

@endsection





