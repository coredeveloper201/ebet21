@extends('layouts.admin_layout')


@section('title')
    Admin / User Assignment
@endsection

@section('body-title')
    User Assignment <small style="font-size: 16px!important;">to Agent</small>
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item active"><a href="{{ url('admin/assign-users') }}">Assign User</a></li>
    <li class="breadcrumb-item active">Create</li>
@endsection

@section('content')
    <section class="content">
        <div class="container-fluid">
                <form action="{{ route('assign-user.store') }}" method="post">
                        @csrf
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Assign to <b>{{ $data->username }}</b></h3>
                    <button type="submit" class="btn btn-success float-right">Submit</button>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                            <input name="agent_id" value="{{ $data->id }}" type="hidden">
                            <table class="table myTbl table-bordered">
                                <thead>
                                <tr>
                                    <th width="15%">Select User</th>
                                    <th width="40%">User Name</th>
                                    <th>Assign</th>
                                    <th> Action </th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($users as $user)
                                    <tr>
                                        <td>
                                            <div class="form-group">
                                                <div class="custom-control custom-checkbox">
                                                    <input class="custom-control-input"
                                                           name="user[]"
                                                           {{ !empty($user->agent_username) ? 'checked':'' }}
                                                           type="checkbox" id="user{{ $user->id }}"
                                                           {{ !empty($user->agent_id) && $data->id != $user->agent_id ? 'disabled':'' }}
                                                           value="{{ $user->id }}">
                                                    <label for="user{{ $user->id }}" class="custom-control-label"></label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>{{ $user->username }}</td>
                                        <td><b>Agent Assign : </b>
                                            @if ($user->assign_id != null)
                                            {{$user->agent_username}}
                                            @endif
                                        </td>
                                        <td>
                                            @if ($user->assign_id != null)
                                            <a
                                                class="btn btn-sm btn-danger"
                                                href="{{url('admin/assign-user/delete/'.$user->assign_id)}}"
                                                 onclick="return confirm('Are you sure?')">
                                                 <i class="fa fa-times" aria-hidden="true"></i>
                                                </a>
                                        @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="card-footer">
                            </div>
                    </div>
                </div>
            </div>
        </form>
        </div><!-- /.container-fluid -->
    </section>
@endsection
@section('page_js')

@endsection





