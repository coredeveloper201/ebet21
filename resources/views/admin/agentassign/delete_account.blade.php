@extends('layouts.admin_layout')


@section('title')
    Admin / Transaction
@endsection

@section('body-title')
    Transaction
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item active">Transaction</li>
@endsection

@section('content')
    <section class="content">
        <div class="container-fluid">
            <table style="width:100%; background:#fff; padding:20px; height:50px;">
                <tbody>
                @if($data->role == 'user')
                    <tr>
                        <td style="text-align:center"><b> {{ $data->username }}</b></td>
                        <td style="text-align:center"><b>Balance : {{ $data->available_balance }} </b></td>
                        <td style="text-align:center"><b>Free Balance : {{ $data->free_pay_balance }} </b></td>
                        <td style="text-align:center"><b>Agent : admin</b></td>
                        <td style="text-align:center"><b>Pending : 00.00</b></td>
                    </tr>
                @else
                    <tr>
                        <td width="50%" style="text-align:center"><b>Agent Name {{ $data->username }}</b></td>
                        <td width="50%" style="text-align:center"><b>Agent Total : {{ $data->agent_total }} </b></td>
                    </tr>
                @endif
                </tbody>
            </table>

            <div class="row mt-5">
                <div class="col-12 col-sm-12 col-lg-12">
                    <div class="card card-primary card-outline">
                        <div class="card-header p-0 pt-1 border-bottom-0">
                            @if($data->role == 'user')
                                @include('admin.users.user_navbar')
                            @else
                                @include('admin.agents.agent_navbar')
                            @endif
                        </div>
                        <div class="card-body">
                            <div class="card-secondary">
                                <div class="card-header">
                                    <h3 class="card-title">Delete Account</h3>
                                </div>

                                <div class="card-footer">
                                    <a href="{{ url('admin/delete-account/'. $data->id) }}" onclick="return confirm('Are you sure delete this account...?')" class="btn btn-primary">Delete Account</a>
                                </div>
                            </div>
                        </div>
                        <!-- /.card -->
                    </div>

                </div>

            </div>

        </div><!-- /.container-fluid -->
    </section>
@endsection
@section('page_js')

@endsection





