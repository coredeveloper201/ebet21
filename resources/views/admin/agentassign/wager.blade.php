@extends('layouts.admin_layout')


@section('title')
    Admin / Wager
@endsection

@section('body-title')
    Wager
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item active">Wager</li>
@endsection

@section('content')
    <section class="content">
        <div class="container-fluid">
            <table style="width:100%; background:#fff; padding:20px; height:50px;">
                <tbody>
                @if($data->role == 'user')
                <tr>
                    <td style="text-align:center"><b> {{ $data->username }}</b></td>
                    <td style="text-align:center"><b>Balance : {{ $data->available_balance }} </b></td>
                    <td style="text-align:center"><b>Free Balance : {{ $data->free_pay_balance }} </b></td>
                    <td style="text-align:center"><b>Agent : admin</b></td>
                    <td style="text-align:center"><b>Pending : 00.00</b></td>

                </tr>
                @else
                    <tr>
                        <td width="50%" style="text-align:center"><b>Agent Name {{ $data->username }}</b></td>
                        <td width="50%" style="text-align:center"><b>Agent Total : {{ $data->agent_total }} </b></td>
                    </tr>
                @endif
                </tbody>
            </table>

            <div class="row mt-5">
                <div class="col-12 col-sm-12 col-lg-12">
                    <div class="card card-primary card-outline">
                        <div class="card-header p-0 pt-1 border-bottom-0">
                            @if($data->role == 'user')
                                @include('admin.users.user_navbar')
                            @else
                                @include('admin.agents.agent_navbar')
                            @endif
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table data-page-length='50' class="table myTbl table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Description</th>
                                        <th>Risk/Win</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($wagers as $wager)
                                              <tr>
                                                    <td> @php ($sportLeague = json_decode($wager->sport_league, true))
                                                            @php ($extraInfo = json_decode($wager->bet_extra_info))
                                                            {{ sportsName($sportLeague[0])."-".$sportLeague[1] }}<br>
                                                            @if(!empty($extraInfo))
                                                                {{ $extraInfo->bet_on_team_name." VS ". $extraInfo->other_team_name }}<br>
                                                                {{ _getDate(strtotime($wager->created_at)) }} ({{ _getTime(strtotime($wager->created_at)) }})
                                                                <span style="color:#e26c32;">
                                                                    @if($wager->result==1)
                                                                        Win
                                                                    @elseif($wager->result==2)
                                                                        Loss
                                                                    @endif
                                                                </span>
                                                                <br>
                                                                <strong style="font-weight: bold;">{{ $extraInfo->betting_wager.' '.$wager->betting_condition_original }}</strong>
                                                                for <strong style="font-weight:bold;"> {{ $extraInfo->bet_on_team_name }} </strong>
                                                            @endif</b></td>
                                                    <td> {{ $wager->risk_amount }}  / {{ $wager->betting_win }} </td>
                                              </tr>
                                        @endforeach



                                    </tbody>

                                </table>
                            </div>
                        </div>

                        <!-- /.card -->
                    </div>

                </div>

            </div>

        </div><!-- /.container-fluid -->
    </section>
@endsection
@section('page_js')

@endsection





