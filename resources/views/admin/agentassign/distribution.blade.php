@extends('layouts.admin_layout')


@section('title')
    Admin / Transaction
@endsection

@section('body-title')
    Transaction
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item active">Transaction</li>
@endsection

@section('content')
    <section class="content">
        <div class="container-fluid">
            <table style="width:100%; background:#fff; padding:20px; height:50px;">
                <tbody>
                @if($data->role == 'user')
                    <tr>
                        <td style="text-align:center"><b> {{ $data->username }}</b></td>
                        <td style="text-align:center"><b>Balance : {{ $data->available_balance }} </b></td>
                        <td style="text-align:center"><b>Free Balance : {{ $data->free_pay_balance }} </b></td>
                        <td style="text-align:center"><b>Agent : admin</b></td>
                        <td style="text-align:center"><b>Pending : 00.00</b></td>

                    </tr>
                @else
                    <tr>
                        <td width="50%" style="text-align:center"><b>Agent Name {{ $data->username }}</b></td>
                        <td width="50%" style="text-align:center"><b>Agent Total : {{ $data->agent_total }} </b></td>
                    </tr>
                @endif
                </tbody>
            </table>

            <div class="row mt-5">
                <div class="col-12 col-sm-12 col-lg-12">
                    <div class="card card-primary card-outline">
                        <div class="card-header p-0 pt-1 border-bottom-0">
                            @if($data->role == 'user')
                                @include('admin.users.user_navbar')
                            @else
                                @include('admin.agents.agent_navbar')
                            @endif
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table data-page-length='50' class="table myTbl table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Week</th>
                                        <th>Rate</th>
                                        <th>Week Total</th>
                                        <th>Agent Total</th>
                                        <th>Active Player</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(!empty($dataArray))
                                        @foreach($dataArray as $distribution)
                                        <tr>
                                            <td>{{ _getDate(strtotime($distribution->start_week)) }}</td>
                                            <td>{{ $distribution->rate }}</td>
                                            <td>{{ $distribution->total_amount }}</td>
                                            <td>{{ $distribution->total_amount_new }}</td>
                                            <td>{{ $distribution->total_player }}</td>
                                        </tr>
                                        @endforeach
                                    @endif

                                    </tbody>

                                </table>
                            </div>
                        </div>

                        <!-- /.card -->
                    </div>

                </div>

            </div>

        </div><!-- /.container-fluid -->
    </section>
@endsection
@section('page_js')

@endsection





