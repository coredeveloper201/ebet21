@extends('layouts.admin_layout')

@section('title')
Admin / Users
@endsection

@section('body-title')
Weekly agents lists
@endsection

@section('breadcrumb')
<li class="breadcrumb-item active">Weekly agents lists</li>
@endsection

@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="card">
            <!-- /.card-header -->
            <div class="card-body">
                <div class="table-responsive">
                    <table id="example1" data-page-length='50' class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>S No.</th>
                                <th>Username</th>
                                <th>Name</th>
                                <th>Owes Amount</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(!empty($total_wagers))
                            @foreach($total_wagers as $key => $represnt_list)
                            <tr>
                                <td>{{ ++$key }}</td>
                            <td>{{ $represnt_list->username }}</td>
                            <td>{{ $represnt_list->name }}</td>
                            <td>{{ $represnt_list->total }}</td>
                            </tr>
                            @endforeach
                            @endif

                        </tbody>

                    </table>
                </div>
            </div>
            <!-- /.card-body -->
        </div>
    </div><!-- /.container-fluid -->
</section>
@endsection

@section('page_js')

@endsection
