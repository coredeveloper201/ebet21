@extends('layouts.admin_layout')


@section('title')
    Admin / Assign User
@endsection

@section('body-title')
    Assign User
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item active">Assign User</li>
@endsection

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Assign User</h3>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table data-page-length='50' class="table myTbl table-bordered table-striped">
                            <thead>
                            <tr>
                                <th width="15%">S.No</th>
                                <th>Agent</th>
                                <th width="10%">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($data as $key => $user)
                            <tr>
                                <td>{{ ++$key }}</td>
                                <td>{{ $user->username }}</td>
                                <td><a href="{{ url('admin/assign-user/create/'. $user->id) }}" class="btn btn-sm btn-info"><i class="fas fa-edit"></i></a></td>
                            </tr>
                            @endforeach

                            </tbody>

                        </table>
                    </div>
                </div>

            </div>

        </div><!-- /.container-fluid -->
    </section>
@endsection
@section('page_js')

@endsection





