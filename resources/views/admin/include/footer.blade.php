<footer class="main-footer text-center">
    <strong> &copy; </strong>
    {{ date('Y') }} All rights reserved.
</footer>
