<aside class="main-sidebar sidebar-dark-primary elevation-4">

    <a href="{{ url('admin/dashboard') }}" class="brand-link">
        <img src="{{ asset('admin/dist') }}/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
        @if(Auth::user()->role == 'admin')
            <span class="brand-text font-weight-light">Admin </span><small style="font-size: 15px">Control Panel</small>
        @elseif(Auth::user()->role == 'agent')
            <span class="brand-text font-weight-light">Agent </span><small style="font-size: 15px">Control Panel</small>
        @elseif(Auth::user()->role == 'subagent')
            <span class="brand-text font-weight-light">Sub Agent </span><small style="font-size: 15px">Control Panel</small>
        @endif
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="{{ asset('admin/dist') }}/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
                <a href="#" class="d-block">{{ Auth::user()->name }}</a>
            </div>

        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">

                <li class="nav-item">
                    <a href="{{ url('admin/dashboard') }}" class="nav-link {{ Request::is('admin/dashboard') ? 'active':'' }}">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>Dashboard</p>
                    </a>
                </li>


                @if(Auth::user()->role == 'admin')
                <li class="nav-item">
                    <a href="{{ url('admin/straight-league-approval') }}" class="nav-link {{ Request::is('admin/straight-league-approval') ? 'active':'' }}">
                        <i class="nav-icon fas fa-list"></i>
                        <p>Straight League Approval</p>
                    </a>
                </li>
                @endif

                <li class="nav-item has-treeview {{ Request::is('admin/admin-manage*') || Request::is('admin/assign-user*') || Request::is('admin/user*') || Request::is('admin/agent*') || Request::is('admin/active-user') ? 'menu-open':'' }}">
                    <a href="#" class="nav-link {{ Request::is('admin/admin-manage*') || Request::is('admin/assign-user*') || Request::is('admin/user*') || Request::is('admin/agent*') || Request::is('admin/active-user') ? 'active':'' }}">
                        <i class="nav-icon fas fa-users"></i>
                        <p>
                            User Management
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('user.index') }}" class="nav-link {{ Request::is('admin/user') || Request::is('admin/assign-user*') || Request::is('admin/user/*/edit') ? 'active':'' }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Users</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('user.active-user') }}" class="nav-link {{ Request::is('admin/active-user')? 'active':'' }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Active Users</p>
                            </a>
                        </li>

                        @if(Auth::user()->role == 'admin')
                        <li class="nav-item">
                            <a href="{{ route('agent.index') }}" class="nav-link {{ Request::is('admin/agent') || Request::is('admin/agent/*/edit') ? 'active':'' }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Agents</p>
                            </a>
                        </li>

                       {{--  <li class="nav-item">
                            <a href="{{ url('admin/agents-users/lists') }}" class="nav-link {{ Request::is('admin/agents-users/lists') ? 'active':'' }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Assign user</p>
                            </a>
                        </li> --}}

                        <li class="nav-item">
                            <a href="{{ route('admin-manage.index') }}" class="nav-link {{ Request::is('admin/admin-manage*') ? 'active':'' }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Administrator</p>
                            </a>
                        </li>
                        @endif
                    </ul>
                </li>


                @if(Auth::user()->role != 'subagent')
                    <li class="nav-item has-treeview {{ Request::is('admin/record*') ? 'menu-open':'' }}">
                        <a href="#" class="nav-link {{ Request::is('admin/record*') ? 'active':'' }}">
                            <i class="nav-icon fas fa-list-alt"></i>
                            <p>
                                User Record
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">

                            @if(Auth::user()->role == 'agent')
                            <li class="nav-item">
                                <a href="{{ url('admin/record/weekly-figures') }}" class="nav-link {{ Request::is('admin/record/weekly-figures') ? 'active':'' }}">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Weekly Figures</p>
                                </a>
                            </li>
                            @endif

                            <li class="nav-item">
                                <a href="{{ url('admin/record/wagers') }}" class="nav-link {{ Request::is('admin/record/wagers') ? 'active':'' }}">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>All Wagers</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ url('admin/record/pending-wagers') }}" class="nav-link {{ Request::is('admin/record/pending-wagers') ? 'active':'' }}">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Pending Wagers</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ url('admin/record/delete-wagers') }}" class="nav-link {{ Request::is('admin/record/delete-wagers') ? 'active':'' }}">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Deleted Wagers</p>
                                </a>
                            </li>

                        </ul>
                    </li>
                @endif


                @if(Auth::user()->role == 'agent')
                <li class="nav-item has-treeview {{ Request::is('admin/distribution*') ? 'menu-open':'' }}">
                    <a href="#" class="nav-link {{ Request::is('admin/distribution*') ? 'active':'' }}">
                        <i class="nav-icon fas fa-chart-bar"></i>
                        <p>
                            Distribution
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ url('admin/distribution/summary') }}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Agent Summary</p>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="{{ url('admin/distribution/log') }}" class="nav-link {{ Request::is('admin/distribution/log') ? 'active':'' }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Total Weekly Distribution</p>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-item has-treeview {{ Request::is('admin/subagent*') ? 'menu-open':'' }}">
                    <a href="#" class="nav-link {{ Request::is('admin/subagent*') ? 'active':'' }}">
                        <i class="nav-icon fas fa-chart-area"></i>
                        <p>
                            Manage Sub-Agent
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('subagent.index') }}" class="nav-link {{ Request::is('admin/subagent') || Request::is('admin/subagent/*/edit') ? 'active':'' }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Manage Sub-Agent</p>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="{{ url('admin/subagent/assign-users') }}" class="nav-link {{ Request::is('admin/subagent/assign-users') ? 'active':'' }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Assign User To Sub-Agent</p>
                            </a>
                        </li>
                    </ul>
                </li>
                @endif

                @if(Auth::user()->role == 'admin')
                <li class="nav-item">
                    <a href="{{ url('admin/invoice') }}" class="nav-link {{ Request::is('admin/invoice') ? 'active':'' }}">
                        <i class="nav-icon fas fa-shopping-cart"></i>
                        <p>Invoices</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ url('admin/site-maintenance') }}" class="nav-link {{ Request::is('admin/site-maintenance') ? 'active':'' }}">
                        <i class="nav-icon fas fa-cog"></i>
                        <p>Site Maintenance</p>
                    </a>
                </li>
                @endif


            </ul>
        </nav>
    </div>
</aside>
