@extends('layouts.admin_layout')


@section('title')
    Admin / Site maintenance
@endsection

@section('body-title')
    Site maintenance
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item active">Site maintenance</li>
@endsection

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="card card-teal">
                <div class="card-header">
                    <h3 class="card-title">Maintenance Status</h3>
                </div>
                <form role="form" action="{{ route('maintenance.store') }}" method="post">
                    @csrf
                    <div class="card-body">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-2">
                                    <label for="">Maintenance Status</label>
                                </div>
                                <div class="col-md-1">
                                    <div class="custom-control custom-radio">
                                        <input class="custom-control-input" {{ isset($data->status) && $data->status == 1 ? 'checked':'' }} type="radio" value="1" id="open" name="maintenance_status">
                                        <label for="open" class="custom-control-label">Enable</label>
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <div class="custom-control custom-radio">
                                        <input class="custom-control-input" {{ isset($data->status) && $data->status == 0 ? 'checked':'' }} type="radio" value="0" id="close" name="maintenance_status">
                                        <label for="close" class="custom-control-label">Disable</label>
                                    </div>
                                </div>
                            </div>
                            @if ($errors->has('maintenance_status'))
                                <small class="text-danger">{{ $errors->first('maintenance_status') }}</small>
                            @endif
                        </div>

                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>

            </div>

        </div>
    </section>
@endsection
@section('page_js')

@endsection





