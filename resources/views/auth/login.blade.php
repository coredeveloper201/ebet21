<!DOCTYPE HTML>
<html lang="en-US">

<head>
    <meta charset="UTF-8">
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Font name -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,700,900&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Rubik&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('front_end') }}/css/main.css" />
</head>

<body class="wrapperbody login-wrapper">

    <section id="main_body_area">

        <div class="container">
            <div class="row">
                <div class="fullscreen">
                    <div class="cetner_midel">
                        <div class="form-box-size login-form-1 box-shadow">
                            <div class="login-form-box-1">
                                <div class="form-wrapper">
                                    <div class="logo_here">
                                        <img src="{{ asset('assets/img/elogo.png') }}" alt="">
                                    </div>
                                    @if(session('error'))
                                    <p class="text-danger"><b> {{ session('error') }}</b></p>
                                    @endif

                                    <form class="form" method="POST" action="{{ url('login') }}">
                                        @csrf
                                        <div class="form-group">
                                            <label class="form-label" for="first">Username</label>
                                            <input id="first" name="email" value="{{ old('email') }}"
                                                class="form-input {{ $errors->has('email') ? ' is-invalid' : '' }}"
                                                autocomplete="off" type="text" required />
                                            <i class="material-icons"> person </i>

                                            @if ($errors->has('email'))
                                            <small class="text-danger">{{ $errors->first('email') }}</small>
                                            @endif
                                        </div>

                                        <div class="form-group">
                                            <label class="form-label" for="last">Password</label>
                                            <input id="last"
                                                class="form-input {{ $errors->has('password') ? ' is-invalid' : '' }}"
                                                name="password" value="{{ old('password') }}" autocomplete="off"
                                                type="password" required />
                                            <i class="material-icons"> keyboard_hide </i>
                                            @if ($errors->has('password'))
                                            <small style="padding-top: 5px; display: inline-block;" class="text-danger">{{ $errors->first('password') }}</small>
                                            @endif
                                        </div>

                                       {{--  <div class="checkbox">
                                            <label>
                                                <input type="checkbox" checked><span class="checkbox-material"><span
                                                        class="check"></span></span> Remember Me
                                            </label>
                                        </div> --}}

                                        <div class="button-style">
                                            <button type="submit" class="button login">
                                                Secure Login <i style="padding-bottom: 8px" class="material-icons"> lock </i>
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script src="{{ asset('assets') }}/js/handlebars.js"></script>
       <!-- main js file -->

    <script type="text/javascript" src="{{ asset('assets/js/jquery-3.3.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets') }}/js/idle-timer.js"></script>
    <script type="text/javascript" src="{{ asset('front_end') }}/js/main.js"></script>


    @if(Auth::check())
    @if(Auth::user()->role == 'admin' || Auth::user()->role == 'agent')
    <script>
        window.location.href = "{{ url('admin/dashboard') }}";
    </script>
    @else
    <script>
        window.location.href = "{{ url('straight-bet') }}";
    </script>
    @endif
    @endif

</body>

</html>
