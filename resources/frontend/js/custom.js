const Betslip = (function (hbObj, $) {
    "use strict"
    let cart_array = [];
    let parlay_arrays=[];
    let teaser_arrays=[];
    let parlaynumber=0;
    let num=0;
    let teaser_num=0;
    let togglecount=0;
    let Roundtogglecount=0;
    let Teasertogglecount=0;
    let IfBettogglecount=0;
    let ReverseBettogglecount=0;
    let NFL=[
        ['-120', '-130','-140'],
        ['+150', '+135','+120'],
        ['+260', '+225','+200'],
        ['+400', '+350','+325'],
        ['+600', '+500','+450']
    ];
    let CFL=[
        ['-110', '-120','-130'],
        ['+160', '+140','+120'],
        ['+260', '+225','+200'],
        ['+400', '+375','+325'],
        ['+600', '+500','+450']
    ];
    let College=[
        ['-105', '-115','-125'],
        ['+170', '+150','+130'],
        ['+275', '+240','+220'],
        ['+425', '+375','+350'],
        ['+650', '+525','+475'],
    ];
    let Basketball=[
        ['+100', '-110','-120','-140'],
        ['+180', '+160','+150','+125'],
        ['+300', '+260','+225','+190'],
        ['+500', '+400','+350','+300'],
        ['+700', '+600','+500','+425']
    ];

    /* render slip item when page is refreshed  */
    let _printSlipItem = function () {
        let parlay_array;
        let betss = $('.bet_slip_add').length;
        if (betss === 0) {
            cart_array = _getStoredItems();
            if (cart_array.length !== 0)
            {
                cart_array.forEach((item) => {
                    parlay_array=_getTarget_String(item);
                    parlay_arrays.push(parlay_array);
                    _createHtml(item);
                    $("#" + item.slip_item).addClass('odd_active');
                });
                var parlaynumber=_confirmDuplicated(cart_array.length,parlay_arrays);
                _displayParlayHtml(cart_array.length,parlaynumber);
                for(var i=0;i<cart_array.length;i++)
                {
                    teaser_arrays=_getTeaser_Array(cart_array[i]);
                }
                let  teaserbettype=_confirmTeaserDuplicated(cart_array.length,teaser_arrays);
                _displayTeaserHtml(cart_array.length,teaserbettype);
            }
        }
    };
    let _createHtml = function (obj) {
        if (document.getElementById('bet-card-slip') !== null) {
            Handlebars.registerHelper('ifCond', function (v1, operator, v2, options) {
                switch (operator) {
                    case '==':
                        return (v1 == v2) ? options.fn(this) : options.inverse(this);
                    case '===':
                        return (v1 === v2) ? options.fn(this) : options.inverse(this);
                    case '!=':
                        return (v1 != v2) ? options.fn(this) : options.inverse(this);
                    case '!==':
                        return (v1 !== v2) ? options.fn(this) : options.inverse(this);
                    case '<':
                        return (v1 < v2) ? options.fn(this) : options.inverse(this);
                    case '<=':
                        return (v1 <= v2) ? options.fn(this) : options.inverse(this);
                    case '>':
                        return (v1 > v2) ? options.fn(this) : options.inverse(this);
                    case '>=':
                        return (v1 >= v2) ? options.fn(this) : options.inverse(this);
                    case '&&':
                        return (v1 && v2) ? options.fn(this) : options.inverse(this);
                    case '||':
                        return (v1 || v2) ? options.fn(this) : options.inverse(this);
                    default:
                        return options.inverse(this);
                }
            });
            if((obj.handicap==="-3.0")||(obj.handicap==="-3.5")
                ||(obj.handicap==="+2.5")||(obj.handicap==="+3.0")
                ||(obj.handicap==="-7.0")||(obj.handicap==="-7.5")
                ||(obj.handicap==="+6.5")||(obj.handicap==="+7.0"))
            {
                var first_changed_odd=-135;
                var second_changed_odd=-145;
                var third_changed_odd=-155;
            }
            else
            {
                var  first_changed_odd=parseInt(obj.odd)-10;
                var second_changed_odd=parseInt(obj.odd)-20;
                var third_changed_odd=parseInt(obj.odd)-30;
            }
            let slip_obj = document.getElementById('bet-card-slip').innerHTML;
            let templete = hbObj.compile(slip_obj);
            const contex = {
                first_changed_odd:  first_changed_odd,
                second_changed_odd: second_changed_odd,
                third_changed_odd:  third_changed_odd,
                slip_item: obj
            };
            //console.log('html create',parseFloat(obj.handicap));
            let generatedHtml = templete(contex);
            $("#dev-straightbet-slip-container").append(generatedHtml);
        }
    };
    /*When the page is refreshed create parlay_betslip*/
    let _displayParlayHtml=function(cart_array_number,parlaynumber) {
        if(cart_array_number>=2)
        {
            if(parlaynumber===2)
            {
                _createReverseBetHtml(parlaynumber);
            }
            else
            {
                $("#dev-reversebet-slip-container").empty();
                _clearReverseBet_Value();
            }
            if(parlaynumber>=2)
            {
                if(parlaynumber<8)
                {
                    _createParlayHtml(cart_array.length);
                    _createIfBetHtml(cart_array.length);
                }
                else
                {
                    $("#dev-parlaybet-slip-container").empty();
                    $("#dev-roundrobinbet-slip-container").empty();
                    $("#dev-ifbet-slip-container").empty();
                    _clearParlay_Value();
                    _clearIfBet_Value();
                }
            }
            if(parlaynumber>=3)
            {
                _createRoundRobinHtml(cart_array.length);
            }
            if(parlaynumber<4)
            {
                $("#threeteamparlay-content").empty();
                _clearThreeRoundRobin_Value();
            }
            if(parlaynumber<5)
            {
                $("#fourteamparlay-content").empty();
                _clearFourRoundRobin_Value();
            }
            if(parlaynumber<3)
            {
                $("#dev-roundrobinbet-slip-container").empty();
                _clearTwoRoundRobin_Value();
            }
            if(parlaynumber==0)
            {
                $("#dev-parlaybet-slip-container").empty();
                $("#dev-roundrobinbet-slip-container").empty();
                $("#dev-ifbet-slip-container").empty();
                _clearTwoRoundRobin_Value();
                _clearThreeRoundRobin_Value();
                _clearParlay_Value();
                _clearIfBet_Value();
            }
        }
        if(cart_array_number<2)
        {
            $("#dev-parlaybet-slip-container").empty();
            $("#dev-ifbet-slip-container").empty();
            $("#dev-reversebet-slip-container").empty();
            _clearReverseBet_Value();
            _clearParlay_Value();
            _clearIfBet_Value();
        }
    };
    let _displayTeaserHtml=function(teasernumber,teaserbettype) {
        if((teasernumber>=2)&&(teasernumber<7)&&(teaserbettype!==""))
        {
            if(teaser_num===0)
            {
                $("#dev-teaserbet-slip-container").empty();
                _createTeaserHtml(teasernumber,teaserbettype);
                teaser_num++;
            }
            else
            {
                _clearTeaser_Value();
                $("#dev-teaserbet-slip-container").empty();
                _createTeaserHtml(teasernumber,teaserbettype);
                teaser_num++;
            }
        }
        else
        {
            $("#dev-teaserbet-slip-container").empty();
            _clearTeaser_Value();
        }
    };
    /*----------------------------------- parlay slip item------------------------------------------- */
    let _clearParlay_Value=function(){
        let storageItem = _getStoredItems();
        if (storageItem.length !== 0) {
            storageItem.forEach((item) => {
                {
                    item.parlaywin_amount =null;
                    item.parlayrisk_amount = null;
                }
            });
        }
        localStorage.setItem('slip_items', JSON.stringify(storageItem));
    };
    let _clearIfBet_Value=function(){
        let storageItem = _getStoredItems();
        if (storageItem.length !== 0) {
            storageItem.forEach((item) => {
                {
                    item.ifbetrisk_amount =null;
                    item.ifbetwin_amount = null;
                }
            });
        }
        localStorage.setItem('slip_items', JSON.stringify(storageItem));
    };
    let _clearReverseBet_Value=function(){
        let storageItem = _getStoredItems();
        if (storageItem.length !== 0) {
            storageItem.forEach((item) => {
                {
                    item.reversebetwin_amount =null;
                    item.reversebetrisk_amount= null;
                }
            });
        }
        localStorage.setItem('slip_items', JSON.stringify(storageItem));
    };
    let _clearTeaser_Value=function(){
        let storageItem = _getStoredItems();
        if (storageItem.length !== 0) {
            storageItem.forEach((item) => {
                {
                    item.teaserwin_amount =null;
                    item.teaserrisk_amount = null;
                }
            });
        }
        localStorage.setItem('slip_items', JSON.stringify(storageItem));
    };
    let _clearTwoRoundRobin_Value=function() {
        let storageItem = _getStoredItems();
        if (storageItem.length !== 0) {
            storageItem.forEach((item) => {
                {
                    item.tworoundrobinrisk_amount =null;
                    item.tworoundrobinwin_amount = null;
                }
            });
        }
        localStorage.setItem('slip_items', JSON.stringify(storageItem));
    };
    let _clearThreeRoundRobin_Value=function(){
        let storageItem=_getStoredItems();
        if (storageItem.length !== 0) {
            storageItem.forEach((item) => {
                {
                    item.threeroundrobinrisk_amount =null;
                    item.threeroundrobinwin_amount = null;
                }
            });
        }
        localStorage.setItem('slip_items', JSON.stringify(storageItem));
    };
    let _clearFourRoundRobin_Value=function()
    {
        let storageItem=_getStoredItems();
        if (storageItem.length !== 0) {
            storageItem.forEach((item) => {
                {
                    item.fourroundrobinrisk_amount =null;
                    item.fourroundrobinwin_amount = null;
                }
            });
        }
        localStorage.setItem('slip_items', JSON.stringify(storageItem));
    };
    let _createParlayHtml=function(number){
        $("#dev-parlaybet-slip-container").empty();
        if (document.getElementById('parlaybet-card-slip') !== null) {
            let array=_getStoredItems();
            let slip_obj = document.getElementById('parlaybet-card-slip').innerHTML;
            let templete = hbObj.compile(slip_obj);
            const contex = {
                team_count: number,
                parlayrisk:array[0].parlayrisk_amount,
                parlaywin:array[0].parlaywin_amount,
            };
            let generatedHtml = templete(contex);
            $("#dev-parlaybet-slip-container").append(generatedHtml);
            /* let slip_items=$(".bet_slip_add");
            $(".__slip_count").text(slip_items.length);
            $("#total_bets").text(slip_items.length); */
        }
    };
    let _createIfBetHtml=function(number){
        $("#dev-ifbet-slip-container").empty();
        if (document.getElementById('ifbet-card-slip') !== null) {
            let array=_getStoredItems();
            let slip_obj = document.getElementById('ifbet-card-slip').innerHTML;
            let templete = hbObj.compile(slip_obj);
            const contex = {
                team_count: number,
                ifbetrisk:array[0].ifbetrisk_amount,
                ifbetwin:array[0].ifbetwin_amount,
            };
            let generatedHtml = templete(contex);
            $("#dev-ifbet-slip-container").append(generatedHtml);
           /*  let slip_items=$(".bet_slip_add");
            $(".__slip_count").text(slip_items.length);
            $("#total_bets").text(slip_items.length); */
        }
    };
    let _createReverseBetHtml=function(number){
        $("#dev-reversebet-slip-container").empty();
        if (document.getElementById('reversebet-card-slip') !== null) {
            let array=_getStoredItems();
            let slip_obj = document.getElementById('reversebet-card-slip').innerHTML;
            let templete = hbObj.compile(slip_obj);
            const contex = {
                team_count: number,
                reversebetrisk:array[0].reversebetrisk_amount,
                reversebetwin:array[0].reversebetwin_amount,
            };
            let generatedHtml = templete(contex);
            $("#dev-reversebet-slip-container").append(generatedHtml);
            /* let slip_items=$(".bet_slip_add");
            $(".__slip_count").text(slip_items.length);
            $("#total_bets").text(slip_items.length); */
        }
    };
    let _createRoundRobinHtml=function(number){
        if(number>=3)
        {
            $("#dev-roundrobinbet-slip-container").empty();
            if(document.getElementById('RoundRobinbet-card-slip')!==null){
                let array=_getStoredItems();
                let slip_obj=document.getElementById('RoundRobinbet-card-slip').innerHTML;
                let template=hbObj.compile(slip_obj);
                let team_count=number*(number-1)/2;
                let risk=null;
                let win=null;
                if(array[0].tworoundrobinrisk_amount==null)
                {
                    risk=null;
                    win=null;
                }
                else
                {
                    risk=array[0].tworoundrobinrisk_amount/team_count;
                    win=array[0].tworoundrobinwin_amount;
                }
                const contex={
                    team_count:team_count,
                    tworoundrobinrisk:risk,
                    tworoundrobinwin:win,
                };
                let generatedHtml=template(contex);
                $("#dev-roundrobinbet-slip-container").append(generatedHtml);
               /*  let slip_items=$(".bet_slip_add");
                $(".__slip_count").text(slip_items.length);
                $("#total_bets").text(slip_items.length); */
            }
        }
        if(number>=4)
        {
            $("#threeteamparlay-content").empty();
            if(document.getElementById('ThreeRoundRobinbet-card-slip')!==null){
                let array=_getStoredItems();
                let slip_obj=document.getElementById('ThreeRoundRobinbet-card-slip').innerHTML;
                let team_count=number*(number-1)*(number-2)/6;
                let template=hbObj.compile(slip_obj);
                let risk=null;
                let win=null;
                if(array[0].threeroundrobinrisk_amount==null)
                {
                    risk=null;
                    win=null;
                }
                else{
                    risk=array[0].threeroundrobinrisk_amount/team_count;
                    win=array[0].threeroundrobinwin_amount;
                }
                const contex={
                    team_count:team_count,
                    threeroundrobinrisk:risk,
                    threeroundrobinwin:win,
                };
                let generatedHtml=template(contex);
                $("#threeteamparlay-content").append(generatedHtml);
               /*  let slip_items=$(".bet_slip_add");
                $(".__slip_count").text(slip_items.length);
                $("#total_bets").text(slip_items.length); */
            }
        }
        if(number>=5)
        {
            $("#fourteamparlay-content").empty();
            if(document.getElementById('FourRoundRobinbet-card-slip')!==null){
                let array=_getStoredItems();
                let slip_obj=document.getElementById('FourRoundRobinbet-card-slip').innerHTML;
                let team_count=number*(number-1)*(number-2)*(number-3)*(number-4)/24;
                let template=hbObj.compile(slip_obj);
                let risk=null;
                let win=null;
                if(array[0].fourroundrobinrisk_amount==null)
                {
                    risk=null;
                    win=null;
                }
                else
                {
                    risk=array[0].fourroundrobinrisk_amount/team_count;
                    win=array[0].fourroundrobinwin_amount;
                }
                const contex={
                    team_count:team_count,
                    Fourroundrobinrisk:risk,
                    Fourroundrobinwin:win,
                };
                let generatedHtml=template(contex);
                $("#fourteamparlay-content").append(generatedHtml);
                /*  let slip_items=$(".bet_slip_add");
                 $(".__slip_count").text(slip_items.length);
                 $("#total_bets").text(slip_items.length); */
            }
        }
    };
    let _createTeaserHtml=function(teasernumber,teaserbettype) {
        let teaserpoints;
        if(teaserbettype==="CFL")
        {
            teaserpoints=CFL[teasernumber-2];
        }
        else if((teaserbettype==="NFL")||(teaserbettype==="Cross League NFL"))
        {
            teaserpoints=NFL[teasernumber-2];
        }
        else if(teaserbettype==="College") {
            teaserpoints=College[teasernumber-2];
        }
        else if(teaserbettype==="Basketball") {
            teaserpoints=Basketball[teasernumber-2];
        }
        $("#dev-teaserbet-slip-container").empty();
        if(document.getElementById('teaserbet-card-slip')!==null) {
            let slip_obj=document.getElementById('teaserbet-card-slip').innerHTML;
            let teaserbettypes=[];
            let array=_getStoredItems();
            let template=hbObj.compile(slip_obj);
            let teaser_infoarray=_teaserinfo();
            if(teaserbettype==="Basketball")
            {
                teaserbettypes=["Basketball 4 points "+teaserpoints[0],"Basketball 4.5 points "+teaserpoints[1],"Basketball 5 points "+teaserpoints[2],"Basketball 6 points "+teaserpoints[3]];
            }
            else
            {
                let teaserbettype1=teaserbettype+"Football 6 points "+teaserpoints[0];
                let teaserbettype2=teaserbettype+"Football 6.5 points "+teaserpoints[1];
                let teaserbettype3=teaserbettype+"Football 7 points "+teaserpoints[2];
                teaserbettypes.push(teaserbettype1);
                teaserbettypes.push(teaserbettype2);
                teaserbettypes.push(teaserbettype3);
            }
            const contex=
                {
                    team_count:teasernumber,
                    bettype:teaserbettypes,
                    obj:teaser_infoarray,
                    teaserrisk:array[0].teaserrisk_amount,
                    teaserwin:array[0].teaserwin_amount,
                };
            let generateHtml=template(contex);
            $("#dev-teaserbet-slip-container").append(generateHtml);
            _teaserinfo_display(teaser_infoarray);
           /*  let slip_items=$(".bet_slip_add");
            $(".__slip_count").text(slip_items.length);
            $("#total_bets").text(slip_items.length); */
        }
    };
    let _teaserinfo=function() {
        let teaser_info;
        let teaser_infoarray=[];
        let obj=_getStoredItems();

        for(var i=0;i<obj.length;i++)
        {
            if((obj[i].row_line==="under")||(obj[i].row_line==="over"))
            {
                if(((obj[i].teamName).includes("Under")===true)||((obj[i].teamName).includes("Over")===true))
                {
                    teaser_info=obj[i].teamName;
                }
                else
                {
                    if(obj[i].row_line==="under")
                    {
                        teaser_info="Under "+obj[i].teamName;
                    }
                    else
                    {
                        teaser_info="Over "+obj[i].teamName;
                    }
                }
            }
            else
            {

                teaser_info=obj[i].bet_extra_info.bet_on_team_name+" "+obj[i].handicap;
            }
            teaser_infoarray.push(teaser_info);
        }
        return teaser_infoarray;
    };
    let _teaserinfo_display=function(teaser_infoarray){
        /*-------------------------teaser_infodisplay--------------------------*/
        let teaser_point;
        let teaser_teamname;
        let bonus_point;
        var selected_value=$("#points option:selected").text();
        bonus_point=parseFloat(selected_value.split(" ")[selected_value.split(" ").length-3]);
        let odd=(selected_value.split(" ")[selected_value.split(" ").length-1]);
        $("#odd p").empty();
        $("#odd p").text(odd);
        $(".Teaserbet_info").empty();
        for (var i=0;i<teaser_infoarray.length;i++)
        {
            teaser_point=teaser_infoarray[i].split(" ")[teaser_infoarray[i].split(" ").length-1];
            teaser_teamname=teaser_infoarray[i].split(" ")[teaser_infoarray[i].split(" ").length-2];
            if(teaser_teamname==="Over")
            {
                teaser_point=parseFloat(teaser_point)-bonus_point;
            }
            else if(teaser_teamname==="Under")
            {
                teaser_point=parseFloat(teaser_point)+bonus_point;
            }
            else
            {
                teaser_point=parseFloat(parseFloat(teaser_point)+bonus_point).toFixed(1);
                if(teaser_point>0)
                {
                    teaser_point="+"+teaser_point.toString();
                }
            }
            $(".Teaserbet_info").append("<div class='row' style='margin:5px'> " +
                "<div class='col-sm-8' style='padding:0'>" +
                "<span class='teaser_info' style='margin-bottom:5px'>"+teaser_infoarray[i]+"</span></div> " +
                "<div class='col-sm-4' style='text-align:right'><span class='points'>"+teaser_point+"</span>" +
                "</div></div>")
        }
        /*-----------------------teaser_infodisplay--------------------*/
    };
    let _updateteaserriskCal=function(risk){
        let win=0;
        let odd;
        let score=parseInt($("#odd").text());
        if(score>0)
        {
            odd=(score*1+100)/100;
        }
        else
        {
            odd=Math.abs((-100/score*1).toFixed(2))+1
        }
        win=Math.abs(((odd-1)*risk).toFixed(2));
        $('#teaser_win').val(win);
        _updateTeaserWinRiskValue(win,parseFloat(risk));
        _renderBetSLIP();
    };
    let _confirmDuplicated=function(length,parlay_arrays){
        for (var i=0;i<length;i++)
        {
            for(var j=i;j<length;j++)
            {
                if(i != j &&parlay_arrays[i]==parlay_arrays[j])
                {
                    var Is_dup=1;
                    break;
                }
            }
        }
        if(Is_dup!=1)
        {
            if(num!=0)
            {
                _clearParlay_Value();
                _clearIfBet_Value();
                _clearTwoRoundRobin_Value();
                _clearThreeRoundRobin_Value();
                _renderBetSLIP();
            }
            parlaynumber=length;
            num++;
            Is_dup=0;
        }
        else
        {
            parlaynumber=0;
            Is_dup=0;
        }
        return parlaynumber;
    };
    let _confirmTeaserDuplicated=function(length,teaser_arrays) {

        let Is_dup=0;
        let state="";
        for (var i=0;i<length;i++)
        {
            for(var j=i;j<length;j++)
            {
                if(i != j &&parlay_arrays[i]==parlay_arrays[j])
                {
                    Is_dup=1;
                    break;
                }
            }
        }
        if(Is_dup!=1)
        {

            for(var i=0;i<length;i++)
            {
                if (teaser_arrays[i] === "money")
                {
                    Is_dup=1;
                    break;
                }
            }

            if(Is_dup!=1)
            {
                for (var i=0;i<length;i++)
                {
                    if((teaser_arrays[i]==="12Cfl")||(teaser_arrays[i]==="12Nfl")||(teaser_arrays[i]==="12College")||(teaser_arrays[i]==="18Basketball"))
                    {
                        Is_dup=0;
                    }
                    else
                    {
                        Is_dup=1;
                        break;
                    }
                }

                if(Is_dup!=1)
                {
                    if((teaser_arrays.every(checkCFL))===true)
                    {
                        state="CFL"
                    }
                    else if((teaser_arrays.every(checkCollege))===true)
                    {
                        state="College";
                    }
                    else if((teaser_arrays.every(checkNFL))===true)
                    {
                        state="NFL";
                    }
                    else if(teaser_arrays.every(checkBasketball)===true)
                    {
                        state="Basketball";
                    }
                    else
                    {
                        state="Cross League NFL";
                    }

                }
                // else
                // {
                //     if(teaser_arrays.every(checkBasketball)===true)
                //     {
                //         state="Basketball";
                //     }
                // }
            }
        }
        return state;
        function checkCFL(ele)
        {
            return ele==="12Cfl";
        }
        function checkNFL(ele)
        {
            return ele==="12Nfl";
        }
        function checkCollege(ele)
        {
            return ele==="12College";
        }
        function checkBasketball(ele)
        {
            return ele==="18Basketball";
        }
    };
    let _removeElement=function(arrays,elem){
        var index = arrays.indexOf(elem);
        if (index > -1) {
            arrays.splice(index, 1);
        }
        return arrays;
    };
    let _getParlay_Array=function(_cartObj){
        var parlayarray;
        if(_cartObj.subType=="Spread"||_cartObj.subType=="Money Line")
        {
            parlayarray="Drawline"+_cartObj.event_id;
        }
        else
        {
            parlayarray=_cartObj.subType+_cartObj.event_id;
        }
        parlay_arrays.push(parlayarray);
        return parlay_arrays;
    };
    let _getTeaser_Array=function(_cartObj) {
        var teaserarray;

        let league_name=_cartObj.league_name.toUpperCase();
        let sport_id = _cartObj.sport_id.toString();
        if(_cartObj.column==="money")
        {
            teaserarray="money";
        }
        else
        {
            if(((league_name.includes("CFL"))===true)&&(sport_id==="12"))
            {
                teaserarray="12Cfl";
            }
            else if(((league_name.includes("NFL"))===true)&&(sport_id==="12"))
            {
                teaserarray="12Nfl";
            }
            else if(((league_name.includes("CFL"))===false)&&(sport_id==="12")&&((league_name.includes("NFL"))===false))
            {
                teaserarray="12College";
            }
            else if(sport_id==="18")
            {
                teaserarray="18Basketball";
            }
            else
            {
                teaserarray=_cartObj.sport_id+_cartObj.league_name;
            }
        }

        teaser_arrays.push(teaserarray);

        return teaser_arrays;
    };
    let _getTarget_String=function(_targetArray){
        var target_string;
        if(_targetArray.subType=="Spread"||_targetArray.subType=="Money Line")
        {
            target_string="Drawline"+_targetArray.event_id;
        }
        else
        {
            target_string=_targetArray.subType+_targetArray.event_id;
        }
        return target_string;
    };
    let _getTeaserTarget_String=function(_targetArray){
        var targetstring;
        let league_name=_cartObj.league_name;
        if(_cartObj.column==="money")
        {
            targetstring="money";
        }
        else
        {
            if(((league_name.includes("CFL"))===true)&&(_cartObj.sport_id==="12"))
            {
                targetstring="12Cfl";
            }
            else if(((league_name.includes("NFL"))===true)&&(_cartObj.sport_id==="12"))
            {
                targetstring="12Nfl";
            }
            else if(((league_name.includes("CFL"))===false)&&(_cartObj.sport_id==="12")&&((league_name.includes("NFL"))===false))
            {
                targetstring="12College";
            }
            else if(_cartObj.sport_id==="18")
            {
                targetstring="18Basketball";
            }
            else
            {
                targetstring=_cartObj.sport_id+_cartObj.league_name;
            }
        }
        return targetstring;
    };
    let _displayBetCount=function() {
        //let betss = $('.bet_slip_add').length;
        let betss = $('.straight_bet_count').length;
        // $("#slip_count").text(slip_items.length);
        $(".__slip_count").text(betss);
        $("#total_bets").text(betss);
    }
    /*---------------------------------------- parlay slip item----------------------------------- */
    let _getStoredItems = function () {
        if (localStorage.getItem('slip_items') === null) {
            return [];
        }
        return JSON.parse(localStorage.getItem('slip_items'));
    }
    let _getUserLimits = function(){
        if (localStorage.getItem('user_limits') === 'undefined' || localStorage.getItem('user_limits') === null)
            return null;
        return JSON.parse(localStorage.getItem('user_limits'));
    }
    function _renderBetSLIP() {
        let slip_items = [];
        let betss = $('.bet_slip_add').length;
        if (betss === 0) {
            $(".bet_slip_empty").css('display', 'block');
        } else {
            $(".bet_slip_empty").css('display', 'none');
        }
        slip_items = _getStoredItems();
        var parlayrisk=0;
        var parlaywin=0;
        var teaserrisk=0;
        var teaserwin=0;
        var ifbetrisk=0;
        var ifbetwin=0;
        if(betss>=2)
        {
            if((!isNaN(slip_items[0].parlayrisk_amount))&&(!isNaN(slip_items[0].parlaywin_amount)))
            {
                parlayrisk=slip_items[0].parlayrisk_amount;
                parlaywin=slip_items[0].parlaywin_amount;
            }
            if((!isNaN(slip_items[0].teaserrisk_amount))&&(!isNaN(slip_items[0].teaserwin_amount)))
            {
                teaserrisk=slip_items[0].teaserrisk_amount;
                teaserwin=slip_items[0].teaserwin_amount;
            }
            if((!isNaN(slip_items[0].ifbetrisk_amount))&&(!isNaN(slip_items[0].ifbetwin_amount)))
            {
                ifbetrisk=slip_items[0].ifbetrisk_amount;
                ifbetwin=slip_items[0].ifbetwin_amount;
            }
        }
        var tworoundrisk=0;
        var tworoundwin=0;
        if(betss>=3)
        {
            if((!isNaN(slip_items[0].tworoundrobinrisk_amount))&&(!isNaN(slip_items[0].tworoundrobinwin_amount)))
            {
                tworoundrisk=slip_items[0].tworoundrobinrisk_amount;
                tworoundwin=slip_items[0].tworoundrobinwin_amount;
            }
        }
        var threeroundrisk=0;
        var threeroundwin=0;
        if(betss>=4)
        {
            if((!isNaN(slip_items[0].threeroundrobinrisk_amount))&&(!isNaN(slip_items[0].threeroundrobinwin_amount)))
            {
                threeroundrisk=slip_items[0].threeroundrobinrisk_amount;
                threeroundwin=slip_items[0].threeroundrobinwin_amount;
            }
        }
        var fourroundrisk=0;
        var fourroundwin=0;
        if(betss>=4)
        {
            if((!isNaN(slip_items[0].fourroundrobinrisk_amount))&&(!isNaN(slip_items[0].fourroundrobinwin_amount)))
            {
                fourroundrisk=slip_items[0].fourroundrobinrisk_amount;
                fourroundwin=slip_items[0].fourroundrobinwin_amount;
            }
        }
        $(".__slip_count").text($(".bet_slip_add").length);
        $("#total_bets").text($(".bet_slip_add").length);
        let stakee = 0;
        stakee=stakee+parlayrisk+tworoundrisk+threeroundrisk+fourroundrisk+teaserrisk+ifbetrisk;
        $('.risk_stake_slip').each(function (i, v) {
            if (($.trim($(v).val())).length > 0) {
                if (!isNaN($(v).val())) {
                    stakee = stakee + parseFloat($(v).val());
                }
            }
        });
        let winnning = 0;
        winnning=winnning+parlaywin+tworoundwin+threeroundwin+fourroundwin+teaserwin+ifbetwin;
        $('.risk_win_slip').each(function (i, v) {
            if (($.trim($(v).val())).length > 0) {
                if (!isNaN($(v).val())) {
                    winnning = winnning + parseFloat($(v).val());
                }
            }
        });
        $("#total_stake").text(stakee.toFixed(2));
        $("#possible_winning").text(winnning.toFixed(2));
    }
    function _updateWinRiskValue(win, risk, keyid) {
        let storageItem = _getStoredItems();
        if (storageItem.length !== 0) {
            storageItem.forEach((item) => {
                if (keyid === item.slip_item) {
                    if (!isNaN(win) && !isNaN(risk)) {
                        item.win_amount = win;
                        item.risk_amount = risk;
                    }
                }
            });
        }
        localStorage.setItem('slip_items', JSON.stringify(storageItem));
    }
    function _updateHandicapOddValue(Handicap,Odd,keyid)
    {
        let storageItem = _getStoredItems();
        if (storageItem.length !== 0) {
            storageItem.forEach((item) => {
                if (keyid === item.slip_item) {
                        item.handicap = Handicap;
                        item.odd = Odd;
                        if(parseInt(Odd)<0)
                        {
                            item.odd_sign ='';
                        }
                }
            });
        }
        localStorage.setItem('slip_items', JSON.stringify(storageItem));
    }
    function _updateParlayWinRiskValue(win,risk) {
        let storageItem = _getStoredItems();
        if (storageItem.length !== 0) {
            storageItem.forEach((item) => {
                {
                    item.parlaywin_amount = win;
                    item.parlayrisk_amount = risk;
                }
            });
        }
        localStorage.setItem('slip_items', JSON.stringify(storageItem));
    }
    function _updateIfBetWinRiskValue(win,risk){
        let storageItem = _getStoredItems();
        if (storageItem.length !== 0) {
            storageItem.forEach((item) => {
                {
                    item.ifbetwin_amount = win;
                    item.ifbetrisk_amount = risk;
                }
            });
        }
        localStorage.setItem('slip_items', JSON.stringify(storageItem));
    }
    function _updateReverseBetWinRiskValue(win,risk){
        let storageItem = _getStoredItems();
        if (storageItem.length !== 0) {
            storageItem.forEach((item) => {
                {
                    item.reversebetwin_amount = win;
                    item.reversebetrisk_amount = risk;
                }
            });
        }
        localStorage.setItem('slip_items', JSON.stringify(storageItem));
    }
    function _updateTwoRoundWinRiskValue(win,risk){
        let storageItem = _getStoredItems();
        if (storageItem.length !== 0) {
            storageItem.forEach((item) => {
                {
                    item.tworoundrobinwin_amount= win;
                    item.tworoundrobinrisk_amount= risk;
                }
            });
        }
        localStorage.setItem('slip_items', JSON.stringify(storageItem));
    }
    function _updateThreeRoundWinRiskValue(win,risk){
        let storageItem = _getStoredItems();
        if (storageItem.length !== 0) {
            storageItem.forEach((item) => {
                {
                    item.threeroundrobinwin_amount = win;
                    item.threeroundrobinrisk_amount = risk;
                }
            });
        }
        localStorage.setItem('slip_items', JSON.stringify(storageItem));
    }
    function _updateFourRoundWinRiskValue(win,risk)
    {
        let storageItem = _getStoredItems();
        if (storageItem.length !== 0) {
            storageItem.forEach((item) => {
                {
                    item.fourroundrobinwin_amount = win;
                    item.fourroundrobinrisk_amount = risk;
                }
            });
        }
        localStorage.setItem('slip_items', JSON.stringify(storageItem));
    }
    function _updateTeaserWinRiskValue(win,risk) {
        let storageItem = _getStoredItems();
        if (storageItem.length !== 0) {
            storageItem.forEach((item) => {
                {
                    item.teaserwin_amount = win;
                    item.teaserrisk_amount = risk;
                }
            });
        }
        localStorage.setItem('slip_items', JSON.stringify(storageItem));
    }
    function _popUpMessageBox(msg, cardid, validated) {
        if (validated === 1)
        {
            $("#limit_check_"+cardid).removeClass('d-none').addClass('d-inline-block').text(msg);
            $("#card_" + cardid).addClass('_has_error');
        } else if (validated === 2) {
            $("#cartMessage").removeClass('d-none').addClass('d-inline-block').addClass('bg-success').text(msg);
        } else if (validated === 3) {
            $(cardid).removeClass('hide').addClass('show').addClass('red-alert').text(msg);
            $(cardid).removeClass('hide').addClass('show').addClass('red-alert').text(msg);
        } else {
            $("#limit_check_"+cardid).removeClass('d-inline-block').addClass('d-none');
            $("#card_" + cardid).removeClass('_has_error');
        }
    }
    function _checkLimit(value, field) {
        let is_valid = 0;
        if (value.length ===0)
        {
            is_valid = 0;
            _popUpMessageBox("", field, is_valid);
        }
        else
        {
            let user_limit = _getUserLimits();
            if (user_limit === null)
            {
                console.log("user limits return null");
            }
            else
            {
                if (user_limit.straight_min > value) {
                    is_valid = 1;
                    _popUpMessageBox("You cannot bet less than " + user_limit.straight_min + " .",   field, is_valid);
                } else if (user_limit.straight_max < value) {
                    is_valid = 1;
                    _popUpMessageBox("You cannot bet more than " + user_limit.straight_max + " .",   field, is_valid);
                } else if (parseFloat(user_limit.credit_limit) < parseFloat($("#total_stake").text())) {
                    _popUpMessageBox("Please Review Wagers Carefully! click to confirm", "#messagebox", 3);
                } else {
                    is_valid = 0;
                    _popUpMessageBox("", field, is_valid);
                    _popUpMessageBox("Please Review Wagers Carefully! click to confirm", "#cartMessage", 2);
                }
            }
        }
    }
    let _cartObj = {
        event_id: '',
        sport_id: '',
        league_name: '',
        slip_item: '',
        home_team_id: '',
        away_team_id: '',
        teamName: '',
        subType: '',
        home_name: '',
        away_name: '',
        handicap: '',
        odd: '',
        time: '',
        date: '',
        column: '',
        row_line: '',
        win_amount: null,
        risk_amount: null,
        parlayrisk_amount:null,
        parlaywin_amount:null,
        tworoundrobinrisk_amount:null,
        tworoundrobinwin_amount:null,
        threeroundrobinrisk_amount:null,
        threeroundrobinwin_amount:null,
        fourroundrobinrisk_amount:null,
        fourroundrobinwin_amount:null,
        teaserrisk_amount:null,
        teaserwin_amount:null,
        ifbetrisk_amount:null,
        ifbetwin_amount:null,
        reversebetrisk_amount:null,
        reversebetwin_amount:null,
        bet_type: '',
        slug_wager: '',
        bet_extra_info: '',
        odd_sign :'',
        check_limit:false
    };
    let _clearBetSLip = function () {
        cart_array = _getStoredItems();
        cart_array.forEach(item => {
            $("#card_" + item.slip_item).remove();
            $("#" + item.slip_item).removeClass('odd_active');
        });
        localStorage.removeItem('slip_items');
        $("#dev-parlaybet-slip-container").empty();
        $("#dev-roundrobinbet-slip-container").empty();
        $("#dev-teaserbet-slip-container").empty();
        $("#dev-ifbet-slip-container").empty();
        $("#dev-reversebet-slip-container").empty();
        cart_array = [];
        parlay_arrays=[];
        teaser_arrays=[];
        _renderBetSLIP();
    };

    /* initiating the functions */
    _printSlipItem();
    _renderBetSLIP();
    /* ! initiating the functions */

    function SaveDataAPi(dataObj,parlayObj,tworoundObj,threeroundObj,fourroundObj,teaserObj,ifbetObj,reversebetObj,callback) {
        if ((dataObj.items.length !== 0)||(parlayObj.items.length!==0)||(tworoundObj.items.length!==0)||(threeroundObj.items.length!==0)||(fourroundObj.items.length!==0)||(teaserObj.items.length!==0)||(ifbetObj.items.length!==0)||(reversebetObj.items.length!==0))
        {
            //$("#confirm_password").removeClass('d-none');
            $("#passwordModal").modal('show');
            $("#submit_confirm_password").off().click(function (e) {
                let pass = $("#bet_confirm_password").val();
                if (pass.length === 0)
                {
                    $('#passwordMessage').html("password fields can't be empty");
                }
                else
                {
                    $("#submit_confirm_password").prop('disabled', true);
                    $.ajax({
                        url: "check-password-straight-bet",
                        type: "POST",
                        dataType: "JSON",
                        data:
                            {
                                "_token": $('meta[name="csrf-token"]').attr('content'),
                                password: pass
                            },
                        success: function (resPass)
                        {
                            if (resPass.status) {
                                $("#passwordModal").modal('hide');
                                $("#submit_confirm_password").prop('disabled', false);
                                $.ajax({
                                    url: "check-straight-bet",
                                    type: "POST",
                                    dataType: "JSON",
                                    data: {
                                        "_token": $('meta[name="csrf-token"]').attr('content'),
                                        items: JSON.stringify(dataObj.items),
                                        status: true
                                    },
                                    success: function (resCheck) {
                                        if (!resCheck.changed) {
                                            $.ajax({
                                                url: "save-straight-bet",
                                                type: "POST",
                                                dataType: "JSON",
                                                data: {
                                                    "_token": $('meta[name="csrf-token"]').attr('content'),
                                                    ...dataObj
                                                },
                                                success: function (res) {
                                                    callback(res);
                                                }
                                            });
                                            if(parlayObj.items.length!==0)
                                            {
                                                $.ajax({
                                                    url:"save-parlay-bet",
                                                    type:"POST",
                                                    dataType:"JSON",
                                                    data:{
                                                        "_token":$('meta[name="csrf-token"]').attr('content'),
                                                        ...parlayObj
                                                    },
                                                    success:function(res){
                                                        callback(res);
                                                    }
                                                });
                                            }
                                            if(tworoundObj.items.length!==0)
                                            {
                                                let TwoRoundrisk=parseFloat($("#twoteamparlay_risk").val());
                                                let  length=tworoundObj.items.length;
                                                for (var i = 0; i < length; i++) {
                                                    for (var j = i + 1; j < length; j++) {
                                                        TwoRoundsubmit(i, j, TwoRoundrisk,tworoundObj.items);
                                                    }
                                                }
                                            }
                                            if(threeroundObj.items.length!==0)
                                            {
                                                let ThreeRoundRisk=parseFloat($("#threeteamparlay_risk").val());
                                                let length=threeroundObj.items.length;
                                                for (var i = 0; i < length; i++) {
                                                    for (var j = i + 1; j < length; j++) {
                                                        for (var z = j + 1; z < length; z++) {
                                                            ThreeRoundsubmit(i, j, z, ThreeRoundRisk, threeroundObj.items);
                                                        }
                                                    }
                                                }
                                            }
                                            if(fourroundObj.items.length!==0)
                                            {
                                                let FourRoundRisk=parseFloat($("#fourteamparlay_risk").val());
                                                let length=fourroundObj.items.length;
                                                for (var i = 0; i < length; i++) {
                                                    for (var j = i + 1; j < length; j++) {
                                                        for (var z = j + 1; z < length; z++) {
                                                            for(var k=z+1;k<length;k++)
                                                            {
                                                                FourRoundsubmit(i, j, z, k, FourRoundRisk, fourroundObj.items);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            if(teaserObj.items.length!==0)
                                            {
                                                let Teaserbet_info=$(".Teaserbet_info>.row");
                                                let length=Teaserbet_info.length;
                                                for (var i=0;i<length;i++)
                                                {
                                                    if(teaserObj.items[i].subType==="Total")
                                                    {
                                                        let text=$(".Teaserbet_info>.row:eq("+i+")").find(".points").text();
                                                        var handicap=teaserObj.items[i].handicap;

                                                        var array=handicap.split(" ")[0]+" "+text;

                                                        teaserObj.items[i].handicap=array;
                                                        teaserObj.items[i].teamName=array;
                                                        if(teaserObj.items[i].bet_extra_info.other_team_name === teaserObj.items[i].home_name)
                                                           teaserObj.items[i].bet_extra_info.bet_on_team_name=teaserObj.items[i].away_name;
                                                        else
                                                        {
                                                            teaserObj.items[i].bet_extra_info.bet_on_team_name=teaserObj.items[i].home_name;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        let text=$(".Teaserbet_info>.row:eq("+i+")").find(".points").text();
                                                        teaserObj.items[i].handicap=text;
                                                    }
                                                }
                                                var teaserrisk_amount=$("#teaser_risk").val();
                                                var teaserwin_amount=$("#teaser_win").val();
                                                var newteaserObj={
                                                    items:teaserObj.items,
                                                    risk_amount:teaserrisk_amount,
                                                    win_amount:teaserwin_amount,
                                                    status:"teaser"
                                                };
                                                $.ajax({
                                                    url:"save-parlay-bet",
                                                    type:"POST",
                                                    dataType:"JSON",
                                                    data:{
                                                        "_token":$('meta[name="csrf-token"]').attr('content'),
                                                        ...newteaserObj
                                                    },
                                                    success:function(res){
                                                        callback(res);
                                                    }
                                                });
                                            }
                                            if(ifbetObj.items.length!==0)
                                            {
                                                var ifbetrisk_amount=$("#ifbet_risk").val();
                                                var ifbetwin_amount=$("#ifbet_win").val();
                                                var newifbetObj={
                                                    items:ifbetObj.items,
                                                    risk_amount:ifbetrisk_amount,
                                                    win_amount:ifbetwin_amount,
                                                    status:"ifbet"
                                                };
                                                $.ajax({
                                                    url:"save-parlay-bet",
                                                    type:"POST",
                                                    dataType:"JSON",
                                                    data:{
                                                        "_token":$('meta[name="csrf-token"]').attr('content'),
                                                        ...newifbetObj
                                                    },
                                                    success:function(res){
                                                        callback(res);
                                                    }
                                                });
                                            }
                                            if(reversebetObj.items.length!==0)
                                            {
                                                var reversebetrisk_amount=$("#reversebet_risk").val();
                                                var reversebetwin_amount=$("#reversebet_win").val();
                                                var newreversebetObj={
                                                    items:reversebetObj.items,
                                                    risk_amount:reversebetrisk_amount,
                                                    win_amount:reversebetwin_amount,
                                                    status:"reversebet"
                                                }
                                                $.ajax({
                                                    url:"save-parlay-bet",
                                                    type:"POST",
                                                    dataType:"JSON",
                                                    data:{
                                                        "_token":$('meta[name="csrf-token"]').attr('content'),
                                                        ...newreversebetObj
                                                    },
                                                    success:function(res){
                                                        callback(res);
                                                    }
                                                });
                                            }
                                        }
                                        else {
                                            $("#betNotice").modal('show');
                                        }
                                    }
                                });
                            }
                            else {
                                $("#passwordMessage").text("password didn't match, try again !");
                            }
                        }
                    });
                }
            });
        }
        else {
            alert("No Item is selected");
        }
    }

    function TwoRoundsubmit(i,j,TwoRoundRisk,TwoRoundArray)
    {
        let tworoundrisk=TwoRoundRisk;
        let TwoRoundarr=[];
        let TwoRoundfirst=TwoRoundArray[i];
        let TwoRoundsecond=TwoRoundArray[j];
        TwoRoundarr.push(TwoRoundfirst);
        TwoRoundarr.push(TwoRoundsecond);
        let totalodds=1;
        let score1=parseInt(TwoRoundArray[i].odd);
        if(score1>0)
        {
            var  odd1=(score1*1+100)/100;
        }
        else
        {
            var odd1=Math.abs((-100/score1*1).toFixed(2))+1
        }
        let score2=parseInt(TwoRoundArray[j].odd)
        {
            if(score2>0)
            {
                var  odd2=(score2*1+100)/100;
            }
            else
            {
                var odd2=Math.abs((-100/score2*1).toFixed(2))+1
            }
        }
        totalodds=odd1*odd2;
        var tworoundwin=Math.abs(((totalodds-1)*tworoundrisk).toFixed(2));
        var  parlayObj={
            items:TwoRoundarr,
            risk_amount:tworoundrisk,
            win_amount:tworoundwin,
            status:"tworound"
        };
        $.ajax({
            url:"save-parlay-bet",
            type:"POST",
            dataType:"JSON",
            data:{
                "_token":$('meta[name="csrf-token"]').attr('content'),
                ...parlayObj
            },
            success:function(res){
                callback(res);
            }
        });
    }

    function ThreeRoundsubmit(i,j,z,ThreeRoundRisk,ThreeRoundArray)
    {
        let threeroundrisk=ThreeRoundRisk;
        let ThreeRoundarr=[];
        let ThreeRoundfirst=ThreeRoundArray[i];
        let ThreeRoundsecond=ThreeRoundArray[j];
        let ThreeRoundthird=ThreeRoundArray[z];
        ThreeRoundarr.push(ThreeRoundfirst);
        ThreeRoundarr.push(ThreeRoundsecond);
        ThreeRoundarr.push(ThreeRoundthird);
        let totalodds=1;
        let score1=parseInt(ThreeRoundArray[i].odd);
        if(score1>0)
        {
            var  odd1=(score1*1+100)/100;
        }
        else
        {
            var odd1=Math.abs((-100/score1*1).toFixed(2))+1
        }
        let score2=parseInt(ThreeRoundArray[j].odd)
        {
            if(score2>0)
            {
                var  odd2=(score2*1+100)/100;
            }
            else
            {
                var odd2=Math.abs((-100/score2*1).toFixed(2))+1
            }
        }
        let score3=parseInt(ThreeRoundArray[z].odd)
        {
            if(score3>0)
            {
                var  odd3=(score3*1+100)/100;
            }
            else
            {
                var odd3=Math.abs((-100/score3*1).toFixed(2))+1
            }
        }
        totalodds=odd1*odd2*odd3;
        var threeroundwin=Math.abs(((totalodds-1)*threeroundrisk).toFixed(2));
        var  parlayObj={
            items:ThreeRoundarr,
            risk_amount:threeroundrisk,
            win_amount:threeroundwin,
            status:"threeround"
        }
        $.ajax({
            url:"save-parlay-bet",
            type:"POST",
            dataType:"JSON",
            data:{
                "_token":$('meta[name="csrf-token"]').attr('content'),
                ...parlayObj
            },
            success:function(res){
                callback(res);
            }
        });
    }

    function FourRoundsubmit(i,j,z,k,FourRoundRisk,FourRoundArray)
    {
        let fourroundrisk=FourRoundRisk;
        let FourRoundarr=[];
        let FourRoundfirst=FourRoundArray[i];
        let FourRoundsecond=FourRoundArray[j];
        let FourRoundthird=FourRoundArray[z];
        let FourRoundforth=FourRoundArray[k];
        FourRoundarr.push(FourRoundfirst);
        FourRoundarr.push(FourRoundsecond);
        FourRoundarr.push(FourRoundthird);
        FourRoundarr.push(FourRoundforth);
        let totalodds=1;
        let score1=parseInt(FourRoundArray[i].odd);
        if(score1>0)
        {
            var  odd1=(score1*1+100)/100;
        }
        else
        {
            var odd1=Math.abs((-100/score1*1).toFixed(2))+1
        }
        let score2=parseInt(FourRoundArray[j].odd)
        {
            if(score2>0)
            {
                var  odd2=(score2*1+100)/100;
            }
            else
            {
                var odd2=Math.abs((-100/score2*1).toFixed(2))+1
            }
        }
        let score3=parseInt(FourRoundArray[z].odd)
        {
            if(score3>0)
            {
                var  odd3=(score3*1+100)/100;
            }
            else
            {
                var odd3=Math.abs((-100/score3*1).toFixed(2))+1
            }
        }
        let score4=parseInt(FourRoundArray[k].odd)
        {
            if(score4>0)
            {
                var  odd4=(score4*1+100)/100;
            }
            else
            {
                var odd4=Math.abs((-100/score4*1).toFixed(2))+1;
            }
        }
        totalodds=odd1*odd2*odd3*odd4;
        var fourroundwin=Math.abs(((totalodds-1)*fourroundrisk).toFixed(2));
        var  parlayObj={
            items:FourRoundarr,
            risk_amount:fourroundrisk,
            win_amount:fourroundwin,
            status:"fourround"
        }
        $.ajax({
            url:"save-parlay-bet",
            type:"POST",
            dataType:"JSON",
            data:{
                "_token":$('meta[name="csrf-token"]').attr('content'),
                ...parlayObj
            },
            success:function(res)
            {
                callback(res);
            }
        });
    }
    return {
        addToCart: function (event_info, odds, type = null) {
            let team_name;
            let sub_type;
            let s_item_id;
            let  team_over_name
                ,extra_home_name
                ,team_under_name
                ,extra_away_name;
            if (type === "bwin") {
                s_item_id = odds.Unique_id;
                _cartObj.event_id = odds.Id;
                _cartObj.sport_id = event_info.SportId;
                _cartObj.league_name = event_info.LeagueName;
                _cartObj.slip_item = s_item_id;
                _cartObj.home_team_id = odds.Id;
                _cartObj.away_team_id = 0;
                _cartObj.teamName = odds.Name;
                _cartObj.subType = event_info.SportName + "-" + event_info.LeagueName;
                _cartObj.home_name = odds.Name;
                _cartObj.away_name = odds.Name;
                _cartObj.handicap = "";
                _cartObj.odd = odds.Odds;
                _cartObj.time = event_info.Time;
                _cartObj.date = event_info.Date;
                _cartObj.column = "";
                _cartObj.row_line = "";
                _cartObj.win_amount = null;
                _cartObj.risk_amount = null;
                _cartObj.bet_type = 'straight';
                _cartObj.slug_wager = ['', ''];
            }
            else if (type === "prop_bet") {
                odds = JSON.parse(odds);
                var betOnTeam;
                var otherTeam;
                if (odds.row_line === 'home')  {
                     betOnTeam  =  event_info.home.name;
                     otherTeam  =  event_info.away.name;
                } else if (odds.row_line === 'away')  {
                     betOnTeam  =  event_info.away.name;
                     otherTeam  =  event_info.home.name;
                } else {
                     betOnTeam  =  event_info.home.name;
                     otherTeam  =  event_info.away.name;
                }

                if(odds.team_name ==='Team Total - Home' || odds.team_name ==='Home Total Goals')
                {
                    team_name = event_info.home.name;
                    sub_type  = 'Team Total';
                }
                else if(odds.team_name ==='First Period')
                {
                    if (odds.row_line === 'home')  {
                        team_name = event_info.home.name;
                        sub_type  = 'First Period '+odds.betting_wager;
                   } else if (odds.row_line === 'away')  {
                        team_name = event_info.away.name;
                        sub_type  = 'First Period '+odds.betting_wager;
                   }

                }
                else if(odds.team_name ==='1st half')
                {
                    if (odds.row_line === 'home')  {
                        team_name = event_info.home.name;
                        sub_type  = 'First Half '+odds.betting_wager;
                   } else if (odds.row_line === 'away')  {
                        team_name = event_info.away.name;
                        sub_type  = 'First Half '+odds.betting_wager;
                   }

                }
                else if(odds.team_name ==='1st Set')
                {
                    if (odds.row_line === 'home')  {
                        team_name = event_info.home.name;
                        sub_type  = 'First Set '+odds.betting_wager;
                   } else if (odds.row_line === 'away')  {
                        team_name = event_info.away.name;
                        sub_type  = 'First Set '+odds.betting_wager;
                   }

                }
                else if(odds.team_name ==='First five innings')
                {
                    if (odds.row_line === 'home')  {
                        team_name = event_info.home.name;
                        sub_type  = 'First five innings '+odds.betting_wager;
                   } else if (odds.row_line === 'away')  {
                        team_name = event_info.away.name;
                        sub_type  = 'First five innings '+odds.betting_wager;
                   }

                }
                else if(odds.team_name ==='First Inning')
                {
                    if (odds.row_line === 'home')  {
                        team_name = event_info.home.name;
                        sub_type  = 'First Inning '+odds.betting_wager;
                   } else if (odds.row_line === 'away')  {
                        team_name = event_info.away.name;
                        sub_type  = 'First Inning '+odds.betting_wager;
                   }

                }
                else if(odds.team_name ==='2nd half')
                {
                    if (odds.row_line === 'home')  {
                        team_name = event_info.home.name;
                        sub_type  = 'Second Half '+odds.betting_wager;
                   } else if (odds.row_line === 'away')  {
                        team_name = event_info.away.name;
                        sub_type  = 'Second Half '+odds.betting_wager;
                   }

                }
                else if(odds.team_name ==='Winning Margin')
                {
                    if (odds.sub_type_name === 'home')  {
                        team_name = event_info.home.name;
                        sub_type  = 'Winning Margin';
                   } else if (odds.sub_type_name === 'away')  {
                        team_name = event_info.away.name;
                        sub_type  = 'Winning Margin';
                   }

                }
                else if(odds.team_name ==='Over Time')
                {
                    team_name = 'Over Time';
                    sub_type = '';
                }
                else if(odds.team_name ==='1st Quarter table')
                {
                    if (odds.row_line === 'home')  {
                        team_name = event_info.home.name;
                        sub_type  = '1st Quarter '+odds.betting_wager;
                   } else if (odds.row_line === 'away')  {
                        team_name = event_info.away.name;
                        sub_type  = '1st Quarter '+odds.betting_wager;
                   }

                }
                else if(odds.team_name ==='Team Total - Away'|| odds.team_name ==='Away Total Goals')
                {
                    team_name = event_info.away.name;
                    sub_type = 'Team Total';
                }
                else if(odds.team_name ==='Home team will score a Goal')
                {
                    team_name = event_info.home.name;
                    sub_type  = "will score a Goal" ;
                }
                else if(odds.team_name ==='Away team will score a Goal')
                {
                    team_name = event_info.away.name;
                    sub_type  = "will score a Goal" ;
                }
                else if(odds.team_name ==='Highest Scoring Half')
                {
                    team_name = 'Highest Scoring Half';
                    sub_type  = "" ;
                }
                else
                {
                    team_name = odds.team_name;
                    if(odds.sub_type_name ==='1st Quarter table')
                         sub_type = "1st Quarter";
                    else  sub_type = odds.sub_type_name;
                }
                s_item_id = odds.Unique_id;
                _cartObj.event_id = event_info.id;
                _cartObj.sport_id = event_info.sport_id;
                _cartObj.league_name = event_info.league.name;
                _cartObj.home_team_id = event_info.home.id;
                _cartObj.away_team_id = event_info.away.id;
                _cartObj.slip_item = s_item_id;
                _cartObj.teamName = team_name;
                _cartObj.subType = sub_type;
                _cartObj.home_name = event_info.home.name;
                _cartObj.away_name = event_info.away.name;
                _cartObj.handicap = ('handicap' in odds)?odds.handicap:"";
                _cartObj.odd = odds.odd;
                _cartObj.odd_sign = (parseInt(_cartObj.odd)>0)?"+":"";
                _cartObj.time = event_info.time;
                _cartObj.date = event_info.date;
                _cartObj.column = "";
                _cartObj.row_line = "";
                _cartObj.win_amount = null;
                _cartObj.risk_amount = null;
                _cartObj.bet_type = 'Props Bet';
                _cartObj.slug_wager = [odds.betting_wager, odds.row_line];
                _cartObj.bet_extra_info = {
                    bet_on_team_name  : betOnTeam,
                    other_team_name  : otherTeam,
                    betting_slug     : odds.row_line,
                    betting_wager    : odds.betting_wager,
                    prop_bet_type    : odds.team_name,
                    event_date : _cartObj.date,
                    event_time : _cartObj.time,
                };
            }
            else {
                if (odds.row_line === "home") {
                    team_name = event_info.home.name;
                    if (odds.column === 'spread') {
                        sub_type = "Spread";
                    } else if (odds.column === 'money') {
                        sub_type = "Money Line";
                    }
                } else if (odds.row_line === "away") {
                    team_name = event_info.away.name;
                    if (odds.column === 'spread') {
                        sub_type = "Spread";
                    } else if (odds.column === 'money') {
                        sub_type = "Money Line";
                    }
                } else if (odds.row_line === "draw") {
                    team_name = event_info.away.name;
                    sub_type = "Draw Line";
                } else if (odds.row_line === "over") {
                    team_name = odds.handicap;
                    extra_home_name = event_info.home.name;
                    extra_away_name = event_info.away.name;
                    odds.handicap = '';
                    sub_type = "Total";
                } else if (odds.row_line === "under") {
                    team_name = odds.handicap;
                    extra_home_name = event_info.away.name;
                    extra_away_name = event_info.home.name;
                    odds.handicap = '';
                    sub_type = "Total";
                }
                else if (odds.row_line === "one")
                {
                    team_name = event_info.home.name;
                    sub_type = "Team Total One";
                }
                else if (odds.row_line === "two")
                {
                    team_name = event_info.home.name;
                    sub_type = "Team Total Two";
                }
                else if (odds.row_line === "three")
                {
                    team_name = event_info.away.name;
                    sub_type = "Team Total Three";
                }
                else if (odds.row_line === "four")
                {
                    team_name = event_info.away.name;
                    sub_type = "Team Total Four";
                }
                s_item_id = event_info.id + "_" + odds.column + "_" + odds.row_line;
                _cartObj.event_id = event_info.id;
                _cartObj.sport_id = event_info.sport_id;
                _cartObj.league_name = event_info.league.name;
                _cartObj.home_team_id = event_info.home.id;
                _cartObj.away_team_id = event_info.away.id;
                _cartObj.slip_item = s_item_id;
                _cartObj.teamName = team_name;
                _cartObj.subType = sub_type;
                _cartObj.home_name = event_info.home.name;
                _cartObj.away_name = event_info.away.name;
                _cartObj.handicap = (odds.handicap !=="")?odds.handicap:"";
                _cartObj.odd = odds.odd;
                _cartObj.odd_sign = (parseInt(odds.odd)>0)?"+":"";
                _cartObj.time = odds.time;
                _cartObj.date = odds.date;
                _cartObj.column = odds.column;
                _cartObj.row_line = odds.row_line;
                _cartObj.win_amount = null;
                _cartObj.risk_amount = null;
                _cartObj.bet_type = 'straight';
                _cartObj.slug_wager = [odds.column, odds.row_line];
                if(odds.row_line === "over" || odds.row_line === "under")
                {
                    _cartObj.bet_extra_info = {
                        bet_on_team_name : extra_home_name,
                        other_team_name  : extra_away_name,
                        betting_slug     : odds.row_line,
                        betting_wager    : odds.columnName,
                        event_date : _cartObj.date,
                        event_time : _cartObj.time,
                    };
                }
                else
                {
                    _cartObj.bet_extra_info = {
                        bet_on_team_name : team_name,
                        other_team_name  : (team_name === event_info.home.name) ? event_info.away.name : event_info.home.name,
                        betting_slug     : odds.row_line,
                        betting_wager    : odds.columnName,
                        event_date : _cartObj.date,
                        event_time : _cartObj.time,
                    };
                }

            }


            if (!$("#" + s_item_id).hasClass('odd_active'))
            {
                $("#" + s_item_id).addClass('odd_active');
                cart_array = _getStoredItems();
                if (cart_array.length === 0) {
                    cart_array.push(_cartObj);
                    _createHtml(_cartObj);
                    localStorage.setItem('slip_items', JSON.stringify(cart_array));
                    _renderBetSLIP();
                } else {
                    let objitem = null;
                    cart_array.forEach((item_obj) => {
                        if (type === 'bwin') {
                            if (item_obj.slip_item !== odds.Unique_id) {
                                objitem = item_obj;
                            }
                        } else {
                            if (item_obj.id !== event_info.id) {
                                objitem = item_obj;
                            }
                        }

                    });
                    if (objitem !== null) {
                        cart_array.push(_cartObj);
                        _createHtml(_cartObj);
                        localStorage.setItem('slip_items', JSON.stringify(cart_array));
                        _renderBetSLIP();
                    }
                }
                if(type !== "prop_bet")
                {

                    /*parlay array insert*/
                    parlay_arrays=_getParlay_Array(_cartObj);
                    var count=cart_array.length;
                    var parlaynumber=_confirmDuplicated(count,parlay_arrays);
                    /*parlay array insert*/

                    /*teaser array insert*/
                    teaser_arrays=_getTeaser_Array(_cartObj);
                    var teaserbettype=_confirmTeaserDuplicated(count,teaser_arrays);
                    _displayTeaserHtml(count,teaserbettype);
                    /*teaser array insert*/
                    _displayParlayHtml(count,parlaynumber);
                }

                _displayBetCount();

            }
            else {
                let object_index = null;
                cart_array = _getStoredItems();
                cart_array.forEach((t_item) => {
                    if (t_item.slip_item === s_item_id) {
                        object_index = cart_array.indexOf(t_item, 0);
                    }
                });
                if (object_index !== null) {
                    var target_array=_getTarget_String(cart_array[object_index]);
                    var updated_arrays=_removeElement(parlay_arrays,target_array);
                    var teasertarget_array=_getTeaserTarget_String(cart_array[object_index]);
                    var updatedteaser_arrays=_removeElement(teaser_arrays,teasertarget_array);
                    cart_array.splice(object_index, 1);
                    $("#" + s_item_id).removeClass('odd_active');
                    $("#card_" + s_item_id).remove();
                }
                localStorage.setItem('slip_items', JSON.stringify(cart_array));
                var count=cart_array.length;
                var parlaynumber=_confirmDuplicated(count,updated_arrays);
                _displayParlayHtml(count,parlaynumber);
                var teaserbettype=_confirmTeaserDuplicated(count,updatedteaser_arrays);
                _displayTeaserHtml(count,teaserbettype);
                _renderBetSLIP();
                _displayBetCount();
            }
        },
        clearBetSlip: function () {
            _clearBetSLip();
        },
        remove_Slip_Item: function (item_id) {
            let object_index = null;
            cart_array = _getStoredItems();
            cart_array.forEach(item => {
                if (item.slip_item === item_id) {
                    object_index = cart_array.indexOf(item, 0);
                }
            });
            if (object_index !== null) {
                var target_array=_getTarget_String(cart_array[object_index]);
                var updated_arrays=_removeElement(parlay_arrays,target_array);
                var teasertarget_array=_getTeaserTarget_String(cart_array[object_index]);
                var updatedteaser_arrays=_removeElement(teaser_arrays,teasertarget_array);
                cart_array.splice(object_index, 1);
                $("#card_" + item_id).remove();
                $("#" + item_id).removeClass('odd_active');
            }
            localStorage.setItem('slip_items', JSON.stringify(cart_array));
            var count=cart_array.length;
            var parlaynumber=_confirmDuplicated(count,updated_arrays);
            _displayParlayHtml(count,parlaynumber);
            var teaserbettype=_confirmTeaserDuplicated(count,updatedteaser_arrays);
            _displayTeaserHtml(count,teaserbettype);
            _renderBetSLIP();
            _displayBetCount();
        },
        riskCal: function (scores, currentval, current='',bet_props = false) {
            let inputvalue = currentval.value;
            let fieldid = currentval.getAttribute("data-id");
            if(current !== '')
            {
                if(typeof(current)==="object")
                {
                     scores=parseFloat(current.get(0).parentNode.parentNode.parentNode.parentNode.parentNode.childNodes[1].childNodes[1].childNodes[0].childNodes[3].childNodes[1].childNodes[5].innerHTML);
                    var real_handicap=current.get(0).parentNode.parentNode.parentNode.parentNode.parentNode.childNodes[1].childNodes[1].childNodes[0].childNodes[3].childNodes[1].childNodes[5].innerHTML;
                    var real_odd=current.get(0).parentNode.parentNode.parentNode.parentNode.parentNode.childNodes[1].childNodes[1].childNodes[0].childNodes[3].childNodes[1].childNodes[1].innerHTML;
                    _updateHandicapOddValue(real_odd.replace("(","").replace(")",""),real_handicap,fieldid);
                }
                else
                {
                     scores=current;
                }
            }

            _checkLimit(inputvalue, fieldid);
            let win_amount = 0;
            if (scores >= 0) {
                win_amount = Math.abs(((scores * inputvalue) / 100).toFixed(2));
                if (bet_props) {
                    document.getElementById("input_win_" + fieldid).value = win_amount;
                } else {
                    document.getElementById("input_win_" + fieldid).value = win_amount;
                }
            } else {
                win_amount = Math.abs(((100 / scores) * inputvalue).toFixed(2));
                if (bet_props) {
                    document.getElementById("input_win_" + fieldid).value = win_amount;
                } else {
                    document.getElementById("input_win_" + fieldid).value = win_amount;
                }
            }
            _renderBetSLIP();
            _updateWinRiskValue(win_amount, inputvalue, fieldid);
        },
        winCal: function (score, currentval, current ='',bet_props = false) {
            let inputvalue = currentval.value;
            let fieldid = currentval.getAttribute("data-id");
            if(current !=='')
            {
                score=parseFloat(current.get(0).parentNode.parentNode.childNodes[1].childNodes[1].childNodes[0].childNodes[3].childNodes[1].childNodes[5].innerHTML);
                var real_handicap=current.get(0).parentNode.parentNode.parentNode.parentNode.parentNode.childNodes[1].childNodes[1].childNodes[0].childNodes[3].childNodes[1].childNodes[5].innerHTML;
                var real_odd=current.get(0).parentNode.parentNode.parentNode.parentNode.parentNode.childNodes[1].childNodes[1].childNodes[0].childNodes[3].childNodes[1].childNodes[1].innerHTML;
                _updateHandicapOddValue(real_odd.replace("(","").replace(")",""),real_handicap,fieldid);
            }

            let riskvalue = ("#input_risk_" + fieldid);
            if (bet_props) {
                riskvalue = riskvalue + "_11";
            }
            _checkLimit(inputvalue, fieldid);
            let risk_amount = 0;
            if (score >= 0) {
                risk_amount = Math.abs(((inputvalue * 100) / score).toFixed(2));
                $(riskvalue).val(risk_amount);
            } else {
                risk_amount = Math.abs(((score / 100) * inputvalue).toFixed(2));
                $(riskvalue).val(risk_amount);
            }
            _renderBetSLIP();
            _updateWinRiskValue(inputvalue, risk_amount, fieldid);
        },
        riskparlayCal:function(currentval){
            let risk= currentval.value;
            let field="Parlay";
            _checkLimit(parseFloat(risk),field);
            let array=_getStoredItems();
            let odds=[];
            let length=array.length;
            let totalodds=1;
            for (var i=0;i<length;i++)
            {
                let scores=parseInt(array[i].odd);
                if(scores>0)
                {
                    var  odd=(scores*1+100)/100;
                }
                else
                {
                    var odd=Math.abs((-100/scores*1).toFixed(2))+1
                }
                odds.push(odd);
            }
            for (var j=0;j<length;j++)
            {
                totalodds=totalodds*1*odds[j];
            }

            var win=Math.abs(((totalodds-1)*risk).toFixed(2));
            document.getElementById("parlay_win").value = win;
            _updateParlayWinRiskValue(win, parseFloat(risk));
            _renderBetSLIP();
        },
        winparlayCal:function(currentval){
            let  win=currentval.value;
            let field="Parlay";
            let array=_getStoredItems();
            let odds=[];
            let length=array.length;
            let totalodds=1;
            for (var i=0;i<length;i++)
            {
                let scores=parseInt(array[i].odd);
                if(scores>0)
                {
                    var odd=(scores*1+100)/100;
                }
                else
                {
                    var odd=Math.abs((-100/scores*1).toFixed(2))+1;
                }
                odds.push(odd);
            }
            for (var j=0;j<length;j++)
            {
                totalodds=totalodds*1*odds[j];
            }
            let risk=Math.abs((win/(totalodds-1)).toFixed(2));
            _checkLimit(parseFloat(risk),field);
            document.getElementById("parlay_risk").value = risk;
            _updateParlayWinRiskValue(parseFloat(win), risk);
            _renderBetSLIP();
        },
        twoparlayriskCal:function(currentval) {
            let totalwins=0;
            let field="RoundRobin";
            let scores=1;
            let win=1;
            let risk=currentval.value;
            let array=_getStoredItems();
            let count=array.length;
            let odds=new Array();
            for (var i=0;i<count;i++)
            {
                scores=parseInt(array[i].odd);
                if(scores>0)
                {
                    var odd=(scores*1+100)/100;
                }
                else
                {
                    var odd=Math.abs((-100/scores*1).toFixed(2))+1
                }
                odds.push(odd);
            }
            for (var i=0;i<count;i++)
            {
                for(var j=i+1;j<count;j++)
                {
                    win=Math.abs(((odds[i]*odds[j]-1)*risk).toFixed(2));
                    totalwins=totalwins+win;
                }
            }
            var finalwins=Math.abs((totalwins).toFixed(2));
            var totalbets=count*(count-1)/2;
            var total_risk=risk*totalbets;
            _checkLimit(parseFloat(total_risk),field);
            document.getElementById("twoteamparlay_win").value = finalwins;
            _updateTwoRoundWinRiskValue(finalwins, parseFloat(total_risk));
            _renderBetSLIP();
        },
        twoparlaywinCal:function(currentval){
            let totalodds=0;
            let field="RoundRobin";
            let scores=1;
            let win=currentval.value;
            let array=_getStoredItems();
            let count=array.length;
            let odds=new Array();
            for (var i=0;i<count;i++)
            {
                scores=parseInt(array[i].odd);
                if(scores>0)
                {
                    var odd=(scores*1+100)/100;
                }
                else
                {
                    var odd=Math.abs((-100/scores*1).toFixed(2))+1
                }
                odds.push(odd);
            }
            for (var i=0;i<count;i++)
            {
                for(var j=i+1;j<count;j++)
                {
                    var odd=Math.abs((odds[i]*odds[j]-1).toFixed(2));
                    totalodds=totalodds+odd;
                }
            }
            var risk=Math.abs((win/(totalodds)).toFixed(2));
            var totalbets=count*(count-1)/2;
            var total_risk=risk*totalbets;
            _checkLimit(parseFloat(total_risk),field);
            document.getElementById("twoteamparlay_risk").value = risk;
            _updateTwoRoundWinRiskValue(win, parseFloat(total_risk));
            _renderBetSLIP();
        },
        threeparlayriskCal:function(currentval){
            let totalwins=0;
            let field="RoundRobin";
            let scores=1;
            let win=1;
            let risk=currentval.value;
            let array=_getStoredItems();
            let count=array.length;
            let odds=new Array();
            for (var i=0;i<count;i++)
            {
                scores=parseInt(array[i].odd);
                if(scores>0)
                {
                    var odd=(scores*1+100)/100;
                }
                else
                {
                    var odd=Math.abs((-100/scores*1).toFixed(2))+1
                }
                odds.push(odd);
            }
            for (var i=0;i<count;i++)
            {
                for(var j=i+1;j<count;j++)
                {
                    for(var z=j+1;z<count;z++)
                    {
                        win=Math.abs(((odds[i]*odds[j]*odds[z]-1)*risk).toFixed(2));
                        totalwins=totalwins+win;
                    }
                }
            }
            var finalwins=Math.abs((totalwins).toFixed(2));
            var totalbets=count*(count-1)*(count-2)/6;
            var total_risk=risk*totalbets;
            _checkLimit(parseFloat(total_risk),field);
            document.getElementById("threeteamparlay_win").value = finalwins;
            _updateThreeRoundWinRiskValue(finalwins, parseFloat(total_risk));
            _renderBetSLIP();
        },
        threeparlaywinCal:function(currentval){
            let totalodds=0;
            let field="RoundRobin";
            let scores=1;
            let win=currentval.value;
            let array=_getStoredItems();
            let count=array.length;
            let odds=new Array();
            for (var i=0;i<count;i++)
            {
                scores=parseInt(array[i].odd);
                if(scores>0)
                {
                    var odd=(scores*1+100)/100;
                }
                else
                {
                    var odd=Math.abs((-100/scores*1).toFixed(2))+1
                }
                odds.push(odd);
            }
            for (var i=0;i<count;i++)
            {
                for(var j=i+1;j<count;j++)
                {
                    for(var z=j+1;z<count;z++)
                    {
                        var odd=Math.abs((odds[i]*odds[j]*odds[z]-1).toFixed(2));
                        totalodds=totalodds+odd;
                    }
                }
            }
            var risk=Math.abs((win/(totalodds)).toFixed(2));
            var totalbets=count*(count-1)*(count-2)/6;
            var total_risk=risk*totalbets;
            _checkLimit(parseFloat(total_risk),field);
            document.getElementById("threeteamparlay_risk").value = risk;
            _updateThreeRoundWinRiskValue(win, parseFloat(total_risk));
            _renderBetSLIP();
        },
        fourparlayriskCal:function(currentval) {
            let totalwins=0;
            let field="RoundRobin";
            let scores=1;
            let win=1;
            let risk=currentval.value;
            let array=_getStoredItems();
            let count=array.length;
            let odds=new Array();
            for (var i=0;i<count;i++)
            {
                scores=parseInt(array[i].odd);
                if(scores>0)
                {
                    var odd=(scores*1+100)/100;
                }
                else
                {
                    var odd=Math.abs((-100/scores*1).toFixed(2))+1
                }
                odds.push(odd);
            }
            for (var i=0;i<count;i++)
            {
                for(var j=i+1;j<count;j++)
                {
                    for(var z=j+1;z<count;z++)
                    {
                        for(var k=z+1;k<count;k++)
                        {
                            win=Math.abs(((odds[i]*odds[j]*odds[z]*odds[k]-1)*risk).toFixed(2));
                            totalwins=totalwins+win;
                        }
                    }
                }
            }
            var finalwins=Math.abs((totalwins).toFixed(2));
            var totalbets=count*(count-1)*(count-2)*(count-3)/24;
            var total_risk=risk*totalbets;
            _checkLimit(parseFloat(total_risk),field);
            document.getElementById("fourteamparlay_win").value = finalwins;
            _updateFourRoundWinRiskValue(finalwins, parseFloat(total_risk));
            _renderBetSLIP();
        },
        fourparlaywinCal:function(currentval) {
            let totalodds=0;
            let field="RoundRobin";
            let scores=1;
            let win=currentval.value;
            let array=_getStoredItems();
            let count=array.length;
            let odds=new Array();
            for (var i=0;i<count;i++)
            {
                scores=parseInt(array[i].odd);
                if(scores>0)
                {
                    var odd=(scores*1+100)/100;
                }
                else
                {
                    var odd=Math.abs((-100/scores*1).toFixed(2))+1
                }
                odds.push(odd);
            }
            for (var i=0;i<count;i++)
            {
                for(var j=i+1;j<count;j++)
                {
                    for(var z=j+1;z<count;z++)
                    {
                        for(var k=z+1;k<count;k++)
                        {
                            var odd=Math.abs((odds[i]*odds[j]*odds[z]*odds[k]-1).toFixed(2));
                            totalodds=totalodds+odd;
                        }
                    }
                }
            }
            var risk=Math.abs((win/(totalodds)).toFixed(2));
            var totalbets=count*(count-1)*(count-2)*(count-3)/24;
            var total_risk=risk*totalbets;
            _checkLimit(parseFloat(total_risk),field);
            document.getElementById("fourteamparlay_risk").value = risk;
            _updateFourRoundWinRiskValue(win, parseFloat(total_risk));
            _renderBetSLIP();
        },
        teaserriskCal:function(currentval){
            let risk=currentval.value;
            let field="Teaser";
            _checkLimit(parseFloat(risk),field);
            let win=0;
            let odd;
            let score=parseInt($("#odd").text());
            if(score>0)
            {
                odd=(score*1+100)/100;
            }
            else
            {
                odd=Math.abs((-100/score*1).toFixed(2))+1
            }
            win=Math.abs(((odd-1)*risk).toFixed(2));
            $('#teaser_win').val(win);
            _updateTeaserWinRiskValue(win, parseFloat(risk));
            _renderBetSLIP();
        },
        teaserwinCal:function(currentval){
            let win=currentval.value;
            let field="Teaser";
            let risk=0;
            let odd;
            let score=parseInt($("#odd").text());
            if(score>0)
            {
                odd=(score*1+100)/100;
            }
            else
            {
                odd=Math.abs((-100/score*1).toFixed(2))+1
            }
            risk=Math.abs((win/(odd-1)).toFixed(2));
            _checkLimit(parseFloat(risk),field);
            $('#teaser_risk').val(risk);
            _updateTeaserWinRiskValue(parseFloat(win), risk);
            _renderBetSLIP();
        },
        IfBetriskCal:function(currentval){
            let scores = 0;
            let win = 0;
            let field="ifbet";
            let wins = 0;
            let odd;
            let risk = currentval.value;
            _checkLimit(parseFloat(risk),field);
            let array=_getStoredItems();
            let count = array.length;
            for (var i = 0; i < count; i++) {
                scores = array[i].odd;
                if (scores > 0) {
                    odd = (scores * 1 + 100) / 100;
                } else {
                    odd = Math.abs((-100 / scores * 1).toFixed(2)) + 1
                }
                win = Math.abs(((odd - 1) * risk).toFixed(2));
                wins = wins + win;
            }
            $("#ifbet_win").val(Math.abs(wins.toFixed(2)));
            _updateIfBetWinRiskValue(wins, parseFloat(risk));
            _renderBetSLIP();

        },
        IfBetwinCal:function(currentval){
            let totalodds = 0;
            let scores = 0;
            let odd;
            let odds=[];
            let field="ifbet";
            let risk = 0;
            let win = currentval.value;
            let array =_getStoredItems();
            let count =array.length;
            for (var i = 0; i < count; i++)
            {
                scores = array[i].odd;
                if (scores > 0) {
                    odd = (scores * 1 + 100) / 100 - 1;
                } else {
                    odd = Math.abs((-100 / scores * 1).toFixed(2));
                }
                odds.push(odd);
            }
            for (var j = 0; j < count; j++) {
                totalodds = totalodds + odds[j];
            }
            risk = Math.abs((win / totalodds).toFixed(2));
            _checkLimit(parseFloat(risk),field);
            $('#ifbet_risk').val(risk);
            _updateIfBetWinRiskValue(parseFloat(win), risk);
            _renderBetSLIP();
        },
        ReverseBetriskCal:function(currentval) {
            let scores = 0;
            let win = 0;
            let wins = 0;
            let field="reversebet";
            let finalwins;
            let odd;
            let array=_getStoredItems();
            let risk = currentval.value;
            _checkLimit(risk,field);
            let count = array.length;
            for (var i = 0; i < count; i++) {
                scores = parseFloat(array[i].odd);
                if (scores > 0) {
                    odd = (scores * 1 + 100) / 100;
                } else {
                    odd = Math.abs((-100 / scores * 1).toFixed(2)) + 1
                }
                win = Math.abs(((odd - 1) * risk).toFixed(2));
                wins = wins + win;
            }
            finalwins=Math.abs((2 * wins).toFixed(2));
            $('#reversebet_win').val(finalwins);
            _updateReverseBetWinRiskValue(finalwins, parseFloat(risk));
            _renderBetSLIP();
        },
        ReverseBetwinCal:function(currentval) {
            let totalodds = 0;
            let scores = 0;
            let field="reversebet";
            let risk = 0;
            let odd;
            let array=_getStoredItems();
            let win = currentval.value;
            let count = array.length;
            let odds = [];
            for (var i = 0; i < count; i++) {
                scores = parseFloat(array[i].odd);
                if (scores > 0) {
                    odd = (scores * 1 + 100) / 100 - 1;
                } else {
                    odd = Math.abs((-100 / scores * 1).toFixed(2));
                }
                odds.push(odd);
            }
            for (var j = 0; j < count; j++) {
                totalodds = totalodds + odds[j];
            }
            risk = Math.abs((win / (2 * totalodds)).toFixed(2));
            _checkLimit(risk,field);
            $('#reversebet_risk').val(risk);
            _updateReverseBetWinRiskValue(win, parseFloat(risk));
            _renderBetSLIP();
        },
        saveData: function () {
            let dataArray = _getStoredItems();

            if(dataArray!==[])
            {
                var  myObj = {
                    items: dataArray,
                    status: "oka"
                };
                if((dataArray[0].parlayrisk_amount!=="")&&(dataArray[0].parlayrisk_amount!==null)&&(dataArray[0].parlayrisk_amount!==0))
                {
                    var  parlayObj={
                        items:dataArray,
                        risk_amount:dataArray[0].parlayrisk_amount,
                        win_amount:dataArray[0].parlaywin_amount,
                        status:"parlay"
                    }
                }
                else
                {
                    var parlayObj={
                        items:[]
                    }
                }
                if((dataArray[0].tworoundrobinrisk_amount!=="")&&(dataArray[0].tworoundrobinrisk_amount!==null)&&(dataArray[0].tworoundrobinrisk_amount!==0))
                {
                    var tworoundObj={
                        items:dataArray,
                        status:"tworoundrobin"
                    }
                }
                else
                {
                    var tworoundObj={
                        items:[]
                    }
                }
                if((dataArray[0].threeroundrobinrisk_amount!=="")&&(dataArray[0].threeroundrobinrisk_amount!==null)&&(dataArray[0].threeroundrobinrisk_amount!==0))
                {
                    var threeroundObj={
                        items:dataArray,
                        status:"threeroundrobin"
                    }
                }
                else
                {
                    var threeroundObj={
                        items:[]
                    }
                }
                if((dataArray[0].threeroundrobinrisk_amount!=="")&&(dataArray[0].fourroundrobinrisk_amount!==null)&&(dataArray[0].fourroundrobinrisk_amount!==0))
                {
                    var fourroundObj={
                        items:dataArray,
                        status:"fourroundrobin"
                    }
                }
                else
                {
                    var fourroundObj={
                        items:[]
                    }
                }
                if((dataArray[0].teaserrisk_amount!=="")&&(dataArray[0].teaserrisk_amount!==null)&&(dataArray[0].teaserrisk_amount!==0))
                {
                    var teaserObj={
                        items:dataArray,
                        risk_amount:dataArray[0].teaserrisk_amount,
                        win_amount:dataArray[0].teaserwin_amount,
                        status:"teaser"
                    }
                }
                else
                {
                    var teaserObj={
                        items:[]
                    }
                }
                if((dataArray[0].ifbetrisk_amount!=="")&&(dataArray[0].ifbetrisk_amount!==null)&&(dataArray[0].ifbetrisk_amount!==0))
                {
                    var ifbetObj={
                        items:dataArray,
                        risk_amount:dataArray[0].ifbetrisk_amount,
                        win_amount:dataArray[0].ifbetwin_amount,
                        status:"ifbet"
                    }
                }
                else
                {
                    var ifbetObj={
                        items:[]
                    }
                }
                if((dataArray[0].reversebetrisk_amount!=="")&&(dataArray[0].reversebetrisk_amount!==null)&&(dataArray[0].reversebetrisk_amount!==0))
                {
                    var reversebetObj={
                        items:dataArray,
                        risk_amount:dataArray[0].reversebetrisk_amount,
                        win_amount:dataArray[0].reversebetwin_amount,
                        status:"reversebet"
                    }
                }
                else
                {
                    var reversebetObj={
                        items:[]
                    }
                }
            }
            SaveDataAPi(myObj, parlayObj,tworoundObj,threeroundObj,fourroundObj,teaserObj,ifbetObj,reversebetObj,function (res) {
                if (res.status) {
                    $('#header_available_balance').html(res.user.available_balance);
                    $('#header_pending_amount').html(res.user.pending_amount);
                    _clearBetSLip();
                    $('#cartMessage').html('<div class="alert alert-success alert-dismissible">'+
                        '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+
                        '<strong>Success!</strong> Successfully submit your bet.'+
                        '</div>').removeClass('d-none');
                } else {
                    $('#cartMessage').html('<div class="alert alert-danger alert-dismissible">'+
                        '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+
                        '<strong>Warning!</strong> '+res.response+
                        '</div>').removeClass('d-none');
                }
            })
        },
        toggleParlay:function()
        {
            if(togglecount%2==0)
            {
                $("#toggle").removeClass("fa-plus");
                $("#toggle").addClass("fa-minus");
                $("#parlay-content").css("display","block");
                togglecount++;
            }
            else
            {
                $("#toggle").removeClass("fa-minus");
                $("#toggle").addClass("fa-plus");
                $("#parlay-content").css("display","none");
                togglecount++;
            }

        },
        toggleRoundRobin:function()
        {
            if(Roundtogglecount%2==0)
            {
                $("#RoundRobintoggle").removeClass("fa-plus");
                $("#RoundRobintoggle").addClass("fa-minus");
                $("#twoteamparlay-content").css("display","block");
                $("#threeteamparlay-content").css("display","block");
                $("#fourteamparlay-content").css("display","block");
                Roundtogglecount++;
            }
            else
            {
                $("#RoundRobintoggle").removeClass("fa-minus");
                $("#RoundRobintoggle").addClass("fa-plus");
                $("#twoteamparlay-content").css("display","none");
                $("#threeteamparlay-content").css("display","none");
                $("#fourteamparlay-content").css("display","none");
                Roundtogglecount++;
            }
        },
        toggleTeaser:function()
        {
            if(Teasertogglecount%2==0)
            {
                $("#Teasertoggle").removeClass("fa-plus");
                $("#Teasertoggle").addClass("fa-minus");
                $("#Teaser-content").css("display","block");
                Teasertogglecount++;
            }
            else
            {
                $("#Teasertoggle").removeClass("fa-minus");
                $("#Teasertoggle").addClass("fa-plus");
                $("#Teaser-content").css("display","none");
                Teasertogglecount++;
            }

        },
        toggleIfBet:function()
        {
            if(IfBettogglecount%2==0)
            {
                $("#IfBettoggle").removeClass("fa-plus");
                $("#IfBettoggle").addClass("fa-minus");
                $("#IfBet-content").css("display","block");
                IfBettogglecount++;
            }
            else
            {
                $("#IfBettoggle").removeClass("fa-minus");
                $("#IfBettoggle").addClass("fa-plus");
                $("#IfBet-content").css("display","none");
                IfBettogglecount++;
            }
        },
        toggleReverseBet:function()
        {
            if(ReverseBettogglecount%2==0)
            {
                $("#ReverseBettoggle").removeClass("fa-plus");
                $("#ReverseBettoggle").addClass("fa-minus");
                $("#ReverseBet-content").css("display","block");

                ReverseBettogglecount++;
            }
            else
            {
                $("#ReverseBettoggle").removeClass("fa-minus");
                $("#ReverseBettoggle").addClass("fa-plus");
                $("#ReverseBet-content").css("display","none");
                ReverseBettogglecount++;
            }
        },
        teaserselected:function(current)
        {
            var currentdata=current.get(0).options[current.get(0).selectedIndex].innerHTML;
            let teaser_infoarray= _teaserinfo();
            _teaserinfo_display(teaser_infoarray);
            let risk=$("#teaser_risk").val();
            _updateteaserriskCal(risk)
        },
        Buyingpoint:function(current)
        {

            var currentdata=current.get(0).options[current.get(0).selectedIndex].innerHTML;
            var bet_slip=current.get(0).parentNode.parentNode.childNodes[1].childNodes[1].childNodes[0].childNodes[3].childNodes[1];
            var currentvalue=current.get(0).parentNode.parentNode.childNodes[5].childNodes[1].childNodes[0].childNodes[1].childNodes[1];
            var Pre_handicap=current.get(0).parentNode.childNodes[13].value;
            let teamName = current.get(0).closest('.add_title').querySelector('.tname1');
            let slip_title = current.get(0).closest('.add_title').querySelector('.bet_slip_title');
            let odd = slip_title.querySelector('.odd');
            let handicap_item = slip_title.querySelector('.handcap');
            if(currentdata ==='Buy Points!')
            {
                let dd = $(current.get(0)).closest('.bet_slip_add');
                let id = dd.attr('id');
                let targetId = id.replace('card_', "");
                let initvalue = $.trim($('#'+targetId).html());

                if(initvalue.includes('Over'))
                {
                    teamName.innerHTML = initvalue.split(' ')[0]+' '+initvalue.split(' ')[1];
                    odd.innerHTML = initvalue.split(' ')[2].replace("(","").replace(")","");
                }
                else if(initvalue.includes('Under'))
                {
                    teamName.innerHTML = initvalue.split(' ')[0]+' '+initvalue.split(' ')[1];
                    odd.innerHTML =initvalue.split(' ')[2].replace("(","").replace(")","");
                }
                else
                {
                    bet_slip.childNodes[1].innerHTML = initvalue.split(' ')[0];
                    odd.innerHTML = initvalue.split(' ')[1].replace("(","").replace(")","");
                }
                this.riskCal(1,currentvalue,odd.innerHTML);
            }
            else if(currentdata !== "Buying options")
            {

                if(teamName.innerHTML.split(' ')[0] === 'Over')
                {
                    var  handicap=teamName.innerHTML.split(' ')[1];
                    var type="over";
                    var old_handicap=handicap;
                    var old_odd=odd.innerHTML;
                }
                else if(teamName.innerHTML.split(' ')[0] === 'Under')
                {
                    var handicap=teamName.innerHTML.split(' ')[1];
                    var type="under";
                    var old_handicap=handicap;
                    var old_odd=odd.innerHTML;
                }
                else
                {
                    var handicap=handicap_item.innerHTML;
                    var type="spread"
                }

                if(currentdata.split(" ")[0]==="½")
                {
                    var bonus_handicap=0.5;
                }
                else if(currentdata.split(" ")[0]==="1")
                {
                    var bonus_handicap=1.0;
                }
                else if (currentdata.split(" ")[0]==="1½")
                {
                    var bonus_handicap=1.5;
                }

                if(teamName.innerHTML.split(' ')[0] ==='Under')
                {
                    var new_handicap=parseFloat(parseFloat(handicap)+bonus_handicap).toFixed(1);
                    var new_odd=currentdata.split(" ")[2].replace("(","").replace(")","");
                }
                else if(teamName.innerHTML.split(' ')[0] === 'Over')
                {
                    var new_handicap=parseFloat(parseFloat(handicap)-bonus_handicap).toFixed(1);
                    var new_odd=currentdata.split(" ")[2].replace("(","").replace(")","");
                }
                else
                {
                    var new_handicap=parseFloat(parseFloat(handicap)+bonus_handicap).toFixed(1);
                    var new_odd=currentdata.split(" ")[2].replace("(","").replace(")","");
                }

                if(type==="over")
                {
                    teamName.innerHTML="Over "+new_handicap.toString();
                    var real_handicap="Over "+new_handicap.toString();
                }
                else if(type==="under")
                {
                    teamName.innerHTML ="Under "+new_handicap.toString();
                    var real_handicap="Under "+new_handicap.toString();
                }
                else if(type==="spread")
                {
                    if(new_handicap>0)
                    {
                        bet_slip.childNodes[1].innerHTML= "+"+new_handicap.toString();
                        var real_handicap="+"+new_handicap.toString();
                    }
                    else
                    {
                        bet_slip.childNodes[1].innerHTML=new_handicap.toString();
                        var real_handicap=new_handicap.toString();
                    }
                }
                if( parseFloat(new_odd) < 0)  bet_slip.childNodes[3].innerHTML = '';
                odd.innerHTML=new_odd;
                if(currentvalue.value !==" ")
                {
                    let fieldid = currentvalue.getAttribute("data-id");
                    _updateHandicapOddValue(real_handicap,new_odd,fieldid);
                    this.riskCal(1,currentvalue,new_odd);
                }
            }
        },
    }
})(Handlebars, jQuery);







const HorseBetslip=(function (Obj, $){
        function _popUpMessageBox(msg, cardid, validated) {
            if (validated === 1)
            {
                $("#limit_check_"+cardid).removeClass('d-none').addClass('d-inline-block').text(msg);
                $("#card_" + cardid).addClass('_has_error');
            } else if (validated === 2) {
                $("#cartMessage").removeClass('d-none').addClass('d-inline-block').addClass('bg-success').text(msg);
            } else if (validated === 3) {
                $(cardid).removeClass('hide').addClass('show').addClass('red-alert').text(msg);
                $(cardid).removeClass('hide').addClass('show').addClass('red-alert').text(msg);
            } else {
                $("#limit_check_"+cardid).removeClass('d-inline-block').addClass('d-none');
                $("#card_" + cardid).removeClass('_has_error');
            }
        }
        function _checkLimit(value, field) {
            let is_valid = 0;
            if (value.length ===0)
            {
                is_valid = 0;
                _popUpMessageBox("", field, is_valid);
            }
            else
            {
                let user_limit = _getUserLimits();
                if (user_limit === null)
                {
                    console.log("user limits return null");
                }
                else
                {
                    if (user_limit.straight_min > value) {
                        is_valid = 1;
                        _popUpMessageBox("You cannot bet less than " + user_limit.straight_min + " .",   field, is_valid);
                    } else if (user_limit.straight_max < value) {
                        is_valid = 1;
                        _popUpMessageBox("You cannot bet more than " + user_limit.straight_max + " .",   field, is_valid);
                    } else if (parseFloat(user_limit.credit_limit) < parseFloat($("#total_stake").text())) {
                        _popUpMessageBox("Please Review Wagers Carefully! click to confirm", "#messagebox", 3);
                    } else {
                        is_valid = 0;
                        _popUpMessageBox("", field, is_valid);
                        _popUpMessageBox("Please Review Wagers Carefully! click to confirm", "#cartMessage", 2);
                    }
                }
            }
        }
        let _getUserLimits = function(){
            if (localStorage.getItem('user_limits') === 'undefined' || localStorage.getItem('user_limits') === null)
                return null;
            return JSON.parse(localStorage.getItem('user_limits'));
        }
        let horsecart_array = [];
        let _getStoredItems = function () {
            if (localStorage.getItem('HorseBetSlip_items') === null) {
                return [];
            }
            return JSON.parse(localStorage.getItem('HorseBetSlip_items'));
        };
        let _insertSelectedItems=function(horse_num,tournament_name,tournament_id,Race_name,horse_name,horse_id,Race_date,Race_id,Race_type,unique_id) {
            horsecart_array=_getStoredItems();
            let _horsecartObj={};
            _horsecartObj.tournament_name=tournament_name;
            _horsecartObj.tournament_id=tournament_id;
            _horsecartObj.Race_name=Race_name;
            _horsecartObj.horse_name=horse_name;
            _horsecartObj.Race_date=Race_date;
            _horsecartObj.Race_id=Race_id;
            _horsecartObj.Race_type=Race_type;
            _horsecartObj.horse_id=horse_id;
            _horsecartObj.unique_id=unique_id;
            _horsecartObj.button_id=tournament_id+"_"+Race_id;
            _horsecartObj.active=0;
            _horsecartObj.horse_num=horse_num;
            _horsecartObj.created_at=new Date();
            _horsecartObj.risk_amount=null;
            _horsecartObj.random_id=(Math.floor((Math.random() * 100000000) + 1)).toString();
            horsecart_array.push(_horsecartObj);
            localStorage.setItem("HorseBetSlip_items",JSON.stringify(horsecart_array));
        };
        let _removeSelectedItems=function(unique_id) {
            horsecart_array=_getStoredItems();
            horsecart_array.forEach((t_item) => {
                if ((t_item.unique_id === unique_id)&&(t_item.active===0)) {
                    object_index = horsecart_array.indexOf(t_item, 0);
                }
            });
            horsecart_array.splice(object_index, 1);

            localStorage.setItem("HorseBetSlip_items",JSON.stringify(horsecart_array));
        };
        let _clearStoredItem=function() {
            horsecart_array=[];
            localStorage.setItem("HorseBetSlip_items",JSON.stringify(horsecart_array));
        };
        let _addButtonConfirm=function(button_id) {
            horsecart_array = _getStoredItems();
            horsecart_array.forEach((each_item) => {
                if (each_item.button_id === button_id) {
                    var button=document.getElementById(button_id);
                    button.removeAttribute("disabled");
                }
            });
        };
        let _disableButtonComfirm=function(button_id) {   var num=0;
            horsecart_array.forEach((each_item) => {
                if (each_item.button_id === button_id) {
                    num++
                }
            });
            if(num===0)
            {
                var button=document.getElementById(button_id);
                button.setAttribute("disabled","true");
            }
        };
        let _createHtml=function(button_id){
            if (document.getElementById('horse-bet-card-slip') !== null) {
                $(".horse_bet_slip_empty").css("display","none");
                let horsecart_array=_getStoredItems();
                horsecart_array.forEach((t_item)=> {
                    if ((t_item.button_id === button_id)&&(t_item.active===0))
                    {
                         var obj=t_item;
                        t_item.active=1;
                        let slip_obj = document.getElementById('horse-bet-card-slip').innerHTML;
                        let templete = Obj.compile(slip_obj);
                        const contex={
                            horse_slip_item: obj
                        };
                        let generatedHtml = templete(contex);
                        $("#dev-horsebet-slip-container").append(generatedHtml);
                    }
                });
                localStorage.setItem("HorseBetSlip_items", JSON.stringify(horsecart_array));
                document.getElementById(button_id).setAttribute("disabled","true");
                var all_buttons=document.querySelectorAll('[id^="Horse_'+button_id+'"]');
                for(var i=0;i<all_buttons.length;i++)
                {
                    all_buttons[i].setAttribute("class","bet-btn");
                }
            }
            $("#horse_slip_count").text($(".horse_bet_slip_add").length);
            $("#horse_total_bets").text($(".horse_bet_slip_add").length);
        };
        let _confirmW_P_S=function(share_id,current_type,keyword){
            if(keyword==="add")
            {
                if(current_type==="win")
                {
                    let Place_id=share_id+"_Place";
                    let Show_id=share_id+"_Show";
                    let W_P_S_id=share_id+"_W/P/S";
                    let Current_Place_Class=document.getElementById(Place_id).getAttribute("class");
                    let Current_Show_Class=document.getElementById(Show_id).getAttribute("class");
                    if((Current_Place_Class==="bet-btn addhorseactive")&&(Current_Show_Class==="bet-btn addhorseactive"))
                    {
                        document.getElementById(W_P_S_id).setAttribute("class","bet-btn addhorseactive");
                    }
                }
                else if(current_type==="place")
                {
                    let Win_id=share_id+"_Win";
                    let Show_id=share_id+"_Show";
                    let W_P_S_id=share_id+"_W/P/S";
                    let Current_Win_Class=document.getElementById(Win_id).getAttribute("class");
                    let Current_Show_Class=document.getElementById(Show_id).getAttribute("class");
                    if((Current_Win_Class==="bet-btn addhorseactive")&&(Current_Show_Class==="bet-btn addhorseactive"))
                    {
                        document.getElementById(W_P_S_id).setAttribute("class","bet-btn addhorseactive");
                    }
                }
                else if(current_type==="show")
                {
                    let Win_id=share_id+"_Win";
                    let Place_id=share_id+"_Place";
                    let W_P_S_id=share_id+"_W/P/S";
                    let Current_Win_Class=document.getElementById(Win_id).getAttribute("class");
                    let Current_Place_Class=document.getElementById(Place_id).getAttribute("class");
                    if((Current_Win_Class==="bet-btn addhorseactive")&&(Current_Place_Class==="bet-btn addhorseactive"))
                    {
                        document.getElementById(W_P_S_id).setAttribute("class","bet-btn addhorseactive");
                    }
                }
            }
          else
            {
                if(current_type==="win")
                {
                    let Place_id=share_id+"_Place";
                    let Show_id=share_id+"_Show";
                    let W_P_S_id=share_id+"_W/P/S";
                    let Current_Place_Class=document.getElementById(Place_id).getAttribute("class");
                    let Current_Show_Class=document.getElementById(Show_id).getAttribute("class");
                    if((Current_Place_Class==="bet-btn addhorseactive")&&(Current_Show_Class==="bet-btn addhorseactive"))
                    {
                        document.getElementById(W_P_S_id).setAttribute("class","bet-btn");
                    }
                }
                else if(current_type==="place")
                {
                    let Win_id=share_id+"_Win";
                    let Show_id=share_id+"_Show";
                    let W_P_S_id=share_id+"_W/P/S";
                    let Current_Win_Class=document.getElementById(Win_id).getAttribute("class");
                    let Current_Show_Class=document.getElementById(Show_id).getAttribute("class");
                    if((Current_Win_Class==="bet-btn addhorseactive")&&(Current_Show_Class==="bet-btn addhorseactive"))
                    {
                        document.getElementById(W_P_S_id).setAttribute("class","bet-btn");
                    }
                }
                else if(current_type==="show")
                {
                    let Win_id=share_id+"_Win";
                    let Place_id=share_id+"_Place";
                    let W_P_S_id=share_id+"_W/P/S";
                    let Current_Win_Class=document.getElementById(Win_id).getAttribute("class");
                    let Current_Place_Class=document.getElementById(Place_id).getAttribute("class");
                    if((Current_Win_Class==="bet-btn addhorseactive")&&(Current_Place_Class==="bet-btn addhorseactive"))
                    {
                        document.getElementById(W_P_S_id).setAttribute("class","bet-btn");
                    }
                }
            }
        };
        let _clearActivedHorseCartArray=function() {
            let horsecart_array=_getStoredItems();
            var object_index_array=[];
            horsecart_array.forEach((item)=> {
                    if (item.active === 1) {
                        object_index=horsecart_array.indexOf(item,0);
                        object_index_array.push(object_index);
                    }
                    horsecart_array.splice(object_index_array[0],object_index_array.length);
                });
            localStorage.setItem("HorseBetSlip_items",JSON.stringify(horsecart_array));
            $("#horse_slip_count").text(0);
            $("#horse_total_bets").text(0);
        };
        let _Cal_AllRisk=function(){
            let horsecart_array=_getStoredItems();
            let total_risk=0;
            horsecart_array.forEach((item)=>
            {
                if((item.risk_amount!==null))
                {
                    total_risk=total_risk+parseFloat(item.risk_amount);
                }
            });
            $("#horse_total_stake").text(total_risk);
        };
        let _getHorseObj=function() {
            let horsecart_array=_getStoredItems();
            var getHorseObj=[];
            horsecart_array.forEach((item)=>
            {
                if(item.active===1)
                {
                    getHorseObj.push(item);
                }
            });
            return getHorseObj;
        };

        let  _removeSameLinedata=function(tournament_id,Race_id,horse_id){
           let horsecart_array=_getStoredItems();
           let object_index_array=[];
           horsecart_array.forEach((item)=>
               {
                   if((item.tournament_id===tournament_id)&&(item.Race_id===Race_id)&&(item.horse_id===horse_id)&&(item.active===0))
                   {
                       var object_index=horsecart_array.indexOf(item,0);
                       object_index_array.push(object_index);
                   }
               },
           );
            horsecart_array.splice(object_index_array[0],object_index_array.length);
            localStorage.setItem("HorseBetSlip_items",JSON.stringify(horsecart_array));
        };
        $( document ).ready(function() {
            _clearStoredItem();
            $(window).load(function() {
                // Animate loader off screen
                $(".se-pre-con").fadeOut("slow");;
            });
            $(".horse_bet_slip_empty").css("display","block");
        });
        let _clearBetSlipContent=function()
        {
            $("#dev-horsebet-slip-container").empty();
            $(".horse_bet_slip_empty").css("display","block");
            $("#horse_total_stake").text(0);
            $("#horse_slip_count").text(0);
            _clearActivedHorseCartArray();
        }
        return {
        clearBetSlip:function() {
                $("#dev-horsebet-slip-container").empty();
                $(".horse_bet_slip_empty").css("display","block");
                $("#horse_total_stake").text(0);
                $("#horse_slip_count").text(0);
               _clearActivedHorseCartArray();
            },
        horsebettoggle: function (current) {
            var icon=current.getAttribute("class");
            if(icon==="fa fa-minus")
            {
                current.setAttribute("class","fa fa-plus");
            }
            else
            {
                current.setAttribute("class","fa fa-minus");
            }
            var node=current.parentNode.parentNode.parentNode.parentNode.nextSibling.nextSibling;
            if((node.style.display==="")||(node.style.display)==="none")
            {
                node.style.display="block";
            }
            else if(node.style.display==="block")
            {
                node.style.display="none";
            }
    },
        addToHorseCart(button_id) {
           _createHtml(button_id);
        },
        removeHorseCart:function(unique_id,created_time,current){
            current.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.remove();
            horsecart_array=_getStoredItems();
            horsecart_array.forEach((t_item) =>{
                if ((t_item.unique_id === unique_id)&&(t_item.created_at===created_time)&&(t_item.active===1)){
                    object_index = horsecart_array.indexOf(t_item, 0);
                }
            });
            horsecart_array.splice(object_index, 1);
            localStorage.setItem("HorseBetSlip_items",JSON.stringify(horsecart_array));
            if($(".horse_bet_slip_add").length===0) {
                $(".horse_bet_slip_empty").css("display","block");
            }
            $("#horse_slip_count").text($(".horse_bet_slip_add").length);
            $("#horse_total_bets").text($(".horse_bet_slip_add").length);
        },
        Winsave:function(current,horse_num,tournament_name,tournament_id,Race_name,horse_name,horse_id,Race_date,Race_id) {
             let unique_id="win"+tournament_id+Race_id+horse_id;
             let button_id=tournament_id+"_"+Race_id;
             let share_id="Horse_"+button_id+"_"+horse_id;
            let Race_type="win";
            var currentclass=current.getAttribute("class");
            if(currentclass==="bet-btn") {
                let keyword="add";
                _insertSelectedItems(horse_num,tournament_name,tournament_id,Race_name,horse_name,horse_id,Race_date,Race_id,Race_type,unique_id);
               _addButtonConfirm(button_id);
                current.setAttribute("class","bet-btn addhorseactive");
                _confirmW_P_S(share_id,Race_type,keyword);
            }
            else {
                current.setAttribute("class","bet-btn");
                let keyword="delete";
                _removeSelectedItems(unique_id);
                _disableButtonComfirm(button_id);
                _confirmW_P_S(share_id,Race_type,keyword)
            }
         },
        Placesave:function(current,horse_num,tournament_name,tournament_id,Race_name,horse_name,horse_id,Race_date,Race_id) {
             let unique_id="place"+tournament_id+Race_id+horse_id;
             let button_id=tournament_id+"_"+Race_id;
            let share_id="Horse_"+button_id+"_"+horse_id;
            let Race_type="place";
            var currentclass=current.getAttribute("class");
            if(currentclass==="bet-btn")
            {
                let keyword="add";
                _insertSelectedItems(horse_num,tournament_name,tournament_id,Race_name,horse_name,horse_id,Race_date,Race_id,Race_type,unique_id);
               _addButtonConfirm(button_id);
                current.setAttribute("class","bet-btn addhorseactive");
                _confirmW_P_S(share_id,Race_type,keyword);
            }
            else
            {
                current.setAttribute("class","bet-btn");
                let keyword="delete";
                _removeSelectedItems(unique_id);
                _disableButtonComfirm(button_id);
                _confirmW_P_S(share_id,Race_type,keyword)
            }
         },
        Showsave:function(current,horse_num,tournament_name,tournament_id,Race_name,horse_name,horse_id,Race_date,Race_id){
            let unique_id="show"+tournament_id+Race_id+horse_id;
            let button_id=tournament_id+"_"+Race_id;
            let share_id="Horse_"+button_id+"_"+horse_id;
            let Race_type="show";
            var currentclass=current.getAttribute("class");
            if(currentclass==="bet-btn")
            {
                let keyword="add";
                _insertSelectedItems(horse_num,tournament_name,tournament_id,Race_name,horse_name,horse_id,Race_date,Race_id,Race_type,unique_id);
               _addButtonConfirm(button_id);
                current.setAttribute("class","bet-btn addhorseactive");
                _confirmW_P_S(share_id,Race_type,keyword);
            }
            else
            {
                current.setAttribute("class","bet-btn");
                let keyword="delete";
                _removeSelectedItems(unique_id);
                _disableButtonComfirm(button_id);
                _confirmW_P_S(share_id,Race_type,keyword)
            }
        },
        W_P_S:function(current,horse_num,tournament_name,tournament_id,Race_name,horse_name,horse_id,Race_date,Race_id){
                var currentclass=current.getAttribute("class");
                let button_id=tournament_id+"_"+Race_id;
                if(currentclass==="bet-btn")
                {
                    current.setAttribute("class","bet-btn addhorseactive");
                    let  Win_id="Horse_"+button_id+"_"+horse_id+"_Win";
                    let  Place_id="Horse_"+button_id+"_"+horse_id+"_Place";
                    let  Show_id="Horse_"+button_id+"_"+horse_id+"_Show";
                    let keyword="save";
                    _removeSameLinedata(tournament_id,Race_id,horse_id);
                    document.getElementById(Win_id).setAttribute("class","bet-btn addhorseactive");
                    this.W_P_S_Winsave(horse_num,tournament_name,tournament_id,Race_name,horse_name,horse_id,Race_date,Race_id,keyword);
                    document.getElementById(Place_id).setAttribute("class","bet-btn addhorseactive");
                    this.W_P_S_Placesave(horse_num,tournament_name,tournament_id,Race_name,horse_name,horse_id,Race_date,Race_id,keyword);
                    document.getElementById(Show_id).setAttribute("class","bet-btn addhorseactive");
                    this.W_P_S_Showsave(horse_num,tournament_name,tournament_id,Race_name,horse_name,horse_id,Race_date,Race_id,keyword);
                    _addButtonConfirm(button_id);
                }
                else if(currentclass==="bet-btn addhorseactive")
                {
                    current.setAttribute("class","bet-btn");
                    _disableButtonComfirm(button_id);
                    let  Win_id="Horse_"+button_id+"_"+horse_id+"_Win";
                    let  Place_id="Horse_"+button_id+"_"+horse_id+"_Place";
                    let  Show_id="Horse_"+button_id+"_"+horse_id+"_Show";
                    let keyword="deletd";
                    document.getElementById(Win_id).setAttribute("class","bet-btn");
                    this.W_P_S_Winsave(horse_num,tournament_name,tournament_id,Race_name,horse_name,horse_id,Race_date,Race_id,keyword);
                    document.getElementById(Place_id).setAttribute("class","bet-btn");
                    this.W_P_S_Placesave(horse_num,tournament_name,tournament_id,Race_name,horse_name,horse_id,Race_date,Race_id,keyword);
                    document.getElementById(Show_id).setAttribute("class","bet-btn");
                    this.W_P_S_Showsave(horse_num,tournament_name,tournament_id,Race_name,horse_name,horse_id,Race_date,Race_id,keyword);
                    _disableButtonComfirm(button_id);
                }
        },
        W_P_S_Winsave:function(horse_num,tournament_name,tournament_id,Race_name,horse_name,horse_id,Race_date,Race_id,keyword){
            let unique_id="win"+tournament_id+Race_id+horse_id;
            let button_id=tournament_id+"_"+Race_id;
            let Race_type="win";
            if(keyword==="save")
            {
                _insertSelectedItems(horse_num,tournament_name,tournament_id,Race_name,horse_name,horse_id,Race_date,Race_id,Race_type,unique_id);
            }
            else
            {
                _removeSelectedItems(unique_id);
            }
        },
        W_P_S_Placesave:function(horse_num,tournament_name,tournament_id,Race_name,horse_name,horse_id,Race_date,Race_id,keyword){
            let unique_id="win"+tournament_id+Race_id+horse_id;
            let button_id=tournament_id+"_"+Race_id;
            let Race_type="win";
            if(keyword==="save")
            {
                _insertSelectedItems(horse_num,tournament_name,tournament_id,Race_name,horse_name,horse_id,Race_date,Race_id,Race_type,unique_id);
            }
            else
            {
                _removeSelectedItems(unique_id);
            }
        },
        W_P_S_Showsave:function(horse_num,tournament_name,tournament_id,Race_name,horse_name,horse_id,Race_date,Race_id,keyword){
            let unique_id="win"+tournament_id+Race_id+horse_id;
            let button_id=tournament_id+"_"+Race_id;
            let Race_type="win";
            if(keyword==="save")
            {
                _insertSelectedItems(horse_num,tournament_name,tournament_id,Race_name,horse_name,horse_id,Race_date,Race_id,Race_type,unique_id);
            }
            else
            {
              _removeSelectedItems(unique_id);
            }

        },
        InputRisk:function(current) {
            var unique_data=current.getAttribute("id");
            var data=unique_data.split(",");
            var checkid="";
            let horsecart_array=_getStoredItems();
            horsecart_array.forEach((item)=>
            {
                if((item.unique_id===data[0])&&(item.created_at===data[1])&&(item.active===1))
                {
                   item.risk_amount=current.value;
                   checkid=item.unique_id+item.random_id;
                }
            });
            localStorage.setItem("HorseBetSlip_items",JSON.stringify(horsecart_array));
            _checkLimit(current.value,checkid);
            _Cal_AllRisk();
        },
        saveHorseBetData:function(){
            var horsearray=_getHorseObj();
            var  horseObj={
                items:horsearray,
                status:"horse"
            };
            $("#passwordModal").modal('show');
            $("#submit_confirm_password").off().click(function (e) {
                let pass = $("#bet_confirm_password").val();
                if (pass.length === 0)
                {
                    $('#passwordMessage').html("password fields can't be empty");
                }
                else
                {
                    $("#submit_confirm_password").prop('disabled', true);
                    $.ajax({
                        url: "check-password-straight-bet",
                        type: "POST",
                        dataType: "JSON",
                        data:
                            {
                                "_token": $('meta[name="csrf-token"]').attr('content'),
                                password: pass
                            },
                        success: function (resPass)
                        {
                            if (resPass.status)
                            {
                                $("#passwordModal").modal('hide');
                                $("#submit_confirm_password").prop('disabled', false);
                                $.ajax({
                                    url: "save-horse-bet",
                                    type: "POST",
                                    dataType: "JSON",
                                    data: {
                                        "_token": $('meta[name="csrf-token"]').attr('content'),
                                        ...horseObj
                                    },
                                    success: function (res)
                                    {
                                        if (res.status===true)
                                        {
                                            _clearBetSlipContent();
                                            $('#header_available_balance').html(res.user.available_balance);
                                            $('#header_pending_amount').html(res.user.pending_amount);
                                            $('#cartMessage').html('<div class="alert alert-success alert-dismissible">'+
                                                '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+
                                                '<strong>Success!</strong> Successfully submit your bet.'+
                                                '</div>').removeClass('d-none');
                                        }
                                        else
                                       {
                                           $('#cartMessage').html('<div class="alert alert-danger alert-dismissible">'+
                                                '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+
                                                '<strong>Warning!</strong> '+res.response+
                                                '</div>').removeClass('d-none');
                                        }
                                    }
                                });
                            }
                            else
                                {
                                $("#passwordMessage").text("password didn't match, try again !");
                            }
                        }
                    });
                }
            });
        }
        }}
)(Handlebars, jQuery);

