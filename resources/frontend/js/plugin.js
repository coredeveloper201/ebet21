let plugin = (function ($) {
    'use strict'


    $('input').focus(function () {
        $(this).parents('.form-group').addClass('focused');
    });

    $('input').blur(function () {
        var inputValue = $(this).val();
        if (inputValue == "") {
            $(this).removeClass('filled');
            $(this).parents('.form-group').removeClass('focused');
        } else {
            $(this).addClass('filled');
        }
    });



    /* $(window).on('scroll', function () {
        var wScroll = $(this).scrollTop();
        if (wScroll > 180) {
            $('.selip').addClass('selip-fixed');
        } else {
            $('.selip').removeClass('selip-fixed');
        }
        ;
    }); */


  /*   $('.tab ul.tabs').addClass('active').find('> li:eq(0)').addClass('current');
    $('#body_warapper').on('click','.tab ul.tabs li a',function (g) {
        var tab = $(this).closest('.tab'),
            index = $(this).closest('li').index();

        tab.find('ul.tabs > li').removeClass('current');
        $(this).closest('li').addClass('current');

        tab.find('.tab_content').find('div.tabs_item').not('div.tabs_item:eq(' + index + ')').slideUp();
        tab.find('.tab_content').find('div.tabs_item:eq(' + index + ')').slideDown();

        g.preventDefault();
    }); */


    $('ul.nabvar li a').click(function () {
        $('li a').removeClass("actives");
        $(this).addClass("actives");
    });


    /*     $(document).ready(function() {
            window.onscroll = function() { myFunction() };

            var header = document.getElementById("live-bet-slip");
            var sticky = header.offsetTop;

            function myFunction() {
                if (window.pageYOffset > sticky) {
                    header.classList.add("sticky");
                } else {
                    header.classList.remove("sticky");
                }
            }
        });
     */


    class SideNav {
        constructor() {
            this.counter = 0;
            if (document.querySelector('.js-side-nav') === null) {
                this.counter++;
            } else {
                this.sideNavEl = document.querySelector('.js-side-nav');
            }


            if (document.querySelector('.js-side-nav-container') === null) {
                this.counter++;
            } else {
                this.sideNavContainerEl = document.querySelector('.js-side-nav-container');
            }

            if (document.querySelector('.js-menu-open') === null) {
                this.counter++;
            } else {
                this.showButtonEl = document.querySelector('.js-menu-open');
            }


            if (document.querySelector('.js-menu-close') === null) {
                this.counter++;
            } else {
                this.closeButtonEl = document.querySelector('.js-menu-close');
            }


            this.openSideNav = this.openSideNav.bind(this);
            this.closeSideNav = this.closeSideNav.bind(this);
            this.blockClicks = this.blockClicks.bind(this);
            this.onTransitionEnd = this.onTransitionEnd.bind(this);
            if (this.counter === 0)
                this.addEventListeners();
        }

        addEventListeners() {
            this.showButtonEl.addEventListener('click', this.openSideNav);
            this.closeButtonEl.addEventListener('click', this.closeSideNav);
            this.sideNavEl.addEventListener('click', this.blockClicks);
            this.sideNavContainerEl.addEventListener('click', this.closeSideNav);
        }

        blockClicks(evt) {
            evt.stopPropagation();
        }

        onTransitionEnd(evt) {
            this.sideNavContainerEl.classList.remove('side-nav-animatable');
            this.sideNavContainerEl.removeEventListener('transitionend', this.onTransitionEnd);
        }

        openSideNav() {
            this.sideNavContainerEl.classList.add('side-nav-animatable');
            this.sideNavContainerEl.classList.add('side-nav-visible');
            this.sideNavContainerEl.addEventListener('transitionend', this.onTransitionEnd);
        }

        closeSideNav() {
            this.sideNavContainerEl.classList.add('side-nav-animatable');
            this.sideNavContainerEl.classList.remove('side-nav-visible');
            this.sideNavContainerEl.addEventListener('transitionend', this.onTransitionEnd);
        }

    }

    new SideNav();


    /* login page code */
    let site_path = window.location.pathname;
    if (site_path === "/") {
        if (localStorage.getItem('slip_items') !== null || localStorage.getItem('slip_items') !== 'undefined') {
            localStorage.removeItem('slip_items');
        }
        if (localStorage.getItem('user_limits') !== null || localStorage.getItem('user_limits') !== 'undefined') {
            localStorage.removeItem('user_limits');
        }
    }
    /* !login page code */

    /*CLICK TO TOP DIV HIDE SHOW*/
    var $logo = $('#logo-scroll');
    $(document).scroll(function() {
        $logo.css({display: $(this).scrollTop()>100 ? "block":"none"});
    });
    /*CLICK TO TOP DIV HIDE SHOW*/

    /*CLICK TO TOP ACTION*/
    $('#logo-scroll').click(function(){
        $('html, body').animate({scrollTop:0}, 'slow');
        return false;
    });
    /*CLICK TO TOP ACTION*/
    /*nav bar sticky mobile*/
    if(window.innerWidth < 768){
        var navbarCasino = document.getElementById("navbar-casino");
        if(navbarCasino != null){
            var sticky = navbarCasino.offsetTop;
            /*casino scroll-navbar-mobile*/
            window.onscroll = function() {
                if (window.pageYOffset >= sticky) {
                    navbarCasino.classList.add("sticky-casion")
                } else {
                    navbarCasino.classList.remove("sticky-casion");
                }
            };
        }

        var navbarLiveBet = document.getElementById("navbar-live-bet");
        if(navbarLiveBet != null){
            var stickyLive = navbarLiveBet.offsetTop;
            /*liveBet scroll-navbar-mobile*/
            window.onscroll = function() {
                if (window.pageYOffset >= stickyLive) {
                    navbarLiveBet.classList.add("sticky-live")
                } else {
                    navbarLiveBet.classList.remove("sticky-live");
                }
            };
        }
    }
    /*nav bar sticky mobile*/
    if(window.innerWidth > 767){
        //$(".clp-target").hide();
        $(".single_game_catagory").find('.collapse').addClass('show');

    }
    else
    {
        //$(".clp-target").hide();
        $(".single_game_catagory").find('.collapse').removeClass('show');
    }


    return {
        changeIcon: function (data) {
            $(data).find(".clp-target").toggle();
          /*   if(window.innerWidth <= 767){
                $(data).find(".clp-target").toggle();
            } */
        },
    }
    /* window.onbeforeunload = function () {
         confirm("Do you really want to close?");
     };*/


})(jQuery);
