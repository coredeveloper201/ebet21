
(function($){
    "use strict"
    /* defined */
    window.l  = console.log;
    window.lg = console.group;
    window.lge = console.groupEnd;
    window.lgc = console.groupCollapsed;
    window.at  = alert;
    window._token = $('meta[name="csrf-token"]').attr('content');


    let months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul","Aug", "Sep", "Oct", "Nov", "Dec"];


    lgc("datetime");
    lge();

    let _currentYearWeeks = function()
    {
        const dt = new Date();
        let fullYear = dt.setUTCFullYear(dt.getFullYear());
        let getMonths = new Date(dt.getUTCMonth(fullYear));
        l(getMonths);
    }


    function getWeekNumber(d) {
        d = new Date(Date.UTC(d.getFullYear(), d.getMonth(), d.getDate()));
        d.setUTCDate(d.getUTCDate() + 4 - (d.getUTCDay()||7));
        var yearStart = new Date(Date.UTC(d.getUTCFullYear(),0,1));
        var weekNo = Math.ceil(( ( (d - yearStart) / 86400000) + 1)/7);
        return [d.getUTCFullYear(), weekNo];
    }

    let _weeksCount = function(year, month_number) {
        var firstOfMonth = new Date(year, month_number - 1, 1);
        var day = firstOfMonth.getDay() || 6;
        day = day === 1 ? 0 : day;
        if (day) { day-- }
        var diff = 7 - day;
        var lastOfMonth = new Date(year, month_number, 0);
        var lastDate = lastOfMonth.getDate();
        if (lastOfMonth.getDay() === 1) {
            diff--;
        }
        var result = Math.ceil((lastDate - diff) / 7);
        return result + 1;
      };
      let opt = '';
      let weeknumbers = 0;
      for(let i=1;i<=12;i++)
      {
         let wc = _weeksCount(2019, i);
         let wcc = wc-1;
         opt +=`<optgroup label="2019 ${months[i-1]}">`;
           for(let j=1;j<=parseInt(wc);j++)
           {    wcc--;
                weeknumbers += wcc;
                opt+=`<option value="${weeknumbers}">Week ${j}</option>`;
           }
           opt +=`</optgroup>`;
      }
      $('.print_all_weeks').html(opt);


})(jQuery);
