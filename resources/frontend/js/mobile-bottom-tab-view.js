(function ($) {
    "use strict"

    let screenWidth = screen.width;
    if (screenWidth <= 767)
    {
        let go_continue = $('.go_conintue button');
         $(go_continue).html("<span>Go <i class=\"fas fa-angle-right\"></i></span>");
        $('.-mobile-devices-previous-button a').html("<i class=\"fas fa-angle-left\"></i>");
        go_continue.removeClass('btn-cont')
        go_continue.addClass('btn-transparent')
    }
    else {
        $('.go_conintue input').val("continue");
    }

})(jQuery);
