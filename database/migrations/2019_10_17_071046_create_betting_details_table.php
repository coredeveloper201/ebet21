<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBettingDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('betting_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('sport_league', 200)->nullable();
            $table->bigInteger('user_id');
            $table->bigInteger('ticket_id')->nullable();
            $table->string('event_id')->nullable();
            $table->dateTime('event_date')->nullable();
            $table->integer('team_id')->default(0);
            $table->string('sport_name')->nullable()->comment('sport_id');
            $table->string('is_away', 4)->nullable();
            $table->string('is_home', 4)->nullable();
            $table->decimal('betting_win', 10, 2)->default(0);
            $table->decimal('betting_condition', 10, 2)->default(0);
            $table->string('betting_condition_original', 300)->nullable();
            $table->string('type',150)->default(0)->comment('1 for run line, 2 for money line, 3 for total runs');
            $table->integer('wager')->default(0)->comment('1 for risk, 2 for win, 3 for base amount');
            $table->decimal('risk_amount', 10, 2)->default(0);
            $table->decimal('betting_amount', 10, 2)->default(0);
            $table->string('token_id', 50)->nullable();
            $table->tinyInteger('status')->nullable()->comment('0 for start, 2 for bet confirm');
            $table->tinyInteger('result')->nullable()->comment('0 for pending, 1 for win, 2 for loss');
            $table->decimal('user_current_credit_limit', 10, 2)->default(0);
            $table->decimal('user_current_max_limit', 10, 2)->default(0);
            $table->decimal('user_current_pending', 10, 2)->default(0);
            $table->decimal('user_current_balance', 10, 2)->default(0);
            $table->tinyInteger('free_play')->default(0);
            $table->string('bet_type', 30)->nullable();
            $table->text('bet_extra_info')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('betting_details');
    }
}
