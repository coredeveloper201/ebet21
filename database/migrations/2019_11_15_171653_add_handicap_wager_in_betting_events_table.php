<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddHandicapWagerInBettingEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('betting_events', function (Blueprint $table) {
            $table->string('handicap')->nullable()->after('betting_type');
            $table->string('wager')->nullable()->after('handicap');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('betting_events', function (Blueprint $table) {
        });
    }
}
