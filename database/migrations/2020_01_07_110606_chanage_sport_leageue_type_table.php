<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChanageSportLeageueTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('wager_disables', function (Blueprint $table) {
            DB::statement("ALTER TABLE `wager_disables` CHANGE `sport_league` `sport_league` VARCHAR(200) NOT NULL");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('wager_disables', function (Blueprint $table) {
            DB::statement('ALTER TABLE `wager_disables` CHANGE `sport_league` `sport_league` int(11) NOT NULL');
        });
    }
}
