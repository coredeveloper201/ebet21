<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHorseBettingDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('horse_betting_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id');
            $table->bigInteger('ticket_id')->nullable();
            $table->string('tournament_id', 200)->nullable();
            $table->string('tournament_name',200)->nullable();
            $table->string('race_id',200)->nullable();
            $table->string('race_name',200)->nullable();
            $table->string('race_type',200)->nullable();
            $table->string('race_date',200)->nullable();
            $table->string('horse_id',200)->nullable();
            $table->string('horse_number',200)->nullable();
            $table->string('horse_name',200)->nullable();
            $table->string('unique_id',200)->nullable();
            $table->tinyInteger('result')->comment("0:for pending ,1:for win ,2:for loss")->default(0);
            $table->decimal("risk_amount",10,2)->nullable();
            $table->decimal("win_amount",10,2)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('horse_betting_details');
    }
}