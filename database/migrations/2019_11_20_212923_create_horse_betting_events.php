<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHorseBettingEvents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('horse_betting_events', function (Blueprint $table)
        {
            $table->bigIncrements('id');
            $table->bigInteger('user_id');
            $table->bigInteger('ticket_id')->nullable();
            $table->string('tournament_id', 200)->nullable();
            $table->string('tournament_name',200)->nullable();
            $table->string('race_id',200)->nullable();
            $table->string('race_name',200)->nullable();
            $table->string('horse_id',200)->nullable();
            $table->string('horse_number',200)->nullable();
            $table->string('horse_name',200)->nullable();
            $table->string('unique_id',200)->nullable();
            $table->decimal("win",10,2)->nullable();
            $table->decimal("place",10,2)->nullable();
            $table->decimal("show",10,2)->nullable();
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('horse_betting_events');
    }
}
