<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgentRatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agent_rates', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('player_id')->unsigned()->comment('user ID');
            $table->integer('user_id')->unsigned()->comment('agent ID');
            $table->decimal('rate', 10, 2)->default(0);
            $table->integer('is_paid')->default(0);
            $table->datetime('paid_at')->nullable();
            $table->date('create_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agent_rates');
    }
}
