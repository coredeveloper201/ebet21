<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTicketIdAndUserIdInParlayBettingEvents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('parlay_betting_events', function (Blueprint $table) {
            $table->bigInteger('user_id')->after('id');
            $table->bigInteger('ticket_id')->nullable()->after('user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('parlay_betting_events', function (Blueprint $table) {
            //
        });
    }
}
