<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCreditLimitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('credit_limits', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id');
            $table->decimal('all_min_amount', 10,2)->default(0)->nullable();
            $table->decimal('all_max_amount', 10,2)->default(0)->nullable();
            $table->decimal('straight_min_amount', 10,2)->default(0)->nullable();
            $table->decimal('straight_max_amount', 10,2)->default(0)->nullable();
            $table->decimal('casino_min_amount', 10,2)->default(0)->nullable();
            $table->decimal('casino_max_amount', 10,2)->default(0)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('credit_limits');
    }
}
