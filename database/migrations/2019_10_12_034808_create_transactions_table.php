<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('assign_id');
            $table->integer('user_id');
            $table->integer('betting_id');
            $table->decimal('amount', 10,2);
            $table->tinyInteger('type')->default(0)->comment('0 for withdrawal, 1 for deposit');
            $table->tinyInteger('status')->default(0);
            $table->text('description')->nullable();
            $table->text('private_note')->nullable();
            $table->tinyInteger('source')->nullable()->comment('0 from betting, 1 from agent done , 2 for Wager Delete By Agent, 5 For Free Pay Betting	');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
