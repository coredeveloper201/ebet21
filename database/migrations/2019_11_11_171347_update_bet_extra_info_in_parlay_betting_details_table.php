<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateBetExtraInfoInParlayBettingDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('parlay_betting_details', function (Blueprint $table)
        {
            $table->text('bet_extra_info')->nullable()->after('betting_condition_original');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('parlay_betting_details', function (Blueprint $table) {
        });
    }
}
