<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\BettingDetail;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(BettingDetail::class, function (Faker $faker) {
    return [
        'sport_league' => '['.$faker->randomNumber(4).', '.$faker->firstName.']',
        'user_id' => $faker->randomNumber(5),
        'ticket_id' => $faker->numberBetween(0, 10),
        'o_and_u' => "12",
        'event_id' => $faker->randomNumber(6),
        'event_date' => $faker->dateTimeBetween('this week', 'this month'),
        'team_id' => $faker->numberBetween(1, 9),
        'sport_name' => $faker->firstName,
        'is_away' => $faker->numberBetween(0, 4),
        'is_home' => $faker->numberBetween(0, 4),
        'betting_win' => $faker->randomNumber(5),
        'betting_condition' => $faker->randomNumber(5),
        'betting_condition_original' => $faker->text,
        'type' => $faker->numberBetween(0,3),
        'wager' => $faker->numberBetween(0,3),
        'risk_amount' => $faker->randomNumber(5),
        'betting_amount' => $faker->randomNumber(5),
        'token_id' => uniqid(),
        'status' => $faker->numberBetween(0, 2),
        'result' => $faker->numberBetween(0, 2),
        'user_current_credit_limit' => $faker->randomNumber(5),
        'user_current_max_limit' => $faker->randomNumber(5),
        'user_current_pending' => $faker->randomNumber(5),
        'user_current_balance' => $faker->randomNumber(5),
        'free_play' => $faker->numberBetween(0, 3),
        'bet_type' => 'straight',
    ];
});
