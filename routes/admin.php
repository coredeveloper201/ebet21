<?php


//Admin Route

Route::get('site-maintenance/check', 'Admin\AdminController@siteMaintenanceCheck')->name('maintenance.check');
Route::get('/maintenance', 'Admin\AdminController@maintenance')->name('maintenance');

Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function () {

    Route::get('dashboard', 'Admin\AdminController@dashboard')->name('home');

    Route::group(['middleware' => 'admin'], function () {
        Route::get('straight-league-approval', 'UserController\StraightBet@straightLeagueApproval');
        Route::post('wager-disable', 'UserController\StraightBet@WagerDisableStore')->name('wager-disable.store');
        Route::get('wager-disable-delete/{sport_league}', 'UserController\StraightBet@WagerDisableDelete');

        //Agent
        Route::resource('invoice', 'Admin\InvoiceController');
        Route::resource('agent', 'Admin\AgentController');

        // revert the changes
        //Assign User
        Route::get('assign-users', 'Admin\AgentassignController@assignUsers');
        Route::get('assign-user/create/{id}', 'Admin\AgentassignController@assignUsersCreate');
        Route::get('assign-user/delete/{id}', 'Admin\AgentassignController@assignUsersRemove');
        Route::post('assign-user/store', 'admin\agentassigncontroller@assignusersstore')->name('assign-user.store');

        //Admin
        Route::get('site-maintenance', 'Admin\AdminController@siteMaintenance');
        Route::post('site-maintenance/store', 'Admin\AdminController@siteMaintenanceStore')->name('maintenance.store');

        Route::resource('admin-manage', 'Admin\AdminController');
    });

    Route::group(['middleware' => 'admin' || 'agent' || 'subagent'], function () {
        //User
        Route::post('reset-password/{id}', 'Admin\UserController@resetPassword');
        Route::resource('user', 'Admin\UserController');
        Route::get('active-user', 'Admin\AdminController@activeUsersList')->name('user.active-user');
        Route::get('today-represent-lists', 'Admin\AdminController@todayReprestsList');
        Route::get('weekly-represent-lists', 'Admin\AdminController@weeklyReprestsList');
        Route::get('deleted-wager-lists', 'Admin\AdminController@deletedWagerListsby');
        Route::get('weekly-agents-wagers', 'Admin\AdminController@weeklyWager');
        Route::get('total-agents-wagers', 'Admin\AdminController@totalWager');

        //agentassign
        Route::get('agentassign/transaction/{id}', 'Admin\AgentassignController@transaction');
        Route::post('agentassign/transaction/store', 'Admin\AgentassignController@transactionStore')->name('transaction.store');

        Route::get('agentassign/limits/{id}', 'Admin\AgentassignController@limits');
        Route::post('agentassign/limits/store', 'Admin\AgentassignController@limitStore')->name('limit.store');

        Route::get('agentassign/pending/{id}', 'Admin\AgentassignController@pending');
        Route::get('agentassign/pending/delete/{id}', 'Admin\AgentassignController@pendingDelete');
        Route::get('agentassign/wager/{id}', 'Admin\AgentassignController@wager');

        Route::get('agentassign/notification/{id}', 'Admin\AgentassignController@notification');
        Route::post('agentassign/notification/store', 'Admin\AgentassignController@notificationStore')->name('notification.store');

        Route::get('agentassign/internet-log/{id}', 'Admin\AgentassignController@internetLog');
        Route::get('agentassign/distribution/{id}', 'Admin\AgentassignController@distribution');

        Route::get('agentassign/delete-account/{id}', 'Admin\AgentassignController@deleteAccount');
        Route::get('delete-account/{id}', 'Admin\AgentassignController@deleteAccountAction');

        //Record
        Route::get('record/weekly-figures', 'Admin\RecordController@weeklyFigures');
        Route::get('record/wagers', 'Admin\RecordController@wagers');
        Route::get('record/pending-wagers', 'Admin\RecordController@pendingWager');
        Route::get('record/delete-wagers', 'Admin\RecordController@deleteWager');
    });


    Route::group(['middleware' => 'agent'], function () {
        // Distribution
        Route::get('distribution/log', 'Admin\DistributionController@distributionLog');
        Route::get('distribution/summary', 'Admin\InvoiceController@index');

        //Sub Agent
        Route::get('subagent/assign-users', 'Admin\SubAgentController@assignUsers');
        Route::resource('subagent', 'Admin\SubAgentController');
    });

    Route::group(['middleware' => 'subagent'], function () { });
});
