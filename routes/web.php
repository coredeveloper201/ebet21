<?php

use App\Ebet\BetResults;
use App\Ebet\Sports\Live\Tennis;
use Elasticsearch\ClientBuilder;
use App\Ebet\Sports\Live\Baseball;
use App\Ebet\Sports\Live\Football;
use App\Ebet\Results\ResultPublish;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Ebet\Sports\Live\LiveBetFormat;
use App\Ebet\Sports\Straight\StraightSport;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



include('admin.php');
Route::post('/login', 'Auth\LoginController@login');
Route::get('/logout', function () {
    \Illuminate\Support\Facades\Auth::logout();
    return redirect('/login');
});

Auth::routes();



   Route::get('/', 'WebsiteController@login');



   Route::get('/checkApi',function(){
         $obj =new  StraightSport(
             new \App\Ebet\Sports\Straight\Basketball('https://www.goalserve.com/getfeed/4d5f7e372939425c2b7d08d76ad955ff/getodds/soccer?cat=basket_10',18)
            );
        return $obj->getLeagues();
   });

   Route::get('/checkresult',function(){

    return  (new ResultPublish(new App\Ebet\Results\Sports\Esport('http://www.goalserve.com/getfeed/4d5f7e372939425c2b7d08d76ad955ff/esports/d-1',151)))->getObj();



   });

   Route::get('/checkliveApi',function(){
    $hockey =  new LiveBetFormat(new Baseball('http://inplay.goalserve.com/inplay-baseball.json'));
    return $hockey->getData();
   });


        Route::get('storeData','WebsiteController@storeStraightSport');
        Route::get('storeLiveData','WebsiteController@storeLiveSport');
        Route::get('storeHorseData','WebsiteController@storeHorse');
        Route::get('result','WebsiteController@getResult');
        Route::get('test_live_data','UserController\LiveBetsController@getSportsData');

        Route::get('/live-football', 'UserController\LiveBetsController@liveFootBall');

Route::group(['middleware' => ['httpReferer']], function () {
    Route::group(['middleware' => ['auth']], function () {
        Route::get('/header-pending-data', 'WebsiteController@pendingAjax');

       // Route::get('/straight-bet', 'UserController\StraightBet@storeSports');
        Route::get('/straight-bet', 'UserController\StraightBet@index');

        Route::post('/straight_odds', 'UserController\StraightBet@straightOdds');

        /*
          required when user refresh the page
        */
        Route::get('/straight_odds', 'UserController\StraightBet@straightOdds');

        Route::get('/rules', function () {
            return view('frontend.rules');
        });

        Route::get('/account', 'WebsiteController@account');
        Route::get('/account/wager', 'WebsiteController@accountWager');
        Route::get('/pending', 'WebsiteController@pending');
        Route::get('/live-casino', 'WebsiteController@casinoLive');
        Route::get('/horses', 'UserController\HorseBet@index');
        Route::get('/guide', 'WebsiteController@guide');
        Route::get('/casino-games', 'WebsiteController@casino');
        Route::get('/casino-games/{gameName}', 'WebsiteController@casinoGames');
        //game
        Route::get('/game/{gameName}/start', 'WebsiteController@gamePlay');
        Route::post('/casino/store', 'WebsiteController@casinoHistoryStore')->name('casino.store');

        Route::post('/props-bet', 'UserController\StraightBet@propsBet');

        Route::post('/check-straight-bet', 'BetSave@index');
        Route::post ('/save-horse-bet','BetSave@saveHorseBet');
        Route::post('/check-password-straight-bet', 'BetSave@checkBetPassword');
        Route::post('/save-straight-bet', 'BetSave@saveStraightBet');
        Route::post('/save-parlay-bet','BetSave@saveParlayBet');


        /*
        |----------------------------------------------------
        | Live bets start
        |----------------------------------------------------
        */

        Route::get('/live-games', function () {
            return view('frontend.live_games');
        });

        /*
        |-----------------------------------------------------------------
        | we may use these routes to check every live bet page individually
        |---------------------------------------------------------------
        */
         Route::get('/live-basket-view', function () {
            return view('frontend.live-basket-test');
        });
        /* Route::get('/live-basket-ball-view', function () {
            return view('frontend.live_bets.live-baseball');
        }); */
        /* Route::get('/live-base-ball-view', function () {
            return view('frontend.live_bets.live-baseball');
        }); */
        Route::get('/live-sports', 'UserController\LiveBetsController@liveSports');
        Route::get('/live-sports-menu', 'UserController\LiveBetsController@liveSportsMenus');
        Route::get('/live-tennis', 'UserController\LiveBetsController@liveTennis');
        Route::get('/live-hockey', 'UserController\LiveBetsController@liveHockey');
        Route::get('/live-base-ball', 'UserController\LiveBetsController@liveBaseBall');
        Route::get('/live-basket-ball', 'UserController\LiveBetsController@liveBasketBall');
        Route::get('/live-vollyball', 'UserController\LiveBetsController@liveVolleyBall');
        Route::get('/live-motor-sport', 'UserController\LiveBetsController@liveMotorSport');
        Route::get('/live-cycling-sport', 'UserController\LiveBetsController@liveCycling');
        Route::get('/live-soccer-league', 'UserController\LiveBetsController@liveSoccerLeaguesLists');
        Route::post('/check-user-password', 'UserController\LiveBetsController@checkUserpassword');
        Route::get('/live-soccer', 'UserController\LiveBetsController@liveSoccer');
        Route::get('/live-soccer-view', 'UserController\LiveBetsController@liveSoccer');
        Route::post('/save-live-data', 'BetSave@saveLiveBet');
        Route::get('/test-result', 'WebsiteController@getResult');

        /* live hockey view on test purpose */
        Route::get('/live-hockey-view', 'UserController\LiveBetsController@liveHockeyView');
    });
});


Route::group(['prefix' => 'result'], function () {
    Route::get('/football', function(){
        $sportIdArr = [];
        for($i=0;$i<8; $i++)
        {
            if($i==0)  $sportIdArr += (new ResultPublish(new App\Ebet\Results\Sports\Football('http://www.goalserve.com/getfeed/4d5f7e372939425c2b7d08d76ad955ff/football/home',12)))->getObj();
            else $sportIdArr += (new ResultPublish(new App\Ebet\Results\Sports\Football('http://www.goalserve.com/getfeed/4d5f7e372939425c2b7d08d76ad955ff/football/d-'.$i,12)))->getObj();
        }
        return $sportIdArr;
    });
    Route::get('/baseball', function(){
        $sportIdArr = [];
        for($i=0;$i<8; $i++)
        {
            if($i==0)  $sportIdArr += (new ResultPublish(new App\Ebet\Results\Sports\Baseball('http://www.goalserve.com/getfeed/4d5f7e372939425c2b7d08d76ad955ff/baseball/home',16)))->getObj();
            else $sportIdArr += (new ResultPublish(new App\Ebet\Results\Sports\Baseball('http://www.goalserve.com/getfeed/4d5f7e372939425c2b7d08d76ad955ff/baseball/d-'.$i,16)))->getObj();
        }
        return $sportIdArr;
    });
    Route::get('/basketball', function(){
        $sportIdArr = [];
        for($i=0;$i<8; $i++)
        {
            if($i==0)  $sportIdArr += (new ResultPublish(new App\Ebet\Results\Sports\Basketball('http://www.goalserve.com/getfeed/4d5f7e372939425c2b7d08d76ad955ff/bsktbl/home',18)))->getObj();
            else $sportIdArr += (new ResultPublish(new App\Ebet\Results\Sports\Basketball('http://www.goalserve.com/getfeed/4d5f7e372939425c2b7d08d76ad955ff/bsktbl/d-'.$i,18)))->getObj();
        }
        return $sportIdArr;
    });
    Route::get('/volleyball', function(){
        $obj2 = [];
        for($i=0;$i<8; $i++)
           {
               if($i==0) $obj2 += (new ResultPublish(new App\Ebet\Results\Sports\Volleyball('http://www.goalserve.com/getfeed/4d5f7e372939425c2b7d08d76ad955ff/volleyball/home',14)))->getObj();
               else $obj2 += (new ResultPublish(new App\Ebet\Results\Sports\Volleyball('http://www.goalserve.com/getfeed/4d5f7e372939425c2b7d08d76ad955ff/volleyball/d-'.$i,14)))->getObj();
           }
           return $obj2;
    });
    Route::get('/e-sport', function(){
        $obj2 = [];
        for($i=0;$i<8; $i++)
           {
               if($i==0) $obj2 += (new ResultPublish(new App\Ebet\Results\Sports\Esport('http://www.goalserve.com/getfeed/4d5f7e372939425c2b7d08d76ad955ff/esports/home',151)))->getObj();
               else $obj2 += (new ResultPublish(new App\Ebet\Results\Sports\Esport('http://www.goalserve.com/getfeed/4d5f7e372939425c2b7d08d76ad955ff/esports/d-'.$i,151)))->getObj();
           }
           return $obj2;
    });
    Route::get('/hockey', function(){
        $sportIdArr = [];
        for($i=0;$i<8; $i++)
        {
            if($i==0)  $sportIdArr += (new ResultPublish(new App\Ebet\Results\Sports\Hockey('http://www.goalserve.com/getfeed/4d5f7e372939425c2b7d08d76ad955ff/hockey/home',17)))->getObj();
            else $sportIdArr += (new ResultPublish(new App\Ebet\Results\Sports\Hockey('http://www.goalserve.com/getfeed/4d5f7e372939425c2b7d08d76ad955ff/hockey/d-'.$i,17)))->getObj();
        }
        return $sportIdArr;
    });
    Route::get('/soccer', function(){
        $sportIdArr = [];
        for($i=0;$i<8; $i++)
        {
            if($i==0)  $sportIdArr += (new ResultPublish(new App\Ebet\Results\Sports\Soccer('http://www.goalserve.com/getfeed/4d5f7e372939425c2b7d08d76ad955ff/soccernew/home',1)))->getObj();
            else $sportIdArr += (new ResultPublish(new App\Ebet\Results\Sports\Soccer('http://www.goalserve.com/getfeed/4d5f7e372939425c2b7d08d76ad955ff/soccernew/d-'.$i,1)))->getObj();
        }
        return $sportIdArr;
    });
    Route::get('/handball', function(){
        $sportIdArr = [];
        for($i=0;$i<8; $i++)
        {
            if($i==0)  $sportIdArr += (new ResultPublish(new App\Ebet\Results\Sports\Handball('http://www.goalserve.com/getfeed/4d5f7e372939425c2b7d08d76ad955ff/handball/home',19)))->getObj();
            else $sportIdArr += (new ResultPublish(new App\Ebet\Results\Sports\Handball('http://www.goalserve.com/getfeed/4d5f7e372939425c2b7d08d76ad955ff/handball/d-'.$i,19)))->getObj();
        }
        return $sportIdArr;
    });
    Route::get('/rugby', function(){
        $sportIdArr = [];
        for($i=0;$i<8; $i++)
        {
            if($i==0)  $sportIdArr += (new ResultPublish(new App\Ebet\Results\Sports\Rugby('http://www.goalserve.com/getfeed/4d5f7e372939425c2b7d08d76ad955ff/rugby/home',19)))->getObj();
            else $sportIdArr += (new ResultPublish(new App\Ebet\Results\Sports\Rugby('http://www.goalserve.com/getfeed/4d5f7e372939425c2b7d08d76ad955ff/rugby/d-'.$i,19)))->getObj();
        }
        return $sportIdArr;
    });
    Route::get('/tennis', function(){
        $obj = new App\Ebet\Results\Sports\Tennis('http://www.goalserve.com/getfeed/4d5f7e372939425c2b7d08d76ad955ff/tennis_scores/d-1',13);
        return $obj->evaluateResult();
    });
    Route::get('/cricket', function(){
        $obj2 = (new ResultPublish( new App\Ebet\Results\Sports\Cricket('http://www.goalserve.com/getfeed/4d5f7e372939425c2b7d08d76ad955ff/cricket/home',15)))->getObj();
        $obj2 += (new ResultPublish( new App\Ebet\Results\Sports\Cricket('http://www.goalserve.com/getfeed/4d5f7e372939425c2b7d08d76ad955ff/cricket/d-1',15)))->getObj();
        return $obj2;
    });
    Route::get('all', function () {
        (new BetResults())();
    });
    Route::get('live/{date}/{id}', function ($date,$id) {
        return __liveBetResult($date,$id);
    });
});


/* Route::group(['prefix' => 'load'], function () {

    Route::group(['prefix' => 'live'], function () {
        $live_all = new StoreLiveOdds();
        Route::get('all',function() use($live_all){
           return  $live_all();
        });
    });
}); */


Route::group(['prefix' => 'straight'], function () {
    Route::group(['prefix' => 'db'], function () {
        $client = ClientBuilder::create()->build();
        Route::get('/football', function() use($client){
              return   $client->get(['index'=> 'football','id'=>'sport_12_football']);
        });
        Route::get('/tennis', function() use($client){
              return   $client->get(['index'=> 'tennis','id'=>'sport_13_tennis']);
        });
        Route::get('/hockey', function() use($client){
              return   $client->get(['index'=> 'hockey','id'=>'sport_17_hockey']);
        });
        Route::get('/basketball', function() use($client){
              return  $client->get(['index'=> 'basketball','id'=>'sport_18_basketball']);
        });
        Route::get('/volleyball', function() use($client){
              return   $client->get(['index'=> 'volleyball','id'=>'sport_14_volleyball']);
        });
        Route::get('/soccer', function() use($client){
              return   $client->get(['index'=> 'soccer','id'=>'sport_1_soccer']);
        });
        Route::get('/boxing', function() use($client){
              return   $client->get(['index'=> 'boxing','id'=>'sport_11_boxing']);
        });
        Route::get('/mma', function() use($client){
              return   $client->get(['index'=> 'mma','id'=>'sport_9_mma']);
        });
        Route::get('/baseball', function() use($client){
              return   $client->get(['index'=> 'baseball','id'=>'sport_16_baseball']);
        });
        Route::get('/cricket', function() use($client){
              return   $client->get(['index'=> 'cricket','id'=>'sport_15_cricket']);
        });
    });



    Route::group(['prefix' => 'load'], function () {

        $elastic_sports = [
            '16' => "baseball",
            '17' => "hockey",
            '9'  => "mma",
            '13' => "tennis",
            '12' => "football",
            '1'  => "soccer",
            '18' => "basketball",
            '11' => "boxing",
            '14' => "volleyball",
            '15' => "cricket",
            '19' => "handball",
            '151' => "esports",
            '20' => "rugby",
            '21' => "rugby_league",
        ];
        Route::get('/football', function ()  use($elastic_sports) {
            $client = ClientBuilder::create()->build();
            $arr = [];
            $football = new StraightSport(new \App\Ebet\Sports\Straight\Football('https://www.goalserve.com/getfeed/4d5f7e372939425c2b7d08d76ad955ff/getodds/soccer?cat=football_10',12));
            $sports_data =  $football->getLeagues();
            $sprtKey = 12;
            $params = [
                'index' => $elastic_sports[$sprtKey],
                'id'    => 'sport_'.$sprtKey."_".$elastic_sports[$sprtKey],
                'body'  => [$elastic_sports[$sprtKey].'_sports' => json_decode( json_encode($sports_data), true)]
            ];
            $arr[] = $client->index($params);

            $leagues_Store = [];

           foreach($sports_data as $sport_data)
           {
                $leagues_Store['12_'.$sport_data->league_id] = $sport_data;
           }

            if(!empty($leagues_Store))
            {
                foreach($leagues_Store as $league_id => $data)
                {
                    $league_params = [
                        'index' => 'league_wise_data',
                        'id'    => $league_id,
                        'body'  => ['league_data' => $data ]
                    ];
                    $arr[] = $client->index($league_params);
                }
            }
            print_r($arr);
        });


        Route::get('/hockey', function ()  use($elastic_sports) {
            $client = ClientBuilder::create()->build();
            $arr = [];
            $hockey = new StraightSport(new \App\Ebet\Sports\Straight\Hockey('https://www.goalserve.com/getfeed/4d5f7e372939425c2b7d08d76ad955ff/getodds/soccer?cat=hockey_10&bm=17,16,',17));
            $sports_data =  $hockey->getLeagues();
            $sprtKey = 17;
            $params = [
                'index' => $elastic_sports[$sprtKey],
                'id'    => 'sport_'.$sprtKey."_".$elastic_sports[$sprtKey],
                'body'  => [$elastic_sports[$sprtKey].'_sports' => json_decode( json_encode($sports_data), true)]
            ];
            $arr[] = $client->index($params);

            $leagues_Store = [];

           foreach($sports_data as $sport_data)
           {
                $leagues_Store['17_'.$sport_data->league_id] = $sport_data;
           }

            if(!empty($leagues_Store))
            {
                foreach($leagues_Store as $league_id => $data)
                {
                    $league_params = [
                        'index' => 'league_wise_data',
                        'id'    => $league_id,
                        'body'  => ['league_data' => $data ]
                    ];
                    $arr[] = $client->index($league_params);
                }
            }
            print_r($arr);
        });


        Route::get('/volleyball', function ()  use($elastic_sports) {
            $client = ClientBuilder::create()->build();
            $arr = [];
            $volleyball = new StraightSport(new \App\Ebet\Sports\Straight\Volleyball('https://www.goalserve.com/getfeed/4d5f7e372939425c2b7d08d76ad955ff/getodds/soccer?cat=volleyball_10',14));
            $sports_data =  $volleyball->getLeagues();
            $sprtKey = 14;
            $params = [
                'index' => $elastic_sports[$sprtKey],
                'id'    => 'sport_'.$sprtKey."_".$elastic_sports[$sprtKey],
                'body'  => [$elastic_sports[$sprtKey].'_sports' => json_decode( json_encode($sports_data), true)]
            ];
            $arr[] = $client->index($params);

            $leagues_Store = [];

           foreach($sports_data as $sport_data)
           {
                $leagues_Store['14_'.$sport_data->league_id] = $sport_data;
           }

            if(!empty($leagues_Store))
            {
                foreach($leagues_Store as $league_id => $data)
                {
                    $league_params = [
                        'index' => 'league_wise_data',
                        'id'    => $league_id,
                        'body'  => ['league_data' => $data ]
                    ];
                    $arr[] = $client->index($league_params);
                }
            }
            print_r($arr);
        });


        Route::get('/baseball', function ()  use($elastic_sports) {
            $client = ClientBuilder::create()->build();
            $arr = [];
            $sprtKey = 16;
            $baseball = new StraightSport(new \App\Ebet\Sports\Straight\Baseball('http://www.goalserve.com/getfeed/4d5f7e372939425c2b7d08d76ad955ff/getodds/soccer?cat=baseball_10',$sprtKey));
            $sports_data =  $baseball->getLeagues();
            $params = [
                'index' => $elastic_sports[$sprtKey],
                'id'    => 'sport_'.$sprtKey."_".$elastic_sports[$sprtKey],
                'body'  => [$elastic_sports[$sprtKey].'_sports' => json_decode( json_encode($sports_data), true)]
            ];
            $arr[] = $client->index($params);

            $leagues_Store = [];

           foreach($sports_data as $sport_data)
           {
                $leagues_Store[$sprtKey.'_'.$sport_data->league_id] = $sport_data;
           }

            if(!empty($leagues_Store))
            {
                foreach($leagues_Store as $league_id => $data)
                {
                    $league_params = [
                        'index' => 'league_wise_data',
                        'id'    => $league_id,
                        'body'  => ['league_data' => $data ]
                    ];
                    $arr[] = $client->index($league_params);
                }
            }
            print_r($arr);
        });

        Route::get('/cricket', function ()  use($elastic_sports) {
            $client = ClientBuilder::create()->build();
            $arr = [];
            $sprtKey = 15;
            $cricket = new StraightSport(new \App\Ebet\Sports\Straight\Cricket('https://www.goalserve.com/getfeed/4d5f7e372939425c2b7d08d76ad955ff/getodds/soccer?cat=cricket_10',$sprtKey));
            $sports_data =  $cricket->getLeagues();
            $params = [
                'index' => $elastic_sports[$sprtKey],
                'id'    => 'sport_'.$sprtKey."_".$elastic_sports[$sprtKey],
                'body'  => [$elastic_sports[$sprtKey].'_sports' => json_decode( json_encode($sports_data), true)]
            ];
            $arr[] = $client->index($params);

            $leagues_Store = [];

           foreach($sports_data as $sport_data)
           {
                $leagues_Store[$sprtKey.'_'.$sport_data->league_id] = $sport_data;
           }

            if(!empty($leagues_Store))
            {
                foreach($leagues_Store as $league_id => $data)
                {
                    $league_params = [
                        'index' => 'league_wise_data',
                        'id'    => $league_id,
                        'body'  => ['league_data' => $data ]
                    ];
                    $arr[] = $client->index($league_params);
                }
            }
            print_r($arr);
        });


        Route::get('/handball', function ()  use($elastic_sports) {
            $client = ClientBuilder::create()->build();
            $arr = [];
            $sprtKey = 19;
            $handball = new StraightSport(new \App\Ebet\Sports\Straight\Handball('https://www.goalserve.com/getfeed/4d5f7e372939425c2b7d08d76ad955ff/getodds/soccer?cat=handball_10',$sprtKey));
            $sports_data =  $handball->getLeagues();
            $params = [
                'index' => $elastic_sports[$sprtKey],
                'id'    => 'sport_'.$sprtKey."_".$elastic_sports[$sprtKey],
                'body'  => [$elastic_sports[$sprtKey].'_sports' => json_decode( json_encode($sports_data), true)]
            ];
            $arr[] = $client->index($params);

            $leagues_Store = [];

           foreach($sports_data as $sport_data)
           {
                $leagues_Store[$sprtKey.'_'.$sport_data->league_id] = $sport_data;
           }

            if(!empty($leagues_Store))
            {
                foreach($leagues_Store as $league_id => $data)
                {
                    $league_params = [
                        'index' => 'league_wise_data',
                        'id'    => $league_id,
                        'body'  => ['league_data' => $data ]
                    ];
                    $arr[] = $client->index($league_params);
                }
            }
            print_r($arr);
        });

        Route::get('/tennis', function ()  use($elastic_sports) {
            $client = ClientBuilder::create()->build();
            $arr = [];
            $sprtKey = 13;
            $tennis = new StraightSport(new \App\Ebet\Sports\Straight\Tennis('https://www.goalserve.com/getfeed/4d5f7e372939425c2b7d08d76ad955ff/getodds/soccer?cat=tennis_10',$sprtKey));
            $sports_data =  $tennis->getLeagues();
            $params = [
                'index' => $elastic_sports[$sprtKey],
                'id'    => 'sport_'.$sprtKey."_".$elastic_sports[$sprtKey],
                'body'  => [$elastic_sports[$sprtKey].'_sports' => json_decode( json_encode($sports_data), true)]
            ];
            $arr[] = $client->index($params);

            $leagues_Store = [];

           foreach($sports_data as $sport_data)
           {
                $leagues_Store[$sprtKey.'_'.$sport_data->league_id] = $sport_data;
           }

            if(!empty($leagues_Store))
            {
                foreach($leagues_Store as $league_id => $data)
                {
                    $league_params = [
                        'index' => 'league_wise_data',
                        'id'    => $league_id,
                        'body'  => ['league_data' => $data ]
                    ];
                    $arr[] = $client->index($league_params);
                }
            }
            print_r($arr);
        });

        Route::get('/boxing', function ()  use($elastic_sports) {
            $client = ClientBuilder::create()->build();
            $arr = [];
            $sprtKey = 11;
            $boxing = new StraightSport(new \App\Ebet\Sports\Straight\Boxing('https://www.goalserve.com/getfeed/4d5f7e372939425c2b7d08d76ad955ff/getodds/soccer?cat=boxing_10',11));
            $sports_data =  $boxing->getLeagues();
            $params = [
                'index' => $elastic_sports[$sprtKey],
                'id'    => 'sport_'.$sprtKey."_".$elastic_sports[$sprtKey],
                'body'  => [$elastic_sports[$sprtKey].'_sports' => json_decode( json_encode($sports_data), true)]
            ];
            $arr[] = $client->index($params);

            $leagues_Store = [];

           foreach($sports_data as $sport_data)
           {
                $leagues_Store[$sprtKey.'_'.$sport_data->league_id] = $sport_data;
           }

            if(!empty($leagues_Store))
            {
                foreach($leagues_Store as $league_id => $data)
                {
                    $league_params = [
                        'index' => 'league_wise_data',
                        'id'    => $league_id,
                        'body'  => ['league_data' => $data ]
                    ];
                    $arr[] = $client->index($league_params);
                }
            }
            print_r($arr);
        });


        Route::get('/mma', function ()  use($elastic_sports) {
            $client = ClientBuilder::create()->build();
            $arr = [];
            $sprtKey = 9;
            $mma = new StraightSport(new \App\Ebet\Sports\Straight\Mma('https://www.goalserve.com/getfeed/4d5f7e372939425c2b7d08d76ad955ff/getodds/soccer?cat=mma_10',9));
            $sports_data =  $mma->getLeagues();
            $params = [
                'index' => $elastic_sports[$sprtKey],
                'id'    => 'sport_'.$sprtKey."_".$elastic_sports[$sprtKey],
                'body'  => [$elastic_sports[$sprtKey].'_sports' => json_decode( json_encode($sports_data), true)]
            ];
            $arr[] = $client->index($params);

            $leagues_Store = [];

           foreach($sports_data as $sport_data)
           {
                $leagues_Store[$sprtKey.'_'.$sport_data->league_id] = $sport_data;
           }

            if(!empty($leagues_Store))
            {
                foreach($leagues_Store as $league_id => $data)
                {
                    $league_params = [
                        'index' => 'league_wise_data',
                        'id'    => $league_id,
                        'body'  => ['league_data' => $data ]
                    ];
                    $arr[] = $client->index($league_params);
                }
            }
            print_r($arr);
        });


        Route::get('/e-sport', function ()  use($elastic_sports) {
            $client = ClientBuilder::create()->build();
            $arr = [];
            $sprtKey = 151;
            $e_sport = new StraightSport(new \App\Ebet\Sports\Straight\Esports('https://www.goalserve.com/getfeed/4d5f7e372939425c2b7d08d76ad955ff/getodds/soccer?cat=esports_10',151));
            $sports_data =  $e_sport->getLeagues();
            $params = [
                'index' => $elastic_sports[$sprtKey],
                'id'    => 'sport_'.$sprtKey."_".$elastic_sports[$sprtKey],
                'body'  => [$elastic_sports[$sprtKey].'_sports' => json_decode( json_encode($sports_data), true)]
            ];
            $arr[] = $client->index($params);

            $leagues_Store = [];

           foreach($sports_data as $sport_data)
           {
                $leagues_Store[$sprtKey.'_'.$sport_data->league_id] = $sport_data;
           }

            if(!empty($leagues_Store))
            {
                foreach($leagues_Store as $league_id => $data)
                {
                    $league_params = [
                        'index' => 'league_wise_data',
                        'id'    => $league_id,
                        'body'  => ['league_data' => $data ]
                    ];
                    $arr[] = $client->index($league_params);
                }
            }
            print_r($arr);
        });


        Route::get('/basketball', function ()  use($elastic_sports) {
            $client = ClientBuilder::create()->build();
            $arr = [];
            $sprtKey = 18;
            $basketball = new StraightSport(new \App\Ebet\Sports\Straight\Basketball('https://www.goalserve.com/getfeed/4d5f7e372939425c2b7d08d76ad955ff/getodds/soccer?cat=basket_10&bm=16,',18));
            $sports_data =  $basketball->getLeagues();
            $params = [
                'index' => $elastic_sports[$sprtKey],
                'id'    => 'sport_'.$sprtKey."_".$elastic_sports[$sprtKey],
                'body'  => [$elastic_sports[$sprtKey].'_sports' => json_decode( json_encode($sports_data), true)]
            ];
            $arr[] = $client->index($params);

            $leagues_Store = [];

           foreach($sports_data as $sport_data)
           {
                $leagues_Store[$sprtKey.'_'.$sport_data->league_id] = $sport_data;
           }

            if(!empty($leagues_Store))
            {
                foreach($leagues_Store as $league_id => $data)
                {
                    $league_params = [
                        'index' => 'league_wise_data',
                        'id'    => $league_id,
                        'body'  => ['league_data' => $data ]
                    ];
                    $arr[] = $client->index($league_params);
                }
            }
            print_r($arr);
        });


        Route::get('/soccer', function ()  use($elastic_sports) {
            $client = ClientBuilder::create()->build();
            $arr = [];
            $sprtKey = 1;
            $soccer = new StraightSport(new \App\Ebet\Sports\Straight\Soccer('http://www.goalserve.com/getfeed/4d5f7e372939425c2b7d08d76ad955ff/getodds/soccer?cat=soccer_10&&bm=56,16,',1));
            $sports_data =  $soccer->getLeagues();
            $params = [
                'index' => $elastic_sports[$sprtKey],
                'id'    => 'sport_'.$sprtKey."_".$elastic_sports[$sprtKey],
                'body'  => [$elastic_sports[$sprtKey].'_sports' => json_decode( json_encode($sports_data), true)]
            ];
            $arr[] = $client->index($params);

            $leagues_Store = [];

           foreach($sports_data as $sport_data)
           {
                $leagues_Store[$sprtKey.'_'.$sport_data->league_id] = $sport_data;
           }

            if(!empty($leagues_Store))
            {
                foreach($leagues_Store as $league_id => $data)
                {
                    $league_params = [
                        'index' => 'league_wise_data',
                        'id'    => $league_id,
                        'body'  => ['league_data' => $data ]
                    ];
                    $arr[] = $client->index($league_params);
                }
            }
            print_r($arr);
        });


        Route::get('/rugby', function ()  use($elastic_sports) {
            $client = ClientBuilder::create()->build();
            $arr = [];
            $sprtKey = 20;
            $rugby = new StraightSport(new \App\Ebet\Sports\Straight\Rugby('https://www.goalserve.com/getfeed/4d5f7e372939425c2b7d08d76ad955ff/getodds/soccer?cat=rugby_10',20));
            $sports_data =  $rugby->getLeagues();
            $params = [
                'index' => $elastic_sports[$sprtKey],
                'id'    => 'sport_'.$sprtKey."_".$elastic_sports[$sprtKey],
                'body'  => [$elastic_sports[$sprtKey].'_sports' => json_decode( json_encode($sports_data), true)]
            ];
            $arr[] = $client->index($params);

            $leagues_Store = [];

           foreach($sports_data as $sport_data)
           {
                $leagues_Store[$sprtKey.'_'.$sport_data->league_id] = $sport_data;
           }

            if(!empty($leagues_Store))
            {
                foreach($leagues_Store as $league_id => $data)
                {
                    $league_params = [
                        'index' => 'league_wise_data',
                        'id'    => $league_id,
                        'body'  => ['league_data' => $data ]
                    ];
                    $arr[] = $client->index($league_params);
                }
            }
            print_r($arr);
        });

        Route::get('/rugby-league', function ()  use($elastic_sports) {
            $client = ClientBuilder::create()->build();
            $arr = [];
            $sprtKey = 21;
            $rugby_league = new StraightSport(new \App\Ebet\Sports\Straight\RugbyLeague('https://www.goalserve.com/getfeed/4d5f7e372939425c2b7d08d76ad955ff/getodds/soccer?cat=rugbyleague_10',21));
            $sports_data =  $rugby_league->getLeagues();
            $params = [
                'index' => $elastic_sports[$sprtKey],
                'id'    => 'sport_'.$sprtKey."_".$elastic_sports[$sprtKey],
                'body'  => [$elastic_sports[$sprtKey].'_sports' => json_decode( json_encode($sports_data), true)]
            ];
            $arr[] = $client->index($params);

            $leagues_Store = [];

           foreach($sports_data as $sport_data)
           {
                $leagues_Store[$sprtKey.'_'.$sport_data->league_id] = $sport_data;
           }

            if(!empty($leagues_Store))
            {
                foreach($leagues_Store as $league_id => $data)
                {
                    $league_params = [
                        'index' => 'league_wise_data',
                        'id'    => $league_id,
                        'body'  => ['league_data' => $data ]
                    ];
                    $arr[] = $client->index($league_params);
                }
            }
            print_r($arr);
        });

    });

    Route::group(['prefix' => 'api'], function () {
        $client = ClientBuilder::create()->build();
        Route::get('/football', function(){
              return   (new StraightSport(
                  new \App\Ebet\Sports\Straight\Football('https://www.goalserve.com/getfeed/4d5f7e372939425c2b7d08d76ad955ff/getodds/soccer?cat=football_10',12)
                ))->getLeagues();
        });
        Route::get('/hockey', function(){
            return   (new StraightSport(
                new \App\Ebet\Sports\Straight\Hockey('https://www.goalserve.com/getfeed/4d5f7e372939425c2b7d08d76ad955ff/getodds/soccer?cat=hockey_10&bm=17,16,',17)
            ))->getLeagues();
        });
        Route::get('/basketball', function(){
            return   (new StraightSport(
                new \App\Ebet\Sports\Straight\Basketball('https://www.goalserve.com/getfeed/4d5f7e372939425c2b7d08d76ad955ff/getodds/soccer?cat=basket_10&bm=16,',18)
            ))->getLeagues();
        });
        Route::get('/volleyball', function(){
            return   (new StraightSport(
                new \App\Ebet\Sports\Straight\Volleyball('https://www.goalserve.com/getfeed/4d5f7e372939425c2b7d08d76ad955ff/getodds/soccer?cat=volleyball_10',14)
            ))->getLeagues();
        });
        Route::get('/soccer', function(){
            return   (new StraightSport(
                new \App\Ebet\Sports\Straight\Soccer('http://www.goalserve.com/getfeed/4d5f7e372939425c2b7d08d76ad955ff/getodds/soccer?cat=soccer_10&bm=56,16,',1)
            ))->getLeagues();
        });
        Route::get('/boxing', function(){
            return   (new StraightSport(
                new \App\Ebet\Sports\Straight\Boxing('https://www.goalserve.com/getfeed/4d5f7e372939425c2b7d08d76ad955ff/getodds/soccer?cat=boxing_10',11)
            ))->getLeagues();
        });
        Route::get('/mma', function(){
            return   (new StraightSport(
                new \App\Ebet\Sports\Straight\Mma('https://www.goalserve.com/getfeed/4d5f7e372939425c2b7d08d76ad955ff/getodds/soccer?cat=mma_10',9)
            ))->getLeagues();
        });
        Route::get('/baseball', function(){
            return   (new StraightSport(
                new \App\Ebet\Sports\Straight\Baseball('https://www.goalserve.com/getfeed/4d5f7e372939425c2b7d08d76ad955ff/getodds/soccer?cat=baseball_10',16)
            ))->getLeagues();
        });
        Route::get('/tennis', function(){
            return   (new StraightSport(
                new \App\Ebet\Sports\Straight\Tennis('https://www.goalserve.com/getfeed/4d5f7e372939425c2b7d08d76ad955ff/getodds/soccer?cat=tennis_10',13)
            ))->getLeagues();
        });
        Route::get('/handball', function(){
            return   (new StraightSport(
                new \App\Ebet\Sports\Straight\Handball('http://www.goalserve.com/getfeed/4d5f7e372939425c2b7d08d76ad955ff/getodds/soccer?cat=handball_10',19)
            ))->getLeagues();
        });
        Route::get('/e-sport', function(){
            return   (new StraightSport(
                new \App\Ebet\Sports\Straight\Esports('http://www.goalserve.com/getfeed/4d5f7e372939425c2b7d08d76ad955ff/getodds/soccer?cat=esports_10',151)
            ))->getLeagues();
        });
        Route::get('/cricket', function(){
            return   (new StraightSport(
                new \App\Ebet\Sports\Straight\Cricket('http://www.goalserve.com/getfeed/4d5f7e372939425c2b7d08d76ad955ff/getodds/soccer?cat=cricket_10',15)
            ))->getLeagues();
        });
        Route::get('/rugby', function(){
            return   (new StraightSport(
                new \App\Ebet\Sports\Straight\Rugby('http://www.goalserve.com/getfeed/4d5f7e372939425c2b7d08d76ad955ff/getodds/soccer?cat=rugby_10',20)
            ))->getLeagues();
        });
        Route::get('/rugby-league', function(){
            return   (new StraightSport(
                new \App\Ebet\Sports\Straight\RugbyLeague('http://www.goalserve.com/getfeed/4d5f7e372939425c2b7d08d76ad955ff/getodds/soccer?cat=rugbyleague_10',21)
            ))->getLeagues();
        });
    });
});




Route::group(['prefix' => 'live'], function () {
    Route::get('/football', function () {
        $url = 'http://inplay.goalserve.com/inplay-amfootball.json';
        $arrContextOptions=array(
            "ssl"=>array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ),
        );
        return file_get_contents($url, false, stream_context_create($arrContextOptions));
    });

    Route::get('/soccer', function () {
        $url = 'http://inplay.goalserve.com/inplay-soccer.json';
        $arrContextOptions=array(
            "ssl"=>array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ),
        );
        return file_get_contents($url, false, stream_context_create($arrContextOptions));
    });

    Route::get('/basketball', function () {
        $url = 'http://inplay.goalserve.com/inplay-basket.json';
        $arrContextOptions=array(
            "ssl"=>array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ),
        );
        return file_get_contents($url, false, stream_context_create($arrContextOptions));
    });

    Route::get('/hockey', function () {
        $url = 'http://inplay.goalserve.com/inplay-hockey.json';
        $arrContextOptions=array(
            "ssl"=>array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ),
        );
        return file_get_contents($url, false, stream_context_create($arrContextOptions));
    });

    Route::get('/baseball', function () {
        $url = 'http://inplay.goalserve.com/inplay-baseball.json';
        $arrContextOptions=array(
            "ssl"=>array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ),
        );
        return file_get_contents($url, false, stream_context_create($arrContextOptions));
    });

    Route::get('/tennis', function () {
        $url = 'http://inplay.goalserve.com/inplay-tennis.json';
        $arrContextOptions=array(
            "ssl"=>array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ),
        );
        return file_get_contents($url, false, stream_context_create($arrContextOptions));
    });
});
